<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>A speaker at one of the Eventforce sessions</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Speaker</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Best_Practice_Scenario__c</fullName>
        <externalId>false</externalId>
        <label>Best Practice Story (Brief Description)</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Clean_Account_Name__c</fullName>
        <externalId>false</externalId>
        <label>Clean Account Name</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Clean_Contact_Name__c</fullName>
        <externalId>false</externalId>
        <label>Clean Contact Name</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_ID_New__c</fullName>
        <externalId>false</externalId>
        <label>Contact ID - New</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Speaker</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Dreamforce_Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Dreamforce_Contact__r.Account.Name</formula>
        <label>Dreamforce Account Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Dreamforce_Contact_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Dreamforce_Contact__r.FirstName &amp; &quot; &quot; &amp; Dreamforce_Contact__r.LastName</formula>
        <label>Dreamforce Contact Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Dreamforce_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Registered Contact</inlineHelpText>
        <label>Dreamforce Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Speakers (Dreamforce Contact)</relationshipLabel>
        <relationshipName>Speakers</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Insert_Flag__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Insert Flag</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Ready_to_Publish__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Ready to Publish</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Session__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Session</label>
        <referenceTo>Session__c</referenceTo>
        <relationshipName>Speakers</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Speaker_Rating__c</fullName>
        <externalId>false</externalId>
        <label>Speaker Rating</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Speaker_Status__c</fullName>
        <externalId>false</externalId>
        <label>Speaker Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Self-submitted</fullName>
                    <default>false</default>
                    <label>Self-submitted</label>
                </value>
                <value>
                    <fullName>Nominated</fullName>
                    <default>false</default>
                    <label>Nominated</label>
                </value>
                <value>
                    <fullName>Not a good fit</fullName>
                    <default>false</default>
                    <label>Not a good fit</label>
                </value>
                <value>
                    <fullName>Request - sent</fullName>
                    <default>false</default>
                    <label>Request - sent</label>
                </value>
                <value>
                    <fullName>Request - tentative</fullName>
                    <default>false</default>
                    <label>Request - tentative</label>
                </value>
                <value>
                    <fullName>Request - accepted</fullName>
                    <default>false</default>
                    <label>Request - accepted</label>
                </value>
                <value>
                    <fullName>Request - declined</fullName>
                    <default>false</default>
                    <label>Request - declined</label>
                </value>
                <value>
                    <fullName>No comp pass</fullName>
                    <default>false</default>
                    <label>No comp pass</label>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                    <label>Cancelled</label>
                </value>
                <value>
                    <fullName>Confirmation sent</fullName>
                    <default>false</default>
                    <label>Confirmation sent</label>
                </value>
                <value>
                    <fullName>Speaker Approved</fullName>
                    <default>false</default>
                    <label>Speaker Approved</label>
                </value>
                <value>
                    <fullName>Duplicate</fullName>
                    <default>false</default>
                    <label>Duplicate</label>
                </value>
                <value>
                    <fullName>Ready to Publish</fullName>
                    <default>false</default>
                    <label>Ready to Publish</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Topic_Expertise__c</fullName>
        <externalId>false</externalId>
        <label>Topic Expertise</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>ROI</fullName>
                    <default>false</default>
                    <label>ROI</label>
                </value>
                <value>
                    <fullName>PRM</fullName>
                    <default>false</default>
                    <label>PRM</label>
                </value>
                <value>
                    <fullName>Self-service portal</fullName>
                    <default>false</default>
                    <label>Self-service portal</label>
                </value>
                <value>
                    <fullName>Analytics/Dashboards</fullName>
                    <default>false</default>
                    <label>Analytics/Dashboards</label>
                </value>
                <value>
                    <fullName>Demand generation</fullName>
                    <default>false</default>
                    <label>Demand generation</label>
                </value>
                <value>
                    <fullName>Campaigns</fullName>
                    <default>false</default>
                    <label>Campaigns</label>
                </value>
                <value>
                    <fullName>Leads</fullName>
                    <default>false</default>
                    <label>Leads</label>
                </value>
                <value>
                    <fullName>Search engine marketing</fullName>
                    <default>false</default>
                    <label>Search engine marketing</label>
                </value>
                <value>
                    <fullName>Email marketing</fullName>
                    <default>false</default>
                    <label>Email marketing</label>
                </value>
                <value>
                    <fullName>Sales Operations</fullName>
                    <default>false</default>
                    <label>Sales Operations</label>
                </value>
                <value>
                    <fullName>Pipeline analysis</fullName>
                    <default>false</default>
                    <label>Pipeline analysis</label>
                </value>
                <value>
                    <fullName>AppExchange</fullName>
                    <default>false</default>
                    <label>AppExchange</label>
                </value>
                <value>
                    <fullName>Apex Code</fullName>
                    <default>false</default>
                    <label>Apex Code</label>
                </value>
                <value>
                    <fullName>Integration</fullName>
                    <default>false</default>
                    <label>Integration</label>
                </value>
                <value>
                    <fullName>Data quality</fullName>
                    <default>false</default>
                    <label>Data quality</label>
                </value>
                <value>
                    <fullName>Implementation</fullName>
                    <default>false</default>
                    <label>Implementation</label>
                </value>
                <value>
                    <fullName>Global deployment</fullName>
                    <default>false</default>
                    <label>Global deployment</label>
                </value>
                <value>
                    <fullName>SOA</fullName>
                    <default>false</default>
                    <label>SOA</label>
                </value>
                <value>
                    <fullName>Community</fullName>
                    <default>false</default>
                    <label>Community</label>
                </value>
                <value>
                    <fullName>Mobile</fullName>
                    <default>false</default>
                    <label>Mobile</label>
                </value>
                <value>
                    <fullName>Workflow</fullName>
                    <default>false</default>
                    <label>Workflow</label>
                </value>
                <value>
                    <fullName>KYC</fullName>
                    <default>false</default>
                    <label>KYC</label>
                </value>
                <value>
                    <fullName>Wallet share</fullName>
                    <default>false</default>
                    <label>Wallet share</label>
                </value>
                <value>
                    <fullName>Capital Markets</fullName>
                    <default>false</default>
                    <label>Capital Markets</label>
                </value>
                <value>
                    <fullName>SOX</fullName>
                    <default>false</default>
                    <label>SOX</label>
                </value>
                <value>
                    <fullName>Nonprofits</fullName>
                    <default>false</default>
                    <label>Nonprofits</label>
                </value>
                <value>
                    <fullName>Territory management</fullName>
                    <default>false</default>
                    <label>Territory management</label>
                </value>
                <value>
                    <fullName>Training</fullName>
                    <default>false</default>
                    <label>Training</label>
                </value>
                <value>
                    <fullName>S-controls</fullName>
                    <default>false</default>
                    <label>S-controls</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>X62_Org_Accnt_Id__c</fullName>
        <externalId>false</externalId>
        <label>62 Org Accnt Id</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>X62_Org_Contact_Id__c</fullName>
        <externalId>false</externalId>
        <label>62 Org Contact Id</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>X62_Org_Session_Id__c</fullName>
        <externalId>false</externalId>
        <label>62 Org Session Id</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Speaker</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Session__c</columns>
        <columns>Clean_Account_Name__c</columns>
        <columns>Clean_Contact_Name__c</columns>
        <columns>Is_Ready_to_Publish__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>SP-{0000}</displayFormat>
        <label>Speaker ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Speakers</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Session__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Session__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Speaker_Status__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
