<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>SBQQ__PriceConditionHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Condition that fires the parent price rule.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>SBQQ__Field__c</fullName>
        <deprecated>false</deprecated>
        <description>Field on Quote Line or Product Option object that is evaluated by this condition.</description>
        <externalId>false</externalId>
        <inlineHelpText>Choose the field that should contain the value you enter in the Value field.</inlineHelpText>
        <label>Field</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Product Code</fullName>
                    <default>false</default>
                    <label>Product Code</label>
                </value>
                <value>
                    <fullName>SBQQ__BillingCountry__c</fullName>
                    <default>false</default>
                    <label>SBQQ__BillingCountry__c</label>
                </value>
                <value>
                    <fullName>Bill_To_Country__c</fullName>
                    <default>false</default>
                    <label>Bill_To_Country__c</label>
                </value>
                <value>
                    <fullName>Revenue_Type__c</fullName>
                    <default>false</default>
                    <label>Revenue_Type__c</label>
                </value>
                <value>
                    <fullName>SBQQ__Primary__c</fullName>
                    <default>false</default>
                    <label>SBQQ__Primary__c</label>
                </value>
                <value>
                    <fullName>Partner_Discount__c</fullName>
                    <default>false</default>
                    <label>Partner_Discount__c</label>
                </value>
                <value>
                    <fullName>SBQQ__PartnerDiscount__c</fullName>
                    <default>false</default>
                    <label>SBQQ__PartnerDiscount__c</label>
                </value>
                <value>
                    <fullName>Distributor_Discount__c</fullName>
                    <default>false</default>
                    <label>Distributor_Discount__c</label>
                </value>
                <value>
                    <fullName>Maintenance_Level__c</fullName>
                    <default>false</default>
                    <label>Maintenance_Level__c</label>
                </value>
                <value>
                    <fullName>Maintenance_Percentage__c</fullName>
                    <default>false</default>
                    <label>Maintenance_Percentage__c</label>
                </value>
                <value>
                    <fullName>SBQQ__ProductFamily__c</fullName>
                    <default>false</default>
                    <label>SBQQ__ProductFamily__c</label>
                </value>
                <value>
                    <fullName>SBQQ__ProductName__c</fullName>
                    <default>false</default>
                    <label>SBQQ__ProductName__c</label>
                </value>
                <value>
                    <fullName>Evaluation_Expiry_Date__c</fullName>
                    <default>false</default>
                    <label>Evaluation_Expiry_Date__c</label>
                </value>
                <value>
                    <fullName>One_Time_Partner_Margin__c</fullName>
                    <default>false</default>
                    <label>One_Time_Partner_Margin__c</label>
                </value>
                <value>
                    <fullName>Name</fullName>
                    <default>false</default>
                    <label>Name</label>
                </value>
                <value>
                    <fullName>SBQQ__MasterContract__c</fullName>
                    <default>false</default>
                    <label>SBQQ__MasterContract__c</label>
                </value>
                <value>
                    <fullName>OEM_Item_Type__c</fullName>
                    <default>false</default>
                    <label>OEM_Item_Type__c</label>
                </value>
                <value>
                    <fullName>SE_OEM_Development__c</fullName>
                    <default>false</default>
                    <label>SE_OEM_Development__c</label>
                </value>
                <value>
                    <fullName>Minimum_Order_Qty_Override__c</fullName>
                    <default>false</default>
                    <label>Minimum_Order_Qty_Override__c</label>
                </value>
                <value>
                    <fullName>Use_Price_Level__c</fullName>
                    <default>false</default>
                    <label>Use_Price_Level__c</label>
                </value>
                <value>
                    <fullName>SBQQ__SpecialPrice__c</fullName>
                    <default>false</default>
                    <label>SBQQ__SpecialPrice__c</label>
                </value>
                <value>
                    <fullName>SBQQ__NetPrice__c</fullName>
                    <default>false</default>
                    <label>SBQQ__NetPrice__c</label>
                </value>
                <value>
                    <fullName>SBQQ__SpecialPriceType__c</fullName>
                    <default>false</default>
                    <label>SBQQ__SpecialPriceType__c</label>
                </value>
                <value>
                    <fullName>Support_provided_by_Qlik__c</fullName>
                    <default>false</default>
                    <label>Support_provided_by_Qlik__c</label>
                </value>
                <value>
                    <fullName>License_Discount_user_input__c</fullName>
                    <default>false</default>
                    <label>License_Discount_user_input__c</label>
                </value>
                <value>
                    <fullName>Training_Discount_user_input__c</fullName>
                    <default>false</default>
                    <label>Training_Discount_user_input__c</label>
                </value>
                <value>
                    <fullName>Consulting_Discount_user_input__c</fullName>
                    <default>false</default>
                    <label>Consulting_Discount_user_input__c</label>
                </value>
                <value>
                    <fullName>Support_Services_Discount_user_input__c</fullName>
                    <default>false</default>
                    <label>Support_Services_Discount_user_input__c</label>
                </value>
                <value>
                    <fullName>SBQQ__Discount__c</fullName>
                    <default>false</default>
                    <label>SBQQ__Discount__c</label>
                </value>
                <value>
                    <fullName>Partner_Discount_on_Maintenance__c</fullName>
                    <default>false</default>
                    <label>Partner_Discount_on_Maintenance__c</label>
                </value>
                <value>
                    <fullName>SBQQ__NonPartnerDiscountable__c</fullName>
                    <default>false</default>
                    <label>SBQQ__NonPartnerDiscountable__c</label>
                </value>
                <value>
                    <fullName>SBQQ__AdditionalDiscountAmount__c</fullName>
                    <default>false</default>
                    <label>SBQQ__AdditionalDiscountAmount__c</label>
                </value>
                <value>
                    <fullName>Product_Code__c</fullName>
                    <default>false</default>
                    <label>Product_Code__c</label>
                </value>
                <value>
                    <fullName>CurrencyIsoCode</fullName>
                    <default>false</default>
                    <label>CurrencyIsoCode</label>
                </value>
                <value>
                    <fullName>List_Unit_Price_JPY__c</fullName>
                    <default>false</default>
                    <label>List_Unit_Price_JPY__c</label>
                </value>
                <value>
                    <fullName>SBQQ__PriceEditable__c</fullName>
                    <default>false</default>
                    <label>SBQQ__PriceEditable__c</label>
                </value>
                <value>
                    <fullName>Generate_License__c</fullName>
                    <default>false</default>
                    <label>Generate_License__c</label>
                </value>
                <value>
                    <fullName>Record_Type_Text__c</fullName>
                    <default>false</default>
                    <label>Record_Type_Text__c</label>
                </value>
                <value>
                    <fullName>SBQQ__NonDiscountable__c</fullName>
                    <default>false</default>
                    <label>SBQQ__NonDiscountable__c</label>
                </value>
                <value>
                    <fullName>Old_Education_Discount_Value__c</fullName>
                    <default>false</default>
                    <label>Old_Education_Discount_Value__c</label>
                </value>
                <value>
                    <fullName>Old_Consulting_Discount_Value__c</fullName>
                    <default>false</default>
                    <label>Old_Consulting_Discount_Value__c</label>
                </value>
                <value>
                    <fullName>Old_License_Discount_Value__c</fullName>
                    <default>false</default>
                    <label>Old_License_Discount_Value__c</label>
                </value>
                <value>
                    <fullName>Resell_SPD_Discount__c</fullName>
                    <default>false</default>
                    <label>Resell_SPD_Discount__c</label>
                </value>
                <value>
                    <fullName>SPD_Eligibility__c</fullName>
                    <default>false</default>
                    <label>SPD_Eligibility__c</label>
                </value>
                <value>
                    <fullName>Program_Version__c</fullName>
                    <default>false</default>
                    <label>Program_Version__c</label>
                </value>
                <value>
                    <fullName>Partner_Level__c</fullName>
                    <default>false</default>
                    <label>Partner_Level__c</label>
                </value>
                <value>
                    <fullName>License_List_Amount_USD__c</fullName>
                    <default>false</default>
                    <label>License_List_Amount_USD__c</label>
                </value>
                <value>
                    <fullName>Partner_Category__c</fullName>
                    <default>false</default>
                    <label>Partner_Category__c</label>
                </value>
                <value>
                    <fullName>Is_Perpetual__c</fullName>
                    <default>false</default>
                    <label>Is_Perpetual__c</label>
                </value>
                <value>
                    <fullName>SBQQ__Type__c</fullName>
                    <default>false</default>
                    <label>SBQQ__Type__c</label>
                </value>
                <value>
                    <fullName>Segment__c</fullName>
                    <default>false</default>
                    <label>Segment__c</label>
                </value>
                <value>
                    <fullName>Opportunity_Type__c</fullName>
                    <default>false</default>
                    <label>Opportunity_Type__c</label>
                </value>
                <value>
                    <fullName>New_Logo_Incentive__c</fullName>
                    <default>false</default>
                    <label>New_Logo_Incentive__c</label>
                </value>
                <value>
                    <fullName>Is_NLI_Applicable__c</fullName>
                    <default>false</default>
                    <label>Is_NLI_Applicable__c</label>
                </value>
                <value>
                    <fullName>Second_Partner__c</fullName>
                    <default>false</default>
                    <label>Second_Partner__c</label>
                </value>
                <value>
                    <fullName>MR_Channel_Incentive__c</fullName>
                    <default>false</default>
                    <label>MR_Channel_Incentive__c</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__FilterFormula__c</fullName>
        <deprecated>false</deprecated>
        <description>You can construct a formula using many of Salesforce&apos;s available operators and functions. Refer to Salesforce CPQ documentation for the full list. You can also add in information from the quote or the target object field (i.e., SBQQ__QuoteLine__c.SBQQ__ListPrice__c).</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter a formula with operators, functions, and available fields on quote and target object. Not supported on legacy calculator.</inlineHelpText>
        <label>Filter Formula</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SBQQ__FilterType__c</fullName>
        <deprecated>false</deprecated>
        <description>Type of filtering used in this condition.</description>
        <externalId>false</externalId>
        <inlineHelpText>Choose Variable to evaluate this condition against a Summary Variable; otherwise, choose Value.</inlineHelpText>
        <label>Filter Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Value</fullName>
                    <default>false</default>
                    <label>Value</label>
                </value>
                <value>
                    <fullName>Variable</fullName>
                    <default>false</default>
                    <label>Variable</label>
                </value>
                <value>
                    <fullName>Formula</fullName>
                    <default>false</default>
                    <label>Formula</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__FilterVariable__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Summary variable that filters this condition.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the Summary Variable you want this condition to use as a filter.</inlineHelpText>
        <label>Filter Variable</label>
        <referenceTo>SBQQ__SummaryVariable__c</referenceTo>
        <relationshipLabel>Filtered Price Conditions</relationshipLabel>
        <relationshipName>FilteredPriceConditions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SBQQ__Index__c</fullName>
        <deprecated>false</deprecated>
        <description>Assign an unique number to this Condition to reference it in the Price Rule&apos;s Advanced Condition.</description>
        <externalId>false</externalId>
        <inlineHelpText>Assign an unique number to this Condition to reference it in the Price Rule&apos;s Advanced Condition.</inlineHelpText>
        <label>Index</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SBQQ__Object__c</fullName>
        <deprecated>false</deprecated>
        <description>Object against which the condition is evaluated.</description>
        <externalId>false</externalId>
        <inlineHelpText>Object against which the condition is evaluated.</inlineHelpText>
        <label>Object</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Quote</fullName>
                    <default>false</default>
                    <label>Quote</label>
                </value>
                <value>
                    <fullName>Quote Line</fullName>
                    <default>false</default>
                    <label>Quote Line</label>
                </value>
                <value>
                    <fullName>Product Option</fullName>
                    <default>false</default>
                    <label>Product Option</label>
                </value>
                <value>
                    <fullName>Summary Variable</fullName>
                    <default>false</default>
                    <label>Summary Variable</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__Operator__c</fullName>
        <deprecated>false</deprecated>
        <description>The operator that is applied when comparing the Field with the Value.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the operator to apply when comparing the Field with the Value.</inlineHelpText>
        <label>Operator</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>equals</fullName>
                    <default>false</default>
                    <label>equals</label>
                </value>
                <value>
                    <fullName>not equals</fullName>
                    <default>false</default>
                    <label>not equals</label>
                </value>
                <value>
                    <fullName>less than</fullName>
                    <default>false</default>
                    <label>less than</label>
                </value>
                <value>
                    <fullName>less or equals</fullName>
                    <default>false</default>
                    <label>less or equals</label>
                </value>
                <value>
                    <fullName>greater than</fullName>
                    <default>false</default>
                    <label>greater than</label>
                </value>
                <value>
                    <fullName>greater or equals</fullName>
                    <default>false</default>
                    <label>greater or equals</label>
                </value>
                <value>
                    <fullName>starts with</fullName>
                    <default>false</default>
                    <label>starts with</label>
                </value>
                <value>
                    <fullName>ends with</fullName>
                    <default>false</default>
                    <label>ends with</label>
                </value>
                <value>
                    <fullName>contains</fullName>
                    <default>false</default>
                    <label>contains</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__ParentRuleIsActive__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>SBQQ__Rule__r.SBQQ__Active__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ParentRuleIsActive</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SBQQ__RuleTargetsCalculator__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>ISPICKVAL(SBQQ__Rule__r.SBQQ__TargetObject__c, &apos;Calculator&apos;) || ISPICKVAL(SBQQ__Rule__r.SBQQ__TargetObject__c, &apos;Quote Line&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>RuleTargetsCalculator</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SBQQ__Rule__c</fullName>
        <deprecated>false</deprecated>
        <description>Price rule running this condition.</description>
        <externalId>false</externalId>
        <inlineHelpText>Price rule running this condition.</inlineHelpText>
        <label>Price Rule</label>
        <referenceTo>SBQQ__PriceRule__c</referenceTo>
        <relationshipLabel>Price Conditions</relationshipLabel>
        <relationshipName>PriceConditions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SBQQ__TestedFormula__c</fullName>
        <deprecated>false</deprecated>
        <description>You can construct a formula using many of Salesforce&apos;s available operators and functions. Refer to Salesforce CPQ documentation for the full list. You can also add in information from the quote or the target object field (i.e., SBQQ__QuoteLine__c.SBQQ__ListPrice__c).</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter a formula with operators, functions, and available fields on quote and target object. Not supported on legacy calculator.</inlineHelpText>
        <label>Tested Formula</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SBQQ__TestedVariable__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Summary variable tested when evaluating this price condition.</description>
        <externalId>false</externalId>
        <inlineHelpText>Summary variable tested by this price condition.</inlineHelpText>
        <label>Tested Variable</label>
        <referenceTo>SBQQ__SummaryVariable__c</referenceTo>
        <relationshipLabel>Price Conditions (Tested Variable)</relationshipLabel>
        <relationshipName>TestedPriceConditions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SBQQ__Value__c</fullName>
        <deprecated>false</deprecated>
        <description>Value to evaluate against.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the value you want to match in the field you selected using the operator chosen.</inlineHelpText>
        <label>Filter Value</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Price Condition</label>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Condition #</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Price Conditions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>SBQQ__Filter_formula_field_null</fullName>
        <active>true</active>
        <description>Require Filter Formula when Filter Type is Formula.</description>
        <errorConditionFormula>AND(ISPICKVAL(SBQQ__FilterType__c, &quot;Formula&quot;), ISBLANK(SBQQ__FilterFormula__c))</errorConditionFormula>
        <errorMessage>If Filter Type is Formula, then Filter Formula cannot be empty.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>SBQQ__Tested_field_or_variable</fullName>
        <active>true</active>
        <description>Require either tested field or variable.</description>
        <errorConditionFormula>AND(ISBLANK(TEXT(SBQQ__Field__c)),ISBLANK(SBQQ__TestedVariable__c),ISBLANK(SBQQ__TestedFormula__c))</errorConditionFormula>
        <errorMessage>You must specify either field or tested variable.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
