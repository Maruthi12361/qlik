<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>ContactId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>ULC Details</relationshipLabel>
        <relationshipName>ULC_Details</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>LeadId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Lead</label>
        <referenceTo>Lead</referenceTo>
        <relationshipLabel>ULC Details</relationshipLabel>
        <relationshipName>ULC_Details</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>oUser__c</fullName>
        <externalId>false</externalId>
        <label>oUser</label>
        <length>3000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>	
    <fields>
        <fullName>Toggle_dirty__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Toggle_dirty</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ULCEntityId__c</fullName>
        <externalId>false</externalId>
        <formula>if (
    NOT(ISBlank( ContactId__c )),
     ContactId__c ,
     LeadId__c 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULCEntityId</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULCName__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>ULC Username</label>
        <length>80</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>ULCStatus__c</fullName>
        <externalId>false</externalId>
        <label>ULC Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Awaiting Activation</fullName>
                    <default>false</default>
                    <label>Awaiting Activation</label>
                </value>
                <value>
                    <fullName>Disabled</fullName>
                    <default>false</default>
                    <label>Disabled</label>
                </value>
                <value>
                    <fullName>Disable Temporarily</fullName>
                    <default>false</default>
                    <label>Disable Temporarily</label>
                </value>
                <value>
                    <fullName>Resend Password</fullName>
                    <default>false</default>
                    <label>Resend Password</label>
                </value>
                <value>
                    <fullName>Reset Password</fullName>
                    <default>false</default>
                    <label>Reset Password</label>
                </value>
                <value>
                    <fullName>Send Details</fullName>
                    <default>false</default>
                    <label>Send Details</label>
                </value>
                <value>
                    <fullName>Send Download Template</fullName>
                    <default>false</default>
                    <label>Send Download Template</label>
                </value>
                <value>
                    <fullName>Upsert SumTotal User</fullName>
                    <default>false</default>
                    <label>Upsert SumTotal User</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ULC_DataInjection__c</fullName>
        <externalId>false</externalId>
        <label>ULC_DataInjection</label>
        <length>131072</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>ULC_Force_Password_Reset__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>ULC Force Password Reset</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ULC_PartnerSourceNo__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.PartnerSourceNo__c,ContactId__r.PartnerSourceNo__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_PartnerSourceNo</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_Password__c</fullName>
        <externalId>false</externalId>
        <label>ULC Password</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_PortalUserActive__c</fullName>
        <externalId>false</externalId>
        <formula>ContactId__r.Portal_User_Active__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_PortalUserActive</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ULC_Problem_Description__c</fullName>
        <externalId>false</externalId>
        <label>ULC Problem Description</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>ULC_Problem_Type__c</fullName>
        <externalId>false</externalId>
        <label>ULC Problem Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>None</fullName>
                    <default>true</default>
                    <label>None</label>
                </value>
                <value>
                    <fullName>Cannot login</fullName>
                    <default>false</default>
                    <label>Cannot login</label>
                </value>
                <value>
                    <fullName>Send to Account Manager</fullName>
                    <default>false</default>
                    <label>Send to Account Manager</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ULC_QCloudID__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.QCloudID__c,ContactId__r.QCloudID__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_QCloudID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_accountid__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,null,ContactId__r.AccountId)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_accountid</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_city__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.City,ContactId__r.MailingCity)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_city</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_country__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.Country,ContactId__r.MailingCountry)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_country</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_countrycode__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.Country_Code__r.Name,ContactId__r.Country_Code__r.Name)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_countrycode</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_email__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.Email,ContactId__r.Email)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_firstname__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.FirstName,ContactId__r.FirstName)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_firstname</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_lastname__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.LastName,ContactId__r.LastName)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_lastname</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_levels__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,&quot;Base&quot;,ContactId__r.ULCLevel_calculated__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_levels</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_phone__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.Phone,ContactId__r.Phone)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_phone</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_postalcode__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.PostalCode,ContactId__r.MailingPostalCode)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_postalcode</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_state__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.State,ContactId__r.MailingState)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_state</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ULC_street__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ContactId__c == null,LeadId__r.Street,ContactId__r.MailingStreet)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ULC_street</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>qlikid__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>qlikid</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>ULC Detail</label>
    <nameField>
        <displayFormat>ULCD-{00000000}</displayFormat>
        <label>ULC Detail Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>ULC Details</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>ULCName__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ULCStatus__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>XO70_ULCUsername</fullName>
        <active>true</active>
        <errorConditionFormula>NOT(
 REGEX( ULCName__c, &quot;[a-z0-9+@_\\-\\.]*&quot;) 
)</errorConditionFormula>
        <errorMessage>Invalid username. Please make sure that the username contains only lower case characters (a-z), numeric values (0-9), ( _ ),  ( – ) or ( . ).</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
