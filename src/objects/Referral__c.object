<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Records for potential opportunities identified by a Qlik Partner and accepted by a Qlik Salesperson (who will associate to an Opportunity record)</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Accepted_Terms_and_Conditions__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Confirms that the partner has accepted the Terms and Conditions</description>
        <externalId>false</externalId>
        <inlineHelpText>The Terms &amp; Conditions are available to view on the Home page and also from a referral record after creation</inlineHelpText>
        <label>Accepted Terms &amp; Conditions</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Close_Date__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.CloseDate</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Expected Close Date of the Opportunity</inlineHelpText>
        <label>Close Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>End_User_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>End User Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Referrals</relationshipLabel>
        <relationshipName>Referrals</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>End_User_Contact_Email__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Populate the work email address of the Contact person at the End User Account</inlineHelpText>
        <label>End User Contact Email</label>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_User_Contact_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Populate the Contact person at the End User Account</inlineHelpText>
        <label>End User Contact Name</label>
        <length>50</length>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_User_Contact_Phone__c</fullName>
        <externalId>false</externalId>
        <label>End User Contact Phone</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Influence_Criteria_Comment__c</fullName>
        <description>Influence Criteria Comment</description>
        <externalId>false</externalId>
        <label>Influence Criteria Comment</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Met_Influence_Criteria__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Did the Partner meet Influence Criteria</description>
        <externalId>false</externalId>
        <label>Met Influence Criteria</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Non_Standard_Partner_Fee__c</fullName>
        <description>Non Standard Partner Fee for a referral associated to an opportunity</description>
        <externalId>false</externalId>
        <label>Non Standard Partner Fee %</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Currency__c</fullName>
        <externalId>false</externalId>
        <formula>Text(Opportunity__r.CurrencyIsoCode )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Opportunity Currency</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Stage__c</fullName>
        <description>Populates with the Stage of the associated Opportunity</description>
        <externalId>false</externalId>
        <formula>TEXT(Opportunity__r.StageName)</formula>
        <inlineHelpText>Populates with the Stage of the associated Opportunity</inlineHelpText>
        <label>Opportunity Stage</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Populated by Qlik PSM/PAM upon accepting the referral</description>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Referrals</relationshipLabel>
        <relationshipName>Referrals</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Partner_Fee_PAM__c</fullName>
        <description>Partner Fee PAM</description>
        <externalId>false</externalId>
        <label>Partner Fee PAM</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Partner_Fee_Type__c</fullName>
        <defaultValue>&quot;Finder&apos;s Fee&quot;</defaultValue>
        <description>Partner Fee Type</description>
        <externalId>false</externalId>
        <label>Partner Fee Type</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Influence Fee</fullName>
                    <default>false</default>
                    <label>Influence Fee</label>
                </value>
                <value>
                    <fullName>Finder&apos;s Fee</fullName>
                    <default>false</default>
                    <label>Finder&apos;s Fee</label>
                </value>
                <value>
                    <fullName>Referral Fee</fullName>
                    <default>false</default>
                    <label>Referral Fee</label>
                </value>
                <value>
                    <fullName>Deal Protection</fullName>
                    <default>false</default>
                    <label>Deal Protection</label>
                </value>
                <value>
                    <fullName>Finder’s Fee</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Finder’s Fee</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Partner_Fee_Value__c</fullName>
        <description>Value from the Associated Opportunity</description>
        <externalId>false</externalId>
        <formula>(Partner_Fee__c *  (( Opportunity__r.License_Forecast_Amount__c  +  Opportunity__r.Subscription_Forecast_ACV_Amount__c )/100))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Partner Fee Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Partner_Fee__c</fullName>
        <externalId>false</externalId>
        <formula>IF(  ISBLANK(  Non_Standard_Partner_Fee__c ) , 

VALUE(Text(Standard_Partner_Fee__c)) ,Non_Standard_Partner_Fee__c)</formula>
        <label>Partner Fee %</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Partner_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Partner Manager as determined by the Partner Account who is submitting</description>
        <externalId>false</externalId>
        <label>Partner Manager</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Referrals</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Referral_Form_Uploaded__c</fullName>
        <description>determine whether a Referral Form has been Uploaded</description>
        <externalId>false</externalId>
        <label>Referral Form Uploaded</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Not Uploaded</fullName>
                    <default>false</default>
                    <label>Not Uploaded</label>
                </value>
                <value>
                    <fullName>Uploaded</fullName>
                    <default>false</default>
                    <label>Uploaded</label>
                </value>
                <value>
                    <fullName>In Process</fullName>
                    <default>false</default>
                    <label>In Process</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Referral_Status__c</fullName>
        <externalId>false</externalId>
        <label>Referral Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>false</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Accepted</fullName>
                    <default>false</default>
                    <label>Accepted</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Draft</fullName>
                    <default>true</default>
                    <label>Draft</label>
                </value>
                <value>
                    <fullName>Invoice for Payment</fullName>
                    <default>false</default>
                    <label>Invoice for Payment</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Referring_Partner_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Partner account the user who did the referral belongs to</description>
        <externalId>false</externalId>
        <inlineHelpText>Partner account the user who did the referral belongs to</inlineHelpText>
        <label>Referring Partner Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Referrals (Referring Partner Account)</relationshipLabel>
        <relationshipName>Referrals1</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Rejection_Reason__c</fullName>
        <externalId>false</externalId>
        <label>Rejection Reason</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Referral_Status__c</controllingField>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Existing Opportunity</fullName>
                    <default>false</default>
                    <label>Existing Opportunity</label>
                </value>
                <value>
                    <fullName>Goal Not Confirmed</fullName>
                    <default>false</default>
                    <label>Goal Not Confirmed</label>
                </value>
                <value>
                    <fullName>Example 1</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Example 1</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Pending</controllingFieldValue>
                <controllingFieldValue>Rejected</controllingFieldValue>
                <valueName>Existing Opportunity</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pending</controllingFieldValue>
                <controllingFieldValue>Rejected</controllingFieldValue>
                <valueName>Goal Not Confirmed</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Related_Opportunity__c</fullName>
        <description>Read only field for Partner page layout to display the Opportunity name as Read Only</description>
        <externalId>false</externalId>
        <formula>Opportunity__r.Name</formula>
        <label>Related Opportunity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Standard_Partner_Fee__c</fullName>
        <defaultValue>&quot;5&quot;</defaultValue>
        <description>Standard Partner Fee %</description>
        <externalId>false</externalId>
        <label>Standard Partner Fee %</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>5</fullName>
                    <default>false</default>
                    <label>5</label>
                </value>
                <value>
                    <fullName>10</fullName>
                    <default>false</default>
                    <label>10</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Terms_Conditions__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;https://qlik.my.salesforce.com/sfc/p/20000000IGPX/a/3z000000DCkr/SmxUJNyFESUx23TaxDhsR5XzUbA.obL06iz.1FJnKXw &quot;,&quot;View Here&quot;,&quot;_blank&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Terms &amp; Conditions</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Won_Opportunity__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Marked as True if the associated Opportunity is Won</description>
        <externalId>false</externalId>
        <label>Won Opportunity</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Referral</label>
    <listViews>
        <fullName>Accepted_Referrals</fullName>
        <columns>NAME</columns>
        <columns>End_User_Account__c</columns>
        <columns>End_User_Contact_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Referral_Status__c</columns>
        <columns>Opportunity_Stage__c</columns>
        <columns>Partner_Fee__c</columns>
        <columns>Partner_Fee_Value__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Referral_Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </filters>
        <label>Qlik Commerce - Accepted Referrals</label>
    </listViews>
    <listViews>
        <fullName>Invoice_for_Payment_Referrals</fullName>
        <columns>NAME</columns>
        <columns>End_User_Account__c</columns>
        <columns>End_User_Contact_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Referral_Status__c</columns>
        <columns>Opportunity_Stage__c</columns>
        <columns>Partner_Fee__c</columns>
        <columns>Partner_Fee_Value__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Referral_Status__c</field>
            <operation>equals</operation>
            <value>Invoice for Payment</value>
        </filters>
        <label>Qlik Commerce - Invoice for Payment Ref</label>
    </listViews>
    <listViews>
        <fullName>Pending_Referrals</fullName>
        <columns>NAME</columns>
        <columns>End_User_Account__c</columns>
        <columns>End_User_Contact_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Referral_Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Referral_Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </filters>
        <label>Qlik Commerce - Pending Referrals</label>
    </listViews>
    <listViews>
        <fullName>Rejected_Referrals</fullName>
        <columns>NAME</columns>
        <columns>End_User_Account__c</columns>
        <columns>End_User_Contact_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Referral_Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Referral_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </filters>
        <label>Qlik Commerce - Rejected Referrals</label>
    </listViews>
    <nameField>
        <displayFormat>R-{0000}</displayFormat>
        <label>Referral Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Referrals</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>End_User_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>End_User_Contact_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Referring_Partner_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Referral_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Opportunity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Opportunity_Stage__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Partner_Fee__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Partner_Fee_Value__c</customTabListAdditionalFields>
        <excludedStandardButtons>New</excludedStandardButtons>
        <excludedStandardButtons>OpenListInQuip</excludedStandardButtons>
        <excludedStandardButtons>MassChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>End_User_Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>End_User_Contact_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Referring_Partner_Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Referral_Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Opportunity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Opportunity_Stage__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Partner_Fee__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Partner_Fee_Value__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>End_User_Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>End_User_Contact_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Referring_Partner_Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Referral_Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Opportunity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Opportunity_Stage__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Partner_Fee__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Partner_Fee_Value__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>End_User_Account__c</searchFilterFields>
        <searchFilterFields>Referral_Status__c</searchFilterFields>
        <searchFilterFields>Opportunity_Stage__c</searchFilterFields>
        <searchResultsAdditionalFields>End_User_Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>End_User_Contact_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Referring_Partner_Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Referral_Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Opportunity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Opportunity_Stage__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Partner_Fee__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Partner_Fee_Value__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>XORE001_Opportunity_needed_if_Approved</fullName>
        <active>true</active>
        <description>The Opportunity lookup field needs to be populated in order for the Referral to be Accepted</description>
        <errorConditionFormula>ISPICKVAL( Referral_Status__c , &quot;Accepted&quot;) &amp;&amp; ISBLANK( Opportunity__c )</errorConditionFormula>
        <errorDisplayField>Opportunity__c</errorDisplayField>
        <errorMessage>XORE001: The Opportunity needs to be specified in order to approve a Referral</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>XORE002_Terms_and_Conditions_must_be_acc</fullName>
        <active>true</active>
        <description>User must accept the Terms and Conditions in order to Create a Referral Record</description>
        <errorConditionFormula>Accepted_Terms_and_Conditions__c = FALSE</errorConditionFormula>
        <errorDisplayField>Accepted_Terms_and_Conditions__c</errorDisplayField>
        <errorMessage>XORE002: Terms &amp; Conditions must be accepted in order to create the record. The Terms may be reviewed on the next page before submittal</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>XORE003_Require_Referral_Fields_if_Acc</fullName>
        <active>true</active>
        <description>If the Referral has been Associated to an opportunity then Qlik needs to populate the Referral fields</description>
        <errorConditionFormula>NOT(ISBLANK(Opportunity__c))&amp;&amp;
OR(
ISBLANK( Partner_Fee_PAM__c ),
ISBLANK( TEXT(Partner_Fee_Type__c ))
)</errorConditionFormula>
        <errorMessage>XORE003: You must populate the Referral Fields when associating an opportunity</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>XORE004_Referral_Rejection_Reason_Req</fullName>
        <active>true</active>
        <description>Rejection Reason is required if the referral is rejected by Qlik</description>
        <errorConditionFormula>ISPICKVAL(Referral_Status__c , &quot;Rejected&quot;) &amp;&amp;
ISBLANK( TEXT(Rejection_Reason__c))</errorConditionFormula>
        <errorDisplayField>Rejection_Reason__c</errorDisplayField>
        <errorMessage>XORE004: Rejection Reason needs to be entered in order to Reject the Referral</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
