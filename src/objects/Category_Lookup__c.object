<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Area_Issue__c</fullName>
        <externalId>false</externalId>
        <formula>Name&amp;&apos;-&apos;&amp;Category_Level__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Area_Issue</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Article__c</fullName>
        <externalId>false</externalId>
        <label>Recommended Article</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Attachment_Upload_Prompt__c</fullName>
        <description>Expects the name of a Custom Label. The value of the custom label will be presented along side the file upload section of the Case Detail page, optionally on the Wizard itself.</description>
        <externalId>false</externalId>
        <label>Attachment Upload Prompt</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Case_Owner__c</fullName>
        <description>Stores the queue name who will be used as the default case owner</description>
        <externalId>false</externalId>
        <label>Case Owner (Queue)</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Category_1_Order__c</fullName>
        <externalId>false</externalId>
        <label>Category 1 Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Category_2_Order__c</fullName>
        <externalId>false</externalId>
        <label>Category 2 Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Category_Level_3__c</fullName>
        <externalId>false</externalId>
        <label>Category Level 3</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Category_Level__c</fullName>
        <externalId>false</externalId>
        <label>Category Level 2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Channel__c</fullName>
        <externalId>false</externalId>
        <label>Preferred Channel</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Phone</fullName>
                    <default>false</default>
                    <label>Phone</label>
                </value>
                <value>
                    <fullName>Chat</fullName>
                    <default>false</default>
                    <label>Chat</label>
                </value>
                <value>
                    <fullName>Cloud Digital River</fullName>
                    <default>false</default>
                    <label>Cloud Digital River</label>
                </value>
                <value>
                    <fullName>Cloud Qlik Community</fullName>
                    <default>false</default>
                    <label>Cloud Qlik Community</label>
                </value>
                <value>
                    <fullName>Attunity Portal</fullName>
                    <default>false</default>
                    <label>Attunity Portal</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Environment__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Environment</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Functional_Area__c</fullName>
        <externalId>false</externalId>
        <label>Functional Area</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Deployment</fullName>
                    <default>false</default>
                    <label>Deployment</label>
                </value>
                <value>
                    <fullName>Design</fullName>
                    <default>false</default>
                    <label>Design</label>
                </value>
                <value>
                    <fullName>Development</fullName>
                    <default>false</default>
                    <label>Development</label>
                </value>
                <value>
                    <fullName>Infrastructure</fullName>
                    <default>false</default>
                    <label>Infrastructure</label>
                </value>
                <value>
                    <fullName>Integrations</fullName>
                    <default>false</default>
                    <label>Integrations</label>
                </value>
                <value>
                    <fullName>Qoncierge</fullName>
                    <default>false</default>
                    <label>Qoncierge</label>
                </value>
                <value>
                    <fullName>Attunity</fullName>
                    <default>false</default>
                    <label>Attunity</label>
                </value>
                <value>
                    <fullName>QSCloud</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>QSCloud</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Hide_Login_Prompt__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Hide Login Prompt</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Question_Prompt_1__c</fullName>
        <description>Custom Label Name to be entered</description>
        <externalId>false</externalId>
        <label>Question Prompt 1</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Question_Prompt_2__c</fullName>
        <description>Custom Label Name to be specified</description>
        <externalId>false</externalId>
        <label>Question Prompt 2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Question_Prompt_3__c</fullName>
        <description>Custom Label Name to be specified</description>
        <externalId>false</externalId>
        <label>Question Prompt 3</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Question_Prompt_4__c</fullName>
        <externalId>false</externalId>
        <label>Question Prompt 4</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Question_Prompt_5__c</fullName>
        <externalId>false</externalId>
        <label>Question Prompt 5</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Record_Type_Name__c</fullName>
        <externalId>false</externalId>
        <label>Record Type Name</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Request_Type__c</fullName>
        <description>Service Request Type that will populate the same field on case</description>
        <externalId>false</externalId>
        <inlineHelpText>Service Request Type that will populate the same field on case</inlineHelpText>
        <label>Service Request Type</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Severity__c</fullName>
        <externalId>false</externalId>
        <label>Severity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                    <label>3</label>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                    <label>2</label>
                </value>
                <value>
                    <fullName>1</fullName>
                    <default>false</default>
                    <label>1</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Skill_set__c</fullName>
        <description>Skill set assignment used by Live Chat Support for routing purposes</description>
        <externalId>false</externalId>
        <inlineHelpText>Skill set assignment used by Live Chat Support for routing purposes</inlineHelpText>
        <label>Skill set</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Deployment</fullName>
                    <default>false</default>
                    <label>Deployment</label>
                </value>
                <value>
                    <fullName>Design</fullName>
                    <default>false</default>
                    <label>Design</label>
                </value>
                <value>
                    <fullName>Development</fullName>
                    <default>false</default>
                    <label>Development</label>
                </value>
                <value>
                    <fullName>Infrastructure</fullName>
                    <default>false</default>
                    <label>Infrastructure</label>
                </value>
                <value>
                    <fullName>All Customer Support</fullName>
                    <default>false</default>
                    <label>All Customer Support</label>
                </value>
                <value>
                    <fullName>All Product Support</fullName>
                    <default>false</default>
                    <label>All Product Support</label>
                </value>
                <value>
                    <fullName>All Support</fullName>
                    <default>false</default>
                    <label>All Support</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Unauthenticated__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Unauthenticated</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Category Lookup</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Category_Level__c</columns>
        <columns>Channel__c</columns>
        <columns>Skill_set__c</columns>
        <columns>Article__c</columns>
        <columns>Severity__c</columns>
        <columns>Case_Owner__c</columns>
        <columns>Functional_Area__c</columns>
        <columns>Environment__c</columns>
        <columns>Record_Type_Name__c</columns>
        <columns>Unauthenticated__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Category Level 1</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Category Lookup</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
