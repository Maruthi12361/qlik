<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>attached to change request to show which individual items have been changed</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>API_Name__c</fullName>
        <description>API Name of the component</description>
        <externalId>false</externalId>
        <label>API Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Action__c</fullName>
        <description>shows how a change component was changed</description>
        <externalId>false</externalId>
        <inlineHelpText>shows how a change component was changed</inlineHelpText>
        <label>Action</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Deleted</fullName>
                    <default>false</default>
                    <label>Deleted</label>
                </value>
                <value>
                    <fullName>Created</fullName>
                    <default>false</default>
                    <label>Created</label>
                </value>
                <value>
                    <fullName>Edited</fullName>
                    <default>false</default>
                    <label>Edited</label>
                </value>
                <value>
                    <fullName>Activated</fullName>
                    <default>false</default>
                    <label>Activated</label>
                </value>
                <value>
                    <fullName>Deactivated</fullName>
                    <default>false</default>
                    <label>Deactivated</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Added_to_change_set__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates if the Change Component was added to a change set.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check if component was added to a change set.</inlineHelpText>
        <label>Added to change set</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Applied_to__c</fullName>
        <description>Shows what the action was applied to</description>
        <externalId>false</externalId>
        <label>Applied to</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>APEX Class</fullName>
                    <default>false</default>
                    <label>APEX Class</label>
                </value>
                <value>
                    <fullName>APEX Code</fullName>
                    <default>false</default>
                    <label>APEX Code</label>
                </value>
                <value>
                    <fullName>APEX Trigger</fullName>
                    <default>false</default>
                    <label>APEX Trigger</label>
                </value>
                <value>
                    <fullName>Approval process</fullName>
                    <default>false</default>
                    <label>Approval process</label>
                </value>
                <value>
                    <fullName>Approval Recall Action</fullName>
                    <default>false</default>
                    <label>Approval Recall Action</label>
                </value>
                <value>
                    <fullName>Approval Rejection Action</fullName>
                    <default>false</default>
                    <label>Approval Rejection Action</label>
                </value>
                <value>
                    <fullName>Approval Step</fullName>
                    <default>false</default>
                    <label>Approval Step</label>
                </value>
                <value>
                    <fullName>Approval Step action</fullName>
                    <default>false</default>
                    <label>Approval Step action</label>
                </value>
                <value>
                    <fullName>Approval Submission Action</fullName>
                    <default>false</default>
                    <label>Approval Submission Action</label>
                </value>
                <value>
                    <fullName>Boomi process</fullName>
                    <default>false</default>
                    <label>Boomi process</label>
                </value>
                <value>
                    <fullName>Boomi schedules</fullName>
                    <default>false</default>
                    <label>Boomi schedules</label>
                </value>
                <value>
                    <fullName>Buttons and Links</fullName>
                    <default>false</default>
                    <label>Buttons and Links</label>
                </value>
                <value>
                    <fullName>Change set</fullName>
                    <default>false</default>
                    <label>Change set</label>
                </value>
                <value>
                    <fullName>Code</fullName>
                    <default>false</default>
                    <label>Code</label>
                </value>
                <value>
                    <fullName>Custom Label</fullName>
                    <default>false</default>
                    <label>Custom Label</label>
                </value>
                <value>
                    <fullName>Custom Setting</fullName>
                    <default>false</default>
                    <label>Custom Setting</label>
                </value>
                <value>
                    <fullName>Email Alert</fullName>
                    <default>false</default>
                    <label>Email Alert</label>
                </value>
                <value>
                    <fullName>Email template</fullName>
                    <default>false</default>
                    <label>Email template</label>
                </value>
                <value>
                    <fullName>Field</fullName>
                    <default>false</default>
                    <label>Field</label>
                </value>
                <value>
                    <fullName>Field Update</fullName>
                    <default>false</default>
                    <label>Field Update</label>
                </value>
                <value>
                    <fullName>Label Values</fullName>
                    <default>false</default>
                    <label>Label Values</label>
                </value>
                <value>
                    <fullName>NetSuite Bundle installation</fullName>
                    <default>false</default>
                    <label>NetSuite Bundle installation</label>
                </value>
                <value>
                    <fullName>NetSuite Custom Field</fullName>
                    <default>false</default>
                    <label>NetSuite Custom Field</label>
                </value>
                <value>
                    <fullName>NetSuite Custom Form</fullName>
                    <default>false</default>
                    <label>NetSuite Custom Form</label>
                </value>
                <value>
                    <fullName>NetSuite Custom List.</fullName>
                    <default>false</default>
                    <label>NetSuite Custom List.</label>
                </value>
                <value>
                    <fullName>NetSuite Custom Record</fullName>
                    <default>false</default>
                    <label>NetSuite Custom Record</label>
                </value>
                <value>
                    <fullName>NetSuite Saved Search/Report</fullName>
                    <default>false</default>
                    <label>NetSuite Saved Search/Report</label>
                </value>
                <value>
                    <fullName>NetSuite SuiteScript</fullName>
                    <default>false</default>
                    <label>NetSuite SuiteScript</label>
                </value>
                <value>
                    <fullName>NetSuite Workflow</fullName>
                    <default>false</default>
                    <label>NetSuite Workflow</label>
                </value>
                <value>
                    <fullName>Orbis Task</fullName>
                    <default>false</default>
                    <label>Orbis Task</label>
                </value>
                <value>
                    <fullName>Outbound message</fullName>
                    <default>false</default>
                    <label>Outbound message</label>
                </value>
                <value>
                    <fullName>Page Layout</fullName>
                    <default>false</default>
                    <label>Page Layout</label>
                </value>
                <value>
                    <fullName>Permission Set</fullName>
                    <default>false</default>
                    <label>Permission Set</label>
                </value>
                <value>
                    <fullName>Profile</fullName>
                    <default>false</default>
                    <label>Profile</label>
                </value>
                <value>
                    <fullName>Public Group</fullName>
                    <default>false</default>
                    <label>Public Group</label>
                </value>
                <value>
                    <fullName>Queue</fullName>
                    <default>false</default>
                    <label>Queue</label>
                </value>
                <value>
                    <fullName>Rapidi Schedule</fullName>
                    <default>false</default>
                    <label>Rapidi Schedule</label>
                </value>
                <value>
                    <fullName>Rapidi Transfer</fullName>
                    <default>false</default>
                    <label>Rapidi Transfer</label>
                </value>
                <value>
                    <fullName>Record Type</fullName>
                    <default>false</default>
                    <label>Record Type</label>
                </value>
                <value>
                    <fullName>ServiceNow Configuration</fullName>
                    <default>false</default>
                    <label>ServiceNow Configuration</label>
                </value>
                <value>
                    <fullName>Sharing Settings</fullName>
                    <default>false</default>
                    <label>Sharing Settings</label>
                </value>
                <value>
                    <fullName>Task</fullName>
                    <default>false</default>
                    <label>Task</label>
                </value>
                <value>
                    <fullName>Validation Rule</fullName>
                    <default>false</default>
                    <label>Validation Rule</label>
                </value>
                <value>
                    <fullName>VF Page</fullName>
                    <default>false</default>
                    <label>VF Page</label>
                </value>
                <value>
                    <fullName>VisualForce page</fullName>
                    <default>false</default>
                    <label>VisualForce page</label>
                </value>
                <value>
                    <fullName>Visual Workflow</fullName>
                    <default>false</default>
                    <label>Visual Workflow</label>
                </value>
                <value>
                    <fullName>Workflow action</fullName>
                    <default>false</default>
                    <label>Workflow action</label>
                </value>
                <value>
                    <fullName>Workflow Rule</fullName>
                    <default>false</default>
                    <label>Workflow Rule</label>
                </value>
                <value>
                    <fullName>Process Builder</fullName>
                    <default>false</default>
                    <label>Process Builder</label>
                </value>
                <value>
                    <fullName>Custom Metadata Type</fullName>
                    <default>false</default>
                    <label>Custom Metadata Type</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Change_Control__c</fullName>
        <externalId>false</externalId>
        <label>Change Control</label>
        <referenceTo>SLX__Change_Control__c</referenceTo>
        <relationshipLabel>Change Components</relationshipLabel>
        <relationshipName>Change_Components</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Description_of_function_other_info__c</fullName>
        <externalId>false</externalId>
        <label>Description of function/other info</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Name__c</fullName>
        <externalId>false</externalId>
        <label>Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>New_Value__c</fullName>
        <externalId>false</externalId>
        <label>New Value</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Object__c</fullName>
        <externalId>false</externalId>
        <label>Object</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Page_Layouts__c</fullName>
        <externalId>false</externalId>
        <label>Page Layouts</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Previous_Value__c</fullName>
        <externalId>false</externalId>
        <label>Previous Value</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Profiles__c</fullName>
        <externalId>false</externalId>
        <label>Profiles</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Reason_for_change__c</fullName>
        <externalId>false</externalId>
        <label>Reason for change (if appliacable)</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Screenshot_Further_Info__c</fullName>
        <externalId>false</externalId>
        <label>Screenshot/Further Info</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Visibility_Read_Only__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Which Profiles were given read only access?</inlineHelpText>
        <label>Visibility (Read Only)</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Visibility_read_write__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>which profiles has this been made visible to?</inlineHelpText>
        <label>Visibility (read write)</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <label>Change Component</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>CR_19391_BTN</fullName>
        <columns>Name__c</columns>
        <columns>Object__c</columns>
        <columns>API_Name__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Create Counters to Calculate Days in Waterfall Stage</value>
        </filters>
        <label>CR# 19391 BTN</label>
    </listViews>
    <listViews>
        <fullName>CR_22749_BTN</fullName>
        <columns>Name__c</columns>
        <columns>Object__c</columns>
        <columns>API_Name__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Create new Clicktools survey for Skills Assessment for Qlik Sense</value>
        </filters>
        <label>CR# 22749 BTN</label>
    </listViews>
    <listViews>
        <fullName>CR_23836_BTN</fullName>
        <columns>Name__c</columns>
        <columns>Object__c</columns>
        <columns>API_Name__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Lead Lifecycle-Clear FUR Fields</value>
        </filters>
        <label>CR# 23836 BTN</label>
    </listViews>
    <listViews>
        <fullName>cce_16194_Contact_Object</fullName>
        <columns>Name__c</columns>
        <columns>Object__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Update Lead Lifecycle Process</value>
        </filters>
        <filters>
            <field>Object__c</field>
            <operation>equals</operation>
            <value>Contact</value>
        </filters>
        <label>cce 16194 Contact Object</label>
    </listViews>
    <listViews>
        <fullName>cce_16194_Fields</fullName>
        <columns>Object__c</columns>
        <columns>API_Name__c</columns>
        <columns>Name__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Update Lead Lifecycle Process</value>
        </filters>
        <filters>
            <field>Applied_to__c</field>
            <operation>equals</operation>
            <value>Field</value>
        </filters>
        <filters>
            <field>Object__c</field>
            <operation>equals</operation>
            <value>Opportunity</value>
        </filters>
        <label>cce 16194 Fields</label>
    </listViews>
    <listViews>
        <fullName>cce_16194_RecordTypes</fullName>
        <columns>Name__c</columns>
        <columns>Object__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Update Lead Lifecycle Process</value>
        </filters>
        <filters>
            <field>Applied_to__c</field>
            <operation>equals</operation>
            <value>Record Type</value>
        </filters>
        <label>cce 16194 RecordTypes</label>
    </listViews>
    <listViews>
        <fullName>cce_16194_Validation</fullName>
        <columns>Name__c</columns>
        <columns>Object__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Update Lead Lifecycle Process</value>
        </filters>
        <filters>
            <field>Applied_to__c</field>
            <operation>equals</operation>
            <value>Validation Rule</value>
        </filters>
        <label>cce 16194 Validation</label>
    </listViews>
    <listViews>
        <fullName>cce_16194_Workflows</fullName>
        <columns>Object__c</columns>
        <columns>API_Name__c</columns>
        <columns>Name__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Update Lead Lifecycle Process</value>
        </filters>
        <filters>
            <field>Applied_to__c</field>
            <operation>equals</operation>
            <value>Field Update,Workflow action,Workflow Rule</value>
        </filters>
        <filters>
            <field>Object__c</field>
            <operation>equals</operation>
            <value>contact</value>
        </filters>
        <label>cce 16194 Workflows</label>
    </listViews>
    <listViews>
        <fullName>cce_16194_code_and_pages</fullName>
        <columns>API_Name__c</columns>
        <columns>Name__c</columns>
        <columns>Object__c</columns>
        <columns>Action__c</columns>
        <columns>Added_to_change_set__c</columns>
        <columns>Applied_to__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Change_Control__c</field>
            <operation>equals</operation>
            <value>Update Lead Lifecycle Process</value>
        </filters>
        <filters>
            <field>Applied_to__c</field>
            <operation>equals</operation>
            <value>APEX Class,APEX Code,APEX Trigger,Page Layout</value>
        </filters>
        <label>cce 16194 code and pages</label>
    </listViews>
    <nameField>
        <displayFormat>compon-{0000000}</displayFormat>
        <label>Change Components Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Change Components</pluralLabel>
    <recordTypes>
        <fullName>IS_Change_Component</fullName>
        <active>true</active>
        <description>USed for change components attached to ITsystems type CRs</description>
        <label>IS Change Component</label>
        <picklistValues>
            <picklist>Action__c</picklist>
            <values>
                <fullName>Activated</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Created</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Deactivated</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Deleted</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Edited</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Applied_to__c</picklist>
            <values>
                <fullName>APEX Class</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>APEX Code</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>APEX Trigger</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Approval Recall Action</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Approval Rejection Action</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Approval Step</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Approval Step action</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Approval Submission Action</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Approval process</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Boomi process</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Boomi schedules</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Buttons and Links</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Change set</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Code</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Custom Label</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Custom Metadata Type</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Custom Setting</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Email Alert</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Email template</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Field</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Field Update</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Label Values</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite Bundle installation</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite Custom Field</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite Custom Form</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite Custom List%2E</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite Custom Record</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite Saved Search%2FReport</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite SuiteScript</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NetSuite Workflow</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Orbis Task</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Outbound message</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Page Layout</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Permission Set</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Process Builder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Profile</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Public Group</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Queue</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Record Type</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>ServiceNow Configuration</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Sharing Settings</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Task</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>VF Page</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Validation Rule</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Visual Workflow</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>VisualForce page</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Workflow Rule</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Workflow action</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Action__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Applied_to__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Object__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Previous_Value__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>New_Value__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description_of_function_other_info__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Reason_for_change__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Action__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Applied_to__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Object__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Previous_Value__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>New_Value__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Reason_for_change__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
