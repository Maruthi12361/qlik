<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to hold NS Contracts</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Contract_End_Date__c</fullName>
        <externalId>false</externalId>
        <label>Support End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Contract_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Support Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>End_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>End User</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Support Contracts</relationshipLabel>
        <relationshipName>NS_Support_Contracts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Has_Active_Items__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Number_of_Active_Items__c &gt; 0, True, False)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Has Active Items</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>INT_NetSuite_InternalID__c</fullName>
        <externalId>true</externalId>
        <label>INT_NetSuite_InternalID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_Active_Items__c</fullName>
        <externalId>false</externalId>
        <label>Number of Active Items</label>
        <summaryFilterItems>
            <field>NS_Support_Contract_Item__c.State__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </summaryFilterItems>
        <summaryForeignKey>NS_Support_Contract_Item__c.NS_Support_Contract__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Renewal_Terms__c</fullName>
        <externalId>false</externalId>
        <label>Renewal Terms</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reseller__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Current Sell Through Partner</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Support Contracts (Current Sell Through Partner)</relationshipLabel>
        <relationshipName>NS_Support_Contracts1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Responsible_Partner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Support Provided By</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>NS Support Contracts (Support Provided By)</relationshipLabel>
        <relationshipName>NS_Support_Contracts2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Support_Level__c</fullName>
        <externalId>false</externalId>
        <label>Support Level</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Support_Percentage__c</fullName>
        <externalId>false</externalId>
        <label>Support Percentage</label>
        <precision>6</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Target_Renewal_Date__c</fullName>
        <externalId>false</externalId>
        <label>Target Renewal Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>NS Support Contract</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>OBJECT_ID</columns>
        <columns>Contract_End_Date__c</columns>
        <columns>Contract_Start_Date__c</columns>
        <columns>INT_NetSuite_InternalID__c</columns>
        <columns>Support_Level__c</columns>
        <columns>Number_of_Active_Items__c</columns>
        <columns>Has_Active_Items__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Contract Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>NS Support Contracts</pluralLabel>
    <searchLayouts>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>INT_NetSuite_InternalID__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <sharingReasons>
        <fullName>Partner_account__c</fullName>
        <label>Partner account</label>
    </sharingReasons>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Refresh_Contract_Items</fullName>
        <availability>online</availability>
        <description>This button kicks off the integration to update the contract items populated on the contract.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Refresh Contract Items</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!requireScript(&quot;/soap/ajax/30.0/connection.js&quot;)}
{!requireScript(&quot;/soap/ajax/30.0/apex.js&quot;)}

try { 		
		var orgId = &apos;{!$Organization.Id}&apos;; 
		var nsSupContractId = &apos;{!NS_Support_Contract__c.INT_NetSuite_InternalID__c}&apos;;
		sforce.connection.sessionId = &apos;{!$Api.Session_ID}&apos;; 
		RefreshContractItems(nsSupContractId);  
	} 
catch (ex) { 
	alert(&apos;Unrecoverable ERROR: &apos; + ex); 
} 

//Get Boomi info from QTCustom Settings 
function getBoomiSettings() { 
	var resultCustomSettings = sforce.connection.query(&quot;select BoomiBaseURL__c, BoomiToken__c From QTCustomSettings__c where name = &apos;default&apos;&quot;); 
	var customRecords = resultCustomSettings.getArray(&quot;records&quot;); 

	if (customRecords.length &gt; 0) { 
		var rec = customRecords[0]; 
		if (rec.BoomiBaseURL__c &amp;&amp; rec.BoomiBaseURL__c != &apos;&apos; &amp;&amp; rec.BoomiToken__c &amp;&amp; rec.BoomiToken__c != &apos;&apos;) 
		return { BoomiBaseURL: rec.BoomiBaseURL__c, BoomiToken: rec.BoomiToken__c }; //Boomi URL and Token 
	} 
} 


/* Call Boomi to refresh NS support contract items */ 
function RefreshContractItems(nsSupContractId) { 
	log(&apos;NS Support Contract ID : &apos; + nsSupContractId ); 

	var boomiSettings = getBoomiSettings(); 

	if (!boomiSettings.BoomiBaseURL || !boomiSettings.BoomiToken) { 
		alert(&apos;BOOMI Custom Settings are missing!!&apos;); 
		return; 
	} 	

	var urlToCall = boomiSettings.BoomiBaseURL + &apos;upsertContract&apos;; 

	var boomiMessage = &apos;&lt;?xml version=&quot;1.0&quot;?&gt;&lt;Record environment=&quot;&apos; + orgId + &apos;&quot; type=&quot;NSContract&quot;&gt;&lt;ID&gt;&apos; + nsSupContractId + &apos;&lt;/ID&gt;&lt;/Record&gt;&apos;; 
	log(&apos;License   Calling BOOMI &apos; + urlToCall + &apos; with Message :: &apos; + boomiMessage); 

	// HTTP headers, Content-Type and Digest Authorization. 
	var requestHeaders = { &quot;Content-Type&quot;: &quot;text/xml&quot;, &quot;Authorization&quot;: boomiSettings[&apos;BoomiToken&apos;] }; 

	sforce.connection.remoteFunction({ 
		url: urlToCall, 
		requestHeaders: requestHeaders, 
		requestData: boomiMessage, 
		method: &quot;POST&quot;, 
		timeout: 2000000, 
		async: false, 
		onSuccess: function (response) { alert(&apos;Contract items updated.&apos; + response); }, 
		onFailure: function (response) { alert(&apos;Unable to update contract items &apos; + response);}
	}); 
} 


function log(message) { 
	if (console &amp;&amp; console.log) 
	console.log(message); 
}</url>
    </webLinks>
</CustomObject>
