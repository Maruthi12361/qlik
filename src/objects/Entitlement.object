<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>AccountAccountLicenseEqual__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Account_License__r.Account__c = AccountId, &apos;true&apos;,&apos;false&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>AccountAccountLicenseEqual</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AccountId</fullName>
        <trackHistory>false</trackHistory>
        <type>MasterDetail</type>
    </fields>
    <fields>
        <fullName>Account_License__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account License</label>
        <referenceTo>Account_License__c</referenceTo>
        <relationshipLabel>Entitlements</relationshipLabel>
        <relationshipName>Entitlements</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Account_Support_Office_r_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Account.Support_Office__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Support Office</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Application__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Application__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Application</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AssetId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Automatic__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If an Entitlement is flagged as automatic and no Account License linked to the Entitlement, it will be deleted in the following cases (see EntitlementSyncBatch24h and EntitlementSyncBatchable):
1 - if there are no cases linked to the Entitlement it will be deleted
2 - if there are cases linked to the Entitlement, the Name will be updated to &quot;Deleted - &quot;+old name. All other fields are cleared, except Account Name. The record type will be changed to &quot;Obsolete Entitlement&quot;.</description>
        <externalId>false</externalId>
        <inlineHelpText>When flagged, the entitlement will be deleted if there are no cases linked, otherwise the name will be updated and all fields will be cleared, except Account Name and the record type will be changed to &quot;Obsolete Entitlement&quot;.</inlineHelpText>
        <label>Automatic</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>BusinessHoursId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CasesPerEntitlement</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>ContractLineItemId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Current_Sell_Through_Partner__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;/&quot;&amp; Account_License__r.Selling_Partner__c , Account_License__r.Selling_Partner__r.Name)</formula>
        <label>Current Sell Through Partner</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSE_Designated_Support_Engineer__c</fullName>
        <externalId>false</externalId>
        <formula>Account.QT_Designated_Support_Contact__r.LastName &amp; &quot; &quot; &amp; Account.QT_Designated_Support_Contact__r.FirstName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>DSE-Designated Support Engineer</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Designated_Support_Engineer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Designated Support Engineer</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Entitlements</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Difference_Exists_Office_Global__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Account__r.Global_Support_Account__c &amp;&amp;
IF(
AND (
CONTAINS( Account_License__r.INT_NetSuite_Support_Level__c, &apos;Enterprise&apos;),
NOT(CONTAINS(SLA_Process_Name__c, &apos;Signature&apos;)) 
),
NOT(CONTAINS(BusinessHours.Name, &apos;Enterprise&apos;)),
CONTAINS(BusinessHours.Name, &apos;Enterprise&apos;)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference Exists Office Global</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Difference_Exists_Office_Not_Global__c</fullName>
        <externalId>false</externalId>
        <formula>NOT(Account_License__r.Account__r.Global_Support_Account__c) &amp;&amp; 
IF(
AND(
CONTAINS( Account_License__r.INT_NetSuite_Support_Level__c, &apos;Enterprise&apos;),
NOT(CONTAINS(SLA_Process_Name__c, &apos;Signature&apos;)) 
),
BusinessHours.Id &lt;&gt; Account_License__r.Account__r.Support_Office__r.Enterprise_Business_Hours__c,
BusinessHours.Id &lt;&gt; Account_License__r.Account__r.Support_Office__r.Business_Hours__c
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference Exists Office Not Global</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Difference_Exists_Subscription__c</fullName>
        <externalId>false</externalId>
        <formula>Subscription__c &lt;&gt; Account_License__r.Subscription__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference Exists Subscription</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Difference_Exists_Support_Date__c</fullName>
        <externalId>false</externalId>
        <formula>OR(
StartDate &lt;&gt;  Account_License__r.Support_From__c, 
EndDate &lt;&gt;  Account_License__r.Support_To__c
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference Exists Support Date</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Difference_Exists_Support_Level__c</fullName>
        <externalId>false</externalId>
        <formula>OR(
AND(
OR(
AND( NOT(ISBLANK(Signature_End_Date__c)), Signature_End_Date__c &gt;= TODAY()),
Account.Premier_Support__c
),
NOT(CONTAINS(SLA_Process_Name__c, &apos;Signature&apos;))
)
,
AND (
AND( OR (ISBLANK(Signature_End_Date__c), Signature_End_Date__c &lt; TODAY()), NOT(Account.Premier_Support__c)),
OR (
CONTAINS(SLA_Process_Name__c, &apos;Signature&apos;),
AND (
NOT(ISBLANK(Account_License__r.INT_NetSuite_Support_Level__c)),
NOT(CONTAINS(SLA_Process_Name__c, Account_License__r.INT_NetSuite_Support_Level__c))
)
)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference Exists Support Level</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Difference_Exists_Support_Office__c</fullName>
        <description>Element of the &apos;Difference Exists&apos; formula field:
- If it&apos;s an Enterprise customer, the Business hours should always be the enterprise Business Hours from Support Office of Account, even if it is Global Support Account
- If it&apos;s not Enterprise (Basic), the Business hours should be the same as the Business hours on the account license
- If it&apos;s a Global Support Account , but not an Enterprise customer, the Business hours should not be updated
- If Enterprise level is changed to Basic,  the Business hours should be updated even fpr Global Accounts</description>
        <externalId>false</externalId>
        <formula>OR (
Difference_Exists_Office_Global__c,
Difference_Exists_Office_Not_Global__c 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference Exists Support Office</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Difference_Exists__c</fullName>
        <description>This field is used to check if there are differences between the values on the account license and product license.There are 5 fields in total.</description>
        <externalId>false</externalId>
        <formula>AND(
NOT(ISBLANK( Account_License__c )),
NOT(ISBLANK( Account_License__r.Account__c )),
NOT(ISBLANK( Account_License__r.Account__r.Support_Office__c )),
NOT(ISBLANK( Account_License__r.INT_NetSuite_Support_Level__c )),
OR(Account_License__r.INT_NetSuite_Support_Level__c = &apos;Enterprise&apos;, Account_License__r.INT_NetSuite_Support_Level__c = &apos;Basic&apos;),
Account_License__r.Support_To__c &gt;= Account_License__r.Support_From__c ,
OR(
Difference_Exists_Support_Date__c,
Difference_Exists_Support_Office__c,
Difference_Exists_Support_Level__c,
Difference_Exists_Subscription__c
)
)</formula>
        <label>Difference Exists</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Document_CAL__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Document_CAL__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Document CAL</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Dynamic_update_RTS__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Account_License__r.Dynamic_update__c, &apos;True&apos;, &apos;False&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Dynamic update (RTS)</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EndDate</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Entitlement_Check__c</fullName>
        <description>set to true if the product license should be available for selection</description>
        <externalId>false</externalId>
        <formula>OR
(
$User.Contact_Account_Name__c=AccountId, 
$User.Contact_Account_Name__c=Account_License__r.Support_Provided_By__c, 
$User.Contact_Account_Name__c=Account_License__r.Selling_Partner__c, 
ISPICKVAL($User.UserType,&apos;Standard&apos;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>true if the product license should be available for selection</inlineHelpText>
        <label>Entitlement Check</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Full_Version__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Full_Version__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Full Version</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IsPerIncident</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Legacy_License_Key__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Legacy_License_Key__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Legacy License Key</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>License_Account_NetSuite_Id__c</fullName>
        <description>Account NetSuiteId</description>
        <externalId>false</externalId>
        <formula>Account.INT_NetSuite_InternalID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>License Account NetSuite Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>License_Netsuite_Internal_Id__c</fullName>
        <description>Field used to store the netsuite id of the account license.</description>
        <externalId>false</externalId>
        <formula>Account_License__r.INT_NetSuite_InternalID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>License Netsuite Internal Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>License_Reference__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Field for internal references. Use the Edit button to modify.</inlineHelpText>
        <label>License Reference</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>License_Restrictions__c</fullName>
        <description>Field which populates with the Source and Target on relevant license records (Attunity Replicate). Populated via a process building on Account License object</description>
        <externalId>false</externalId>
        <label>License Restrictions</label>
        <length>4000</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Name</fullName>
        <inlineHelpText>The 16-digit QlikView license number which the Support Case relates to. &apos;Deleted&apos; means that the license is no longer in use, but is linked to previously reported Support Cases.</inlineHelpText>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Named_User_CAL__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Named_User_CAL__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Named User CAL</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>No_of_Documents__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.No_of_Documents__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>No of Documents</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>No_of_Servers_in_Cluster__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.No_of_Servers_in_Cluster__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>No of Servers in Cluster</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RemainingCases</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Responsible_Partner_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Selling_Partner__r.Name</formula>
        <label>Current Sell Through Partner Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SLA_Process_Name__c</fullName>
        <description>This field stores the name of the entitlment process lookup as its not available in formula fields. This will be available through lookup.</description>
        <externalId>false</externalId>
        <label>SLA Process Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ServiceContractId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Session_CAL__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Session_CAL__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Session CAL</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Signature_End_Date__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Signature_End_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Manually set on the Account License by CSM</inlineHelpText>
        <label>Signature End Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SlaProcessId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Special_Edition__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Special_Edition__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Special Edition</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>StartDate</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Status</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>StatusIndicator</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Subscription_End_Date__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Subscription_End_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Subscription End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Subscription_Start_Date__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Subscription_Start_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Subscription Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Subscription__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Subscription</label>
        <referenceTo>Zuora__Subscription__c</referenceTo>
        <relationshipLabel>Entitlements</relationshipLabel>
        <relationshipName>Entitlements</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Support_Level__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.INT_NetSuite_Support_Level__c</formula>
        <label>Support Level</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Support_Provided_By_Id__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Support_Provided_By__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Support Provided By Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Support_Provided_By_Text__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Support_Provided_By__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Support Provided By Text</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Support_Provided_By__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;/&quot;&amp; Account_License__r.Support_Provided_By__c , Account_License__r.Support_Provided_By__r.Name )</formula>
        <label>Support Provided By</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time_Limit__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Time_Limit__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Time Limit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Tokens__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Tokens__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tokens</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type</fullName>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Uncapped__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Account_License__r.Uncapped__c, &apos;True&apos;, &apos;False&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Uncapped</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Upgrade_From__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Upgrade_From__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Upgrade From</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Upgrade_To__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Upgrade_To__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Upgrade To</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Usage_CAL__c</fullName>
        <externalId>false</externalId>
        <formula>Account_License__r.Usage_CAL__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Usage CAL</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Web_Parts__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Account_License__r.Web_Parts__c, &apos;True&apos;, &apos;False&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Web Parts</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Workbench__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Account_License__r.Workbench__c, &apos;True&apos;,&apos;False&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Workbench</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <listViews>
        <fullName>Active_Product_Licenses</fullName>
        <booleanFilter>1 AND 2</booleanFilter>
        <columns>ENTITLEMENT.NAME</columns>
        <columns>Description__c</columns>
        <columns>ACCOUNT.NAME</columns>
        <columns>ENTITLEMENT.STARTDATE</columns>
        <columns>ENTITLEMENT.ENDDATE</columns>
        <columns>ENTITLEMENT.STATUS</columns>
        <columns>SLA.NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>SLA.NAME</field>
            <operation>notContain</operation>
            <value>old</value>
        </filters>
        <filters>
            <field>ENTITLEMENT.STATUS</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Active Product Licenses</label>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
            <allPartnerUsers></allPartnerUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>All_Product_Licenses</fullName>
        <columns>ENTITLEMENT.NAME</columns>
        <columns>Description__c</columns>
        <columns>ACCOUNT.NAME</columns>
        <columns>ENTITLEMENT.STARTDATE</columns>
        <columns>ENTITLEMENT.ENDDATE</columns>
        <columns>ENTITLEMENT.STATUS</columns>
        <columns>SLA.NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All Product Licenses</label>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>Entitlement_Product_License</fullName>
        <columns>ENTITLEMENT.NAME</columns>
        <columns>ACCOUNT.NAME</columns>
        <columns>ENTITLEMENT.STARTDATE</columns>
        <columns>ENTITLEMENT.ENDDATE</columns>
        <columns>ENTITLEMENT.STATUS</columns>
        <columns>ENTITLEMENT.STATUSINDICATOR</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Entitlement_Check__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Entitlement/Product License</label>
    </listViews>
    <listViews>
        <fullName>Inactive_Entitlement_Processes</fullName>
        <booleanFilter>1 AND 2</booleanFilter>
        <columns>ENTITLEMENT.NAME</columns>
        <columns>ACCOUNT.NAME</columns>
        <columns>ENTITLEMENT.STARTDATE</columns>
        <columns>ENTITLEMENT.ENDDATE</columns>
        <columns>ENTITLEMENT.STATUS</columns>
        <columns>ENTITLEMENT.LAST_UPDATE</columns>
        <columns>SLA.NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>SLA.NAME</field>
            <operation>notEqual</operation>
            <value>Standard Support SLA</value>
        </filters>
        <filters>
            <field>SLA.NAME</field>
            <operation>notEqual</operation>
            <value>Premium Support SLA</value>
        </filters>
        <label>Inactive Entitlement Processes</label>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>Old_Entitlements</fullName>
        <columns>ENTITLEMENT.NAME</columns>
        <columns>ACCOUNT.NAME</columns>
        <columns>ENTITLEMENT.STARTDATE</columns>
        <columns>ENTITLEMENT.ENDDATE</columns>
        <columns>ENTITLEMENT.STATUS</columns>
        <columns>ENTITLEMENT.STATUSINDICATOR</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>SLA.NAME</field>
            <operation>contains</operation>
            <value>old</value>
        </filters>
        <label>Old Entitlements</label>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Obsolete</fullName>
        <active>true</active>
        <label>Obsolete</label>
        <picklistValues>
            <picklist>Type</picklist>
            <values>
                <fullName>Phone Support</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Web Support</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Standard</fullName>
        <active>true</active>
        <label>Standard</label>
        <picklistValues>
            <picklist>Type</picklist>
            <values>
                <fullName>Phone Support</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Web Support</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>ENTITLEMENT.NAME</customTabListAdditionalFields>
        <customTabListAdditionalFields>ACCOUNT.NAME</customTabListAdditionalFields>
        <customTabListAdditionalFields>ENTITLEMENT.STARTDATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>ENTITLEMENT.ENDDATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>ENTITLEMENT.STATUS</customTabListAdditionalFields>
        <customTabListAdditionalFields>ENTITLEMENT.STATUSINDICATOR</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>ENTITLEMENT.NAME</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ACCOUNT.NAME</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ENTITLEMENT.STARTDATE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ENTITLEMENT.ENDDATE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ENTITLEMENT.STATUS</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ENTITLEMENT.STATUSINDICATOR</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ENTITLEMENT.NAME</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ACCOUNT.NAME</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ENTITLEMENT.STARTDATE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ENTITLEMENT.ENDDATE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ENTITLEMENT.STATUS</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ENTITLEMENT.STATUSINDICATOR</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>ENTITLEMENT.NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ACCOUNT.NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ENTITLEMENT.STARTDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ENTITLEMENT.ENDDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ENTITLEMENT.STATUS</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ENTITLEMENT.STATUSINDICATOR</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Cannot_Edit_Entitlement_Name</fullName>
        <active>true</active>
        <description>Profiles in cases management processes can not modify Entitlement Name. Entitlement name can only be modified by profiles Custom: support Manager and System Administrator</description>
        <errorConditionFormula>ISCHANGED( Name ) &amp;&amp; NOT($Profile.Name = &apos;Custom: Support Manager&apos;) &amp;&amp; NOT($Profile.Name = &apos;System Administrator&apos;)</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>Entitlement name is not editable.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Portal_User_Cannot_Edit_Entitlement_Name</fullName>
        <active>false</active>
        <errorConditionFormula>ISCHANGED( Name ) &amp;&amp;  (TEXT($Profile.UserType)&lt;&gt;&apos;Standard&apos;)</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>Entitlement name is not editable.</errorMessage>
    </validationRules>
</CustomObject>
