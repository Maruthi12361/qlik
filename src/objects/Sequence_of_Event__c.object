<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Checkpoint__c</fullName>
        <externalId>false</externalId>
        <label>Checkpoint</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Go</fullName>
                    <default>false</default>
                    <label>Go</label>
                </value>
                <value>
                    <fullName>No Go</fullName>
                    <default>false</default>
                    <label>No Go</label>
                </value>
                <value>
                    <fullName>Go / No Go</fullName>
                    <default>false</default>
                    <label>Go / No Go</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>Not Applicable</fullName>
                    <default>true</default>
                    <label>Not Applicable</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Cost__c</fullName>
        <externalId>false</externalId>
        <label>Cost</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Customer_Agreed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Customer Agreed?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Event_Charge__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Event Charge?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Opportunity_ID__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Sequence of Events</relationshipLabel>
        <relationshipName>Sequence_of_Events</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Proposed_Event__c</fullName>
        <externalId>false</externalId>
        <label>Proposed Event</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Interview with Key Players</fullName>
                    <default>false</default>
                    <label>Interview with Key Players</label>
                </value>
                <value>
                    <fullName>Survey IT environment / Gather information for SIB</fullName>
                    <default>false</default>
                    <label>Survey IT environment / Gather information for SIB</label>
                </value>
                <value>
                    <fullName>Prove Capabilities to Key Players (SIB)</fullName>
                    <default>false</default>
                    <label>Prove Capabilities to Key Players (SIB)</label>
                </value>
                <value>
                    <fullName>Customer SIB Sign-off</fullName>
                    <default>false</default>
                    <label>Customer SIB Sign-off</label>
                </value>
                <value>
                    <fullName>Implementation Plan Developed</fullName>
                    <default>false</default>
                    <label>Implementation Plan Developed</label>
                </value>
                <value>
                    <fullName>Facilitate Cost versus Benefit (ROI/TCO Tool)</fullName>
                    <default>false</default>
                    <label>Facilitate Cost versus Benefit (ROI/TCO Tool)</label>
                </value>
                <value>
                    <fullName>Provide contracts for legal review</fullName>
                    <default>false</default>
                    <label>Provide contracts for legal review</label>
                </value>
                <value>
                    <fullName>Define Success Metrics</fullName>
                    <default>false</default>
                    <label>Define Success Metrics</label>
                </value>
                <value>
                    <fullName>Gain legal approval of contracts</fullName>
                    <default>false</default>
                    <label>Gain legal approval of contracts</label>
                </value>
                <value>
                    <fullName>Share reference information</fullName>
                    <default>false</default>
                    <label>Share reference information</label>
                </value>
                <value>
                    <fullName>Conduct pre-decision review</fullName>
                    <default>false</default>
                    <label>Conduct pre-decision review</label>
                </value>
                <value>
                    <fullName>Present proposal for Approval</fullName>
                    <default>false</default>
                    <label>Present proposal for Approval</label>
                </value>
                <value>
                    <fullName>Services Initiation meeting</fullName>
                    <default>false</default>
                    <label>Services Initiation meeting</label>
                </value>
                <value>
                    <fullName>Customer Task</fullName>
                    <default>false</default>
                    <label>Customer Task</label>
                </value>
                <value>
                    <fullName>Production Go Live Date</fullName>
                    <default>false</default>
                    <label>Production Go Live Date</label>
                </value>
                <value>
                    <fullName>Partner Task</fullName>
                    <default>false</default>
                    <label>Partner Task</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Responsibility__c</fullName>
        <externalId>false</externalId>
        <label>Responsibility</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>S_o_E_Date__c</fullName>
        <externalId>false</externalId>
        <label>S o E Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Customer</fullName>
                    <default>false</default>
                    <label>Customer</label>
                </value>
                <value>
                    <fullName>QlikTech</fullName>
                    <default>false</default>
                    <label>QlikTech</label>
                </value>
                <value>
                    <fullName>Partner</fullName>
                    <default>false</default>
                    <label>Partner</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <label>Sequence of Event</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>SOE{0000000}</displayFormat>
        <label>S o E ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sequence of Events</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Opportunity_ID__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Cost__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>S_o_E_Date__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Opportunity_ID__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Cost__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>S_o_E_Date__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Opportunity_ID__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Cost__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>S_o_E_Date__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchResultsAdditionalFields>Opportunity_ID__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Cost__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>S_o_E_Date__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>XO009_ProEvent_SoEDate_cannot_be_blank</fullName>
        <active>true</active>
        <description>XO009 The Proposed Event field and the SoE Date field cannot be blank.</description>
        <errorConditionFormula>/* 2013_04_22 CCE CR# 7922 */
OR(
    ISPICKVAL(Proposed_Event__c,&quot;&quot;),
    ISBLANK( S_o_E_Date__c) 
)</errorConditionFormula>
        <errorMessage>XO009 The Proposed Event field and the SoE Date field cannot be blank.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Create_email</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Create email</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/16.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/16.0/apex.js&quot;)}

function emailResults(sendEmailResult) { 
 
    if (sendEmailResult.length &gt; 0) {
        if (sendEmailResult[0].getBoolean(&quot;success&quot;)) {
             alert(&quot;Email has been sent!&quot;);
        } else {	
             alert(&apos;Got error while sending your email: &apos; + sendEmailResult[0].errors);
        }
    } else { 
        alert(&quot;Didn&apos;t get return data.&quot;); 
    } 
} 



var email = new sforce.SingleEmailMessage();

// Set the Id of the Visualforce template here
email.templateId = &apos;00X200000017O31&apos;;

// Set reciepient credentials
email.targetObjectId = sforce.connection.getUserInfo().userId;

// Don&apos;t save this message to activities (if true an error will be generated since it&apos;s a User and not a Contact we&apos;re sending the email to)
email.saveAsActivity = false;

// Set the related Id (opportunity)
email.whatId = &apos;{!Opportunity.Id}&apos;;


try {

    var sendMailRes = sforce.connection.sendEmail([email], emailResults);

} catch (e) {
    alert(&apos;Cought an exception: &apos; + e.description);
}</url>
    </webLinks>
    <webLinks>
        <fullName>Multiple_SoE_Edit</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Create &amp; Manage the SoE</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/multiSoEEdit?opportunityid={!Opportunity.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Sequence_of_Events_Summary</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Sequence of Events Summary</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/33.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/33.0/apex.js&quot;)} 

var args={oppid:&quot;{!JSENCODE(Opportunity.Id)}&quot;,name:&quot;{!JSENCODE(Opportunity.Name)}&quot;,url:&quot;{!JSENCODE(Opportunity.Link)}&quot;}; 
var resp = sforce.apex.execute(&quot;OpportunityPDFCreator&quot;,&quot;CreateSoEPDF&quot;,args); 
if(resp.indexOf(&quot;Error:&quot;) == 0) { 
window.alert(resp); 
} 
else { 
var pdf_url = &quot;/servlet/servlet.FileDownload?file=&quot; + resp; 
window.open(pdf_url,&quot;&quot;,&quot;left=200,width=900,height=600&quot;); 
window.location = &quot;{!Opportunity.Link}&quot;; 
}</url>
    </webLinks>
</CustomObject>
