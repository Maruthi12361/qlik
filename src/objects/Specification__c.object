<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Approved__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Approved</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Change_Control__c</fullName>
        <externalId>false</externalId>
        <label>Change Control</label>
        <referenceTo>SLX__Change_Control__c</referenceTo>
        <relationshipLabel>Specifications</relationshipLabel>
        <relationshipName>Specifications</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>15</visibleLines>
    </fields>
    <fields>
        <fullName>Requestor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Should be populated from master objects requestor field</description>
        <externalId>false</externalId>
        <label>Requestor</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Specifications</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>true</default>
                    <label>New</label>
                </value>
                <value>
                    <fullName>Work in progress</fullName>
                    <default>false</default>
                    <label>Work in progress</label>
                </value>
                <value>
                    <fullName>Ready for approval</fullName>
                    <default>false</default>
                    <label>Ready for approval</label>
                </value>
                <value>
                    <fullName>Pending approval</fullName>
                    <default>false</default>
                    <label>Pending approval</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Recalled</fullName>
                    <default>false</default>
                    <label>Recalled</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Specification</label>
    <nameField>
        <label>Specification</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Specifications</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Cant_reset_to_new_on_working_spec</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
  ISCHANGED( Status__c ),
  NOT(ISPICKVAL(PRIORVALUE( Status__c ), &quot;New&quot;)),
  ISPICKVAL( Status__c, &quot;New&quot;)
)</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>Open Specification Status cannot be reset to New.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Check_that_Requestor_as_in_Change_Contro</fullName>
        <active>false</active>
        <description>Control that the Requestor field is the same as in master object</description>
        <errorConditionFormula>Requestor__c = Change_Control__r.SLX__Requestor__c</errorConditionFormula>
        <errorDisplayField>Requestor__c</errorDisplayField>
        <errorMessage>Requestor must be the same as in Change Request/Project</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
