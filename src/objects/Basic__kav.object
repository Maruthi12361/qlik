<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <articleTypeChannelDisplay>
        <articleTypeTemplates>
            <channel>App</channel>
            <template>Toc</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Prm</channel>
            <template>Toc</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Csp</channel>
            <template>Toc</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Pkb</channel>
            <page>Basic_Article</page>
            <template>Page</template>
        </articleTypeTemplates>
    </articleTypeChannelDisplay>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Basic articles are responses to questions on how to do something or requests for information on items like features, procedures, etc. typically How To or FAQ articles.</description>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <fields>
        <fullName>Attachment_1__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Attach documents as needed. Use hyperlinks instead when possible.</inlineHelpText>
        <label>Attachment 1</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>File</type>
    </fields>
    <fields>
        <fullName>Attachment_2__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Attach documents as needed. Use hyperlinks instead when possible.</inlineHelpText>
        <label>Attachment 2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>File</type>
    </fields>
    <fields>
        <fullName>Attachment_3__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Attach documents as needed. Use hyperlinks instead when possible.</inlineHelpText>
        <label>Attachment 3</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>File</type>
    </fields>
    <fields>
        <fullName>Author__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Author</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Basic</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Bug_ID__c</fullName>
        <description>The ID of the related bug</description>
        <externalId>false</externalId>
        <label>Bug ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Cause__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The Cause identifies the root cause of the actual problem. It explains why you are applying the resolution to the customer’s problem. The Cause can be as lengthy as necessary to fully describe the problem, and should be punctuated appropriately.</inlineHelpText>
        <label>Cause</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Chat_Answer__c</fullName>
        <description>CR# 11567</description>
        <externalId>false</externalId>
        <label>Chat Answer</label>
        <length>5000</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Provide an introduction - what FAQ will be answered, what How To description will be given etc.</inlineHelpText>
        <label>Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Internal_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Internal Comments are viewable by internal users only. Internal Comments are used for general or supplemental information that you want to communicate to an agent who may use the solution.</inlineHelpText>
        <label>Internal Comments</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>PKB_Link__c</fullName>
        <externalId>false</externalId>
        <formula>IF (IsVisibleInPkb,
$Setup.QS_Partner_Portal_Urls__c.Support_Portal_Login_Url__c + &quot;/articles/&quot; + ArticleNumber,
null)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>PKB Link</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Prior_ID__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Use e.g. When two articles are merged after deduplication.</inlineHelpText>
        <label>Prior ID</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Resolution__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The Resolution clearly sets forth the steps to take to resolve an issue. It should first tell the user why they are performing the steps (the Intent), then tell them where they should be in the interface (if necessary for clarity), and then tell them what</inlineHelpText>
        <label>Resolution</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>SFDC_Org2Id__c</fullName>
        <description>This field has been created to assist with data migration activities, which will store the legacy ArticleId from another Salesforce org.</description>
        <externalId>false</externalId>
        <label>SFDC Org2Id</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tags__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Tag the article to describe the contents. This helps finding the article, and also evaluating if it is relevant once found.</inlineHelpText>
        <label>Tags</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>TextArea</type>
    </fields>
    <label>Basic</label>
    <pluralLabel>Basic</pluralLabel>
</CustomObject>
