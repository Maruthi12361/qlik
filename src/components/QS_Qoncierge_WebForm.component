<!----------------------------------------------------------------------------------------------------------
Purpose: Qoncierge Contact Web Form

Change log:
2018-04-23 IT-573 Fix Country and Description error messages - ext_bad
2018-05-03 IT-572 Add message to unblock popu ups - ext_bad
2018-08-28 NC0139648 - disable Chat with us button after clicking - extbad
2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
2019-06-12 extbad IT-1923 Fix Chat button for chats that need contact creation
2019-06-14 extbad CHG0036350 (IT-1936) Disable Log a case button after clock on it
-------------------------------------------------------------------------------------------------------------->
<apex:component controller="QS_Qoncierge_WebForm_Cntrl" allowDML="true" id="qonciergeWebForm">
    <apex:attribute type="QS_CaseWizard_Controller" required="true" name="wizardPageRef"
                    description="Case Wizard Page's Reference" assignTo="{!caseWizardRef}"/>

    <div style="display:none">
        <apex:pageMessages id="PQonmessage"/>
    </div>

    <apex:pageBlock id="userQonciergeInputPanel">

        <apex:outputText styleClass="qs-h2" value="Contact information"
                         rendered="{! !isFailure && (tempCaseObj == null || tempCaseObj.Id == null)}"/>
        <apex:outputPanel id="outerQonciergePanel"
                          rendered="{! !isFailure && (tempCaseObj == null || tempCaseObj.Id == null)}">
            <div class="qs-submitqonciergeform">
                <apex:outputPanel id="componentContactFields">
                    <apex:outputlabel for="first_name">First Name *</apex:outputlabel>
                    <div class="field textfield">
                        <apex:inputField id="first_name" required="true" styleClass="form_first_name" value="{!contactTempObj.FirstName}"/>
                    </div>
                    <apex:outputlabel for="last_name">Last Name *</apex:outputlabel>
                    <div class="field textfield">
                        <apex:inputField id="last_name" required="true" styleClass="form_last_name" value="{!contactTempObj.LastName}"/>
                    </div>
                    <apex:outputlabel for="company *">Company</apex:outputlabel>
                    <div class="field textfield">
                        <apex:inputText id="company" styleClass="form_company" value="{!contactCompany}"/>
                    </div>
                    <apex:outputlabel for="country">Country</apex:outputlabel>
                    <div class="field">
                        <div class="selectfield">
                            <apex:actionRegion>
                                <apex:inputField id="country" styleClass="form_country" required="true" value="{!contactTempObj.Contact_Country__c}">
                                    <apex:actionSupport event="onchange" action="{!populateCountrySupport}" reRender="licenseNumberPanel"/>
                                </apex:inputField>
                            </apex:actionRegion>
                        </div>
                    </div>
                    <apex:outputlabel for="work_email">Email *</apex:outputlabel>
                    <div class="field textfield">
                        <apex:inputField id="work_email" styleClass="form_email" value="{!contactTempObj.Email}"
                                     required="true"/>
                    </div>
                    <apex:outputlabel for="phone_number">Phone Number incl. Country Code</apex:outputlabel>
                    <div class="field textfield">
                        <apex:inputField id="phone_number" styleClass="form_phone" value="{!contactTempObj.Phone}"/>
                    </div>
                    <apex:variable rendered="{! qonciergeCountrySupport == null || qonciergeCountrySupport.Support_Office__c == null || qonciergeCountrySupport.Support_Office__c == ''}" var="renderlicensenumber" value="1">
                        <apex:outputlabel for="license_number">
                            License Number
                        </apex:outputlabel>
                        <div class="field textfield">
                            <apex:inputField id="license_number" styleClass="form_license_number" value="{!tempCaseObj.License_No__c}"/>
                        </div>
                    </apex:variable>
                </apex:outputPanel>
                <apex:outputPanel id="contactFields">
                    <div style="display:none">
                        <apex:outputText styleClass="contact_errors" value="{!apexMessages}"/>
                        <apex:outputText styleClass="contact_id" rendered="{!newContactChat != null}"
                                         value="{!newContactChat.Id}"/>
                        <apex:outputText styleClass="contact_recordTypeId"
                                         rendered="{!newContactChat != null && newContactChat.Id == null}"
                                         value="{!newContactChat.RecordTypeId}"/>
                        <apex:outputText styleClass="contact_ownerId"
                                         rendered="{!newContactChat != null && newContactChat.Id == null}"
                                         value="{!newContactChat.OwnerId}"/>
                        <apex:outputText styleClass="contact_accountId"
                                         rendered="{!newContactChat != null}"
                                         value="{!newContactChat.AccountId}"/>
                        <apex:outputText styleClass="contact_description"
                                         rendered="{!newContactChat != null && newContactChat.Id == null}"
                                         value="{!newContactChat.Description}"/>
                        <apex:outputText styleClass="form_prod_license"
                                         rendered="{!newContactChat != null}"
                                         value="{!prodLicenseId}"/>
                    </div>
                </apex:outputPanel>
            </div>

            <apex:outputPanel styleClass="qs-wizard-button" id="submitQonciergePanel">

                <apex:actionFunction name="populateContactFields"
                                     id="populateContactFields"
                                     action="{!populateContactFields}"
                                     onComplete="validateCaseFieldsAfterContact()"
                                     reRender="showRequiredErrorMessageSection, PmessagePanel, PQonmessage, errorsCaseAPI, contactFields, componentContactFields"/>
                <script type="text/javascript">
                    function startGuestCase() {
                        var submit = $('.submitContactFromBtn');
                        var submitting = $('.submittingContactFromBtn');
                        if (submit != null && submitting != null) {
                            submit.css('display', 'none');
                            submitting.css('display', 'block');
                        }
                        submitDetailsFunction();
                    }
                </script>

                <apex:actionFunction name="submitDetailsFunction"
                                     id="submitDetailsFunction"
                                     onComplete="finishLogCase()"
                                     action="{!submitDetails}"
                                     reRender="submitQonciergeRequest, PmessagePanel, finishGuestCase, componentContactFields, contactFields"/>
                <div class="buttons">
                    <div id="chatButtonDiv" style="display:none">
                        <apex:commandButton styleClass="btn chatButton"
                                            value="Chat with us"
                                            onclick="this.disabled=true;populateContactFields()"
                                            reRender="empty">
                        </apex:commandButton>
                    </div>
                    <div id="chatButtonUnavailableDiv" style="display:none">
                        <apex:commandButton styleClass="btn btn-secondary"
                                            disabled="true"
                                            value="Chat unavailable">
                        </apex:commandButton>
                    </div>
                    <div >
                        <apex:outputPanel id="submitQonciergeRequest" >
                            <apex:commandButton styleClass="btn submitContactFromBtn"
                                                id="submitQonciergeForm"
                                                value="Log a case"
                                                onclick="startGuestCase()"
                                                reRender="empty"/>
                            <apex:commandButton styleClass="btn submittingContactFromBtn" value="Submitting..." disabled="true"
                                                id="submittingQonciergeForm"
                                                style="display:none"/>
                        </apex:outputPanel>
                    </div>

                    <apex:commandButton styleClass="btn"
                                        value="Cancel"
                                        onclick="CancelCaseCreation()"
                                        reRender="empty">
                    </apex:commandButton>
                </div>
                <div style="display:none;" class="blockedChatMessage">
                    <b>No chat pops up?</b>
                    Please check if your browser is preventing the pop up, approve it and try again.
                </div>
                <div style="display:none" class="afterChatMessage">
                    If the chat resolved your issue or a follow-up case has been created during your chat session,
                    please close this window.
                    If you were not able to chat with us, please click Log a Case to submit your information and we will
                    contact you shortly.
                </div>
            </apex:outputPanel>

        </apex:outputPanel>
    </apex:pageBlock>

</apex:component>
