/********
* NAME : QuoteCreationValidation
* Description: Helper class to validate the quote creation
* 
*
*Change Log:
*   MAW 2015-02-16 Class creation
*   CAH 2015-02-26 CR# 21620 rename CTW to QSG
*   MAW 2015-03-20 CR# 21158: First Quote Approver Sales and Training Validation
*   MAW 2015-02-26 Skipped Quote Approval Role checking
*   MTM 2015-03-20 CR 16147
******/
global class QuoteCreationValidation {

    private static String message = '';
    webservice static String Validate(String oppId)
    {
        System.debug('Validating Quote creation for opp id: '+ oppId);
        if(oppId != null){
            try
            {
                Opportunity opp = GetOpp(oppId);
                
                if(opp != null && IsValidOpp(opp)){
                    return 'True';
                }
            }
            catch(DmlException e){
                system.Debug('Error in QuoteCreationValidation : ' + e); 
                message += e;
            }
        }
        return message;
        
    }
    
    private static Opportunity GetOpp(String OppId){
        for(Opportunity opp :
            [SELECT id, 
            RecordTypeId,
            Revenue_Type__c,
            StageName,

            Account.Segment_New__c, 
            Account.Billing_Contact__c,
            Account.Billing_Country_Code__c,
            Account.Billing_Contact__r.email,
            
            Signature_Type__c,

            First_Quote_Approver_License__c,
            First_Quote_Approver_Service__c,
            First_Quote_Approver_Training__c,

            License_Recipient__r.email,

            PartnerAccountId, 
            Partner_Contact__c,
            Partner_Contact__r.email,
            Partner_Contact__r.Partner_Portal_Status__c,
            Partner_Contact_for_NFR__c,
            Sell_Through_Partner__c,
            Sell_Through_Partner__r.INT_NetSuite_InternalID__c,
            Sell_Through_Partner__r.Billing_Contact__c,
            Sell_Through_Partner__r.Billing_Country_Code__c,
            Sell_Through_Partner__r.Billing_Contact__r.email,

            Owner.NS_Quote_Approval_Role__c,

            (
               SELECT Contact__c , Quote_Recipient__c, Contact__r.Email
               FROM Sphere_of_Influence__r
            )

            FROM Opportunity WHERE Id =: oppId]
            ){
                return opp;
            }
        return null;
    }

    private static String RecordType_QB1 = '01220000000J1KR';//Qlikbuy CCS Standard
    private static String RecordType_QB2 = '012D0000000KEKO';//Qlikbuy CCS Standard II
    private static String RecordType_CCS = '01220000000DugI';//CCS - Customer Centric Selling    
    private static String RecordType_PartnerNFR = Label.NFRRecordType; //Partner Not For Resale
    private static String portalStatus_Complete = 'Complete';
    private static String portalStatus_Enabled = 'Portal user enabled';
    private static String profile = '{!$Profile.Name}';

    public static Boolean IsValidOpp(Opportunity opp){             
        if(opp == null)
            return Error(Label.invalidOppErrorMsg); //Label Created - DNN 4/28/2015

        if(String.isEmpty(opp.Revenue_Type__c))
            return Error(Label.specifyOppRevenueTypeErrorMsg);  //Label Created - DNN 4/28/2015    

        Boolean isResellerOpp = opp.Revenue_Type__c.contains('Reseller');
        Boolean isFulfillmentOpp = opp.Revenue_Type__c.contains('Fulfillment');
        Boolean isOEMOpp = opp.Revenue_Type__c.contains('OEM');
        Boolean isOEMPrepayOpp = opp.Revenue_Type__c.contains('Prepay');
        Boolean isDirectOpp = opp.Revenue_Type__c.contains('Direct');
        Boolean isAcademicOpp = opp.Revenue_Type__c.contains('Academic');
        Boolean isServicesPrepayOpp = opp.Revenue_Type__c.contains('Services - Prepaid');

        if(opp.StageName.contains('Closed'))
            return Error(Label.closedOppErrorMsg); //Label Created - DNN 4/28/2015

        if(opp.Account.Segment_New__c == null)
            return Error(Label.missingSegmentErrorMsg); //Label Created - DNN 4/28/2015
        
        //Billing contact check on account will work for all deals except Reseller deal, For Reseller deal, we should be checking the Billing Contact on the Sell Through Partner instead
        if(!isResellerOpp && !isFulfillmentOpp)
        {
            if(opp.Account.Billing_Contact__c == null)
                return Error(Label.billingContactNullErrorMsg); //Label Created - DNN 4/28/2015

            if(opp.Account.Billing_Country_Code__c == null)
                return Error(Label.billingCountryNullErrorMsg); //Label Created - DNN 4/28/2015
        }

        //Validation not required for Partner Not For Resale
        if (opp.RecordTypeId == RecordType_PartnerNFR)        
        {
            if(String.isEmpty(opp.Partner_Contact_for_NFR__c))
                return Error(Label.partnerContactNullErrorMsg);
        }
        else
        {
            //For all types of OEM except OEMPrepay the license recipient should have an email address
            if(isOEMOpp && !isOEMPrepayOpp &&  String.isEmpty(opp.License_Recipient__r.email))
                return Error(Label.oemDealOppContactEmailErrorMsg); //Label Created - DNN 4/28/2015
            
            if(isResellerOpp || isFulfillmentOpp){
                
                if(String.isEmpty(opp.Partner_Contact__c))
                    return Error(Label.partnerContactNullErrorMsg); //Label Created - DNN 4/28/2015
                if(String.isEmpty(opp.Partner_Contact__r.email))
                    return Error(Label.partnerContactEmailNullErrorMsg); //Label Created - DNN 4/28/2015
                String portalStatus = opp.Partner_Contact__r.Partner_Portal_Status__c;
                if(String.isEmpty(portalStatus) || !( portalStatus.contains(portalStatus_Complete) || portalStatus.contains(portalStatus_Enabled)))
                    return Error(Label.partnerContactPortalUserErrorMsg); //Label Created - DNN 4/28/2015
                
                if(opp.Sell_Through_Partner__c == null)
                    return Error(Label.sellThroughPartnerErrorMsg); //Label Created - DNN 4/28/2015
                if(opp.Sell_Through_Partner__r.INT_NetSuite_InternalID__c == null)
                    return Error(Label.sellThroughPartnerNetSuiteIDErrorMsg); //Label Created - DNN 4/28/2015

                //For Reseller deal, we should be checking the Billing Contact on the Sell Through Partner instead
                if(String.isEmpty(opp.Sell_Through_Partner__r.Billing_Contact__c)){
                    return Error(Label.billingContactNullErrorMsg);  //Label Created - DNN 4/28/2015
				}
                if(String.isEmpty(opp.Sell_Through_Partner__r.Billing_Country_Code__c)){
                    return Error(Label.billingCountryNullErrorMsg); //Label Created - DNN 4/28/2015
				}
            } 
        }


        if(opp.Owner == null)
            return Error(Label.salesRepErrorMsg); //Label Created - DNN 4/28/2015

         //Quote Approval Role is only populated for internal employees in SalesForce. So we cannot trust this field.
        //if(String.isEmpty(opp.Owner.NS_Quote_Approval_Role__c) )
        //    return Error('There is no Approval Role configured for the assigned Sales Rep. Please contact Helpdesk to have this issue resolved.'); 

        //We cannot check this (have to consider different revenue types and the setting of first quote approver)
        //if(String.isEmpty(opp.First_Quote_Approver_License__c) && String.isEmpty(opp.First_Quote_Approver_Service__c))
        //    return Error('First Quote Approver for Sales or Services is required. Please populate First Quote Approver and try again.');

        Boolean isQuoteRecipientDefined = false, isSOIDefined = false, quoerRecipientHasValidEmail = false;        
        Boolean revenueTypeRequiresSOICheck = (opp.Revenue_Type__c != 'Academic Program' && (!opp.Revenue_Type__c.contains('QSG')));
        for(Sphere_of_Influence__c soi : opp.Sphere_of_Influence__r){
            isSOIDefined = true;
            if(soi.Quote_Recipient__c){
                isQuoteRecipientDefined = true;
                quoerRecipientHasValidEmail = !string.isEmpty(soi.Contact__r.Email);
            }

                
        }


        if ((!isSOIDefined) && (opp.RecordTypeId == RecordType_CCS || opp.RecordTypeId == RecordType_QB1 || opp.RecordTypeId == RecordType_QB2) && revenueTypeRequiresSOICheck)  
            return Error(Label.directDealQuoteRecipientErrorMsg);  //Label Created - DNN 4/28/2015       

        if( opp.Revenue_Type__c.contains('Direct')){
            if(!isQuoteRecipientDefined)
                return Error(Label.selectQuoteRecipientErrorMsg); //Label Created - DNN 4/28/2015

            if(!quoerRecipientHasValidEmail)
                return Error(Label.directDealQuoteRecipientEmailErrorMsg); //Label Created - DNN 4/28/2015
        }
        //Check if signature type is empty but not if it is a PRM user
        //Only applies to Direct and Academic rev types
        if(opp.Signature_Type__c == null  && !profile.contains('PRM') && (isDirectOpp || isAcademicOpp || isServicesPrepayOpp)){
                return Error(Label.signatureRequiredinOppErrorMsg); //Label Created - DNN 11/04/2015
        }
        
        return true;
    }

    private static Boolean Error(String msg){
        message = msg + '\n';
        return False;
    }
}