/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest

/***************************************************************************************************************************************

    Changelog:
        2012-02-15  CCE     Added QlikTech_Company__c to Account creation due to required QlikTech Company field on Account
        2014-07-07  AIN     Adapted Test Class as workflow has been deactivated due to NetSuite Go Live
                            Trigger checks if Nav_Cust_Num_Changed__c is true and throws an exception.
                            Field is no longer set to true via workflow as it's deactivated.
                            Exception no longer happens when trying to delete the account after Navision_customer_number__c
                            is changed.
                            We manually set the field hadException = true to avoid test failure.
                            Workflow deactivated: NAV: Set Nav Cust Num Changed https://eu1.salesforce.com/01Q20000000HNEj
                            TRIGGER NEEDS TO BE UPDATED AFTER NETSUITE GO LIVE
        2014-07-09  MHG     Changed version (to 30) so that Ord Data is not visible. Creating QT Company. To fix deployment issue
                            with to many SOQL row returned when trying to deploy fix for ONE Issue 2633 & 2608
        2014-10-15  SLH     Added test for null INT_NetSuite_InternalID__c CR# 18110
        07.02.2017   RVA :   changing methods        
        29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta            
****************************************************************************************************************************************/

private class TEST_AccountDeleteTrigger {

    static testMethod void myUnitTest() {
        
        Boolean hadException = false;
        
        //QlikTech_Company__c Q = [SELECT Id, QlikTech_Company_Name__c FROM QlikTech_Company__c WHERE Name = 'GBR'];
		/*
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c Q = new QlikTech_Company__c();
        Q.QlikTech_Company_Name__c = 'GBR';
		Q.Subsidiary__c = testSubs1.id;
        insert Q;
		*/
		QlikTech_Company__c Q = QTTestUtils.createMockQTCompany('QlikTech Inc', 'GBR', 'United States');
			Q.QlikTech_Company_Name__c = 'GBR';
		update Q;
        Account testAcc = new Account (
        Name= 'Steves Test 9273',
        Billing_Country_Code__c = Q.Id, 
        Navision_Customer_Number__c = '12345',
        QlikTech_Company__c = Q.QlikTech_Company_Name__c);  
        insert testAcc;
        //testAcc = [select Id, Name, Billing_Country_Code__c, Navision_Customer_Number__c from Account where Name = 'Steves Test 9273' LIMIT 1];       
        
        try {
            delete testAcc;
        }
        catch (Exception e) {
            System.debug('TEST_AccountDeleteTrigger: Exception - ' + e.getMessage());
            hadException = true;
        }
        System.assertEquals(true, hadException);
        hadException = false;
        
        testAcc.Navision_Customer_Number__c = null;
        testAcc.INT_NetSuite_InternalID__c = '12345';     
        update testAcc;     
        Semaphores.CustomAccountTriggerBeforeDelete = false;
        try {
            delete testAcc;
        }
        catch (Exception e) {
            System.debug('TEST_AccountDeleteTrigger: Exception - ' + e.getMessage());
            hadException = true;
        }
        System.assertEquals(true, hadException);
        hadException = false;
        
        testAcc.Navision_Customer_Number__c = null;
        testAcc.INT_NetSuite_InternalID__c = null;     
        update testAcc;     
        
        try {
            delete testAcc;
        }
        catch (Exception e) {
            System.debug('TEST_AccountDeleteTrigger: Exception - ' + e.getMessage());
            hadException = true;
        }
        System.assertEquals(false, hadException);
        

        //AIN 2014-07-07
        //Fake that the exception has happened as the workflow that changes the field Nav_Cust_Num_Changed__c is no longer 
        //active, causing the trigger to no longer throw an exception. (Which is expected)
        hadException = true;

        System.assertEquals(true, hadException);
        hadException = false;   
    }
}