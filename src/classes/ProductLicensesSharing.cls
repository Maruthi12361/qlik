/******************************************************

    Class: ProductLicensesSharing
    
    Changelog:
        2015-12-21  IRN     Created file
                            
******************************************************/
public class ProductLicensesSharing{

	public void populateRelatedPartners(List<NS_Support_Contract__c> contracts){
		System.debug('---populateRelatedPartners---');
		Map<Id, NS_Support_Contract__c> uniqueContracts = new Map<Id, NS_Support_Contract__c>();
		for(NS_Support_Contract__c c : contracts){
			if(!uniqueContracts.containsKey(c.Id)){
				uniqueContracts.put(c.Id, c);
			}
		}
		List<AggregateResult> items =[Select Contract_Item_Account_License__c, NS_Support_Contract__c from NS_Support_Contract_Item__c where NS_Support_Contract__c in :uniqueContracts.keyset() GROUP BY NS_Support_Contract__c, Contract_Item_Account_License__c]; 

		System.debug('items ' +items);
		Map<Id, Id> accountLicenseMap = new Map<Id, Id>();
		for(AggregateResult item : items){
			if(!accountLicenseMap.containsKey(String.valueof(item.get('Contract_Item_Account_License__c')))){
				accountLicenseMap.put(String.valueof(item.get('Contract_Item_Account_License__c')), String.valueof(item.get('NS_Support_Contract__c')));
			}
		}
		List<Account_License__c> licenses = [Select id, Support_Provided_By__c, Selling_Partner__c from Account_License__c where Id in :accountLicenseMap.keyset()];
		List<Account_License__c> tobeUpdated = new List<Account_License__c>();
		for(Account_License__c l : licenses){
			Boolean add = false;
			Id contractId = accountLicenseMap.get(l.Id);
			NS_Support_Contract__c contract = uniqueContracts.get(contractId);
			
			if(l.Selling_Partner__c != contract.Reseller__c){//sell through partner
				l.Selling_Partner__c = contract.Reseller__c;
				add = true;
			}
			if(l.Support_Provided_By__c != contract.Responsible_Partner__c){
				l.Support_Provided_By__c = contract.Responsible_Partner__c;
				add = true;
			}

			if(add){
				tobeUpdated.add(l);
			}
		}
		System.debug('Account_License__c to update ' + tobeUpdated);
		update tobeUpdated;
	}

	public void removeRelatedPartnersWhenNSSupportContractIsRemoves(List<Id> contractIds){
		System.debug('---removeRelatedPartnersWhenNSSupportContractIsRemoves---');
		
		List<AggregateResult> items =[Select Contract_Item_Account_License__c, NS_Support_Contract__c from NS_Support_Contract_Item__c where NS_Support_Contract__c in :contractIds GROUP BY NS_Support_Contract__c, Contract_Item_Account_License__c]; 

		System.debug('items ' +items);
		Map<Id, Id> accountLicenseMap = new Map<Id, Id>();
		for(AggregateResult item : items){
			if(!accountLicenseMap.containsKey(String.valueof(item.get('Contract_Item_Account_License__c')))){
				accountLicenseMap.put(String.valueof(item.get('Contract_Item_Account_License__c')), String.valueof(item.get('NS_Support_Contract__c')));
			}
		}
		List<Account_License__c> licenses = [Select id, Support_Provided_By__c, Selling_Partner__c from Account_License__c where Id in :accountLicenseMap.keyset()];
		List<Account_License__c> tobeUpdated = new List<Account_License__c>();
		for(Account_License__c l : licenses){
			l.Selling_Partner__c = '';
			l.Support_Provided_By__c = '';
			tobeUpdated.add(l);
		}
		update tobeUpdated;
	}

}