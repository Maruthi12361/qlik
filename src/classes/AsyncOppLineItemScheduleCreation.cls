/********************************************************
* AsyncOppLineItemScheduleCreation
* Description: This class is used to create opp product schedules asynchronously.
*
* Change Log:
* 2017-09-15   UIN Initial Development. 
* 2017-09-20   Nikhil Jain QCW-3804 Changes for MSP Schedule creation
**********************************************************/
public class AsyncOppLineItemScheduleCreation implements Queueable {
	
	Map<String, SBQQ__Quote__c> mapQuoteFromOpp = new Map<String, SBQQ__Quote__c>();
    List<OpportunityLineItem>  inputList = new List<OpportunityLineItem>();
    private static List<OpportunityLineItem> lineItemsUpd= new List<OpportunityLineItem>();

	public AsyncOppLineItemScheduleCreation(List<OpportunityLineItem> ipList, Map<String, SBQQ__Quote__c> testMap){
        this.mapQuoteFromOpp = testMap;
        this.inputList = ipList;
	}


	public void execute(QueueableContext context) {
        system.debug('UIN'+inputList);
        system.debug('UIN'+mapQuoteFromOpp);
        OpportunityScheduleCreationHandler.processScheduleSub(inputList, mapQuoteFromOpp);        
    }
    /*
    public static void processScheduleSub(List<OpportunityLineItem>  inputList, Map<String, SBQQ__Quote__c> mapQuoteFromOpp) {
        OpportunityScheduleCreationHandler.processScheduleSub(inputList, mapQuoteFromOpp);        
        system.debug('UIN'+lineItemsUpd);     
    } 
    */       
}