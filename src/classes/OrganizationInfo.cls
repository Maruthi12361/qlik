/**
 * 2019-12-09 FEH Caches organization info so that we only need to use a soql query once to load the information. 
 * In prod after the record is created it should only need to be updated if new fields are added this results in query reductions 
 * for code that needs to check if we're running in a sandbox, it also doesn't add any configuration steps as detection and creation 
 * of the record is based on org id and will be created on all sandboxes after the first run. 
 */
global class OrganizationInfo {
	
    global static Organization_Info__c getOrganizationInfo(){
    
        Organization_Info__c orgInfo = Organization_Info__c.getInstance(UserInfo.getOrganizationId());         
        if(orgInfo == null || orgInfo.isSandbox__c == null || orgInfo.Id == null || orgInfo.Id != orgInfo.CreatedInOrgId__c) {
            Organization o = [SELECT IsSandbox FROM Organization WHERE Id =: UserInfo.getOrganizationId()];
            orginfo = new Organization_Info__c(name = UserInfo.getOrganizationId(), isSandbox__c = o.isSandbox);
             //don't run if we're in test context 
            loadOrganizationRecord(); //do the dml in a future method to prevent it failing if there's an error futher in the transaction
        } 
        return orgInfo; 
    }

	@future
    global static void loadOrganizationRecord() {
        Organization o = [SELECT IsSandbox FROM Organization WHERE Id =: UserInfo.getOrganizationId()];
       	Organization_Info__c orgInfo = Organization_Info__c.getInstance(UserInfo.getOrganizationId()); 
        orgInfo.isSandbox__c = o.isSandbox;
        orgInfo.CreatedInOrgId__c = UserInfo.getOrganizationId();
        upsert orginfo; 
    }    
        
}