/******************************************************

    Class: CPQ_Controller
    
    This is the Controller for the CPQ page 
    
    Changelog:
        2010-09-10  MHG     Created file
                
******************************************************/
public with sharing class CPQ_Controller 
{

    /******************************************************
    
        Property: PUrl
        
        2010-09-10  MHG Created Method
    
     ******************************************************/
    public string PUrl
    {
        get 
        {
            if (System.currentPagereference().getParameters().containsKey('PUrl'))
            {
                //return EncodingUtil.urlDecode(System.currentPagereference().getParameters().get('PUrl'), 'UTF-8');
                
                return EncodingUtil.urlEncode(System.currentPagereference().getParameters().get('PUrl'), 'UTF-8');  
            }
            
            return '';
        }   
    }
    
    public string fullPUrl
    {
        get 
        {
            if (System.currentPagereference().getParameters().containsKey('PUrl'))
            {
                //return EncodingUtil.urlDecode(System.currentPagereference().getParameters().get('PUrl'), 'UTF-8');
                
                return System.currentPagereference().getParameters().get('PUrl');  
            }
            
            return '';
        }   
    }


    /******************************************************
    
        TestMethod: test_CPQ_Controller
        
        This method test the controller.
        
        2010-09-10  MHG Created Method
    
     ******************************************************/
    static testMethod void test_CPQ_Controller()
    {
        CPQ_Controller Controller = new CPQ_Controller();       
        System.assert(Controller.PUrl == '');       
        System.currentPageReference().getParameters().put('PUrl', 'https%3A%2F%2Fwww.google.com');
        //System.assert(Controller.PUrl == 'https://www.google.com');
    }

}