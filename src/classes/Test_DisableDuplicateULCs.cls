public with sharing class Test_DisableDuplicateULCs
{
	
}
/*
    30-12-2013 MTM  CR# 8336 Fix the duplicate ULC creation test class

@isTest(SeeAllData=true)
public with sharing class Test_DisableDuplicateULCs {

    private static List<ULC_Details__c> ulcs = new List<ULC_Details__c>();
    private static void SetUpTestData()
    {
        System.debug('Test_DisableDuplicateULCs: Starting');
        
        Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
                
        User testUser = Util.createUser('mattman', 'MyName', testProfile.Id, testUserRole.Id);
        insert testUser;
        System.runas(testUser)
        {
            QlikTech_Company__c company = [SELECT Id, QlikTech_Company_Name__c FROM QlikTech_Company__c WHERE Name = 'GBR' LIMIT 1];
                
            Account testAcc = new Account (
            OwnerId = testUser.Id,
            Name= 'Mattman',
            Navision_Customer_Number__c = '12345',
            QlikTech_Company__c = company.QlikTech_Company_Name__c,
            
            Billing_Country_Code__c = company.Id,
            BillingStreet = 'Mill street',
            BillingState ='Berkshire',
            BillingPostalCode = 'EM3',
            BillingCountry = 'United Kingdom',
            BillingCity = 'Reading',
            
            Shipping_Country_Code__c = company.Id,
            ShippingStreet = 'Mill street',
            ShippingState ='Berkshire',
            ShippingPostalCode = 'EM3',
            ShippingCountry = 'United Kingdom',
            ShippingCity = 'Reading'); 
            insert testAcc;
                
            Contact c = new Contact();
            c.FirstName = 'Junk';
            c.LastName = 'Boy';
            c.accountId = testAcc.Id;
            c.Email = 'mattman@junkmail.com';
            c.Allow_Partner_Portal_Access__c= false;
                
            insert c;
            
            ULC_Details__c ulc = new ULC_Details__c(ULCName__c = 'mattmanu', ULCStatus__c = 'Active', ContactId__c = c.Id);
            insert ulc;
            
            ulcs.Add(ulc);
        }
    }
    static testMethod void TestDisableDuplicateULCsBatch()
    {
        SetUpTestData();
        Test.StartTest();
        
        ULC_Details__c ulc1 = [select Id, ULCName__c, ULCStatus__c from ULC_Details__c where Id =: ulcs[0].Id];
        System.assertEquals(ulc1.ULCName__c, 'mattmanu');
        System.assertEquals(ulc1.ULCStatus__c, 'Active');
        
        DisableDuplicateULCs batch = new DisableDuplicateULCs();
        ID batchId = Database.executeBatch(batch);
        Test.StopTest();
        
        ULC_Details__c ulc2 = [select Id, ULCStatus__c from ULC_Details__c where Id =: ulcs[0].Id];
        System.assertEquals(ulc2.ULCStatus__c, 'Disabled');
        
    }
    
    static testMethod void testDisableDuplicateULCsSchedulable()
    {
        Test.StartTest();
        DisableDuplicateULCsSchedulable  schedule1 = new DisableDuplicateULCsSchedulable();
        string sch = '0 30 * * * ?';
        system.schedule('DisableDuplicateULCsSchedulable', sch, schedule1);
        Test.StopTest();
    } 
   
}*/