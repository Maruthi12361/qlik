/*
	Class: Test_multiEdit_EventPlan
	
	Changelog:
		2013-05-09	CCE		Initial development. Test class for multiEdit_ExecMapping_Controller and multiEdit_ExecMapping page.
		07.02.2017   RVA :   changing methods 							
*/
@isTest
private class Test_multiEdit_EventPlan {

    static testMethod void TestAddingNewRecord() {
		/*Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
			QTComp.Country_Name__c = 'United Kingdom';
		update QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
		
		Account_Plan__c TestAccountPlan = new Account_Plan__c(
			Account__c = TestPartnerAccount.Id,
			Name = 'TestAccPlan1',
			Approved_by_Sales_Productivity__c = true,
			Last_Review_Date__c = Date.Today()
		);
		insert TestAccountPlan;
		
		//and add a child object
		List<Event_Plan__c> EventPlans = new List<Event_Plan__c>();
		Event_Plan__c TestEventPlan1 = new Event_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Name = 'EventPlan1',
			Sales_Objective__c = 'Some stuff'
		);
		EventPlans.add(TestEventPlan1);
		
		Event_Plan__c TestEventPlan2 = new Event_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Name = 'EventPlan2',
			Sales_Objective__c = 'Some stuff'
		);
		EventPlans.add(TestEventPlan2);		
		insert EventPlans;
		
		test.startTest();
		PageReference pageRef = new PageReference('/apex/multiEdit_EventsPlan?accountPlanId=' + TestAccountPlan.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(TestAccountPlan);
		multiEdit_EventsPlan_Controller controller = new multiEdit_EventsPlan_Controller(StandardController);

		List<Event_Plan__c> multiList = controller.getEventsPlans();
		System.assert(multiList != null);
		//we should get our original 2 records plus the new blank one
		System.assert(multiList.size() == 3);	
		
		//Next we will modify an existing record
		Event_Plan__c existing = multiList[0];
		existing.Sales_Objective__c = 'Different stuff';
		// and edit the new blank record
		Event_Plan__c newPlan = multiList[2];
		newPlan.Name = 'A new event';
		newPlan.Sales_Objective__c = 'New stuff';
		controller.QuickSaveSet();	
		
		List<Event_Plan__c> newList = controller.getEventsPlans();
		System.assert(newList != null);
		//we should get our original 2 records plus the new one plus a new blank one
		System.assert(newList.size() == 4);
		//test we modified an existing record	
		Event_Plan__c testModify = newList[0];
		System.assert(testModify.Sales_Objective__c == 'Different stuff');				
				
		test.stopTest();
    }
    
    static testMethod void TestCancelingNewRecord() {
		/*Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
			QTComp.Country_Name__c = 'United Kingdom';
		update QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
		
		Account_Plan__c TestAccountPlan = new Account_Plan__c(
			Account__c = TestPartnerAccount.Id,
			Name = 'TestAccPlan1',
			Approved_by_Sales_Productivity__c = true,
			Last_Review_Date__c = Date.Today()
		);
		insert TestAccountPlan;
		
		//and add a child object
		List<Event_Plan__c> EventPlans = new List<Event_Plan__c>();
		Event_Plan__c TestEventPlan1 = new Event_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Name = 'EventPlan1',
			Sales_Objective__c = 'Some stuff'
		);
		EventPlans.add(TestEventPlan1);
		
		Event_Plan__c TestEventPlan2 = new Event_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Name = 'EventPlan2',
			Sales_Objective__c = 'Some stuff'
		);
		EventPlans.add(TestEventPlan2);		
		insert EventPlans;
		
		test.startTest();
		PageReference pageRef = new PageReference('/apex/multiEdit_EventsPlan?accountPlanId=' + TestAccountPlan.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(TestAccountPlan);
		multiEdit_EventsPlan_Controller controller = new multiEdit_EventsPlan_Controller(StandardController);

		List<Event_Plan__c> multiList = controller.getEventsPlans();
		System.assert(multiList != null);
		//we should get our original 2 records plus the new blank one
		System.assert(multiList.size() == 3);	
		
		//Edit the new blank record and then Cancel it
		Event_Plan__c newPlan = multiList[2];
		newPlan.Name = 'A new event';
		newPlan.Sales_Objective__c = 'New stuff';		//do the Cancel
		controller.CancelSet();	
		
		List<Event_Plan__c> newList = controller.getEventsPlans();
		System.assert(newList != null);
		//we should get our original 2 records plus a new blank one
		System.assert(newList.size() == 3);	
				
		test.stopTest();
    }
    
    static testMethod void TestSaveWithoutAddingNewRecord() {
		/*Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
			QTComp.Country_Name__c = 'United Kingdom';
		update QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
		
		Account_Plan__c TestAccountPlan = new Account_Plan__c(
			Account__c = TestPartnerAccount.Id,
			Name = 'TestAccPlan1',
			Approved_by_Sales_Productivity__c = true,
			Last_Review_Date__c = Date.Today()
		);
		insert TestAccountPlan;
		
		//and add a child object
		List<Event_Plan__c> EventPlans = new List<Event_Plan__c>();
		Event_Plan__c TestEventPlan1 = new Event_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Name = 'EventPlan1',
			Sales_Objective__c = 'Some stuff'
		);
		EventPlans.add(TestEventPlan1);
		
		Event_Plan__c TestEventPlan2 = new Event_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Name = 'EventPlan2',
			Sales_Objective__c = 'Some stuff'
		);
		EventPlans.add(TestEventPlan2);		
		insert EventPlans;
		
		test.startTest();
		PageReference pageRef = new PageReference('/apex/multiEdit_EventsPlan?accountPlanId=' + TestAccountPlan.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(TestAccountPlan);
		multiEdit_EventsPlan_Controller controller = new multiEdit_EventsPlan_Controller(StandardController);

		List<Event_Plan__c> multiList = controller.getEventsPlans();
		System.assert(multiList != null);
		//we should get our original 2 records plus the new blank one
		System.assert(multiList.size() == 3);	
		
		//Next we will modify an existing record
		Event_Plan__c existing = multiList[0];
		existing.Sales_Objective__c = 'Different stuff';
		//and Save the change
		controller.SaveSet();
		
		List<Event_Plan__c> newList = controller.getEventsPlans();
		System.assert(newList != null);
		//we should get our original 2 records plus a new blank one
		System.assert(newList.size() == 3);	
		
		//test we modified an existing record	
		Event_Plan__c testModify = newList[0];
		System.assert(testModify.Sales_Objective__c == 'Different stuff');			
				
		test.stopTest();
    }

}