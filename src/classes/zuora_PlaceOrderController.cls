/**
    Change log
*   11.08.2018 : UIN  IMP-872
    11.11.2018 : Linus Löfberg - IMP-903
**/  
public class zuora_PlaceOrderController {
    public Boolean status {
        get {
            return VCstatus;
        }
        private set;
    }

    public Boolean VCstatus {
        get {
            return ECustomsHelper.VCStatus(quote.Revenue_Type__c, quote.zqu__Account__r.ECUSTOMS__RPS_Status__c,
                                          quote.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c, quote.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,
                                          quote.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c);
        }
        private set;
    }

    public Boolean poRequired {
        get {
            return sellThroughPartner.PO_Required__c;
        }
        private set;
    }

    public zqu__Quote__c quote { get; set; }
    public ApexPages.StandardController controller;

    public Account endUserAccount, sellThroughPartner, secondpartner;
    private Opportunity opportunity;

    public zuora_PlaceOrderController(ApexPages.StandardController stdController) {
        quote = (zqu__Quote__c)stdController.getRecord();
        controller = stdController;

        quote = [SELECT Id, Name, Revenue_Type__c, ApprovalStatus__c, Partner_Special_Term_Request__c, zqu__Primary__c, Quote_Status__c, ACV__c, Qlik_TCV__c, zqu__Opportunity__r.Sell_Through_Partner__r.Name, zqu__Account__r.Name, CurrencyIsocode, zqu__Currency__c, zqu__Opportunity__r.StageName, zqu__Opportunity__r.Sell_Through_Partner__r.PO_Required__c, Undiscounted_Total__c, Is_Quote_Expired__c, Partner_Category__c, Partner_Level__c, zqu__Account__c, zqu__Account__r.ECUSTOMS__RPS_Status__c, zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c, zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c, zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c FROM zqu__Quote__c WHERE Id = :quote.Id];
    }
}