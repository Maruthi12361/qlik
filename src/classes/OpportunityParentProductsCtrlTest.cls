// CR 10681
// test class to test OpportunityParentProductsCtrl
// Change log:
// January 20, 2013 - Initial Implementation - Madhav Kakani - Fluido Oy
// NTE Febuary 12, 2014 - Changes made to replace SQL quries with static IDs to reduct the quey numbers.
//
//  2014-06-03  TJG     Fix test error
//                      System.DmlException: Insert failed. First exception on row 0; 
//                      first error: INVALID_CROSS_REFERENCE_KEY, Record Type ID: this ID value isn't valid for the user: 01220000000J1KRAA0: [RecordTypeId]
//	2016-06-10   roman@4front  runTest1 method updated to cover more code lines;
//	07.02.2017   RVA :   changing QT methods
//	24.03.2017 Rodion Vakulovskyi   
@isTest
private class OpportunityParentProductsCtrlTest{
    // test case without SOI in the parent
    static testMethod void runTest1() {
        

        
        Profile p = [select id from profile where name='System Administrator']; 
          User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hosurserr.com');

        insert mockSysAdmin;
        System.RunAs(mockSysAdmin) {   
                    

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            Country_Name__c = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = testSubs1.id
			);
        insert QTComp;
        QlikTech_Company__c qtc = new QlikTech_Company__c();
            qtc.name = 'SWE';
            qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
            qtc.Country_Name__c = 'Sweden';
            qtc.CurrencyIsoCode = 'SEK';
			qtc.Subsidiary__c = testSubs1.id;
        insert qtc;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'USA');
			QTComp.Country_Name__c = 'USA';
            QTComp.QlikTech_Company_Name__c = 'QlikTech Inc';
		update QTComp;
		QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
			qtc.Country_Name__c = 'Sweden';
            qtc.CurrencyIsoCode = 'SEK';
		update qtc;

        //Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Id testProfile = '00e20000000yyUz';
        //UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        Id testUserRole = '00E20000000vrIn';
            
        
            Account act = new Account(name='Test Account');
            act.OwnerId = mockSysAdmin.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
            
            // RecordType rt = [SELECT Id FROM RecordType WHERE Name='Qlikbuy CCS Standard'];
            //rt is opp record type id for 'Qlikbuy CCS Standard' record type
            
            //  Id rt = '01220000000J1KR'; Commented out on 2014-06-03
            Id rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;// QTNS '012f0000000Cugl'; //Qlikbuy CCS Standard II on QTNS
            //RecordType rtChild = [SELECT Id FROM RecordType WHERE Name='Deal Split Child'];
            //rtChild is opp record type id for "Deal Split Child" record type
            Id rtChild = '01220000000DoEj';
                
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            //Create test parent Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - Parent',
                Name = 'TestOpp - Parent',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = rt,
                CurrencyIsoCode = 'GBP',
                ForecastCategoryName = 'Omitted',
                Signature_Type__c = 'Digital Signature',
                AccountId = ct.AccountId
            );
           
            insert testNewOpp;
            
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;         
            insert LineItem;    
            
            system.assert(testNewOpp.Id != null);

            //Create test child Opportunity
            Opportunity testChildOpp = New Opportunity (
                Short_Description__c = 'TestOpp - Child',
                Name = 'TestOpp - Child',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Confirmed',
                RecordTypeId = rtChild,
                Deal_Split_Parent_Opportunity__c = testNewOpp.Id,
                Deal_Split_Percentage__c = 0.50,
                CurrencyIsoCode = 'GBP',
                ForecastCategoryName = 'Omitted',
                Signature_Type__c = 'Digital Signature',
                AccountId = ct.AccountId,
                Included_Products__c = 'Qlik Sense'
            );
               Test.startTest();
            insert testChildOpp;
             Test.stopTest();
			
            system.assert(testChildOpp.Id != null);

        
            ApexPages.StandardController ctrl = new ApexPages.StandardController(testChildOpp);
            OpportunityParentProductsCtrl oppctrl = new OpportunityParentProductsCtrl(ctrl);
            
            system.assert(oppctrl != null);
            system.assert(oppctrl.litems.size() == 1);
                                
             
        }
    } // runTest1()  

    // test case with an SOI in the parent
    static testMethod void runTest2() {
        
                Profile p = [select id from profile where name='System Administrator']; 
          User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        insert mockSysAdmin;
        System.RunAs(mockSysAdmin) {

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            Country_Name__c = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = testSubs1.id
			);
        insert QTComp;
        QlikTech_Company__c qtc = new QlikTech_Company__c();
            qtc.name = 'SWE';
            qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
            qtc.Country_Name__c = 'Sweden';
            qtc.CurrencyIsoCode = 'SEK';
			qtc.Subsidiary__c = testSubs1.id;
        insert qtc;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'USA');
			QTComp.Country_Name__c = 'USA';
            QTComp.QlikTech_Company_Name__c = 'QlikTech Inc';
		update QTComp;
		QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
			qtc.Country_Name__c = 'Sweden';
            qtc.CurrencyIsoCode = 'SEK';
		update qtc;

        // Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        // UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        Id testProfile = '00e20000000yyUz';
        Id testUserRole = '00E20000000vrIn';
        
        
            Account act = new Account(name='Test Account');
            act.OwnerId = mockSysAdmin.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';
            insert act;
                       
            Contact ct1 = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct1;
            Contact ct2 = new Contact(AccountId=act.Id,lastname='Contact2',firstname='Test');
            insert ct2;
                        
            //RecordType rt = [SELECT Id FROM RecordType WHERE Name='Qlikbuy CCS Standard'];
            //RecordType rtChild = [SELECT Id FROM RecordType WHERE Name='Deal Split Child'];
            //rt is opp record type id for 'Qlikbuy CCS Standard' record type
            //  Id rt = '01220000000J1KR'; Commented out on 2014-06-03
            Id rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;//QTNS '012f0000000Cugl'; //Qlikbuy CCS Standard II on QTNS
            //rtChild is opp record type id for "Deal Split Child" record type
            Id rtChild = '01220000000DoEj';
                
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            //Create test parent Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - Parent',
                Name = 'TestOpp - Parent',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = rt,
                CurrencyIsoCode = 'GBP',
                ForecastCategoryName = 'Omitted',
                Signature_Type__c = 'Digital Signature',
                AccountId = ct1.AccountId
            );
        
            insert testNewOpp;
            
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;         
            insert LineItem;    
            
            system.assert(testNewOpp.Id != null);

           
            
            // Add the contact to the parent opportunity SOI
            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = testNewOpp.Id;
            soi.Contact__c = ct1.Id;
            soi.Role__c = 'Coach'; 
            Test.startTest();
            insert soi;
            
            //Create test child Opportunity
            Opportunity testChildOpp = New Opportunity (
                Short_Description__c = 'TestOpp - Child',
                Name = 'TestOpp - Child',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Confirmed',
                RecordTypeId = rtChild,
                Deal_Split_Parent_Opportunity__c = testNewOpp.Id,
                Deal_Split_Percentage__c = 0.50,
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                AccountId = ct1.AccountId,
                Included_Products__c = 'Qlik Sense'
            );
          
            insert testChildOpp; // this should also copy the SOI from the parent
			
            system.assert(testChildOpp.Id != null);

            List<Sphere_of_Influence__c> childSOI = [SELECT Id FROM Sphere_of_Influence__c WHERE Opportunity__c = :testChildOpp.Id];
            system.assert(childSOI != null);
        //    system.assert(childSOI.size() == 1);
            
            // Add the second contact to the parent opportunity SOI
            // however the child opp should not get this soi as the child opp is already created
            soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = testNewOpp.Id;
            soi.Contact__c = ct2.Id;
            soi.Role__c = 'Coach';            
            insert soi;
         
               
            childSOI = [SELECT Id FROM Sphere_of_Influence__c WHERE Opportunity__c = :testChildOpp.Id];
             
            system.assert(childSOI != null);
      //      system.assert(childSOI.size() == 1); // not two soi's           
                       
            ApexPages.StandardController ctrl = new ApexPages.StandardController(testChildOpp);
            OpportunityParentProductsCtrl oppctrl = new OpportunityParentProductsCtrl(ctrl);
              Test.StopTest();
            system.assert(oppctrl != null);
          //  system.assert(oppctrl.litems.size() == 1);
                                
                
        }
    } // runTest2()      
} // class OpportunityParentProductsCtrlTest