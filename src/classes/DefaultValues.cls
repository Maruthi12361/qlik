/* 
 *  Copyright (c) 2018 Zuora, Inc.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of 
 *  this software and associated documentation files (the "Software"), to use copy, 
 *  modify, merge, publish the Software and to distribute, and sublicense copies of 
 *  the Software, provided no fee is charged for the Software.  In addition the
 *  rights specified above are conditioned upon the following:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  Zuora, Inc. or any other trademarks of Zuora, Inc.  may not be used to endorse
 *  or promote products derived from this Software without specific prior written
 *  permission from Zuora, Inc.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 *  ZUORA, INC. BE LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
 *
 *  IN THE EVENT YOU ARE AN EXISTING ZUORA CUSTOMER, USE OF THIS SOFTWARE IS GOVERNED
 *
 *  BY THIS AGREEMENT AND NOT YOUR MASTER SUBSCRIPTION AGREEMENT WITH ZUORA.
 */
 /*
 *Z_DefaultValues
 *Created by: Richard Bakare Updated on Nov 6th by Ariel Xiao
 *The purpose of this class is to default the values for new and amendment quotes
 */
global class DefaultValues extends zqu.CreateQuoteController.PopulateDefaultFieldValuePlugin{ 
   private static String FUNC_STR = 'DefaultValues: populateDefaultFieldValue: '; 
    
   global override void populateDefaultFieldValue(SObject record, zqu.PropertyComponentController.ParentController pcc){   
      if(!Test.isRunningTest()) {
         super.populateDefaultFieldValue(record, pcc); 
      }
      System.debug(FUNC_STR+'pcc: '+pcc);
      System.debug(FUNC_STR+'record: '+record);

      /*Nov 14th update: do not run this class when it is the maintain quote*/
      String QuoteIDString ='';
      QuoteIDString = ApexPages.currentPage().getParameters().get('id');
      Map<String, String> pageParams = ApexPages.currentPage().getParameters();
      String quoteType = pageParams.get('quoteType');
      String existSubId = pageParams.get('subscriptionId');

      System.debug(FUNC_STR+'Quote id is:'+QuoteIDString);

      List<zqu__Quote_Template__c> quoteTemplateList = [SELECT Id, Name FROM zqu__Quote_Template__c WHERE Name LIKE 'Quote Estimate Form'];
      System.debug(quoteTemplateList);

      if(String.isEmpty(QuoteIDString)){
         /** ---------------------- Quote Default Fields --------------------------------*/ 
         //Populate default values in the quote header  
         System.debug(FUNC_STR+record.get('zqu__SubscriptionType__c'));
         if(quoteType.equals('Subscription')||Test.isRunningTest() ){
            record.put('zqu__RenewalTerm__c', 12);
            record.put('zqu__InitialTerm__c', 36);  
         }else{
             System.debug(FUNC_STR+record.get('zqu__SubscriptionType__c')+',This is not a new subscritpiton.');
         }

         record.put('zqu__ValidUntil__c', Date.today().addDays(30));   
         
         if(record.get('Allow_Start_Date_Override__c') == null || !(Boolean)record.get('Allow_Start_Date_Override__c') || record.get('zqu__StartDate__c') == null) {
         	record.put('zqu__StartDate__c', Date.today());
         }   
         
         
         record.put('zqu__InvoiceTargetDate__c', Date.today());  
         record.put('zqu__PaymentMethod__c', 'Other'); 
         record.put('zqu__BillingBatch__c', 'Batch1');
         record.put('zqu__AutoRenew__c', TRUE);
         record.put('zqu__QuoteBusinessType__c', 'New'); 
         record.put('zqu__Primary__c', TRUE);
         record.put('Quote_Status__c', 'Open');
         record.put('Billing_Frequency__c', 'Annual');
         record.put('zqu__InvoiceSeparately__c', TRUE);
         

         record.put('Name', ' '); 
         //System.debug(FUNC_STR+'Billing_Frequency__c'+record.get('Billing_Frequency__c'));

         if (!String.isBlank(existSubId)){
            Zuora__Subscription__c[] subs = [SELECT Id, Billing_Frequency__c,Zuora__QuoteNumber__c,Zuora__TermEndDate__c    from  Zuora__Subscription__c where Zuora__External_Id__c =: existSubId limit 1];
            
            if(subs.size() > 0) {
            	Zuora__Subscription__c sub = subs[0];
	            record.put('Billing_Frequency__c', sub.Billing_Frequency__c);
	            record.put('Name',sub.Zuora__QuoteNumber__c);
	             if (quoteType == 'Renewal'){
	             	 if(record.get('Allow_Start_Date_Override__c') == null || !(Boolean)record.get('Allow_Start_Date_Override__c') || record.get('zqu__StartDate__c') == null) {
	                 	record.put('zqu__StartDate__c', sub.Zuora__TermEndDate__c); 
	             	 }
	                 record.put('zqu__InvoiceTargetDate__c', sub.Zuora__TermEndDate__c);  
	             }
            }
         }
         System.debug(FUNC_STR+'Billing_Frequency__c'+record.get('Billing_Frequency__c'));

         record.put('Subscription_Up_Lift__c','UseLatestProductCatalogPricing');
         if(!quoteTemplateList.isEmpty() && !Test.isRunningTest()){
            record.put('zqu__QuoteTemplate__c', quoteTemplateList[0].Id);
            // Before retrieving  the lookup  options, needs to populate the map first            
            super.setLookupOptions(pcc);            
            // Now retrieve the lookup component options            
            zqu.LookupComponentOptions quoteTemplate = super.getLookupOption('zqu__QuoteTemplate__c');            
            quoteTemplate.targetId = quoteTemplateList[0].Id;            
            quoteTemplate.targetName = quoteTemplateList[0].Name;         
            
         }
          
         /** ---------------------- Quote Invoice owner Default Fields --------------------------------*/
          List<Zuora__CustomerAccount__c> billingAccounts;
             
         /*Nov 6th: Invoice owner added:**/
         Id OpportunityId =(Id) record.get('zqu__Opportunity__c');
         System.debug('OppId:' + OpportunityId);
         
         List<opportunity> opps = [SELECT (Select Id,role,contactid From OpportunityContactRoles), CurrencyIsoCode,Revenue_Type__c,Sell_Through_Partner__c, id FROM Opportunity WHERE Opportunity.id=: OpportunityId]; 
         System.debug(opps[0].Sell_Through_Partner__c);
         System.debug(FUNC_STR+'THIS IS the opporutnity Revenue_Type__c:'+opps[0].Revenue_Type__c);
         System.debug(FUNC_STR+'Fulfillment.equalsIgnoreCase(opps[0].Revenue_Type__c)'+
            'Fulfillment'.equalsIgnoreCase(opps[0].Revenue_Type__c));

         /*Nov20th set the currency */
         if(opps.size() > 0)
            record.put('zqu__Currency__c', opps[0].CurrencyIsoCode); 


         if(opps.size() > 0 && !(String.isEmpty(opps[0].Sell_Through_Partner__c))){  

            // Set the Invoice Owner Information (From the soldToAccount) Details.
            billingAccounts= [SELECT Id, Name, Zuora__Account__c,Zuora__Zuora_Id__c
                                                               FROM Zuora__CustomerAccount__c 
                                                               WHERE Zuora__Account__c =: opps[0].Sell_Through_Partner__c 
                                                            ];
            if (billingAccounts.size() > 0 && (!String.isEmpty(billingAccounts[0].Zuora__Zuora_Id__c)) && !Test.isRunningTest()){
               //List soldToAccount = billingAccounts[0];
               record.put('zqu__InvoiceOwnerName__c', billingAccounts[0].Name);
               record.put('zqu__InvoiceOwnerId__c', billingAccounts[0].Zuora__Zuora_Id__c);
                // Client must call this method explicitly from the plugin to initialize Invoice Owner lookup.
               super.populateInvoiceOwner(pcc);
            }else{

               System.debug(FUNC_STR+'billing account info'+billingAccounts+ 'size of billingAccounts.size()'+billingAccounts.size()+'.It should be larger than 0.');
            }
            System.debug(FUNC_STR+' Sell_Through_Partner__c is:'+opps[0].Sell_Through_Partner__c +',zqu__InvoiceOwnerId__c'+record.get('zqu__InvoiceOwnerId__c'));
         }else{
            System.debug(FUNC_STR+'opps.size()'+opps.size()+'.It should be larger than 0.');
         }
         /** ---------------------- Quote Contacts Default Fields --------------------------------*/
         Id accountId = !opps[0].Revenue_Type__c.equals('Direct') ? opps[0].Sell_Through_Partner__c  : (Id) record.get('zqu__Account__c'); 
         List<Account> accounts = [SELECT Id,Name,Payment_Terms__c FROM Account WHERE Account.Id = :accountId];  

         System.debug(FUNC_STR+'Account Payment term:'+accounts[0].Payment_Terms__c);

         //record.put('zqu__PaymentTerm__c', accounts[0].Payment_Terms__c); 
         /*Transform from account payment term to Quote payment term*/
         if(accounts[0].Payment_Terms__c.equals('EONM')){
            record.put('zqu__PaymentTerm__c', 'End of Next Month'); 
         }
         else if(accounts[0].Payment_Terms__c.equals('0 days')){
             record.put('zqu__PaymentTerm__c', 'Due on receipt'); 
         }
         else{
            List<String> PaymentTermSplittedbySpace = accounts[0].Payment_Terms__c.split(' ');
            String paymentTermUpdate = 'Net '+PaymentTermSplittedbySpace[0]+' Days';
            System.debug(FUNC_STR+'Account Payment term:'+PaymentTermSplittedbySpace[0]+'. Payment term update:'+paymentTermUpdate );
            record.put('zqu__PaymentTerm__c', paymentTermUpdate); 
         }

         // Retrieve the account ID from the quote  
         /* Id accountId = (Id) record.get('zqu__Account__c');        
          
         // Find the contacts associated with the account        
         List<Contact>contacts = [SELECT Id, MailingCountry , Name FROM Contact WHERE Account.Id = :accountId];    
         zqu.LookupComponentOptions billToOptions;
         zqu.LookupComponentOptions soldToOptions;
         // Assuming the contacts are present set the billTo and soldTo to the first contact        
         if  (contacts.size() > 0) {            
            // System.debug('mp: about to add ' + contacts[0].Id + ' as a contact ID');            
            record.put('zqu__BillToContact__c', contacts[0].Id);            
            record.put('zqu__SoldToContact__c', contacts[0].Id);

            if (!Test.isRunningTest()){
               
               // Beforeretrieving  the lookup  options, needs to populate the map first            
               super.setLookupOptions(pcc);            
             
               // Now retrieve the lookup component options            
               billToOptions = super.getLookupOption('zqu__BillToContact__c');            
               soldToOptions  = super.getLookupOption('zqu__SoldToContact__c');            
               
            }else{
                soldToOptions = new zqu.LookupComponentOptions();
                billToOptions = new zqu.LookupComponentOptions();
            }    
            System.debug(FUNC_STR+'billToOptions'+billToOptions+'soldToOptions'+soldToOptions);


            billToOptions.targetId = contacts[0].Id;            
            billToOptions.targetName = contacts[0].Name; 
            


            soldToOptions.targetId = contacts[0].Id;            
            soldToOptions.targetName = contacts[0].Name;    

         }else{
            System.debug(FUNC_STR + 'contacts is null. Please check again.');
         }*/

         /*Nov 14th: #894 opps**/
         if(opps.size()>0&& opps[0].Revenue_Type__c.equals('Direct')) {
            System.debug(FUNC_STR+'THIS IS the oppContact:'+opps[0].OpportunityContactRoles);
            for(OpportunityContactRole oppROLE:opps[0].OpportunityContactRoles ){
                // Find the contacts associated with the account        
               List<Contact>contacts1 = [SELECT Id, Name FROM Contact WHERE id = :oppROLE.contactid AND (NOT Email LIKE '%@qlik.com') AND (NOT Email LIKE '%@qliktech.com') AND (NOT Email LIKE '%@qlikview.com')];  
               if(oppROLE.role != NULL && oppROLE.role.equals('Quote Recipient - Sold To')){
                  System.debug(FUNC_STR+oppROLE.role);
                  System.debug(FUNC_STR+oppROLE.role+'This is the soldTo id:'+oppROLE.contactid);
                  zqu.LookupComponentOptions billToOptions1;
                  zqu.LookupComponentOptions soldToOptions1;
                  // Assuming the contacts are present set the billTo and soldTo to the first contact        
                  if  (contacts1.size() > 0) {            
                     // System.debug('mp: about to add ' + contacts[0].Id + ' as a contact ID');            
                    
                     record.put('zqu__SoldToContact__c', contacts1[0].Id);

                     if (!Test.isRunningTest()){
                        
                        // Beforeretrieving  the lookup  options, needs to populate the map first            
                        super.setLookupOptions(pcc);            
                      
                        // Now retrieve the lookup component options            
                        billToOptions1  = super.getLookupOption('zqu__BillToContact__c');            
                        soldToOptions1  = super.getLookupOption('zqu__SoldToContact__c');            
                     }else{
                         soldToOptions1 = new zqu.LookupComponentOptions();
                         billToOptions1  = new zqu.LookupComponentOptions(); 
                     }    
                     System.debug(FUNC_STR+'soldToOptions'+soldToOptions1);
                   //  System.debug(FUNC_STR+'billToOptions'+billToOptions1);


                     /*if(String.isBlank(billToOptions1.targetName)){
                        billToOptions1.targetId = contacts1[0].Id;            
                        billToOptions1.targetName = contacts1[0].Name; 
                     }*/

                     soldToOptions1.targetId = contacts1[0].Id;            
                     soldToOptions1.targetName = contacts1[0].Name;    

                  }else{
                     System.debug(FUNC_STR + 'contacts is null. Please check again.');
                  }
               }else if(oppROLE.role != NULL && oppROLE.role.equals('Invoice Recipient - Bill To')){
                  zqu.LookupComponentOptions billToOptions1;
                  // Assuming the contacts are present set the billTo and soldTo to the first contact        
                  if  (contacts1.size() > 0) {            
                     // System.debug('mp: about to add ' + contacts[0].Id + ' as a contact ID');            
                     record.put('zqu__BillToContact__c', contacts1[0].Id);            
                     if (!Test.isRunningTest()){
                        // Beforeretrieving  the lookup  options, needs to populate the map first            
                        super.setLookupOptions(pcc);            
                        // Now retrieve the lookup component options            
                        billToOptions1 = super.getLookupOption('zqu__BillToContact__c');            
                     }else{
                        billToOptions1 = new zqu.LookupComponentOptions();
                     }    
                     System.debug(FUNC_STR+'billToOptions'+billToOptions1);
                     billToOptions1.targetId = contacts1[0].Id;            
                     billToOptions1.targetName = contacts1[0].Name; 
                  }else{
                     System.debug(FUNC_STR + 'contacts is null. Please check again.');
                  }
               }else{
                  System.debug(FUNC_STR + 'role has the problem.');
               }
            }
         }else if(opps.size()>0 && (
            'Reseller'.equalsIgnoreCase(opps[0].Revenue_Type__c)|| 
            'Distributor'.equalsIgnoreCase(opps[0].Revenue_Type__c)||
            'Fulfillment'.equalsIgnoreCase(opps[0].Revenue_Type__c)
            )){
            System.debug(FUNC_STR+'THIS IS the oppContact:'+opps[0].OpportunityContactRoles);
            System.debug(FUNC_STR+'THIS IS the opporutnity Revenue_Type__c:'+opps[0].Revenue_Type__c);
            
            Id SoldTocontactid;
            Id BuildTocontactid;
            System.debug(FUNC_STR+'opps[0].OpportunityContactRoles size'+opps[0].OpportunityContactRoles.size());
            if(opps[0].OpportunityContactRoles.size()>1){
               for(OpportunityContactRole oppROLE:opps[0].OpportunityContactRoles ){
                  List<Contact> contacts1 = [SELECT Id, Name FROM Contact WHERE id = :oppROLE.contactid];  
                  if(oppROLE.role != NULL && oppROLE.role.equals('Quote Recipient - Sold To')){
                     System.debug(FUNC_STR+oppROLE.role+'This is the soldTo id:'+oppROLE.contactid);
                     SoldTocontactid =(id)oppROLE.contactid;

                     zqu.LookupComponentOptions soldToOptions1;
                     // Assuming the contacts are present set the billTo and soldTo to the first contact        
                     if (contacts1.size() > 0) {            
                        // System.debug('mp: about to add ' + contacts[0].Id + ' as a contact ID');            
                        record.put('zqu__SoldToContact__c', contacts1[0].Id);
                        if (!Test.isRunningTest()){
                           // Beforeretrieving  the lookup  options, needs to populate the map first            
                           super.setLookupOptions(pcc);            
                           // Now retrieve the lookup component options            
                           soldToOptions1  = super.getLookupOption('zqu__SoldToContact__c');            
                        }else{
                           soldToOptions1 = new zqu.LookupComponentOptions();
                        }    
                        System.debug(FUNC_STR+'soldToOptions'+soldToOptions1);
                        soldToOptions1.targetId = contacts1[0].Id;            
                        soldToOptions1.targetName = contacts1[0].Name;    
                     }else{
                        System.debug(FUNC_STR + 'contacts is null. Please check again.');
                     }
                  }else if(oppROLE.role != NULL && oppROLE.role.equals('Invoice Recipient - Bill To')){
                     zqu.LookupComponentOptions billToOptions1;
                     // Assuming the contacts are present set the billTo and soldTo to the first contact        
                     if  (contacts1.size() > 0) {            
                        // System.debug('mp: about to add ' + contacts[0].Id + ' as a contact ID');            
                        record.put('zqu__BillToContact__c', contacts1[0].Id);            
                        if (!Test.isRunningTest()){                         
                           // Beforeretrieving  the lookup  options, needs to populate the map first            
                           super.setLookupOptions(pcc);            
                           // Now retrieve the lookup component options            
                           billToOptions1 = super.getLookupOption('zqu__BillToContact__c');                
                        }else{
                           billToOptions1 = new zqu.LookupComponentOptions();
                        }    
                        System.debug(FUNC_STR+'billToOptions'+billToOptions1);
                        billToOptions1.targetId = contacts1[0].Id;            
                        billToOptions1.targetName = contacts1[0].Name; 
                     }else{
                        System.debug(FUNC_STR + 'contacts is null. Please check again.');
                     }
                  }
               }
            }else if(opps[0].OpportunityContactRoles.size() == 1){
               for(OpportunityContactRole oppROLE:opps[0].OpportunityContactRoles ){
                  List<Contact>contacts1 = [SELECT Id, Name FROM Contact WHERE id = :oppROLE.contactid];  
                  zqu.LookupComponentOptions billToOptions;
                  zqu.LookupComponentOptions soldToOptions;
                  // Assuming the contacts are present set the billTo and soldTo to the first contact        
                  if (contacts1.size() > 0) {            
                     // System.debug('mp: about to add ' + contacts[0].Id + ' as a contact ID');            
                     record.put('zqu__BillToContact__c', contacts1[0].Id);            
                     record.put('zqu__SoldToContact__c', contacts1[0].Id);
                     if (!Test.isRunningTest()){
                        // Beforeretrieving  the lookup  options, needs to populate the map first            
                        super.setLookupOptions(pcc);            
                        // Now retrieve the lookup component options            
                        billToOptions = super.getLookupOption('zqu__BillToContact__c');            
                        soldToOptions  = super.getLookupOption('zqu__SoldToContact__c');            
                     }else{
                        soldToOptions = new zqu.LookupComponentOptions();
                        billToOptions = new zqu.LookupComponentOptions();
                     }    
                     System.debug(FUNC_STR+'billToOptions'+billToOptions+'soldToOptions'+soldToOptions);
                     billToOptions.targetId = contacts1[0].Id;            
                     billToOptions.targetName = contacts1[0].Name; 
                     soldToOptions.targetId = contacts1[0].Id;            
                     soldToOptions.targetName = contacts1[0].Name;    
                  }else{
                     System.debug(FUNC_STR + 'contacts is null. Please check again.');
                  } 
               }
            }
         }else{
            System.debug(FUNC_STR+', the quote id is already there.');
         }
      }
   }
}