/*******************************************************************
* Class EmailAttachToCR
*
* Class controlling the creation of a note when an email is sent to cr@qlikview.com when the subject line contains CR#
* 
* Change Log:
* 2012-10-15   Fluido (Alberto de Diego)	 	Initial Development
*												CR# 4581 https://eu1.salesforce.com/a0CD000000J53L3?srPos=0&srKp=a0C
* 2012-11-19   RDZ  							CR# 6685 Add sender and receiver info on "Email to CR" notes
*												https://eu1.salesforce.com/a0CD000000U6yt1
* 2012-12-19   CCE  							CR# 6704 #DEV Fix 80 chars subject limit on "mail to CR" feature
*												https://eu1.salesforce.com/a0CD000000U75nj
* 2014-01-22   AIN								CR# 6848 #DEV Extend chars body limit on "mail to CR" feature
*												https://eu1.salesforce.com/a0CD000000U7fDq
**********************************************************************/
global class EmailAttachToCR implements Messaging.InboundEmailHandler {
	
	private string END_OF_LINE = '\r\n';
	
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        result.success = true;
        
        String emailSubjectTitle = email.Subject.abbreviate(80);	//CCE CR 6704
        String emailSubject= email.Subject.trim();
        List<String> strings = emailSubject.split(' ');
        String crName = extractCRFromTitle(emailSubject);
        if (crName == '') return result;
        
        List<SLX__Change_Control__c> changeControls = [SELECT Id FROM SLX__Change_Control__c WHERE CR_Number2__c = :crName];
        if (changeControls.size() <= 0) return result;
        
        //CR# 6685 Add sender and receiver info on "Email to CR" notes
        String sender = envelope.FromAddress;
        String fromAddress = email.fromAddress != null?'From: ' + email.fromAddress + END_OF_LINE:'';
        String toAddresses = email.toAddresses != null?'To: ' + email.toAddresses + END_OF_LINE:'';
        String ccAddresses = email.ccAddresses != null?'CC: ' + email.ccAddresses + END_OF_LINE:'';
        String allRecipients = toAddresses + ccAddresses + END_OF_LINE;
        List<User> users = [SELECT Id FROM User WHERE Email = :sender AND IsActive = true];
        
        
        //AIN CR# 6848 Max body length can be no longer than 32.000 characters, body is cropped to 32000 chars, also includes elipses (...) to show truncated text
        string noteBody = (fromAddress + allRecipients + email.plainTextBody).abbreviate(32000);
        Note note = new Note(Title = emailSubjectTitle, ParentId = changeControls[0].Id, Body = noteBody, 
        					 OwnerId = users.size() > 0 ? users[0].Id : UserInfo.getUserId());
       	//CCE CR 6704 created new string which gets cropped to 80 chars & used to populate the title field, also includes elipses (...) to show truncated text        
        /*Note note = new Note(Title = emailSubjectTitle, ParentId = changeControls[0].Id, Body = fromAddress + allRecipients + email.plainTextBody, 
        					 OwnerId = users.size() > 0 ? users[0].Id : UserInfo.getUserId());*/    
        /*Note note = new Note(Title = email.Subject, ParentId = changeControls[0].Id, Body = fromAddress + allRecipients + email.plainTextBody, 
        					 OwnerId = users.size() > 0 ? users[0].Id : UserInfo.getUserId());*/
        insert note;
        
        createAttachments(changeControls[0].Id, email.binaryAttachments);
        createAttachments(changeControls[0].Id, email.textAttachments);
        
        return result;
    }
    
    private static String extractCRFromTitle(String s) {
    	Pattern p = Pattern.compile('CR# [0-9]+');
		Matcher m = p.matcher(s);
		
		return m.find() ? m.group(0) : '';		
    }    
    
    private static void createAttachments(String parentId, Messaging.InboundEmail.BinaryAttachment[] binaryAttachments) {
        if (binaryAttachments == null) return;
        for (integer i = 0; i < binaryAttachments.size(); i++) {
            createAttachment(parentId, binaryAttachments[i].filename,
                             binaryAttachments[i].body);
        }
    }
    
    private static void createAttachments(String parentId, Messaging.InboundEmail.TextAttachment[] textAttachments) {
        if (textAttachments == null) return;
        for (integer i = 0; i < textAttachments.size(); i++) {
            createAttachment(parentId, textAttachments[i].filename,
                             Blob.valueOf(textAttachments[i].body));
        }
    }
    
    private static void createAttachment(String id, String filename, Blob body) {
        insert new Attachment(ParentId = id, Name = filename, Body = body);
    }    
            
}