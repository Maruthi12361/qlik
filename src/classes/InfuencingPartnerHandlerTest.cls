/******************************************************

        InfuencingPartnerHandlerTest
        Kumar Navneet
        Date - 04/21/17

        Changelog:
        05/01/2018 Pramod Kumar V  Test class coverage

    ******************************************************/

@isTest
private class InfuencingPartnerHandlerTest {

    public static TestMethod void TEST_InfuencingPartnerHandler()
    {
        QTTestUtils.GlobalSetUp();

        Subsidiary__c  sub = new Subsidiary__c (
            Name = 'test',
            Legal_Entity_Name__c = 'Test'

        );
         insert sub;
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',Subsidiary__c = sub.id
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        //Id AccRecordTypeId_OEMEndUserAccount = '012D0000000KBYx'; //OEM End User
        Account TestAccount = new Account(
            Name = 'ULC Test Ltd.',
            Navision_Status__c = 'Customer',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Commercial - Lower SMB'
        );
        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Commercial - Lower SMB'                            
        );
        List<Account> AccToInsert = new List<Account>();
        AccToInsert.Add(TestAccount);
        AccToInsert.Add(TestPartnerAccount);
        insert AccToInsert;

        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partnerr',
            LastName = 'PPr',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact;

        Contact PartnerContact1 = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact1;

        test.startTest();

        Opportunity Opp = new Opportunity();
        Opp.AccountId = TestAccount.Id;
        Opp.Payout_Partner_Contact__c = PartnerContact.Id;
        Opp.PRM_Referring_Contact__c = PartnerContact1.Id;
        Opp.Standard_Referral_Margin__c = '10';
        Opp.Partner_Fee_Type__c = 'Influence Fee';
        Opp.Short_Description__c = 'My Test Description';
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'USD';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Identified';
        Opp.Partner_Contact__c = PartnerContact.Id;
        insert Opp;


        Influencing_Partner__c IPC= new Influencing_Partner__c();
        IPC.Opportunity__c = Opp.Id;
        IPC.Influencing_Partner_Contact__c= PartnerContact1.id ;
        insert IPC;

        test.stopTest();

    }
}