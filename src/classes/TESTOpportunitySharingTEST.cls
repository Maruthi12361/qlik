public class TESTOpportunitySharingTEST {
 
    public static void UpdateOpportunitySharing1()
    {
		List<Opportunity> oppIds = [
            SELECT 	Id
            FROM Opportunity
            WHERE Name like 'N.Novgorod%'
        ];
        
        System.debug('======>Update Opportunity Sharing, oppIds.size() = ' + oppIds.size());
        Integer oppIdsSize = oppIds.size();
        
        List<Opportunity> opps4Ids = [
            SELECT
                Short_Description__c,
                Name,
                StageName,
                AccountId,
                Sell_Through_Partner__c,
                OwnerId,
                Owner.AccountId
            FROM
                Opportunity
            WHERE
                Id IN :oppIds 
        ];
		Integer opps4IdsSize = opps4Ids.size();
        
        List<Id> endUsers = new List<Id>();        
        List<Id> sellTPs  = new List<Id>();
        

        for (Opportunity opp : opps4Ids)
        {
            endUsers.add(opp.AccountId);
            sellTPs.add(opp.Sell_Through_Partner__c);
        }
        Integer endUsersSize = opps4Ids.size();
        Integer sellTPsSize = opps4Ids.size();
        
        System.debug('endUsers size == ' + endUsers.size());
        System.debug('sellTPs size == ' + sellTPs.size());

        List<Opportunity> allOpps = [
            SELECT
                Short_Description__c,
                Name,
                StageName,
                AccountId,
                Sell_Through_Partner__c,
                OwnerId,
                Owner.AccountId
            FROM
                Opportunity
            WHERE
                Id IN :oppIds
                OR Sell_Through_Partner__c IN :sellTPs
                OR AccountId IN :endUsers
        ];
        Integer allOppssSize = allOpps.size();
        System.debug('allOpps size == ' + allOpps.size());


    }
}