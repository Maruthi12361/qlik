/************************************************************************
 * @author  SAN
 * 2014-10-30 customer portal project
 *  
 * Test Class for QS_EnvironmentController_Test.
 *
 * Change log:
 *
 * 2018-01-10  ext_vos CHG0032031: add "test_method_to_coverage" method to increase the code coverage.
 * 2018-01-16  ext_vos CHG0031626: update "test_methods" method for work as Partner user.
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 *************************************************************************/

@isTest
private class QS_EnvironmentController_Test
{
	@isTest
	static void test_Constructor()
	{
		// creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

		//Create Contacts
		Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
		testContact.Persona__c = 'Decision Maker';
		testContact.LeadSource = 'leadSource';
		insert testContact;
		Contact testContact2 = TestDataFactory.createContact('test_FName', 'test_LName2', 'testSandbox2@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact2.Persona__c = 'Decision Maker';
        insert testContact2;
        Contact testContact3 = TestDataFactory.createContact('test_FName', 'test_LName3', 'testSandbox3@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact3.Persona__c = 'Decision Maker';
        insert testContact3;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

		// Create Community Users
		User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;



		// so far we are doing it for code coverage only so we can deploy it to qa/live
		// need proper unit test later
		QS_EnvironmentController environmentController = new QS_EnvironmentController();
		environmentController.Create();
		environmentController.cancelNewEnvironment();
		environmentController.CreateNewClicked();
		environmentController.ExpandClicked();
		Boolean tempView = environmentController.TempCPVIEW;
		Boolean indirectCust = environmentController.isIndirectCustomer;
		Boolean renderCreateButtonFlag = environmentController.RenderCreateBotton;
		

		environmentController.caseWizardRef = new QS_CaseWizard_Controller();
		

		system.RunAs(communityUser)
		{
			//Done inside as CaseController needs the active users contact
			environmentController.caseDetailRef = new QS_CaseController();

			environmentController.setEnviron();

			Environment__c test = environmentController.Environment;
		
			String testStr = environmentController.MyAccName;
			Id testId = environmentController.MyAccountId;
		
			List<Environment__c> testEnvList = environmentController.GetEnvironments();
			List<EnvironmentProduct__c> testenvProdList = environmentController.getEnvironmentProductList();
			testStr = environmentController.EnvironmentName;
			//environmentController.resultsAllEnvironment = testEnvList;
			environmentController.getresultsTestEnvironment();
			environmentController.getresultsDevEnvironment();
			environmentController.selectedEnvironment = test.id;
			environmentController.DeleteEnvironment();
			String accForCoverage = environmentController.Accounts;
			String entitlementsForCovarage = environmentController.Entitlements;
		}
	}

	@isTest
	static void test_methods() {
		QTTestUtils.GlobalSetUp();
	
		Subsidiary__c subs = new Subsidiary__c(Legal_Entity_Name__c = 'test', Netsuite_Id__c = 'test', Legal_Country__c = 'France');        
        insert subs;
        QlikTech_Company__c QTComp = new QlikTech_Company__c(Name = 'USA', QlikTech_Company_Name__c = 'QlikTech Inc', Subsidiary__c = subs.Id);
        insert QTComp;  
                
        List<Profile> ps = [select name from Profile 
                                where Profile.UserLicense.Name like '%Partner%' and 
                                Name like 'PRM%' and 
                                name != 'PRM - BASE' and
                                (not name like '%READ ONLY%') and
                                name != 'PRM - Base JAPAN' and
                                name != 'PRM External Content User'];
        System.assert(ps.Size() > 0, 'No profile found with Partner user license');

        User partnerUser = QTTestUtils.createMockUserForProfile(ps[0].Name);
        partnerUser = [select id, name, contact.Id, contact.Name from user where id = :partnerUser.id];

        //Sets the owner so the user has access to its contact
        Contact cu = [SELECT Id, OwnerId, AccountId, Name FROM Contact WHERE Id =: partnerUser.ContactId];
        cu.OwnerId = partnerUser.Id;
        update cu;

        Id lundBusinesshours = [select Id from BusinessHours where Name = 'Lund' limit 1].Id;
        Support_Office__c cancunSupportOffice = new Support_Office__c(Name = 'Cancun', Regional_Support_Manager__c = partnerUser.Id, Business_Hours__c = lundBusinesshours);
        
        Account partnerAcc = [select Id, Name from Account where Id = :cu.AccountId];
        partnerAcc.Support_Office__c = cancunSupportOffice.Id;
        update partnerAcc;
        
        
		test.startTest();
		System.runAs(partnerUser) {

			Account_License__c accLic = new Account_License__c(Name = '123321123321123', INT_NetSuite_Support_Level__c = 'Basic', Account__c = partnerAcc.Id,
	                                            Support_From__c = Date.Today(), Support_To__c =  Date.Today() + 1,  INT_NetSuite_InternalID__c = 'testtest',
	                                            Support_Provided_By__c = partnerAcc.Id);
	        insert accLic;

			Entitlement productLicense = [select Id from Entitlement where Account_License__c =: accLic.Id limit 1];       
	        //Create Environment
	        Environment__c environment = TestDataFactory.createEnvironment(partnerAcc.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'Production', 'version');
	        insert environment;

			PageReference pageRef = Page.QS_EnvironmentListPage;

			QS_EnvironmentController environmentController = new QS_EnvironmentController();
			environmentController.fromEnvironmentListPage = true;
			environmentController.GetEnvironments();
			QS_EnvironmentController.getLicenseList();
			environmentController.selectedAccountName = partnerAcc.Id;
			environmentController.env.Name = 'Test_Environment_Partner';
			QS_EnvironmentController.selectedLicense = productLicense.Id;
			environmentController.env.Product__c = 'test_product';
			environmentController.FilterAccounts();
			environmentController.FilterLicenses();
			
			environmentController.Create();
			environmentController.selectedEnvironment = environmentController.env.Id;
			environmentController.DeleteEnvironment();
			system.debug(environmentController.TempISCPLOG);
			system.debug(environmentController.accountListSize);
			system.debug(environmentController.licenseListSize);
		
		}
		test.stopTest(); 
	}

	@isTest
	static void test_method_to_coverage() {

		// creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.Navision_Status__c = 'Customer';
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
                
        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', 'testname');
        insert productLicense;
        Entitlement productLicense2 = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', 'testname2');
        insert productLicense2;
        
		test.startTest();
		System.runAs(communityUser) {			
			//Create Environment
        	Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        	insert environment;

			PageReference pageRef = Page.QS_EnvironmentListPage;
			QS_EnvironmentController environmentController = new QS_EnvironmentController();
			environmentController.selectedEnvironment = environment.Id;
			// test delete
			environmentController.DeleteEnvironment();
			List<Environment__c> envs = [SELECT Id from Environment__c where Id =:environment.Id];
			system.assertEquals(0, envs.size());
			
			//Create new environment
        	Environment__c environment1 = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        	insert environment1;
        	// call some non-covered methods
        	QS_EnvironmentController.selectedLicense = productLicense.Id;
			environmentController.selectedEnvironment = environment1.Id;
			environmentController.selectedAccountName = testAccount.Id;
			environmentController.env.Name = 'Test_Environment_Partner';
			environmentController.env.Type__c = 'Test';
			environmentController.env.Product__c = 'Architecture';
			environmentController.Create();			
			environmentController.FilterAccounts();
			environmentController.refreshLicenseList();
			environmentController.ExpandClicked();
			environmentController.cancelNewEnvironment();
			// set non-called variables
			Boolean flag = environmentController.RenderCreateBotton;
			environmentController.RenderCreateBotton = true;
			flag = environmentController.TempCPVIEW;
			environmentController.TempCPVIEW = false;
			environmentController.TempISCPLOG = false;
			environmentController.objUser = new User();
			flag = environmentController.isIndirectCustomer;
			environmentController.isIndirectCustomer = false;
			environmentController.IsPartner = false;
			environmentController.accountList = new List<Account>();
			environmentController.accountOptionList = new List<SelectOption>();
			environmentController.licenseOptionList = new List<SelectOption>();
			environmentController.Accounts = null;
			environmentController.Entitlements = null;
		}
		test.stopTest();
	}
}