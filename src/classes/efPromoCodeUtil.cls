//*********************************************************/
// Author: Mark Cane&
// Creation date: 19/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efPromoCodeUtil{
	private efRegistrationManager regMan;
	private String promoCode='';
	private String promoMessage='';
	private List<efProductWrapper> promoCodeList= new List<efProductWrapper>();
	private Double promoDiscountAmount = 0.0;
	private String attendeeType='';
	private String subAttendeeType='';
	private String searchAttendeeTypes = '';
	private Boolean promoCodeExecuted = false;		
	private Boolean promoCodesupdated = false;	
	private Boolean registrationPromoApplied = false;
	private Boolean educationPromoApplied = false;
	
	public efPromoCodeUtil(efRegistrationManager regMan){
		this.regMan = regMan;
				
		if(!efUtility.isNull(regMan.getRegistrationWrapper().getRegistration().portal_attendee_type__c))
			attendeeType = regMan.getRegistrationWrapper().getRegistration().portal_attendee_type__c;
		if(!efUtility.isNull(regMan.getRegistrationWrapper().getRegistration().attendee_subtype__c))
			subAttendeeType = regMan.getRegistrationWrapper().getRegistration().attendee_subtype__c;
				
		searchAttendeeTypes = attendeeType;
		if(attendeeType.equals('Attendee'))
			searchAttendeeTypes = subAttendeeType;
		if(attendeeType.equals('Sponsor'))
			searchAttendeeTypes = subAttendeeType;

        if (!efUtility.isNull(regMan.getRegistrationWrapper().getRegistration().PromoCode__c)){
            promoCode = regMan.getRegistrationWrapper().getRegistration().PromoCode__c;
            getPromoCodeList(100000, 0, false);
        }
	}
	
	public void reInitialise(){
		promoCodesupdated = false;	
		registrationPromoApplied = false;
		educationPromoApplied = false;		
		promoCodeExecuted = false;
		promoCode='';
		promoMessage='';		
		promoCodeList= new List<efProductWrapper>();
	} 
	
	public String getPromoCode(){
		return this.promoCode; 
	}
	
	public void setPromoCode(String promoCode){
		this.promoCode = promoCode;
	}
	
	public void setPromoMessage(String m){
		promoMessage = m;
	}
	
	public String getPromoMessage(){
		return promoMessage;
	}
	
	public List<efProductWrapper> getPromoCodeListObject(){
		return promoCodeList;
	}
	
	public boolean getPromoCodeExecuted(){
		return this.promoCodeExecuted; 
	}
	
	public void setPromoCodeExecuted(Boolean val){
		promoCodeExecuted = val; 
	}	
	    
	public List<efProductWrapper> getPromoCodeList(Double regAmount, Double classAmount, Boolean isClassesSelected){		
		promoMessage = '';
		Double promoAmount = 0.0;
		String pCode = getPromoCode();
		
		setRegistrationPromoApplied();
		
		if(!efUtility.isNull(pCode)){
			pCode = pCode.trim();
			efProductWrapper pProducts = getPromoProduct(pCode);

			if(pProducts==null){
				promoMessage = efConstants.PROMO_MESSAGE_CANNOT_APPLY;
				return promoCodeList;
			}else if (pProducts.getProduct().Promo_code_type__c.equals(efConstants.PROMO_TYPE_REGISTRATION) &&
			         registrationPromoApplied){
				promoMessage = efConstants.PROMO_MESSAGE_USED; 
				return promoCodeList;
			} else if (pProducts.getProduct().Promo_code_type__c.equals(efConstants.PROMO_TYPE_EDUCATION) &&
			         educationPromoApplied) {
				promoMessage = efConstants.PROMO_MESSAGE_USED;
				return promoCodeList;
			} else if(existsInList(pProducts)){
				promoMessage = efConstants.PROMO_MESSAGE_ALREADY_ADDED;					
				return promoCodeList;
			} else{
				Product2 p = pProducts.getProduct();
				String promoCodeType = p.Promo_code_type__c;
					
				double totalRegAmt = getBalanceRegistrationAmount(regAmount);
				double totalClassAmt = getBalanceClassesAmount(classAmount);
				double discountAmount = pProducts.getProduct().Discounts_Allowed__c;
				String discountType = p.Discount_Type__c;
					
				if(!efUtility.isNull(discountType) && discountType.equals('Percent') && discountAmount>100){
					promoMessage = efConstants.PROMO_MESSAGE_DISCOUNT_GREATER;							
					return promoCodeList; 
				}
				
				if(promoCodeType.equals('Registration')){
					if(totalRegAmt<1){
						promoMessage = efConstants.PROMO_MESSAGE_DISCOUNT_GREATER;
						return promoCodeList; 
					} else if(!efUtility.isNull(discountType) && discountType.equals('Percent')){
						double amt = -(totalRegAmt * (discountAmount/100));
						pProducts.setCalculatedDiscountAmount(efUtility.formatDecimals(String.valueOf(amt)));	
					}
					else if(!efUtility.isNull(discountType) && discountType.equals('Amount')){
						if(totalRegAmt<(discountAmount * -1)){
							promoMessage = efConstants.PROMO_MESSAGE_DISCOUNT_GREATER;
							return promoCodeList; 
						}
						pProducts.setCalculatedDiscountAmount(efUtility.formatDecimals(String.valueOf(discountAmount)));	
					}	
					
					promoCodeList.add(pProducts);
					registrationPromoApplied = true;
					setPromoCode('');
				} else if(promoCodeType.equals('Education')){
					if(!isClassesSelected){
						promoMessage = efConstants.PROMO_MESSAGE_NO_TRAINING;								
						return promoCodeList; 
					} else if(totalClassAmt<1){
						promoMessage = efConstants.PROMO_MESSAGE_TRAINING_DISCOUNT_GREATER;								
						return promoCodeList;
					} else if(!efUtility.isNull(discountType) && discountType.equals('Percent')){
						double amt = -(totalClassAmt * (discountAmount/100));
						pProducts.setCalculatedDiscountAmount(efUtility.formatDecimals(String.valueOf(amt)));	
					} else if(!efUtility.isNull(discountType) && discountType.equals('Amount')){	
						if(totalClassAmt<(discountAmount * -1)){
							promoMessage = efConstants.PROMO_MESSAGE_TRAINING_DISCOUNT_GREATER;						
							return promoCodeList; 
						}
						pProducts.setCalculatedDiscountAmount(efUtility.formatDecimals(String.valueOf(discountAmount)));	
					}
					promoCodeList.add(pProducts);
					educationPromoApplied = true;
					setPromoCode('');
				}					
			}
		} else{
			promoMessage='';
		}
		setPromoCode('');
		return promoCodeList;
	}
	
	public efProductWrapper getPromoProduct(String promoCode){ 
		if(!efUtility.isNull(promoCode)){
			
			Product2[] products = [Select Id,
			                              Name,
			                              Used__c,
			                              Discount_Type__c,
			                              Discounts_Allowed__c,
			                              ProductCode,
			                              Promo_Code_Type__c,
			                              Accounts_Specific__c,
			                              Campaign__c,
			                              Team_Discount__c,
			                              Balance__c,
			                              Contest_Promo_Code__c,
			                              (
			                                 Select Id,
			                                        UnitPrice
			                                 From PricebookEntries
			                                 Where Pricebook2Id=:regMan.getEventWrapper().getPriceBookId()
			                              )
			                       From Product2
			                       Where isActive=true And
			                       	Event__c=:regMan.getEventWrapper().getEvent().Id And
			                       	Family=:efConstants.PROD_FAMILY_PROMO And
			                       	ProductCode=:promoCode  And
			                       	Balance__c>0 And
			                       	StartDate__c<=TODAY And
			                       	EndDate__c>=TODAY And
			                       	Valid_Attendee_types__c includes (:searchAttendeeTypes)];
			if(products!=null && products.size()>0){
				efProductWrapper p2i = new efProductWrapper(products[0]);
				return p2i; 
			} 
		}		
		return null;
	}
    
    public boolean getAnyPromoCodeAdded(){
        return promoCodeList!=null && promoCodeList.size()>0;
    }
			
	public void removePromoCode(String pid){
		promoCode='';
		if(promoCodeList!=null && promoCodeList.size()>0){
			for(Integer i=0;i<promoCodeList.size();i++){
				efProductWrapper p = promoCodeList.get(i);

				if (p.getProduct().ProductCode.equals(pid) &&
				    p.getProduct().Promo_code_type__c.equals(efConstants.PROMO_TYPE_EDUCATION)){
					promoCodeList.remove(i);
					educationPromoApplied = false; 
				} else if (p.getProduct().ProductCode.equals(pid) &&
				         p.getProduct().Promo_code_type__c.equals(efConstants.PROMO_TYPE_REGISTRATION)){
					promoCodeList.remove(i);
					registrationPromoApplied = false;				
				}				
			}		
		}
	}
	
	public void updatePromoProducts(){
		if(!promoCodesupdated){
			if(promoCodeList!=null && promoCodeList.size()>0){
				List <Product2> pUpdate = new List<Product2>();
				for(efProductWrapper p: promoCodeList){
					Double counter = 0;
					if(p.getProduct().Used__c!=null)
						counter = p.getProduct().Used__c;
						
					p.getProduct().Used__c = counter + 1;
					pUpdate.add(p.getProduct());
				}
					
				if(pUpdate!=null && pUpdate.size()>0){
					update pUpdate;
					promoCodesupdated = true;
				}	
			}
		}
	}
	
	public void removeEducationPromoCode(){
		removePromoCodeByType(efConstants.PROMO_TYPE_EDUCATION);
        educationPromoApplied = false;
	}
	
	private void removePromoCodeByType(String pcType){
        if (promoCodeList != null && promoCodeList.size() > 0){
            for (Integer i = 0; i < promoCodeList.size(); i++){
                efProductWrapper p = promoCodeList.get(i);
                if (p.getProduct().Promo_code_type__c.equals(pcType)){
                    promoCodeList.remove(i);
                }
            }
        }
	}
	
	public Boolean validatePromoCodes(){
    	Boolean error = false;
    	Set<Id> promoIDs = new Set<Id>();
    	
    	if (promoCodeList != null){
	    	for (efProductWrapper pi:promoCodeList){
	    		promoIDs.add(pi.getProduct().Id);
	    	}
	    	
	    	for (Product2 p:[Select Id,
	                                Used__c,
	                                ProductCode,
	                                Balance__c
	                         From Product2
	                         Where Id In :promoIDs]){
	        	if (p.Balance__c <= 0){
	        		error = true;
	        		removePromoCode(p.ProductCode);
	        	} else{
	        		updateUsage(p.Id, p.Used__c);
	        	}
	        }
    	}	
    	if (error){
    		promoMessage = efConstants.PROMO_MESSAGE_CANNOT_APPLY; 
    	}
    	return !error;
    }
    
    private void updateUsage(Id promoID, Decimal usage){
		if (promoCodeList != null){
			for (efProductWrapper pi:promoCodeList){
				if (pi.getProduct().Id == promoId){
					pi.getProduct().Used__c = usage;
					break;
				}
			}
		}
    }	
	
	private Double getBalanceRegistrationAmount(Double totalRegAmt){
		if(promoCodeList!=null && promoCodeList.size()>0){
			for(efProductWrapper  p: promoCodeList){
				if(p.getProduct().Promo_code_type__c.equals(efConstants.PROMO_TYPE_REGISTRATION)){
					double amt = double.valueOf(p.getCalculatedDiscountAmount());
					totalRegAmt = totalRegAmt + amt;
				}
			}
			return totalRegAmt;
		}
		return totalRegAmt;	
	}
	
	private Double getBalanceClassesAmount(Double totalEduAmt){
		if(promoCodeList!=null && promoCodeList.size()>0){
			for(efProductWrapper  p: promoCodeList){
				if(p.getProduct().Promo_code_type__c.equals(efConstants.PROMO_TYPE_EDUCATION)){
					double amt = double.valueOf(p.getCalculatedDiscountAmount());					
					totalEduAmt = totalEduAmt + amt;	
				}
			}
			return totalEduAmt;		
		}
		return totalEduAmt;	
	}
	
	
	private Boolean existsInList(efProductWrapper pProducts){
		if(promoCodeList!=null && promoCodeList.size()>0){
			for(efProductWrapper  p: promoCodeList){
				if(p.getProduct().ProductCode.equals(pProducts.getProduct().ProductCode)){
					return true;
				}
			}
			return false;		
		} else{
			return false;
		}	
	}
	
	// Comment & : called from OppManager.
	public List<OpportunityLineItem> addEducationPromoToOLI(List<OpportunityLineItem> oliRecords, String oppId){
		if(promoCodeList!=null && promoCodeList.size()>0){
			for(efProductWrapper p:promoCodeList){
				if(p.getProduct().Promo_Code_Type__c.equals('Education')){
					PricebookEntry pe = p.getProduct().PriceBookEntries; 
					OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = oppId,pricebookentryId=pe.Id,Quantity=1,Registration__c=regMan.getRegistrationWrapper().getRegistration().id);
					oli.unitprice=double.valueOf(p.getCalculatedDiscountAmount());
					oliRecords.add(oli);
				} 
			}  
		}
		return oliRecords;
	}
	
	private void setRegistrationPromoApplied(){
		// Need to ignore Registration Opportunities at cancelled status.
		OpportunityLineItem[] olItem = [Select Id,PricebookEntry.Product2.Family,PricebookEntry.Product2Id 
										from OpportunityLineItem 
										where Registration__c =:regMan.getRegistrationWrapper().getRegistration().Id and 
											Opportunity.StageName !=: efConstants.OPP_STATUS_CANCELLED and
											PricebookEntry.Product2.Family =: efConstants.PROD_FAMILY_PROMO and 
											PricebookEntry.Product2.Promo_Code_Type__c =: efConstants.PROMO_TYPE_REGISTRATION];						
		if (olItem != null && olItem.Size() > 0){
			registrationPromoApplied = true;
		}
	}
}