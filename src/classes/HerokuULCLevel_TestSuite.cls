/*
* File HerokuULCLevel_TestSuite
    * @description : Unit test for all Heroku Classes
    * @purpose : Test class coverage
    * @author : Surya Handayani
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       27.01.2016    Surya              Intial class
    2       05.01.2018    Pramod Kumar V     Test Class coverage for HerokuULCLevelSchedulable

*/
@isTest(seeAlldata=false)
private class HerokuULCLevel_TestSuite {

  @testSetup static void setup() {
        Account a = QTTestUtils.createMockAccount('Test Account', new User(Id = UserInfo.getUserId()));
        Contact c = QTTestUtils.createMockContact(a.Id);
        ULC_Details__c d = QTTestUtils.createMockULCDetail(c.Id, 'Active');

        QTCustomSettings__c Settings = new QTCustomSettings__c();
        Settings.Name = 'Default';
        Settings.ULC_Base_URL__c = 'MyBaseURL';
        Settings.ULC_Crypto_Secret__c = 'Secret';
        Settings.ULC_Crypto_Key__c = 'KALLEKALLEKALLEKALLEKALLEKALLE12';
        insert Settings;

         System.runAs(new User(Id = Userinfo.getUserId())) 
        {
        QuoteTestHelper.createCustomSettings();
        }
    }

    @isTest
    static void HerokuULCLevelScheduleUnitTest() {
        QTTestUtils.GlobalSetUp();
        
        //& Test EntitlementSync via schedulable class.     
        //setup();
        
        String CRON_EXP = '0 0 0 3 9 ? 2099';       
        
        Test.startTest();

        // Richard: inflate schedule to get into the loop in HerokuULCLevelSchedulable() otherwise does not reach 75%
        String s = '05 '+ (System.Now().Minute()==59 ? '0' : (System.Now().Minute()+1).format()) + ' * * * ?';
        String j = System.schedule('HerokuULCLevel Sync2 - Auto Schedule', s, new HerokuULCLevelSchedulable());
        String j3 = System.schedule('HerokuULCLevel Sync3 - Auto Schedule', s, new HerokuULCLevelSchedulable());
        String j4 = System.schedule('HerokuULCLevel Sync4 - Auto Schedule', s, new HerokuULCLevelSchedulable());
        String j5 = System.schedule('HerokuULCLevel Sync5 - Auto Schedule', s, new HerokuULCLevelSchedulable());
        String j6 = System.schedule('HerokuULCLevel Sync6 - Auto Schedule', s, new HerokuULCLevelSchedulable());

        // Schedule the test job
        String jobId = System.schedule('testHerokuULCLevelScheduledApex', CRON_EXP, new HerokuULCLevelSchedulable());
        
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same 
        System.assertEquals(CRON_EXP, ct.CronExpression);

        // Verify the job has not run 
        System.assertEquals(0, ct.TimesTriggered);

        // Verify the next time the job will run 
        System.assertEquals('2099-09-03 00:00:00', String.valueOf(ct.NextFireTime)); 
         
        Test.stopTest(); //& instantiates the batchable class but doesn't execute in this context.                  
        
    } 

    
    @isTest
    static void HerokuULCLevelBatchableUnitTest() {
        QTTestUtils.GlobalSetUp();
        
        Account a = [SELECT Id, ParentId FROM Account];

        Account_License__c al = new Account_License__c();
        al.Name = '460987569911';
        al.Product__c = 'QSERVER';
        al.Account__c = a.Id;
        al.Support_From__c = Date.today().addDays(-10);
        al.Support_To__c = Date.today().addDays(100);
        al.O_S__c = 'Windows 3.1';
        al.Environment__c = 'MS Windows';
        al.Application__c = 'QVSERVER';
        al.Time_Limit__c = Date.today().addDays(100);
        insert al;        
        Test.startTest();

        HerokuULCLevelBatchable eb = new HerokuULCLevelBatchable();
        
        ID batchprocessid = Database.executeBatch(eb);
        
        Test.stopTest(); //& batch executes following stopTest.
                

    }

    @isTest
    static void HerokuULCLevelParentBatchableUnitTest() {
        QTTestUtils.GlobalSetUp();
        
        Account a = [SELECT Id, ParentId FROM Account];
        Account a4 = QTTestUtils.createMockAccount('Test Account 4', new User(Id = UserInfo.getUserId()));
        Contact c2 = new Contact(firstName='RDZTestContact', 
                   lastName='RDZTestLastName2', 
                   Email='RDZTest2@test.com.sandbox',
                   Phone= '004487585848787', 
                   HasOptedOutOfEmail=false,
                   AccountId = a4.id
                   );
        insert c2;
        
        a4.ParentId = a.id;
        List<Account> accs = new List<Account>();
        accs.add(a4); 
        update accs;


        
        Test.startTest();

        HerokuULCLevelParentBatchable eb = new HerokuULCLevelParentBatchable();
        
        ID batchprocessid = Database.executeBatch(eb);
        
        Test.stopTest(); //& batch executes following stopTest.
                

    }

    @isTest static void test_GetParentNavisionStatus() {
                ULC_Details__c ulcd = [SELECT Id,
                                        ULCName__c, 
                                        ContactId__r.FirstName,
                                        ContactId__r.LastName,
                                        ContactId__r.Email,
                                        ContactId__c,
                                        ContactId__r.AccountId,
                                        ContactId__r.Id,
                                        ContactId__r.Account_Type__c,
                                        ContactId__r.Account.Adopted_Navision_Status__c,
                                        ContactId__r.Account.Partner_Type__c,                                       
                                        ContactId__r.Account.Parent.ParentId,
                                        ContactId__r.MailingCity, 
                                        ContactId__r.MailingCountry, 
                                        ContactId__r.MailingPostalCode, 
                                        ContactId__r.MailingState, 
                                        ContactId__r.MailingStreet, 
                                        ContactId__r.Country_Code__r.Name,
                                        ContactId__r.Phone, 
                                        LeadId__c,
                                        LeadId__r.FirstName,
                                        LeadId__r.LastName,
                                        LeadId__r.Email, 
                                        LeadId__r.City, 
                                        LeadId__r.Street, 
                                        LeadId__r.Country, 
                                        LeadId__r.PostalCode, 
                                        LeadId__r.State,
                                        LeadId__r.Country_Code__r.Name,
                                        LeadId__r.Phone
                                         FROM ULC_Details__c];

        HerokuULCLevelParentBatchable c = new HerokuULCLevelParentBatchable();
         
        Account a1 = [SELECT Id, ParentId FROM Account];
        system.assertEquals(null, c.GetParentNavisionStatus(a1.Id, 0));

        Account a2 = QTTestUtils.createMockAccount('Test Account 2', new User(Id = UserInfo.getUserId()));
        Account a3 = QTTestUtils.createMockAccount('Test Account 3', new User(Id = UserInfo.getUserId()));      
        a1.ParentId = a2.Id;
        a2.Navision_Status__c = 'Partner';
        a3.Navision_Status__c = 'Customer';
        List<Account> accs = new List<Account>();
        accs.add(a1); accs.add(a2); accs.add(a3);
        update accs;
        
        system.assertEquals(null, c.GetParentNavisionStatus(a1.Id, 3));
        system.assertEquals('Partner', c.GetParentNavisionStatus(a1.Id, 0));
        system.assertEquals(null, c.GetParentNavisionStatus('001c000000k8WpW', 0));  // This ID should not be found - this is a negative test

        a1.ParentId = a3.Id;
        update a1;
        system.assertEquals('Customer', c.GetParentNavisionStatus(a1.Id, 0));  
        HerokuULCLevelSchedulable b2 = new HerokuULCLevelSchedulable();
        String sch = '0 0 23 * * ?'; system.schedule('Test Territory Check', sch, b2);        
 
    }

}