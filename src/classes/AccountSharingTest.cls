/********
* NAME :AccountSharingTest
* Description: Tests for account sharing 
* 
*
*Change Log:
    TJG      2015-05-05   Updated for CR#78696  https://eu1.salesforce.com/a0CD000000xSg6v
    TJG      2015-12-24   re-structured and added tests.
    IRN      2015-11-27   Partner sharing project, Created test class
       2016-06-08    Andrew Lokotosh Commented Forecast_Amount fields line  506
       29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta
    AIN      2019-03-05   Fixed test class errors after we removed partner sharing. PS, the above date format is bad and you should feel bad.
******/
 
@isTest
public class AccountSharingTest {

    public static Account portalAccount;
    static final integer NBR_OF_ACCOUNTS = 10;
    static final Id SysAdminProfileId = '00e20000000yyUzAAI';
    static final Id PortalUserRoleId = '00E20000000vrJSEAY';
    //PRM - Sales Dependent Territory + QlikBuy
    static final Id PrmProfileId = '00e20000001OyLwAAK';
    //CPLog
    static final Id CustomerPortalProfileId = '00eD0000001PmOz';
    //End User Account Record Type Id
    static final Id EndUserAccRecTypeId = '01220000000DOFuAAO';
    // Partner Account Record Type ID
    static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';
    // Qlikbuy CCS Standard II 
    static final Id OppRecTypeId = '012D0000000KEKOIA4';

    private static void Setup() {
        ApexSharingRules.TestingAccountShare = true;
        //createMockPatnerUser();
    }


    private static testMethod void accountSharingTest1() {
        QTTestUtils.GlobalSetUp();
        Setup();
        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator
        User myUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        // Create a number of accounts. Half of them owned by myUser, 
        // and the other half owner by a different partner user 
        List<Account> myAccounts = createTestAccounts(myUser, ctList);
        //List<Account> lsAccounts = new List<Account>();
        List<Responsible_Partner__c>resPartners = new List<Responsible_Partner__c>();
        List<User> pUsers = createUserFromProfileId(CustomerPortalProfileId, ctList);

        // make sure NBR_OF_ACCOUNTS at least 2!
        // test responsible partner
        // Integer iLen = myAccounts.size();

        for (integer i = 1; i < NBR_OF_ACCOUNTS; i++) {
            //lsAccounts.add(myAccounts[i]);

            Responsible_Partner__c resp = new Responsible_Partner__c(End_User__c = myAccounts[i].Id, Partner__c = myAccounts[i - 1].Id);

            resPartners.add(resp);
        }
        Responsible_Partner__c respt = new Responsible_Partner__c(End_User__c = myAccounts[0].Id, Partner__c = myAccounts[NBR_OF_ACCOUNTS - 1].Id);

        resPartners.add(respt);

        //lsAccounts.add(myAccounts[0]);

        Insert resPartners;

        User nuUser = createMockPatnerUser(myUser);
        List<Id> accountIds = new List<Id>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
            accountIds.add(myAccounts[i].Id);
            myAccounts[i].Owner = pUsers[0]; //nuUser;
            myAccounts[i].QT_Contracted_Partner__c = i == (NBR_OF_ACCOUNTS - 1) ? myAccounts[0].Id : myAccounts[i + 1].Id;
        }

        AccountSharing.startSharing(accountIds);

        AccountSharing.startSharing(accountIds);

        List<AccountShare> accountShares = [
                SELECT
                        Id,
                        UserOrGroupId,
                        AccountId,
                        AccountAccessLevel,
                        CaseAccessLevel,
                        ContactAccessLevel,
                        OpportunityAccessLevel
                FROM
                        AccountShare
                WHERE
                RowCause = 'Manual' AND AccountId in :accountIds
        ];

        System.debug('accountShares ' + accountShares);

        System.assertNotEquals(0, accountShares.size());

        //--ApexSharingRules.updateAccountSharing(myAccounts, lsAccounts);

        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = false;
        Update myAccounts;

        AccountSharing.startSharing(accountIds);

        Test.stopTest();
    }

    private static testMethod void accountSharingTest2() {

        QTTestUtils.GlobalSetUp();
        Setup();
        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User myUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);
        List<Contact> ctList = new List<Contact>();
        List<Account> myAccounts = createTestAccounts(myUser, ctList);
        List<Account> lsAccounts = new List<Account>();

        List<User> pUsers = createUserFromProfileId(CustomerPortalProfileId, ctList);

        List<Responsible_Partner__c>resPartners = new List<Responsible_Partner__c>();

        // make sure NBR_OF_ACCOUNTS at least 2!
        // test responsible partner
        Integer iLen = myAccounts.size();

        for (integer i = 1; i < iLen; i++) {
            lsAccounts.add(myAccounts[i]);

            Responsible_Partner__c resp = new Responsible_Partner__c(End_User__c = myAccounts[i].Id, Partner__c = myAccounts[i - 1].Id);

            resPartners.add(resp);
        }

        Responsible_Partner__c respt = new Responsible_Partner__c(End_User__c = myAccounts[0].Id, Partner__c = myAccounts[iLen - 1].Id);

        resPartners.add(respt);

        lsAccounts.add(myAccounts[0]);

        //Update myAccounts;

        Insert resPartners;

        List<Id> accountIds = new List<Id>();
        for (Account acc : myAccounts) {
            accountIds.add(acc.Id);
        }

        //AccountSharing.addAccountSharing(myAccounts);
        AccountSharing.startSharing(accountIds);

        // swap odd and even number owners
        for (integer i = 0; i < iLen; i++) {
            myAccounts[i].owner = myUser;
            myAccounts[i].OwnerId = myUser.Id;
        }

        Update myAccounts;

        // create a manual share
        /*
        AccountShare accShare = new AccountShare(
            AccountId = myAccounts[0].Id,
            AccountAccessLevel = 'Read',
            CaseAccessLevel = 'None',
            ContactAccessLevel = 'None',
            OpportunityAccessLevel = 'None',
            UserOrGroupId = pUsers[0].Id);

        Insert accShare; */

        //ApexSharingRules.updateAccountSharing(myAccounts, lsAccounts);

        List<AccountShare> accountShares = [
                SELECT
                        Id,
                        UserOrGroupId,
                        AccountId,
                        AccountAccessLevel,
                        CaseAccessLevel,
                        ContactAccessLevel,
                        OpportunityAccessLevel
                FROM
                        AccountShare
                WHERE
                RowCause = 'Manual' and AccountId in :accountIds
        ];

        System.debug('accountShares ' + accountShares);

        System.assertNotEquals(0, accountShares.size());

        AccountSharing.deleteRelatedListAccountSharing(resPartners);

        Test.stopTest();
    }

    private static testMethod void testStartSharing() {
        QTTestUtils.GlobalSetUp();
        Setup();
        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // create system Administrator
        User myUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        // Create a number of accounts. Half of them owned by myUser, 
        // and the other half owner by a different partner user
        List<Account> myAccounts = createTestAccounts(myUser, ctList);
        //List<Account> lsAccounts = new List<Account>();
        List<Responsible_Partner__c>resPartners = new List<Responsible_Partner__c>();
        List<User> pUsers = createUserFromProfileId(CustomerPortalProfileId, ctList);

        // make sure NBR_OF_ACCOUNTS at least 2!
        // test responsible partner
        Integer iLen = myAccounts.size();

        for (integer i = 1; i < iLen; i++) {
            //lsAccounts.add(myAccounts[i]);

            Responsible_Partner__c resp = new Responsible_Partner__c(End_User__c = myAccounts[i].Id, Partner__c = myAccounts[i - 1].Id);

            resPartners.add(resp);
        }
        Responsible_Partner__c respt = new Responsible_Partner__c(End_User__c = myAccounts[0].Id, Partner__c = myAccounts[iLen - 1].Id);

        resPartners.add(respt);

        //lsAccounts.add(myAccounts[0]);

        Insert resPartners;

        User nuUser = createMockPatnerUser(myUser);
        List<Id> accountIds = new List<Id>();
        for (Integer i = 0; i < myAccounts.size(); i++) {
            accountIds.add(myAccounts[i].Id);
            myAccounts[i].Owner = nuUser;
        }

        AccountSharing.startSharing(accountIds);

        //AccountSharing.startSharing(accountIds);
        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = false;

        List<AccountShare> accountShares = [
                SELECT
                        Id,
                        UserOrGroupId,
                        AccountId,
                        AccountAccessLevel,
                        CaseAccessLevel,
                        ContactAccessLevel,
                        OpportunityAccessLevel
                FROM
                        AccountShare
                WHERE
                RowCause = 'Manual' AND AccountId in :accountIds
        ];

        System.debug('accountShares ' + accountShares);

        System.assertNotEquals(0, accountShares.size());

        //--ApexSharingRules.updateAccountSharing(myAccounts, lsAccounts);

        Update myAccounts;

        Test.stopTest();
    }

    private static testMethod void testUpdateSharing() {
        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();
        Setup();
        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User myUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);
        List<Contact> ctList = new List<Contact>();
        List<Account> myAccounts = createTestAccounts(myUser, ctList);
        Semaphores.SetAllSemaphoresToFalse();
        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = false;

        List<Account> lsAccounts = new List<Account>();

        List<User> pUsers = createUserFromProfileId(CustomerPortalProfileId, ctList);

        List<Responsible_Partner__c>resPartners = new List<Responsible_Partner__c>();

        // make sure NBR_OF_ACCOUNTS at least 2!
        // test responsible partner
        Integer iLen = myAccounts.size();

        for (integer i = 1; i < iLen; i++) {
            lsAccounts.add(myAccounts[i]);

            Responsible_Partner__c resp = new Responsible_Partner__c(End_User__c = myAccounts[i].Id, Partner__c = myAccounts[i - 1].Id);

            resPartners.add(resp);
        }

        Responsible_Partner__c respt = new Responsible_Partner__c(End_User__c = myAccounts[0].Id, Partner__c = myAccounts[iLen - 1].Id);

        resPartners.add(respt);

        lsAccounts.add(myAccounts[0]);

        //Update myAccounts;

        Insert resPartners;

        List<Id> accountIds = new List<Id>();
        for (Account acc : myAccounts) {
            accountIds.add(acc.Id);
        }

        //--AccountSharing.startSharing(accountIds);

        for (integer i = 0; i < iLen; i++) {
            myAccounts[i].owner = me;
            myAccounts[i].OwnerId = me.Id;
        }
        update myAccounts[0];

        // create a manual share
        /*
        AccountShare accShare = new AccountShare(
            AccountId = myAccounts[0].Id,
            AccountAccessLevel = 'Read',
            CaseAccessLevel = 'None',
            ContactAccessLevel = 'None',
            OpportunityAccessLevel = 'None',
            UserOrGroupId = pUsers[0].Id);

        Insert accShare; */

        //ApexSharingRules.updateAccountSharing(myAccounts, lsAccounts);
        Test.stopTest();

        List<AccountShare> accountShares = [
                SELECT
                        Id,
                        UserOrGroupId,
                        AccountId,
                        AccountAccessLevel,
                        CaseAccessLevel,
                        ContactAccessLevel,
                        OpportunityAccessLevel
                FROM
                        AccountShare
                WHERE
                RowCause = 'Manual' and AccountId in :accountIds
        ];

        System.assertNotEquals(0, accountShares.size());

        AccountSharing.deleteRelatedListAccountSharing(resPartners);

    }

    private static User createMockPatnerUser(User me) {

        User aUser = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');

        aUser.IsPortalEnabled = true;

        System.runAs (me) {
            update aUser;
        }

        return aUser;
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me) {
        User u
                = new User(
                        alias = 'adminUsr', email = 'adminUser@tjtest.com.test',
                        Emailencodingkey = 'UTF-8', lastname = 'adminTest',
                        Languagelocalekey = 'en_US', localesidkey = 'en_US',
                        Profileid = profId,
                        Timezonesidkey = 'America/Los_Angeles',
                        //ContactId = cont.Id,
                        UserRoleId = roleId,
                        Username = System.now().millisecond() + '_' + '_newuser@jttest.com.test'
                );

        System.runAs (me) {
            insert u;
        }

        return u;
    }

    private static List<Account> createTestAccounts(User aUser, List<Contact> cLst) {
        List<Account> aLst = new List<Account>();
        //List<Contact> clst = new List<Contact>();
        //User user1 = createMockPatnerUser(aUser);
        //Boolean oddIndex = false;
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser) {
            for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
                //Create account

                Account userAccount = new Account(
                        Billing_Country_Code__c = qtComp.Id,
                        QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                        Name = 'UserAcc' + i,
                        OwnerId = aUser.Id,
                        RecordTypeId = PartnerAccRecTypeId
                );
                //if (oddIndex)
                //{
                //    userAccount.Owner = user1;
                //    userAccount.OwnerId = user1.Id;
                //}
                //oddIndex = !oddIndex;
                aLst.add(userAccount);
            }

            Insert aLst;

            for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
                Contact contact = new Contact(FirstName = 'TestCon' + i,
                        Lastname = 'testsson',
                        AccountId = aLst[i].Id,
                        Email = 'tc' + i + '@test.com');
                cLst.add(contact);
            }
            insert cLst;
        }
        return aLst;
    }

    public static List<User> createUserFromProfileId(Id profId, List<Contact> clist) {
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        List<User> usrs = new List<User>();
        System.runAs (thisUser) {
            integer ndx = 1;
            for (Contact ct : clist) {
                User u = new User(
                        alias = 'newUser',
                        email = 'puser' + ndx + '@tjtest.com.test',
                        Emailencodingkey = 'UTF-8',
                        lastname = 'Testing',
                        Languagelocalekey = 'en_US',
                        localesidkey = 'en_US',
                        Profileid = profId,
                        Timezonesidkey = 'America/Los_Angeles',
                        ContactId = ct.Id,
                        Username = 'shuser' + ndx + '@tjtest.com.test'
                );

                usrs.add(u);

                ndx++;
            }

            insert usrs;

            for (integer i = 0; i < usrs.size(); i++) {
                usrs[i].IsPortalEnabled = true;
            }

            update usrs;
        }

        return usrs;
    }

    private static Opportunity createAnOpportunity(String sShortDescription, String sName, String sType, String sRevenueType,
            String sStage, String sRecordTypeId, String sCurrencyIsoCode, Account acc, Id stp, User ownerUsr, Id prmRefCon, Decimal refMargin, String standardRM) {
        Opportunity opp = New Opportunity(
                Short_Description__c = sShortDescription,
                Name = sName,
                Type = sType,
                Revenue_Type__c = sRevenueType,
                CloseDate = Date.today().addDays(10),
                StageName = sStage,
                RecordTypeId = sRecordTypeId,
                CurrencyIsoCode = sCurrencyIsoCode,
                AccountId = acc.Id,
                Customers_Business_Pain__c = 'First project scheduled',
                //Consultancy_Forecast_Amount__c = 10,
                Sell_Through_Partner__c = stp,
                Owner = ownerUsr,
                Signature_Type__c = 'Digital',
                PRM_Referring_Contact__c = prmRefCon,
                Referral_Margin__c = refMargin,
                Standard_Referral_Margin__c = standardRM
        );

        return opp;
    }
}