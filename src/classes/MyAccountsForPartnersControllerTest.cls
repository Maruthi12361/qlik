/******************************************************

Class: MyAccountsForPartnersControllerTest
Description: Test class for MyAccountsForPartnersController

Changelog:
2019-01-14  Malay Desai, Slalom      Created file
2019-03-05  AIN Test class fix

******************************************************/
@isTest
public class MyAccountsForPartnersControllerTest {
    public static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
    public static Schema.DescribeSObjectResult accDescribe= gd.get('Account').getDescribe();
    public static Schema.DescribeSObjectResult oppDescribe= gd.get('Opportunity').getDescribe();
    
    public static String END_USER_ACCOUNT_RT = accDescribe.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
    public static String PARTNER_ACCOUNT_RT =  accDescribe.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
    public static String SALES_QCCS_OPP_RT = oppDescribe.getRecordTypeInfosByName().get('Qlikbuy CCS Standard').getRecordTypeId();
    
    @TestSetup
    static void createTestData() {
        
        /* Set up partner user - START*/
        Id p = [SELECT Id FROM Profile WHERE Name='PRM - Sales Dependent Territory + QlikBuy'].Id;
        
        Account acc = new Account(name ='Test Partner Account', RecordTypeId=PARTNER_ACCOUNT_RT) ;
        /* Test End User Account - START */
        Account eua1 = new Account(name ='Test EUA1', RecordTypeId=END_USER_ACCOUNT_RT) ;
        Account eua2 = new Account(name ='Test EUA2', RecordTypeId=END_USER_ACCOUNT_RT) ;
        Account eua3 = new Account(name ='Test EUA3', RecordTypeId=END_USER_ACCOUNT_RT) ;
        
        insert new List<Account>{acc,eua1,eua2,eua3};
            
            /* Test End User Account - END */
            
            Contact con = new Contact(LastName ='Test Partner Contact',AccountId = acc.Id);
        
        insert con;  
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Targaryen', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='Daenerysthefirstofhername@stormborn.com');
        
        insert user;
        
        Referral__c ref = new Referral__c(End_User_Account__c= eua1.Id,Referring_Partner_Account__c=acc.Id,
                                         End_User_Contact_Email__c='test@noemail.com', End_User_Contact_Name__c='test contact',
                                         OwnerId= user.Id, Accepted_Terms_and_Conditions__c=true); 
        insert ref;
    }
    
    @isTest
    public static void TestFetchRelatedAccountIds(){

        Test.startTest();
        User user = [SELECT Id, Name,alias FROM User WHERE alias='test123'];
        system.runAs(user){
            MyAccountsForPartnersController.AccountWrapper aw = MyAccountsForPartnersController.getRecordCountAndIds();
			List<Account> accounts = MyAccountsForPartnersController.getAccounts(aw.accountIds, 20, 0);
            System.assertEquals(1, aw.accountIds.size()); 
        }
        Test.stopTest();
    }
}