/**
*
* extbad    2019-09-17 IT-2037 Send an email to the 'Submitted_by'
*/
global with sharing class ArticleFeedbackHelper {
    public static final String EMAIL_TEMPLATE_NAME = 'Support_Article_Feedback_Response';

    webService static Boolean sendFeedbackEmail(String caseId) {
        Boolean emailSent = false;
        Case cs = [
                SELECT Id, Subject, Feedback_to_submitter_Actions_taken__c, Submitted_by__c, Submitted_by__r.Email,
                        Knowledge_Article__c
                FROM Case
                WHERE Id = :caseId
        ];

        List<EmailTemplate> templates = [
                SELECT Id, Name, HtmlValue, Body, Subject
                FROM EmailTemplate
                WHERE DeveloperName = :EMAIL_TEMPLATE_NAME
                LIMIT 1
        ];

        if (!templates.isEmpty()) {
            EmailTemplate template = templates.get(0);

            String articleNumber = cs.Knowledge_Article__c;
            String articleTitle = '';
            List<Basic__kav> basicArticle = [SELECT Id, ArticleNumber, Title FROM Basic__kav WHERE Id = :cs.Knowledge_Article__c];
            if (basicArticle.size() == 1) {
                articleNumber = basicArticle.get(0).ArticleNumber;
                articleTitle = basicArticle.get(0).Title;
            } else {
                List<Diagnostic__kav> diagnArticle = [SELECT Id, ArticleNumber, Title FROM Diagnostic__kav WHERE Id = :cs.Knowledge_Article__c];
                if (diagnArticle.size() == 1) {
                    articleNumber = diagnArticle.get(0).ArticleNumber;
                    articleTitle = basicArticle.get(0).Title;
                }
            }

            // Here build the single test email message
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {'invalid@emailaddr.test'});
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setTemplateId(template.Id);

            //The email template will be merged with the brandtemplate (letterhead) after sending. But because of the rollback it will not be sent
            Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            Database.rollback(sp);

            // Use htmlbody merged with letterhead to replace placeholders
            String body = mail.getHtmlBody().replace('CASE_ACTIONS_TAKEN',
                    cs.Feedback_to_submitter_Actions_taken__c != null ? cs.Feedback_to_submitter_Actions_taken__c : '');
            body = body.replaceAll('ARTICLE_TITLE', articleTitle);
            QS_Partner_Portal_Urls__c partnerUrls = QS_Partner_Portal_Urls__c.getInstance();
            String portalUrl = partnerUrls.Support_Portal_Login_Url__c;
            body = body.replaceAll('ARTICLE_LINK', portalUrl != null ? portalUrl + '/articles/' + articleNumber : '');
            body = body.replaceAll('DID_YOU_KNOW', '<b><font size="3" style="color: rgb(0, 152,69);">Did you know?</font></b>');

            String subj = template.Subject.replace('ARTICLE_TITLE', articleTitle);

            Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();

            String noReplyAddress = '';
            if (QTCustomSettings__c.getInstance('Default') != null) {
                noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
            }
            List<OrgWideEmailAddress> oweaList = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :noReplyAddress];
            if (oweaList.size() > 0) {
                emailMessage.setOrgWideEmailAddressId(oweaList.get(0).Id);
            }

            emailMessage.setToAddresses(new String[] {cs.Submitted_by__r.Email});
            emailMessage.setHtmlBody(body);
            emailMessage.setSubject(subj);
            emailMessage.setWhatId(caseId);
            try {
                List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailMessage});
                for (Messaging.SendEmailResult res : sendres) {
                    System.debug('Result: ' + res);
                }
                emailSent = true;
            } catch (System.EmailException ex) {
                System.debug('Error when sending mail: ' + ex.getMessage());
            }
        }
        return emailSent;
    }
}