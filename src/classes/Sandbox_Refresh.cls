/*
* File Sandbox_Refresh 
    * @description : Class for Post deployemnt activity for updating post deployment activity
                     It will be executed after QA refresh in prod.
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       09.01.2018   Pramod Kumar V      Created Class 
    2       20.01.2018   Pramod Kumar V      Added logic from SandboxRefresh_Controller class
                                             Automated Jira Tickets: SFDC: Sandbox Refresh App,SFDC: Contact email updates,SFDC: Lead Email Updates

    3       28.04.2018   Pramod Kumar v      ITRM-60 Fixed Mixed DML exception and added logic to create subsidary
    4       21.06.2018   Maksim Karpitski    Added a method call to create Campaign
    5       23.07.2018   ext_bjd             QAR-64 Fixed call the Post Refresh logic from one place instead of duplicating in two classes
*/
global class Sandbox_Refresh implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        if (context != null) {
            String SandboxName = context.sandboxName();
            System.debug('!!!! Our Sandbox name is : ' + SandboxName);
            PostRefreshHandler.PostRefreshCustomSettingHelper(SandboxName);
        }
    }

//    global static void PostRefreshCustomSettingHelper(String SandboxName) {
//        Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox ;
//        if (isSandbox) {
//
//            //Create QlikTech Company
//            RefreshApp.Create_QlikComp();
//            //Create Campaign object
//            SandboxRefresh_Controller.createCampaign();
//
//            //Not used since we are doing the same using sfapex.If in future if sfapex not license renewed just remove the comments below
//            //Contact and Lead Email Update Logic
//            //PostRefreshSandboxEmailUpdate pt=new PostRefreshSandboxEmailUpdate();
//            //pt.PostRefreshEmailUpdate () ;
//
//            // Custom settings update Refer the link to see what custom setting are updated
//            //https://confluence.qliktech.com/pages/viewpage.action?spaceKey=ISD&title=Salesforce+-+Refresh+Guide#Salesforce-RefreshGuide-HowtogenerateaSecurityToken
//            UpdateCustomSettings cs = new UpdateCustomSettings();
//            RefreshSandboxData__mdt RefSandBox = cs.getRefreshSandboxDataBySandboxName(SandboxName);
//            cs.UpdateCustomSettingsfromMdt(RefSandBox) ;
//        }
//    }
}