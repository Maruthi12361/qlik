/* 
Purpose: Partner Expertise Controller test
Created Date: 4 Jan 2016
Initial delvelpment MTM 4 Jan 2016
Change log:
* 03/01/2017 : Pramod Kumar V : Test class coverage
 **/
@isTest
public class EP_PartnerExpertiseControllerTest {

    Static Account account { get; set; }
    Static Partner_Expertise_Area__c ExpertiseArea {get; set;}
    Static Partner_Expertise_Application_Lookup__c ExpertLookup {get; set;} 
    Static User testUser {get; set;}
    Static Contact testContact {get; set;}
    
    static testmethod void test_IsPRMBase() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();            
            System.assertEquals(false, controller.IsPRMBase);    
        }
        Test.stopTest();
    }
    
    static testmethod void test_calculateEligibility() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();            
            Boolean eligiblity = controller.calculateEligibility();
            System.assertEquals(false, eligiblity);    
        }
        Test.stopTest();
    }
    
    static testmethod void test_Save() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();
            controller.DedicatedContact = testContact.Id;
            controller.ExpertiseAreaSector = 'Communications';
            controller.Save();
            System.assertEquals('Under Review', controller.PartnerExpertiseArea.Expertise_Application_Eligibility__c);    
        }
        Test.stopTest();
    }
    
    static testmethod void test_ListExpertiseSector() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();
            controller.DedicatedContact = testContact.Id;
            controller.ExpertiseAreaSector = 'Communications';
            controller.Save();
            System.assert(controller.ListExpertiseSector.size() == 0);
        }
        Test.stopTest();
    }
    
    Static testmethod void test_ListApprovedPE() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();
            controller.DedicatedContact = testContact.Id;
            controller.ExpertiseAreaSector = 'Communications';
            controller.Save();
            System.assert(controller.ListApprovedPE.size() == 0);
        }
        Test.stopTest();
    }
    
    Static testmethod void test_ListExpertiseArea() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();
            controller.DedicatedContact = testContact.Id;
            controller.ExpertiseAreaSector = 'Communications';
            controller.Save();
            List<Partner_Expertise_Area__c> peList = controller.ListExpertiseArea;
            System.assertEquals(1, peList.size());
        }
        Test.stopTest();
    }
    
    Static testmethod void test_ExpertiseContacts() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();            
            System.assertEquals(1, controller.ExpertiseContacts.size());
        }
        Test.stopTest();
    }
    
    Static testmethod void test_ExpertiseSectorOptions() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('varAccountID', account.Id);
            EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();            
            System.assert(controller.ExpertiseSectorOptions.size() > 0);
        }
        Test.stopTest();
    }
    
    public Static void Setup()
    {                    
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        
        insert qtc;
        
        //Create Account
        account = TestDataFactory.createAccount('Test AccountName', qtc);
        insert account;
         
        //Create Contacts
        testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', account.Id);        
        insert testContact;
        
        // Retrieve Customer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'PRM - Sales Dependent Territory + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Users     
        testUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert testUser;
        
        testContact.OwnerId = testUser.Id; 
        update testContact;
        //Create ExpertiseArea
       // ExpertiseArea = EP_TestDataSetup.CreatExpertiseArea(account.Id, 'Communications', testContact);        
        //Create ExpertiseArea lookup object
        //ExpertLookup = EP_TestDataSetup.CreatExpertiseLookup(account.Id, ExpertiseArea.Id);  
    }
     Static testmethod void test_All() 
    {
      
        Test.startTest();
         EP_PartnerExpertiseController controller = new EP_PartnerExpertiseController();
         controller.BIsApproved=true;
         controller.ShowUnderReview=true;
         controller.Cancel();
        Test.stopTest();
    }
}