/**************************************************
* Author: Madhav Kakani Fluido Denmark
*
* Change Log:
* 2015-04-16 Madhav Kakani: Added test case for CR 20197
*        Initial Development
*
**************************************************/
@isTest(SeeAllData=true)
private class CaseTriggerTopicsTest {
    //Removed this test due to test failures related to the winter 16  release
    // Create a sample case and associate a topic to it
   static testmethod void testTopics(){
        /*User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        Account act = QTTestUtils.createMockAccount('Test Account', mockSysAdmin, true);
        system.assert(act.Id != null);
        
        Contact ct = QTTestUtils.createMockContact(act.Id);
        system.assert(ct.Id != null);        

        RecordType rt = [SELECT Id FROM RecordType WHERE Name='QlikTech Master Support Record Type'];
        system.assert(rt.Id != null);

        Case c = new Case();
        c.AccountId = act.Id;
        c.ContactId = ct.Id;
        c.RecordTypeId = rt.Id;
        c.Origin = 'Email';
        c.Subject = 'Test Case';
        c.Description = 'Test Case';
        c.Severity__c = '1';
        c.Status = 'Action Required';
        c.Priority = 'Urgent';
        c.Issue__c = 'Batch';
        c.Product__c = 'Desktop';
        c.Area__c = 'Installation';
        insert c;
        system.assert(c.Id != null); // should create the topics*/
        
        Test.startTest();

        List<Case> cs = [select Id, Description from Case 
            where RecordType.Name = 'QlikTech Master Support Record Type' and
            AccountId != null and
            ContactId != null and
            Contact.AccountId != null
            limit 1];
        if(cs.Size() == 0)
        {
            system.debug('No cases found, aborting!');
            return;
        }
        Case c = cs[0];
              
        // Check that two topics are created
        ConnectApi.TopicPage tpage = ConnectApi.Topics.getTopics(null, c.Id);
        // system.assert(tpage.topics.size() > 0); // Topic names Batch and Installation

        Test.stopTest();
    }
}