/******************************************************

    Test class: ContactQSD_Test
    
    Initiator: Ram
    
    Changelog:
        2017-03-06  Ram            Test class for QSD trigger ContactQSD 
                
******************************************************/
@isTest
private class ContactQSD_Test {
    
    @isTest static void taskCreationTest() {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.QSD_total_logins__c = 6;
        insert testContact;
        system.debug('ggg1'+testContact.QSD_total_logins__c);
        List<Contact> testContacts = [select id,QSD_total_logins__c  from contact where id = :testContact.ID];
        system.debug('ggg'+testContacts);
		Semaphores.TriggerHasRun(1);
        testContacts[0].QSD_total_logins__c= 15;
        system.debug('ggg3'+testContacts);
        update testContacts;
    }
   
}