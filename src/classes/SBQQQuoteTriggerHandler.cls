/*
    change log: IRN removed QuoteMaitanenceHandler call due to this is now done in process builder
    20161114 UIN Changes related to SOI insert/update for primary quotes
    16.03.2017 : Rodion Vakulovskyi : QCW-1890
    2017-05-17 MTM Set QCW-1633 OEM Subscription deals
    2017-07-07 MTM  QCW-2606  Address validations
    2017-08-01 Aslam QCW-2934 Changes
    2017-07-21 Oleksandr@4Front QCW-2396
    1.7 2017-08-03 Shubham QCW-1961 discarding stephan changes
    9/72017 Viktor@4Front QCW-3691
    22/09/2017 Rodion Vakulovskyi
    2017-11-1 Sergii Grushai - Q2CW-2948 Add call to processOppType after update
    2018-06-04 MTM BSL-490 New visual compliance implementations    
*/
public class SBQQQuoteTriggerHandler{
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public Static boolean IsAfterInsertProcessing = false;
    public Static boolean IsAfterUpdateProcessing = false;
    public Static boolean IsBeforeInsertProcessing = false;
    public Static boolean IsBeforeUpdateProcessing = false;
    private ECustomsHelper eCustomsHelper;
    public SBQQQuoteTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
        eCustomsHelper = new ECustomsHelper();
    }

    public void OnBeforeInsert(SBQQ__Quote__c[] newSBQQQuotes){
        if(!IsBeforeInsertProcessing){
            IsBeforeInsertProcessing = true;
            SBQQQuoteSetFields.PopulateOnAmendQuotes(newSBQQQuotes);
            CustomQuoteTriggerHandler.onBeforeInsert(Trigger.new);
            Quote_SetApproverFieldsHandler.onBeforeInsert(Trigger.new);
            SBQQQuoteSetFields.MarketValidation(newSBQQQuotes, null);
            SBQQQuoteSetFields.CheckAddresses(newSBQQQuotes, null);
            SBQQQuoteSetFields.PopulateProductFilterOnQuotes(newSBQQQuotes);
            SBQQQuoteSetFields.populatePartnerCategoryStatus(newSBQQQuotes); // Aslam QCW-2934
            SBQQQuoteSetFields.HandleQuoteSubscriptionFields(newSBQQQuotes);
        }
    }

    public void OnAfterInsert(SBQQ__Quote__c[] newSBQQQuotes, Map<ID, SBQQ__Quote__c> SBQQQuoteMap){
        if(!IsAfterInsertProcessing){
            IsAfterInsertProcessing = true;
            Set<Id> quoteToUpdate = new Set<Id>();
            List<SBQQ__Quote__c> lQuotesForSOI = new List<SBQQ__Quote__c>();
            Set<String> setOfOppIds = new Set<String>();
            List<String> addrId = new List<String>();
            for(SBQQ__Quote__c newQuote: newSBQQQuotes) {
                if(newQuote.SBQQ__Primary__c && String.isNotBlank(newQuote.Quote_Recipient__c)
                        && String.isNotBlank(newQuote.SBQQ__Opportunity2__c) ){
                    lQuotesForSOI.add(newQuote);
                    setOfOppIds.add(newQuote.SBQQ__Opportunity2__c);
                }

            }
            if(!lQuotesForSOI.isEmpty())
                //Method to create SOI for quote opportunity on primary quote insert
                SBBQQQuoteSOIHelper.callCreateUpdateSOI(lQuotesForSOI, setOfOppIds, new Set<String>());
            QuoteSharingRulesOnSellTPHandler.onAfterInsert(Trigger.new);
            CustomQuoteTriggerHandler.populateQuoteExists(trigger.new);//Shubham QCW-1961
        }
    }

    @future (callout=true)
    public static void OnAfterInsertAsync(Set<ID> quoteIds){
    }

    public void OnBeforeUpdate(SBQQ__Quote__c[] oldSBQQQuotes, SBQQ__Quote__c[] updatedSBQQQuotes, Map<ID, SBQQ__Quote__c> SBQQQuoteMap, Map<ID, SBQQ__Quote__c> OldSBQQQuoteMap){
        if(!IsBeforeUpdateProcessing)
        {
            IsBeforeUpdateProcessing = true;
            eCustomsHelper.CheckVisualCompliance(Trigger.new, OldSBQQQuoteMap);
            Quote_SetApproverFieldsHandler.onBeforeUpdate(Trigger.old, Trigger.new, SBQQQuoteMap);
            CustomQuoteTriggerHandler.onBeforeUpdate(Trigger.new, SBQQQuoteMap, OldSBQQQuoteMap);
            SBBQQQuoteSOIHelper.updQuoteOppSoi(updatedSBQQQuotes, OldSBQQQuoteMap);
            SBQQQuoteSetFields.MarketValidation(updatedSBQQQuotes, OldSBQQQuoteMap);
            SBQQQuoteSetFields.CheckAddresses(updatedSBQQQuotes, OldSBQQQuoteMap);
            SBQQQuoteSetFields.PopulateProductFilterOnQuotes(updatedSBQQQuotes);
            //Rodion changes : 16.03.2017 : QCW-1890
            SBQQQuoteSetFields.invoiceAddressCheck(updatedSBQQQuotes, OldSBQQQuoteMap);
            // end of Rodion changes
            SBQQQuoteSetFields.populatePartnerCategoryStatus(updatedSBQQQuotes); //QCW-2934
            SBQQQuoteSetFields.HandleQuoteSubscriptionFields(updatedSBQQQuotes);
        }
    }

     public void OnAfterUpdate(SBQQ__Quote__c[] oldSBQQQuotes, SBQQ__Quote__c[] updatedSBQQQuotes, Map<ID, SBQQ__Quote__c> SBQQQuoteMap, Map<ID, SBQQ__Quote__c> SBQQQuoteOldMap){
        if(!IsAfterUpdateProcessing)
        {
            IsAfterUpdateProcessing = true;// Viktor@4Front QCW-3691
            /*** UIN Changes for SOI ****/
            Set<Id> quoteToUpdate = new Set<Id>();
            CustomQuoteTriggerHandler.onAfterUpdate(updatedSBQQQuotes, SBQQQuoteMap, SBQQQuoteOldMap);
            Set<Id> quoteToCreateOrUpdateAssetFor = new Set<Id>();
            for(SBQQ__Quote__c oldQuote: oldSBQQQuotes)
            {
                SBQQ__Quote__c newQuote = SBQQQuoteMap.get(oldQuote.Id);
                if(!newQuote.Legal_Approval_Triggered__c) {
                    quoteToUpdate.add(newQuote.id);
                }
                if(newQuote.Closure_Trigger__c != oldQuote.Closure_Trigger__c && newQuote.Closure_Trigger__c == 'License Delivery')
                {
                    quoteToCreateOrUpdateAssetFor.Add(newQuote.Id);
                }

            }
            if(quoteToUpdate.size() > 0)

            {
                System.debug('going to Async');
                OnAfterUpdateAsync(quoteToUpdate);
            }
            if(quoteToCreateOrUpdateAssetFor.size() > 0){
                System.debug('before assetUtil ');
                AssetUtil assetUtil = new AssetUtil();
                assetUtil.createAsset(quoteToCreateOrUpdateAssetFor, SBQQQuoteMap);
            }
            QuoteSharingRulesOnSellTPHandler.onAfterUpdate(updatedSBQQQuotes, SBQQQuoteOldMap);
            System.debug(updatedSBQQQuotes);
            System.debug(SBQQQuoteOldMap);
            
              //Q2CW-2948
            CustomQuoteTriggerHandler.processOppType(updatedSBQQQuotes);
        }
    }

    @future (callout=true)
    public static void OnAfterUpdateAsync(Set<ID> quoteId){
        //ECustomsHelper.CheckRestrictedCompany(quoteId);
    }

    public void OnBeforeDelete(SBQQ__Quote__c[] SBQQQuotesToDelete, Map<ID, SBQQ__Quote__c> SBQQQuoteMap){


    }
    //Update Quote_Exists__c fields on related opportunity
    public void OnAfterDelete(SBQQ__Quote__c[] deletedSBQQQuotes, Map<ID, SBQQ__Quote__c> SBQQQuoteMap){
        Set<Id> oppIds = new Set<Id>();
        for(SBQQ__Quote__c item : deletedSBQQQuotes) {
            if(String.isNotBlank(item.SBQQ__Opportunity2__c))
                oppIds.add(item.SBQQ__Opportunity2__c);
        }
        if(!oppIds.isEmpty()){
            //List<Opportunity> lOpps = new List<Opportunity>([select id,name from opportunity where id in: oppIds]);
            Map<Id, Opportunity> mOpps = new Map<Id, Opportunity>([select id,name,Quote_Exists__c from opportunity where id in: oppIds]);
            List<SBQQ__Quote__c> lQuotes = new List<SBQQ__Quote__c>([select id,SBQQ__Opportunity2__c from SBQQ__Quote__c where SBQQ__Opportunity2__c in: oppIds]);
            Map<Id, List<SBQQ__Quote__c>> mOppIdQuotes = new Map<Id, List<SBQQ__Quote__c>>();
            for(SBQQ__Quote__c quote:lQuotes){
                if(mOppIdQuotes.containskey(quote.SBQQ__Opportunity2__c))
                    mOppIdQuotes.get(quote.SBQQ__Opportunity2__c).add(quote);
                else
                    mOppIdQuotes.put(quote.SBQQ__Opportunity2__c, new List<SBQQ__Quote__c>{quote});
            }
            List<Opportunity> oppsForUpd = new List<Opportunity>();
            if(!mOpps.isEmpty()){
                for(Id oppId:oppIds){
                    if(mOppIdQuotes.containsKey(oppId)){
                        if(!mOpps.get(oppId).Quote_Exists__c){
                            mOpps.get(oppId).Quote_Exists__c = true;
                            oppsForUpd.add(mOpps.get(oppId));
                        }
                    }else{
                        if(mOpps.get(oppId).Quote_Exists__c){
                            mOpps.get(oppId).Quote_Exists__c = false;
                            oppsForUpd.add(mOpps.get(oppId));
                        }
                    }
                }
                if(!oppsForUpd.isEmpty())
                    update oppsForUpd;
            }
        }
    }



    @future public static void OnAfterDeleteAsync(Set<ID> deletedSBQQQuoteIDs){

    }

    public void OnUndelete(SBQQ__Quote__c[] restoredSBQQQuotes){

    }

    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }

    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }
}