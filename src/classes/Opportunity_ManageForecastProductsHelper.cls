/************************************************************************
*
*   Opportunity_ManageForecastProductsHelper.cls
*   
*   Helper classes for Opportunity_ManageForecastProducts.trigger    
*       
*   Changelog:
*       2013-03-04  CCE     Initial development. CR 7524 https://eu1.salesforce.com/a0CD000000XN6U6
*       2013-06-10  CCE     Changed as CR requirements have changed
*                 
*                
*************************************************************************/
public class Opportunity_ManageForecastProductsHelper {

    public static OpportunityLineItem CreateProduct(Opportunity Opp, string ProductType, decimal Val, list<PricebookEntry> ProductList) {
        
        System.Debug('Opportunity_ManageForecastProductsHelper: ProductList.size() = ' + ProductList.size());   
        //Search the ProductList for the PricebookEntry to use
        PricebookEntry pbeToUse = new PricebookEntry();
        for (PricebookEntry pbe : ProductList)
        {
            if ((pbe.Name == ProductType) && (pbe.CurrencyIsoCode == Opp.CurrencyIsoCode))
            {
                pbeToUse = pbe;
                System.Debug('Opportunity_ManageForecastProductsHelper: pbeToUse = ' + pbeToUse);                   
            }
        }
        System.Debug('Opportunity_ManageForecastProductsHelper: pbeToUse2 = ' + pbeToUse);  
        if (pbeToUse.Name == null)
        {
            System.Debug('Opportunity_ManageForecastProductsHelper: No PricebookEntry found');          
        }
        else
        {       
            OpportunityLineItem oli = new OpportunityLineItem
            (
                OpportunityId = Opp.Id,
                PricebookEntryId = pbeToUse.Id,
                Quantity = 1,
                UnitPrice = Val
            );
            System.Debug('Opportunity_ManageForecastProductsHelper: Create: oli = ' + oli);
            return oli;
        }
        
        return null;
    }
    
    
/* //Original CreateProduct method took account of possible currency conversion
        public static OpportunityLineItem CreateProduct(Opportunity Opp, string ProductType, decimal Val, boolean ValIsFromOpp, Map<string, decimal> IsoToConvert, list<PricebookEntry> ProductList) {
        decimal DefaultAmount = 0; 
        
        //Generate the default amount to be used for the License_Forecast_Amount__c (currency adjusted)
        DefaultAmount = Val;
        //if the Val is from an Opp it will be in the correct currency otherwise it will be in USD and will need converting to the Opp currency
        if (ValIsFromOpp == false) {
            if (Opp.CurrencyIsoCode != 'USD') {
                if (IsoToConvert.containsKey(Opp.CurrencyIsoCode)) {
                    DefaultAmount = Val * IsoToConvert.get(Opp.CurrencyIsoCode);
                }
            }
        }
                
        //Search the ProductList for the PricebookEntry to use
        PricebookEntry pbeToUse = new PricebookEntry();
        for (PricebookEntry pbe : ProductList)
        {
            if ((pbe.Name == ProductType) && (pbe.CurrencyIsoCode == Opp.CurrencyIsoCode))
            {
                pbeToUse = pbe;
                System.Debug('Opportunity_ManageForecastProductsHelper: pbeToUse = ' + pbeToUse);                   
            }
        }
        System.Debug('Opportunity_ManageForecastProductsHelper: pbeToUse2 = ' + pbeToUse);  
        if (pbeToUse.Name == null)
        {
            System.Debug('Opportunity_ManageForecastProductsHelper: No PricebookEntry found');          
        }
        else
        {       
            OpportunityLineItem oli = new OpportunityLineItem
            (
                OpportunityId = Opp.Id,
                PricebookEntryId = pbeToUse.Id,
                Quantity = 1,
                UnitPrice = DefaultAmount
            );
            System.Debug('Opportunity_ManageForecastProductsHelper: Create: oli = ' + oli);
            return oli;
        }
        
        return null;
    }
    
    // Original UpdateProduct methos took accout of currency conversion
    public static OpportunityLineItem UpdateProduct(Opportunity Opp, OpportunityLineItem oliLicenses, decimal Val, boolean ValIsFromOpp, Map<string, decimal> IsoToConvert) {
        decimal DefaultAmount = 0; 
        OpportunityLineItem oli = new OpportunityLineItem();
        
        oli = oliLicenses;
        //Generate the default amount to be used for the License_Forecast_Amount__c (currency adjusted)
        DefaultAmount = Val;
        //if the Val is from an Opp it will be in the correct currency otherwise it will be in USD and will need converting to the Opp currency
        if (ValIsFromOpp == false) {
            if (Opp.CurrencyIsoCode != 'USD') {
                if (IsoToConvert.containsKey(Opp.CurrencyIsoCode)) {
                    DefaultAmount = Val * IsoToConvert.get(Opp.CurrencyIsoCode);
                }
            }
        }
        
        oli.UnitPrice = DefaultAmount;
        System.Debug('Opportunity_ManageForecastProductsHelper: Update: oli = ' + oli);
        return oli;        
    }*/
    
}