/*
* File PostRefreshHandlerTest
    * @description : Unit test for PostRefreshHandler
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       05.02.2018   Pramod Kumar V      Created Class
    2       28.04.2018   Pramod Kumar v      ITRM-60 Added assert methods
    3       06.08.2018   ext_bjd             QAR-64 Added additional checks for QA, DEVQ, PROD
    4       06.03.2019   ext_bjd             ITRM-320 Added method getUrlForInstance to determine instance url and update
    *                                        custom settings Support_Portal_Login_Url__c and Support_Portal_Url__c
    5       11.06.2019   ext_bjd             Added fix to avoid System.LimitException: Too many query rows: 50001 from
    *                                        CustomUserTriggerHandler.processUserAssignment

*/
@isTest
public class Sandbox_RefreshTest {

    @isTest
    static void testCustomSettingsUpdateForDefaultValuesOnSandboxNotDefinedOnPostRefreshData() {
        QTCustomSettings__c qtSettingDef = QTTestUtils.createQTCustomSettingsTestData();
        Steelbrick_Settings__c sbSettingDef = QTTestUtils.createSteelBrickSettingsTestData();
        QVM_Settings__c qvmSettingDef = QTTestUtils.createQVMSettingsTestData();
        QS_Partner_Portal_Urls__c qsSettingDef = QTTestUtils.createQS_PartnerPortalUrlsTestData();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        //disable CustomUserTriggerHandler.processUserAssignment
        Q2CWSettings__c customSettings = Q2CWSettings__c.getInstance(UserInfo.getOrganizationId());
        if (customSettings.Disable_User_trigger__c == false) {
            customSettings.Disable_User_trigger__c = true;
            upsert customSettings;
        }

        Test.startTest();

        Sandbox_Refresh classInstance = new Sandbox_Refresh();
        System.Test.testSandboxPostCopyScript(
                classInstance, UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());

        Test.stopTest();

        RefreshSandboxData__mdt refSand = testGetRefreshSandboxDataBySandboxName('test');
        System.debug('!!! Refresh Sandbox Data are : ' + refSand);

        QTCustomSettings__c updatedQt = [
                SELECT BoomiBaseURL__c, ULC_Base_URL__c
                FROM QTCustomSettings__c
                WHERE Id = :qtSettingDef.Id
        ];
        System.debug('!!! updatedQt : ' + updatedQt);
        Steelbrick_Settings__c updatedSb = [
                SELECT BoomiBaseURL__c, SpringAccountPrefix__c, CMInstanceUrl__c
                FROM Steelbrick_Settings__c
                WHERE Id = :sbSettingDef.Id
        ];
        System.debug('!!! updatedSB : ' + updatedSb);
        QVM_Settings__c updatedQvm = [
                SELECT QlikMarket_Website__c
                FROM QVM_Settings__c
                WHERE Id = :qvmSettingDef.Id
        ];
        System.debug('!!! updatedQvm : ' + updatedQvm);
        QS_Partner_Portal_Urls__c updatedQs = [
                SELECT Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c,
                        Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c
                FROM QS_Partner_Portal_Urls__c
                WHERE Id = :qsSettingDef.Id
        ];
        System.debug('!!! updatedQs : ' + updatedQs);

        //Debug statements
        Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        String defineEnvironmentName = UserInfo.getUserName();
        Boolean isQaSandbox = QTTestUtils.checkIfIsSandboxOrNotQa(defineEnvironmentName);
        if (IsSandbox && isQaSandbox != true) {
            System.debug('qvmSetting-->' + qvmSettingDef);
            System.debug('BoomiQS-->' + updatedQt.BoomiBaseURL__c);
            System.debug('ULC_Base_URL__c-->' + refSand.ULC_Base_URL__c);

            System.assertEquals(refSand.ULC_Base_URL__c, updatedQt.ULC_Base_URL__c);
            System.assertEquals(refSand.BoomiBaseURL__c, updatedQt.BoomiBaseURL__c);

            System.assertEquals(refSand.BoomiBaseURL__c, updatedSb.BoomiBaseURL__c);
            System.assertEquals(refSand.CMInstanceUrl__c, updatedSb.CMInstanceUrl__c);
            System.assertEquals(refSand.SpringAccountPrefix__c, updatedSb.SpringAccountPrefix__c);

            System.assertEquals(refSand.QlikMarket_Website__c, updatedQvm.QlikMarket_Website__c);

            System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : refSand.Support_Portal_Login_Url__c, updatedQs.Support_Portal_Login_Url__c);
            System.assertEquals(refSand.Support_Portal_CSS_Base__c, updatedQs.Support_Portal_CSS_Base__c);
            System.assertEquals(refSand.Support_Portal_index_allow_options__c, updatedQs.Support_Portal_index_allow_options__c);
            System.assertEquals(refSand.Support_Portal_Live_Agent_API_Endpoint__c, updatedQs.Support_Portal_Live_Agent_API_Endpoint__c);
            System.assertEquals(refSand.Support_Portal_Login_Page_Url__c, updatedQs.Support_Portal_Login_Page_Url__c);
            System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : refSand.Support_Portal_Url__c, updatedQs.Support_Portal_Url__c);
            System.assertEquals(refSand.Support_Portal_Logout_Page_Url__c, updatedQs.Support_Portal_Logout_Page_Url__c);
            System.assertEquals(refSand.Support_Portal_Url_Base__c, updatedQs.Support_Portal_Url_Base__c);
        }
    }

    @isTest
    static void testCustomSettingsUpdateForQa() {
        QTCustomSettings__c qtSettingDef = QTTestUtils.createQTCustomSettingsTestData();
        Steelbrick_Settings__c sbSettingDef = QTTestUtils.createSteelBrickSettingsTestData();
        QVM_Settings__c qvmSettingDef = QTTestUtils.createQVMSettingsTestData();
        QS_Partner_Portal_Urls__c qsSettingDef = QTTestUtils.createQS_PartnerPortalUrlsTestData();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        //disable CustomUserTriggerHandler.processUserAssignment
        Q2CWSettings__c customSettings = Q2CWSettings__c.getInstance(UserInfo.getOrganizationId());
        if (customSettings.Disable_User_trigger__c == false) {
            customSettings.Disable_User_trigger__c = true;
            upsert customSettings;
        }

        Test.startTest();

        Sandbox_Refresh classInstance = new Sandbox_Refresh();
        System.Test.testSandboxPostCopyScript(
                classInstance, UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());

        Test.stopTest();

        RefreshSandboxData__mdt refSand = testGetRefreshSandboxDataBySandboxName('QA');
        System.debug('!!! Refresh Sandbox Data are : ' + refSand);

        QTCustomSettings__c updatedQt = [
                SELECT BoomiBaseURL__c, ULC_Base_URL__c
                FROM QTCustomSettings__c
                WHERE Id = :qtSettingDef.Id
        ];
        System.debug('!!! updatedQt : ' + updatedQt);
        Steelbrick_Settings__c updatedSb = [
                SELECT BoomiBaseURL__c, SpringAccountPrefix__c, CMInstanceUrl__c
                FROM Steelbrick_Settings__c
                WHERE Id = :sbSettingDef.Id
        ];
        System.debug('!!! updatedSB : ' + updatedSb);
        QVM_Settings__c updatedQvm = [
                SELECT QlikMarket_Website__c
                FROM QVM_Settings__c
                WHERE Id = :qvmSettingDef.Id
        ];
        System.debug('!!! updatedQvm : ' + updatedQvm);
        QS_Partner_Portal_Urls__c updatedQs = [
                SELECT Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c,
                        Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c
                FROM QS_Partner_Portal_Urls__c
                WHERE Id = :qsSettingDef.Id
        ];
        System.debug('!!! updatedQs : ' + updatedQs);

        //Debug statements
        Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        String defineEnvironmentName = UserInfo.getUserName();
        Boolean isQaSandbox = QTTestUtils.checkIfIsSandboxOrNotQa(defineEnvironmentName);
        if (IsSandbox && isQaSandbox) {
            System.debug('qvmSetting-->' + qvmSettingDef);
            System.debug('BoomiQS-->' + updatedQt.BoomiBaseURL__c);
            System.debug('ULC_Base_URL__c-->' + refSand.ULC_Base_URL__c);

            System.assertEquals(refSand.ULC_Base_URL__c, updatedQt.ULC_Base_URL__c);
            System.assertEquals(refSand.BoomiBaseURL__c, updatedQt.BoomiBaseURL__c);

            System.assertEquals(refSand.BoomiBaseURL__c, updatedSb.BoomiBaseURL__c);
            System.assertEquals(refSand.CMInstanceUrl__c, updatedSb.CMInstanceUrl__c);
            System.assertEquals(refSand.SpringAccountPrefix__c, updatedSb.SpringAccountPrefix__c);

            System.assertEquals(refSand.QlikMarket_Website__c, updatedQvm.QlikMarket_Website__c);

            System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : refSand.Support_Portal_Login_Url__c, updatedQs.Support_Portal_Login_Url__c);
            System.assertEquals(refSand.Support_Portal_CSS_Base__c, updatedQs.Support_Portal_CSS_Base__c);
            System.assertEquals(refSand.Support_Portal_index_allow_options__c, updatedQs.Support_Portal_index_allow_options__c);
            System.assertEquals(refSand.Support_Portal_Live_Agent_API_Endpoint__c, updatedQs.Support_Portal_Live_Agent_API_Endpoint__c);
            System.assertEquals(refSand.Support_Portal_Login_Page_Url__c, updatedQs.Support_Portal_Login_Page_Url__c);
            System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : refSand.Support_Portal_Url__c, updatedQs.Support_Portal_Url__c);
            System.assertEquals(refSand.Support_Portal_Logout_Page_Url__c, updatedQs.Support_Portal_Logout_Page_Url__c);
            System.assertEquals(refSand.Support_Portal_Url_Base__c, updatedQs.Support_Portal_Url_Base__c);
        }
    }

    @isTest
    static void testCustomSettingsUpdateForDevq() {
        QTCustomSettings__c qtSettingDef = QTTestUtils.createQTCustomSettingsTestData();
        Steelbrick_Settings__c sbSettingDef = QTTestUtils.createSteelBrickSettingsTestData();
        QVM_Settings__c qvmSettingDef = QTTestUtils.createQVMSettingsTestData();
        QS_Partner_Portal_Urls__c qsSettingDef = QTTestUtils.createQS_PartnerPortalUrlsTestData();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        //disable CustomUserTriggerHandler.processUserAssignment
        Q2CWSettings__c customSettings = Q2CWSettings__c.getInstance(UserInfo.getOrganizationId());
        if (customSettings.Disable_User_trigger__c == false) {
            customSettings.Disable_User_trigger__c = true;
            upsert customSettings;
        }

        Test.startTest();

        Sandbox_Refresh classInstance = new Sandbox_Refresh();
        System.Test.testSandboxPostCopyScript(
                classInstance, UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());

        Test.stopTest();

        RefreshSandboxData__mdt refSand = testGetRefreshSandboxDataBySandboxName('DEVQ');
        System.debug('!!! Refresh Sandbox Data are : ' + refSand);

        QTCustomSettings__c updatedQt = [
                SELECT BoomiBaseURL__c, ULC_Base_URL__c
                FROM QTCustomSettings__c
                WHERE Id = :qtSettingDef.Id
        ];
        System.debug('!!! updatedQt : ' + updatedQt);
        Steelbrick_Settings__c updatedSb = [
                SELECT BoomiBaseURL__c, SpringAccountPrefix__c, CMInstanceUrl__c
                FROM Steelbrick_Settings__c
                WHERE Id = :sbSettingDef.Id
        ];
        System.debug('!!! updatedSB : ' + updatedSb);
        QVM_Settings__c updatedQvm = [
                SELECT QlikMarket_Website__c
                FROM QVM_Settings__c
                WHERE Id = :qvmSettingDef.Id
        ];
        System.debug('!!! updatedQvm : ' + updatedQvm);
        QS_Partner_Portal_Urls__c updatedQs = [
                SELECT Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c,
                        Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c
                FROM QS_Partner_Portal_Urls__c
                WHERE Id = :qsSettingDef.Id
        ];
        System.debug('!!! updatedQs : ' + updatedQs);

        //Debug statements
        Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        String defineEnvironmentName = UserInfo.getUserName();
        Boolean isQaSandbox = QTTestUtils.checkIfIsSandboxOrNotQa(defineEnvironmentName);
        if (IsSandbox && isQaSandbox) {
            System.debug('qvmSetting-->' + qvmSettingDef);
            System.debug('BoomiQS-->' + updatedQt.BoomiBaseURL__c);
            System.debug('ULC_Base_URL__c-->' + refSand.ULC_Base_URL__c);

            System.assertEquals(refSand.ULC_Base_URL__c, updatedQt.ULC_Base_URL__c);
            System.assertEquals(refSand.BoomiBaseURL__c, updatedQt.BoomiBaseURL__c);

            System.assertEquals(refSand.BoomiBaseURL__c, updatedSb.BoomiBaseURL__c);
            System.assertEquals(refSand.CMInstanceUrl__c, updatedSb.CMInstanceUrl__c);
            System.assertEquals(refSand.SpringAccountPrefix__c, updatedSb.SpringAccountPrefix__c);

            System.assertEquals(refSand.QlikMarket_Website__c, updatedQvm.QlikMarket_Website__c);

            System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : refSand.Support_Portal_Login_Url__c, updatedQs.Support_Portal_Login_Url__c);
            System.assertEquals(refSand.Support_Portal_CSS_Base__c, updatedQs.Support_Portal_CSS_Base__c);
            System.assertEquals(refSand.Support_Portal_index_allow_options__c, updatedQs.Support_Portal_index_allow_options__c);
            System.assertEquals(refSand.Support_Portal_Live_Agent_API_Endpoint__c, updatedQs.Support_Portal_Live_Agent_API_Endpoint__c);
            System.assertEquals(refSand.Support_Portal_Login_Page_Url__c, updatedQs.Support_Portal_Login_Page_Url__c);
            System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : refSand.Support_Portal_Url__c, updatedQs.Support_Portal_Url__c);
            System.assertEquals(refSand.Support_Portal_Logout_Page_Url__c, updatedQs.Support_Portal_Logout_Page_Url__c);
            System.assertEquals(refSand.Support_Portal_Url_Base__c, updatedQs.Support_Portal_Url_Base__c);
        }
    }

    @isTest
    static void testCustomSettingsUpdateForLive() {
        QTCustomSettings__c qtSetting = QTTestUtils.createQTCustomSettingsTestDataForLive();
        Steelbrick_Settings__c sbSetting = QTTestUtils.createSteelBrickSettingsTestDataForLive();
        QVM_Settings__c qvmSetting = QTTestUtils.createQVMSettingsTestDataForLive();
        QS_Partner_Portal_Urls__c qsSetting = QTTestUtils.createQS_PartnerPortalUrlsTestDataForLive();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        //disable CustomUserTriggerHandler.processUserAssignment
        Q2CWSettings__c customSettings = Q2CWSettings__c.getInstance(UserInfo.getOrganizationId());
        if (customSettings.Disable_User_trigger__c == false) {
            customSettings.Disable_User_trigger__c = true;
            upsert customSettings;
        }

        Test.startTest();

        Sandbox_Refresh classInstance = new Sandbox_Refresh();
        System.Test.testSandboxPostCopyScript(
                classInstance, UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());

        Test.stopTest();

        QTCustomSettings__c updatedQt = [
                SELECT BoomiBaseURL__c, ULC_Base_URL__c
                FROM QTCustomSettings__c
                WHERE Id = :qtSetting.Id
        ];
        System.debug('!!! updatedQt : ' + updatedQt);
        Steelbrick_Settings__c updatedSb = [
                SELECT BoomiBaseURL__c, SpringAccountPrefix__c, CMInstanceUrl__c
                FROM Steelbrick_Settings__c
                WHERE Id = :sbSetting.Id
        ];
        System.debug('!!! updatedSB : ' + updatedSb);
        QVM_Settings__c updatedQvm = [
                SELECT QlikMarket_Website__c
                FROM QVM_Settings__c
                WHERE Id = :qvmSetting.Id
        ];
        System.debug('!!! updatedQvm : ' + updatedQvm);
        QS_Partner_Portal_Urls__c updatedQs = [
                SELECT Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c,
                        Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, Support_Portal_Logout_Page_Url__c
                FROM QS_Partner_Portal_Urls__c
                WHERE Id = :qsSetting.Id
        ];
        System.debug('!!! updatedQs : ' + updatedQs);

        //Debug statements
        Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        if (!IsSandbox) {
            System.debug('qvmSetting-->' + qvmSetting);
            System.debug('BoomiQS-->' + updatedQt.BoomiBaseURL__c);

            System.assertEquals(qtSetting.ULC_Base_URL__c, updatedQt.ULC_Base_URL__c);
            System.assertEquals(qtSetting.BoomiBaseURL__c, updatedQt.BoomiBaseURL__c);

            System.assertEquals(sbSetting.BoomiBaseURL__c, updatedSb.BoomiBaseURL__c);
            System.assertEquals(sbSetting.CMInstanceUrl__c, updatedSb.CMInstanceUrl__c);
            System.assertEquals(sbSetting.SpringAccountPrefix__c, updatedSb.SpringAccountPrefix__c);

            System.assertEquals(qvmSetting.QlikMarket_Website__c, updatedQvm.QlikMarket_Website__c);

            //System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : qsSetting.Support_Portal_Login_Url__c, updatedQs.Support_Portal_Login_Url__c);
            System.assertEquals(qsSetting.Support_Portal_CSS_Base__c, updatedQs.Support_Portal_CSS_Base__c);
            System.assertEquals(qsSetting.Support_Portal_index_allow_options__c, updatedQs.Support_Portal_index_allow_options__c);
            System.assertEquals(qsSetting.Support_Portal_Live_Agent_API_Endpoint__c, updatedQs.Support_Portal_Live_Agent_API_Endpoint__c);
            System.assertEquals(qsSetting.Support_Portal_Login_Page_Url__c, updatedQs.Support_Portal_Login_Page_Url__c);
            //System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : qsSetting.Support_Portal_Url__c, updatedQs.Support_Portal_Url__c);
            System.assertEquals(qsSetting.Support_Portal_Logout_Page_Url__c, updatedQs.Support_Portal_Logout_Page_Url__c);
            System.assertEquals(qsSetting.Support_Portal_Url_Base__c, updatedQs.Support_Portal_Url_Base__c);
        }
    }

    public static RefreshSandboxData__mdt testGetRefreshSandboxDataBySandboxName(String sandboxName) {
        RefreshSandboxData__mdt refSand = null;
        String defaultKeyValue = 'DEFAULTVALUES';
        Map<String, RefreshSandboxData__mdt> RefreshSandboxItem = new Map<String, RefreshSandboxData__mdt>();
        for (RefreshSandboxData__mdt item : [
                SELECT Id, MasterLabel, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c,
                        Support_Portal_Login_Url__c, Support_Portal_index_allow_options__c, Support_Portal_Url__c, Support_Portal_Url_Base__c,
                        Support_Portal_Logout_Page_Url__c, Support_Portal_CSS_Base__c, SpringAccountPrefix__c,
                        CMInstanceUrl__c, QlikMarket_Website__c, BoomiBaseURL__c, NSQuoteApprovals__c, ULC_Base_URL__c
                FROM RefreshSandboxData__mdt
        ]) {
            RefreshSandboxItem.put(item.MasterLabel.toUpperCase(), item);
        }
        if (RefreshSandboxItem.containsKey(sandboxName.toUpperCase())) {
            refSand = RefreshSandboxItem.get(sandboxName.toUpperCase());
        } else if (RefreshSandboxItem.containsKey(defaultKeyValue)) {
            refSand = RefreshSandboxItem.get(defaultKeyValue);
        }
        return refSand;
    }
}
