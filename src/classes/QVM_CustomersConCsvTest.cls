/*
	2015-05-07	TJG CR 33807 https://eu1.salesforce.com/a0CD000000sQx8W
        Provide Partners the ability to export just the data desired
    2019-03-05  AIN Moved custom settings to QuoteTestHelper
*/
@isTest
private class QVM_CustomersConCsvTest {
	
  static testMethod void test_ControllerCsv() {
        QTTestUtils.GlobalSetUp();

        System.debug('QVM_CustomersConCsvTest.test_ControllerCsv: Starting');

        test.startTest(); 

        QVM_CustomersConCsv conCsv = new QVM_CustomersConCsv();

        System.assertEquals(null, conCsv.CusProducts);

        //User jgltest = [select  Id from user where contactId <> null and Profile.Name like 'PRM%' limit 1];
        user jgltest = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');

        jgltest = [select id, name, Contact.Id, Contact.Account.Id from user where id =:jgltest.id];
        system.debug('jgltest.Contact.Id: ' + jgltest.Contact.Id);
        system.debug('jgltest.Contact.Account.Id: ' + jgltest.Contact.Account.Id);

        QVM_Partner_Data__c partner_data = new QVM_Partner_Data__c();
        partner_data.Partner_Account__c = jgltest.Contact.Account.Id;
        partner_data.Approved__c = true;
        insert partner_data;

        System.RunAs(jgltest){

        	PageReference pageRef = Page.QVM_Customers;
    		
    		Test.setCurrentPage(pageRef);

        	QVM_CustomersCon contrl = new QVM_CustomersCon();

        	PageReference p = contrl.exportCustomersExcel();

        	Test.setCurrentPage(p);

        	QVM_CustomersConCsv  cCsv = new QVM_CustomersConCsv();

        	System.assertNotEquals(null, cCsv.CusProducts);

        	PageReference exPage = cCsv.exportCustomersExcel();

        	System.assertNotEquals(null, exPage);
    	
    	}

        test.stopTest();

        System.debug('QVM_CustomersConCsvTest.test_ControllerCsv: Ended'); 
    } 	
}