/*************************************************************************

    Semaphores

    This class contains flags used to stop multiple trigger exexutions
    due to workflow updates

    Changelog:
        2011-12-15  MHG     Created method
                            CR #3660 - #Quick Marketing Request complete status trigger
                            https://eu1.salesforce.com/a0CD000000FytBn
        2012-08-17  CCE     Adding OppUpdateExecutiveSponsor_BatchIsRunning semaphore
                            to allow us to stop the lastModifiedUpdate.trigger from
                            making an @future callout while running a batch job/
                            CR# 5658 - New field on Opportunity page layout
                            https://eu1.salesforce.com/a0CD000000NIU0Y
        2012-10-16  SAN     Fix the unit test to pass the future call limit problem
        2013-01-09  CCE     Adding TestOppIfPartnerDealRecordPSMAtClosedWonHasRun semaphore
                            to allow us to stop the lastModifiedUpdate.trigger to fix the unit
                            test to pass the future call limit problem.
        2013-05-30  YKA     CR# 7899 :Prevent "duplicate" calls to Voucher creation.
                            To limit trigger Opportunity_CreateVoucherForNLRProds execution to once.
        2013-06-28  YKA     CR# 8269:   https://eu1.salesforce.com/a0CD000000Z7dKY
                            (Highlight Sev 1 Cases in Account chatter feed) Adding variable here as the CaseBusinessCritical after trigger is executing again and creating duplicate Chatter Feed message in some instances.
        2013-08-20  MTM     CR# 9273 https://eu1.salesforce.com/a0CD000000acQFI

        QTNext changes:
        2013-03-08  CCE   Adding Opportunity_ManageForecastProductsHasRun semaphore
              to stop recurrision when the trigger runs.
        2013-03-12  CCE     Adding Opportunity_ManageForecastProductsIsInsert semaphore to stop Opportunity_MandatorySoIOnUpdate firing when Opportunity_ManageForecastProducts runs.
        2013-04-19  MHG     Fixing Callout Gov. limit issue in lastModifiedUpdate.trigger
                            and Opportunity_Legal_CompanyCheck.trigger
        2013-04-24  RDZ     Adding Semaphores to avoid calling voucher management creation
                            twice for same opp Issue reported on: Case#00157080 https://eu1.salesforce.com/500D000000N6mJ8
                            CR# 7899 https://eu1.salesforce.com/a0CD000000YF9vC
                            Opportunity_VoucherManagementCreation
        2013-09-30  CCE     CR# 8968 Adding Opportunity_ManageForecastProductsHasRun_After semaphore to stop recurrision when the trigger runs.
                            and changing the name of Opportunity_ManageForecastProductsHasRun to Opportunity_ManageForecastProductsHasRun_Before
        //Changes to help reduce to many SOQL queries error message
        2013-10-01  CCE     CR# 8968 https://eu1.salesforce.com/a0CD000000ZwqHt Adding OppUpdateContactSOI_IsAfter flag to prevent duplicate calls when the trigger runs.
        2013-10-02  CCE     CR# 8968 https://eu1.salesforce.com/a0CD000000ZwqHt Adding Opportunity_OppPartnerContactTriggerHasRun flag to prevent duplicate calls when the trigger runs.
        2013-10-18  CCE     CR# 9755 https://eu1.salesforce.com/a0CD000000bSsB7 Adding TaskSetTypeHasRun flag to prevent recursion when the trigger runs.

        2013-10-15 MTM      CR# 8535 Update parent's split percentage as cumulative split percentages of children
                            Set children to Closed Won if Parent is Closed won
        2013-10-31  CCE     CR# 8968 https://eu1.salesforce.com/a0CD000000ZwqHt Adding OppUpdateAccOwnerNameHasRun flag to prevent duplicate calls when the trigger runs.
        2013-11-05  CCE     CR# 8968 https://eu1.salesforce.com/a0CD000000ZwqHt Adding Opportunity_ExecDealsVisibility_Insert flag to prevent duplicate calls when the trigger runs.
        2013-11-05  CCE     CR# 8968 https://eu1.salesforce.com/a0CD000000ZwqHt Adding Opportunity_ExecDealsVisibility_Update flag to prevent duplicate calls when the trigger runs.
        2013-10-18  CCE     CR# 9755 https://eu1.salesforce.com/a0CD000000bSsB7 Adding TaskSetTypeHasRun flag to prevent recursion when the trigger runs.
        2013-12-03  CCE     CR# 9755 https://eu1.salesforce.com/a0CD000000bSsB7 Adding TaskLeadFollowUp_HasRunBefore flag to prevent duplicate calls when the trigger runs.
        2013-12-03  CCE     CR# 9755 https://eu1.salesforce.com/a0CD000000bSsB7 Adding TaskLeadFollowUp_HasRunAfter flag to prevent duplicate calls when the trigger runs.
        2014-01-07  RDZ    CR# 10110   https://eu1.salesforce.com/a0CD000000dRzRV Adding  ChangeControlRequestorApprovalCheckHasRun to prevent duplicate calls when the trigger runs.
        2014-03-07  CCE     Added TriggerHasRun() and associated methods to help reduce "to many SOQL queries" issuse while working on CR# 10129 testing.
        2014-05-07  SAN     Added OpportunityMarketType_HasRun
        2014-05-16  MTM     Added  UpdateDealSplitChildOpp_BatchIsRunning
        2014-05-30 Madhav Kakani - Fluido Oy - Added boolean for CR# 10215
        2014-09-08  KMH     CR# 12644 https://eu1.salesforce.com/a0CD000000iErmb Added CaseUpdateSupportContact_HasRunAfterUpdate,CaseUpdateSupportContact_HasRunAfterDelete
        2014-12-09  SAN     QS_ Support Portal Project to avoid hitting SOQL limit on case trigger
        2015-03-12 Madhav Kakani - Fluido Denmark - Added boolean for CR# 20060 - Add Predictive Lead Score Grade and Overall Priority
        2016-06-08  TJG     Updated slightly for Opportunity trigger clean up
        2016-11-08  AIN     Added SetAllSemaphoresToTrue to set all semaphores to true.
        2017-04-10  Linus Löfberg (4front)   Added oppSourceUpdonLeadConv_HasRunAfter and LeadAcademicProgramConversion_HasRunAfter
        2017-04-26  AIN     Added
        2017-06-22 Rodion Vakulvoskyi added static flag to get rid of soql query limit
        2017-09-23 Shubham Gupta QCW-2849 Added SplitDealOpptyProduct at line 122
        2017-09-12  Anjuna  Added OpportunityQuoteUpdate.
        2017-11-01 Viktor@4Front QCW-3787, QCW-4218 static flags for quote trigger. Last Modified By Andreas Nilsson, 01/11/2017 10:14
        2017-11-13 AYS BMW-402 : Static flag variables for Lead trigger.
        2017-11-15  ext_vos CR#CHG0031444 Adding semaphores for PseProjectTrigger
        2017-11-16  ext_vos CR#CHG0031444 Adding semaphores for PseBudgetTrigger
        2017-11-23  ext_vos CR#CHG0030519 Adding semahpores for TaskTrigger
        2017-12-22  Shubham Gupta QCW-4610 Trigger Consolidation.
        2018-01-01 Viktor@4Front QCW-4609, QCW-4760 Trigger consolidation.
        29/5/2018 BSL-418 done some modifications trigger consolidation   shubham gupta
        2018-07-04 ext_vos CHG0033919: Adding semaphores for TrainingCardManagementTrigger
        2018-11-08 ext_vos CHG0034844: Adding semaphores for Diagnostic and Basic Knowledge Articles triggers.
        2018-12-03 CTS  SCU-5 Removal of variables used in Inactive Triggers.
        2019-01-18 Linus Löfberg - Adding semaphore variables for the Z_QuoteTrigger
        2019-05-22 ext_vos CHG0036145: Add semaphores for PseTimecardHeaderTrigger.

*************************************************************************/
public class Semaphores {

    // Semaphores for CustomAccountTrigger and Account handlers
    // Important: All semaphores relating to the account object must be placed here!
    // QCW-4609 Viktor@4Front
    public static Boolean CustomAccountTriggerBeforeInsert = false;
    public static Boolean CustomAccountTriggerAfterInsert = false;
    public static Boolean CustomAccountTriggerBeforeUpdate = false;
    public static Boolean CustomAccountTriggerAfterUpdate = false;
    public static Boolean CustomAccountTriggerBeforeDelete = false;
    public static Boolean CustomAccountTriggerAfterDelete = false;
    // QCW-4609 Viktor@4Front^
    // Semaphores for CustomAccountTrigger and Account handlers^

    // Semaphores for CustomCaseTrigger and Case handlers
    // Important: All semaphores relating to the Case object must be placed here!
    // QCW-4612 Viktor@4Front
    public static Boolean CustomCaseTriggerBeforeInsert = false;
    public static Boolean CustomCaseTriggerAfterInsert = false;
    public static Boolean CustomCaseTriggerBeforeUpdate = false;
    public static Boolean CustomCaseTriggerAfterUpdate = false;

    // QCW-4612 Viktor@4Front^
    // Semaphores for CustomCaseTrigger and Case handlers^

    public static Boolean UpdateMarketingRequestTriggerHasRun = false;
    public static Boolean OppUpdateExecutiveSponsor_BatchIsRunning = false;
    public static Boolean PPMarketingPortalClassTestHasRun = false;
    public static Boolean TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = false;
    public static Boolean AccountSharingRulesOnPartnerPortalAccountsHasRun = false;
    public static Boolean Opp_CVForNLRPTriggerRun = false;
    
    //CTS SCU-5 Removal of Inactive Triggers
    //public static Boolean CaseBusinessCritical_AfterRun = false;
    
    public static Boolean CaseTriggerBeforeUpdateHasRun = false;
    public static Boolean Opportunity_Legal_CompanyCheckHasRun = false;
    public static Boolean lastModifiedUpdateHasRun = false;
    public static Boolean Opportunity_VoucherManagementCreationHasRun = false;
    public static Boolean Opportunity_ManageForecastProductsHasRun_Before = false;
    public static Boolean Opportunity_ManageForecastProductsHasRun_After = false;
    public static Boolean Opportunity_ManageForecastProductsIsInsert = false;
    public static Boolean OppUpdateContactSOI_IsAfter = false;
    public static Boolean Opportunity_OppPartnerContactTriggerHasRun = false;
    public static Boolean TaskSetTypeHasRun = false;
    public static Boolean DealSplitUpdateHasRun = false;
    public static Boolean DealSplitPercentCalcHasRun = false;
    public static Boolean OppUpdateAccOwnerNameHasRun = false;
    public static Boolean Opportunity_ExecDealsVisibility_Insert = false;
    public static Boolean Opportunity_ExecDealsVisibility_Update = false;
    public static Boolean TaskLeadFollowUp_HasRunBefore = false;
    public static Boolean TaskLeadFollowUp_HasRunAfter = false;
    public static Boolean ChangeControlRequestorApprovalCheckHasRun = false;
    public static Boolean OpportunityMarketType_HasRun = false;
    public static Boolean UpdateDealSplitChildOpp_BatchIsRunning = false;
    public static Boolean OpportuntyChildCloneHasRun = false; // CR# 10215
    
    //CTS SCU-5 Removal of Inactive Triggers
    //public static Boolean CaseUpdateSupportContact_HasRunAfterUpdate = false;
    //public static Boolean CaseUpdateSupportContact_HasRunAfterDelete = false;
    
    public static Boolean CaseTrigger_HasRunAfterUpdate = false;
    
    //CTS SCU-5 Removal of Inactive Triggers
    //public static Boolean CaseClosedSendSurvey_HasRunAfterUpdate = false;
    
    public static Boolean LeadScoreTrigger_HasRunBefore = false;
    
    //CTS SCU-5 Removal of Inactive Triggers
    //public static Boolean ContactScoreTrigger_HasRunBefore = false;
    
    public static Boolean OppPreventUpdateForecastAmountsHandler_HasRun = false;
    public static Boolean OpportunityPartnerprogramHandler = false;
    public static Boolean oppSourceUpdonLeadConv_HasRunAfter = false;
    public static Boolean LeadAcademicProgramConversion_HasRunAfter = false;
    public static Boolean UserAddUserToChatterGroupsHasRun = false;
    public static Boolean OpportunityPopulatePricebookHandler = false;
    public static Boolean OpportunityTriggerBeforeUpdate = false;
    public static Boolean OpportunityTriggerAfterUpdate = false;
    public static Boolean OpportunityTriggerAfterInsert = false;
    public static Boolean OpportunityTriggerBeforeInsert = false;
    public static Boolean OpportunityLineItemTriggerBeforeUpdate = false;
    public static Boolean OpportunityLineItemTriggerAfterUpdate = false;
    public static Boolean OpportunityTriggerBeforeUpdate2 = false;
    public static Boolean OpportunityTriggerAfterUpdate2 = false;
    public static Boolean OpportunityLineItemTriggerAfterInsert = false;
    public static Boolean OpportunityLineItemTriggerBeforeInsert = false;
    public static Boolean AssetTriggerAfterInsert = false;
    public static Boolean SplitDealOpptyProduct = false;
    public static Boolean OpportunityQuoteUpdate = false;
    public static Boolean NoTwiceRun = false;
    public static Boolean PseProjectTriggerBeforeInsert = false;
    public static Boolean PseProjectTriggerBeforeUpdate = false;
    public static Boolean PseProjectTriggerAfterUpdate = false;
    public static Boolean PseProjectTriggerAfterInsert = false;
    public static Boolean PseProjectTriggerAfterDelete = false;
    public static Boolean PseBudgetTriggerAfterInsert = false;
    public static Boolean PseBudgetTriggerAfterUpdate = false;
    public static Boolean TaskTriggerAfterInsert = false;
    public static Boolean TaskTriggerAfterUpdate = false;

    //2017-11-01 Viktor@4Front
    public static Boolean SBQQQuoteTriggerHandlerBeforeInsert = false;
    public static Boolean SBQQQuoteTriggerHandlerAfterInsert = false;
    public static Boolean SBQQQuoteTriggerHandlerBeforeUpdate = false;
    public static Boolean SBQQQuoteTriggerHandlerAfterUpdate = false;
    //2017-11-01 Viktor@4Front ^

    public static Boolean Z_QuoteTriggerBeforeInsert = false;
    public static Boolean Z_QuoteTriggerAfterInsert = false;
    public static Boolean Z_QuoteTriggerBeforeUpdate = false;
    public static Boolean Z_QuoteTriggerAfterUpdate = false;

    public static Boolean Z_QuoteSummaryTriggerBeforeInsert = false;
    public static Boolean Z_QuoteSummaryTriggerAfterInsert = false;
    public static Boolean Z_QuoteSummaryTriggerBeforeUpdate = false;
    public static Boolean Z_QuoteSummaryTriggerAfterUpdate = false;
    

    //2017-11-13 AYS BMW-402
    public static Boolean LeadTriggerHandlerBeforeInsert = false;
    public static Boolean LeadTriggerHandlerAfterInsert = false;
    public static Boolean LeadTriggerHandlerBeforeUpdate = false;
    public static Boolean LeadTriggerHandlerBeforeUpdate2 = false;
    public static Boolean LeadTriggerHandlerAfterUpdate = false;
    //2017-11-13 AYS BMW-402^

    //QCW-4610 start
    public static Boolean ContactTriggerHandlerBeforeInsert = false;
    public static Boolean ContactTriggerHandlerAfterInsert = false;
    public static Boolean ContactTriggerHandlerBeforeUpdate = false;
    public static Boolean ContactTriggerHandlerAfterUpdate = false;
    public static Boolean ContactTriggerHandlerBeforeDelete = false;
    public static Boolean ContactTriggerHandlerAfterDelete = false;
    //QCW-4610 End

    //ext_vos CHG0033919
    public static Boolean TrainingCardManagementTriggerAfterInsert = false;   
    
    public static Boolean CaseTriggerAfter = false;

    //BSL-1736 - update voucher status for training card product
    public static Boolean VoucherManagementUpdated = false;

    //ext_vos CHG0034844
    public static Boolean BasicKnowledgeArticlesTriggerBeforeInsert = false;
    public static Boolean BasicKnowledgeArticlesTriggerBeforeUpdate = false;
    public static Boolean DiagnosticKnowledgeArticlesTriggerBeforeInsert = false;
    public static Boolean DiagnosticKnowledgeArticlesTriggerBeforeUpdate = false;

    //ext_vos CHG0036145
    public static Boolean PseTimecardHeaderTriggerAfterInsert = false;
    public static Boolean PseTimecardHeaderTriggerAfterUpdate = false;
    //ext_ciz CHG0036145
    public static Boolean PseTimecardHeaderTriggerAfterDelete = false;

    private static Set<string> compoundDetail = new Set<string>();
    //CCE This method can be used to wrap a trigger so that each before/after insert/update will be only run once
    //Do not use this if you want a trigger to be re-run after a workflow update

    //Will make TriggerHasRun always return true if true, blocking the trigger.
    private static boolean blockTriggers = false;

    public static Boolean TriggerHasRun(string triggerName) {
        //System.debug('Semaphores triggerName = ' + triggerName);
        if (blockTriggers)
            return true;
        string strDetail = triggerName;
        if (Trigger.isExecuting) {
            if (Trigger.isAfter) strDetail += 'isAfter';
            if (Trigger.isBefore) strDetail += 'isBefore';
            if (Trigger.isInsert) strDetail += 'isInsert';
            if (Trigger.isUpdate) strDetail += 'isUpdate';
        }
        System.debug('Semaphores strDetail = ' + strDetail);
        //System.debug('Semaphores compoundDetail.size = ' + compoundDetail.size());
        if (!compoundDetail.contains(strDetail)) {
            compoundDetail.add(strDetail);
            //System.debug('Semaphores compoundDetail = ' + compoundDetail);
            return false;
        }
        return true;
    }

    //CCE When writing test classes for your trigger you may need to clear the compoundDetail Set of any entries related to your trigger
    //to do this call this method with the trigger name and clearSet = 1
    public static void TriggerHasRun(string triggerName, integer clearSet) {
        //System.debug('Semaphores triggerName = ' + triggerName);
        if (clearSet == 1) {
            Set<string> compoundClear = new Set<string>{
                    triggerName + 'isAfter',
                    triggerName + 'isAfterisInsert',
                    triggerName + 'isAfterisUpdate',
                    triggerName + 'isAfterisInsertisUpdate',
                    triggerName + 'isAfterisUpdateisInsert',
                    triggerName + 'isBefore',
                    triggerName + 'isBeforeisInsert',
                    triggerName + 'isBeforeisUpdate',
                    triggerName + 'isBeforeisInsertisUpdate',
                    triggerName + 'isBeforeisUpdateisInsert'
            };
            compoundDetail.removeAll(compoundClear);
            //System.debug('Semaphores compoundDetail.size = ' + compoundDetail.size());
        }
    }

    //CCE When writing test classes for your trigger you may need to clear the compoundDetail Set of all entries
    //to do this call this method with clearSet = 1
    public static void TriggerHasRun(integer clearSet) {
        //System.debug('Semaphores clearSet = ' + clearSet);
        //System.debug('Semaphores compoundDetail.size before = ' + compoundDetail.size());
        if (clearSet == 1) compoundDetail.clear();
        //System.debug('Semaphores compoundDetail.size after = ' + compoundDetail.size());

    }
    //Sets all the Semaphores to true for tests, also blocks triggers not blocked by booleans that use TriggerHasRun
    public static void SetAllSemaphoresToTrue() {

        blockTriggers = true;

        Z_QuoteTriggerBeforeInsert = true;
        Z_QuoteTriggerAfterInsert = true;
        Z_QuoteTriggerBeforeUpdate = true;
        Z_QuoteTriggerAfterUpdate = true;

        // QCW-4609 Viktor@4Front
        CustomAccountTriggerBeforeInsert = true;
        CustomAccountTriggerAfterInsert = true;
        CustomAccountTriggerBeforeUpdate = true;
        CustomAccountTriggerAfterUpdate = true;
        CustomAccountTriggerBeforeDelete = true;
        CustomAccountTriggerAfterDelete = true;

        CustomCaseTriggerBeforeInsert = true;
        CustomCaseTriggerAfterInsert = true;
        CustomCaseTriggerBeforeUpdate = true;
        CustomCaseTriggerAfterUpdate = true;
        // QCW-4609 Viktor@4Front^

        UpdateMarketingRequestTriggerHasRun = true;
        OppUpdateExecutiveSponsor_BatchIsRunning = true;
        PPMarketingPortalClassTestHasRun = true;
        TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = true;
        AccountSharingRulesOnPartnerPortalAccountsHasRun = true;
        Opp_CVForNLRPTriggerRun = true;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //CaseBusinessCritical_AfterRun = true;
        
        CaseTriggerBeforeUpdateHasRun = true;
        Opportunity_Legal_CompanyCheckHasRun = true;
        lastModifiedUpdateHasRun = true;
        Opportunity_VoucherManagementCreationHasRun = true;
        Opportunity_ManageForecastProductsHasRun_Before = true;
        Opportunity_ManageForecastProductsHasRun_After = true;
        Opportunity_ManageForecastProductsIsInsert = true;
        OppUpdateContactSOI_IsAfter = true;
        Opportunity_OppPartnerContactTriggerHasRun = true;
        TaskSetTypeHasRun = true;
        DealSplitUpdateHasRun = true;
        DealSplitPercentCalcHasRun = true;
        OppUpdateAccOwnerNameHasRun = true;
        Opportunity_ExecDealsVisibility_Insert = true;
        Opportunity_ExecDealsVisibility_Update = true;
        TaskLeadFollowUp_HasRunBefore = true;
        TaskLeadFollowUp_HasRunAfter = true;
        ChangeControlRequestorApprovalCheckHasRun = true;
        OpportunityMarketType_HasRun = true;
        UpdateDealSplitChildOpp_BatchIsRunning = true;
        OpportuntyChildCloneHasRun = true;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //CaseUpdateSupportContact_HasRunAfterUpdate = true;
        //CaseUpdateSupportContact_HasRunAfterDelete = true;
        
        CaseTrigger_HasRunAfterUpdate = true;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //CaseClosedSendSurvey_HasRunAfterUpdate = true;
        
        LeadScoreTrigger_HasRunBefore = true;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //ContactScoreTrigger_HasRunBefore = true;
        
        OppPreventUpdateForecastAmountsHandler_HasRun = true;
        oppSourceUpdonLeadConv_HasRunAfter = true;
        LeadAcademicProgramConversion_HasRunAfter = true;
        //2017-11-01 Viktor@4Front
        SBQQQuoteTriggerHandlerBeforeInsert = true;
        SBQQQuoteTriggerHandlerAfterInsert = true;
        SBQQQuoteTriggerHandlerBeforeUpdate = true;
        SBQQQuoteTriggerHandlerAfterUpdate = true;
        //2017-11-01 Viktor@4Front ^
        //2017-11-13 AYS BMW-402
        LeadTriggerHandlerBeforeInsert = true;
        LeadTriggerHandlerAfterInsert = true;
        LeadTriggerHandlerBeforeUpdate = true;
        LeadTriggerHandlerBeforeUpdate2 = true;
        LeadTriggerHandlerAfterUpdate = true;
        //2017-11-13 AYS BMW-402^

        PseProjectTriggerBeforeInsert = true;
        PseProjectTriggerBeforeUpdate = true;
        PseProjectTriggerAfterUpdate = true;
        PseProjectTriggerAfterInsert = true;
        PseProjectTriggerAfterDelete = true;
        PseBudgetTriggerAfterInsert = true;
        PseBudgetTriggerAfterUpdate = true;
        TaskTriggerAfterInsert = true;
        TaskTriggerAfterUpdate = true;

        CaseTriggerAfter = true;

        BasicKnowledgeArticlesTriggerBeforeInsert = true;
        BasicKnowledgeArticlesTriggerBeforeUpdate = true;
        DiagnosticKnowledgeArticlesTriggerBeforeInsert = true;
        DiagnosticKnowledgeArticlesTriggerBeforeUpdate = true;
        TrainingCardManagementTriggerAfterInsert = true;

        Z_QuoteSummaryTriggerBeforeInsert = true;
        Z_QuoteSummaryTriggerAfterInsert = true;
        Z_QuoteSummaryTriggerBeforeUpdate = true;
        Z_QuoteSummaryTriggerAfterUpdate = true;

        PseTimecardHeaderTriggerAfterInsert = true;
        PseTimecardHeaderTriggerAfterUpdate = true;
        PseTimecardHeaderTriggerAfterDelete = true;

    }
    
    public static void SetAllSemaphoresToFalse() {

        blockTriggers = false;

        Z_QuoteTriggerBeforeInsert = false;
        Z_QuoteTriggerAfterInsert = false;
        Z_QuoteTriggerBeforeUpdate = false;
        Z_QuoteTriggerAfterUpdate = false;

        // QCW-4609 Viktor@4Front
        CustomAccountTriggerBeforeInsert = false;
        CustomAccountTriggerAfterInsert = false;
        CustomAccountTriggerBeforeUpdate = false;
        CustomAccountTriggerAfterUpdate = false;
        CustomAccountTriggerBeforeDelete = false;
        CustomAccountTriggerAfterDelete = false;

        CustomCaseTriggerBeforeInsert = false;
        CustomCaseTriggerAfterInsert = false;
        CustomCaseTriggerBeforeUpdate = false;
        CustomCaseTriggerAfterUpdate = false;
        // QCW-4609 Viktor@4Front^

        UpdateMarketingRequestTriggerHasRun = false;
        OppUpdateExecutiveSponsor_BatchIsRunning = false;
        PPMarketingPortalClassTestHasRun = false;
        TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = false;
        AccountSharingRulesOnPartnerPortalAccountsHasRun = false;
        Opp_CVForNLRPTriggerRun = false;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //CaseBusinessCritical_AfterRun = false;
        
        CaseTriggerBeforeUpdateHasRun = false;
        Opportunity_Legal_CompanyCheckHasRun = false;
        lastModifiedUpdateHasRun = false;
        Opportunity_VoucherManagementCreationHasRun = false;
        Opportunity_ManageForecastProductsHasRun_Before = false;
        Opportunity_ManageForecastProductsHasRun_After = false;
        Opportunity_ManageForecastProductsIsInsert = false;
        OppUpdateContactSOI_IsAfter = false;
        Opportunity_OppPartnerContactTriggerHasRun = false;
        TaskSetTypeHasRun = false;
        DealSplitUpdateHasRun = false;
        DealSplitPercentCalcHasRun = false;
        OppUpdateAccOwnerNameHasRun = false;
        Opportunity_ExecDealsVisibility_Insert = false;
        Opportunity_ExecDealsVisibility_Update = false;
        TaskLeadFollowUp_HasRunBefore = false;
        TaskLeadFollowUp_HasRunAfter = false;
        ChangeControlRequestorApprovalCheckHasRun = false;
        OpportunityMarketType_HasRun = false;
        UpdateDealSplitChildOpp_BatchIsRunning = false;
        OpportuntyChildCloneHasRun = false;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //CaseUpdateSupportContact_HasRunAfterUpdate = false;
        //CaseUpdateSupportContact_HasRunAfterDelete = false;
        
        CaseTrigger_HasRunAfterUpdate = false;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //CaseClosedSendSurvey_HasRunAfterUpdate = false;
        
        LeadScoreTrigger_HasRunBefore = false;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //ContactScoreTrigger_HasRunBefore = false;
        
        OppPreventUpdateForecastAmountsHandler_HasRun = false;
        oppSourceUpdonLeadConv_HasRunAfter = false;
        LeadAcademicProgramConversion_HasRunAfter = false;
        //2017-11-01 Viktor@4Front
        SBQQQuoteTriggerHandlerBeforeInsert = false;
        SBQQQuoteTriggerHandlerAfterInsert = false;
        SBQQQuoteTriggerHandlerBeforeUpdate = false;
        SBQQQuoteTriggerHandlerAfterUpdate = false;
        //2017-11-01 Viktor@4Front ^
        //2017-11-13 AYS BMW-402
        LeadTriggerHandlerBeforeInsert = false;
        LeadTriggerHandlerAfterInsert = false;
        LeadTriggerHandlerBeforeUpdate = false;
        LeadTriggerHandlerBeforeUpdate2 = false;
        LeadTriggerHandlerAfterUpdate = false;
        //2017-11-13 AYS BMW-402^

        PseProjectTriggerBeforeInsert = false;
        PseProjectTriggerBeforeUpdate = false;
        PseProjectTriggerAfterUpdate = false;
        PseProjectTriggerAfterInsert = false;
        PseProjectTriggerAfterDelete = false;
        PseBudgetTriggerAfterInsert = false;
        PseBudgetTriggerAfterUpdate = false;
        TaskTriggerAfterInsert = false;
        TaskTriggerAfterUpdate = false;

        CaseTriggerAfter = false;

        BasicKnowledgeArticlesTriggerBeforeInsert = false;
        BasicKnowledgeArticlesTriggerBeforeUpdate = false;
        DiagnosticKnowledgeArticlesTriggerBeforeInsert = false;
        DiagnosticKnowledgeArticlesTriggerBeforeUpdate = false;
        TrainingCardManagementTriggerAfterInsert = false;

        Z_QuoteSummaryTriggerBeforeInsert = false;
        Z_QuoteSummaryTriggerAfterInsert = false;
        Z_QuoteSummaryTriggerBeforeUpdate = false;
        Z_QuoteSummaryTriggerAfterUpdate = false;

        PseTimecardHeaderTriggerAfterInsert = false;
        PseTimecardHeaderTriggerAfterUpdate = false;
        PseTimecardHeaderTriggerAfterDelete = false;
    }
}