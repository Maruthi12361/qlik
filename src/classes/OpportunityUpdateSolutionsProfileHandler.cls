/************************************************************************************************
*
*   OpportunityUpdateSolutionsProfileHandler
*   
*   This handler will make sure Opportunity will update Solutions profile changes accordingly
*  
*   Changelog:
*		2016-06-02	tjg		Renamed to OpportunityUpdateSolutionsProfileHandler
*							from OpportunityUpdateSolutionsProfileHandle
*       2016-05-19  Roman@4front     Migrated from Opportunity_UpdateSolutionsProfile.trigger
* 		2016-12-29  Roman@4front	 Updated. Removed SOQL from 'for' loop.
*		2017-03-26	tjg		Record type 'Sales QCCS' is not created yet in Production, remove type filter at least for now
* 			
*   Opportunity Record Types: "QlikBuy CCS Standard" and "CCS Customer Centric Selling"
*   Account Record Types: "End User" and "Partner Account" (related list needs to be added to Partner Account page layout)
*	2017-06-22 MTM  QCW-2711 remove sharing option
*************************************************************************************************/

public class OpportunityUpdateSolutionsProfileHandler {
	public static void handle(List<Opportunity> triggerNew, List<Opportunity> triggerOld) {
		System.Debug('Starting OpportunityUpdateSolutionsProfileHandler');
		Map<String, String> oppAccMap = new Map<String, String>();
		Set<Opportunity> oppList = new Set<Opportunity>();
		Set<String> accIdList = new Set<String>();
		for(Integer i = 0; i<triggerNew.size(); i++) {
			if(triggerNew[i].StageName != triggerOld[i].StageName && triggerNew[i].StageName == 'Goal Confirmed')
			{
				oppList.add(triggerNew[i]);
				accIdList.add(triggerNew[i].AccountId);
				oppAccMap.put(triggerNew[i].Id, triggerNew[i].AccountId);
			}
		}

		if(!oppList.isEmpty()) {
			Map<Id, Account> accMap = new Map<Id, Account>([Select Id, Sector__c, Industry_from_SIC_Code_Lookup__c, RecordTypeId from Account where Id In :accIdList]);
			for(Opportunity o : oppList)
			{
				Account acc = accMap.get(oppAccMap.get(o.Id));
				if( //(o.RecordTypeId == '012D0000000KEKO' || o.RecordTypeId == '01220000000DugI') &&
						(acc.RecordTypeId == '01220000000DOFu' || acc.RecordTypeId == '01220000000DOFz'))//Opportunity record type and account record type
				{
					o.Sector__c = acc.Sector__c;
					o.Industry__c = acc.Industry_from_SIC_Code_Lookup__c;
				}
			}
		}
	    System.Debug('Finishing OpportunityUpdateSolutionsProfileHandler');
	}
}