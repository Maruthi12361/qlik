/*
	2017-03-29 : Roman Dovbush : Adding QTTestUtils.GlobalSetUp();
*/

@isTest
private class ULCCrypto_Tests {

	@testSetup static void setup() {
		QTTestUtils.GlobalSetUp();
		Account a = QTTestUtils.createMockAccount('Test Account', new User(Id = UserInfo.getUserId()));
		Contact c = QTTestUtils.createMockContact(a.Id);
		ULC_Details__c d = QTTestUtils.createMockULCDetail(c.Id, 'Active');

		QTCustomSettings__c Settings = new QTCustomSettings__c();
		Settings.Name = 'Default';
		Settings.ULC_Base_URL__c = 'MyBaseURL';
		Settings.ULC_Crypto_Secret__c = 'Secret';
		Settings.ULC_Crypto_Key__c = 'KALLEKALLEKALLEKALLEKALLEKALLE12';
		insert Settings;
	}
	
	@isTest static void test_getBaseURL() {
		ULCCryptoController c = new ULCCryptoController();
		c.ULCId = [SELECT Id FROM ULC_Details__c].Id;
		system.assertEquals('MyBaseURL', c.getBaseURL());
	}
	
	@isTest static void test_LoadAdditionalLevels() {
		ULCCryptoController c = new ULCCryptoController();
		c.ULCId = [SELECT Id FROM ULC_Details__c].Id;
		Contact con = [SELECT Id FROM Contact];
		system.assertEquals('', c.LoadAdditionalLevels(con.Id));
		Assigned_ULC_Level__c alevel = QTTestUtils.createMockULCAssignedLevel(con.Id, 'TestLevel1');
		alevel = QTTestUtils.createMockULCAssignedLevel(con.Id, 'TestLevel2');
		string levels = c.LoadAdditionalLevels(con.Id);
		system.assertEquals(true, levels.contains('TestLevel1'));
		system.assertEquals(true, levels.contains('TestLevel2'));
	}
	
	@isTest static void test_HasActiveLicense() {
		ULCCryptoController c = new ULCCryptoController();
		Account a = [SELECT Id FROM Account];
		system.assertEquals(false, c.HasActiveLicense(a.Id));
		QTTestUtils.createMockAccountLicense(a.Id);
		system.assertEquals(true, c.HasActiveLicense(a.Id));
	}

	@isTest static void test_getGeneratedCrypto_1() {
		QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');
		system.assert(Settings != null);
		ULCCryptoController c = new ULCCryptoController();
		c.ULCId = 'a08c0000003WwsE'; // This ID should not be found -- negative test
		system.assertEquals('', c.getGeneratedCrypto());
		ULC_Details__c ulcd = [SELECT Id, ContactId__c FROM ULC_Details__c];
		c.ULCId = ulcd.Id;
		Account a = [SELECT Id, Navision_Status__c FROM Account];
		a.Navision_Status__c = 'Partner';
		a.Partner_Type__c = 'MasterReseller';
		update a;
		Assigned_ULC_Level__c alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel1');
		alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel2');
		QTTestUtils.createMockAccountLicense(a.Id);
		string encrypted = c.getGeneratedCrypto();
		system.assertNotEquals(null, encrypted);
		system.assert(encrypted.length() > 0);
		Blob enc = EncodingUtil.convertFromHex(EncodingUtil.urlDecode(encrypted, 'UTF-8'));
		Blob decrypt = Crypto.decryptWithManagedIV('AES256', Blob.valueOf(Settings.ULC_Crypto_Key__c), enc);
		system.assertEquals(true, decrypt.toString().contains('Base'));
		system.assertEquals(true, decrypt.toString().contains('Partner'));
		system.assertEquals(true, decrypt.toString().contains('MasterReseller'));
		system.assertEquals(true, decrypt.toString().contains('TestLevel1'));
		system.assertEquals(true, decrypt.toString().contains('TestLevel2'));
	}

	@isTest static void test_getGeneratedCrypto_2() {
		QTCustomSettings__c Settings = QTCustomSettings__c.getValues('Default');
		system.assert(Settings != null);
		ULCCryptoController c = new ULCCryptoController();
		ULC_Details__c ulcd = [SELECT Id, ContactId__c FROM ULC_Details__c];
		c.ULCId = ulcd.Id;
		Account a1 = [SELECT Id, ParentId, Navision_Status__c FROM Account];
		Account a2 = QTTestUtils.createMockAccount('Test Account 2', new User(Id = UserInfo.getUserId()));
		a2.Navision_Status__c = 'Partner';
		a1.ParentId = a2.Id;
		List<Account> accs = new List<Account>();
		accs.add(a1); accs.add(a2);
		update accs;
		Assigned_ULC_Level__c alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel1');
		alevel = QTTestUtils.createMockULCAssignedLevel(ulcd.ContactId__c, 'TestLevel2');
		QTTestUtils.createMockAccountLicense(a1.Id);
		string encrypted = c.getGeneratedCrypto();
		system.assertNotEquals(null, encrypted);
		system.assert(encrypted.length() > 0);
		Blob enc = EncodingUtil.convertFromHex(EncodingUtil.urlDecode(encrypted, 'UTF-8'));
		Blob decrypt = Crypto.decryptWithManagedIV('AES256', Blob.valueOf(Settings.ULC_Crypto_Key__c), enc);
		system.assertEquals(true, decrypt.toString().contains('Base'));
		system.assertEquals(true, decrypt.toString().contains('Partner'));
		system.assertEquals(true, decrypt.toString().contains('TestLevel1'));
		system.assertEquals(true, decrypt.toString().contains('TestLevel2'));
	}


	@isTest static void test_GetParentNavisionStatus() {
		ULCCryptoController c = new ULCCryptoController();
		
		Account a1 = [SELECT Id, ParentId FROM Account];
		system.assertEquals(null, c.GetParentNavisionStatus(a1.Id, 0));

		Account a2 = QTTestUtils.createMockAccount('Test Account 2', new User(Id = UserInfo.getUserId()));
		Account a3 = QTTestUtils.createMockAccount('Test Account 3', new User(Id = UserInfo.getUserId()));		
		a1.ParentId = a2.Id;
		a2.Navision_Status__c = 'Partner';
		a3.Navision_Status__c = 'Customer';
		List<Account> accs = new List<Account>();
		accs.add(a1); accs.add(a2); accs.add(a3);
		update accs;
		
		system.assertEquals(null, c.GetParentNavisionStatus(a1.Id, 3));
		system.assertEquals('Partner', c.GetParentNavisionStatus(a1.Id, 0));
		system.assertEquals(null, c.GetParentNavisionStatus('001c000000k8WpW', 0));  // This ID should not be found - this is a negative test

		a1.ParentId = a3.Id;
		update a1;
		system.assertEquals('Customer', c.GetParentNavisionStatus(a1.Id, 0));		
	}

}