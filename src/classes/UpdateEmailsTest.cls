/**************************************************************
* This class contains unit tests for validating the behavior of
* UpdateEmails batch job for Lead object. The validation of the
* logic for the Contact object is redundant, as it will be full
* duplication of the current validation.
*
* Log History:
* 04.02.2020   ext_bjd  ITRM-383 Initial Development
***************************************************************/

@IsTest
private class UpdateEmailsTest {

    @TestSetup
    static void setup() {
        //Leads Creation
        List<Lead> newLeads = new List<Lead>();
        for (Integer i = 0; i < 200; i++) {
            Lead newLead = new Lead(FirstName = 'test' + i, LastName = 'test' + i,
                    Country = 'Sweden', Email = 'test' + i + '@salesforcetest.com',
                    Company = 'Eastern');
            newLeads.add(newLead);
        }
        insert newLeads;

        //Update Leads
        List<Lead> leadsToConvert = new List<Lead>();
        List<Lead> modifiedLeads = new List<Lead>();
        for (Integer i = 0; i < 100; i++) {
            if (i < 50) {
                leadsToConvert.add(newLeads[i]);
            } else {
                /*salesforce fields with email address data type allows only up to 80 characters,
                check if the limit is exceeded */
                newLeads[i].Email = '1234567890qwertyuiopasdfghjklzxcvbnmZXCB@qwertyuiopasdfghjklzxcvbnqwertyuiop.com';
                newLeads[i].FirstName = null;
                modifiedLeads.add(newLeads[i]);
            }
        }

        //Leads Convertation
        List<Database.LeadConvert> leadConverts = new List<Database.LeadConvert>();
        for (Lead l : leadsToConvert) {
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(l.Id);
            lc.convertedStatus = 'Lead - Converted';
            lc.setDoNotCreateOpportunity(true);
            leadConverts.add(lc);
        }

        //DML Operations
        update modifiedLeads;
        Database.convertLead(leadConverts);
    }

    @IsTest
    static void test() {
        Test.startTest();
        UpdateEmails uce = new UpdateEmails(UpdateEmails.LEAD_UPDATE);
        Database.executeBatch(uce);
        Test.stopTest();

        //Updates for standard emails
        System.assertEquals(100, [
                SELECT COUNT()
                FROM Lead
                WHERE Email LIKE '%salesforcetest.com.invalid'
        ]);
        //Email field limit exceeded
        System.assertEquals(49, [
                SELECT COUNT()
                FROM Lead
                WHERE Email LIKE '%@test.invalid'
        ]);
        //Converted Leads
        System.assertEquals(50, [
                SELECT COUNT()
                FROM Lead
                WHERE Email LIKE '%@salesforcetest.com'
        ]);
        //FirstName or LastName isEmpty
        List<Lead> leads = [
                SELECT FirstName, LastName
                FROM Lead
                WHERE Email LIKE '%com.invalid'
                LIMIT 1
        ];
        System.assertEquals(leads[0].FirstName, leads[0].LastName);
    }
}