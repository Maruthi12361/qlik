/*
@author: Malay Desai, Slalom LLC
@date: 01/24/2019
@description: Controller for the QVM customers list page in Qlik Commerce Community
*/
public without sharing class QVM_CustomersCon_QC {
    static final String soqlSelect = 'select Id, First_Name__c, Last_Name__c, Name__c, QVM_Product__c, QVM_Product_Name__c, Type__c, Phone__c, Email__c, CreatedDate, Lead__c, Lead_Contact__c, Company_Name__c, Responsible_Partner__c, Navision_Status_Check_Field__c, QlikTech_Company__c, Shipping_Country_Code__c from QVM_Customer_Products__c ';
    private QVM_Settings__c settings {get;set;}
    public QVM_Table customerTable { get; private set; }
    public list<QVM_Customer_Products__c> customerList { get { return customerTable.rows; }}
    
    public list<customerWrapper> customers {get;set;}
    
    public Boolean convertSuccess {get;set;}
    
    public String startDate {get;set;}
    public String endDate {get;set;}
    public String filter {get;set;}
    
    public QVM_CustomersCon_QC() {
        QVM_Partner_Data__c partnerData = null;

        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) return;

        customerTable = new QVM_Table(new List<QVM_Table.Column> {
            //new QVM_Table.Column(QVM_Customer_Products__c.Lead_Contact__c.getDescribe().getLabel(), 'Lead_Contact__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.Type__c.getDescribe().getLabel(), 'Type__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.Name__c.getDescribe().getLabel(), 'Name__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.Phone__c.getDescribe().getLabel(), 'Phone__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.Company_Name__c.getDescribe().getLabel(), 'Company_Name__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.QVM_Product_Name__c.getDescribe().getLabel(), 'QVM_Product_Name__c', true),
            //new QVM_Table.Column(QVM_Customer_Products__c.Responsible_Partner__c.getDescribe().getLabel(), 'Responsible_Partner__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.Navision_Status_Check_Field__c.getDescribe().getLabel(), 'Navision_Status_Check_Field__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.QlikTech_Company__c.getDescribe().getLabel(), 'QlikTech_Company__c', true),
            new QVM_Table.Column(QVM_Customer_Products__c.CreatedDate.getDescribe().getLabel(), 'CreatedDate', true)
        },'CreatedDate');
        settings = QVM_Settings__c.getOrgDefaults();
        customerTable.customRowsToDisplay = settings.Pagination_After_Row__c.intValue();
        filter = '';
        this.populateCustomerList();
        convertSuccess = false;
    }
    
    public class customerWrapper {
        public Boolean selected {get;set;}
        public QVM_Customer_Products__c customer {get;set;}
        public customerWrapper(QVM_Customer_Products__c cust) {
            selected = false;
            customer = cust;
        }
    }
    
    public list<customerWrapper> getCustomerList() {
            list<customerWrapper> items = new list<customerWrapper>();
            for(QVM_Customer_Products__c c : customerList) {
                customerWrapper customer = new customerWrapper(c);
                items.add(customer);
            }
            return items;
    }
    
    private void populateCustomerList() {
        this.getTableData(filter);
        customers = getCustomerList();
    }
    // CR 8221 limit the browsing pane to 1000 records so that we won't get viewstate limit exceeded error
    private void getTableData(String filterString) {
        String custListQuery = soqlSelect + 'where QVM_Partner_Data__c = \'' + QVM.getUserPartnerDataId() + '\' ' + filterString + ' LIMIT 1000';
        
        customerTable.populate(custListQuery);
    }
    /*
    // CR 8221 unlimited for export to Excel
    private void getTableDataAll(String filterString) {
        String custListQuery = soqlSelect + 'where QVM_Partner_Data__c = \'' + QVM.getUserPartnerDataId() + '\' ' + filterString;
        
        customerTable.populate(custListQuery);
    }
    */
    public void addTableFilter() {
        
        try {
            if( (startDate == null || startDate == '') || (endDate == null || endDate == '') ) {
                throw new QVM.newException(Label.QVM_Customers_Date_Exception);
            }
            if(startDate > endDate) {
                throw new QVM.newException(Label.QVM_Customers_Date_Exception_2);
            }
            
            Datetime startDateDT = QVM.getDatetimeFromString(startDate, false);
            Datetime endDateDT = QVM.getDatetimeFromString(endDate, true);
            
            String startDateConverted = startDateDT.formatGmt('yyyy-MM-dd') + 'T' + startDateDT.formatGmt('HH:mm:ss') + '.' +  startDateDT.formatGmt('SSS') + 'Z';
            String endDateConverted = endDateDT.formatGmt('yyyy-MM-dd') + 'T' + endDateDT.formatGmt('HH:mm:ss') + '.' +  endDateDT.formatGmt('SSS') + 'Z';
            filter = ' and CreatedDate >= ' + startDateConverted + ' and CreatedDate <= ' + endDateConverted;
            this.populateCustomerList();
            
        } catch (Exception e) {
            Apexpages.addMessages(e);
        }
        
    }
    
    public void clearTableFilter() {
        startDate = null;
        endDate = null;
        filter = '';
        this.populateCustomerList();
    }
    
    public void sort() {
        customerTable.sort();
        this.populateCustomerList();
    }
    
    public PageReference exportCustomersExcel() {
        if(filter == null || filter == '') {
            // CR 8221 extend the limit from 1000 to 10000
            customerTable.customRowsToDisplay = 10000;
            //this.getTableDataAll(filter);
            //customers = getCustomerList();
        }
        String SoqlQuery = EncodingUtil.urlEncode(soqlSelect + 'where QVM_Partner_Data__c = \'' + QVM.getUserPartnerDataId() + '\' ' + filter, 'UTF-8');
        System.debug('SoqlQuery=' + SoqlQuery);
        PageReference p;
        p = Page.QVM_CustomersCSV_QC;
        p.getParameters().put('s',SoqlQuery);
        return p;
    }
    
    public void createLead() {
        try {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            
            list<Lead> leadsToInsert = new list<Lead>();
            for(customerWrapper c : Customers) {
                if(c.selected == true) {
                    Lead l = new Lead();
                    l.RecordTypeId = QVM.getRecordType('Lead', settings.Default_Lead_Record_Type__c);
                    l.setOptions(dmo);
                    l.Qlikmarket_Originated__c = true;
                    l.QlikMarket_Originated_Date__c = system.Now();
                    l.Status = settings.Default_Lead_Status__c;
                    l.QVM_Customer__c = c.customer.Id;
                    l.FirstName = c.customer.First_Name__c;
                    l.LastName = c.customer.Last_Name__c;
                    l.Company = c.customer.Company_Name__c;
                    l.Email = c.customer.Email__c;
                    l.Phone = c.customer.Phone__c;
                    l.LeadSource = settings.Default_Lead_Source__c;
                    leadsToInsert.add(l);
                }
            }
            if(leadsToInsert.Size() == 0) {
                throw new QVM.newException(Label.QVM_Customers_Convert_Lead_Error);
            }
            if(leadsToInsert.Size() > 0) {
                insert leadsToInsert;
            }
            map<Id,Id> customerToLeadMap = new map<Id,Id>();
            for(Lead l : leadsToInsert) {
                customerToLeadMap.put(l.QVM_Customer__c, l.Id);
            }
            list<QVM_Customer_Products__c> customersToUpdate = new list<QVM_Customer_Products__c>();
            for(customerWrapper c : Customers) {
                if(c.selected == true) {
                    c.customer.Lead__c = customerToLeadMap.get(c.customer.Id);
                    customersToUpdate.add(c.customer);
                }
            }
            if(customersToUpdate.Size() > 0) {
                update customersToUpdate;
            }
            convertSuccess = true;
        } catch(Exception e) { ApexPages.addMessages(e); }
    }
    
    public void firstPage() {
        customerTable.firstPage();
        customers = getCustomerList();
    }
    
    public void prevPage() {
        customerTable.prevPage();
        customers = getCustomerList();
    }
    
    public void nextPage() {
        customerTable.nextPage();
        customers = getCustomerList();
    }
    
    public void lastPage() {
        customerTable.lastPage();
        customers = getCustomerList();
    }
    
    public PageReference goToQVMControlPanel() {
        PageReference p;
        p = Page.QVM_ControlPanel_QC;
        return p;
    }
    
    // CCE  |  CR# 40489 Qlik Market - add notice to register to additional pages
    public PageReference checkRegistration() {
        PageReference p = null;
        QVM_Partner_Data__c partnerData = null;
                
        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) p = Page.QVM_ControlPanelMain_QC;
        return p;
    }

}