/*
----------------------------------------------------------------------------
|  Class: BugWeightSys_BatchJob
|
|  Filename: 
|
|  Author: Peter Friberg, Fluido Sweden AB
|
|  Description:
|    Class implementing the Database.Batchable and Database.Stateful classes.
|    Will invoke the BugWeightSys_Manager as many times as it needs to
|    calculate the weight for all open Bugs. 
|
| Change Log:
| 2013-10-24  PetFri  Initial Development
| 2013-10-28  PetFri  Updates each Bug with number of cases
| 2013-10-29  PetFri  Uses active Entitlements to find Account License.
| 2013-11-07  PetFri  Uses active Entitlement/SLA Process to find SLA level
----------------------------------------------------------------------------
*/

global class BugWeightSys_BatchJob implements Database.Batchable<sObject>, Database.Stateful {
    
    // String to hold the query
    String query;

    /*
    |----------------------------------------------------------
    |  Constructor: BugWeightSys_BatchJob
    |
    |  Description:
    |      Constructor for this batchable class.
    |      Stores the query set by the schedulable class.
    |
    |  Input parameters:
    |      theQuery : String - The query to find all open Bugs.
    |----------------------------------------------------------
    */

    global BugWeightSys_BatchJob(String theQuery) { 
        query = theQuery;
    }

    /*
    |----------------------------------------------------------
    |  Method: start
    |
    |  Description:
    |      This method is invoked when the bacth job is started
    |      by the schedulable class.
    |
    |  Input parameters:
    |      BC : Database.BatchableContext
    |           The context that will hold the status of the
    |           batch job throughout the whole execution of
    |           the batch job.
    |
    |  Returns:
    |      Database.QueryLocator
    |           The first location in the query set for the
    |           whole batch execution. This is the first set
    |           of records returned by the query.
    |----------------------------------------------------------
    */

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    /*
    |----------------------------------------------------------
    |  Method: execute
    |
    |  Description:
    |      This method is invoked for every part of the batch
    |      job. Normally a long batch job is split up into parts
    |      managing 200 records each.
    |      It invokes the BugWeightSysManager.xxx method
    |      that calculates the weight for the list of Bugs.
    |      Every invokation is run in a separate context
    |
    |  Input parameters:
    |      BC    : Database.BatchableContext
    |              The context that will hold the status of the
    |              batch job throughout the whole execution of
    |              the batch job.
    |
    |      scope : The list of Bugs managed by this part of
    |              the batch execution.
    |
    |  Returns:
    |      Nothing
    |----------------------------------------------------------
    */

    global void execute(Database.BatchableContext BC, List<Bugs__c> bugs) {              
        
        System.debug('@@@@ [PART] Calculating the weight for ' + bugs.size() + ' bugs...');     
        
        // Invoke the BugWeightSys_Manager class to calculate
        // the weight of the bugs
        if (bugs.size() > 0 ) {

            // Create a new instance of the class
            BugWeightSys_Manager bm = new BugWeightSys_Manager();
            
            // Calcultae the weight of the Bugs
            bm.processBugs(bugs);

            // Write Bugs weights to database
            UPDATE bugs;
        }

        System.debug('@@@@ [PART] Calculation done!');     
    }

    /*
    |----------------------------------------------------------
    |  Method: finish
    |
    |  Description:
    |      This method is invoked when the batch job ends.
    |
    |  Input parameters:
    |      BC    : Database.BatchableContext
    |              The context that will hold the status of the
    |              batch job throughout the whole execution of
    |              the batch job.
    |
    |  Returns:
    |      Nothing
    |----------------------------------------------------------
    */

    global void finish(Database.BatchableContext BC) {
        
        // Just say that the batch job is completely done
        System.debug('@@@@ Batch done!');     
    }   
}