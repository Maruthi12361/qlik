/******************************************************

        AccountNDACountryCheckTest
        Kumar Navneet
        Date - 03/24/17


    ******************************************************/

@isTest
private class AccountNDACountryCheckTest {

    public static TestMethod void TEST_AccountNDACountryCheck()
    {
        QTTestUtils.GlobalSetUp();

         QTCustomSettings__c settings = new QTCustomSettings__c();
    settings.Name = 'Default';
    settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        insert settings;

        Subsidiary__c  sub = new Subsidiary__c (
            Name = 'test',
            Legal_Entity_Name__c = 'Test'

        );
         insert sub;
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc' ,
            Subsidiary__c = sub.id

        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        Id AccRecordTypeId_OEMEndUserAccount = '012D0000000KBYx'; //OEM End User
        Account TestAccount = new Account(
            Name = 'ULC Test Ltd.',
            Navision_Status__c = 'Customer',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Commercial - Lower SMB'
        );
        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            //Billing_Country_Code__c = 'a0A200000032Zy6'
            Billing_Country_Code__c = QTComp.Id,
            Segment_New__c = 'Commercial - Lower SMB'                            
        );
        List<Account> AccToInsert = new List<Account>();
        AccToInsert.Add(TestAccount);
        AccToInsert.Add(TestPartnerAccount);
        insert AccToInsert;
         ApexPages.StandardController stc = new ApexPages.StandardController(TestPartnerAccount );

        test.startTest();
        AccountNDACountryCheck ob = new AccountNDACountryCheck(stc);
        boolean ob1 = ob.isRedCountry;
        String ob2 = ob.formedURL;
        test.stopTest();

    }



}