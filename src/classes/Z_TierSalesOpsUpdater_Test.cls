@isTest(SeeAllData=true)
private class Z_TierSalesOpsUpdater_Test {
	
	@isTest static void test_tier_owner_update() {
		Product2 prod = Z_TestFactory.makeProduct();
    zqu__ProductRatePlan__c prp = Z_TestFactory.makeProductRatePlan(String.valueOf(prod.Id), 'prp');
    zqu__ProductRatePlanCharge__c prpc = Z_TestFactory.makeProductRatePlanCharge(String.valueOf(prp.Id), 'Recurring', 'Flat Fee', 'Test');
    zqu__ProductRatePlanChargeTier__c prpct = Z_TestFactory.makeProductRatePlanChargeTier(prpc.Id, 1);
     
    //Create Account/Opportunity/Quote
    Account acc = Z_TestFactory.makePartnerAccount('country', 'cName', 'nState', 'zipcode');
		acc.Pending_Validation__c = false;
   	insert acc;
    

    Contact contact = Z_TestFactory.makeContact(acc);
    insert contact;

    Opportunity opp = Z_TestFactory.buildOpportunity(acc);
    opp.Revenue_Type__c='Direct';
  	insert opp;

    zqu__Quote__c quote = Z_TestFactory.makeQuote(opp, acc);
    quote.zqu__BillToContact__c = contact.id;
    quote.zqu__SoldToContact__c = contact.id;
    quote.Billing_Frequency__c = 'Prepaid';
  	insert quote;
    
    zqu__QuoteAmendment__c qa = Z_TestFactory.makeQuoteAmendment(quote);
   	insert qa;
    zqu__QuoteRatePlan__c qrp = Z_TestFactory.makeQRP(quote, qa, prp);
   	insert qrp;

    zqu__QuoteRatePlanCharge__c qrpc = Z_TestFactory.makeQRPC(quote, qrp, prpc, 'Test');
    qrpc.zqu__SubscriptionRatePlanChargeZuoraId__c = '1';
   	insert qrpc;

   	zqu__QuoteCharge_Tier__c chargeTier = Z_TestFactory.makeQuoteChargeTier(qrpc.Id, 1);
   	List<zqu__Quote__c> quotes = [
  																	SELECT Id, OwnerId
  																	FROM zqu__Quote__c
  																	WHERE id =: quote.id
  																];
	  quote = quotes.get(0);

		Map<Id, zqu__Quote__c> quoteMap = new Map<Id, zqu__Quote__c>();
		quoteMap.put(quote.id, quote);
   	Z_QuoteTriggerHandler.updateTierOwner(quoteMap);

  	
		
	}
	
	
	
}