/******************************************************

    Class: CaseSharing
    Case sharing should remain the same (as the web service sharing handler). 
    We only have to make sure the existing case sharing is not broken after our sharing code updates.

    Changelog:
        2016-01-05  TJG     Created file
        2016-02-28  TJG     Phase 2, share with sell through partner
            If a Support Case is created against Customer A’s license with Partner A as "Sell Through Partner" or "Support Provided by" then
            Record should be shared with “End User – Account Name” (Account_Origin__c) and with “Support Provided by” (Support_Provided_By__c) 
            and/or “Current Sell Through Partner” (Current_Sell_Through_Partner__c) from the associated Product License (Entitlement lookup)
            
            If “Environment” (EnvironmentOfCase__c) or Product License (Entitlement) is not populated
            record should be shared with “Account Name”( Account) and "End User - Account Name"(Account__Origin__c)
        2017-11-07 AIN      Changed the logic to always share with the account to include all roles.
                
******************************************************/
public class CaseSharing {

    public static Boolean UpdateCaseSharing(List<Id> cazIds)
    {
        if (cazIds.size() == 0)
        {
            return true;
        }

        Set<Id> uniqueAccIds = new Set<Id>();
        Map<Id, Id> case2Account = new Map<Id, Id>();
        Map<Id, Id> case2EndUser = new Map<Id, Id>();
        //Map<Id, Id> case2STP = new Map<Id, Id>();
        //Map<Id, Id> case2SPB = new Map<Id, Id>();
        List<Case> cases = [
            SELECT  
                Id, 
                Account_Origin__c,
                AccountId,
                EntitlementId,
                EnvironmentOfCase__c,
                Entitlement.Account_License__r.Support_Provided_By__c,
                Entitlement.Account_License__r.Selling_Partner__c,
                Entitlement.Name
            FROM 
                Case 
            WHERE Account_Origin__c != null AND Id IN : cazIds];
        
        for (Case caz : cases)
        {
            case2EndUser.put(caz.Id, caz.Account_Origin__c);
            if (!uniqueAccIds.contains(caz.Account_Origin__c))
            {
                uniqueAccIds.add(caz.Account_Origin__c);
            }

            //if (caz.AccountId != null && caz.EntitlementId == null)
            if (caz.AccountId != null)
            {
                case2Account.put(caz.Id, caz.AccountId);
                if (!uniqueAccIds.contains(caz.AccountId))
                {
                    uniqueAccIds.add(caz.AccountId);
                }                
            }
            System.debug('caz.AccountId = ' + caz.AccountId);

            /*
            Id spbId = caz.Entitlement.Account_License__r.Support_Provided_By__c;
            if (spbId != null)
            {
                case2SPB.put(caz.Id, spbId);
                if (!uniqueAccIds.contains(spbId))
                {
                    uniqueAccIds.add(spbId);
                }                
            }
            System.debug('spbId = ' + spbId);

            Id cstpId = caz.Entitlement.Account_License__r.Selling_Partner__c;
            if (cstpId != null && cstpId != spbId)
            {
                case2STP.put(caz.Id, cstpId);
                if (!uniqueAccIds.contains(cstpId))
                {
                    uniqueAccIds.add(cstpId);
                }                
            }
            System.debug('cstpId = ' + cstpId);
            */
        }
        

        Boolean noNewShares = false;
        List<CaseShare> newCaseShares = new List<CaseShare>();

        // If no UniqueAccounts found then no new shares needed
        if (uniqueAccIds.size() == 0)
        {
            System.debug('UpdateCaseSharing: No unique accounts found.');            
            noNewShares = true;
        }
        else
        {
            // Fetch all the PortalRoles
            Map<Id, List<Id>> accountToRoles = new Map<Id,List<Id>>();
            Set<Id> uniquePortalRoles = new Set<Id>();
            List<Id> portalRoleIds = new List<Id>();
            List<String> portalTypes = new List<String>();
            portalTypes.add('Partner');
            portalTypes.add('CustomerPortal');

            List<UserRole> userRoles = ApexSharingRules.GetListOfUserRolesByPortalAccountIds(uniqueAccIds, portalTypes, 'Executive');
            
            for (UserRole usrRole :userRoles)
            {
                if (!uniquePortalRoles.contains(usrRole.Id))
                {
                    uniquePortalRoles.add(usrRole.Id);
                    portalRoleIds.add(usrRole.Id);
                }
                List<Id> myRoles = accountToRoles.get(usrRole.PortalAccountId);
                if (myRoles == null)
                {
                    myRoles = new List<Id>();
                    accountToRoles.put(usrRole.PortalAccountId, myRoles);
                }
                myRoles.add(usrRole.Id);
            }

            System.debug('UpdateCaseSharing: uniqueAccIds=' + uniqueAccIds + ', uniquePortalRoles=' + uniquePortalRoles + '###### userRoles=' + userRoles);

            if (uniquePortalRoles.size() == 0)
            {            
                System.debug('UpdateCaseSharing: No unique PortalRoles found.');
                noNewShares = true;
            }

            Map<Id,Id> roleToGroup = new Map<Id,Id>();

            List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(portalRoleIds, 'RoleAndSubordinates');
            for (Group grp : groups)
            {
                roleToGroup.put(grp.RelatedId, grp.Id);
            }

            if (roleToGroup.size() == 0)
            {
                System.debug('UpdateCaseSharing: No unique Groups found.');            
                noNewShares = true;
            }

            if (!noNewShares)
            {
                for (Case caz : cases)
                {
                    if (case2EndUser.containsKey(caz.Id))
                    {
                        Id accountId = case2EndUser.get(caz.Id);
                        
                        System.debug('End User Id=' + accountId);
                        System.debug('accountToRoles=' + accountToRoles);

                        if (accountToRoles.containsKey(accountId))
                        {
                            List<Id> myRoles = accountToRoles.get(accountId);
                            for (Id accountIdRole : myRoles)
                            {
                                if (roleToGroup.containsKey(accountIdRole))
                                {
                                    newCaseShares.add(createNewCaseShare(caz.Id, 'Read', roleToGroup.get(accountIdRole)));
                                }
                            }
                        }
                    }
                    
                    if (case2Account.containsKey(caz.Id))
                    {
                        Id accountId = case2Account.get(caz.Id);
                        
                        System.debug('accountId=' + accountId);
                        System.debug('accountToRoles=' + accountToRoles);

                        if (accountToRoles.containsKey(accountId))
                        {
                            List<Id> myRoles = accountToRoles.get(accountId);
                            for (Id accountIdRole : myRoles)
                            {
                                if (roleToGroup.containsKey(accountIdRole))
                                {
                                    newCaseShares.add(createNewCaseShare(caz.Id, 'Read', roleToGroup.get(accountIdRole)));
                                }
                            }
                        }
                    }

                    /*

                    if (case2SPB.containsKey(caz.Id))
                    {
                        Id accountId = case2SPB.get(caz.Id);
                        
                        System.debug('SPB AccountId=' + accountId);
                        System.debug('accountToRoles=' + accountToRoles);

                        if (accountToRoles.containsKey(accountId))
                        {
                            List<Id> myRoles = accountToRoles.get(accountId);
                            for (Id accountIdRole : myRoles)
                            {
                                if (roleToGroup.containsKey(accountIdRole))
                                {
                                    newCaseShares.add(createNewCaseShare(caz.Id, 'Read', roleToGroup.get(accountIdRole)));
                                }
                            }
                        }
                    }                    

                    if (case2STP.containsKey(caz.Id))
                    {
                        Id accountId = case2STP.get(caz.Id);
                        
                        System.debug('STP AccountId=' + accountId);
                        System.debug('accountToRoles=' + accountToRoles);

                        if (accountToRoles.containsKey(accountId))
                        {
                            List<Id> myRoles = accountToRoles.get(accountId);
                            for (Id accountIdRole : myRoles)
                            {
                                if (roleToGroup.containsKey(accountIdRole))
                                {
                                    newCaseShares.add(createNewCaseShare(caz.Id, 'Read', roleToGroup.get(accountIdRole)));
                                }
                            }
                        }
                    } 
                    */                   
                }
            }
        }

        // retrieving existing Manual Opportunity Sharing
        List<CaseShare> caseShares = [select Id,CaseId, UserOrGroupId, CaseAccessLevel from CaseShare 
                    where CaseId in :cazIds and RowCause = 'Manual'];

        // filter out those need to be deleted
        List<CaseShare> toDel = new List<CaseShare>();
        for (CaseShare oldShare : caseShares)
        {
            Boolean matched = false;
            for (integer i=0; i<newCaseShares.size(); i++)
            {
                if (oldShare.CaseAccessLevel == newCaseShares[i].CaseAccessLevel &&
                    oldShare.CaseId == newCaseShares[i].CaseId &&
                    oldShare.UserOrGroupId == newCaseShares[i].UserOrGroupId )
                {
                    matched = true;
                    // already in place, no need to insert again
                    newCaseShares.remove(i);
                }
            }
            if (!matched)
            {
                toDel.add(oldShare);
            }
        } 
        // delete those sharing that are no longer valid
        if (toDel.size() > 0)
        {
            Delete toDel;
        }

        // Insert sharing if there is the need
        if (newCaseShares.size() > 0)
        {
            Database.Insert(newCaseShares, false);
        }       

        return true;
    }

    public static CaseShare createNewCaseShare(Id caseId, string accessLevel, string usrOrGroupId)
    {
        CaseShare cShare = new CaseShare(
                CaseId = caseId,
                CaseAccessLevel = accessLevel,
                UserOrGroupId = usrOrGroupId
            );

        return cShare;
    }
}