/**************************************************
* CR# 20060 – Add Predictive Lead Score Grade and Overall Priority
* CR# 23836 – Lead/Contact Lifecycle-Clear FUR Fields
* Change Log:
* 2015-03-12 Madhav Kakani: Test case for ContactScore_Trigger
* 2015-06-11 CCE Added test for clearing of follow-up flags (testContactScoreTrigger_ClearFU_RequiredFlags)
* 2015-06-19 NAD Added testLeadScoreTrigger_ConvertLeads() test method for better coverage.
* 2015-07-13 CCE/Madhav Kakani CR# 30746 - Create New Predictive Lead Score Field and Revise Grade changes
* 2016-04-21 CCE CR# 82611 - Replacing Lead Status value "Follow-Up Accepted" with new value "Follow-Up Attempt 1"
* 2016-10-20 CCE CR# 95899 - Removing testmethod testLeadScoreTrigger as no longer required as code it was testing has been removed from the trigger
* 2018-03-29 CCE CHG0033445 - Add test for "Set Conversica options field to Stop (case sensitive)"
* 2018-10-12 CCE CHG0034825 BMW-1054 Update test due to code changes for this CHG
* 2018-10-12 CCE CHG0034697 BMW-1015 Update test due to code changes for this CHG
* 2019-03-18 CCE CHG0035745 BMW-1364 Update test. Setting New FU Req field as new validation rule LE020 requires it
* 2019-05-17 CCE CHG0036072 Merge New and Existing Follow-Up conditions
* 2019-12-19 extbjd ITRM-388 recreated testLeadScoreTrigger_ConvertLeads. add SeeAllData=true because lead convert with
                                new object creation throws an error. it is because Validation rules
                                does not work with hierarchy custom settings during this process.
                                Removed Setup because SeeAllData=true tests can't contain setup methods.
                                testLeadScoreTrigger_AQLStartDate was activated again
                                based on what is the main code in the MasterRealeseBranch.
                                testLeadScoreTrigger_ClearFU_RequiredFlags version taken from MasterRealeseBranch.
**************************************************/
@isTest(SeeAllData = true) //do not remove. SeeAllData=false will cause errors. more details in the comments above
private class LeadScore_TriggerTest {
    private static Id academicLeadRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Academic_Program_Lead' LIMIT 1].Id;
    //static testmethod void testLeadScoreTrigger(){                   
    //    Lead l = new Lead(LastName = 'test lead', FirstName = 'first name', Country = 'Finland',
    //                        Email = 'test@test.com', Company = 'ACME');
    //    insert l;
    //    system.assert(l.Id != null);
        
    //    Test.startTest();

    //    Semaphores.TriggerHasRun(1);
    //    l.Predictive_Lead_Score__c = 96;
    //    l.Status = 'Follow-Up Rejected';
    //    update l;
    //    Lead lTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Lead WHERE Id=:l.Id];
    //    system.assert(lTmp.Predictive_Lead_Score_Grade__c == 'A');
    //    system.assert(lTmp.Predictive_Lead_Score_Numeric_Grade__c == '5');

    //    Semaphores.TriggerHasRun(1);
    //    l.Predictive_Lead_Score__c = 80;
    //    update l;
    //    lTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Lead WHERE Id=:l.Id];
    //    system.assert(lTmp.Predictive_Lead_Score_Grade__c == 'B');
    //    system.assert(lTmp.Predictive_Lead_Score_Numeric_Grade__c == '4');

    //    Semaphores.TriggerHasRun(1);
    //    l.Predictive_Lead_Score__c = 51;
    //    update l;
    //    lTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Lead WHERE Id=:l.Id];
    //    system.assert(lTmp.Predictive_Lead_Score_Grade__c == 'C');
    //    system.assert(lTmp.Predictive_Lead_Score_Numeric_Grade__c == '3');

    //    Semaphores.TriggerHasRun(1);
    //    l.Predictive_Lead_Score__c = 1;
    //    update l;
    //    lTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Lead WHERE Id=:l.Id];
    //    system.assert(lTmp.Predictive_Lead_Score_Grade__c == 'D');
    //    system.assert(lTmp.Predictive_Lead_Score_Numeric_Grade__c == '1');

    //    Test.stopTest();
    //}

    /*@testSetup
    public static void Setup() {
        QuoteTestHelper.createCustomSettings();
    }*/

    @isTest
    static void testLeadScoreTrigger_ClearFU_RequiredFlags() {
        Lead l = new Lead(LastName = 'test lead', FirstName = 'first name', Country = 'Finland',
                Email = 'test@test.com', Company = 'ACME');
        insert l;
        system.assert(l.Id != null);

        // Semaphores.TriggerHasRun(1);
        l.Predictive_Lead_Score__c = 96;
        l.Status = 'Follow-Up Rejected';
        update l;

        //Set Lead to required start state
        l.Status = 'Follow-Up Attempt 5';
        l.Event_Follow_Up_Required__c = true;
        l.Existing_Follow_Up_Required__c = true;
        l.New_Follow_Up_Required__c = true;
        l.AVA__AVAAI_action_required__c = true;
        l.Number_of_Follow_Up_Voicemails__c = 5;
        l.Number_of_Follow_Up_Emails__c = 1;
        update l;

        Lead lTmp = [SELECT Overall_Follow_up_Required__c FROM Lead WHERE Id = :l.Id];
        system.assert(lTmp.Overall_Follow_up_Required__c == true);

        Test.startTest();

        l.Number_of_Follow_Up_Voicemails__c = 6;
        //l.Status = 'Contacted-Additional Work Required';
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update l;

        lTmp = [SELECT Event_Follow_Up_Required__c, Existing_Follow_Up_Required__c, New_Follow_Up_Required__c, Overall_Follow_up_Required__c, AVA__AVAAI_options__c FROM Lead WHERE Id=:l.Id];
        system.assert(lTmp.Event_Follow_Up_Required__c == false);
        system.assert(lTmp.Existing_Follow_Up_Required__c == false);
        system.assert(lTmp.New_Follow_Up_Required__c == false);
        system.assert(lTmp.Overall_Follow_up_Required__c == false);
        system.assert(lTmp.AVA__AVAAI_options__c == 'Stop');

        Test.stopTest();
    }

    @isTest
    static void testLeadScoreTrigger_AQLStartDate() {
        LeadScoreHandler lsh = new LeadScoreHandler();

        Lead l = new Lead(LastName = 'test lead', FirstName = 'first name', Country = 'Finland',
                Email = 'test@test.com', Company = 'ACME');
        insert l;
        system.assert(l.Id != null);
        Lead lTmp = [SELECT Status, AQL_Start_Date__c FROM Lead WHERE Id = :l.Id];
        system.assert(l.AQL_Start_Date__c == null);
        Test.startTest();
        l.New_Follow_Up_Required__c = true; //Setting New FU Req field as new validation rule LE020 requires it
        l.Status = 'Follow-Up Required';
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update l;

        lTmp = [SELECT Status, AQL_Start_Date__c FROM Lead WHERE Id = :l.Id];

        system.assert(l.AQL_Start_Date__c == null);

        Test.stopTest();
    }

    @isTest
    static void testLeadScoreTrigger_ConvertLeads(){

        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
                Country_Name__c = 'USA',
                Subsidiary__c = sub.id
        );
        insert QTComp;
        String qlikId = QTComp.Id;

        Long dt = System.now().getTime();
        Lead lead = new Lead(
                FirstName = 'First name test' + dt, LastName = 'test' + dt, Company = 'Test company' + dt,
                Opp_Reg_Rejected__c = false, Email = 'test' + dt + '@testtesttest.com',
                RecordTypeId = academicLeadRT, Signed_License_Agreement_Received__c = true,
                Country_Code__c = qlikId, Estimated_Close_Date__c = Date.today().addDays(1));
        insert lead;

        Semaphores.LeadTriggerHandlerAfterUpdate = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadAcademicProgramConversion_HasRunAfter = false;

        Test.startTest();
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(lead.Id);
        lc.setDoNotCreateOpportunity(false);
        lc.setConvertedStatus('Lead - Converted');

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());

        Test.stopTest();

        lead = [
                SELECT ConvertedAccountId, ConvertedOpportunityId, ConvertedContactId
                FROM Lead
                WHERE Id = :lead.Id
        ];

        Opportunity dbOpp = [
                SELECT Academic_Contact_Name__c, RecordTypeId, Revenue_Type__c, StageName, Type
                FROM Opportunity
                WHERE Id = :lead.ConvertedOpportunityId
        ];

        System.assertEquals('Academic Program', dbOpp.Revenue_Type__c);
        System.assertEquals('AP - Open', dbOpp.StageName);
        System.assertEquals('New Customer', dbOpp.Type);

        System.assertEquals(
                lead.ConvertedContactId, dbOpp.Academic_Contact_Name__c,
                'Academic contact name should match the related contact!'
        );
        List<Contact> l_Contact = new List<Contact>([
                SELECT Id
                FROM Contact
                WHERE Id = :lead.ConvertedContactId
        ]);
        System.assertEquals(1, l_Contact.size());
    }
}