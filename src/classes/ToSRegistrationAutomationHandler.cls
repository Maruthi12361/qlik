/***
* 28-02-2018 - AYS - Created instead of ToSRegistrationAutomation.trigger
* Trigger history:
* >>Trigger to kick off the approval process for ToS Registrations that are set to 
* >>Legal Approval Status: Legal Approval Required on creation. Created per CR# 90926
* >>
* >>2016-11-29 NAD : Trigger created - Test method is in ToSRegistrationAutomationTest.class
***/

public class ToSRegistrationAutomationHandler {

	public static void handle(List<TOS_Registration__c> triggerNew){
		for(TOS_Registration__c ToSObject : triggerNew) {
			//check to see if legal approval status field is 'Legal Approval Required'
			if(ToSObject.TOS_Legal_Approval_Status__c == 'Legal Approval Required') {
				
				//create new approval request to submit
				Approval.ProcessSubmitRequest submitReq = new Approval.ProcessSubmitRequest();

				//set the required fields for submitting a request
				submitReq.setComments('Automatic Submission');
				submitReq.setObjectId(ToSObject.id);

				//submit approval request
				Approval.ProcessResult result = Approval.process(submitReq);

				//write to console to verify success/failure
				System.debug('Submitted ToS for approval: ' + result.isSuccess());
			}
		}
	}
}