/******************************************************

    Changelog:
        2017-05-26  CCE     CHG0031108 - Updates to Operations Request - Initial development of test class for MarketingDeliverableTrigger
                        
******************************************************/
@isTest
private class MarketingDeliverableTriggerTest {
    
    @isTest static void test_AddMarketingDeliverableToOperationsRequest() {
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(mockSysAdmin) {
            Marketing_Request__c mr = new Marketing_Request__c();
            mr.Requestor__c = mockSysAdmin.Id;
            mr.Project_Name__c = 'My OR test';
            mr.Project_Description__c = 'Blah blah blah';
            mr.Campaign_type__c = 'Internal project';
            mr.Expected_Campaign_Launch_Date__c = Date.today().addDays(1);
            mr.Target_Completion_Date__c = Date.today().addDays(10);
            mr.Region__c = 'Americas';
            mr.RecordTypeId = '012D0000000KJMD'; //Marketo Support Requests record type
            insert mr;

            Test.startTest();
            Marketing_Deliverable__c myMD = new Marketing_Deliverable__c();
            myMD.Marketing_Request__c = mr.Id;
            myMD.RecordTypeId = '012D0000000JrqP'; //Data - Response / Target Upload
            myMD.Subject__c = 'My test';
            myMD.Approval_Status__c = 'New';
            myMD.Requestor__c = mockSysAdmin.Id;
            myMD.Assigned_To__c = mockSysAdmin.Id;
            myMD.Requested_Due_Date__c = Date.today().addDays(1);
            myMD.Requestor_Region__c = 'Americas';
            myMD.Priority__c = 'Standard';
            myMD.List_Source__c = 'Internal Referral';
            myMD.Approval_Status__c = 'New';
            myMD.Affected_Records__c = 1;
            insert myMD;

            Marketing_Request__c mrRet = [select Send_Marketing_Deliverable_Email__c from Marketing_Request__c where Id = :mr.Id];
            //This is where we need to add an assert to test Send_Marketing_Deliverable_Email__c is being set. Unfortunately as soon as the field is 
            //set a workflow will see it, send an email and clear it before we get a chance to read it again. This means that we can 
            //only provide coverage for the trigger. At this time of initial deveopment coverage is 100%
            //System.assert(mrRet.Send_Marketing_Deliverable_Email__c == true);
            Test.stopTest();
        }
    }   
}