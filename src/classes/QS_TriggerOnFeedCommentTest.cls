/*********************************************
Change Log:
2015-11-03 IRN created a testclass to test instering feed comments
2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*********************************************/
@isTest
private class QS_TriggerOnFeedCommentTest
{

    static testMethod void createFeedCommentTest()
    {
        Test.startTest();

        Case cs = new Case();
        insert cs;
        // Create a feed item and attach it to the Case
        FeedItem feedItem = new FeedItem(ParentId=cs.Id, Body='Test Feed Item'); 
        insert feedItem;
        system.assert(feedItem.Id != null);

        FeedComment com = new FeedComment(CommentBody='test', FeedItemId = feedItem.Id);
        insert com;
        system.assert(com.Id != null);
        Test.stopTest();
    }
    
}