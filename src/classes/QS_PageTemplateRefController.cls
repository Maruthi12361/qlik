/************************************************************
* 2019-05-05 AIN IT-1597 Created for Support Portal Redesign
* 2019-09013 extbad IT-2125 Add MyQlik link
************************************************************/
public with sharing class QS_PageTemplateRefController {
	public QS_PageTemplateController QS_PageTemplateController {get; set;}
    public String myQlikLink {get; set;}
    
	public QS_PageTemplateRefController() {
		Boolean isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        myQlikLink = isSandbox ? 'https://myqlik-staging.qlik.com/portal' : 'https://myqlik.qlik.com/portal';
	}
}