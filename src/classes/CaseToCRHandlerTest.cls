/*
 File Name: CaseToCRHandlerTest
 Moved from class Test_CasetoCR_CRtoCase

 2013_07_23  CCE CR# 9083 https://eu1.salesforce.com/a0CD000000Zy1GV
 2014-02-14  SLH CR# 11131 reveresed CR# 9083 Mr Moe is back!
 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.

*/

@IsTest
private class CaseToCRHandlerTest {
    //public Id businessOwnerID = '005D0000001pH7F'; //Ulrika Sandén CCE CR# 9083

    public static TestMethod void testCRtoCase() {
        Semaphores.SetAllSemaphoresToTrue();
        RecordType caseRT = [
                SELECT Id
                FROM RecordType
                WHERE SobjectType = 'Case' AND Name = :'Sales Ops Internal Apps'
                LIMIT 1
        ];
        Id pId = [SELECT Id FROM Profile WHERE Name LIKE 'System Administrator' LIMIT 1].Id;
        User johnBrown = new User();
        johnBrown = [SELECT Id FROM User WHERE profileId = :pId AND isActive = true LIMIT 1];

        List<Case> testCases = new List<Case>();
        for (Integer i = 0; i < 2; i++) {
            Integer no = 50 + i;
            testCases.add(new Case(
                    RecordTypeId = caseRT.Id,
                    Subject = 'Test Case N' + no,
                    Status = 'Not Opened',
                    Description = 'Desc: Test Case N' + no,
                    Category__c = 'Applications',
                    Requester_Name__c = johnBrown.Id,
                    Priority = 'Medium',
                    SuppliedEmail = 'abc@abc.com'));
        }
        test.startTest();
        Semaphores.SetAllSemaphoresToFalse();
        insert testCases;

// For Support Business Area --> Business Owner Michael Moe

        User businessOwner = new User();
        businessOwner = [SELECT Id FROM User WHERE Name LIKE 'Michael Moe%' AND isActive = true LIMIT 1];   //CR# 9083

        List<SLX__Change_Control__c> testChangeControls = new List<SLX__Change_Control__c>();

        for (Integer i = 0; i < 2; i++) {
            Integer no = 50 + i;
            testChangeControls.add(new SLX__Change_Control__c(
                    Business_Owner__c = businessOwner.Id,
                    Business_Area__c = 'Support',
                    Name = 'test change control' + no,
                    SLX__Requestor__c = johnBrown.Id,
                    Change_Description__c = testCases[i].Description,
                    Business_Need__c = testCases[i].Description + ' Business Need',
                    ITIL_level__c = 'Normal - Needs Approval',
                    Type__c = 'Installation',
                    Affected_systems__c = 'Others',
                    Problem_Case_Nr__c = testCases[i].Id));    //CR# 9083

        }

        insert testChangeControls;

        List<SLX__Change_Control__c> tCControls = [
                SELECT Id, Problem_Case_Nr__c
                FROM SLX__Change_Control__c
                WHERE Name LIKE 'test change control%'
                ORDER BY Name
        ];
        List<Case> tCases = [
                SELECT Id, Linked_to_CR__c
                FROM Case
                WHERE Subject LIKE 'Test Case N%'
                ORDER BY Subject
        ];

        for (Integer i = 0; i < 2; i++) {
            System.assertEquals(tCases[i].id, tCControls[i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[i].Linked_to_CR__c);
        }

        test.stopTest();
    }


    public static TestMethod void testCRDeltoCase() {
        Semaphores.SetAllSemaphoresToTrue();
        RecordType caseRT = [
                SELECT Id
                FROM RecordType
                WHERE SobjectType = 'Case' AND Name = :'Sales Ops Internal Apps'
                LIMIT 1
        ];
        Id pId = [
                SELECT Id
                FROM Profile
                WHERE Name LIKE 'System Administrator'
                LIMIT 1
        ].Id;
        User johnBrown = new User();
        johnBrown = [
                SELECT Id
                FROM User
                WHERE profileId = :pId AND isActive = true
                LIMIT 1
        ];

        List<Case> testCases = new List<Case>();
        for (Integer i = 0; i < 2; i++) {
            Integer no = 50 + i;
            testCases.add(new Case(
                    RecordTypeId = caseRT.Id,
                    Subject = 'Test Case N' + no,
                    Status = 'Not Opened',
                    Description = 'Desc: Test Case N' + no,
                    Category__c = 'Applications',
                    Requester_Name__c = johnBrown.Id,
                    Priority = 'Medium',
                    SuppliedEmail = 'abc@abc.com'));
        }
        Semaphores.SetAllSemaphoresToFalse();
        insert testCases;

        User businessOwner = new User();
        businessOwner = [
                SELECT Id
                FROM User
                WHERE Name LIKE 'Michael Moe%' AND isActive = true
                LIMIT 1
        ];   //CR# 9083

        List<SLX__Change_Control__c> testChangeControls = new List<SLX__Change_Control__c>();

        for (Integer i = 0; i < 2; i++) {
            Integer no = 50 + i;
            testChangeControls.add(new SLX__Change_Control__c(
                    Business_Owner__c = businessOwner.Id,
                    Business_Area__c = 'Support',
                    Name = 'test change control' + no,
                    SLX__Requestor__c = johnBrown.Id,
                    Change_Description__c = testCases[i].Description,
                    Business_Need__c = testCases[i].Description + ' Business Need',
                    ITIL_level__c = 'Normal - Needs Approval',
                    Type__c = 'Installation',
                    Affected_systems__c = 'Others',
                    Problem_Case_Nr__c = testCases[i].Id));    //CR# 9083

        }
        test.startTest();
        Semaphores.SetAllSemaphoresToFalse();
        insert testChangeControls;

        List<SLX__Change_Control__c> tCControls = [
                SELECT Id, Problem_Case_Nr__c
                FROM SLX__Change_Control__c
                WHERE Name LIKE 'test change control%'
                ORDER BY Name
        ];
        List<Case> tCases = [
                SELECT Id, Linked_to_CR__c
                FROM Case
                WHERE Subject LIKE 'Test Case N%'
                ORDER BY Subject
        ];

        for (Integer i = 0; i < 2; i++) {
            System.assertEquals(tCases[i].id, tCControls[i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[i].Linked_to_CR__c);
        }

        delete tCControls;

        List<Case> tCases2 = [
                SELECT Id, Linked_to_CR__c
                FROM Case
                WHERE Subject LIKE 'Test Case N%'
                ORDER BY Subject
        ];

        for (Integer i = 0; i < 2; i++) {
            System.assertEquals(tCases2[i].Linked_to_CR__c, null);
        }

        test.stopTest();
    }


    public static TestMethod void testCaseToCR() {
        Semaphores.SetAllSemaphoresToTrue();
        RecordType caseRT = [
                SELECT id
                FROM RecordType
                WHERE SobjectType = 'Case' AND name = :'Sales Ops Internal Apps'
                LIMIT 1
        ];
        Id pId = [
                SELECT Id
                FROM Profile
                WHERE Name LIKE 'System Administrator'
                LIMIT 1
        ].Id;
        User johnBrown = new User();
        johnBrown = [
                SELECT Id
                FROM User
                WHERE profileId = :pId AND isActive = true
                LIMIT 1
        ];

        List<Case> testCases = new List<Case>();
        for (Integer i = 0; i < 2; i++) {
            Integer no = 50 + i;
            testCases.add(
                    new Case(
                            RecordTypeId = caseRT.Id,
                            Subject = 'Test Case N' + no,
                            Status = 'Not Opened',
                            Description = 'Desc: Test Case N' + no,
                            Category__c = 'Applications',
                            Requester_Name__c = johnBrown.Id,
                            Priority = 'Medium',
                            SuppliedEmail = 'abc@abc.com'));
        }

        insert testCases;

        User businessOwner = new User();
        businessOwner = [
                SELECT Id
                FROM User
                WHERE Name LIKE 'Michael Moe%' AND isActive = true
                LIMIT 1
        ];   //CR# 9083

        List<SLX__Change_Control__c> testChangeControls = new List<SLX__Change_Control__c>();

        for (Integer i = 0; i < 2; i++) {
            Integer no = 50 + i;
            testChangeControls.add(new SLX__Change_Control__c(
                    Business_Owner__c = businessOwner.Id,
                    Business_Area__c = 'Support',
                    Name = 'test change control' + no,
                    SLX__Requestor__c = johnBrown.Id,
                    Change_Description__c = testCases[i].Description,
                    Business_Need__c = testCases[i].Description + ' Business Need',
                    ITIL_level__c = 'Normal - Needs Approval',
                    Type__c = 'Installation',
                    Affected_systems__c = 'Others'));  //CR# 9083
        }

        test.startTest();
        insert testChangeControls;

        List<SLX__Change_Control__c> tCControls = [
                SELECT Id, Problem_Case_Nr__c
                FROM SLX__Change_Control__c
                WHERE Name
                        LIKE 'test change control%'
                ORDER BY Name
        ];
        List<Case> tCases = [
                SELECT Id, Linked_to_CR__c
                FROM Case
                WHERE Subject LIKE 'Test Case N%'
                ORDER BY Subject
        ];

        for (Integer i = 0; i < 2; i++) {
            tCases[i].Linked_to_CR__c = tCControls[i].Id;
        }
        Semaphores.SetAllSemaphoresToFalse();
        update tCases;

        tCControls = [
                SELECT Id, Problem_Case_Nr__c
                FROM SLX__Change_Control__c
                WHERE Name LIKE 'test change control%'
                ORDER BY Name
        ];
        tCases = [
                SELECT Id, Linked_to_CR__c
                FROM Case
                WHERE Subject LIKE 'Test Case N%'
                ORDER BY Subject
        ];
        System.debug(LoggingLevel.DEBUG, '[CaseToCRHandler] tCControls:' + tCControls);
        System.debug(LoggingLevel.DEBUG, '[CaseToCRHandler] tCases:' + tCases);
        for (Integer i = 0; i < 2; i++) {
            System.assertEquals(tCases[i].id, tCControls[i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[i].Linked_to_CR__c);
        }

// Changing CR value (Linked_to_CR__c)  of Case

        for (Integer i = 0; i < 2; i++) {
            tCases[i].Linked_to_CR__c = tCControls[1 - i].Id;
        }
        Semaphores.SetAllSemaphoresToFalse();
        update tCases;

        tCControls = [
                SELECT Id, Problem_Case_Nr__c
                FROM SLX__Change_Control__c
                WHERE Name LIKE 'test change control%'
                ORDER BY Name
        ];
        tCases = [
                SELECT Id, Linked_to_CR__c
                FROM Case
                WHERE Subject LIKE 'Test Case N%'
                ORDER BY Subject
        ];

        for (Integer i = 0; i < 2; i++) {
            System.assertEquals(tCases[i].id, tCControls[1 - i].Problem_Case_Nr__c);
            System.assertEquals(tCControls[i].Id, tCases[1 - i].Linked_to_CR__c);
        }

        test.stopTest();
    }

}