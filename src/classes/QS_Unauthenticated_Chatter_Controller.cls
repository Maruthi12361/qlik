/**
 * @author  Tquila Support
 * @date    14/11/2014
 * @usedby  'QS_Unauthenticated_Chatter' Visualforce Component and Home Page for Salesforce Community
 * 
 *
 * This class acts as the controller for the QS_Unauthenticated_Chatter Visualforce component.
 * It populates the static announcement section on the home page and holds the components to be used while showing the read only chatter feeds to the unauthenticated users
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 */
  
public without sharing class QS_Unauthenticated_Chatter_Controller {
    
    // List to hold the group feeds from a particular group
    private List<CollaborationGroupFeed> collaborationGroupFeed;
    // Boolean variable to check if the user is authenticated
    public Boolean unauthenticated {get; set;}
    // list of group feeds and the components to be displayed on home page for unauthenticated users
    public List<GroupFeed> groupFeedList {get; set;}
    // announcement read from the custom setting and displayed on the home page
    public QS_HomePage_Chatter_Announcement__c announcement {get; set;}
    
    // constructor to intialise variables
    public QS_Unauthenticated_Chatter_Controller() {
        
        User currentUser = [Select Id, Name, ProfileId, ContactId, AccountId From User where Id = :userinfo.getUserId() LIMIT 1];
        if(currentUser != null) {
            if(currentUser.ContactId != null && currentUser.AccountId != null)
                unauthenticated = false;
            else
                unauthenticated = true;
        } else {
            unauthenticated = true;
        }
        
        announcement = QS_HomePage_Chatter_Announcement__c.getInstance(System.Label.QS_Announcement_Setting);
        if(announcement!=null)
        {
          if(announcement.body__c != null && announcement.body__c != '') {
             announcement.body__c = announcement.body__c.replaceAll('<','(').replaceAll('>',')').replaceAll('\n','<br/>');
          }
        }
        // executed for unauthenticated users and populates the groupFeedList list to be displayed on home page
        initUnauthenticated();
        
    }
    
    private void initUnauthenticated() {
       // if(unauthenticated) { KMH - Should be avaliable to Authenticated users as well
            string blogName = test.isRunningTest() ? 'testUnauthenticatedChatter CG' : System.Label.QS_Chatter_Group_Blog;

            collaborationGroupFeed = [Select Visibility, Type, Title, RelatedRecordId, ParentId, NetworkScope, LinkUrl, LikeCount, LastModifiedDate, Body,
                                                InsertedById, InsertedBy.Name, Id, CreatedDate, CreatedById, ContentType, ContentSize, ContentFileName, ContentDescription, ContentData, CommentCount,
                                                Parent.NetworkId, Parent.CanHaveGuests, Parent.InformationBody, Parent.InformationTitle, Parent.Description, Parent.CollaborationType, Parent.Name, Parent.Id 
                                                From CollaborationGroupFeed 
                                                Where Parent.Name = :blogName AND Visibility = 'AllUsers'
                                                Order By CreatedDate DESC
                                                LIMIT 20];
            
            if(collaborationGroupFeed != null && !collaborationGroupFeed.isEmpty()) {
                
                Set<Id> userIdSet = new Set<Id>();
                for(CollaborationGroupFeed groupFeedObj : collaborationGroupFeed) {
                    userIdSet.add(groupFeedObj.InsertedById);
                }                               
                Map<Id, User> createdUserMap;
                if(userIdSet != null && !userIdSet.isEmpty())
                    createdUserMap =  new Map<Id, User> ([Select Id, SmallPhotoUrl, FullPhotoUrl, Name From User where Id = :userIdSet]);
                
                groupFeedList = new List<GroupFeed>();
                for(CollaborationGroupFeed groupFeedObj : collaborationGroupFeed) {
                    
                    String feedBody = '';
                    String feedTitle = '';
                    
                    if(groupFeedObj.body != null && groupFeedObj.Body != '') {
                        // Separating the title of the chatter post with the body of the chatter post
                        if(groupFeedObj.body.startsWith('\"')) {
                            String [] feedBodyArr = groupFeedObj.body.split('\"', 3);
                            feedTitle = feedBodyArr[0] + feedBodyArr[1];
                            feedBody = feedBodyArr[2];
                            
                        } else {
                            feedBody = groupFeedObj.body;
                        }
                        // Adding the html <br/> tags for any line breaks in the body  
                        if(feedBody != null)
                            feedBody = feedBody.replaceAll('<','(').replaceAll('>',')').replaceAll('\n','<br/>');
                        // adding the GroupFeed object to the list
                        groupFeedList.add(new GroupFeed(feedBody, feedTitle, groupFeedObj.CreatedDate, createdUserMap.get(groupFeedObj.InsertedById)));
                    }
                }
            }
       // }KMH
    }
    
    // Wrapper class to display the components related to the chatter feeds for unauthenticated users
    public class GroupFeed {
        
        public User createdUser {get; set;}
        public String feedBody {get; set;}
        public String feedTitle {get; set;}
        public DateTime publishedDate {get; set;}
        
        // Constructor to initialise the variables and set the values
        public GroupFeed(String feedBody, String feedTitle, DateTime publishedDate, User createdUser) {
            
            this.feedBody = feedBody;
            this.feedTitle = feedTitle;
            this.publishedDate = publishedDate;
            this.createdUser = createdUser;
        }
        
        
    }
    
}