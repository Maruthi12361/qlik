/******************************************************

  Class: GoogleTranslateUtils
  
  Changelog:
    2012-05-24  MHG    Created file
    2013-11-29  BTN    Added Italian ('it' => 'Italian')
                       Code reviewed by CCE
    2016-04-18  BTN    Added Try and catch for error handling
                                         
******************************************************/


public with sharing class GoogleTranslateUtils {
    
    /** Example of Google Translate API JSON result for detecting the language of a text
        '{' +
        '    "data": {' +
        '        "detections": [' +
        '            [' +
        '                {' +
        '                    "language": "en",' +
        '                    "isReliable": false,' +
        '                    "confidence": 0.114892714' +
        '                }' +
        '            ]' +
        '        ]' +
        '    },' +
        '    "data": {' +
        '        "detections": [' +
        '            [' +
        '                {' +
        '                    "language": "zh-TW",' +
        '                    "isReliable": false,' +
        '                    "confidence": 0.7392313' +
        '                }' +
        '            ]' +
        '        ]' +
        '    }' +
        '}';
    */
        
    @future (callout = true)
    public static void detectLanguages(Map<String,String> idsDescriptions) {
        List<GoogleTranslateUtils.GoogleTranslateResult> results ;         
        try {
            results = GoogleTranslateUtils.detectLanguages(idsDescriptions.values());
        } catch (System.CalloutException e){
            System.debug('ERROR:' + e);
        }
           
       List<String> languages = new List<String>();
       if (results != null && results.size() > 0) {
            for (GoogleTranslateUtils.GoogleTranslateResult res : results) languages.add(res.language);
        
            System.debug('========>'+results);      
            System.debug('========>'+languages);
            System.debug('========>'+idsDescriptions.values());
        
            if (idsDescriptions.keySet().size() != results.size()) {
                System.debug(LoggingLevel.WARN, 'GoogleTranslateUtils returned a different number of results than cases:'+
                                             results.size() +' vs '+idsDescriptions.keySet().size());
                return;
            }
            try {
                updateCasesWithLanguages(idsDescriptions.keySet(), results);
            } catch(IllegalArgumentException e) {
                System.debug('ERROR:' + e);
            }
        }
    }
    
    private static final Map<String, String> codesLanguages = new Map<String, String> {
        'en' => 'English',
        'da' => 'Danish',
        'ja' => 'Japanese',
        'sv' => 'Swedish',
        'no' => 'Norwegian',
        'fi' => 'Finnish',
        'es' => 'Spanish',
        'fr' => 'French',
        'de' => 'German',
        'it' => 'Italian'
    };
    
    /**
        Updates the cases whose ids are passed as the first argument with the languages passed as the second argument
    */
    public static void updateCasesWithLanguages(Set<String> casesIds, List<GoogleTranslateUtils.GoogleTranslateResult> results) {
        Map<Id, Case> cases = new Map<Id, Case>([SELECT Id FROM Case WHERE Id IN :casesIds]);
        if (casesIds.size() != cases.keySet().size()) {
            throw new IllegalArgumentException('Ids set and languages dont match.');
        }               
        
        Integer i = 0;
        for (String caseId : casesIds) {
            String language = codesLanguages.get(results[i].language.toLowerCase().subString(0, 2));
            if (language == null) language = 'Not detected';
            cases.get(caseId).Language__c = language;
            cases.get(caseId).Confidence_Value__c = results[i].confidence;
            i++;
        }
        update cases.values();
    }
    
    public class IllegalArgumentException extends Exception {}
    
    private static List<GoogleTranslateResult> detectLanguages(List<String> strings) {      
        return detectLanguagesParams(toParamStrings(strings));
    }
    
    public final static Integer paramsCharacterLimit = 1900;
    public final static Integer maxStringLength = 100;
    
    /**
        Create list of encoded parameter strings to use for a Google Translation API request.
        The strings passed as the argument are trimmed to maxStringLength, then they are concatenated 
        into several strings not longer than paramsCharacterLimit.
    */
    public static List<String> toParamStrings(List<String> strings) {   
        
        List<String> paramsAux = new List<String>();
        for (String s : strings) {
            String encodedString = '&q=' + EncodingUtil.urlEncode(s, 'UTF-8');                      
            Integer lastIndex = Math.min(encodedString.length(), maxStringLength);
            String trimmedString = encodedString.subString(0, lastIndex);
            
            paramsAux.add(trimmedString);
        }
        
        //limit the url to 1900 characters, in the worst case it will be 1900 characters + 99 characters start of the URL (https://www.googleapis.....)
        //the url limit might be 2000 characters
        String paramsString = '';
        List<String> params = new List<String>();
        for (String param : paramsAux) {
            if (paramsString.length() + param.length() > paramsCharacterLimit) {
                params.add(paramsString);
                paramsString = param;
            } else {
                paramsString += param;
            }
        }
        if (paramsString != '') params.add(paramsString);
        
        return params;
    }
    
    private static List<GoogleTranslateResult> detectLanguagesParams(List<String> params) {
        List<GoogleTranslateResult> results = new List<GoogleTranslateResult>();
        for (String paramsString : params) results.addAll(detectLanguagesRequest(paramsString));        
        return results;
    }
    
    /**
        Makes a request to the google API with the params string passed as argument and returns
        a list of results for each parameter that the params string contains.
    */
    private static List<GoogleTranslateResult> detectLanguagesRequest(String params) {
        List<GoogleTranslateResult> results = new List<GoogleTranslateResult>();
        System.debug('===========>'+params);
        if ( !Test.isRunningTest() )
        {
        HttpResponse r = getRequest('https://www.googleapis.com/language/translate/v2/detect?key=AIzaSyBBqxseObWSAKUWH4ux-1RPRMkWt-94bb0' + params);        
        if (r.getStatusCode() == 200) {
            System.debug('===========>'+r.getBody());
            List<List<GoogleTranslateResult>> parsingResults = parseResults(r.getBody());
            //The google API seems to create only one array "detections" (not two like in the example).
            //All the arrays in the detections array contain only one element. I havent been able to create a query
            //that returns several results for one text piece (e.g. writing in different languages in the text.)
            //That's why I need to flatten the list, no need to select the best result, as there is only one result.            
            /*for (List<GoogleTranslateResult> l : parsingResults) {
                if (l.size() > 0) results.add(bestResult(l));
            }*/
            results = flatten(parsingResults);
        }
        }
        return results;
    }
    
    private static List<GoogleTranslateResult> flatten(List<List<GoogleTranslateResult>> results) {
        List<GoogleTranslateResult> res = new List<GoogleTranslateResult>();
        for (List<GoogleTranslateResult> l : results) res.addAll(l); 
        return res;
    }
    
    //This method is maybe useless, as I havent been able to create a query that returns 
    //several results for one text piece (e.g. writing in different languages in the text.)
    //The Google API returns only one result per text.
    public static GoogleTranslateResult bestResult(List<GoogleTranslateResult> results) {
        if (results.size() <= 0) return null;
        if (results.size() == 0) return results[0];
        
        GoogleTranslateResult result = results[0];
        for(GoogleTranslateResult aux : results) if (aux.confidence > result.confidence) result = aux;
        return result;
    }   
        
    private static HttpResponse getRequest(String url) {
        HttpRequest req = new HttpRequest(); req.setEndpoint(url); req.setMethod('GET');        
        // Send the request, and return a response
        return new Http().send(req);
    }
    
    /**
        Parses the JSON result of the google API        
    */
    public static List<List<GoogleTranslateResult>> parseResults(String jsonString) {
        return parseResults(JSON.createParser(jsonString));
    }
    
    /**
        Iterates through the JSON parser until it finds the detections tag and gives the control over to the
        parseDetections method.
    */
    private static List<List<GoogleTranslateResult>> parseResults(JSONParser parser) {
        List<List<GoogleTranslateResult>> results = new List<List<GoogleTranslateResult>>();
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'detections') {
                   results.add(parseDetections(parser));                
            }
        }
            
        return results;
    }
    
    private static List<GoogleTranslateResult> parseDetections(JSONParser parser) {
        List<GoogleTranslateResult> results = new List<GoogleTranslateResult>();
        if (parser.getCurrentToken() == null || parser.getText() != 'detections') return results;
        
        parser.nextToken();
        Integer depth = 0;
        //iterate through the array inside detections to parse all objects
        do {
            JSONToken curr = parser.getCurrentToken();
            depth = updateDepth(curr, depth);
            if (curr == JSONToken.START_OBJECT) results.add(createObject(parser));
        } while (depth > 0 && parser.nextToken() != null);
        
        return results;
    }
    
    private static GoogleTranslateResult createObject(JSONParser parser) {
        String language = null;
        Boolean reliable = null;
        Double confidence = null;       
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                parser.nextToken();
                
                if (text == 'language') {                   
                    language = parser.getText();
                } else if (text == 'isReliable') {
                    reliable = parser.getBooleanValue();
                } else if (text == 'confidence') {
                    confidence = parser.getDoubleValue();
                } else {
                    System.debug(LoggingLevel.WARN, 'JsonClassifier consuming unrecognized property: '+text);
                    consumeObject(parser);
                }
            }
        }
        return new GoogleTranslateResult(language, reliable, confidence); 
    }
    
    private static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            depth = updateDepth(curr, depth);
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    private static Integer updateDepth(JSONToken curr, Integer depth) {
        if (curr == JSONToken.START_OBJECT || curr == JSONToken.START_ARRAY) return depth + 1;              
        else if (curr == JSONToken.END_OBJECT || curr == JSONToken.END_ARRAY) return depth - 1;
        else return depth;
    }
        
    public class GoogleTranslateResult {
        
        public String language {get; private set;}
        public Boolean reliable {get; private set;}
        public Double confidence {get; private set;}
        
        public GoogleTranslateResult(String language, Boolean reliable, Double confidence) {
            this.language = language;
            this.reliable = reliable;
            this.confidence = confidence;
        }
    }
}