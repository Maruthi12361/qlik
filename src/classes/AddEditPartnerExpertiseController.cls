/********************************************************
* AddEditPartnerExpertiseController
* Description:
* Create new record or edit existing record through the visualforce page for Partner Expertise Area object
*
* Change Log:
* 2012-10-04   SAN      Development by SAN CR#5825
* 2013-01-30   RDZ      CR# 6729, https://eu1.salesforce.com/a0CD000000U7AFt
*                                       Modify AddEditPartnerExpertise Page and controller to follow FLS
* 2013-02-15   RDZ      CR# 6729 and 6833, making new relationship 1 to many between Partner Expertise Area - Solution Profiles
*                       and Partner Expertise Area - QVM Product Data
* 2013-04-05   RDZ      Adding PEA Related list to AddEditPartnerExpertise.page, changes in controller to achieve this.
*                       We need partner account as a public variable so we can access to it from the page, also the
*                       SOQL query needs access to Partner_Expertise_Area__r fields (Inner Select)
* 2013-04-11   RDZ      Adding confirmation message when and expertise area is created.
* 2013-07-10   RDZ		Fixing bug for null pointer exception on save method.
* 2015-07-30   CCE      CR# 48632 Update Qlik Partner Network center URLs
**********************************************************/
public with sharing class AddEditPartnerExpertiseController {

    public SelectOption[] selectedQlikMarkets { get; set; }
    public SelectOption[] unselectedQlikMarkets { get; set; }
    public SelectOption[] selectedSolutions { get; set; }
    public SelectOption[] unselectedSolutions { get; set; }
    private Boolean bDisplayConfirmation;

    public Boolean DisplayConfirmation {
        get
        {
            return isView && bDisplayConfirmation? true:false;
        }
        set
        {
            bDisplayConfirmation = isView && value;
        }
    }


    public Partner_Expertise_Area__c PartnerExpertiseArea{get; private set;}

    private Map<Id, Solution_Profiles__c> MapSolutionProfiles = new Map<Id, Solution_Profiles__c>();
    private Map<Id, QVM_Product_Data__c> MapQVMProducts = new Map<Id, QVM_Product_Data__c>();

    //To display form in edit mode
    public boolean isEdit { get; set;}
    //To display form for creation
    public boolean isCreate { get; set;}
    //To display View List
    public boolean isView {get;set;}
    //To display form for Partners
    public boolean isPartner {get; set;}
    //To display form for Operation Administrators
    public boolean isOpAdmin {get; set;}
    //To display form for System Administrators
    public boolean isAdmin {get; set;}

    private boolean bIsTest;
    //To display test block for developers
    public boolean isTest {
        get
        {
            if (bIsTest == null)
            {
                //See if we are in live env
                String orgId = UserInfo.getOrganizationId();

                System.debug('Trying to assign isTest');
                //Live Env Org Id = 00D20000000IGPX
                bIsTest = orgId.startsWith('00D20000000IGPX')? false: true;

            }

            return bIsTest;
        }
        set
        {
            isTest=value;
        }
    }

    private string smode;
    public string Mode{
            get
            {
                return smode;
            }
            set
            {
                set<string> modeoptions = new set<string>();
                modeoptions.add('edit');
                modeoptions.add('create');
                modeoptions.add('view');
                //If the option is blank or is not in the mode options list then set smode as blank.
                smode = (String.IsBlank(value) || (!modeoptions.contains(value)))?'':value;
                //If user has access, to view, create or edit set variables, if not set all to false, a message will be display to user
                isEdit =        modeoptions.contains(smode.ToLowerCase())
                                        && smode.ToLowerCase().contains('edit')
                                        && Schema.sObjectType.Partner_Expertise_Area__c.isUpdateable()?true:false;
                isCreate=       modeoptions.contains(smode.ToLowerCase())
                                        && smode.ToLowerCase().contains('create')
                                        && Schema.sObjectType.Partner_Expertise_Area__c.isCreateable()?true:false;
                isView=         modeoptions.contains(smode.ToLowerCase())
                                        && smode.ToLowerCase().contains('view')
                                        && Schema.sObjectType.Partner_Expertise_Area__c.isAccessible()?true:false;

            }
    }


    private final Account account;
    public Account PartnerAccount{
            get
        {
            return account;
        }
    }


    private string accId;
    public String AccountId
    {
        get
        {
            return accId;
        }
        set{

            if(value != null && value != '')
            {
                accId = value;
            }
            else
            {
                accId = PartnerExpertiseArea.Partner_Account_Name__c;
            }
        }
    }


    private string saccountName;
    public String AccountName{
        get{
            return saccountName;
        }
        set{
            saccountName=value;
        }
    }

    public AddEditPartnerExpertiseController(ApexPages.StandardController controller)
    {
        //Patner Expertise Area record needs to be set before account id as when set account id may need to use PartnerExpertiseArea Account
        PartnerExpertiseArea = (Partner_Expertise_Area__c) controller.getRecord();

        AccountId = ApexPages.currentPage().getParameters().get('varAccountID');

        Mode = ApexPages.currentPage().getParameters().get('varMode');

        String sdconf = ApexPages.currentPage().getParameters().get('displayConfirmation');
        DisplayConfirmation = String.IsNotBlank(sdconf) && sdconf.contains('true')?true:false;

        //get profile id of the person adding/editing/viewing the record
        string idProfile = userinfo.getProfileId();

        System.debug('------Initial Settings----------------');
        System.debug('varAccountID ' + AccountId );
        System.debug('Mode ' + Mode);
        System.debug('PartnerExpertiseArea ' + PartnerExpertiseArea );
        System.debug('----------------------');

        //If smod contains create, means that PartnerExpertiseArea should be a new one.
        if(AccountId == null && AccountId == '')
        {
                if (isCreate)
                {
                        ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.ERROR,'AccountId is needed to create a new Expertise Area record'));
                        return;
                }
                AccountId = PartnerExpertiseArea.Partner_Account_Name__c;
        }
        else
        {
                System.debug('----------------------');
                System.debug('PartnerExpertiseArea' + PartnerExpertiseArea.Id);
                System.debug('----------------------');
                if (isCreate && PartnerExpertiseArea.Id != null)
                {
                        ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.ERROR,'Attempt to create Partner Expertise Area from already existing record'));
                        DisplayIssueBlockOnPage();
                        return;
                }
                if (isCreate && PartnerExpertiseArea.Id == null)
                {
                        PartnerExpertiseArea.Partner_Account_Name__c = AccountId;
                }
        }

        System.debug('--------------------------------');
        System.debug('AccId = ' + AccountId);
        System.debug('--------------------------------');
        account = [SELECT Id, Name, (select CreatedById, CreatedDate, CurrencyIsoCode, Dedicated_Expertise_Contact__c, IsDeleted, PartnerExpertiseAreaCreationEmailSent__c, Expertise_Area__c, Expertise_Education_Requirement__c, Expertise_Marketing_Requirement__c, Expertise_Renewal_Date2__c, Expertise_Start_Date__c, LastModifiedById, LastModifiedDate, Partner_Account_Name__c, Name, Partner_Expertise_Fulfilled__c, Id, Status__c, SystemModstamp from Partner_Expertise_Area__r) FROM Account WHERE Id = :AccountId];


        AccountName = account.Name;
        AccountId = account.Id;

        InitialiseQlikMarket();
        InitialiseSolutionProfiles();
        InitProfileSettings(idProfile);

    }

    private void DisplayIssueBlockOnPage()
    {
            //Display Warning/Error block if is Create, edit and view are false
            isCreate = false;
            isEdit = false;
            isView = false;
    }

    private void InitProfileSettings(string ProfileId)
    {
            HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
            List<Profile> ProfileList = [Select Name FROM Profile where Id=:ProfileId];
            String ProfileName = ProfileList.size()>0?''+ProfileList.get(0):'';
            isPartner = String.isNotBlank(ProfileName) && ProfileName.Contains('PRM')?true:false;
            //System Administrator = 00e20000000yyUzAAI
            isAdmin = settings.Sys_Admin_ProfileId__c.contains(ProfileId)?true:false;
            //Operations Administrator = 00e20000001ODnH
            isOpAdmin = ProfileId.contains('00e20000001ODnH')?true:false;
    }
    private void InitialiseQlikMarket()
    {
        AccountId = ApexPages.currentPage().getParameters().get('varAccountID');

        selectedQlikMarkets = new List<SelectOption>();
        unselectedQlikMarkets = new List<SelectOption>();
        List<QVM_Product_Data__c> unCategorizedProducts = [SELECT QVMIdAndName__c, Id, Name, Partner_Expertise_Area_ID__c FROM QVM_Product_Data__c WHERE QVM_Partner__r.Partner_Account__c =:AccountId and Partner_Expertise_Area_ID__c=null];
        List<QVM_Product_Data__c> selectedProducts = new List<QVM_Product_Data__c>();
        if (PartnerExpertiseArea != null && PartnerExpertiseArea.Id !=null)
        {
            selectedProducts = [SELECT QVMIdAndName__c, Id, Name, Partner_Expertise_Area_ID__c FROM QVM_Product_Data__c WHERE QVM_Partner__r.Partner_Account__c =:AccountId and Partner_Expertise_Area_ID__c=:PartnerExpertiseArea.Id];
        }

        for ( QVM_Product_Data__c c : unCategorizedProducts ) {
            unselectedQlikMarkets.add(new SelectOption(c.Id, c.QVMIdAndName__c));
            MapQVMProducts.put(c.Id, c);
        }

        for ( QVM_Product_Data__c c : selectedProducts ) {
            selectedQlikMarkets.add(new SelectOption(c.Id, c.QVMIdAndName__c));
            MapQVMProducts.put(c.Id, c);
        }
    }

    private void InitialiseSolutionProfiles()
    {

        AccountId = ApexPages.currentPage().getParameters().get('varAccountID');

        selectedSolutions = new List<SelectOption>();
        unselectedSolutions = new List<SelectOption>();
        List<Solution_Profiles__c> localUnselectedSolutions = [SELECT Name, Id, Account_Name__c, Partner_Expertise_Area_ID__c FROM Solution_Profiles__c WHERE Account_Name__c =:AccountId and Partner_Expertise_Area_ID__c=null ];
        List<Solution_Profiles__c> localSelectedSolutions = new List<Solution_Profiles__c> ();
        if (PartnerExpertiseArea != null && PartnerExpertiseArea.Id !=null)
        {
            localSelectedSolutions = [SELECT Name, Id, Account_Name__c, Partner_Expertise_Area_ID__c FROM Solution_Profiles__c WHERE Account_Name__c =:AccountId and Partner_Expertise_Area_ID__c=:PartnerExpertiseArea.Id];
        }

        for ( Solution_Profiles__c c : localUnselectedSolutions ) {
            System.debug(String.format('unselect: {0} {1}' ,new String[] {''+c.Id , c.Name} ));
            unselectedSolutions.add(new SelectOption(c.Id, c.Name));
            MapSolutionProfiles.put(c.Id, c);
        }

        for ( Solution_Profiles__c c : localSelectedSolutions ) {
            System.debug(String.format('select: {0} {1}' ,new String[] {''+c.Id , c.Name} ));
            selectedSolutions.add(new SelectOption(c.Id, c.Name));
            MapSolutionProfiles.put(c.Id, c);
        }
    }

    private void getQVMPToUpdate(SelectOption[] options, Id idvalue, List<QVM_Product_Data__c> ToUpdate)
    {
        QVM_Product_Data__c qvmp;
        for(Integer j=0; j<options.Size(); j++)
        {
            if (MapQVMProducts.containsKey(options[j].getValue()))
            {
                qvmp = MapQVMProducts.get(options[j].getValue());
                //If previous Id does not belong to current PEA, update the PEA Id on QVM Product Data
                if (qvmp.Partner_Expertise_Area_ID__c != idvalue)
                {
                    qvmp.Partner_Expertise_Area_ID__c = idvalue;
                    ToUpdate.add(qvmp);
                }
            }
        }
    }

    private void getSolutionProfilesToUpdate(SelectOption[] options, Id idvalue, List<Solution_Profiles__c> ToUpdate)
    {
        Solution_Profiles__c sp;
        for(Integer j=0; j<options.Size(); j++)
        {
            if (MapSolutionProfiles.containsKey(options[j].getValue()))
            {
                sp = MapSolutionProfiles.get(options[j].getValue());
                //If previous Id does not belong to current PEA, update the PEA Id on QVM Product Data
                if (sp.Partner_Expertise_Area_ID__c != idvalue)
                {
                    sp.Partner_Expertise_Area_ID__c = idvalue;
                    ToUpdate.add(sp);
                }
            }
        }
    }

    //builds a picklist of user names based on their profile
    public List<selectOption> getExpertiseContacts() {

            AccountId = ApexPages.currentPage().getParameters().get('varAccountID');

            List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
            options.add(new selectOption('', '- None -')); //add the first option of '- None -' in case the user doesn't want to select a value or in case no values are returned from query below
            for (Contact users : [SELECT Id, Name FROM Contact WHERE Account.Id =:AccountId]) { //query for User records with System Admin profile
                    options.add(new selectOption(users.Id, users.Name)); //for all records found - add them to the picklist options
            }
            return options; //return the picklist options
    }


    public PageReference save() {

        List<QVM_Product_Data__c> qvmpToUpdate = new List<QVM_Product_Data__c>();
        List<Solution_Profiles__c> spToUpdate  = new List<Solution_Profiles__c>();

        //Check unselected solution profiles and clear Partner_Expertise_Area_ID__c.
        Solution_Profiles__c sp;
        QVM_Product_Data__c qvmp;

        Boolean isSuccess = true;

        System.Debug('PartnerExpertiseArea.Expertise_Start_Date__c= '+ PartnerExpertiseArea.Expertise_Start_Date__c);

        if(PartnerExpertiseArea.Expertise_Start_Date__c != null)
        {
            System.Debug('PartnerExpertiseArea.Expertise_Education_Requirement__c= '+ PartnerExpertiseArea.Expertise_Education_Requirement__c);
            System.Debug('PartnerExpertiseArea.Expertise_Marketing_Requirement__c= '+ PartnerExpertiseArea.Expertise_Marketing_Requirement__c);
            System.Debug('PartnerExpertiseArea.Partner_Expertise_Fulfilled__c= '+ PartnerExpertiseArea.Partner_Expertise_Fulfilled__c);
            System.Debug('PartnerExpertiseArea.Expertise_Renewal_Date2__c= '+ PartnerExpertiseArea.Expertise_Renewal_Date2__c);

            if(PartnerExpertiseArea.Expertise_Education_Requirement__c && PartnerExpertiseArea.Expertise_Marketing_Requirement__c )
            {
                PartnerExpertiseArea.Partner_Expertise_Fulfilled__c     = true;
            }
            else
            {
                PartnerExpertiseArea.Partner_Expertise_Fulfilled__c     = false;
            }
            PartnerExpertiseArea.Expertise_Renewal_Date2__c = PartnerExpertiseArea.Expertise_Start_Date__c + 365;
        }
        else
        {
            PartnerExpertiseArea.Expertise_Renewal_Date2__c = null;
            PartnerExpertiseArea.Partner_Expertise_Fulfilled__c     = false;
        }


        try
        {
            System.Debug('Upserting PartnerExpertiseArea= '+ PartnerExpertiseArea);
            upsert(PartnerExpertiseArea);

        }
        Catch(DMLException ex)
        {
            System.debug('Issue updating partner expertise area' + ex.getMessage());
            System.debug('Issue updating partner expertise area' + ex.getStackTraceString());
            ApexPages.addMessages(ex);
            isSuccess=false;
        }

        List<Partner_Expertise_Area__c> pealist= [SELECT Id, Expertise_Area__c From Partner_Expertise_Area__c where Partner_Account_Name__c=:AccountId and Expertise_Area__c = :PartnerExpertiseArea.Expertise_Area__c];

        if (pealist.size() > 1)
        {
        	String msg = String.format(System.Label.PEA_Validation_Unique_Expertise_Area_Error, new String[] {pealist.get(0).Expertise_Area__c});
        	System.debug('Issue saving Partner Expertise Area, more than 2 with same expertise area was retrieved');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, msg));
            isSuccess = false;
            return null;
        }

        Partner_Expertise_Area__c pea = pealist.get(0);

        //Once we have update/insert all Partner Expertise Area we will have it id
        //Update selected QVM Prod PEAId field so they are linked to current partner expertise area id.
        getQVMPToUpdate(selectedQlikMarkets, PartnerExpertiseArea.Id, qvmpToUpdate);

        //Update unSelected QVM Prod PEAId field will remove the link if was set before.
        getQVMPToUpdate(unselectedQlikMarkets, null, qvmpToUpdate);

        //Update selected Solution Profiles so they are linked to current partner expertise area id.
        getSolutionProfilesToUpdate(selectedSolutions, PartnerExpertiseArea.Id, spToUpdate);

        //Update unSelected Solution Profiles PEAId field, it will remove the link if was set before.
        getSolutionProfilesToUpdate(unselectedSolutions, null, spToUpdate);


        try{
            if(qvmpToUpdate.size()>0)
            {
                update qvmpToUpdate;
            }
        }
        catch(DMLException ex)
        {
            System.debug('Issue updating qlikmarket product data after saving Partner Expertise Area');
            ApexPages.addMessages(ex);
            isSuccess = false;
        }

        try
        {
            if(spToUpdate.size()>0)
            {
                update spToUpdate;
            }
        }
        catch(DMLException ex)
        {
            System.debug('Issue updating Solution Profiles after saving Partner Expertise Area');
            ApexPages.addMessages(ex);
            isSuccess = false;
        }
       System.Debug('-----------------------');
       System.Debug('isSuccess && isCreate && isPartner: ' + (isSuccess && isCreate && isPartner));
       if (isSuccess && isCreate && isPartner)
       {
       		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, Label.PEA_Creation_Confirmation_Partners));
       }

       System.Debug('isSuccess && isCreate && !isPartner: ' + (isSuccess && isCreate && !isPartner));
       if (isSuccess && isCreate && !isPartner)
       {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, String.format(Label.PEA_Creation_Confirmation_Internal, new List<string> {PartnerExpertiseArea.Expertise_Area__c})));
       }

       ApexPages.StandardController paeController = new ApexPages.StandardController(PartnerExpertiseArea);
       PageReference savePageRef = paeController.save();

       //Tried to modify parameters of the current page, but does not work either to display the confirmation message.
       //PageReference pageReference = ApexPages.currentPage();
       //Map<String, String> params = pageReference.getParameters();
       //params.put('displayConfirmation', 'true');
       //params.put('varMode', 'view');
       //return pageReference;

       //Add display confirmation
       String sURL = savePageRef != null?savePageRef.getUrl():null;

       //if current page contains create, then add sURL += '&displayConfirmation=true';
       if (isSuccess && isCreate)
       {
            sURL = String.Format('/apex/AddEditPartnerExpertise?id={0}&varMode=view&displayConfirmation=true', new String[]{''+pea.Id});
       }

       if (sURL != null)
       {
         PageReference pageRef = new ApexPages.Pagereference(sURL);
         pageRef.setRedirect(true);
         return pageRef;
      }
      else
      {
      		return null;
      }
   }

    public PageReference cancel()
    {
        ApexPages.StandardController paeController = new ApexPages.StandardController(PartnerExpertiseArea);

        paeController.cancel();

        Account theParent=new Account(id=PartnerExpertiseArea.Partner_Account_Name__c);
        PageReference acctPage = new ApexPages.StandardController(theParent).view();
        acctPage.setRedirect(true);
        return acctPage;
    }

    public PageReference backtoConnectProgram()
    {
        ApexPages.StandardController paeController = new ApexPages.StandardController(PartnerExpertiseArea);

        paeController.cancel();

        PageReference connectProgramPage = new PageReference('/apex/ppQlikPartnerNetwork?SubPage=PEP'); //CR# 48632
        //PageReference connectProgramPage = new PageReference('/apex/ppQonnectProgramPage?SubPage=PEP');
        connectProgramPage.setRedirect(true);
        return connectProgramPage;
    }

    public PageReference deletewithredirect()
    {
        Pagereference pageReference = ApexPages.currentPage();
        ApexPages.StandardController paeController = new ApexPages.StandardController(PartnerExpertiseArea);

        if(selectedQlikMarkets.size()>0 || selectedSolutions.size()>0)
        {
            System.debug('Partner Expertise Area has Solution Profiles and/or QVM Products associated. Cancel deletion');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, 'Partner Expertise Areas has Solution Profiles and QlikMarket Applications associated. Please edit the Partner expertise area so does not have dependent records and try to delete again.'));
            return pageReference;
        }

        try{
            System.debug('Partner Expertise Area to delete: ' + PartnerExpertiseArea);
            delete PartnerExpertiseArea;//paeController.delete();

        }
        catch(DMLException ex){
            System.debug('Issue deleting Partner Expertise Area');
            ApexPages.addMessages(ex);
            return pageReference;
        }
        catch(Exception ex){
            System.debug('Issue deleting Partner Expertise Area');
            ApexPages.addMessages(ex);
            return pageReference;
        }

        Account theParent=new Account(id=PartnerExpertiseArea.Partner_Account_Name__c);
        PageReference acctPage = new ApexPages.StandardController(theParent).view();
        acctPage.setRedirect(true);
        return acctPage;
    }

}