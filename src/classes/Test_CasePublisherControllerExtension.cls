/*************************************************************
*Author: MTM Initial development
* Change log
* 2014-03-11 MTM CR# 8444 MTM Automation of Lead generation from Cases
* 2015-04-17 CCE CR# 16347 Renamed the testing Campaign name to remain in line with change made for this CR
* 2015-06-16 CCE CR 32912 - improved coverage by adding test for not in SF
* 2015-10-27 CCE Updated api version to 31, removed seeAllData, - for Winter 16 SF upgrade
* 2017-03-29 : Roman Dovbush  : duplicated QTTestUtils.GlobalSetUp() commented out.
**************************************************************/
@isTest//(SeeAllData=true)
public class Test_CasePublisherControllerExtension{
    
     private User adminUser;
     private Account testAccount;
     private Contact testContact;
     private Case testCase;
     private CasePublisherControllerExtension controller;
        
    public void Init()
    {
        //QTTestUtils.GlobalSetUp();  //setup Custom settings
        PageReference pageRef = new PageReference('/apex/CaseCustomPublish');
        Test.setCurrentPage(pageRef);
        RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'QlikTech Master Support Record Type' limit 1];
        
        adminUser = QTTestUtils.createMockSystemAdministrator();
        testAccount = QTTestUtils.createMockAccount('Zazlinger Account', adminUser);
        testContact = QTTestUtils.createMockContact(testAccount.Id);
                
        testCase = new Case(RecordTypeId = caseRT.Id,
                                Subject = 'Test Case N',
                                Status = 'Not Opened',
                                Description = 'Desc: Test Case N',    
                                Category__c = 'Applications', 
                                Priority = 'Medium',
                                Contact = testContact,
                                ContactId = testContact.Id,
                                Account = testAccount,
                                SuppliedEmail = 'abc@abc.com');
        insert testCase;
        ApexPages.StandardController StandardController = new ApexPages.StandardController(testCase );
        controller = new CasePublisherControllerExtension(StandardController);
        controller.ConsentCheckBox = true;
        controller.TypeOfLead = 'Software/License';
        
    }

    public void Init_NotInSF()
    {
        //QTTestUtils.GlobalSetUp();  //setup Custom settings
        PageReference pageRef = new PageReference('/apex/CaseCustomPublish');
        Test.setCurrentPage(pageRef);
        RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'QlikTech Master Support Record Type' limit 1];
        
        adminUser = QTTestUtils.createMockSystemAdministrator();
        testAccount = QTTestUtils.createMockAccount('Zazlinger Account', adminUser);
        testContact = QTTestUtils.createMockContact(testAccount.Id);
                
        testCase = new Case(RecordTypeId = caseRT.Id,
                                Subject = 'Test Case N',
                                Status = 'Not Opened',
                                Description = 'Desc: Test Case N',    
                                Category__c = 'Applications', 
                                Priority = 'Medium',
                                Contact = testContact,
                                ContactId = testContact.Id,
                                Account = testAccount,
                                SuppliedEmail = 'abc@abc.com');
        insert testCase;
        ApexPages.StandardController StandardController = new ApexPages.StandardController(testCase );
        controller = new CasePublisherControllerExtension(StandardController);
        controller.ConsentCheckBox = true;
        controller.NotInSFCheckBox = true;
        controller.TypeOfLead = 'Training/Consulting/Support';
        
        controller.ContactName = 'Seasick Steve';
        controller.AccountName = 'PO Ltd';
        controller.PhoneNumber = '555 1234';
        controller.Email = 'sss@po.com.sandbox';
        controller.Description = 'some stuff';
        
    }

    //Controllers account name/Phone numbers/Email should be Account Namae and  contact email and Phone number
    static testMethod void TestCasePublisherControllerExtension() 
    {
        QTTestUtils.GlobalSetUp();

        Test_CasePublisherControllerExtension testClass = new Test_CasePublisherControllerExtension();     
        testClass.Init();
        test.startTest();
        
        System.AssertEquals(testClass.controller.AccountName, testClass.testAccount.Name);
        System.AssertEquals(testClass.controller.Email,testClass.testContact.Email);
        System.AssertEquals(testClass.controller.PhoneNumber, testClass.testContact.Phone);
                
        test.stopTest();
    }

    //If TypeOfLead = 'Software/License'; the campign member will be created in 'Global-TM-Services'
    static testMethod void TestSubmitCampaign()
    {
        QTTestUtils.GlobalSetUp();

        Campaign campaign = new Campaign();
        campaign.Name = System.Label.Global_Service_Campaign;
        insert campaign;

        campaign = new Campaign();
        campaign.Name = System.Label.Global_Service_Other_Campaign;
        insert campaign;


        Test_CasePublisherControllerExtension testClass = new Test_CasePublisherControllerExtension();     
        testClass.Init();
        
        test.startTest();
        testClass.controller.SubmitCampaign();
        
        System.AssertEquals(testClass.controller.CampaignName, 'Global-TM-Services');
        System.Assert(testClass.controller.cMemberId != Null);
        CampaignMember cMember = [SELECT Id, ContactId,Status From CampaignMember WHERE Id =: testClass.controller.cMemberId];
        //Why isn't this target?
        //System.AssertEquals(cMember.Status, 'Target');
        test.stopTest();
    }
   
    static testMethod void TestSubmitCampaign_NotInSF()
    {
        QTTestUtils.GlobalSetUp();

        Test_CasePublisherControllerExtension testClass = new Test_CasePublisherControllerExtension();     
        testClass.Init_NotInSF();
        
        test.startTest();
        Boolean bRet = testClass.controller.EnableNextButton;
        testClass.controller.SubmitCampaign();
        
        System.AssertEquals(testClass.controller.CampaignName, 'Global-TM-Other Services');
        //Will not set member.status
        //System.Assert(testClass.controller.cMemberId != Null);
        //CampaignMember cMember = [SELECT Id, ContactId,Status From CampaignMember WHERE Id =: testClass.controller.cMemberId];
        //System.AssertEquals(cMember.Status, 'Target');
        test.stopTest();
    }
   
    static testMethod void TestMessages()
    {
        QTTestUtils.GlobalSetUp();

        Test_CasePublisherControllerExtension testClass = new Test_CasePublisherControllerExtension();     
        testClass.Init();
        
        test.startTest();
        testClass.controller.getMessage();
        test.stopTest();
    }

}