/**     * File Name:AccUpdateSpecializationHandler
        * Description : This handler class is used to update the account specialization fields on insert,update or delete of approved specialization records
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1       07.04.2017   Rodion Vakulovskyi Refactored class
*/
public class AccUpdateSpecializationHandler {

    /*  @Description :This common method is used to update account field after insert of specialization record

        @parameter inputList: Trigger.new Account list
        @parameter newMap   : Map specialization id and records.  
    */
    public static void onAfterInsert(List<Specialization__c> inputList, Map<Id, Specialization__c> newMap) {
        processSpec(inputList);
    }

    /*  @Description :This common method is used to update account field after insert of specialization record

        @parameter inputList: Trigger.new Account list
        @parameter newMap   : Map specialization id and records.
        @parameter oldMap   : Old version Map specialization id and records.  
    */
    public static void onAfterUpdate(List<Specialization__c> inputList, Map<Id, Specialization__c> newMap, Map<Id, Specialization__c> oldMap) {
        List<Specialization__c> listToProcess = new List<Specialization__c> ();
        for(Specialization__c itemSpec : inputList) {
            if(itemSpec.Designation_Status__c != oldMap.get(itemSpec.Id).Designation_Status__c || itemSpec.Specialization__c !=  oldMap.get(itemSpec.Id).Specialization__c) {
                listToProcess.add(itemSpec);
            }
        }
        if(listToProcess.size() > 0) {
            processSpec(inputList);
        }
    }


    /*  @Description :This common method is used to update account field after insert of specialization record

        @parameter inputList: Trigger.new Account list
        @parameter newMap   : Map specialization id and records.  
    */
    public static void onAfterDelete(List<Specialization__c> inputList, Map<Id, Specialization__c> newMap) {
        processSpec(inputList);
    }
    
    private static void processSpec(List<Specialization__c> inputList) {
        Set<Id> setOfAccs = new Set<Id>();
        for(Specialization__c specItem : inputList){
            setOfAccs.add(specItem.Account__c);
        }
        Map<Id, Account> mapOfAccs = new Map<Id, Account>([SELECT Name, Specialization__c, (SELECT Id, Account__c, Designation_Status__c, Specialization__c, Specialization_Category__c FROM Specializations__r)  FROM Account WHERE Id in :setOfAccs]);
        Map<Id, Set<String>> mapOfAccountSpecs = new Map<Id, Set<String>>();
        for(Id accountItem : mapOfAccs.keySet()) {
            mapOfAccs.get(accountItem).Specialization__c = '';
            for(Specialization__c itemSpec : mapOfAccs.get(accountItem).Specializations__r) {
                if(itemSpec.Designation_Status__c  == 'Approved') {
                    if(!mapOfAccountSpecs.containsKey(accountItem)) {
                        mapOfAccountSpecs.put(accountItem, new Set<String>{itemSpec.Specialization__c});
                    }
                    mapOfAccountSpecs.get(accountItem).add(itemSpec.Specialization__c);
                }
            }
        }
        for(Id accountItem : mapOfAccountSpecs.keySet()) {
            List<String> listOfSpecs = new List<String>();
            listOfSpecs.addAll(mapOfAccountSpecs.get(accountItem));
            for(String itemSpecString : listOfSpecs) {
                mapOfAccs.get(accountItem).Specialization__c += itemSpecString + ';';
            }
        }
        if(mapOfAccs.values().size() > 0){
            update mapOfAccs.values();
        }
    }
}