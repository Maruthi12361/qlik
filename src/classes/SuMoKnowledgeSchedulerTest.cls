@isTest
private class SuMoKnowledgeSchedulerTest{

    private static testMethod void scheduleCaseArticleCreatedProcessing_should_create_job(){
        SuMoKnowledgeScheduler.PROCESS = 'testprocess';
        Test.StartTest();
            system.assertNotEquals(null, SuMoKnowledgeScheduler.scheduleCaseArticleCreatedProcessing());        
        Test.StopTest();
    }
    
    private static testMethod void abortCaseArticleCreatedProcessing_should_abort_job(){
        SuMoKnowledgeScheduler.PROCESS = 'testprocess';
        id jobId = SuMoKnowledgeScheduler.scheduleCaseArticleCreatedProcessing();
        Test.StartTest();
            SuMoKnowledgeScheduler.abortCaseArticleCreatedProcessing();
            system.assertEquals(0, [select count() FROM CronTrigger where id =: jobId]);
        Test.StopTest();
    }
    
    private static testMethod void scheduleKnowledgeArticleFirstCreatedProcessing_should_create_job(){
        SuMoKnowledgeScheduler.PROCESS = 'testprocess';
        Test.StartTest();
            system.assertNotEquals(null, SuMoKnowledgeScheduler.scheduleKnowledgeArticleFirstCreatedProcessing());        
        Test.StopTest();
    }
    
     private static testMethod void abortKnowledgeArticleFirstCreatedProcessing_should_abort_job(){
        SuMoKnowledgeScheduler.PROCESS = 'testprocess';
        id jobId = SuMoKnowledgeScheduler.scheduleKnowledgeArticleFirstCreatedProcessing();
        Test.StartTest();
            SuMoKnowledgeScheduler.abortKnowledgeArticleFirstCreatedProcessing();
            system.assertEquals(0, [select count() FROM CronTrigger where id =: jobId]);
        Test.StopTest();
    }
    
     private static testMethod void scheduleKnowledgeArticlePublishedProcessing_should_create_job(){
        SuMoKnowledgeScheduler.PROCESS = 'testprocess';
        Test.StartTest();
            system.assertNotEquals(null, SuMoKnowledgeScheduler.scheduleKnowledgeArticlePublishedProcessing());        
        Test.StopTest();
    }
    
     private static testMethod void abortKnowledgeArticlePublishedProcessing_should_abort_job(){
        SuMoKnowledgeScheduler.PROCESS = 'testprocess';
        id jobId = SuMoKnowledgeScheduler.scheduleKnowledgeArticlePublishedProcessing();
        Test.StartTest();
            SuMoKnowledgeScheduler.abortKnowledgeArticlePublishedProcessing();
            system.assertEquals(0, [select count() FROM CronTrigger where id =: jobId]);
        Test.StopTest();
    }
 
    
    private static testMethod void process_should_create_event_record(){
        SuMoKnowledgeScheduler.PROCESS = 'testprocess';
        
        insert new Account(name = 'sumoTestAccount');
    
        Test.StartTest();
            
            datetime lastRunDate = datetime.now().addDays(-1);
            boolean postToEventObject = true;
            string soql = 'select id, createdById from Account where createdDate >: runFromDate';
            
            
            SuMoKnowledgeScheduler testSuMoKnowledgeScheduler = new  SuMoKnowledgeScheduler(soql, lastRunDate);
            testSuMoKnowledgeScheduler.process();
            
            system.assertEquals(1, [select count() FROM sumo_service_event__c]);
        
        
        Test.StopTest();
    }
    
    


}