/*************************************************************
* Name QTVoucher
* Description: Class to make future calls to qtvoucher webservice, see qtvoucherQliktechCom.cls
*              created from wsdl (Comment binding 12 to make sfdc "create class from wsdl" to work).
* 
* Log History:
*
*   2011-03-17  MHG     Initial Development
*   2011-04-17  RDZ     Changes for project to go live
*   2013-04-03  RDZ     Changes for QTNext Project, so QTVocher works with OppLineItems instead of NLRP
*                       CR# 7756 https://eu1.salesforce.com/a0CD000000XOxlt
*   2013-11-07  SAN     Create a single future method to create and then activate vouchers
*   2013-11-15  MAW     Replaced the create and activate voucher call to a single webService call 
*
*   2013-12-18  TJG     CR# 10211 Increase timeout limit to 60 seconds
*                       https://eu1.salesforce.com/a0CD000000detnE
*   2014-03-03  MAW     Increased the time limit of CallCreateAndActivateVouchersFromOpportunities to 2 minutes
*	2014-06-13  AIN     CR# 7283 https://eu1.salesforce.com/a0CD000000U9yv8
*   2014-07-18  MAW     Make a single call to CallCreateAndActivateVouchersFromOpportunities to avoid double vouchers
*   2015-06-18  TJG     Test coverage drop to 5%. A quick fix to get urgentdeployment done.
*   2018-03-26  AIN     IT-99 Updated class to include deactivate and reactivate vouchers call to WS
**************************************************************/
public with sharing class QTVoucher {
    
    @future (callout=true)
    public static void CallCreateVoucherFromNLRP(List<string> NLRPToUpdate)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
		string ReturnText = '';
        // CR# 10211 Increase timeout limit to 60 seconds stored in custom settings
        service.timeout_x = 60000;        
        for (integer i = 0; i < NLRPToUpdate.size(); i++ )
        {
            if (!Test.isRunningTest())
            {
                service.CreateVoucherFromNLRP(NLRPToUpdate[i], ReturnText);
            }
            system.Debug('Error Creating Voucher from NLRP Id [' +NLRPToUpdate[i]+']: ' + ReturnText);              
        }
    }
    
    @future (callout=true)
    public static void CallCreateVoucherFromOpportunity(List<string> OppIDsWithNLRP)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service);
        string ReturnText = '';
        // CR# 10211 Increase timeout limit to 60 seconds stored in custom settings
        service.timeout_x = 60000;          
        for (integer i = 0; i < OppIDsWithNLRP.size(); i++ )
        {
            if (!Test.isRunningTest())
            {
                service.CreateVoucherFromOpportunity(OppIDsWithNLRP[i], ReturnText);
            }
            system.Debug('Error Creating Vouchers from Opp Id [' +OppIDsWithNLRP[i]+']: ' + ReturnText);                
        }
    } 
    
    @future (callout=true)
    public static void CallCreateVouchersFromOpportunities(List<string> OppIDsWithEducationOLIs)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service);
        qtvoucherQliktechCom.ArrayOfString OppIds = new qtvoucherQliktechCom.ArrayOfString();
        
        OppIds.string_x = OppIDsWithEducationOLIs;
        
        string ReturnText = '';
        if (!Test.isRunningTest())
        {
            service.CreateVouchersFromOpportunities(OppIds, ReturnText);
        }
        
        if (ReturnText != null && ReturnText != '')
        {
            system.Debug('Error Creating Vouchers from Opp Ids: ' + ReturnText);
        }   
        
    } 
    
    @future (callout=true)
    public static void CallActivateVouchersFromOpportunity(List<string> OppIDsWithInactiveVouchers)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service);
        string ReturnText = '';
        // CR# 10211 Increase timeout limit to 60 seconds stored in custom settings
        service.timeout_x = 60000;          
        for (integer i = 0; i < OppIDsWithInactiveVouchers.size(); i++ )
        {
            if (!Test.isRunningTest())
            {
                service.ActivateVouchersFromOpportunity(OppIDsWithInactiveVouchers[i], ReturnText);
            }
            system.Debug('Error Activating Vouchers from Opp Id [' +OppIDsWithInactiveVouchers[i]+']: ' + ReturnText);              
        }
    } 
    
    @future (callout=true)
    public static void CallActivateVouchersFromOpportunities(List<string> OppIDsWithEducationOLIs)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service);
        qtvoucherQliktechCom.ArrayOfString OppIds = new qtvoucherQliktechCom.ArrayOfString();
        
        OppIds.string_x = OppIDsWithEducationOLIs;
        
        string ReturnText = '';
        // CR# 10211 Increase timeout limit to 60 seconds stored in custom settings
        service.timeout_x = 60000;
        if (!Test.isRunningTest())
        {        
            service.ActivateVouchersFromOpportunities(OppIds, ReturnText);
        }

        if (ReturnText != null && ReturnText != '')
        {
            system.Debug('Error Activating Vouchers from Opp Ids: ' + ReturnText);
        }   
        
    } 


    @future (callout=true)
    public static void CallCreateAndActivateVouchersFromOpportunities(List<string> OppIDsWithEducationOLIs)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service);
        qtvoucherQliktechCom.ArrayOfString OppIds = new qtvoucherQliktechCom.ArrayOfString();
        
        OppIds.string_x = OppIDsWithEducationOLIs;
        
        string ReturnText = '';
        // create voucher and activate them, if for whatever reason we got web service down, try twice before give up

        // CR# 10211 Increase timeout limit to 120 seconds stored in custom settings
        service.timeout_x = 120000;          
        Boolean quit = false;
        integer loopcount = 0;
        //while(!quit ) //2014-07-18  MAW     Make a single call to CallCreateAndActivateVouchersFromOpportunities to avoid double vouchers
        {
            loopcount++;
            try
            {
                //CreateVouchersFromOpportunities will create and activate the vouchers so we donot need to explicitly call activate for the voucher
                if (!Test.isRunningTest())
                {
                    service.TryVoucherCreationAndActivation(OppIds, ReturnText);
                }
                
                if (ReturnText != null && ReturnText != '')
                {
                    system.Debug('Error Creating Vouchers from Opp Ids: ' + ReturnText);
                }
            }
            catch(Exception Ex) 
            {
                system.Debug('Error in Vouchers creation with exception: ' + Ex.getMessage());
            }
            
            // each web service call have 120 seconds latency anyway, so if it failed due to timeout, try again the second time
            if (loopcount > 2)
                quit = true;
        }
    }
    //AIN for IT-99
    @future (callout=true)
    public static void CallDeactivateVouchersOnOpportunities(List<string> oppIdsToCancel)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        qtvoucherQliktechCom.ArrayOfString OppIds = new qtvoucherQliktechCom.ArrayOfString();
        
        OppIds.string_x = oppIdsToCancel;
        
        string ReturnText = '';
        // create voucher and activate them, if for whatever reason we got web service down, try twice before give up

        // CR# 10211 Increase timeout limit to 120 seconds stored in custom settings
        service.timeout_x = 120000;          
        Boolean quit = false;
        integer loopcount = 0;
        //while(!quit ) //2014-07-18  MAW     Make a single call to CallCreateAndActivateVouchersFromOpportunities to avoid double vouchers
        {
            loopcount++;
            try
            {
                //CreateVouchersFromOpportunities will create and activate the vouchers so we donot need to explicitly call activate for the voucher
                if (!Test.isRunningTest())
                {
                    service.DeactivateVouchersOnOpportunities(OppIds, ReturnText);
                }
                
                if (ReturnText != null && ReturnText != '')
                {
                    system.Debug('Error deactivating Vouchers from Opp Ids: ' + ReturnText);
                }
            }
            catch(Exception Ex) 
            {
                system.Debug('Error in voucher deactivation with exception: ' + Ex.getMessage());
            }
            
            // each web service call have 120 seconds latency anyway, so if it failed due to timeout, try again the second time
            if (loopcount > 2)
                quit = true;
        }
    }
    //AIN for IT-99
    @future (callout=true)
    public static void CallReactivateVouchersOnOpportunities(List<string> oppIdsToCancel)
    {
        qtvoucherQliktechCom.ServiceSoap service = new qtvoucherQliktechCom.ServiceSoap();
        QTWebserviceUtil.SetWebServiceEndpoint(service);
        qtvoucherQliktechCom.ArrayOfString OppIds = new qtvoucherQliktechCom.ArrayOfString();
        
        OppIds.string_x = oppIdsToCancel;
        
        string ReturnText = '';
        // create voucher and activate them, if for whatever reason we got web service down, try twice before give up

        // CR# 10211 Increase timeout limit to 120 seconds stored in custom settings
        service.timeout_x = 120000;          
        Boolean quit = false;
        integer loopcount = 0;
        //while(!quit ) //2014-07-18  MAW     Make a single call to CallCreateAndActivateVouchersFromOpportunities to avoid double vouchers
        {
            loopcount++;
            try
            {
                //CreateVouchersFromOpportunities will create and activate the vouchers so we donot need to explicitly call activate for the voucher
                if (!Test.isRunningTest())
                {
                    service.ReactivateVouchersOnOpportunities(OppIds, ReturnText);
                }
                
                if (ReturnText != null && ReturnText != '')
                {
                    system.Debug('Error deactivating Vouchers from Opp Ids: ' + ReturnText);
                }
            }
            catch(Exception Ex) 
            {
                system.Debug('Error in voucher deactivation with exception: ' + Ex.getMessage());
            }
            
            // each web service call have 120 seconds latency anyway, so if it failed due to timeout, try again the second time
            if (loopcount > 2)
                quit = true;
        }
    }

}