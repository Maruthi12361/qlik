/***************************************
Class: CampaignAddPrimaryMarketingRequestTest
TestClass for: CampaignAddPrimaryMarketingRequest.trigger

Changes Log:
2018-03-01  CCE Initial creation
****************************************/
@isTest
private class CampaignAddPrimaryMarketingRequestTest {
	
	@isTest static void createCampaignWithPrimaryMarketingAsset() {
		Marketing_Asset_NEW__c ma = new Marketing_Asset_NEW__c();
        ma.Name = 'MATEST1';
        ma.CurrencyIsoCode = 'USD';
        ma.Asset_Title__c = 'MA-Title1';
        ma.Asset_Short_Title__c = 'MA-Title1';
        ma.Function__c = 'IT';
        ma.Language__c = 'English';
        ma.Source__c = 'TP - Third Party';
        ma.Sub_Source__c = 'TP - Other';
        ma.Type__c = 'Video';
        insert ma;
        
        Campaign campaign = new Campaign();
        campaign.Name = 'CampaignTester1';
        campaign.Primary_Marketing_Asset__c = ma.Id;
        insert campaign;        
        
        List<Link_Marketing_Asset__c> lma = [Select Id, Campaign__c, Marketing_Asset_NEW__c from Link_Marketing_Asset__c where Campaign__c = :campaign.Id AND Marketing_Asset_NEW__c = :ma.Id];
        System.assertEquals(lma.size(), 1);
	}
	
	@isTest static void updateCampaignWithPrimaryMarketingAsset() {
        Marketing_Asset_NEW__c ma = new Marketing_Asset_NEW__c();
        ma.Name = 'MATEST1';
        ma.CurrencyIsoCode = 'USD';
        ma.Asset_Title__c = 'MA-Title1';
        ma.Asset_Short_Title__c = 'MA-Title1';
        ma.Function__c = 'IT';
        ma.Language__c = 'English';
        ma.Source__c = 'TP - Third Party';
        ma.Sub_Source__c = 'TP - Other';
        ma.Type__c = 'Video';
        insert ma;
        
        Campaign campaign = new Campaign();
        campaign.Name = 'CampaignTester1';
        insert campaign;

        List<Link_Marketing_Asset__c> lma = [Select Id, Campaign__c, Marketing_Asset_NEW__c from Link_Marketing_Asset__c where Campaign__c = :campaign.Id AND Marketing_Asset_NEW__c = :ma.Id];
        System.assertEquals(lma.size(), 0);

        Semaphores.TriggerHasRun('CampaignAddPrimaryMarketingRequest', 1);
        campaign.Primary_Marketing_Asset__c = ma.Id;
        update campaign;    
        
        lma = [Select Id, Campaign__c, Marketing_Asset_NEW__c from Link_Marketing_Asset__c where Campaign__c = :campaign.Id AND Marketing_Asset_NEW__c = :ma.Id];
        System.assertEquals(lma.size(), 1);
	}
	
}