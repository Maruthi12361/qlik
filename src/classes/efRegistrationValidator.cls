//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//          
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efRegistrationValidator{
    private efRegistrationWrapper r;
    private efEventWrapper e;
    private efContactWrapper c;
    private Account account;
    private String attendeeType;
    private String subAttendeeType;
    private String userType;
    
    public efRegistrationValidator(efRegistrationManager regManager){
        this.r = regManager.getRegistrationWrapper();
        this.c = regManager.getContactWrapper();
        this.e = regManager.getEventWrapper();
        this.account = regManager.getAccount();
        this.attendeeType = regManager.getAttendeeType();
        this.userType = regManager.getUserType();
        this.subAttendeeType = regManager.getSubAttendeeType();        
    }
    
    public Boolean getIsValidPhone(){
        //return efUtility.isValidPhone(c.getContact().phone);
        return true;
    }

    public Boolean getIsValidMobilePhone(){
        return efUtility.isValidPhone(r.getMobilePhone());
    }
    
    public Boolean getIsValidMailingState(){
        return efUtility.isValidState(c.getContact().mailingState, c.getContact().mailingcountry);
    }

    public Boolean getIsValidFirstName(){
        return !efUtility.isNull(c.getContact().firstName);
    }
        
    public Boolean getIsValidLastName(){
        return !efUtility.isNull(c.getContact().lastName);
    }
    
    public Boolean getIsValidMailingStreet(){
        return !efUtility.isNull(c.getContact().mailingstreet);
    }
    
    public Boolean getIsValidMailingCity(){
        return !efUtility.isNull(c.getContact().mailingcity) &&
               c.getContact().mailingcity.length() <= 40;       
    }
    
    public Boolean getIsValidMailingCountry(){
        return !efUtility.isNull(c.getContact().mailingcountry);
    }
    
    public Boolean getIsValidMailingPostalCode(){
        return efUtility.isValidZipCode(c.getContact().mailingpostalcode);
    }
        
    public Boolean getIsValidSpecialRequirements(){
        if (r.getRequiresSpecialAssistance())
           return !efUtility.isNull(r.getSpecialRequirements());
        else
           return true;
    }    
    
    public Boolean getIsValidIndustry(){
        return !efUtility.isNull(account.Industry);
    }
    
    public Boolean getIsValidCompanySize(){
        return !efUtility.isNull(account.Company_Size__c);
    }
        
    public Boolean getIsValidDepartment(){
        //return !efUtility.isNull(r.getDepartment());
        return true;
    }
    
    public Boolean getIsValidPrimaryRole(){
        return !(efUtility.isNull(r.getPrimaryRole()) && !(r.getPrimaryRole()=='-- Select One --'));
        return true;
    }
    
    public Boolean getIsValidJobLevel(){
        //return !efUtility.isNull(c.getContact().job_level__c);
        return true;
    }
    
    public Boolean getIsValidJobFunction(){
        //return !efUtility.isNull(c.getContact().job_function__c);
        return true;
    }
    
    public Boolean getIsValidTrackInterest(){
        return !efUtility.isNull(r.getTrackInterest());
    } 
        
    public Boolean getIsValidWhyRegister(){
        return !efUtility.isNull(r.getWhyRegister());
    }
    
    public Boolean getIsValidWantToLearn(){
        return r.getWantToLearn().size() > 0;
    }

    public Boolean getIsValidEmergencyPhone(){
        return efUtility.isValidPhone(r.getEmergencyPhone()) ||
               (efUtility.isNull(r.getEmergencyContact()) && efUtility.isNull(r.getEmergencyPhone()));
    }
    
    public Boolean getIsValidEmergencyPhoneCheck(){
        return !efUtility.isNull(r.getEmergencyPhone());               
    }
    
    public Boolean getIsValidEmergencyRelationship(){
       return !efUtility.isNull(r.getEmergencyRelationship());
    }
    
    public Boolean getIsValidEmergencyContact(){
        return !efUtility.isNull(r.getEmergencyContact());
    }
    
    public Boolean getIsValidJobTitle(){
        return !efUtility.isNull(c.getJobTitle());
    }
    
    public Boolean getIsValidExtendedStay(){
        if (r.getExtendedStay()){           
            efPicklists pl = new efPicklists();
            List<SelectOption> arrivals = pl.getEventExtendedStayArrivalList(e.getEvent().Extended_Stay_Start_Date__c, e.getEvent().Extended_Stay_End_Date__c);
            List<SelectOption> departures = pl.getEventExtendedStayDepartureList(e.getEvent().Extended_Stay_Start_Date__c, e.getEvent().Extended_Stay_End_Date__c);
            
            Integer arrIdx = 0;
            Integer depIdx = 0;
                    
            
            for (Integer i=0; i < arrivals.size(); i++){
                if (arrivals.get(i).getValue() == r.getExtendedStayArrival()){
                    arrIdx = i;
                    break;
                }
            }
            
            for (Integer i=0; i < departures.size(); i++){
                if (departures.get(i).getValue() == r.getExtendedStayDeparture()){
                    depIdx = i+1; // Comment & : add 1 to compensate for the 1 day offset.
                    break;
                }
            }
            return (depIdx>arrIdx);
        } else { return true; }
    }
    
    public Boolean getIsValidSpouseOffer()
    {
        if (r.getSpouseAddition())
           return !efUtility.isNull(r.getSpouseFirstName()) && !efUtility.isNull(r.getSpouseLastName());
        else
           return true;
    }

    public Boolean getIsBillingCountryNeedVAT()
    {
        if (!efUtility.isNull(account.Billling_Country_Code_Name__c))
        {
            if(account.Billling_Country_Code_Name__c == 'AUT' ||
                account.Billling_Country_Code_Name__c == 'BEL' ||
                account.Billling_Country_Code_Name__c == 'BGR' ||
                account.Billling_Country_Code_Name__c == 'CYP' ||
                account.Billling_Country_Code_Name__c == 'CZE' ||
                account.Billling_Country_Code_Name__c == 'DNK' ||
                account.Billling_Country_Code_Name__c == 'EST' ||
                account.Billling_Country_Code_Name__c == 'FIN' ||
                account.Billling_Country_Code_Name__c == 'FRA' ||
                account.Billling_Country_Code_Name__c == 'DEU' ||
                account.Billling_Country_Code_Name__c == 'GRC' ||
                account.Billling_Country_Code_Name__c == 'HUN' ||
                account.Billling_Country_Code_Name__c == 'IRL' ||
                account.Billling_Country_Code_Name__c == 'ITA' ||
                account.Billling_Country_Code_Name__c == 'LVA' ||
                account.Billling_Country_Code_Name__c == 'LTU' ||
                account.Billling_Country_Code_Name__c == 'LUX' ||
                account.Billling_Country_Code_Name__c == 'MLT' ||
                account.Billling_Country_Code_Name__c == 'NLD' ||
                account.Billling_Country_Code_Name__c == 'POL' ||
                account.Billling_Country_Code_Name__c == 'PRT' ||
                account.Billling_Country_Code_Name__c == 'ROU' ||
                account.Billling_Country_Code_Name__c == 'SVK' ||
                account.Billling_Country_Code_Name__c == 'SVN' ||
                account.Billling_Country_Code_Name__c == 'ESP' ||
                account.Billling_Country_Code_Name__c == 'SWE' ||
                account.Billling_Country_Code_Name__c == 'GBR')
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }
        else
           return false;
    }
    
    public Boolean getIsValidRegistrationData(){
        Boolean isValid = true;
        isValid = getIsValidPhone() &&
                getIsValidMailingState() &&
                getIsValidFirstName() &&
                getIsValidLastName() &&
                getIsValidMailingStreet() &&
                getIsValidMailingCity() &&
                getIsValidMailingCountry() &&
                getIsValidMailingPostalCode() &&
                getIsValidEmergencyPhoneCheck() &&
                getIsValidEmergencyPhone() &&
                getIsValidEmergencyRelationship()&&
                getIsValidEmergencyContact()&&
                getIsValidIndustry()&&
                getIsValidJobTitle()&&
                getIsValidJobLevel()&&
                getIsValidJobFunction()&&
                getIsValidSpecialRequirements()&&
                getIsValidExtendedStay()&&
                getIsValidSpouseOffer() &&
                getIsValidPrimaryRole() &&
                getIsValidMobilePhone();
                
        if(attendeeType.contains('Employee'))
            isValid = isValid  && getIsValidDepartment();
                                    
        return isValid;
    }
    // End validation methods.  
}