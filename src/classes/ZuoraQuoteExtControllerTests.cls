/********************************
* 2018-12-20 - AIN - Added creation of Mock System Administrator in testRecall and assigned permission set for Zuora Admin to the user to 
*                    prevent test class failure.
* 2019-02-14 - AIN - Fixed errors related to new duplicate rule
********************************/
@isTest
private class ZuoraQuoteExtControllerTests {
    
    testMethod static void testSubmit() {
        testSetup();
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        insert secondPartner;
 
        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Distributor', 'Distributor', 'Distributor');
        insert pcs;
 
        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;
 
        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;
 
        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;
 
        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Distributor';
        insert testOpp;
 
        Test.startTest();
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        insert zquote;
        ZuoraQuoteExtController con = new ZuoraQuoteExtController(new ApexPages.StandardController(zquote));
        con.onSubmit();
        zquote = [SELECT ApprovalStatus__c FROM zqu__Quote__c WHERE Id = :zquote.Id LIMIT 1];
       
        Test.stopTest();
        
        System.assertEquals('Approved', zquote.ApprovalStatus__c);
    }
    
    @testSetup
    private static void testSetup() {
        QuoteTestHelper.createCustomSettings();
    }



    testMethod static void testRecall() {

        User admin = QTTestUtils.CreateMockSystemAdministrator();
        PermissionSet ps = [select Id from PermissionSet where Name = 'Zuora_Quotes_Admin_User' limit 1];
        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.Id, AssigneeId = admin.Id);
        insert psa;

        zqu__Quote__c zquote = new zqu__Quote__c();

        system.runas(admin)
        {

            testSetup();
            Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
            insert endUserAccount;
            endUserAccount.Pending_Validation__c =FALSE;
            update endUserAccount;
     
            Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
            insert partner;
     
            Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
            insert secondPartner;
     
            Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Distributor', 'Distributor', 'Distributor');
            insert pcs;
     
            Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
            insert pcs2;
     
            Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
            insert endUserContact;
     
            Contact partnerContact = Z_TestFactory.makeContact(partner);
            partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
            partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
            partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
            insert partnerContact;
     
            Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
            testOpp.Sell_Through_Partner__c = partner.Id;
            testOpp.Partner_Contact__c = partnerContact.Id;
            testOpp.Second_Partner__c = secondPartner.Id;
            testOpp.Revenue_Type__c = 'Distributor';
            insert testOpp;
     
            
            Test.startTest();
            zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
            zquote.zqu__BillToContact__c = endUserContact.id;
            zquote.zqu__SoldToContact__c = endUserContact.id;
            insert zquote;
            ZuoraQuoteExtController con = new ZuoraQuoteExtController(new ApexPages.StandardController(zquote));
            con.onRecall();
            zquote = [SELECT ApprovalStatus__c FROM zqu__Quote__c WHERE Id = :zquote.Id LIMIT 1];
        }
        Test.stopTest();
        
        System.assertEquals('Recalled', zquote.ApprovalStatus__c);
    }
}