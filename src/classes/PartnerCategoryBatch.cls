/**     * File Name: PartnerCategoryBatch
        * Description : This apex batch is for updating partner category staus fields(2 lookup fields)
        * @author : Rodion Vakulovskyi
        * Jira Id : QCW-3903
        * Modification Log :
        
        Ver     Date         Author            
        1      18/09/2017  Rodion Vakulovskyi 
        2      04/10/2017  Rodion Vakulovskyi 
		3	   20/11/2017  Bala Egambaram
     
*/
global class PartnerCategoryBatch implements Database.Batchable<sObject>{ 
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('Start of apex batch');
        SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
        SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
        SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
        SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
        String query = 'SELECT Id, PartnerCategoryStatusSTP__c, Bypass_Rules__c, Revenue_Type__c, PartnerCategoryStatusSecond__c, SBQQ__Notes__c, Sell_Through_Partner__c,  Second_Partner__c, SBQQ__Account__c, SBQQ__Account__r.RecordType.Name FROM SBQQ__Quote__c WHERE (PartnerCategoryStatusSTP__c = NULL AND PartnerCategoryStatusSecond__c = NULL) AND (Sell_Through_Partner__c != NULL OR Second_Partner__c != NULL) AND SBQQ__Status__c != \'Order Placed\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SBQQ__Quote__c> scope){
        SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
        SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
        SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
        SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
        CustomQuoteTriggerHandler.ScheduleRun = false;
        PartnerCategoryBatchHandler.processBatch(scope);
    }

    global void finish(Database.BatchableContext BC){
        System.debug('Finish of apex batch');
    }
}