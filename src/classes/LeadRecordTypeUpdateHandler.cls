/******************************************************
Class: LeadRecordTypeUpdateHandler
Initiator: BAD
    
Changelog:
    2017-06-28  BAD 	Handler class for LeadRecordTypeUpdateHandler trigger
						Code is migrated from trigger RecordTypeUpdate
						Change log has also been copied from the trigger RecordTypeUpdate

						///////////////// Changelog inherited from RecordTypeUpdate /////////////////
						* XX-XX-2011 Initial   PMT Changes for https://eu1.salesforce.com/a0CD000000ETe7g 
						* XX-XX-2011 	       MHG Modifying PMT Attempt
						* XX-XX-2011           SSH Comment out the Standard Lead bit asked by Patrick and adding Original_Partner_OwnerId__c bit.	
						* 25-10-2011           RDZ Adding logs and comment for RecordIds in Live and QTTEST
						*					   https://eu1.salesforce.com/a0CD000000ETe7g
						*
						* 21-02-2013		   TJG CR 6883 https://eu1.salesforce.com/a0CD000000U7iiM
						*						Business Need 
						*							Some Partner-initiated Leads are being flagged as QT Originated, when a lead is assigned back 
						*							to a Partner User (most common in regions working with Master Resellers).
						*						Change Description
						*							As part of the Partner Opp Reg Lead process, there is an existing trigger which enables a QT user 
						*							to reassign a Standard Lead from CRM, to a Partner User in PRM. Once the Lead Owner is changed to 
						*							the Partner User, a trigger updates the lead record type to Partner Opp Reg, plus populates 2 x fields
						*							 - QT Originated and QT Originated Date. Please can this trigger be refined to reference the 'Created By' 
						*							detail of the Lead, so that the QT Originated/QT Originated Dates fields are not populated, if the lead 
						*							has a PRM User as the create user? I have attached a list of PRM Profiles. In short, 
						*							QT Originated/QT Originated Date will be populated when assigning a Lead to a Partner User EXCEPT 
						*							when the Lead has a PRM User as Created By. 
						*
						* 04-11-2015            CCE CR# 58149 https://eu1.salesforce.com/a0CD000000wFDLQ Test for "Junk" in addition to "False" and "Lead - Never"
						* 20170608   			CCE CHG0031443 - This is to resolve an issue where it is stopping partner leads from being rejected.  See INC0095366.
						///////////////////////////////////////////////////////

	2017-09-22 - AYS -  BMW-396/CHG0032034 - Added logic for Archived Record Type.
						* If Status is changed to "Suspect" the Record Type changes to the value in the "Archived: Original Record Type" field. 
						* If no value is present make the Record Type = Standard Lead.

******************************************************/


public class LeadRecordTypeUpdateHandler {
    static final String DEBUGPREFIX = 'LeadRecordTypeUpdateHandler:';    

	public LeadRecordTypeUpdateHandler() {

	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {

	    if(!Semaphores.TriggerHasRun('LeadRecordTypeUpdateHandler'))
	    {
	        Boolean createdByPRMUserHasRun = false;

	        ZiftSettings__c csZift = ZiftSettings__c.getInstance();

	        Set<string> OwnerIDs = new Set<string>();
	        for (integer i = 0; i < triggerNew.size(); i++)
	        {
	            if (!OwnerIDs.Contains(triggerNew[i].OwnerId))
	            {
	                OwnerIDs.Add(triggerNew[i].OwnerId);
	                System.debug(DEBUGPREFIX + ' 1 OwnerIDs: ' + OwnerIDs);           
	            }       
	        }
	        OwnerIDs.Add(UserInfo.getUserId());
	        System.debug(DEBUGPREFIX + ' 2 OwnerIDs: ' + OwnerIDs);
	        
	        // all Lead IDs where CreatedBy user is a PRM
	        Set<Id> leadsCreatedByPrm = new Set<Id>();
	        System.Debug(DEBUGPREFIX + ' leadsCreatedByPrm' + leadsCreatedByPrm);    
	        Map<string, string> UserToUserType = new Map<string, string>();
	        Map<string, string> UserToAccountOwner = new Map<string, string>();
	        Map<string, string> PartnerOppQueueIdToName = new Map<string, string>();
	        Map<id, User> OwnersDetails = new Map<id, User>();
	        for (User u : [select Id, UserType, Contact.Account.OwnerId, Username from User where Id in :OwnerIDs])
	        {
	            UserToUserType.Put(u.Id, u.UserType);
	            UserToAccountOwner.Put(u.Id, u.Contact.Account.OwnerId);
	            OwnersDetails.Put(u.Id, u);
	            System.debug(DEBUGPREFIX + ' OwnersDetails: ' + OwnersDetails);
	        }
	        //Commenting to save the Select as we don't seem to have any queues that start with 'Partner Opp Reg Queue'
	        //for (Group g: [select Id, Name from Group WHERE Type = 'Queue' AND Name like 'Partner Opp Reg Queue%'])
	        //{
	        //     PartnerOppQueueIdToName.Put(g.Id, g.Name);     
	        //} 

	        for (integer i = 0; i < triggerNew.size(); i++)
	        {
	            if (OwnersDetails.ContainsKey(triggerNew[i].OwnerId))
	            {
	                System.debug(DEBUGPREFIX + ' loop');
	                string UserName = OwnersDetails.get(triggerNew[i].OwnerId).Username;
	                if (UserName != null)
	                {
	                    System.debug(DEBUGPREFIX + ' UserName: ' + UserName);
	                    if (!UserName.toUpperCase().contains('@QLIKVIEW.COM') && !UserName.toUpperCase().contains('@QLIKTECH.COM') && triggerNew[i].Opp_Reg_Rejected__c == false)
	                    //CCE 20170608 Not sure who added the QLIK.COM test or why (nothing in the header) so I'm removing it as it seems to stop it working 
	                    //if (!UserName.toUpperCase().contains('@QLIKVIEW.COM') && !UserName.toUpperCase().contains('@QLIKTECH.COM') && triggerNew[i].Opp_Reg_Rejected__c == false && !UserName.toUpperCase().contains('@QLIK.COM'))
	                    {
	                        triggerNew[i].Original_Partner_OwnerId__c = triggerNew[i].OwnerId;
	                        System.debug(DEBUGPREFIX + ' triggerNew[i].Original_Partner_OwnerId__c: ' + triggerNew[i].Original_Partner_OwnerId__c);
	                    }
	                }
	            }

	            if (triggerNew[i].OwnerId != triggerOld[i].OwnerId)   // Only trigger on Owner change 
	            {
	                System.debug(DEBUGPREFIX + ' OwnerId change');
	                if (PartnerOppQueueIdToName.ContainsKey(triggerNew[i].OwnerId) ||
	                    (UserToUserType.ContainsKey(triggerNew[i].OwnerId) && 
	                    (UserToUserType.Get(triggerNew[i].OwnerId) == 'PowerCustomerSuccess' || 
	                     UserToUserType.Get(triggerNew[i].OwnerId) == 'PowerPartner'
	                     )))
	                {
	                    // Partner record type here
	                    // QTTEST 01220000000DaCfAAK = 'Partner Opp Reg'
	                    // LIVE 012D0000000JsWAIA0 = 'Partner Opp Reg'
	                    if (triggerNew[i].Status != 'False' && triggerNew[i].Status != 'Lead - Never' && triggerNew[i].Status != 'Junk')
	                    {
	                        System.Debug(DEBUGPREFIX + ' i = ' + i);
	                        System.Debug(DEBUGPREFIX + ' triggerNew[i].Id = ' + triggerNew[i].Id + ', CreatedById = ' + triggerNew[i].CreatedById);
	                        if(triggerNew[i].Opp_Reg_Rejected__c == false)
	                        {
	                            if (!createdByPRMUserHasRun)
	                            {
	                                for (Lead lead : [Select Id From Lead where CreatedBy.Profile__c like 'PRM %' AND Id in :triggerNew])
	                                {
	                                    leadsCreatedByPrm.add(lead.Id);
	                                    System.debug(DEBUGPREFIX + ' leadsCreatedByPrm: ' + leadsCreatedByPrm);  
	                                }
	                                createdByPRMUserHasRun = true;
	                            }

	                            triggerNew[i].RecordTypeId = '012D0000000JsWAIA0'; //Partner Opp Reg
	                            if (!leadsCreatedByPrm.Contains(triggerNew[i].Id)){
	                                triggerNew[i].QT_Originated__c = true;
	                                System.Debug(DEBUGPREFIX + ' QT_Originated__c set to true');
	                            }
	                        }
	                    }
	                }   
	                //else 
	                //{
	                //    // Default record type here
	                //    // 01220000000DOzwAAG = 'Standard lead'
	                //    if (triggerNew[i].Status != 'False' && triggerNew[i].Status != 'Lead - Never')
	                //    {
	                //        //triggerNew[i].RecordTypeId = '01220000000DOzwAAG';
	                //        // Removed on req. by Patrick on 2011-06-01
	                //        //triggerNew[i].QT_Originated__c = false;
	                //    }
	                //}
	                
	            }
	            else if (UserToUserType.ContainsKey(UserInfo.getUserId()) &&
	                     UserToAccountOwner.ContainsKey(UserInfo.getUserId()) &&
	                        (UserToUserType.Get(UserInfo.getUserId()) == 'PowerPartner' ||
	                        UserToUserType.Get(UserInfo.getUserId()) == 'PowerCustomerSuccess') &&
	                     triggerNew[i].Status != triggerOld[i].Status &&
	                     triggerNew[i].Status == 'Qualified - B')
	            {
	                System.debug(DEBUGPREFIX + ' Change OwnerId');
	                triggerNew[i].OwnerId = UserToAccountOwner.Get(UserInfo.getUserId());
	            } 

	            //BMW-396
	            if(triggerNew[i].Status == 'Suspect' && triggerOld[i].Status != 'Suspect'){	     
	            	if(triggerNew[i].Old_Record_Type_Name__c != null){
	            		try{
	            			triggerNew[i].RecordTypeId =  Schema.SObjectType.Lead.getRecordTypeInfosByName().get(triggerNew[i].Old_Record_Type_Name__c).getRecordTypeId();
	            		}catch(exception e){
	            			triggerNew[i].RecordTypeId = csZift.RT_StandardLead__c;
	            			System.debug('debug_Old_Record_Type_Name_error: ' + e.getMessage());
	            		}
	            	}else{
	            		triggerNew[i].RecordTypeId = csZift.RT_StandardLead__c;
	            	}	            	
	            }
	        }
	    }
	}

}