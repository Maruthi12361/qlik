/*
* File RefreshApp
    * @description : Performs Sandbox Refresh activity.Logic moved from Sandbox Update Tab
                     It will be executed after QA refresh in prod.
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       12.01.2018   Pramod Kumar V      Created Class 
  */

Global class RefreshApp{  
   
   
    Global static string TrainingUserFormatStr = '~Training User{0} (Retired)';

    list <String> ApexClassInputObjList = new List<String>(); 
 
    Global static String VfPageUrlValue;
    //Global String gSandboxName;

    Global String getSandboxName() {
        //Get the User Name to extract the Sandbox Name  
        String lSandBoxName = UserInfo.getUserName();

        String gSandboxName;

            if(lSandBoxName.contains('com.')){
                    List <String> lSandBoxSplit = lSandBoxName.split('com.',2);  
                    gSandboxName= lSandBoxSplit[1];
            }
            else{
                gSandboxName = 'test';
            }

        
        system.debug('gSandboxName inside getSandboxName: ' + gSandboxName);
        return gSandboxName;
    }
  
     Global static void RemoteSettings() {
        // Salesforce VF page remote setting needs to be setup before we try to deactivate the Workflow from VF page
    
        MetadataService.MetadataPort service = createService();
  
        MetadataService.RemoteSiteSetting remoteSiteSettings = new MetadataService.RemoteSiteSetting();
        remoteSiteSettings.fullName = 'SalesforceVFPage';
        //remoteSiteSettings.url = 'https://c.cs8.visual.force.com';
        remoteSiteSettings.url = 'https://'+URL.getSalesforceBaseUrl().getHost()+'/services/Soap/m/27.0';
        system.debug('hostURL '+remoteSiteSettings.url );
       // remoteSiteSettings.url = 'https://test.com';
        remoteSiteSettings.description='Salesforce VF Page Access';
        remoteSiteSettings.isActive=true;
        remoteSiteSettings.disableProtocolSecurity=false;
    
        MetadataService.AsyncResult[] results0 = service.create(new List<MetadataService.Metadata> { remoteSiteSettings });
        MetadataService.AsyncResult[] checkResults0 = service.checkStatus(new List<string> {string.ValueOf(results0[0].Id)});
        system.debug('RemoteSetting chk' + checkResults0 );
    }
 
    /***********************************************************************************
    ********************* Email Template Section***************************************
    ***********************************************************************************/
  
    List <Sandbox_Refresh__c> gEmailTempList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();
    List <EmailTemplate> gEmailTempList= new List<EmailTemplate>();
    List <EmailTemplate> gEmailTempList_Backup= new List<EmailTemplate>();
 
    // Return the Email Templates list to the UI
    Global list<EmailTemplate> getEmailTempList_ToModify() {
        list <EmailTemplate> leTObjList = new List<EmailTemplate>();
        List<String> lemailTempNameList = new List<String>(); 
        String lSandBoxName = UserInfo.getUserName();

        String gSandboxName;

            if(lSandBoxName.contains('com.')){
                    List <String> lSandBoxSplit = lSandBoxName.split('com.',2);  
                    gSandboxName= lSandBoxSplit[1];
            }
            else{
                gSandboxName = 'test';
            }    
   
        gEmailTempList_Of_SandBoxObj= [Select Name From Sandbox_Refresh__c where  Component__c='EmailTemplate'  ];
   
        if(!gEmailTempList_Of_SandBoxObj.isEmpty()) {
            /* gEmailTempList_Of_SandBoxObjis not - EmailTemplates Name read from Sandbox_Refresh__c */
            for(Sandbox_Refresh__c temp:gEmailTempList_Of_SandBoxObj ) {
                lemailTempNameList.add(temp.Name); 
            }
        } else {
            /* gEmailTempList_Of_SandBoxObj is Empty - Emails Templates Hard Coded. 
            Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done.*/
            lemailTempNameList.add('Account - Orbis - send to Navision');
            lemailTempNameList.add('Account - Orbis - send OEM to Navision');
            lemailTempNameList.add('PRM: Partner Qualified Orbis Trigger');
            lemailTempNameList.add('PRM: Partner Rejected Trigger');
            lemailTempNameList.add('CCS Trigger for SoE to XL');
            lemailTempNameList.add('Q2O Realign Account Licenses'); 
            lemailTempNameList.add('KMH - Sandbox Test Template');      
        }
  
        leTObjList = [Select Id,Name,subject from EmailTemplate where name=:lemailTempNameList];
        //if(!EmailTempObjList.isEmpty() )
        {
            //clear and build the new list
            gEmailTempList.Clear(); 
            gEmailTempList_Backup.Clear();
        }
   
        for(EmailTemplate  template:leTObjList) {
            if(template.Subject !=NULL) {
                List <String> lSubsplit  = template.Subject.split(' ',2);
     
                //Add Org id only if its not been added
                if(!lSubsplit[0].equals(gSandboxName)) {
                    template.Subject = gSandboxName + ' ' + template.Subject;     
                }
            } else {
                //  template.Subject = gSandboxName;
            }
            gEmailTempList.add(template);
        }
      
        gEmailTempList_Backup= gEmailTempList.deepClone(true,true,true);  

        return gEmailTempList;
    }
  
    // Method to update EmailTempalte 
    Global void Update_EmailTempalteRefresh_All() {
        System.debug('Size of gEmailTempList'+gEmailTempList.size());
        for(integer i=0; i<gEmailTempList.size();i++ ) {
            if(!gEmailTempList[i].Subject.equals(gEmailTempList_Backup[i].Subject)) {
                gEmailTempList_Backup[i] = gEmailTempList[i];
            }   
        }
        update(gEmailTempList_Backup);
    }
 
    /*********************End of Email Template Section*************************************/



    /***********************************************************************************
         ********************* Common Functions ***************************************
      ***********************************************************************************/
      
     /*Global list<String> getApexClassList()
     {
       ApexClassInputObjList.add('ulcv3QlikvieCom.cls');
       ApexClassInputObjList.add('efQTPaymentGateway.cls');
       ApexClassInputObjList.add('sharinghandlerQlikviewCom.cls ');   
       ApexClassInputObjList.add('qtvoucherQliktechCom.cls');
       ApexClassInputObjList.add('qtecustomsQliktechCom.cls ');
       ApexClassInputObjList.add('Test_Triggers_PRM_Referral.cls ');
       
       return ApexClassInputObjList ;
     }*/
     
     /* QlikTech Companies - To create record for Sweden
        need for Sandbox App to run.
     */
    Global void InitFunc() {
        // First Create the Sweden company record
        Create_QlikComp();
    }
 @future
    Global static void Create_QlikComp() {
        list  <QlikTech_Company__c> QComp= [Select Name,ID,QlikTech_Company_Name__c,Country_Name__c,QlikTech_Region__c,QlikTech_Sub_Region__c,
            QlikTech_Operating_Region__c,Language__c,SLX_Sec_Code_ID__c    
            From QlikTech_Company__c WHERE Name = 'SWE' Limit 1 ];
    
        if (QComp.isempty()) {
            List<Subsidiary__c> subList = [Select Id from Subsidiary__c where Legal_Country__c = 'Sweden' limit 1];
            Subsidiary__c sub;
            if (subList.isempty()) {
                sub = new Subsidiary__c(
                    Legal_Country__c = 'Sweden',
                    Legal_Entity_Name__c = 'QlikTech Nordic AB');
                insert sub;
            } else {
                sub = subList.get(0);
            }
            QlikTech_Company__c LqComp = new QlikTech_Company__c ( 
                Name = 'SWE',
                QlikTech_Company_Name__c='Nordics',
                Country_Name__c='Sweden',
                QlikTech_Region__c='Sweden',
                QlikTech_Sub_Region__c='Sweden',
                QlikTech_Operating_Region__c='Sweden',
                Language__c='Swedish',
                SLX_Sec_Code_ID__c    ='SW',
                Subsidiary__c = sub.Id);      
            insert LqComp;
      
            System.Debug('Sweden Company Name added');
        }
    }
 
  
 
    // Refresh all the Components 
    Global void Refresh_All() {
       // Pagereference p;   
        //UpdateWorkFlows();
        UpdateCases();
        Update_EmailTempalteRefresh_All();   
        Update_TrainingUsers_All();  
        Update_CustomSetting(VfPageUrlValue );   
   
    }
    /*********************End of Common Function *************************************/
 
 
 
    /***********************************************************************************
    ********************* Qlikbuy Section***************************************
    ***********************************************************************************/
    // Custom Settings
  
    Global String getVfPageUrlValue() {
        /* Check Sandbox Name and update URL in the 
        CPQ Pages(CPQ.page, CPQQuoteEdit.page, CPQQuoteNew.page, Quotes_In_CPQ.page )
        Assign
        QA -> sandbox
        QTDev,QTRelation,QTSysdev  -> s60test
        */
        String lSandBoxName = UserInfo.getUserName();

        String gSandboxName;

            if(lSandBoxName.contains('com.')){
                    List <String> lSandBoxSplit = lSandBoxName.split('com.',2);  
                    gSandboxName= lSandBoxSplit[1];
            }
            else{
                gSandboxName = 'test';
            }
        if( gSandboxName.toUpperCase() == 'QA' ) {
            VfPageUrlValue = 'sandbox';
        }
        else {
            VfPageUrlValue = 'v60test';
        }
        return VfPageUrlValue; 
    }
  
    Global void setVfPageUrlValue(String nd) {
        VfPageUrlValue = nd;      
    }
  
    @future
    Global static void Update_CustomSetting(String lvfPageUrlValue) {
        RefreshSandbox__c rSandb = new RefreshSandbox__c ();   
        if(rSandb == null) {
            rSandb = new RefreshSandbox__c (setupOwnerId = System.Userinfo.getOrganizationId());            
            rSandb.VFPageUrlValue__c= lvfPageUrlValue;
        }
        else { 
            //rSandb =  RefreshSandbox__c.getvalues(System.UserInfo.getOrganizationId()); 
            rSandb =  RefreshSandbox__c.getinstance(System.UserInfo.getOrganizationId()); 
            if(rSandb.id == null) {
                System.debug('Kauser Custom setting null');
            } else {
                rSandb.VFPageUrlValue__c= lvfPageUrlValue;
            }
        }   
        if(rSandb.id == null) { 
            insert rSandb;
        }
        else {
            update(rSandb);
        }
    }
    /*********************End of Qlikbuy Section************************************* /
  
 
 
    
 

 
    /***********************************************************************************
    ********************* Training User Section************************************
    ***********************************************************************************/
  
    // Training User 
    // Global static List <Sandbox_Refresh__c> gTrainingUserNameList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();
  
    // Global static List<User> TrainUserList  = new List<User>(); //Global Training user list
    // Global static List<User> TrainUserList_ToUpdate  = new List<User>(); //Global Training user list
  
    List <Sandbox_Refresh__c> gTrainingUserNameList_Of_SandBoxObj = new List<Sandbox_Refresh__c>();
  
    List<User> TrainUserList  = new List<User>(); //Global Training user list
    List<User> TrainUserList_ToUpdate  = new List<User>(); //Global Training user list
 
    Global List<User> getTrainUserInfo() {
        List<String> lTrainUserList= new List<String>();   
 
        gTrainingUserNameList_Of_SandBoxObj = [Select Name From Sandbox_Refresh__c where  Component__c='User'  ];
     
        if(!gTrainingUserNameList_Of_SandBoxObj.isEmpty()) {
   
            /* gTrainingUserNameList_Of_SandBoxObj  not - UserName read from Sandbox_Refresh__c */
            for(Sandbox_Refresh__c temp:gTrainingUserNameList_Of_SandBoxObj) {
                lTrainUserList.add(temp.Name); 
            }
        }
        else {
            /* gTrainingUserNameList_Of_SandBoxObj is Empty - User list Hard Coded. 
            Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done.*/
            for (integer i=1; i < 16; i++) {
                lTrainUserList.add(String.format(TrainingUserFormatStr, new String[]{''+i})); //Name for Training Users 1-15
            }
        }
   
        TrainUserList = [SELECT Id,IsActive,Name,ProfileId,Trigger_CPQ_user_creation__c,UserRoleId, Country FROM User WHERE Name =:lTrainUserList];     
      System.debug('TrainUserList_ToUpdate:2 ' + lTrainUserList);
      System.debug('TrainUserList_ToUpdate:3 ' + TrainUserList );
        for(User tuser:TrainUserList) {
            tuser.ProfileId = '00e20000001OyLc'; //Qlikbuy Sales Std User
        
            tuser.UserRoleId = '00E20000000vwic' ; // UK - Presales
            tuser.IsActive = true;
            tuser.Trigger_CPQ_user_creation__c = true;     
        }

        TrainUserList_ToUpdate = TrainUserList.deepClone(true,true,true);  
  
        return TrainUserList;
    }
 
    // Update Training Users in Sandbox
    Global void Update_TrainingUsers_All() {
    //deactivate users 
      List<User> users = [Select LastLoginDate , IsActive, Id From User where LastLoginDate < :Date.today().addDays(-60) AND IsActive =true AND Profile.UserLicense.Name='Salesforce' order by LastLoginDate limit 20];
    System.debug('The following error has occurred.'+users); 
    
    for(User u : users)

    {

        u.IsActive = False;

    }
    Database.SaveResult[] srList = Database.update(users, false);
    for (Database.SaveResult sr : srList) {

    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed

        System.debug('Successfully inserted user. userID: ' + sr.getId());

    }

    else {

        // Operation failed, so get all errors               

        for(Database.Error err : sr.getErrors()) {

            System.debug('The following error has occurred.');                   

            System.debug(err.getStatusCode() + ': ' + err.getMessage());

            System.debug('Account fields that affected this error: ' + err.getFields());

        }

    }
}
 System.debug('TrainUserList_ToUpdate:1 ' + TrainUserList_ToUpdate);

        for(integer i=0; i<TrainUserList_ToUpdate.size();i++ ) {
            if(TrainUserList_ToUpdate[i].ProfileId != TrainUserList[i].ProfileId ||
                TrainUserList_ToUpdate[i].UserRoleId != TrainUserList[i].UserRoleId ||
                TrainUserList_ToUpdate[i].IsActive != TrainUserList[i].IsActive ||
                TrainUserList_ToUpdate[i].Trigger_CPQ_user_creation__c != TrainUserList[i].Trigger_CPQ_user_creation__c) {
                    TrainUserList_ToUpdate[i] = TrainUserList[i];
            }
        }
        if(!Test.isRunningTest())
            update(TrainUserList_ToUpdate);
    }
 
 
 
    /*********************End of Training User Section*************************************/
 
    /***********************************************************************************
    ********************* WorkFlow Section************************************
    ***********************************************************************************/
  
    //WorkFlow
    Global static List <Sandbox_Refresh__c> gWorkFlowList= new List<Sandbox_Refresh__c>();

    Global List <Sandbox_Refresh__c> getWorkflowList() {
        List <Sandbox_Refresh__c> lWorkFlowList= new List<Sandbox_Refresh__c>();
    
        lWorkFlowList = [SELECT Id,isActive__c,Name,Component__c,Value_To_Change__c FROM Sandbox_Refresh__c WHERE Component__c='WorkFlow' ];
    
    
        if(!lWorkFlowList.isEmpty()) {
            /* lWorkFlowList is not Empty - WorkFlow Name read from Sandbox_Refresh__c */
            for(Sandbox_Refresh__c temp:lWorkFlowList) {
                gWorkFlowList.Clear();  
                gWorkFlowList.add(temp); 
            }
        }
        else {
            /* lWorkFlowlist empty so hard code the values- 
            Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done. */
            gWorkFlowList.Clear();     
            Sandbox_Refresh__c temp = new Sandbox_Refresh__c();
            temp.Name= 'PRM - Partner User Created requires Forecasting setup';
            gWorkFlowList.add(temp);
        }  
        return gWorkFlowList;
    }
 
    // This is API is used to for MetaData API access
    Public static MetadataService.MetadataPort createService() { 
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;     
    }


   

    Global static void UpdateWorkFlows() {
    
        if (gWorkFlowList!= null && gWorkFlowList.size()>0) {
            UpdatePRMWorkFlow( gWorkFlowList[0].isActive__c);
        }
    }
    Global static void UpdatePRMWorkFlow(Boolean isActive) {     
        //Now Set up WorkFlow that needs to be updated
        MetadataService.MetadataPort service = createService();
    
        MetadataService.WorkflowRule workflowRule1 = new MetadataService.WorkflowRule();
   
        workflowRule1.fullName='Contact.PRM - Partner User Created requires Forecasting setup';
        workflowRule1.active=isActive;
        workflowRule1.booleanFilter='1 AND 2 AND 3 AND 4';
        workflowRule1.triggerType='onCreateOrTriggeringUpdate';
       
        MetadataService.FilterItem wcriteriaItem1 = new MetadataService.FilterItem();
        wcriteriaItem1.field= 'Contact.Allow_Partner_Portal_Access__c';
        wcriteriaItem1.operation= 'equals';
        wcriteriaItem1.value= 'true';
      

        MetadataService.FilterItem wcriteriaItem2 = new MetadataService.FilterItem();
        wcriteriaItem2.field= 'Contact.Partner_Portal_Status__c';
        wcriteriaItem2.operation= 'contains';
        wcriteriaItem2.value='Complete';
      

        MetadataService.FilterItem wcriteriaItem3 = new MetadataService.FilterItem();
        wcriteriaItem3.field= 'Contact.ULC_Password__c';
        wcriteriaItem3.operation= 'notEqual';
        wcriteriaItem3.value= '';
       

        MetadataService.FilterItem wcriteriaItem4 = new MetadataService.FilterItem();
        wcriteriaItem4.field= 'Contact.Premier_Support_Mail_Sent__c';
        wcriteriaItem4.operation= 'equals';
        wcriteriaItem4.value= '';   
       
        workflowRule1.criteriaItems = new list<metadataservice.FilterItem>{wcriteriaItem1,wcriteriaItem2,wcriteriaItem3,wcriteriaItem4  }; 
       
      
        MetadataService.updateMetadata ut = new MetadataService.updateMetadata();
        ut.currentName='Contact.PRM - Partner User Created requires Forecasting setup';
        ut.metadata= workflowRule1;
        MetadataService.AsyncResult[] results = service.updateMetadata(new List<MetadataService.updateMetadata> {ut});
        
        MetadataService.AsyncResult[] checkResults_workflowRule1 = service.checkStatus(new List<string> {string.ValueOf(results[0].Id)});
            
        System.debug('Kauser checkResults_workflowRule1: '+ checkResults_workflowRule1 );
    }
 
    /*********************WorkFlow Section*************************************/
 
    /***********************************************************************************
    ********************* Case Section************************************
    ***********************************************************************************/
 

    Global static List<Case> gCaseList  = new List<Case>();
    Global static List <Sandbox_Refresh__c> gCaseNameList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();
 
    Global static List<ID> gCaseID_To_Delete  = new List<ID>();
 
    Global static List<Case> getCaseList() {
        gCaseList .Clear();
 
        gCaseList   = [SELECT Id,CaseNumber,Subject,Employee_First_Name__c,Employee_Last_Name__c, RecordTypeId 
            From    Case 
            WHERE 
                Confidential_New_Starter__c ='true'
                and RecordTypeId='01220000000DdyTAAS'
                and isDeleted=false];
   

        return gCaseList  ;
    } 


    Global static void UpdateCases() {
        for(Case caseTemp:gCaseList) {
            gCaseID_To_Delete.add(caseTemp.Id);  
        }
        DeleteCases(gCaseID_To_Delete);
    }
 
    @future
    Global static void DeleteCases(List<Id> idToDelete) {
        List<Case> lCaseList_To_Delete  = new List<Case>();
    
        lCaseList_To_Delete  = [SELECT Id,CaseNumber,Subject,Employee_First_Name__c,Employee_Last_Name__c From Case WHERE Id =:idToDelete ];
   
        delete lCaseList_To_Delete  ;    
   
    }
    /*********************Case Section*************************************/ 
   
     }