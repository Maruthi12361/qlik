// CR# 24909
// controller class behind the visualforce page: Opportunity_SOE_PDF_Template
// Change log:
// February 22, 2015 - Initial Implementation - Stuart Sleight
public with sharing class OpportunitySoEPDFTemplateCtrl {
    private Opportunity opp;
    public List<Sequence_of_Event__c> lSoE {get;set;}
    public Boolean showSoE {get;set;}    
    
    public OpportunitySoEPDFTemplateCtrl(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();

        showSoE = false;
        lSoE = [SELECT S_o_E_Date__c, Checkpoint__c, Proposed_Event__c, Description__c, Cost__c,
                    Responsibility__c
                    FROM Sequence_of_Event__c WHERE Opportunity_ID__c = :opp.Id];
       if(lSoE.size() > 0) showSoE = true;
    }
}