@isTest
private class TestDataFactoryTest {
	
	private static Id recTypeId;
	private static Id networkId;

	@isTest static void test1() 
	{
		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
		insert qtc;

		Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_Thomas', 'test_Jones', 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        
        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;

        Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);
        insert caseObj;

        FeedItem feedItem = TestDataFactory.createFeedItem('Test Feed Item', caseObj.Id);
        //insert feedItem;

        Diagnostic__kav basicKav =TestDataFactory.createDiagnosticKnowledgeArticle('test apex', 'test from apex', 'test', 'en_US');

        Basic__kav newBasicKav = TestDataFactory.createBasicKnowledgeArticle('test apex', 'test from apex', 'test-apex-Basic', 'en_US');
        //insert newBasicKav;

        Category_Lookup__c cl = TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level','Third Category Level Type', true, 'Please email urgently', 'question1', 'question2', 'question3', 'question4', 'question5', '6546546, 6549687', null, false, 'QlikTech Master Support Record Type', 'Systems Queue');

        TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference', '1234123412341234');

        

        EntitySubscription entitySubscription = TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser.Id, caseObj.Id);

        Survey__c survey = TestDataFactory.createSurvey((String) caseObj.accountId, (String) caseObj.Id, 'Test Survey', 'Test Notes', (String) caseObj.contactId, (String) caseObj.CaseNumber, (String) caseObj.Subject);

        QS_Persona_Category_Mapping__c businessUser = TestDataFactory.createPersonaCategoryMapping( 'Business Users', 'Product:Server_Publisher:AJAX, Product:Server_Publisher:Mobile, Product:Server_Publisher:IE_Plugin, Product:Server_Publisher:Desktop_Client', 'Layout & Visualizations');
        insert businessUser;

        QS_HomePage_Chatter_Announcement__c announcementObj = TestDataFactory.createChatterAnnouncement('Announcement', 'As you might already know, we recently released the next-generation data visualisation and discovery application - Qlik Sense Desktop', 'Sam Qlik Engineer (Qlik.com)', 'Do you have any Questions about Qlik Sense Desktop?');
  		insert announcementObj;

  		CollaborationGroup collaborationGroup = TestDataFactory.createChatterGroup(userinfo.getUserId(), getNetworkIdCustom(), 'Blog', 'Blog', 'Test Body', 'Test Description');
        insert collaborationGroup;

        TestDataFactory.createCountryLicenseSupport('Afghanistan', 'Lund');

        QS_Support_Contact_Info__c sci = TestDataFactory.supportContactInfo('Test Country 1', '1800 137 636 (Toll free) | 02 6111 2045', 'testSupportQlik1@email.com', 'Monday - Friday between 8:00 am - 5:00 pm Sydney EST/EDT');

    
	}

	private static Id getCaseRecordTypeId(String recordTypeName) 
	{
	    if(recTypeId != null) {
	        return recTypeId;
	    } else {
	            
	        recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
	        return recTypeId;
	    }
    }
    private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
                User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];  
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }
	
}