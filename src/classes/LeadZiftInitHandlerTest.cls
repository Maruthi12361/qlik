/*********************************************
	Class:LeadZiftHandlerTest

	Description: 
				Test class for LeadZiftInitHandler 

	Log History:
	2017-11-08    BAD    Created - CHG0032328
	2018-10-09	ext_bjd	 ITRM-26 Fix test related with portal account owner error.
**********************************************/
@isTest
private class LeadZiftInitHandlerTest {
    @isTest
    static void test_setOwnerToZift() {
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        LeadZiftInitHandler tempObj = new LeadZiftInitHandler();
        User mockPortalAccountUsr = QTTestUtils.createMockPortalUser();
        User ziftUser;
        Lead lead;

        System.runAs(mockPortalAccountUsr) {

            Profile profile = [select id from profile where name = 'Custom: Api Only User'];
            ziftUser = new User(
                    alias = 'newUser', email = 'zift@badtest.com.test',
                    Emailencodingkey = 'UTF-8', firstname = 'Zift', lastname = 'API User',
                    Languagelocalekey = 'en_US', localesidkey = 'en_US',
                    Profileid = profile.Id,
                    Timezonesidkey = 'America/Los_Angeles',
                    Username = 'zift@badtest.com.test'
            );
            insert ziftUser;

            Account acc = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
            system.assert(acc.Id != null);

            HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
            setting.Name = 'Default';
            setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
            insert setting;

            lead = new Lead(
                    FirstName = 'Lead',
                    LastName = 'Test1001',
                    Company = 'Test',
                    Country = 'Sweden',
                    Email = 'asd@ad.com',
                    Zift_Lead_Partner__c = acc.Id,
                    Zift_Distribution_Status__c = 'Send to Zift',
                    Zift_Rejection_Reason__c = 'No Capacity',
                    Zift_Unqualified_Invalid_Reason__c = 'No Budget or Interest',
                    Zift_Sales_Rep_Email__c = 'partner@test.com',
                    Zift_Sales_Rep_Name__c = 'Partner',
                    Zift_Registered_Lead__c = true,
                    Zift_SSO_CompanyID__c = acc.Id,
                    Zift_Partner_Id__c = '123456',
                    Zift_Partner_Name__c = 'Partner Name'
            );
            insert lead;

            Campaign camp = new Campaign(
                    Name = 'BADTestCampaign',
                    Type = 'Campaign type',
                    Campaign_Sub_Type__c = 'Campaign sub-type'
            );
            insert camp;

            CampaignMember cmLead = new CampaignMember(
                    LeadId = lead.Id,
                    CampaignId = camp.Id,
                    Status = 'Responded'
            );
            insert cmLead;
        }

        Test.startTest();

        System.runAs(ziftUser) {
            lead.OwnerId = ziftUser.Id;
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
            update lead;
        }

        Lead l = [SELECT Latest_Campaign_Interaction__c, Latest_Campaign_Type__c, Latest_Campaign_Sub_Type__c FROM Lead WHERE Id = :lead.id];

        System.assertEquals('BADTestCampaign', l.Latest_Campaign_Interaction__c);
        System.assertEquals('Campaign type', l.Latest_Campaign_Type__c);
        System.assertEquals('Campaign sub-type', l.Latest_Campaign_Sub_Type__c);

        Test.stopTest();
    }
}