/********************************************************
* CLASS: ConvertInvitedUsersTest
* DESCRIPTION: Test class for ConvertInvitedUsers
*
*
* CHANGELOG:
*   2019-10-17 - BAD - BMW-1792 - Added Initial logic
*********************************************************/
@IsTest
public class ConvertInvitedUsersTest {

@testSetup 
public static void testSetup() {

       List<QlikTech_Company__c> QlikCompanies = new List<QlikTech_Company__c>();
        SIC_Code__c code = new SIC_Code__c(Name ='aaa', Industry__c='bbbb');
        insert code;

        QlikTech_Company__c qc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
            qc.Country_Name__c = 'Sweden';
            qc.QlikTech_Region__c = 'NORDIC';
            qc.QlikTech_Sub_Region__c = 'NORDIC';
            qc.QlikTech_Operating_Region__c = 'N EUROPE';
            qc.Language__c = 'Swedish';
        QlikCompanies.add(qc);
        upsert QlikCompanies;

        List<Account> Accounts = new List<Account>();
        Account a = new Account();
        a = new Account();
        a.Name = 'The Base Company';
        a.Navision_Status__c = '';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);        
        Account a2 = new Account();
        a2.Name = 'The Test Partner';
        a2.Navision_Status__c = 'Partner';
        a2.BillingCountry = 'Sweden';
        a2.Billing_Country_Code__c = QlikCompanies[0].Id;
        a2.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a2.Shipping_Country_Code__c = QlikCompanies[0].Id;
        a2.E_Mail_Domain__c = 'thetestpartner.com';
        Accounts.add(a2);
        insert Accounts;

        List<Contact> Contacts = new List<Contact>();
        Contact c = new Contact();
        c.AccountId = Accounts[0].Id;
        c.FirstName = 'First Base';
        c.LastName = 'Last Base';
        c.Email = 'first.last@thetestbase.com';
        c.No_Auto_CPView__c = false;
        Contacts.add(c);
        Contact c2 = new Contact();
        c2.AccountId = Accounts[1].Id;
        c2.FirstName = 'First';
        c2.LastName = 'Last';
        c2.Email = 'first.last@thetestpartner.com';
        c2.ActiveULC__c = true;
        c2.No_Auto_CPView__c = false;
        Contacts.add(c2);        
        insert Contacts;        

        List<Lead> Leads = new List<Lead>();
        Lead l1 = new Lead();
        l1.FirstName = 'FirstTest';
        l1.LastName = 'LastTest';
        l1.Email = 'first.last2@testbad.com';
        l1.Country_Code__c = QlikCompanies[0].Id;
        l1.Country_Name__c = QlikCompanies[0].Id;
        l1.Country = 'Sweden';
        l1.Company = 'The Lead';
        Leads.add(l1);
        Lead l2 = new Lead();
        l2.FirstName = 'FirstTest';
        l2.LastName = 'LastTest';
        l2.Email = 'test.olle@testersson.com';
        l2.Country_Code__c = QlikCompanies[0].Id;
        l2.Country_Name__c = QlikCompanies[0].Id;
        l2.Country = 'Sweden';
        l2.Company = 'The Lead';
        Leads.add(l2);        
        insert Leads;        

        List<Product_Trial__c> trials = new List<Product_Trial__c>();
        Product_Trial__c trial1 = new Product_Trial__c();
        trial1.Contact__c = Contacts[0].Id;
        trial1.Contact_Account__c = Accounts[0].Id;
        trial1.Trial_Status__c = 'Purchased';
        trial1.Product__c = 'Qlik Sense Business';
        trial1.Trial_License_Key__c = 'Qlik Sense Business';
        trial1.Start_Date__c = system.today();
        trial1.End_Date__c = system.today();        
        trial1.Pending_Conversion__c = true;
        trials.add(trial1);
        Product_Trial__c trial2 = new Product_Trial__c();
        trial2.Contact__c = Contacts[1].Id;
        trial2.Contact_Account__c = Accounts[1].Id;
        trial2.Trial_Status__c = 'Purchased';
        trial2.Product__c = 'Qlik Sense Business';
        trial2.Trial_License_Key__c = 'Qlik Sense Business';
        trial2.Start_Date__c = system.today();
        trial2.End_Date__c = system.today();        
        trial2.Pending_Conversion__c = true;
        trials.add(trial2);        
        insert trials;

        List<Product_User__c> pus = new List<Product_User__c>();
        Product_User__c pu1 = new Product_User__c();
        pu1.Lead__c = Leads[0].Id;
        pu1.Product_Trial__c = trials[0].Id;
        pu1.Status__c = 'Active';
        pu1.User_Role__c = 'Invited';
        pus.add(pu1);
        Product_User__c pu2 = new Product_User__c();
        pu2.Lead__c = Leads[1].Id;
        pu2.Product_Trial__c = trials[1].Id;
        pu2.Status__c = 'Active';
        pu2.User_Role__c = 'Invited';
        pus.add(pu2);
        Product_User__c pu3 = new Product_User__c();
        pu3.Contact__c = Contacts[0].Id;
        pu3.Product_Trial__c = trials[0].Id;
        pu3.Status__c = 'Active';
        pu3.User_Role__c = 'Service Account Owner';
        pu3.Zuora_Subscription_ID__c = '123456';
        pus.add(pu3);
        insert pus;
}

    @istest
    public static void testConvertLeads() 
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        ConvertInvitedUsers tempObj = new ConvertInvitedUsers(); //we don't use this instance, but create it to execute the class constructor

 	    System.runAs (mockSysAdmin) 
 	    {
            test.startTest();

            ConvertInvitedUsers.convertLeads();

            Lead l = [SELECT Id, Name, Email, isConverted FROM Lead WHERE email = 'first.last2@testbad.com'];
            Lead lPartner = [SELECT Id, Name, Email, isConverted FROM Lead WHERE email = 'test.olle@testersson.com'];

            system.assertEquals(true, l.isConverted);            
            system.assertEquals(false, lPartner.isConverted);            

            test.stopTest();
        }
    }

    @istest
    public static void testConvertEmailDomain() 
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
 	    System.runAs (mockSysAdmin) 
 	    {
            string leadEmail = 'test.testsson@testmail.com';
            string emailDomain = 'testmail.com';
            Boolean result = false;

            test.startTest();

            result = ConvertInvitedUsers.IsEmailDomainMatched(leadEmail, emailDomain);
            system.assertEquals(true, result);            

            leadEmail = 'test.testsson@testmailNoMatch.com';
            result = ConvertInvitedUsers.IsEmailDomainMatched(leadEmail, emailDomain);
            system.assertEquals(false, result);            

            test.stopTest();
        }
    }

    @istest
    public static void testSendEmail() 
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
 	    System.runAs (mockSysAdmin) 
 	    {
            string msg = 'This is a test';

            test.startTest();

            ConvertInvitedUsers.sendEmail(msg);

            test.stopTest();
        }
    }

    @istest
    public static void testSchedule() 
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
 	    System.runAs (mockSysAdmin) 
 	    {
            test.startTest();

                ConvertInvitedUsersSchedule sh1 = new ConvertInvitedUsersSchedule();
                String sch = '0 0 23 * * ?'; 
                system.schedule('Test Schedule', sch, sh1); 

            test.stopTest();
        }
    }

}