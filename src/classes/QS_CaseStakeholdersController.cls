/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public class QS_CaseStakeholdersController {
    
    public Case parentCase;
    public Id subId{get;set;}
    private map<Id,EntitySubscription> subsByUserIdMap = new map<Id,EntitySubscription>();
    private map<Id,User> possibleSubs = new map<Id,User>();
    private set<Id> subbedUserIds = new set<Id>();
    public boolean atLeastOneSubscriber {get;set;}

    private list<SelectOption> newSubscribersList;
    
    public QS_CaseStakeholdersController(ApexPages.StandardController controller){
        
        parentCase = (Case)controller.getRecord();
        system.debug(parentCase);
        
        //possibleSubs = getPossibleSubs();
        //getNewSubList();

        updateState();
    }
    
    public list<EntitySubscription> stakeholders{
        get{
            if(stakeholders == null) stakeholders = new list<EntitySubscription>();
            stakeholders = subsByUserIdMap.values();
            return stakeholders;
        }
        set;
    }
    
    public Id userId{get;set;}
    public list<SelectOption> newSubList{
        get{
            
            return newSubscribersList;
        }
    }
    private void updateState()
    {
        system.debug('updateState start');
        subsByUserIdMap = getSubsByUserId();
        possibleSubs = getPossibleSubs();
        newSubscribersList = getNewSubList();
        system.debug('atLeastOneSubscriber: ' + atLeastOneSubscriber);

    }
    
    public map<Id,EntitySubscription> getSubsByUserId(){
        map<Id,EntitySubscription> newMap = new Map<Id,EntitySubscription>();
        subbedUserIds = new set<Id>();
        for(EntitySubscription s : [select Id, SubscriberId, Subscriber.Name, Subscriber.Email, Subscriber.Contact.Account.Name from EntitySubscription where ParentId = :parentCase.Id]){
            newMap.put(s.SubscriberId, s);
            subbedUserIds.add(s.SubscriberId);
        }
        return newMap;
    }

    public List<SelectOption> getNewSubList()
    {
        list<SelectOption> newList = new list<SelectOption>();
        for(Id id : possibleSubs.keySet()){
            if(!subbedUserIds.contains(id)){
                newList.add(new SelectOption(id,possibleSubs.get(id).Name));
                system.debug('Select option: ' + possibleSubs.get(id).Name);
            }
        }
        if(newList.size() > 0) 
        {
            userId = newList[0].getValue();
            atLeastOneSubscriber = true;
        }
        else
        {
            atLeastOneSubscriber = false;
        }
        return newList;
    }
    public map<Id,User> getPossibleSubs(){
        map<Id,Contact> contactMap = new map<Id,Contact>();
        map<Id,User> userMap = new map<Id,User>();
        Case caze = [select Id, AccountId, Account_Origin__c from Case where Id = :parentCase.Id];
        Id accountId = caze.AccountId;
        for(Contact c : [select Id, Name from Contact where AccountId = :accountId ]){
            contactMap.put(c.Id,c);
        }
        
        system.debug('accountId  '+accountId );
        system.debug('contactMap '+contactMap);
        for(User u : [select Id, Name, Email from User where IsActive = true and ContactId IN :contactMap.keySet() order by Name  Limit 1000 ]){
            userMap.put(u.Id, u);
        }
         system.debug('userMap    '+userMap);
        
        return userMap;
    }
    
    public void addStakeholder(){
        
        EntitySubscription newSub = new EntitySubscription();
        newSub.ParentId = parentCase.Id;
        newSub.SubscriberId = userId;

        if(userId == null)
        {
            return;
        }

        
        if(Network.getNetworkId() == null) {
            system.debug('Extracting networkid from user');
            //User currentUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where Id = :UserInfo.getUserId() LIMIT 1];
            User currentUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where Id = :userId LIMIT 1];
            newSub.NetworkId = (currentUser != null && currentUser.NetworkMemberUsers != null  
                                         && (!currentUser.NetworkMemberUsers.isEmpty()) && currentUser.NetworkMemberUsers[0].NetworkId != null )
                ? currentUser.NetworkMemberUsers[0].NetworkId : Network.getNetworkId();
        } else {
            system.debug('Extracting networkid from Network.getNetworkId');
            newSub.NetworkId = Network.getNetworkId();
        }

        system.debug('newSub.ParentId: ' + newSub.ParentId);
        system.debug('newSub.SubscriberId: ' + newSub.SubscriberId);
        system.debug('newSub.NetworkId: ' + newSub.NetworkId);
        
        insert newSub;
        
        //subbedUserIds.add(userId);
        updateState();
    }
    
    public void removeStakeholder(){
        //subId = ApexPages.currentPage().getParameters().get('subId');
        EntitySubscription subToDel = new EntitySubscription(Id = subId);
        delete subToDel;
        //if(subbedUserIds.Contains(subToDel.Id))
            //subbedUserIds.remove(subToDel.Id);
        updateState();
    }

}