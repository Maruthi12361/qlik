/**
* Change log: 
*
* 2018-10-02    ext_vos CHG0034667: update test for Attachments.
* 2019-05-05    AIN IT-1597 Updated for Support Portal Redesign
*/
@isTest
private class QS_AttachmentListControllerTest {
    
    private static Id recTypeId;
    
    // Unit Test for QS_AttachmentList Component and QS_AttachmentListController Class that is being used on QS_CaseDetail visualforce page
    
    static testMethod void test_QS_AttachmentList() {

        QTTestUtils.GlobalSetUp();
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_Thomas', 'test_Jones', 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        
        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        
        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
            
        insert caseObj;
        
        // Insert the feeditem with an attachment related to the case
        Test.startTest();
        
        // Test the methods without inserting any attachment/feed item
        QS_AttachmentListController attachmentController = new QS_AttachmentListController();
        
        attachmentController.contentDocumentParentId = caseObj.Id;
        attachmentController.getFeedItemList();
        attachmentController.getAttachmentList();
        // As there isn't any contentdocument related to the feed so the list would be null
        System.assert(attachmentController.getFeedItemList() == null || attachmentController.getFeedItemList().isEmpty());
        System.assertEquals(attachmentController.showException, false);
        
        FeedItem feedItem = TestDataFactory.createFeedItem('Test Feed Item', caseObj.Id);
        feedItem.Visibility = 'AllUsers';
        feedItem.Type = 'ContentPost';
        feedItem.Title = 'test Title';
        
        
        //Insert ContentVersion
        Blob blobTest1 = blob.valueOf('test');
        ContentVersion cv = new ContentVersion( PathOnClient = '/temp/temp.xls', VersionData = blobTest1, Origin = 'H', Title = 'Test Temp', Description = 'Test Description');
        insert cv;
        
        //refer the content version when inserting the feeditem
        feedItem.RelatedRecordId = cv.Id;
        insert feedItem;

         //Attachment
        Attachment attach = new Attachment();      
        attach.Name = 'QS Test Attachment';
        Blob bodyBlob = Blob.valueOf('QS Test Attachment Body');
        attach.Body = bodyBlob;
        attach.ParentId = caseObj.id;
        insert attach;
        
        attachmentController = new QS_AttachmentListController();
        attachmentController.contentDocumentParentId = caseObj.Id;
        
        // Assertions to test the values in the list
        System.assert(attachmentController.getFeedItemList() != null && attachmentController.getFeedItemList().size() == 1);
        System.assert(attachmentController.getAttachmentFiles() != null && attachmentController.getAttachmentFiles().size() == 1);
        System.assert(attachmentController.getAttachmentList() != null && attachmentController.getAttachmentList().size() == 2);        
           
        Test.stopTest();
        
    }
    // Get Record Type Id of the case based on record type name
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if (recTypeId != null) {
            return recTypeId;
        } else {                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }

}