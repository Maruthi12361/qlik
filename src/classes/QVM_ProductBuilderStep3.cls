/*
@author: Anthony Victorio, Model Metrics
@date: 02/14/2012
@description: Controller for the QVM "product builder step 3" page

2015-09-29  |  BAD  |  CR# 57108 Changed Pageref in saveProduct() and created previewPage()
2017-09-27  |  CCE  |  CHG0032112 - adding new field to page (Product_Acquisition_Visibility__c)
*/
public without sharing class QVM_ProductBuilderStep3 {
    
    public QVM_Product_Data__c product {get;set;}
    public Id pId {get;set;}
    public String productMethod {get;set;}
    
    public String bucketName {get;set;}
    public string AWSAccessKeyId {get;set;}
    public string AWSSecretKeyId {get;set;}
    public String fileFolder {get;set;}
    public String fileAccess {get;set;}
    
    public String formattedexpire {get;set;}
    
    public QVM_Product_Data__c getProduct() {
        return [select Id, Referral_Link__c, Product_Download_Link__c, Product_Method__c,
            Image_1__c, Image_2__c, Image_3__c, Image_4__c, Image_5__c, Magento_Product_Id__c, Magento_Product_URL__c,
            Price__c, Weight__c, Product_Acquisition_Visibility__c  from QVM_Product_Data__c where Id = :pId];
    }
    
    public QVM_ProductBuilderStep3() {
        pId = ApexPages.currentPage().getParameters().get('id');
        
        product = getProduct();
        
        //S3 Related intializations
        
        QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();

        bucketName = settings.S3_Bucket__c;
        
        AWSAccessKeyId = settings.S3_Key__c;
        AWSSecretKeyId = settings.S3_Secret__c;
        
        bucketName = settings.S3_Bucket__c;
        fileAccess = 'public-read';
        
        fileFolder = 'draft/' + pId + '/';
        
        formattedexpire = expire.formatGmt('yyyy-MM-dd') + 
        'T'+expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';
        
    }
    
    @RemoteAction
    public static void setupFileName(String fName, String productId) {
        QVM_Product_Data__c prod = [select Id, Product_Download_Link__c from QVM_Product_Data__c where Id = :productId];
        
        QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
        
        String fFolder = 'draft/' + productId + '/';
        
        prod.Product_Download_Link__c = 'http://' + settings.S3_Bucket__c + '.s3.amazonaws.com/' 
        + fFolder + fName;
        
        update prod;
    }
    
    Datetime expire = system.now().addDays(2);
     
    public String getPolicyJSON() {
        return '{"expiration":"' + formattedexpire + '",' +
        '"conditions":[' + 
            '{"bucket":"'+bucketName + '"},' +
            '{"acl":"' + fileAccess + '"},' +
            '["starts-with","$key",""],' +
            '{"success_action_status":"201"},' +
            '["starts-with","$name",""],' +
            '["starts-with","$Filename",""]' +
        ']}';
    }
    
    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(getPolicyJSON()));
    }
    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(getPolicyJSON())));        
    }
    
    private String make_sig(string canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(AWSSecretKeyId));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer), blob.valueof(AWSSecretKeyId)); 
        macUrl = EncodingUtil.base64Encode(mac);                
        return macUrl;
    }
    
    public list<SelectOption> getProductMethodOptions() {
        return QVM.getPickListAsSelectOptions(QVM_Product_Data__c.Product_Method__c);
    }
    
    public void alternateUploadFile() {
        product.Product_Method__c = productMethod;
        product.Referral_Link__c = null;
        update product;
        product = getProduct();
    }
    
    public void removeProductFile() {
        product.Product_Download_Link__c = null;
        product.Product_Method__c = null;
        update product;
        product = getProduct();
    }
    
    public PageReference removeProductReferral() {
        product.Referral_Link__c = null;
        product.Product_Method__c = null;
        update product;
        
        PageReference p = Page.qvm_productbuilderstep3;
        p.getParameters().put('Id', product.Id);
        p.setRedirect(true); //causes page to reload in order to avoid DOM issues with file uploader
        return p;
    }
    
    public PageReference quickSave() {
        try{
            if(product.Referral_Link__c == null) {
                product.Referral_Link__c = null; //nulling so that VF render logic is not disrupted
                product.Product_Method__c = productMethod;
                throw new QVM.newException(Label.QVM_PB_Step_3_Referral_Missing);
                //return null;
            }
            if(product.Referral_Link__c.contains('http://') || product.Referral_Link__c.contains('https://')) {
                product.Product_Method__c = productMethod;
                product.Product_Download_Link__c = null;
                update product;
                
                PageReference p = Page.qvm_productbuilderstep3;
                p.getParameters().put('Id', product.Id);
                p.setRedirect(true); //causes page to reload in order to avoid DOM issues with file uploader
                return p;
            } else {
                product.Referral_Link__c = null; //nulling so that VF render logic is not disrupted
                product.Product_Method__c = productMethod;
                throw new QVM.newException(Label.QVM_PB_Step_3_Referral_Validation);
                //return null;
            }
        } catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    public PageReference saveProduct() {
        
        try{
            update product;
            PageReference next;

            next = Page.QVM_ProductBuilderStep3;
            return next;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
        
    }

    public PageReference previewPage() {
        try{
            update product;
            PageReference preview;
            preview = Page.QVM_ProductPreview;
            preview.getParameters().put('Id', product.Id);
            return preview;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }    
    
    public PageReference backPage() {
        try{
            update product;
            PageReference back;
            back = Page.QVM_ProductBuilderStep2;
            back.getParameters().put('Id', product.Id);
            return back;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }
    
    public PageReference nextPage() {
        try{
            if(product.Referral_Link__c == null && product.Product_Download_Link__c == null) {
               throw new QVM.newException(Label.QVM_PB_Step_3_Exception);
            }
            //product.Product_Method__c = productMethod;
            update product;
            PageReference next;
            next = Page.QVM_ProductBuilderStep4;
            next.getParameters().put('Id', product.Id);
            return next;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
        
    }

}