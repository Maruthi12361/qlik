/*********************************************
TEST_Account_PartnerRecordUpdate 
Description: Test calls for Account_PartnerRecordudate Trigger
             to update Partner Account name field which enable
             TriggerPortalControl on contacts.
Note: Please check if the Partner Name in CPQ for the contact is updated.
     

Log History:
2013-07-25    KMH    Initial Development
29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta
**********************************************/
@isTest
private class TEST_Account_PartnerRecordUpdate {
	
	 static testMethod void Test_Account_PartnerAccountName() {
	 
	   //QlikTech_Company__c Q = [SELECT Id, QlikTech_Company_Name__c FROM QlikTech_Company__c WHERE Name = 'GBR'];
        
        System.debug('Start of Test_Account_PartnerAccountName:');	
        
		Account TestAccount = new Account (
		Name= 'KMH - Test ',
		//Billing_Country_Code__c = Q.Id, 
		Navision_Customer_Number__c = '12345',
		//QlikTech_Company__c = Q.QlikTech_Company_Name__c
		RecordTypeId = '01220000000DOFz', //Partner Account
		QlikTech_Company__c = 'QlikTech Inc'
		);	

		insert TestAccount;
		
		Contact TestContact = new Contact (
                AccountId = TestAccount.Id,
                //OwnerId = UserInfo.getUserId(),
                LastName = 'HaagenTest',
                FirstName = 'MartinT',
                Email = 'mhg@qlikview.testcom',
                Allow_Partner_Portal_Access__c = true
            );
        Contact TestContact2 = new Contact (
                AccountId = TestAccount.Id,
                //OwnerId = UserInfo.getUserId(),
                LastName = 'Wilson-HemingwayTest',
                FirstName = 'DarylT',
                Email = 'dwy@qlikview.testcom',
                Is_NetExam_user__c = true
            );
            
        List<Contact> Contacts = new List<Contact>();
        Contacts.Add(TestContact); 
        Contacts.Add(TestContact2);      
        insert Contacts;
        
        test.startTest();
        
		TestAccount = [select Name, Navision_Customer_Number__c from Account where Id = :TestAccount.Id LIMIT 1];
		
		String OrgAccountName = TestAccount.Name;
		
		System.debug('Original Acc Name:'+ TestAccount.Name);
		
		TestAccount.Name = 'Kmh - test 12'; //Update Accont Name

		Semaphores.CustomAccountTriggerBeforeUpdate = false;		

        update TestAccount;
        
        TestContact.TriggerPortalControl__c =true;
		TestContact2.TriggerPortalControl__c = true;
		
		update Contacts;
		
        //TestAccount = [select Name, Navision_Customer_Number__c from Account where Id = :TestAccount.Id LIMIT 1];
        Contact testcontact1;
         testcontact1= [select Id,Contact.Account.Name,Name from Contact where AccountId = :TestAccount.Id LIMIT 1]; 
        
		System.debug('Modified Acc Name:'+ testcontact1.Account.Name);	
		
		System.assertEquals(TestAccount.Name, testcontact1.Account.Name);
		
		test.stopTest();			
	 	
	 	System.debug('End of Test_Account_PartnerAccountName:');	
	 }
	  	
}