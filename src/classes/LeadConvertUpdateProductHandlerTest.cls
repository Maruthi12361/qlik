/***************************************************
Class: LeadConvertUpdateProductHandlerTest
Object: Lead
Description: Test class for LeadConvertUpdateProductHandler.cls
Change Log:
 20190724    CCE Initial development
 20190729	 DKF Added assert for Product Trial Contact Account
******************************************************/
@isTest
private class LeadConvertUpdateProductHandlerTest {

	static testMethod void UpdateProductTable() {
        LeadConvertUpdateProductHandler lcuph = new LeadConvertUpdateProductHandler();
		string statusToUse = 'Lead - Converted';

        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany1', 'GBP', 'United Kingdom');

        System.RunAs(mockSysAdmin) {
            //Create a test lead
            Lead l = new Lead(LastName='LastTest1',
                                            FirstName='FirstTest1',
                                            Country='United Kingdom',
                                            Email='asd@asd.com',
                                            Segment__c = 'Enterprise - Target',
                                            Country_Code__c = qtComp.Id,
                                            Company ='Testing');
            insert l;
            system.assert(l.Id != null);
            Semaphores.TriggerHasRun('LeadConvertUpdateProductHandler', 1);

            //Create Product Trial and Product User records and add the lead to them
            Product_Trial__c pt = new Product_Trial__c(
                Lead__c = l.Id, 
                Product__c = 'Qlik Sense Team', 
                Trial_License_Key__c = '1234512345',
                Trial_Status__c = 'Started',
                Start_Date__c = Date.today(),
                End_Date__c = Date.today().addDays(10)
            );
            insert pt;
            system.assert(pt.Id != null);
            
            Product_User__c pu = new Product_User__c(
                Lead__c = l.Id, 
                Status__c = 'Active', 
                User_Role__c = 'Admin'
            );
            insert pu;
            system.assert(pu.Id != null);

            Test.startTest();
            //Convert the lead
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
            Semaphores.LeadTriggerHandlerAfterUpdate = false;
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.Id);
            lc.setConvertedStatus('Lead - Converted');
            lc.setDoNotCreateOpportunity(true);


            Database.LeadConvertResult lcr = Database.convertLead(lc);
            system.assert(lcr.isSuccess());

            System.debug('LeadConvertUpdateProductHandlerTest: lcr.getContactId() = ' + lcr.getContactId());
            Id contactId = lcr.getContactId();

            //check that the Contact Id has been added to the Product Trial record and the Lead Id has been removed
            pt = [Select Id, Lead__c, Contact__c, Contact__r.AccountId, Contact_Account__c from Product_Trial__c where Id = :pt.Id];
            System.debug('LeadConvertUpdateProductHandlerTest: pt = ' + pt);
            system.assertEquals(contactId, pt.Contact__c);
            system.assertEquals(null, pt.Lead__c);
            system.assertEquals(pt.Contact__r.AccountId, pt.Contact_Account__c);

            //check that the Contact Id has been added to the Product User record and the Lead Id has been removed
            pu = [Select Id, Lead__c, Contact__c from Product_User__c where Id = :pu.Id];
            System.debug('LeadConvertUpdateProductHandlerTest: pu = ' + pu);
            system.assertEquals(contactId, pu.Contact__c);
            system.assertEquals(null, pu.Lead__c);
            Test.stopTest();
        }
	}
}