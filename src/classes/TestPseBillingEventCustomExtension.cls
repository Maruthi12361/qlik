@isTest
private class TestPseBillingEventCustomExtension {

    static testMethod void testOneSelectedEvent() {
        Test.setCurrentPage(Page.Pse_BillingEventExport);

        // Setup - at least 1 B.E.
        pse__Billing_Event__c billingEvent = new pse__Billing_Event__c();
        billingEvent.pse__Is_Released__c = true;
        insert billingEvent;

        Test.startTest();

        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<pse__Billing_Event__c>());
        PseBillingEventsCustomExtension exportCon = new PseBillingEventsCustomExtension(ssc);

        // At least 1 B.E., no selections
        System.assert(exportCon.eventList != null && exportCon.eventList.size() > 0);
        System.assert(exportCon.eventRecords != null && exportCon.eventRecords.getResultSize() > 0);
        System.assert(exportCon.selectedEvents != null && exportCon.selectedEvents.size() == 0);
        System.assert(exportCon.selectedEventItems != null && exportCon.selectedEventItems.size() == 0);

        // Select one
        for(PseBillingEventsCustomExtension.WrapperBillingEvent wrappedEvent : exportCon.eventList) {
            if (wrappedEvent.event.Id == billingEvent.Id) {
                wrappedEvent.selected = true;
                break;
            }
        }
        exportCon.preview();

        System.assert(exportCon.eventList != null && exportCon.eventList.size() > 0);
        System.assert(exportCon.eventRecords != null && exportCon.eventRecords.getResultSize() > 0);
        System.assert(exportCon.selectedEvents != null && exportCon.selectedEvents.size() == 1);
        System.assert(exportCon.selectedEventItems != null && exportCon.selectedEventItems.size() == 0);

        exportCon.export();
        for (pse__Billing_Event__c selectedEvent : exportCon.selectedEvents) {
            System.assert(selectedEvent.Export_Date__c != null);
        }
        Test.stopTest();
    }
    
    static testMethod void testMultipleSelectedEvents() {
        Integer numberOfEvents = 200;
        Test.setCurrentPage(Page.Pse_BillingEventExport);

        // Setup - at least numberOfEvents B.E.
        List<pse__Billing_Event__c> billingEvents = new List<pse__Billing_Event__c>();
        for (Integer i = 0; i < numberOfEvents; i++) {
            pse__Billing_Event__c billingEvent = new pse__Billing_Event__c();
            billingEvent.pse__Is_Released__c = true;
            billingEvents.add(billingEvent);
        }
        insert billingEvents;

        Test.startTest();

        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<pse__Billing_Event__c>());
        PseBillingEventsCustomExtension exportCon = new PseBillingEventsCustomExtension(ssc);

        // At least numberOfEvents B.E., no selections
        System.assert(exportCon.eventRecords != null && exportCon.eventRecords.getResultSize() >= numberOfEvents);
        System.assert(exportCon.selectedEvents != null && exportCon.selectedEvents.size() == 0);
        System.assert(exportCon.selectedEventItems != null && exportCon.selectedEventItems.size() == 0);

        // Select all
        for(PseBillingEventsCustomExtension.WrapperBillingEvent wrappedEvent : exportCon.eventList) {
            wrappedEvent.selected = true;
        }
        exportCon.preview();
        //System.debug('### selectedEvents[' + exportCon.selectedEvents.size() + ']: ' + exportCon.selectedEvents);

        System.assert(exportCon.eventRecords != null && exportCon.eventRecords.getResultSize() >= numberOfEvents);
        System.assert(exportCon.selectedEvents != null && exportCon.selectedEvents.size() >= numberOfEvents);

        exportCon.export();
        for (pse__Billing_Event__c selectedEvent : exportCon.selectedEvents) {
            System.assert(selectedEvent.Export_Date__c != null);
        }
        Test.stopTest();
    }

    static testMethod void testExportLabel() {
        // Setup - at least 1 B.E.
        pse__Billing_Event__c billingEvent = new pse__Billing_Event__c();
        billingEvent.pse__Is_Released__c = true;
        insert billingEvent;

        Test.startTest();

        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<pse__Billing_Event__c>());
        PseBillingEventsCustomExtension exportCon = new PseBillingEventsCustomExtension(ssc);

        exportCon.exportEvent.Export_Label__c = 'Test Export Label';

        // Select one
        for(PseBillingEventsCustomExtension.WrapperBillingEvent wrappedEvent : exportCon.eventList) {
            if (wrappedEvent.event.Id == billingEvent.Id) {
                wrappedEvent.selected = true;
                break;
            }
        }
        exportCon.preview();

        System.assert(exportCon.selectedEvents != null && exportCon.selectedEvents.size() == 1);

        exportCon.export();

        for (pse__Billing_Event__c selectedEvent : exportCon.selectedEvents) {
            System.assert(selectedEvent.Export_Date__c != null);
            System.assert(selectedEvent.Export_Label__c == 'Test Export Label');
        }

        Test.stopTest();
    }
}