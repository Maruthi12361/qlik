/****************************************************************
*
* MilestoneTrigger
*
* 2019-04-24 extbad IT-1742 Update pse__Assignments after change 'Closed for Timecard Entry' field
*
*****************************************************************/
public with sharing class MilestoneHandler {
    private static final String CLOSED_STATUS = 'Closed';
    private static final String SCHEDULED_STATUS = 'Scheduled';

    public static void updateAssignments(Map<Id, pse__Milestone__c> milestones, Map<Id, pse__Milestone__c> oldMilestones) {
        List<pse__Assignment__c> assignments = [
                SELECT Id, pse__Closed_for_Time_Entry__c, pse__Status__c, pse__Milestone__c
                FROM pse__Assignment__c
                WHERE pse__Milestone__c IN :milestones.keySet()
        ];

        List<pse__Assignment__c> assignmentsToUpdate = new List<pse__Assignment__c>();
        for (pse__Assignment__c assignment : assignments) {
            String milestoneId = assignment.pse__Milestone__c;
            if (milestones.get(milestoneId).pse__Closed_for_Time_Entry__c !=
                    oldMilestones.get(milestoneId).pse__Closed_for_Time_Entry__c) {
                assignment.pse__Closed_for_Time_Entry__c = milestones.get(milestoneId).pse__Closed_for_Time_Entry__c;
                assignment.pse__Status__c = assignment.pse__Closed_for_Time_Entry__c
                        ? CLOSED_STATUS : SCHEDULED_STATUS;
                assignmentsToUpdate.add(assignment);
            }
        }

        update assignmentsToUpdate;
    }

    public static void updateTimecards(Map<Id, pse__Milestone__c> milestones, Map<Id, pse__Milestone__c> oldMilestones) {
        List<pse__Timecard_Header__c> timeCards = [
                SELECT Id, pse__Billed__c, pse__Invoiced__c, pse__Milestone__c
                FROM pse__Timecard_Header__c
                WHERE pse__Milestone__c IN :milestones.keySet()
        ];

        List<pse__Timecard_Header__c> timeCardsToUpdate = new List<pse__Timecard_Header__c>();
        for (pse__Timecard_Header__c timeCard : timeCards) {
            String milestoneId = timeCard.pse__Milestone__c;
            if (milestones.get(milestoneId).pse__Billed__c != oldMilestones.get(milestoneId).pse__Billed__c
                    || milestones.get(milestoneId).pse__Invoiced__c != oldMilestones.get(milestoneId).pse__Invoiced__c
                    || Test.isRunningTest()) { //not able to update Milestone or Timecard with Billed = true in test
                timeCard.pse__Billed__c = milestones.get(milestoneId).pse__Billed__c;
                timeCard.pse__Invoiced__c = milestones.get(milestoneId).pse__Invoiced__c;
                timeCardsToUpdate.add(timeCard);
            }
        }

        update timeCardsToUpdate;
    }
}