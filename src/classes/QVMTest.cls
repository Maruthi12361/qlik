/*
@author: Anthony Victorio, Model Metrics
@date: 02/06/2012
@description: Houses methods used for creating test data
2017-02-01  |  CCE  | CR# 103173 - updates tests
2018-07-24  CCE CHG0034349 Jira BMW-912 - Update custom setting for Increase MagentoCategoryMap for extra versions
2019-03-05  AIN Moved custom settings to QuoteTestHelper
*/
public class QVMTest {
    
    public static Id getRecordType(String sObjectType, String recordTypeName) {
        return [select Id from RecordType where sObjectType = :sObjectType and 
        Name = :recordTypeName limit 1].Id;
    }
    
    public static Id getProfileId(String profileName) {
        return [select Id from Profile where Name = :profileName].Id;
    }
    
    public static User newUser(String profileId, String contactId) {
        User u = new User();
        u.ProfileId = profileId;
        u.ContactId = contactId;
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'America/Mexico_City';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'en_US';
        return u;
    }
    
    public static Account newAccount(String recordTypeId, String accountName) {
        Account a = new Account();
        a.RecordTypeId = recordTypeId;
        a.Name = accountName;
        return a;
    }
    
    public static Contact newContact(String recordTypeId, String accountId) {
        Contact c = new Contact();
        c.RecordTypeId = recordTypeId;
        c.AccountId = accountId;
        c.FirstName = 'FirstName';
        c.LastName = 'LastName';
        return c;
    }
    
    public static QVM_Partner_Data__c newPartnerData(String accountId, String contactId) {
        QVM_Partner_Data__c p = new QVM_Partner_Data__c();
        p.Partner_Account__c = accountId;
        p.Contact__c = contactId;
        p.Partner_Display_Name__c = 'Test Partner Name XYZ123!@#$';
        return p;
    }
    
    public static QVM_Product_Data__c newProductData(string partnerDataId) {
        QVM_Product_Data__c p = new QVM_Product_Data__c();
        p.QVM_Partner__c = partnerDataId;
        p.Product_Name__c = 'Test Product XYZ123!@#$';
        return p;
    }
    
    public static QVM_Customer_Products__c newCustomerProduct(String contactId, 
    String leadId, String partnerDataId, String productId) {
        QVM_Customer_Products__c c = new QVM_Customer_Products__c();
        c.Contact__c = contactId;
        c.Lead__c = leadId;
        c.QVM_Partner_Data__c = partnerDataId;
        c.QVM_Product__c = productId;
        return c;
    }
    
}