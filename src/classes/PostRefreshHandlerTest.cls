/*
* File PostRefreshHandlerTest
    * @description : Unit test for PostRefreshHandler
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       05.02.2018   Pramod Kumar V      Created Class
    2       21.02.2019   ext_bjd             Excluded ModifyInActiveUsersTest to fix Too many query rows
    3       11.06.2019   ext_bjd             Added fix to avoid System.LimitException: Too many query rows: 50001 from
    *                                        CustomUserTriggerHandler.processUserAssignment
*/
@isTest
private class PostRefreshHandlerTest {

    public static void test_PostRefreshAppHandler() {
        Profile p = [select id from profile where name = 'System Administrator'];
        User mockSysAdmin = new User(alias = 'standt', email = 'standarduser@testorg.com',
                emailencodingkey = 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US',
                localesidkey = 'en_US', profileid = p.Id,
                timezonesidkey = 'America/Los_Angeles', username = 'standarduser@hourserr.com.qa');
        insert mockSysAdmin ;

        System.RunAs(mockSysAdmin) {
            QTCustomSettings__c Qtsetting = QTCustomSettings__c.getOrgDefaults();
            Qtsetting.Name = 'test';
            insert Qtsetting ;

            Steelbrick_Settings__c SBsetting = Steelbrick_Settings__c.getOrgDefaults();
            SBsetting.Name = 'test';
            insert SBsetting;

            QVM_Settings__c qvmSetting = QVM_Settings__c.getOrgDefaults();
            SBsetting.Name = 'test';
            insert qvmSetting;

            QS_Partner_Portal_Urls__c qsSetting = QS_Partner_Portal_Urls__c.getOrgDefaults();
            qsSetting.Name = 'test';
            insert qsSetting;

            //disable CustomUserTriggerHandler.processUserAssignment
            Q2CWSettings__c customSettings = Q2CWSettings__c.getInstance(UserInfo.getOrganizationId());
            if (customSettings.Disable_User_trigger__c == false) {
                customSettings.Disable_User_trigger__c = true;
                upsert customSettings;
            }

            Qtsetting.ULC_Base_URL__c = 'test';
            Qtsetting.BoomiBaseURL__c = 'test';
            SBsetting.BoomiBaseURL__c = 'test';
            SBsetting.CMInstanceUrl__c = 'test';
            SBsetting.SpringAccountPrefix__c = 'test';
            qvmSetting.QlikMarket_Website__c = 'test';

            QSsetting.Support_Portal_CSS_Base__c = 'test';
            QSsetting.Support_Portal_index_allow_options__c = 'test';
            QSsetting.Support_Portal_Live_Agent_API_Endpoint__c = 'test';
            QSsetting.Support_Portal_Login_Page_Url__c = 'test';
            QSsetting.Support_Portal_Login_Url__c = 'test';
            QSsetting.Support_Portal_Logout_Page_Url__c = 'test';
            QSsetting.Support_Portal_Url__c = 'test';
            QSsetting.Support_Portal_Url_Base__c = 'test';

            update Qtsetting ;
            update SBsetting;
            update qvmSetting;
            update qsSetting;
        }
    }

    static testMethod void test_PostRefreshCustomSetting() {
        if (Test.isRunningTest()) {
            test_PostRefreshAppHandler();
            PostRefreshHandler.PostRefreshCustomSettingHelper(null);
        }
    }
}
