/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_PersonaControllerTest {
    private static Id networkId;
    private static Id recTypeId;

    private static Case surveyToDelete;
    private static Attachment attachment;

    @isTest static void testHomepage() {

        User testUser = InsertTestData('Application Developer;Server Administrator',2);
        PageReference pageRef = Page.QS_Home_Page;
        Test.setCurrentPage(pageRef);

        test.startTest();
        system.runas(testUser)
        {
            QS_PersonaController controller = new QS_PersonaController();
            
            controller.setMyEmailOption();
            system.debug('controller.getUserPersona(): ' + controller.getUserPersona());
            system.assert(controller.getUserPersona() == 'Application Developer;Server Administrator');

            //controller.SaveBut();
            string username = controller.UserName;

            attachment = new Attachment();
            attachment.Name = 'Attach';
            attachment.body = Blob.valueof('Some random String');
            controller.ProfilePicture = attachment;

            controller.UploadUserProfilePic();
            QS_PersonaController.BlobToHex(Blob.valueof('Some random String'));
            controller.getUserprofilePic();

            Blob b = Blob.valueOf('Test Data');  
            attachment.Body = b;
            controller.ProfilePicture = attachment;
            controller.SaveBut();
        }
        test.stopTest();
    }
    private static User InsertTestData(string persona, integer numberOfAnnouncements)
    {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = persona;
        testContact.LeadSource = 'leadSource';
        insert testContact;

        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        return communityUser;
    }
    
    private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
                User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];  
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}