/**
*
* extbad    2019-09-17 IT-2088 Send an email to the to Resource Request Owner
*/
@IsTest
private class OpportunityClosedHandlerTest {

    static testMethod void testSendEmail() {
        QTTestUtils.GlobalSetUp();
        String datenow = Datetime.now().format();

        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        Id AccRecordTypeId_PartnerAccount = '01220000000DOFz';
        Account accP = new Account(
                Name = 'TestP' + datenow,
                OwnerId = UserInfo.getUserId(),
                RecordTypeId = AccRecordTypeId_PartnerAccount,
                QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                Navision_Customer_Number__c = '12345',
                Navision_Status__c = 'Partner',
                Legal_Approval_Status__c = 'Legal Approval Granted',
                Billing_Country_Code__c = qtComp.Id);
        insert accP;

        Contact PartnerContact = new Contact();
        PartnerContact.FirstName = 'test' + datenow;
        PartnerContact.LastName = 'test' + datenow;
        PartnerContact.accountId = accP.Id;
        insert PartnerContact;

        Opportunity opp = new Opportunity(
                Short_Description__c = 'test',
                Name = 'test' + datenow,
                Type = 'test',
                Revenue_Type__c = 'test',
                CloseDate = Date.today().addDays(5),
                StageName = 'Open',
                AccountId = accP.Id
        );
        insert opp;

        Test.startTest();
        opp.StageName = OpportunityClosedHandler.CLOSED_LOST;
        opp.Primary_reason_lost__c = 'test';
        opp.Bypass_Rules__c = true;
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        update opp;

        Test.stopTest();

        System.assert(OpportunityClosedHandler.emailSent);
    }
}