/**************************************************
*
* Change Log:
* 
* 2016-06-06   CCE Created
*                  CR# 86352 - adding new features and combining TaskSetType.trigger, TaskUpdateTypeField.trigger, TaskLeadFollowUp.trigger and
*                  Task_cant_be_saved_when_Type_is_Select validation rule.
*                  This has been moved into a help class (TaskSetTypeHelper.class) so that we can control the order and reduce the SOQL quereies.
*                  (includes changes for CR# 82611 - https://eu1.salesforce.com/a0CD000000xSrSI Velocify Lead Lifecycle Changes - Removal of 
*                  Follow-Up Accepted, Update logic to allow for multiple calls in same day to count towards call attempts.)
* 2016-09-19   CCE CR# 94227 NVM create new activity type: TK - NVM Activity
* 2016-12-07   CCE CR# 100688 - allow Unresolved Emails to be added to SF from Outlook
* 2017-02-21   CCE CR# 105551 (NVM replacement project) Bucher and Suter create new activity type: TK - B+S Activity
* 2017-03-28   CCE CR# CHG0031099 (NVM replacement project) Removing NVM dependancy 
* 2017-08-14   CCE CR# CHG0031010 Adding extra Velocify statuses to allow Task creation (Velocify: Contacted - Additional Work Required and Velocify: Follow-Up Disqualified and Velocify: Called: Opportunity Identified)
* 2017-10-30   ext_vos CR# CHG0031855 Allow the 'Select' activity type for Gainsight users.
* 2018-03-15   CCE CHG0033386 - Change Type to TK - Call Attempt instead of TK - B+S Activity to increase number of Voicemails counter
* 2018-03-16   CCE CHG0033391 - New Task Type TK - Conversica Activity
* 2018-11-01   CCE CHG0034917 BMW-1077 Fix #Follow-Up Attempt counter not always counting
**************************************************/
public class TaskSetTypeHelper {

    // These variables store Trigger.oldMap and Trigger.newMap
    Map<Id, Task> oldTasks;
    Map<Id, Task> newTasks;
    boolean isInsert;
    boolean isUpdate;
    static boolean PreviouslyCalledAsInsert = false;
    static final String DEBUGPREFIX = 'ProcessTasks: ';
        
    // This is the constructor
    // A map of the old and new records is expected as inputs
    public TaskSetTypeHelper (Map<Id, Task> oldTriggerTasks, Map<Id, Task> newTriggerTasks, boolean trigInsert, boolean trigUpdate) {
        oldTasks = oldTriggerTasks;
        newTasks = newTriggerTasks;
        isInsert = trigInsert;
        isUpdate = trigUpdate;
    }

    public void ProcessTasks() {
        //Commented out for BMW-1077
        System.debug(DEBUGPREFIX + 'PreviouslyCalledAsInsert = ' + PreviouslyCalledAsInsert);
        if ((isUpdate && PreviouslyCalledAsInsert) && (!Test.isRunningTest())) {
            System.debug(DEBUGPREFIX + 'Returning');
            PreviouslyCalledAsInsert = false;
            return;
        } else {
            PreviouslyCalledAsInsert = true;
        }

        //set<Id> IdUserContactworldUsers = new set<Id> {'005D0000003XLE8', '005D0000001qk4i','005D0000004CBhH'}; //ContactWorld User (APAC)//ContactWorld user (US); //User: Contactworld backupUser 
        Id RoleEnterprise = '00ED00000011kJE';                  //Role: Enterprise
        Id RoleServiceDesk = '00ED00000011kIp';                 //Role: Service Desk
        Id RoleRegionalTeamLead = '00ED00000012L75';            //Role: Regional Team Lead
        Id IdRoleProblemManagement = '00E20000000vwci';         //Role: Problem Management
        
        Id IdRTStandardTasks = Task.SObjectType.getDescribe().getRecordTypeInfosByName().get('Standard Tasks').getRecordTypeId();        
        Id IdRTSupportTasks = Task.SObjectType.getDescribe().getRecordTypeInfosByName().get('Support Tasks').getRecordTypeId();        

        List<Task> callLeftVoicemailTasks = new List<Task>();   // collects both call and email tasks
        List<Task> setSameDayDate = new List<Task>();   
            
        Boolean hasGainsightLicense = [SELECT id FROM UserPackageLicense WHERE PackageLicense.NamespacePrefix = 'JBCXM' AND UserId = :UserInfo.getUserId() limit 1].size() > 0;

        //We need the CreatedById field to be filled in - we only get this "after insert" hence we need the query
        List<Task> tNew = [SELECT Id, Owner.UserRoleId, RecordTypeId, CallType, CreatedById, Type, Subject, CallObject, CurrentUserProfile__c, Status, WhoId, cnx__CTIInfo__c FROM Task WHERE Id IN: newTasks.keySet()];
        boolean doIt = false;   //only set this flag true if we need to do a DML update
        
        System.debug(DEBUGPREFIX + 'tNew size =' + tNew.size());
        System.debug(DEBUGPREFIX + 'tNew =' + tNew);

        for (Task t : tNew) {
            System.debug(DEBUGPREFIX + 't.RecordTypeId=' + t.RecordTypeId);
            System.debug(DEBUGPREFIX + 't.CallType=' + t.CallType);
            
            if ((t.RecordTypeId == IdRTStandardTasks || t.RecordTypeId == IdRTSupportTasks) && (t.Type == null || t.Type == 'Select')) {
                if ((t.Owner.UserRoleId == RoleEnterprise) || 
                    (t.Owner.UserRoleId == RoleServiceDesk) || 
                    (t.Owner.UserRoleId == IdRoleProblemManagement) || 
                    (t.Owner.UserRoleId == RoleRegionalTeamLead)) { 
                        t.Type = 'TK - Support Activity';
                        doIt = true;
                } else if (t.cnx__CTIInfo__c != null) { //B+S created
                    if (t.CallType == 'Inbound') {
                        t.Type = 'TK - Call (Inbound)';
                        doIt = true;
                    } else if (t.CallType == 'Outbound') { //CHG0033386
                        //t.Type = 'TK - B+S Activity'; 
                        t.Type = 'TK - Call Attempt'; 
                        doIt = true;
                    }
                } 
            }
             
            //We should only do something if it's the correct record type (Standard Tasks)
            if (Trigger.isInsert && t.RecordTypeId == IdRTStandardTasks)
            {
                System.debug(DEBUGPREFIX + 'Found the subject:' + t.Subject);
                if ((t.Subject.contains('Goal Identification Letter')) || (t.Subject.contains('Kiitos mielenkiintoisesta keskustelusta')))
                {
                    System.debug(DEBUGPREFIX + 'Found a GI Letter');
                    t.Type = 'TK - Email - GI Letter';
                    doIt = true;
                }
                else
                {
                    if (t.Subject.startsWith('Email:'))
                    {
                        System.debug(DEBUGPREFIX + 'Subject has not been recognised but it is an email');
                        if (t.Type == 'Select')
                        {
                            t.Type = 'TK - Email';
                            doIt = true;
                        }
                    }
                    else if (t.Subject.startsWith('Unresolved Email:'))
                    {
                        System.debug(DEBUGPREFIX + 'Subject has not been recognised but it is an unresolved email');
                        if (t.Type == 'Select')
                        {
                            t.Type = 'TK - Email';
                            doIt = true;
                        }
                    }
                    else if (t.Subject.startsWith('Conversica')) //CHG0033391
                    {                    
                        t.Type = 'TK - Conversica Activity';
                        doIt = true;
                    }
                    //CHG0033386
                    //else if (t.Subject == 'Velocify: Called: Left Message')
                    //{                    
                    //    t.Type = 'TK - Call Attempt';
                    //    doIt = true;
                    //}
                    //else if (t.Subject == 'Velocify: Called: No Contact')
                    //{
                    //    t.Type = 'TK - Call Attempt';
                    //    doIt = true;
                    //}
                    //else if (t.Subject == 'Velocify: Contacted - Additional Work Required')
                    //{
                    //    t.Type = 'TK - Call (Outbound)';
                    //    doIt = true;
                    //}
                    //else if (t.Subject == 'Velocify: Follow-Up Disqualified')
                    //{
                    //    t.Type = 'TK - Call (Outbound)';
                    //    doIt = true;
                    //}
                    //else if (t.Subject == 'Velocify: Called: Opportunity Identified')
                    //{
                    //    t.Type = 'TK - Call (Outbound)';
                    //    doIt = true;
                    //}
                    //else if (t.Subject == 'Velocify: Email Sent')
                    //{
                    //    t.Type = 'TK - Email';
                    //    doIt = true;
                    //}
                    else if (t.Subject == 'Registered - Qlik Sense Cloud')
                    {
                        t.Type = 'TK - Web Activity';
                        doIt = true;
                    }
                }
            }
            
            Boolean taskChanged = false;
            if (isUpdate) {
                Task old = oldTasks.get(t.Id);
                taskChanged = (old.Type != t.Type || old.Status != t.Status);
            }
            System.debug(DEBUGPREFIX + 'TaskLeadFollowUp Part3 t = ' + t);
            System.debug(DEBUGPREFIX + 'TaskLeadFollowUp Part3 taskChanged = ' + taskChanged);
            System.debug(DEBUGPREFIX + 'TaskLeadFollowUp Part3 t.Type = ' + t.Type);
            System.debug(DEBUGPREFIX + 'TaskLeadFollowUp Part3 t.Status = ' + t.Status);
            if ((isInsert || taskChanged) && t.Type != null && (t.Type == 'TK - Call Attempt' || t.Type == 'TK - Call (Outbound)') && t.Status == 'Completed') {callLeftVoicemailTasks.add(t); System.debug(DEBUGPREFIX + '=====>adding TK - Call Attempt or TK - Call (Outbound)');} //CCE CR# 35713
            if ((isInsert || taskChanged) && t.Type != null && t.Type.contains('TK - Email') && t.Status == 'Completed') {callLeftVoicemailTasks.add(t); System.debug(DEBUGPREFIX + '=====>adding TK - Email');}      
            if ((isInsert || taskChanged) && t.Type != null && t.Type == 'TK - Call (Inbound)' && t.Status == 'Completed') {setSameDayDate.add(t); System.debug(DEBUGPREFIX + 'setSameDayDate.size() =' + setSameDayDate.size());} 
            

            if (isInsert && t.Type == 'Select' && t.RecordTypeId == IdRTStandardTasks 
                    &&  t.CurrentUserProfile__c != 'Sys admin profile for Marketo User' && string.isBlank(t.CallObject) 
                    && !hasGainsightLicense && UserInfo.getUserId() != '005D0000003Z6RzIAK') //QT Qtmetadata = 005D0000003Z6RzIAK
            {
                Task actualTask = newTasks.get(t.Id);
                actualTask.addError('TaskSetType.Trigger: Activity Type cannot be Select');
            }
        }
        if (doIt) {update tNew;}


        /////////////////////////////////////////////////////////////
        // Increment the Number_of_Follow_Up_Voicemails__c for all contacts and leads related to 'TK - Call Attempt' or 'TK - Call (Outbound)' tasks by 1
        // Increment the Number_of_Follow_Up_Emails__c for all contacts and leads related to 'TK-Email' tasks by 1
        // if at least one field is true
        // CCE 20131203 CR#9755 Added date field code to only allow flag clearance after 4 days worth of attempts
        /////////////////////////////////////////////////////////////
        System.debug(DEBUGPREFIX + 'Part3 callLeftVoicemailTasks.size() = ' + callLeftVoicemailTasks.size());
        
        Map<Id, List<Task>> leadTasks = new Map<Id, List<Task>>();
        for(Task t : callLeftVoicemailTasks) leadTasks.put(t.WhoId, new List<Task>());
        for(Task t : callLeftVoicemailTasks) leadTasks.get(t.WhoId).add(t);                                   
        
        System.debug(DEBUGPREFIX + 'Part3 leadTasks.keySet().size() = ' + leadTasks.keySet().size());
        if (leadTasks.keySet().size() > 0) {
            System.debug(DEBUGPREFIX + 'Part3 leadTasks.size() = ' + leadTasks.size() + ' leadTasks = ' + leadTasks);
            List<Lead> callLeftVoicemailLeads = [SELECT New_Follow_Up_Required__c, Event_Follow_Up_Required__c, Existing_Follow_Up_Required__c, Number_of_Follow_Up_Voicemails__c, Number_of_Follow_Up_Emails__c, DateOfCountUpdate__c, DateOfEmailCountUpdate__c, Email_Follow_Up_Attempted__c, Status FROM Lead WHERE Id IN :leadTasks.keySet()];
            for (Lead l : callLeftVoicemailLeads) {     
                if(l.New_Follow_Up_Required__c || l.Existing_Follow_Up_Required__c || l.Event_Follow_Up_Required__c) {
                    if (l.Number_of_Follow_Up_Voicemails__c == null) {l.Number_of_Follow_Up_Voicemails__c = 0;} //CCE CR# 7146
                    if (l.Number_of_Follow_Up_Emails__c == null) {l.Number_of_Follow_Up_Emails__c = 0;} //MK CR# 23836

                    List<Task> lTasks = new List<Task>();
                    lTasks = leadTasks.get(l.Id); 
                    for(Task lt : lTasks) {    
                        System.debug(DEBUGPREFIX + 'Part3 lt = ' + lt);
                                
                        if (lt.Type.contains('TK - Call Attempt') || lt.Type.contains('TK - Call (Outbound)')) {    //CCE CR# 35713
                            //if (l.DateOfCountUpdate__c != System.today()) { //CCE CR# 9755
                                l.Number_of_Follow_Up_Voicemails__c += 1;
                                l.DateOfCountUpdate__c = System.today();
                            //}
                        }
                        if (lt.Type.contains('TK - Email')) {
                            //if (l.DateOfEmailCountUpdate__c != System.today()) { //MK CR# 23836
                                l.Email_Follow_Up_Attempted__c = true;
                                l.Number_of_Follow_Up_Emails__c += 1;
                                l.DateOfEmailCountUpdate__c = System.today();
                            //} 
                        }
                        //For the moment comment these lines out but it may be that they will need to set the status to Follow-Up Attempt 1,2,3 for users not using Velocify. Ben to check with Velocify how this will work/interfere with Velocify
                        //if ((l.Number_of_Follow_Up_Voicemails__c + l.Number_of_Follow_Up_Emails__c > 0) && l.Status == 'Follow-Up Required') {l.Status = 'Follow-Up Accepted';}
                        System.debug(DEBUGPREFIX + 'Part3 l.Email_Follow_Up_Attempted__c = ' + l.Email_Follow_Up_Attempted__c);                    
                    }
                    System.debug(DEBUGPREFIX + 'Part3 l.Number_of_Follow_Up_Voicemails__c = ' + l.Number_of_Follow_Up_Voicemails__c);
                    System.debug(DEBUGPREFIX + 'Part3 l.Number_of_Follow_Up_Emails__c = ' + l.Number_of_Follow_Up_Emails__c);
                    System.debug(DEBUGPREFIX + 'Part3 l.Email_Follow_Up_Attempted__c = ' + l.Email_Follow_Up_Attempted__c);
                }
            }
            
            List<Contact> callLeftVoicemailContacts = [SELECT Event_Follow_Up_Required__c, Number_of_Follow_Up_Voicemails__c, Number_of_Follow_Up_Emails__c, New_Follow_Up_Required__c, Existing_Follow_Up_Required__c, DateOfCountUpdate__c, DateOfEmailCountUpdate__c, Email_Follow_Up_Attempted__c, Contact_Status__c FROM Contact WHERE Id IN :leadTasks.keySet()];
            for (Contact c : callLeftVoicemailContacts) {       
                if(c.New_Follow_Up_Required__c || c.Existing_Follow_Up_Required__c || c.Event_Follow_Up_Required__c) {
                    if (c.Number_of_Follow_Up_Voicemails__c == null) {c.Number_of_Follow_Up_Voicemails__c = 0;} //CCE CR# 7146
                    if (c.Number_of_Follow_Up_Emails__c == null) {c.Number_of_Follow_Up_Emails__c = 0;} //MK CR# 23836
                    List<Task> lTasks = new List<Task>();
                    lTasks = leadTasks.get(c.Id); 
                    for(Task lt : lTasks) {    
                        System.debug(DEBUGPREFIX + 'Part3 lt = ' + lt);
                                
                        if (lt.Type.contains('TK - Call Attempt') || lt.Type.contains('TK - Call (Outbound)')) { //CCE CR# 35713
                            //if (c.DateOfCountUpdate__c != System.today()) { //CCE CR# 9755
                                c.Number_of_Follow_Up_Voicemails__c += 1;
                                c.DateOfCountUpdate__c = System.today();
                            //}
                        }
                        if (lt.Type.contains('TK - Email')) {
                            //if (c.DateOfEmailCountUpdate__c != System.today()) { //MK CR# 23836
                                c.Email_Follow_Up_Attempted__c = true;
                                c.Number_of_Follow_Up_Emails__c += 1;
                                c.DateOfEmailCountUpdate__c = System.today();
                            //}
                        }
                        //For the moment comment these lines out but it may be that they will need to set the status to Follow-Up Attempt 1,2,3 for users not using Velocify. Ben to check with Velocify how this will work/interfere with Velocify
                        //if ((c.Number_of_Follow_Up_Voicemails__c + c.Number_of_Follow_Up_Emails__c > 0) && c.Contact_Status__c == 'Follow-Up Required') {c.Contact_Status__c = 'Follow-Up Accepted';}
                        System.debug(DEBUGPREFIX + 'Part3 c.Email_Follow_Up_Attempted__c = ' + c.Email_Follow_Up_Attempted__c);                    
                    }
                    System.debug(DEBUGPREFIX + 'Part3 c.Number_of_Follow_Up_Voicemails__c = ' + c.Number_of_Follow_Up_Voicemails__c);
                    System.debug(DEBUGPREFIX + 'Part3 c.Number_of_Follow_Up_Emails__c = ' + c.Number_of_Follow_Up_Emails__c);
                    System.debug(DEBUGPREFIX + 'Part3 c.Email_Follow_Up_Attempted__c = ' + c.Email_Follow_Up_Attempted__c);
                }
            }
            
            System.debug(DEBUGPREFIX + 'Part3 callLeftVoicemailLeads.size() = ' + callLeftVoicemailLeads.size());
            if (callLeftVoicemailLeads.size() > 0) {update callLeftVoicemailLeads;}
            System.debug(DEBUGPREFIX + 'Part3 callLeftVoicemailContacts.size() = ' + callLeftVoicemailContacts.size());
            if (callLeftVoicemailContacts.size() > 0) {update callLeftVoicemailContacts;}
        }


        /////////////////////////////////////////////////////////////
        // If New: Follow-Up Required OR Existing: Follow-Up Required OR Event: Follow-Up Required is true
        //  AND Lead or Contact Status is changed to Lead – In Qualification
        //  AND Activity is Logged and Type is TK - (Call Inbound) in past day
        //  AND Activity Status is completed
        //  Then set the New: Follow-Up Required and Existing: Follow-Up Required and Event: Follow-Up Required to false
        //  From CR# 7146 
        //  CCE after discussion with Ben - Create a date field on Lead and Contact and this part of the trigger 
        //  will populate it if New: Follow-Up Required OR Existing: Follow-Up Required OR Event: Follow-Up Required is true
        //  A workflow (Same Day Clear Of FollowUp or Contact: Same Day Clear Of FollowUp) will then look at the date field
        //  when the Status changes to Lead – In Qualification and if it's the same day will clear the New: Follow-Up Required
        //  and Existing: Follow-Up Required and Event: Follow-Up Required flags.
        /////////////////////////////////////////////////////////////
        Map<Id, List<Task>> leadTasksToTest = new Map<Id, List<Task>>();
        for(Task t : setSameDayDate) leadTasksToTest.put(t.WhoId, new List<Task>());
        for(Task t : setSameDayDate) leadTasksToTest.get(t.WhoId).add(t);                                   
        
        System.debug(DEBUGPREFIX + 'Part4 leadTasksToTest.size() = ' + leadTasksToTest.size());
        if (leadTasksToTest.size() > 0) {
            List<Lead> setClearDateLeads = [SELECT New_Follow_Up_Required__c, Existing_Follow_Up_Required__c, Event_Follow_Up_Required__c, Same_Day_Clear_Of_Follow_Up_Flags__c FROM Lead WHERE Id IN :leadTasksToTest.keySet()];
            for (Lead l : setClearDateLeads) {     
                if(l.New_Follow_Up_Required__c || l.Existing_Follow_Up_Required__c || l.Event_Follow_Up_Required__c) {
                    l.Same_Day_Clear_Of_Follow_Up_Flags__c = Date.today();
                    System.debug(DEBUGPREFIX + 'Part4 Same_Day_Clear_Of_Follow_Up_Flags__c = ' + l.Same_Day_Clear_Of_Follow_Up_Flags__c);            
                }
            }
            
            List<Contact> setClearDateContacts = [SELECT New_Follow_Up_Required__c, Existing_Follow_Up_Required__c, Event_Follow_Up_Required__c, Same_Day_Clear_Of_Follow_Up_Flags__c FROM Contact WHERE Id IN :leadTasksToTest.keySet()];
            for (Contact c : setClearDateContacts) {       
                if(c.New_Follow_Up_Required__c || c.Existing_Follow_Up_Required__c || c.Event_Follow_Up_Required__c) {
                    c.Same_Day_Clear_Of_Follow_Up_Flags__c = Date.today();
                    System.debug(DEBUGPREFIX + 'Part4 Same_Day_Clear_Of_Follow_Up_Flags__c = ' + c.Same_Day_Clear_Of_Follow_Up_Flags__c);
                }
            }
            
            System.debug(DEBUGPREFIX + 'Part4 setClearDateLeads.size() = ' + setClearDateLeads.size());
            if (setClearDateLeads.size() > 0) {update setClearDateLeads;}
            System.debug(DEBUGPREFIX + 'Part4 setClearDateContacts.size() = ' + setClearDateContacts.size());
            if (setClearDateContacts.size() > 0) {update setClearDateContacts;}
        }

    }

}