//*********************************************************/
// Author: Mark Cane&
// Creation date: 20/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efProductWrapper{
   	Product2 p;
   	Integer counter=0;
   	Boolean isSelected= false ;
   	String calculatedDiscountAmount = '';
   	String relatedOppLnItmId = '';
	String relatedOppLnItemStatus = ''; 
	String relatedOppLnItemDesc = '';
	Boolean sameTimeAlert = false;
	Boolean classFull=false;

    public String genericType {get; set;}
    
   	public efProductWrapper(Product2 p) { this.p = p; }
	// Need to fix this for add Class page popup error.   
   	public String getCounter(){
    	return String.valueOf(counter);
   	}

    public String getIndex(){
        return getCounter();
    }
   
   	public String getSerialNumber(){
    	return String.valueOf(counter +1); 
   	}
   	
   	public void setCounter(Integer i){
    	this.counter = i;
    }
    
    public ID getID(){
    	return p.ID;
    }
    
	public Product2 getProduct() { return p; }
    public PricebookEntry[] getPricebookEntries() { return p.PricebookEntries; }
    
    public Boolean getIsSelected(){
    	return isSelected; 
    }
    
    public boolean getIsContestPromoCode(){
    	return p.Contest_Promo_Code__c;
    }
    
    public void setIsSelected(Boolean isSelected){  
    	this.isSelected = isSelected; 
    }
    
    public Boolean getIsClassFull(){
    	if(p.Balance__c>0)
    		return false;
    	else
    		return true; 
    }

    public Boolean getIsClassFullWarning(){
    	Double warningLimit = p.Capacity__c*efConstants.CLASS_FULL_WARNING_LIMIT/100;
    	if(p.Balance__c <= warningLimit && p.Balance__c>0)
    		return true;
    	else
    		return false; 
    }

    public Boolean getSameTimeAlert(){
    	return sameTimeAlert; 
    }
    
    public void setSameTimeAlert(Boolean sameTimeAlert){  
    	this.sameTimeAlert = sameTimeAlert; 
    }
    
    public String getCalculatedDiscountAmount(){
    	if (efUtility.isNull(calculatedDiscountAmount)){
    	   calculatedDiscountAmount = '0';
    	}    	
    	return calculatedDiscountAmount; 
    }
    
    public void setCalculatedDiscountAmount(String am){  
    	this.CalculatedDiscountAmount = am; 
    }
    
    public String getFormatedDiscountAmount(){
     	String amt = calculatedDiscountAmount;
    	if(efUtility.isNull(amt))amt='0';
    	amt = efUtility.formatDecimals(amt);
    	if(amt.indexOf('-')==0)
    		amt = '(' + amt.substring(1,amt.length())+ ')';
    	return amt; 
    }
    
    //added later for VF Calendar
    public Datetime getSDate(){
    	return this.p.startdate__c;
    }
    
    public Datetime getEDate(){
    	return this.p.enddate__c;
    }
    
    public String getFullStartDate(){
    	return efUtility.getFullFormatedDate(p.startdate__c);
    }
    
    public String getStartDate(){
    	return efUtility.getFormatedDate(p.startdate__c);
    }
    
    public String getEndDate(){
    	return efUtility.getFormatedDate(p.enddate__c);
    }
    
    public DateTime getStartDateTime(){
        return p.StartDate__c;
    }
    
    public DateTime getEndDateTime(){
    	return p.EndDate__c;
    }
   
   	public String getStartTime(){
     	DateTime stdt = p.startdate__c ;
     	if(stdt!=null)
     		return stdt.format('MM/dd/yy HH:mm');
    	return '';
  	}
     
    public String getEndTime(){
     	DateTime stdt = p.enddate__c ;
     	if(stdt!=null)
     		return stdt.format('MM/dd/yy HH:mm');
    	return '';
   	}
    
    public String getName(){
    	return p.Name;
    }    
    
    public String getRoom(){
        return efUtility.giveValue(p.Room__r.Name);
    }
    
    public String getRole(){
    	return p.Role__c;
    }  
    
    public String getDescription(){
    	return p.Description;
    }
    
    public String getProductCode(){
    	return p.ProductCode;
    }
    
	public String getClassDates(){
    	return efUtility.getFormatedDate(p.startdate__c) + '-' + efUtility.getFormatedDate(p.enddate__c);
    }
    
    public String getClassTime(){
    	return efUtility.getFormatedTimeWithAM(p.startdate__c) + '-' + efUtility.getFormatedTimeWithAM(p.enddate__c);
    }
    
    public String getClassTimeLower(){
    	return (efUtility.getFormatedTimeWithAM(p.startdate__c) + ' - ' + efUtility.getFormatedTimeWithAM(p.enddate__c)).toLowerCase();
    }
    
    public String getDateForClassGroup(){
        DateTime stdt = p.startdate__c ;
        DateTime eddt = p.enddate__c ;
        String formatedDate = '';
        
        if(stdt!=null && eddt!=null)
            formatedDate = stdt.format('EEEEE, MMMMM d');
        
        if(!stdt.isSameDay(eddt))
            formatedDate = formatedDate + eddt.format(' - EEEEE, MMMMM d');

        return formatedDate;
    }
    
    public String getFormatedDate(){
    	String res = efActivityUtil.getFormatedDate(p.startdate__c, p.enddate__c);
    	
    	if (!p.startdate__c.isSameDay(p.enddate__c))
            return 'Three Day Class ' + res;
        else
            return res;
    }

    public String getFormatedDateSingle(){
    	String res = efActivityUtil.getFormatedDate(p.startdate__c, p.enddate__c);
        return res;
    }
    
    public String getFormatedDateNoText(){
    	return efActivityUtil.getFormatedDate(p.startdate__c, p.enddate__c);
    }
    
    public String getFormatedTime(){
     	return efActivityUtil.getFormatedTime(p.startdate__c, p.enddate__c);
    }
    
    public String getUnitPrice(){    	
    	PricebookEntry[] pbe = this.p.PricebookEntries;
    	if(pbe!=null && pbe.size()>0){
    		return efUtility.formatDecimals(String.valueOf(pbe[0].UnitPrice));
    	}    	
    	return '0.00';
    }
    
    public String getRelatedOppLnItemId(){
    	return  this.relatedOppLnItmId;
   	}
   	
   	public void setRelatedOppLnItemId(String oppLnItmId){
    	this.relatedOppLnItmId = oppLnItmId;
   	}

   	public String getRelatedOppLnItemStatus(){
    	return  this.relatedOppLnItemStatus;
   	}   	

   	public void setRelatedOppLnItemStatus(String oppLnItemStatus){
    	this.relatedOppLnItemStatus = oppLnItemStatus;
   	}
   	
   	public String getRelatedOppLnItemDesc(){
    	return  this.relatedOppLnItemDesc;
   	}   	

   	public void setRelatedOppLnItemDesc(String oppLnItemDesc){
    	this.relatedOppLnItemDesc = oppLnItemDesc;
   	}   	
   	
    public String getProductFamily(){
        return p.Family;
    }   
}