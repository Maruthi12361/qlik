/**********************************************
* Initial developemnt done by Unknown person
* Change Log:
*	20140627 AIN	Adding @isTest
*   20151030 RDZ	Adding mock EmailMessage 
***********************************************/
@isTest
public class emailAttachmentReassignerTest {
	static testmethod void attachmentTester() {
		Case c = new Case();
		insert c;

		EmailMessage e = new EmailMessage(ParentId=c.Id);
		insert e;

		Attachment a = new Attachment();
		a.name = 'test attachment';
		a.body = blob.valueof('attachment body');
		a.parentid = e.Id;
		//a.parentid = [select id from EmailMessage limit 1].id;
		
		insert a;	
			
		delete a;
	}	
}