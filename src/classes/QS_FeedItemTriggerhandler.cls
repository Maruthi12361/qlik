/**
* @author  Tquila Support
* @date    03/11/2014
*
* Trigger Handler class for QS_EmailCaseFollowers trigger.
* Covers Qlik-15 and Qlik-29
*
* AIN       2015-12-02 added check for internal users to not update the case when an internal comment is posted
* BAD       2017-10-04 CHG0030362 - Rename Qoncierge to Customer Support
* AIN       2017-11-14 CHG0031098 - Reset "Customer Communication" milestone on post to all with access.
* ext_vos   2018-02-14 CHG0032935 - Remove "sendEmail" flag. Customer should recieved emails about all comments except own. Add using of EmailTemplate.
* ext_vos   2018-02-23 CHG0033068 - Case owner should recieve the notification when comment's audience is "Qliktech Only".
* ext_bad   2018-03-05 LCE-17   -   should not change the status of Cases to 'Response Received' for Live Chat after comments.
* ext_vos   2018-03-13 CHG0033068 - Assigned_To user should recieved emails about all comments except own.
* ext_vos  2018-03-26 CHG0030589 - Change ORG_WIDE_EMAIL_NOREPLY from 'noreply@qlikview.com' to 'no-reply@qlik.com'.
* ext_bad   2018-04-12 Delete useless commented code: {ext_vos 2018-02-23 CHG0033068}
* ext_bad   2018-04-12 CHG0033656 - Emails should not be sent for Live Chat Support Record Type cases.
* ext_bad   2018-04-18 CHG0033720 - Change org wide email, select it from custom settings.
* ext_bad   2018-08-27 CHG0034405 - Assigned To should be in BCC.
* ext_vos   2019-02-21 CHG0035404 - Add Case.Subject to email subject.
* AIN       2019-05-05 IT-1597 Updated for Support Portal Redesign
* AIN       2019-07-01 IT-1960 Changed email to no-reply@qlik
* extbad    2019-07-01 IT-1960 Set no-reply@qlik.com for all case types
* extbad    2019-10-14 IT-2174 Added case link for Attunity products
* AIN       2019-10-15 IT-2209 Added check to see if case was already in the list of cases to be updated. Changed list to map.
* extcqb    2020-01-22 IT-2196 Disable email notification for transcript posts
*/

public class QS_FeedItemTriggerhandler {

    public static String QLICK_SUPPORT_URL = 'https://qlikid.qlik.com/portal/support?relaystate=https://support.qlik.com/';
    public static String CASE_DETAILS_URL = 'QS_CaseDetails?caseId=';
    public static String FUNCTIONAL_AREA_FOR_EMAIL = 'Integrations';
    public static String OWNER_TYPE_QUEUE = 'Group';
    public static String OWNER_TYPE_USER = 'User';
    public static String EMAIL_TEMPLATE_NAME = 'Case_FeedItem_Notification_Template';
    public static String FEED_VISIBILITY_ALL = 'AllUsers';
    public static String CASE_ID_STARTS_WITH = '500';
    public static String MS_TYPE_FIRST_RESPONSE = '557D000000000AIIAY';
    public static String MS_TYPE_CUSTOMER_COMMUNICATION = '557D000000000ANIAY';
    public static String CASE_PROFILE_CUSTOMER = 'Customer';
    public static String CASE_PROFILE_PRM = 'PRM';
    private static String LIVE_CHAT_RECORD_TYPE = 'Live Chat Support Record Type';
    private static String MASTER_SUPPORT_RECORD_TYPE = 'QlikTech Master Support Record Type';
    private static final String CHAT_TRANSCRIPT_POST = 'ChatTranscriptPost';

    public enum KEYWORDS {
        CASE_OWNER, CASE_NUMBER, CASE_SUBJECT, FEED_BODY, CASE_URL
    }
    public enum PARAMS_KEY {
        SUBJECT, BODY
    }

    public static void emailCaseFollowers(List<FeedItem> feedItemNewList) {
        String userEmail = [SELECT Email FROM User WHERE Id = :UserInfo.getUserId()].Email;
        String supportPortalUrl = getSupportPortalUrl();

        // Retrieve the current user profile's name
        User currentUser = [SELECT Id, Name, ProfileId, Profile.Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        String profileName = currentUser != null && currentUser.ProfileId != null && currentUser.Profile.Name != null ? currentUser.Profile.Name : '';

        Set<Id> caseIdSet = new Set<Id>();
        Set<Id> caseIdSetQwnerNotify = new Set<Id>();

        Map<Id, FeedItem> caseFeedItemMap = new Map<Id, FeedItem>();
        Map<Id, Case> caseDetailMap = new Map<Id, Case>();

        for (FeedItem feedItem : feedItemNewList) {
            if (feedItem.ParentId != null && ((String) feedItem.ParentId).startsWith(CASE_ID_STARTS_WITH)) {
                if (feedItem.Visibility == FEED_VISIBILITY_ALL) {
                    caseIdSet.add(feedItem.ParentId);
                }
                if (caseFeedItemMap.keySet().isEmpty() || !caseFeedItemMap.containsKey(feedItem.ParentId)) {
                    caseFeedItemMap.put(feedItem.ParentId, feedItem);
                }
            }
        }
        // retrieve all the related cases
        List<Case> caseList = new List<Case>();
        Map<Id, List<String>> caseEmailAddressMap = new Map<Id, List<String>>(); // Map for caseId as key and all related email addresses as list of values
        Map<Id, List<String>> caseEmailAddressMapforBcc = new Map<Id, List<String>>();

        //define contact address
        if (caseIdSet != null && !caseIdSet.isEmpty()) {
            caseList = [
                    SELECT Id, Status, Type, Subject, Assigned_To__c, Assigned_To__r.Email, (
                            SELECT Id, ParentId, SubscriberId, NetworkId, Subscriber.Name, Subscriber.Email,
                                    Subscriber.ContactId, Subscriber.Contact.QS_Opt_out_case_emails__c
                            FROM FeedSubscriptionsForEntity
                    ),
                            CaseNumber, Contact.Email, X1st_Response_Timestamp__c, Last_Communication_Timestamp__c, RecordType.Name, Case_Portal_URL__c
                    FROM Case
                    WHERE Id IN :caseIdSet];

            if (caseList != null && !caseList.isEmpty()) {
                List<EntitySubscription> entitySubscriptionList = new List<EntitySubscription>();

                for (Case caseObj : caseList) {
                    //Add Contact
                    caseDetailMap.put(caseObj.Id, caseObj);
                    if (caseObj.Contact != null && caseObj.Contact.Email != null) {
                        if (userEmail == null || (userEmail != null && caseObj.Contact.Email != userEmail)) {
                            if (caseEmailAddressMap.get(caseObj.Id) == null) {
                                caseEmailAddressMap.put(caseObj.Id, new List<String>(new String[]{caseObj.Contact.Email}));
                            } else {
                                caseEmailAddressMap.get(caseObj.Id).add(caseObj.Contact.Email);
                            }
                        }
                    }
                    // Add Assigned_To User
                    if (caseObj.Assigned_To__c != null && caseObj.Assigned_To__r.Email != null) {
                        if (userEmail == null || (userEmail != null && caseObj.Assigned_To__r.Email != userEmail)) {
                            if (caseEmailAddressMapforBcc.get(caseObj.Id) == null) {
                                caseEmailAddressMapforBcc.put(caseObj.Id, new List<String>(new String[]{caseObj.Assigned_To__r.Email}));
                            } else {
                                caseEmailAddressMapforBcc.get(caseObj.Id).add(caseObj.Assigned_To__r.Email);
                            }
                        }
                    }

                    if (caseObj.FeedSubscriptionsForEntity != null && !caseObj.FeedSubscriptionsForEntity.isEmpty()) {
                        for (EntitySubscription entity : caseObj.FeedSubscriptionsForEntity) {
                            if (userEmail == null || (userEmail != null && entity.Subscriber.Email != userEmail)) {
                                entitySubscriptionList.add(entity);
                            }
                        }
                    }
                }
                if (entitySubscriptionList != null && !entitySubscriptionList.isEmpty()) {
                    for (EntitySubscription entitySubscription : entitySubscriptionList) {
                        if (entitySubscription.SubscriberId == null || entitySubscription.Subscriber.ContactId == null || entitySubscription.Subscriber.Contact.QS_Opt_out_case_emails__c != true) {
                            if (entitySubscription.ParentId != null && entitySubscription.SubscriberId != null && entitySubscription.Subscriber.Email != null) {
                                if (caseEmailAddressMapforBcc.get(entitySubscription.ParentId) == null) {
                                    caseEmailAddressMapforBcc.put(entitySubscription.ParentId, new List<String>(new String[]{
                                            entitySubscription.Subscriber.Email
                                    }));
                                } else {
                                    caseEmailAddressMapforBcc.get(entitySubscription.ParentId).add(entitySubscription.Subscriber.Email);
                                }
                            }
                        }
                    }
                }
            }
        }
        // send notifications
        if (!caseList.isEmpty() || !caseIdSetQwnerNotify.isEmpty()) {
            EmailTemplate template = [
                    SELECT Id, Name, HtmlValue, Body, Subject
                    FROM EmailTemplate
                    WHERE DeveloperName = :EMAIL_TEMPLATE_NAME
                    LIMIT 1
            ];
            if ((caseEmailAddressMap != null && caseEmailAddressMap.keySet() != null && !caseEmailAddressMap.keySet().isEmpty())
                    || (caseEmailAddressMapforBcc!= null && caseEmailAddressMapforBcc.keySet() != null && !caseEmailAddressMapforBcc.keySet().isEmpty())) {

                Map<String, String> placeholdersValue = new Map<String, String>();

                Messaging.SingleEmailMessage[] messagingEmailList = new List<Messaging.SingleEmailMessage>();
                Set<Id> caseIds = new Set<Id>();
                if (caseEmailAddressMap.keySet() != null && !caseEmailAddressMap.isEmpty()) {
                    caseIds.addAll(caseEmailAddressMap.keySet());
                }
                if (caseEmailAddressMapforBcc.keySet() != null && !caseEmailAddressMapforBcc.isEmpty()) {
                    caseIds.addAll(caseEmailAddressMapforBcc.keySet());
                }

                OrgWideEmailAddress noreplyOwea = null;
                String noReplyAddress = '';
                if(QTCustomSettings__c.getInstance('Default') != null) {
                    noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
                }
                List<OrgWideEmailAddress> oweaList = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName =: noReplyAddress];
                if (oweaList.size() > 0) {
                    noreplyOwea = oweaList.get(0);
                }

                for (Id caseId : caseIds) {
                    if (caseDetailMap.get(caseId).RecordType.Name == MASTER_SUPPORT_RECORD_TYPE) {
                        placeholdersValue = setPlaceholdersValue(template, caseDetailMap.get(caseId), caseFeedItemMap, supportPortalUrl);

                        String[] toAddresses = new List<String>();
                        String[] bccAddresses = new List<String>();
                        if ((caseEmailAddressMap.containsKey(caseId) && caseEmailAddressMap.get(caseId) != null) ||
                                (caseEmailAddressMapforBcc.containsKey(caseId) && caseEmailAddressMapforBcc.get(caseId) != null)) {
                            Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();

                            if (caseEmailAddressMap != null && caseEmailAddressMap.keySet() != null && !caseEmailAddressMap.keySet().isEmpty()) {
                                toAddresses.addAll(caseEmailAddressMap.get(caseId));
                            }

                            if (caseEmailAddressMapforBcc != null && caseEmailAddressMapforBcc.keySet() != null && !caseEmailAddressMapforBcc.keySet().isEmpty()) {
                                bccAddresses.addAll(caseEmailAddressMapforBcc.get(caseId));
                            }

                            if ((toAddresses != null && !toAddresses.isEmpty()) || (bccAddresses != null && !bccAddresses.isEmpty())) {

                                emailMessage.setToAddresses(toAddresses);

                                if (bccAddresses != null && !bccAddresses.isEmpty()) {
                                    emailMessage.setBccAddresses(bccAddresses);
                                }

                                if (noreplyOwea != null) {
                                    emailMessage.setOrgWideEmailAddressId(noreplyOwea.Id);
                                }

                                emailMessage.setSubject(placeholdersValue.get(PARAMS_KEY.SUBJECT.name()));
                                emailMessage.setHtmlBody(placeholdersValue.get(PARAMS_KEY.BODY.name()));
                                //emailMessage.setWhatId(caseId);
                                if (!caseFeedItemMap.keySet().isEmpty() && caseFeedItemMap.containsKey(caseId) && caseFeedItemMap.get(caseId) != null) {
                                    String mailBody = caseFeedItemMap.get(caseId).Body;

                                    if (mailBody != null && mailBody.contains('Email received on case:')) {
                                        emailMessage = new Messaging.SingleEmailMessage();
                                        return;
                                    }

                                    if (caseFeedItemMap.get(caseId).Type == CHAT_TRANSCRIPT_POST) {
                                        continue;
                                    }
                                }

                                messagingEmailList.add(emailMessage);
                            }
                        }
                    }
                }
                System.debug ('~~~~~~ Inside QS_FeedItemTriggerhandler - before sending email- ' + messagingEmailList.size());
                if (messagingEmailList != null && !messagingEmailList.isEmpty()) {
                    try {
                        List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(messagingEmailList);
                        for (Messaging.SendEmailResult res : sendRes) {
                            System.debug('Result: ' + res);
                        }
                    } catch (System.EmailException ex) {
                        System.debug('Error when sending mail: ' + ex.getMessage());
                    }
                }
            }
            List<Id> milestoneCaseIDs = new List<Id>();
            Map<Id, Case> caseUpdateList = new Map<Id, Case>();
            // Change the status for the case only when the case is not currently under 'Ready To Close' status
            for (FeedItem feedItem : feedItemNewList) {
                if (feedItem.ParentId != null && ((String) feedItem.ParentId).startsWith(CASE_ID_STARTS_WITH) &&
                        profileName != null && profileName != '' && caseDetailMap != null &&
                        caseDetailMap.keySet() != null && (!caseDetailMap.keySet().isEmpty()) &&
                        caseDetailMap.containsKey(feedItem.ParentId) && caseDetailMap.get(feedItem.ParentId) != null &&
                        feedItem.Visibility == FEED_VISIBILITY_ALL) {
                    //Check the Current User's profile and set the Case status
                    if ((profileName.startsWith(CASE_PROFILE_CUSTOMER) || profileName.startsWith(CASE_PROFILE_PRM))
                            && caseDetailMap.get(feedItem.ParentId).RecordType.Name != LIVE_CHAT_RECORD_TYPE
                            ) {
                        if ((caseDetailMap.get(feedItem.ParentId).Status == null || caseDetailMap.get(feedItem.ParentId).Status != System.Label.QS_CloseCase) &&
                                caseDetailMap.get(feedItem.ParentId).Status != System.Label.QS_CaseClosed &&
                                caseDetailMap.get(feedItem.ParentId).Status != System.Label.QS_CloseCase && !caseUpdateList.containsKey(feedItem.ParentId)) {
                            caseUpdateList.put(feedItem.ParentId, new Case(Id = feedItem.ParentId, Status = System.Label.QS_CaseFeedResponseExt_Status));
                        }
                    } else if(!caseUpdateList.containsKey(feedItem.ParentId)){
                        Case caseObj = new Case();
                        caseObj.Id = feedItem.ParentId;
                        if (caseDetailMap.get(feedItem.ParentId).X1st_Response_Timestamp__c == null) {
                            caseObj.X1st_Response_Timestamp__c = Datetime.now();
                        }
                        caseObj.Last_Communication_Timestamp__c = Datetime.now();
                        caseUpdateList.put(feedItem.ParentId, caseObj);
                        milestoneCaseIDs.add(feedItem.ParentId);
                    }
                    //caseList.add(new Case(Id=feedItem.ParentID, Status = System.Label.QS_CaseFeedResponseExt_Status));
                    //else if (profileName.startsWith('Custom: Support') )
                    //caseUpdateList.add(new Case(Id=feedItem.ParentID, Status = System.Label.QS_CaseFeedResponse_Support_Status));

                }

            }

            List<CaseMilestone> listCM = new List<CaseMilestone>();
            List<CaseMilestone> Caselen = [SELECT Id, CaseId, CompletionDate, MilestoneTypeId FROM CaseMilestone WHERE CaseId =: milestoneCaseIDs AND IsCompleted = FALSE];
            if(Caselen.size() > 0) {
                for (CaseMilestone cm : Caselen) {
                    if (cm.MilestoneTypeId == MS_TYPE_FIRST_RESPONSE) {
                        cm.CompletionDate = Datetime.now();
                    } else if (cm.MilestoneTypeId == MS_TYPE_CUSTOMER_COMMUNICATION) {
                        cm.StartDate = Datetime.now();
                    }
                    listCM.add(cm);
                    //cases.put(cm.CaseId, null);
                    System.debug('Updating milestone with ID ' + cm.Id);
                }
                update Caselen;
            }

            // Update the Status of the Case
            if (caseUpdateList.values() != null && !caseUpdateList.values().isEmpty()) {
                Database.update(caseUpdateList.values(), false);
            }
        }
    }

    private static Map<String,String> setPlaceholdersValue(EmailTemplate t, Case c, Map<Id, FeedItem> feedItemMap, String supportPortalUrl) {
        Map<String,String> params = new Map<String,String>();


        String subj = '';
        if (t.Subject != null && t.Subject.contains(KEYWORDS.CASE_NUMBER.name())) {
            subj = t.Subject.replace(KEYWORDS.CASE_NUMBER.name(), (c.CaseNumber != null ? c.CaseNumber : ''));
        }
        if (t.Subject.contains(KEYWORDS.CASE_SUBJECT.name())) {
            subj = subj.replace(KEYWORDS.CASE_SUBJECT.name(), (c.Subject != null ? c.Subject : ''));
        }
        params.put(PARAMS_KEY.SUBJECT.name(), subj);


        String body = '';
        if (t.HtmlValue != null && t.HtmlValue != '') {
            body = t.HtmlValue;

            if (t.HtmlValue.contains(KEYWORDS.CASE_NUMBER.name())) {
                body = body.replace(KEYWORDS.CASE_NUMBER.name(), (c.CaseNumber != null ? c.CaseNumber : ''));
            }
            if (t.HtmlValue.contains(KEYWORDS.CASE_SUBJECT.name())) {
                body = body.replace(KEYWORDS.CASE_SUBJECT.name(), (c.Subject != null ? c.Subject : ''));
            }
            if (t.HtmlValue.contains(KEYWORDS.CASE_URL.name())) {
                body = body.replace(KEYWORDS.CASE_URL.name(), c.Case_Portal_URL__c);
            }
            if (feedItemMap != null && feedItemMap.size() > 0 && feedItemMap.containsKey(c.Id) && feedItemMap.get(c.Id) != null) {
                if (t.HtmlValue.contains(KEYWORDS.CASE_OWNER.name())) {
                    body = body.replace(KEYWORDS.CASE_OWNER.name(), (feedItemMap.get(c.Id).Visibility == FEED_VISIBILITY_ALL ? 'your' : ''));
                }
                if (t.HtmlValue.contains(KEYWORDS.FEED_BODY.name())) {
                    String feedBody = feedItemMap.get(c.Id).Body != null ? feedItemMap.get(c.Id).Body : '';
                    if (feedBody == '') {
                        feedBody = 'An attachment was added to your case';
                    } else if (feedBody.length() > 600) {
                        feedBody = feedBody.substring(0, 600) + '...(continued) ';
                    }

                    body = body.replace(KEYWORDS.FEED_BODY.name(), feedBody);
                }
            }

        }
        params.put(PARAMS_KEY.BODY.name(), body);

        return params;
    }

    private static String getSupportPortalUrl() {
        String url = QLICK_SUPPORT_URL;

        QS_Partner_Portal_Urls__c cs = QS_Partner_Portal_Urls__c.getInstance();
        if (cs != null && cs.Support_Portal_Url__c != null && cs.Support_Portal_Url__c != '') {
            url = cs.Support_Portal_Url__c;
        }
        return url;
    }

}