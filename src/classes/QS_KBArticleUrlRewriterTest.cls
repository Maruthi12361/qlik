/*****************************************************************
* 2018-12-13 extbad change page param from 'id' to 'articleId' to prevent 'invalid parameter value: []' error on the article page
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*****************************************************************/
@isTest
private class QS_KBArticleUrlRewriterTest {
    
    private static Id recTypeId;

    static testMethod void testRewriter2() {
        
        
        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('000111', 'test from apex', '000111', 'en_US');
        insert basicKav;
        
        basicKav = [Select Id, KnowledgeArticleId, ArticleNumber From Basic__kav where Id = :basicKav.Id LIMIT 1];
         //publish it
        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);
       
        KnowledgeArticleVersion knowledgeArticleVersion = [Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                                                            From KnowledgeArticleVersion
                                                            Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                                                            LIMIT 1];
        
            
        Test.startTest();
        // testing for null values
        QS_KBArticleUrlRewriter2 rewriter = new QS_KBArticleUrlRewriter2();
        System.assert(rewriter.generateUrlFor(null) == null);
        System.assert(rewriter.mapRequestUrl(null) == null);
        
        // test mapRequestUrl method
        rewriter = new QS_KBArticleUrlRewriter2();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/articles/'+knowledgeArticleVersion.ArticleNumber)).getUrl();
        String expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.ArticleNumber;
        system.debug('AIN knowledgeArticleVersion.ArticleNumber: ' + knowledgeArticleVersion.ArticleNumber);
        system.debug('AIN actualResult: ' + actualResult);
        system.debug('AIN expectedResult: ' + expectedResult);
        System.assert(actualResult == expectedResult);

        string extraParameter1 = 'description=test4';
        string extraParameter2 = 'fromCaseWizard=test';
        string extraParameter3 = 'recordTypeName=test5';
        string extraParameter4 = 'severity=test3';
        string extraParameter5 = 'subject=test2';
        string extraParameters = '&'+extraParameter1+'&'+extraParameter2+'&'+extraParameter3+'&'+extraParameter4+'&'+extraParameter5;
        rewriter = new QS_KBArticleUrlRewriter2();
        actualResult = rewriter.mapRequestUrl(new PageReference('/articles/'+knowledgeArticleVersion.ArticleNumber+extraParameters)).getUrl();
        expectedResult = '/QS_CaseWizardKnowledgeArticle';
        
        System.assert(actualResult.contains(expectedResult));
        System.assert(actualResult.contains(extraParameter1));
        System.assert(actualResult.contains(extraParameter2));
        System.assert(actualResult.contains(extraParameter3));
        System.assert(actualResult.contains(extraParameter4));
        System.assert(actualResult.contains(extraParameter5));

        rewriter = new QS_KBArticleUrlRewriter2();
        actualResult = rewriter.mapRequestUrl(new PageReference('/a/'+knowledgeArticleVersion.ArticleNumber)).getUrl();
        expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.ArticleNumber;
        system.debug('AIN knowledgeArticleVersion.ArticleNumber: ' + knowledgeArticleVersion.ArticleNumber);
        system.debug('AIN actualResult: ' + actualResult);
        system.debug('AIN expectedResult: ' + expectedResult);
        System.assert(actualResult == expectedResult);
        
        // test generateUrlFor method test coverage
        rewriter = new QS_KBArticleUrlRewriter2();
        rewriter.generateUrlFor(new List<PageReference>{new PageReference('www.google.com')});

        // test negative cases
        rewriter = new QS_KBArticleUrlRewriter2();
        String actualResultPageRef = rewriter.mapRequestUrl(new PageReference('/articles')).getURL();
        expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=';
        System.assert(actualResultPageRef == '/articles');
        
        Test.stopTest();
    }
     
    static testMethod void testBasicKBURLRewriter() {
        
        
        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('test apex', 'test from apex', 'test-apex', 'en_US');
        insert basicKav;
        
        basicKav = [Select Id, KnowledgeArticleId, ArticleNumber From Basic__kav where Id = :basicKav.Id LIMIT 1];
         //publish it
        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);
       
        KnowledgeArticleVersion knowledgeArticleVersion = [Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                                                            From KnowledgeArticleVersion
                                                            Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                                                            LIMIT 1];
        
            
        Test.startTest();
        // testing for null values
        QS_KBArticleUrlRewriter rewriter = new QS_KBArticleUrlRewriter();
        System.assert(rewriter.generateUrlFor(null) == null);
        System.assert(rewriter.mapRequestUrl(null) == null);
        
        // test mapRequestUrl method
        rewriter = new QS_KBArticleUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/articles/'+knowledgeArticleVersion.UrlName)).getUrl();
        String expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id;
        System.assert(actualResult == expectedResult);
        
        // test generateUrlFor method
        rewriter = new QS_KBArticleUrlRewriter();
        actualResult = '/articles/'+knowledgeArticleVersion.UrlName;
        List<PageReference> salesforcePageRef = new List<PageReference>();
        salesforcePageRef.add( new PageReference('/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id)); 
        expectedResult = rewriter.generateUrlFor(salesforcePageRef)[0].getURL();
        System.assert(actualResult == expectedResult);
        
        
        // test negative cases
        rewriter = new QS_KBArticleUrlRewriter();
        String actualResultPageRef = rewriter.mapRequestUrl(new PageReference('/articles')).getURL();
        expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=';
        System.assert(actualResultPageRef == '/articles');
        
        Test.stopTest();
    }
    
    static testmethod void testAsCommunityUser() {
        
        
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        
        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('test apex', 'test from apex', 'test-apex', 'en_US');
        insert basicKav;
        
        basicKav = [Select Id, KnowledgeArticleId, ArticleNumber From Basic__kav where Id = :basicKav.Id LIMIT 1];
         //publish it
        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);
       
        KnowledgeArticleVersion knowledgeArticleVersion = [Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                                                            From KnowledgeArticleVersion
                                                            Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                                                            LIMIT 1];
        
            
        Test.startTest();
        
        
        // Test the article url rewriting as a community user (Since the article hasn't been assigned to the user so it will not be able to the retrieve the article and hence return the same url)
        System.runAs(communityUser) {
            // test generateUrlFor method when passing the subject, description, record type name etc. (parameters list)
            QS_KBArticleUrlRewriter rewriter = new QS_KBArticleUrlRewriter();
            String expectedResult = '/articles/' + knowledgeArticleVersion.UrlName;
            List<PageReference> salesforcePageRef = new List<PageReference>();
            salesforcePageRef.add( new PageReference('/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id)); 
            List<PageReference> friendlyURL = rewriter.generateUrlFor(salesforcePageRef);
            String actualResult = (friendlyURL != null && !friendlyURL.isEmpty() && friendlyURL[0] != null) ? friendlyURL[0].getURL() : null;
            System.debug('%%%actualResult:'+actualResult);
            System.debug('%%%expectedResult:'+expectedResult);
            System.assertNotEquals(actualResult, expectedResult);
            System.assertEquals(friendlyURL.isEmpty(), true);
            System.assertEquals(actualResult, null);
            
            // test mapRequestUrl method
            rewriter = new QS_KBArticleUrlRewriter();
            actualResult = rewriter.mapRequestUrl(new PageReference('/articles/'+knowledgeArticleVersion.UrlName)).getUrl();
            expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id;
            System.debug('%%%actualResult:'+actualResult);
            System.debug('%%%expectedResult:'+expectedResult);
            System.assertNotEquals(actualResult, expectedResult);
            System.assertEquals(actualResult, '/articles/'+knowledgeArticleVersion.UrlName);
            
            //System.assert(actualResult == expectedResult);
            
        
        }
        
        Test.stopTest();
    }
    
    static testMethod void testKBURLRewriterOtherParams() {
        
        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('test apex', 'test from apex', 'test-apex', 'en_US');
        insert basicKav;
        
        basicKav = [Select Id, KnowledgeArticleId, ArticleNumber From Basic__kav where Id = :basicKav.Id LIMIT 1];
         //publish it
        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);
       
        KnowledgeArticleVersion knowledgeArticleVersion = [Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                                                            From KnowledgeArticleVersion
                                                            Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                                                            LIMIT 1];
        
        Test.startTest();
        
        // test mapRequestUrl method for a list of parameters (i.e. case's subject, severity, description etc.)
        QS_KBArticleUrlRewriter rewriter = new QS_KBArticleUrlRewriter();
        String actualParams = '/true/Test+Subject/3/Have+you+found+any+work+around%3D%0A%0AHow+many+users+are+affected%3F%0A%0AError+Messages+%28if+any%29%0A%0AWhat+are+the+expected+results%3F%0A%0ASteps+to+reproduce+the+issue%0A%0A/QlikTech+Master+Support+Record+Type';
        String actualResult = rewriter.mapRequestUrl(new PageReference('/articles/' + knowledgeArticleVersion.UrlName + actualParams)).getUrl();
        String expectedURLParams = '&description=Have+you+found+any+work+around%3D_newline__newline_How+many+users+are+affected_questionmark__newline__newline_Error+Messages+%28if+any%29_newline__newline_What+are+the+expected+results_questionmark__newline__newline_Steps+to+reproduce+the+issue_newline__newline_&fromCaseWizard=true&recordTypeName=QlikTech+Master+Support+Record+Type&severity=3&subject=Test+Subject';
        String expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id + expectedURLParams;
        System.debug('%%%%%actualResult: '+actualResult);
        System.assert(actualResult == expectedResult);
        
      
        
        // test generateUrlFor method when passing the subject, description, record type name etc. (parameters list)
        rewriter = new QS_KBArticleUrlRewriter();
        actualResult = '/articles/'+knowledgeArticleVersion.UrlName+'/true/Test+Subject/3/Test+Description_questionmark__newline__newline_Test+Questions/QlikTech+Master+Support+Record+Type';
        List<PageReference> salesforcePageRef = new List<PageReference>();
        String otherParams = 'fromCaseWizard%3Dtrue%26subject%3DTest%20Subject%26severity%3D3%26description%3DTest%20Description%3F%0A%0ATest%20Questions%26recordTypeName%3DQlikTech%20Master%20Support%20Record%20Type';
        salesforcePageRef.add( new PageReference('/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id+'&otherParam='+otherParams)); 
        salesforcePageRef.add( new PageReference('/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id+'&otherParam='+otherParams)); 
        List<PageReference> expectedResultList = rewriter.generateUrlFor(salesforcePageRef);
        expectedResult = expectedResultList[0].getURL();
        System.debug('%%%%%expectedResult: '+expectedResult);
        
        System.assert(actualResult == expectedResult);
        
        Test.stopTest();
        
    }
    
    static testMethod void testKBURLRewriterFromCase() {
        
        
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        
        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('test apex', 'test from apex', 'test-apex', 'en_US');
        insert basicKav;
        
        basicKav = [Select Id, KnowledgeArticleId, ArticleNumber From Basic__kav where Id = :basicKav.Id LIMIT 1];
         //publish it
        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);
       
        KnowledgeArticleVersion knowledgeArticleVersion = [Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                                                            From KnowledgeArticleVersion
                                                            Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                                                            LIMIT 1];
        
        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5398347387423');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        
        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                                    testContact.Id, testAccount.Id, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        insert caseObj;                         
            
        Test.startTest();
        
         
        // test mapRequestUrl method for caseId as parameter
        QS_KBArticleUrlRewriter rewriter = new QS_KBArticleUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/articles/'+knowledgeArticleVersion.UrlName+'/'+caseObj.Id)).getUrl();
        String expectedResult = '/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id+'&caseId='+caseObj.Id+'%2F';
        System.debug('%%%%actualResult: '+actualResult);
        System.debug('%%%%expectedResult: '+expectedResult);
        System.assert(actualResult == expectedResult);
        
        // test generateUrlFor method for caseId as parameter
        rewriter = new QS_KBArticleUrlRewriter();
        actualResult = '/articles/'+knowledgeArticleVersion.UrlName +'/caseId='+caseObj.Id+'%2F';
        List<PageReference> salesforcePageRef = new List<PageReference>();
        salesforcePageRef.add( new PageReference('/QS_CaseWizardKnowledgeArticle?articleId=' + knowledgeArticleVersion.Id+'&caseId='+caseObj.Id+'/')); 
        expectedResult = rewriter.generateUrlFor(salesforcePageRef)[0].getURL();
        System.debug('%%%%actualResult: '+actualResult);
        System.debug('%%%%expectedResult: '+expectedResult);
        System.assert(actualResult == expectedResult);
        
        
        
        Test.stopTest();
        
    }
    // Get Record Type Id of the case based on record type name
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }

}