//*********************************************************/
// Author: Mark Cane&
// Creation date: 17/08/2010
// Intent:  Simple provider of page navigation - per event.
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efAppNavigator{
	private efEventWrapper e;
	private String currentPage;
	private String registrationId='';
	private Boolean isEmployee=false;

	public static final String EVENTS_HOME = 'efEventforceHome';
	public static final String REGISTRATION_ATTENDEE_INFO_PAGE = 'efRegistrationAttendeeInfo';
	public static final String REGISTRATION_TRAINING_PAGE = 'efRegistrationTraining';
	public static final String REGISTRATION_SUMMARY_PAGE = 'efRegistrationSummary';
	public static final String REGISTRATION_FINISH_PAGE = 'efRegistrationFinish';
	public static final String REGISTRATION_REGISTER_GROUP = 'efRegisterGroup';	
	public static final String ATTENDEE_PORTAL_HOME = 'efAttendeePortalHome';
	public static final String ATTENDEE_PORTAL_CALENDAR = 'efAttendeePortalCalendar';
	public static final String ATTENDEE_PORTAL_SESSION = 'efAttendeePortalSession';
	public static final String ATTENDEE_PORTAL_ACTIVITY = 'efAttendeePortalActivity';
	public static final String ATTENDEE_PORTAL_REGISTRATION = 'efAttendeePortalRegistration';	
	
	public efAppNavigator(efEventWrapper e, String registrationId){
		this.e = e;
		this.registrationId = registrationId;
	}
	
	public efAppNavigator(efEventWrapper e, String registrationId, Boolean isEmployee){
		this.e = e;
		this.registrationId = registrationId;
		this.isEmployee = isEmployee;
	}
	
	public String getCurrentPage(){
		return currentPage;
	}
	
	public void setCurrentPage(String currentPage){
		this.currentPage = currentPage;
	}
	
	public PageReference getNextPageReference(String currentPage){
		PageReference pr;
		
		if (currentPage==REGISTRATION_ATTENDEE_INFO_PAGE){
			if (getEventHasTraining()){
				this.currentPage = REGISTRATION_TRAINING_PAGE;	
				pr = new PageReference('/apex/'+REGISTRATION_TRAINING_PAGE);				
			} else{
				this.currentPage = REGISTRATION_SUMMARY_PAGE;
				pr = new PageReference('/apex/'+REGISTRATION_SUMMARY_PAGE);
			}			
		}
		if (currentPage==REGISTRATION_TRAINING_PAGE){
			this.currentPage = REGISTRATION_SUMMARY_PAGE;
			pr = new PageReference('/apex/'+REGISTRATION_SUMMARY_PAGE);
		}
		return pr;	
	}
	
	public PageReference getBackPageReference(String currentPage){
		PageReference pr;
		
		if (currentPage==REGISTRATION_TRAINING_PAGE){
			this.currentPage = REGISTRATION_ATTENDEE_INFO_PAGE;
			pr = new PageReference('/apex/'+REGISTRATION_ATTENDEE_INFO_PAGE);
		}
		if (currentPage==REGISTRATION_SUMMARY_PAGE){
			if (getEventHasTraining()){
				this.currentPage = REGISTRATION_TRAINING_PAGE;	
				pr = new PageReference('/apex/'+REGISTRATION_TRAINING_PAGE);				
			} else{
				this.currentPage = REGISTRATION_ATTENDEE_INFO_PAGE;
				pr = new PageReference('/apex/'+REGISTRATION_ATTENDEE_INFO_PAGE);
			}
		}
		return pr;
	}
	
	public PageReference getPageReference(String currentPage){
		PageReference pr;
		
		if (currentPage==REGISTRATION_ATTENDEE_INFO_PAGE){
			this.currentPage = REGISTRATION_ATTENDEE_INFO_PAGE;
			pr = new PageReference('/apex/'+REGISTRATION_ATTENDEE_INFO_PAGE);
		}		
		if (currentPage==REGISTRATION_TRAINING_PAGE){
			this.currentPage = REGISTRATION_TRAINING_PAGE;
			pr = new PageReference('/apex/'+REGISTRATION_TRAINING_PAGE);
		}
		if (currentPage==REGISTRATION_SUMMARY_PAGE){
			this.currentPage = REGISTRATION_SUMMARY_PAGE;
			pr = new PageReference('/apex/'+REGISTRATION_SUMMARY_PAGE);
		}
		if (currentPage==REGISTRATION_FINISH_PAGE){
			this.currentPage = REGISTRATION_FINISH_PAGE;
			pr = new PageReference('/apex/'+REGISTRATION_FINISH_PAGE);
		}
		return pr;
	}
	
	public PageReference resolveFromRegistration(efRegistrationManager rm){
		return null;
	}
	
	public PageReference returnPageReference(String pageName){		
		PageReference pr = new PageReference('/apex/'+pageName);
		pr.getParameters().put('registrationId', registrationId);
		return pr;
	}
	
	public PageReference returnAttendeePortalPageReference(efRegistrationManager rm){
		return new PageReference('/apex/'+ATTENDEE_PORTAL_HOME);
	}
	
	public Boolean getEventHasTraining(){
		// Comment& : guard conditions, dereference null occurrences.	
		if (isEmployee==null) return false;
		if (e.getEvent().Has_Training__c==null) return false;
		
		return (!isEmployee && e.getEvent().Has_Training__c);		
	}
	
	public static PageReference pr(String pageName){
		PageReference p = new PageReference('/apex/'+pageName);
		p.setRedirect(true);
		return p;
	}
}