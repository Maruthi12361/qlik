/*****************************************************************************************
* 2015-09-28 AIN Webservice Mock for class sfUtilsQlikviewCom, using WebServiceMockDispatcher is prefered, see
* that class for details.
****************************************************************************************/
@isTest
public class QTVoucherMock implements WebServiceMock {

  public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    system.debug('QTVoucherMock Start');
    if(request instanceof qtvoucherQliktechCom.CheckVoucher_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CheckVoucher_element');
      response.put('response_x', new qtvoucherQliktechCom.CheckVoucherResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.CreateVoucher_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CreateVoucher_element()');
      response.put('response_x', new qtvoucherQliktechCom.CreateVoucherResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.ConsumeVoucher_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.ConsumeVoucher_element');
      response.put('response_x', new qtvoucherQliktechCom.ConsumeVoucherResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.GeteLearningSubscription_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.GeteLearningSubscription_element');
      response.put('response_x', new qtvoucherQliktechCom.GeteLearningSubscriptionResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.CreateVoucherFromOppLineItems_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CreateVoucherFromOppLineItems_element');
      response.put('response_x', new qtvoucherQliktechCom.CreateVoucherFromOppLineItemsResponse_element());
    }
	else if(request instanceof qtvoucherQliktechCom.CheckCancellation_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CheckCancellation_element');
      response.put('response_x', new qtvoucherQliktechCom.CheckCancellationResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.CreateVoucherFromSFDC_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CreateVoucherFromSFDC_element');
      response.put('response_x', new qtvoucherQliktechCom.CreateVoucherFromSFDCResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.ExpireVouchers_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.ExpireVouchers_element');
      response.put('response_x', new qtvoucherQliktechCom.ExpireVouchersResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.ActiveElearningSubscription_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.ActiveElearningSubscription_element');
      response.put('response_x', new qtvoucherQliktechCom.ActiveElearningSubscriptionResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.CancelVoucher_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CancelVoucher_element');
      response.put('response_x', new qtvoucherQliktechCom.CancelVoucherResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.TryVoucherCreationAndActivation_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.TryVoucherCreationAndActivation_element');
      response.put('response_x', new qtvoucherQliktechCom.TryVoucherCreationAndActivationResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.ActivateVouchersFromOpportunities_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.ActivateVouchersFromOpportunities_element');
      response.put('response_x', new qtvoucherQliktechCom.ActivateVouchersFromOpportunitiesResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.CreateVouchersFromOpportunities_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CreateVouchersFromOpportunities_element');
      response.put('response_x', new qtvoucherQliktechCom.CreateVouchersFromOpportunitiesResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.ActivateVouchersFromOpportunity_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.ActivateVouchersFromOpportunity_element');
      response.put('response_x', new qtvoucherQliktechCom.ActivateVouchersFromOpportunityResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.CreateVoucherFromNLRP_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CreateVoucherFromNLRP_element');
      response.put('response_x', new qtvoucherQliktechCom.CreateVoucherFromNLRPResponse_element());
    }
    else if(request instanceof qtvoucherQliktechCom.CreateVoucherFromOpportunity_element)
    {
      system.debug('instanceof qtvoucherQliktechCom.CreateVoucherFromOpportunity_element');
      response.put('response_x', new qtvoucherQliktechCom.CreateVoucherFromOpportunityResponse_element());
    }
    

    

    system.debug('QTVoucherMock End');
       
  }
}