/* 
 *  2014-10-24  TJG Initial version
 *  To test the QS_EmailCaseFollowers trigger
 *  2017-11-14 AIN Optimized test class to do less inserts;
 *  2018-02-23 ext_vos add test_internalFeedItem for Case.Owner notify.
 *  2018-03-19 ext_vos update test_internalFeedItem for Case.Assigned_To__c notify.
 *  2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 */
@isTest
private class QS_FeedItemTriggerhandlerTest {

    //private static final string ContactResponseReveived = 'Contact Response Received';

    //private static final string PendingContactResponse = 'Pending Contact Response';
    private static Id recTypeId;
    private static Id networkId;

    static testMethod void test_feedItemInsertUpdate() {

        QTTestUtils.GlobalSetUp();

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'leadSource';

        //Create Contact
        Contact testContact2 = TestDataFactory.createContact('test_FName', 'test_LName2', 'testSandbox2@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact2.Persona__c = 'Decision Maker';

        //Create Contact
        Contact testContact3 = TestDataFactory.createContact('test_FName', 'test_LName3', 'testSandbox3@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact3.Persona__c = 'Decision Maker';

        List<Contact> contacts = new List<Contact>();
        contacts.add(testContact);
        contacts.add(testContact2);
        contacts.add(testContact3);
        insert contacts;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));

        // Create Community User
        User communityUser2 = TestDataFactory.createUser(profileId, 'testSandbox2@qlikTech.com', 'tSbo2', String.valueOf(testContact2.Id));

        // Create Community User
        User communityUser3 = TestDataFactory.createUser(profileId, 'testSandbox3@qlikTech.com', 'tSbo3', String.valueOf(testContact3.Id));

        List<USer> users = new List<User>();
        Users.Add(communityUser);
        Users.Add(communityUser2);
        Users.Add(communityUser3);

        insert users;


        User supportProfileUser = [select Id, Name from User where Profile.Name LIKE 'Custom: Support%' AND IsActive = true limit 1];


        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;

        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;


        List<Case> caseObjList = new List<Case>();
        //for(integer i=0; i<3; i++) {
        // assigning the first 60 cases to community user 1 and the remaining 40 to community user 2
        caseObjList.add(TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                false, null, environment.Id,
                productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c));
        //}
        // Setting the first 25 cases as closed
        //for(integer i=0; i<3;i++) {
        //    caseObjList[0].Status = 'Closed';
        //}
        Semaphores.TriggerHasRun('CaseSalesOps', 1);
        Semaphores.TriggerHasRun('CaseEndUserAccountNameCopy', 1);
        Semaphores.TriggerHasRun('CaseQuickClose', 1);
        Semaphores.TriggerHasRun('CaseTrigger', 1);
        Semaphores.TriggerHasRun('createSolutions', 1);
        Semaphores.TriggerHasRun('CasePopulateSupportEmailFields', 1);
        Semaphores.TriggerHasRun('CaseBusinessCritical', 1);
        Semaphores.TriggerHasRun('CaseEntitlementProcessSync', 1);
        Semaphores.TriggerHasRun('CaseDetectLanguage', 1);
        Semaphores.TriggerHasRun('CaseUpdateSupportContact', 1);
        Semaphores.TriggerHasRun('CaseBusinessCriticalSupportTeamSetup', 1);
        Semaphores.TriggerHasRun('CaseClosedSendSurvey', 1);
        Semaphores.TriggerHasRun('CaseToCR', 1);
        Semaphores.TriggerHasRun('Case_Sharing', 1);

        //caseObjList[1].Status = System.Label.QS_CloseCase;
        Test.startTest();
        insert caseObjList;


        List<FeedItem> feedItemList = new List<FeedItem>();
        // adding a feed item to the closed case without any subscribers
        //feedItemList.add( TestDataFactory.createFeedItem('Test FeedItem 1', caseObjList[0].Id));
        // adding a feed item to an open case without any subsribers
        feedItemList.add( TestDataFactory.createFeedItem('Test FeedItem 2', caseObjList[0].Id));
        insert feedItemList;

        // Add few subscribers
        //EntitySubscription
        List<EntitySubscription> entitySubscriptionList = new List<EntitySubscription>();
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser.Id, caseObjList[0].Id));
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser2.Id, caseObjList[0].Id));
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser3.Id, caseObjList[0].Id));
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), userinfo.getUserId(), caseObjList[0].Id));
        //entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser.Id, caseObjList[0].Id));
        //entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser3.Id, caseObjList[0].Id));
        //entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser.Id, caseObjList[1].Id));
        //entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser3.Id, caseObjList[1].Id));

        insert entitySubscriptionList;

        // Can't run the following code as there are couple of triggers on case which are not bulkified
        //again try to insert a new chatter post on closed case
        //System.runAs(communityUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Community User 1', caseObjList[0].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values and if the status changed on closed case
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CaseFeedResponseExt_Status);
        //}

        //again try to insert a new chatter post
        System.runAs(communityUser) {
            FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Community User 1', caseObjList[0].Id);
            fitem1.Visibility = 'AllUsers';
            insert fitem1;
            // Assertions to check the values
            System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CaseFeedResponseExt_Status);
        }

        //again try to insert a new chatter post on 'Ready to Close' case
        //System.runAs(communityUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Community User 1', caseObjList[1].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values and if the status changed on 'Ready to Close' case
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[1].Id][0].Status, System.Label.QS_CloseCase);
        //}

        // Can't run the following code as there are couple of triggers on case which are not bulkified
        //again try to insert a new chatter post
        //System.runAs(supportProfileUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Support', caseObjList[0].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CaseFeedResponse_Support_Status);
        //}
        //again try to insert a new chatter post
        //System.runAs(supportProfileUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Support', caseObjList[0].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CaseFeedResponse_Support_Status);

        //}


        //again try to insert a new chatter post on 'Ready to Close' case
        //System.runAs(supportProfileUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Support', caseObjList[1].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values and if the status changed on 'Ready to Close' case
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[1].Id][0].Status, System.Label.QS_CloseCase);
        //}

        Test.stopTest();
    }

    static testMethod void test_feedItemInsertUpdate2() {
        QTTestUtils.GlobalSetUp();

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'leadSource';

        //Create Contact
        Contact testContact2 = TestDataFactory.createContact('test_FName', 'test_LName2', 'testSandbox2@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact2.Persona__c = 'Decision Maker';

        //Create Contact
        Contact testContact3 = TestDataFactory.createContact('test_FName', 'test_LName3', 'testSandbox3@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact3.Persona__c = 'Decision Maker';

        List<Contact> contacts = new List<Contact>();
        contacts.add(testContact);
        contacts.add(testContact2);
        contacts.add(testContact3);
        insert contacts;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));

        // Create Community User
        User communityUser2 = TestDataFactory.createUser(profileId, 'testSandbox2@qlikTech.com', 'tSbo2', String.valueOf(testContact2.Id));

        // Create Community User
        User communityUser3 = TestDataFactory.createUser(profileId, 'testSandbox3@qlikTech.com', 'tSbo3', String.valueOf(testContact3.Id));

        List<USer> users = new List<User>();
        Users.Add(communityUser);
        Users.Add(communityUser2);
        Users.Add(communityUser3);

        insert users;

        User supportProfileUser = [select Id, Name from User where Profile.Name LIKE 'Custom% Support Manager' AND IsActive = true limit 1];


        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;

        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;


        List<Case> caseObjList = new List<Case>();
        //for(integer i=0; i<2; i++) {
        // assigning the first 60 cases to community user 1 and the remaining 40 to community user 2
        caseObjList.add(TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                false, null, environment.Id,
                productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c));
        //}
        // Setting the first 25 cases as closed
        //for(integer i=0; i<3;i++) {
        //   caseObjList[0].Status = 'Closed';
        //}
        Semaphores.TriggerHasRun('CaseSalesOps', 1);
        Semaphores.TriggerHasRun('CaseEndUserAccountNameCopy', 1);
        Semaphores.TriggerHasRun('CaseQuickClose', 1);
        Semaphores.TriggerHasRun('CaseTrigger', 1);
        Semaphores.TriggerHasRun('createSolutions', 1);
        Semaphores.TriggerHasRun('CasePopulateSupportEmailFields', 1);
        Semaphores.TriggerHasRun('CaseBusinessCritical', 1);
        Semaphores.TriggerHasRun('CaseEntitlementProcessSync', 1);
        Semaphores.TriggerHasRun('CaseDetectLanguage', 1);
        Semaphores.TriggerHasRun('CaseUpdateSupportContact', 1);
        Semaphores.TriggerHasRun('CaseBusinessCriticalSupportTeamSetup', 1);
        Semaphores.TriggerHasRun('CaseClosedSendSurvey', 1);
        Semaphores.TriggerHasRun('CaseToCR', 1);
        Semaphores.TriggerHasRun('Case_Sharing', 1);

        caseObjList[0].Status = System.Label.QS_CloseCase;
        insert caseObjList;



        Test.startTest();
        //List<FeedItem> feedItemList = new List<FeedItem>();
        // adding a feed item to the closed case without any subscribers
        //feedItemList.add( TestDataFactory.createFeedItem('Test FeedItem 1', caseObjList[0].Id));
        // adding a feed item to an open case without any subsribers
        //feedItemList.add( TestDataFactory.createFeedItem('Test FeedItem 2', caseObjList[2].Id));
        //insert feedItemList;

        // Add few subscribers
        //EntitySubscription
        List<EntitySubscription> entitySubscriptionList = new List<EntitySubscription>();
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser.Id, caseObjList[0].Id));
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser3.Id, caseObjList[0].Id));

        insert entitySubscriptionList;

        // Can't run the following code as there are couple of triggers on case which are not bulkified
        //again try to insert a new chatter post on closed case
        //System.runAs(communityUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Community User 1', caseObjList[0].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values and if the status changed on closed case
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CaseFeedResponseExt_Status);
        //}

        //again try to insert a new chatter post
        //System.runAs(communityUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Community User 1', caseObjList[2].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[2].Id][0].Status, System.Label.QS_CaseFeedResponseExt_Status);
        //}

        //again try to insert a new chatter post on 'Ready to Close' case
        System.runAs(communityUser) {
            FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Community User 1', caseObjList[0].Id);
            fitem1.Visibility = 'AllUsers';
            insert fitem1;
            // Assertions to check the values and if the status changed on 'Ready to Close' case
            System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CloseCase);
        }

        // Can't run the following code as there are couple of triggers on case which are not bulkified
        //again try to insert a new chatter post
        // System.runAs(supportProfileUser) {
        //    FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Support', caseObjList[0].Id);
        //    fitem1.Visibility = 'AllUsers';
        //    insert fitem1;
        // Assertions to check the values
        //    System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CaseFeedResponse_Support_Status);

        //}
        //again try to insert a new chatter post
        //System.runAs(supportProfileUser) {
        //FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Support', caseObjList[2].Id);
        //fitem1.Visibility = 'AllUsers';
        //insert fitem1;
        // Assertions to check the values
        //System.assertEquals([Select Id, Status From Case where Id = :caseObjList[2].Id][0].Status, System.Label.QS_CaseFeedResponse_Support_Status);

        //}


        //again try to insert a new chatter post on 'Ready to Close' case
        System.runAs(supportProfileUser) {
            FeedItem fitem1 = TestDataFactory.createFeedItem('New Comment from Support', caseObjList[0].Id);
            fitem1.Visibility = 'AllUsers';
            insert fitem1;
            // Assertions to check the values and if the status changed on 'Ready to Close' case
            System.assertEquals([Select Id, Status From Case where Id = :caseObjList[0].Id][0].Status, System.Label.QS_CloseCase);
        }

        Test.stopTest();
    }

    static testMethod void test_internalFeedItem() {
        Test.startTest();
        QTTestUtils.GlobalSetUp();

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'leadSource';

        //Create Contact
        Contact testContact2 = TestDataFactory.createContact('test_FName', 'test_LName2', 'testSandbox2@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact2.Persona__c = 'Decision Maker';

        List<Contact> contacts = new List<Contact>();
        contacts.add(testContact);
        contacts.add(testContact2);
        insert contacts;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        List<User> communityList = new List<User>();
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        communityList.add(communityUser);
        User ownerUser = TestDataFactory.createUser(profileId, 'testowner@qlikTech.com', 'tow', String.valueOf(testContact2.Id));
        ownerUser.Email = 'ownerTest@test.com';
        communityList.add(ownerUser);
        insert communityList;

        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1102402020420');
        insert productLicense;

        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;

        Test.stopTest();

        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                false, null, environment.Id,
                productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        caseObj.Functional_Area__c = 'Integrations';
        caseObj.OwnerId = ownerUser.Id;
        caseObj.Assigned_To__c = ownerUser.Id;
        insert caseObj;

        Case toTest = [select Id, OwnerId from Case where Id =: caseObj.Id];
        System.assertEquals(ownerUser.Id, toTest.OwnerId);

        System.runAs(communityUser) {
            FeedItem fitem = TestDataFactory.createFeedItem('New internal Comment', caseObj.Id);
            fitem.Visibility = 'InternalUsers';
            List<FeedItem> fis = new List<FeedItem>();
            fis.add(fitem);
            QS_FeedItemTriggerhandler.emailCaseFollowers(fis);
        }

        Group newOwner = new Group(Name='test group name', type='Queue', Email = 'testQueue@test.com');
        insert newOwner;
        System.runAs(new User(Id=UserInfo.getUserId())) {
            QueuesObject testQueue = new QueueSObject(QueueID = newOwner.id, SObjectType = 'Case');
            insert testQueue;
        }
        caseObj.OwnerId = newOwner.Id;
        update caseObj;

        toTest = [select Id, OwnerId from Case where Id =: caseObj.Id];
        System.assertEquals(newOwner.Id, toTest.OwnerId);

        List<FeedItem> fis;
        System.runAs(communityUser) {
            fis = new List<FeedItem>();
            FeedItem fitem = TestDataFactory.createFeedItem('New one internal Comment', caseObj.Id);
            fitem.Visibility = 'InternalUsers';
            fis.add(fitem);
            QS_FeedItemTriggerhandler.emailCaseFollowers(fis);
        }
        fis = new List<FeedItem>();
        FeedItem fitem1 = TestDataFactory.createFeedItem('New one AllUsers Comment', caseObj.Id);
        fitem1.Visibility = 'AllUsers';
        fis.add(fitem1);

        QS_FeedItemTriggerhandler.emailCaseFollowers(fis);
    }

    // Get the networkId if not assigned to the user
    private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
                User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' and NetWork.name = 'support' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live' and NetWork.name = 'support') LIMIT 1];
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }

    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {

            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }


}