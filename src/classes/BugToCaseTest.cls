/**
* Test class for trigger: BugToCase
* Description: Populate/ unpopulate/ bug lookup field on Case record to point to the originating Bug when Bug is saved with Lookup value to a Case 
* (CR# 15361)
*
*   Change Log:
*   ------- 
*   VERSION AUTHOR      DATE        DETAIL
*                                            
*       1.  AIN         2014-09-05  Initial Development
*                                   CR# 15361 https://eu1.salesforce.com/a0CD000000k1d2G
*                                   Changes related to JIRA and SFdC integration
 */
@isTest

public with sharing class BugToCaseTest {
	public static TestMethod void testBugtoCase()
	{
		RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'Sales Ops Internal Apps' limit 1];
		Id pId = [Select Id From Profile Where Name Like 'System Administrator' Limit 1].Id; 
		User johnBrown = new User();
		johnBrown = [select Id from User where profileId=:pId and isActive=true limit 1];
		List<Bugs__c> testBugs = new List<Bugs__c>();
		List<Case> testCases = new List<Case>();			
		
		//creates cases		
		for(Integer i=0; i< 3; i++)
        {
        	Integer no = 50 +i;
        	testCases.add(new Case(RecordTypeId = caseRT.Id, Subject = 'Test Case N'+no, Status = 'Not Opened', Description = 'Desc: Test Case N'+no,    
            Category__c = 'Applications', Requester_Name__c = johnBrown.Id, Priority = 'Medium', SuppliedEmail = 'abc@abc.com'));        	        	        
        }

        insert testCases;

        //Creates bugs with a lookup to the cases created.
        for(Integer i=0; i< 3; i++)
        {
        	Integer no = 50 +i;
        	testBugs.add(new Bugs__c(Name = 'Test Bug N'+no, Product__c = 'TPT (Terradata)', LookupForPublisherActionToCase__c = testcases[i].id));
        }
		
		test.startTest();
		
		insert testBugs;
		
		List<Case> tCases = [Select Id, Bug__c from Case where Subject like 'Test Case N%' order by Subject];
		List<Bugs__c> tBugs = [Select Id from Bugs__c where Name like 'test Bug N%' order by Name];		
		
		//Assert that the case is pointing to the new bug
		for(Integer i=0; i<3; i++)
		{
			System.assertEquals(tCases[i].Bug__c, tBugs[i].Id);
		}
		
		test.stopTest();
	}
}