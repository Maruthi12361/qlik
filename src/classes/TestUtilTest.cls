/**

Dummy test class to cover TestUtils , which happens to be a utility class for Test Cases 
**/
@isTest
private class TestUtilTest {
    
    public static void CreateTestDate()
    {
        List<appirio_core__Config_Group__c> configGroups = new List<appirio_core__Config_Group__c>();
        configGroups.add(new appirio_core__Config_Group__c(Name = 'Currencies', appirio_core__Key__c = 'appirio_core.currencies', appirio_core__Version__c = '1'));
        insert configGroups;

        List<appirio_core__Config_Option__c> options = new List<appirio_core__Config_Option__c>();
        options.add(new appirio_core__Config_Option__c(Name = 'Use_Salesforce_Advanced_Multi_Currency', appirio_core__Config_Group__c = configGroups[0].Id, appirio_core__Allow_Multiple_Values__c = false, appirio_core__Type__c = 'Boolean', appirio_core__Version__c = '1.1', appirio_core__Help_Text__c = 'Bla', appirio_core__Description__c = 'Bla'));
        options.add(new appirio_core__Config_Option__c(Name = 'Use_Dated_Exchange_Rates', appirio_core__Config_Group__c = configGroups[0].Id, appirio_core__Allow_Multiple_Values__c = false, appirio_core__Type__c = 'Text', appirio_core__Version__c = '1', appirio_core__Help_Text__c = 'Bla', appirio_core__Description__c = 'Bla'));
        insert options;

        List<appirio_core__Config_Value__c> configs = new List<appirio_core__Config_Value__c>();
        configs.add(new appirio_core__Config_Value__c(appirio_core__Config_Option__c = options[0].Id, appirio_core__Value__c = 'false'));
        configs.add(new appirio_core__Config_Value__c(appirio_core__Config_Option__c = options[1].Id, appirio_core__Value__c = 'true'));
        insert configs;

        List<appirio_core__Currency__c> currencies = new List<appirio_core__Currency__c>();
        currencies.add(new appirio_core__Currency__c(Name = 'Australian Dollar', appirio_core__Currency_Code__c = 'AUD')); //0
        currencies.add(new appirio_core__Currency__c(Name = 'Brazillian Real', appirio_core__Currency_Code__c = 'BRL')); //1
        currencies.add(new appirio_core__Currency__c(Name = 'British Pound', appirio_core__Currency_Code__c = 'GBP')); //2
        currencies.add(new appirio_core__Currency__c(Name = 'Canadian Dollar', appirio_core__Currency_Code__c = 'CAD')); //3
        currencies.add(new appirio_core__Currency__c(Name = 'Danish Krone', appirio_core__Currency_Code__c = 'DKK')); //4
        currencies.add(new appirio_core__Currency__c(Name = 'Euro', appirio_core__Currency_Code__c = 'EUR')); //5
        currencies.add(new appirio_core__Currency__c(Name = 'Hong Kong Dollar', appirio_core__Currency_Code__c = 'HKD')); //6
        currencies.add(new appirio_core__Currency__c(Name = 'Indian Rupee', appirio_core__Currency_Code__c = 'INR')); //7
        currencies.add(new appirio_core__Currency__c(Name = 'Japanese Yen', appirio_core__Currency_Code__c = 'JPY')); //8
        currencies.add(new appirio_core__Currency__c(Name = 'Mexico Peso', appirio_core__Currency_Code__c = 'MXN')); //9
        currencies.add(new appirio_core__Currency__c(Name = 'Norwegian Krone', appirio_core__Currency_Code__c = 'NOK')); //10
        currencies.add(new appirio_core__Currency__c(Name = 'Polish Zloty', appirio_core__Currency_Code__c = 'PLN')); //11
        currencies.add(new appirio_core__Currency__c(Name = 'Russian Ruble', appirio_core__Currency_Code__c = 'RUB')); //12
        currencies.add(new appirio_core__Currency__c(Name = 'Singapore Dollar', appirio_core__Currency_Code__c = 'SGD')); //13
        currencies.add(new appirio_core__Currency__c(Name = 'Swedish Krona', appirio_core__Currency_Code__c = 'SEK')); //14
        currencies.add(new appirio_core__Currency__c(Name = 'Swiss Franc', appirio_core__Currency_Code__c = 'CHF')); //15
        currencies.add(new appirio_core__Currency__c(Name = 'US Dollar', appirio_core__Currency_Code__c = 'USD', appirio_core__Is_Corporate_Currency__c = true)); //16
        insert currencies;

        Date d2008 = Date.newInstance(2008, 1, 1);

        List<appirio_core__Currency_Exchange_Rate__c> currencyExchangeRates = new List<appirio_core__Currency_Exchange_Rate__c>();
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[0].Id, appirio_core__Rate__c = 1.282050));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[1].Id, appirio_core__Rate__c = 2.712890));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[2].Id, appirio_core__Rate__c = 0.666670));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[3].Id, appirio_core__Rate__c = 1.250000));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[4].Id, appirio_core__Rate__c = 5.500250));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[5].Id, appirio_core__Rate__c = 0.741000, appirio_core__Effective_Date__c = d2008));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[5].Id, appirio_core__Rate__c = 0.884960));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[6].Id, appirio_core__Rate__c = 8.000000));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[7].Id, appirio_core__Rate__c = 63.694270));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[8].Id, appirio_core__Rate__c = 118.203310));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[9].Id, appirio_core__Rate__c = 14.762330));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[10].Id, appirio_core__Rate__c = 7.806400));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[11].Id, appirio_core__Rate__c = 3.759540));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[12].Id, appirio_core__Rate__c = 0.018868));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[13].Id, appirio_core__Rate__c = 1.268550));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[14].Id, appirio_core__Rate__c = 6.750000, appirio_core__Effective_Date__c = d2008));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[14].Id, appirio_core__Rate__c = 8.276090));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[15].Id, appirio_core__Rate__c = 0.847460));
        currencyExchangeRates.add(new appirio_core__Currency_Exchange_Rate__c(appirio_core__Currency__c = currencies[16].Id, appirio_core__Rate__c = 1.000000));
        insert currencyExchangeRates;
    }
    public  static testmethod void testCreateBasicTestData() {
        CreateTestDate();
        TestUtil.createBasicTestData();
    }
}