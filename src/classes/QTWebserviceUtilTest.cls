/**********************************************************************
* Class QTWebserviceUtilTest
*
* Test class for the method QTWebserviceUtil
* 
* Change Log:
* 2014-06-04    AIN		Initial Development
*						CR# 7283 https://eu1.salesforce.com/a0CD000000U9yv8
* 2014-09-01	AIN 	Added tests for exception handling
**********************************************************************/
@isTest //(SeeAllData=true) 
public with sharing class QTWebserviceUtilTest {
	public static testmethod void test1() 
	{
		InsertQTWebserviceUtilCustomSetting();
		QTWebserviceUtil.SkipEndpointOrganizationValidation = true;
	/* UIN commented as part of BSL-449
		string orgName = 'qttest';
		string webserviceName = 'QTeCustoms';
		string endpoint = 'http://webservice.qliktech.com:7890/QTeCustoms/service1.asmx';
		string expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTeCustomsqttest/service1.asmx';
		DoTest(endpoint, webserviceName,  orgName, expectedNewEndpoint);

		orgName = 'rmtest';
		webserviceName = 'QTeCustoms';
		endpoint = 'http://webservice.qliktech.com:7890/QTeCustomsqttest/service1.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTeCustomsrmtest/service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		orgName = 'refreshest';
		webserviceName = 'QTeCustoms';
		endpoint = 'http://webservice.qliktech.com:7890/QTeCustomsqttest/service1.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTeCustomsrefreshest/service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		orgName = 'corpsvcs';
		webserviceName = 'QTeCustoms';
		endpoint = 'http://webservice.qliktech.com:7890/QTeCustomsrefreshest/service1.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTeCustomscorpsvcs/service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint); 
	UIN commented as part of BSL-449	
		*/
		
		string orgName = 'refreshest';
		string webserviceName = 'QTException';
		string endpoint = 'http://webservice.qliktech.com:7890/QTException/service1.asmx';
		string expectedNewEndpoint = 'http://webservice.qliktech.com:7890/QTException/service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		orgName = 'qttest';
		webserviceName = 'QTException';
		endpoint = 'http://webservice.qliktech.com:7890/QTExceptionqttest/service1.asmx';
		expectedNewEndpoint = 'http://webservice.qliktech.com:7890/QTExceptionqttest/service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		/*
		//Sandbox specific settings test
		orgName = 'Test1';
		webserviceName = 'QTeCustoms';
		endpoint = 'http://webserviceAIN.qliktech.com:7890/QTException/service1.asmx';
		expectedNewEndpoint = 'http://QTeCustoms.qliktech.com/service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		orgName = 'Test2';
		webserviceName = 'QTeCustoms';
		endpoint = 'http://webserviceAIN.qliktech.com:7890/QTException/service1.asmx';
		expectedNewEndpoint = 'http://Test2.this.is.a.test/service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		//QTeCustoms test 

		orgName = 'qttest';
		webserviceName = 'QTeCustoms';
		endpoint = 'http://webserviceAIN.qliktech.com:7890/QTException/service1.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTeCustomsQTTest/Service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		orgName = 'refreshest';
		webserviceName = 'QTeCustoms';
		endpoint = 'http://webserviceAIN.qliktech.com:7890/QTException/service1.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTeCustomsrefreshest/Service1.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);
		
		*/

		//QTVoucher test
		orgName = 'Test';
		webserviceName = 'QTVoucher';
		endpoint = 'http://testwebservices.qliktech.com/QTVoucherTest/Service.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTVoucherTest/Service.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		orgName = 'qttest';
		webserviceName = 'QTVoucher';
		endpoint = 'http://webservice.qliktech.com:7890/QTVoucher/Service.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/QTVoucherQTTest/Service.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		//ULC Test
		orgName = 'qttest';
		webserviceName = 'ULCv3';
		endpoint = 'http://testwebservices.qliktech.com/ULCv3qttest/Service.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/ULCv3qttest/Service.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);

		orgName = 'qttest';
		webserviceName = 'ULCv3';
		endpoint = 'http://webservice.qliktech.com:7890/ULCv3Live/Service.asmx';
		expectedNewEndpoint = 'http://testwebservices.qliktech.com/ULCv3qttest/Service.asmx';
		DoTest(endpoint, webserviceName, orgName, expectedNewEndpoint);


		//For test coverage
		sfUtilsQlikviewCom.ServiceSoap service1 = new sfUtilsQlikviewCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service1);

		sharinghandlerQlikviewCom.ServiceSoap service2 = new sharinghandlerQlikviewCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service2);

		efQTPaymentGateway.ServiceSoap service3 = new efQTPaymentGateway.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service3);

		ulcv3QlikviewCom.ServiceSoap service4 = new ulcv3QlikviewCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service4);

		qtvoucherQliktechCom.ServiceSoap service5 = new qtvoucherQliktechCom.ServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service5);

		/* UIN commented as part of BSL-449
		qtecustomsQliktechCom.Service1Soap service6 = new qtecustomsQliktechCom.Service1Soap();
		QTWebserviceUtil.SetWebServiceEndpoint(service6);
		*/
		QtStrikeironCom.AddressValidationServiceSoap service7 = new QtStrikeironCom.AddressValidationServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service7);
		
		/* UIN commented as part of BSL-449
		qtecustomsq2cw.eCustomsServiceSoap service8 = new qtecustomsq2cw.eCustomsServiceSoap();
		QTWebserviceUtil.SetWebServiceEndpoint(service8);
		*/
		QTWebserviceUtil.GetEndpoint('endpoint', null, 'sandbox');

		QTWebserviceUtil.GetEndpoint('endpoint', 'webserviceName', '');

		QTWebserviceUtil.SkipEndpointOrganizationValidation = false;
		QTWebserviceUtil.GetEndpoint('endpoint', 'webserviceName', '');
	}

	private static void InsertQTWebserviceUtilCustomSetting()
	{
		List<QTWebserviceUtil__c> newSettings = new List<QTWebserviceUtil__c>();
		
		QTWebserviceUtil__c newSetting;
/*
		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'QTeCustomsTest3';
		newSetting.Sandbox__c = 'Test3';
		newSetting.WebserviceName__c = 'QTeCustoms';
		newSetting.Endpoint__c = 'http://{sandbox}.this.is.a.test/service1.asmx';
		newSettings.add(newSetting);

		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'QTeCustomsAll';
		newSetting.Sandbox__c = '*';
		newSetting.WebserviceName__c = 'QTeCustoms';
		newSetting.Endpoint__c = 'http://testwebservices.qliktech.com/QTeCustoms{sandbox}/service1.asmx';
		newSettings.add(newSetting);

		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'QTeCustomsTest1';
		newSetting.Sandbox__c = 'Test1';
		newSetting.WebserviceName__c = 'QTeCustoms';
		newSetting.Endpoint__c = 'http://QTeCustoms.qliktech.com/service1.asmx';
		newSettings.add(newSetting);

		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'QTeCustomsTest2';
		newSetting.Sandbox__c = 'Test2';
		newSetting.WebserviceName__c = 'QTeCustoms';
		newSetting.Endpoint__c = 'http://{sandbox}.this.is.a.test/service1.asmx';
		newSettings.add(newSetting);
*/
		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'QTVoucherAll';
		newSetting.Sandbox__c = '*';
		newSetting.WebserviceName__c = 'QTVoucher';
		newSetting.Endpoint__c = 'http://testwebservices.qliktech.com/QTVoucher{sandbox}/Service.asmx';
		newSettings.add(newSetting);

		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'ULCv3All';
		newSetting.Sandbox__c = '*';
		newSetting.WebserviceName__c = 'ULCv3';
		newSetting.Endpoint__c = 'http://testwebservices.qliktech.com/ULCv3{sandbox}/Service.asmx';
		newSettings.add(newSetting);
		 
		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'sfUtilsAll';
		newSetting.Sandbox__c = '*';
		newSetting.WebserviceName__c = 'SFUtils';
		newSetting.Endpoint__c = 'http://testwebservices.qliktech.com/sfUtils{sandbox}/service.asmx';
		newSettings.add(newSetting);

		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'SFDCSharingHandlerAll';
		newSetting.Sandbox__c = '*';
		newSetting.WebserviceName__c = 'SFDCSharingHandler';
		newSetting.Endpoint__c = 'http://testwebservices.qliktech.com/SFDCSharingHandler{sandbox}/service.asmx';
		newSettings.add(newSetting);

		newSetting = new QTWebserviceUtil__c();
		newSetting.Name = 'efQTPaymentGatewayAll';
		newSetting.Sandbox__c = '*';
		newSetting.WebserviceName__c = 'efQTPaymentGateway';
		newSetting.Endpoint__c = 'https://qtpgwstaging.qliktech.com/Service.asmx';
		newSettings.add(newSetting);

		insert newSettings;
	}
	private static void DoTest(string endPoint, string webservicename, string sandbox, string expectedNewEndpoint)
	{

		string newEndpoint = QTWebserviceUtil.GetEndpoint(endpoint, webservicename, sandbox);
		system.debug('Endpoint: ' + endpoint);
		system.debug('NewEndpoint: ' + newEndpoint);
		system.debug('ExpectedNewEndpoint: ' + expectedNewEndpoint);
		system.assertEquals(expectedNewEndpoint.toLowercase(), newEndpoint.toLowercase());

	}
}