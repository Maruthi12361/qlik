/*
----------------------------------------------------------------------------
|  Class: BugWeightSys_TestFactory
|
|  Filename: BugWeightSys_TestFactory.cls
|
|  Author: Anders Nehlin, Fluido Sweden AB
|
|  Description:
|    Class for generating test objects.
|
| Change Log:
| 2013-11-10  AndNeh  Initial Development
| 2013-11-21  PetFri  Commenting and corrections
|                     Now loads slaProcesses from database
|                     Renamed from TestFactory.cls to BugWeightSys_TestFactory.cls
| 2015-11-11  NAD     Updated support level names per CR# 57556
| 2016-02-19  NAD     Removed Support_Level__c and Support_Per__c references per CR# 33068 (Responsible Partner change)
----------------------------------------------------------------------------
*/

public class BugWeightSys_TestFactory {

    // Returns a list of valid mock bugs
    public static List<Bugs__c> buildTestBugs(Integer count) {    
        List<Bugs__c> bugs = new List<Bugs__c>();
        for(Integer i = 0; i < count; i++) {
            Bugs__c bug = new Bugs__c();
            bug.name = '1' + String.valueOf(i);
            Integer k = math.mod(i,3) + 1;
            bug.severity__c = String.valueOf(k);
            bug.status__c = 'Open';
            bug.bug_id__c = '1' + String.valueOf(i);
            bug.bug_weight__c = 30 + 30 * i;
            bug.currencyIsoCode = 'SEK';
            bug.number_of_cases__c = 3;
            bug.service_class__c = 'sadfa';
            bugs.add(bug);  
        }
        return bugs;
    }
    
    // Returns a list of valid mock SLA Processess
    public static List<SlaProcess> buildTestSlaProcesses() { 
        List<SlaProcess> slaProcesses = new List<SlaProcess>([
            SELECT  id, name, description, isactive, startdatefield, createddate,
                    createdbyid, lastmodifieddate, lastmodifiedbyid, systemmodstamp
            FROM    SlaProcess
            WHERE   isactive = true]);
        System.debug('SLA PROCESS=' + slaProcesses);
        return slaProcesses;
    }
    
    // Returns a list of valid mock Entitlements
    public static List<Entitlement> buildTestEntitlements(Integer count, 
                                                          List<Account> accs,
                                                          List<Account_License__c> accLics,
                                                          List<SlaProcess> slas) {  
        List<Entitlement> entitlements = new List<Entitlement>();
        for(Integer i = 0; i < count; i++) {
            Entitlement ent = new Entitlement();
            ent.name = 'Product License_' +  String.valueOf(i);
            ent.accountId = i < accs.size() ? accs[i].id : accs[0].id;
            ent.account_license__c = i < accLics.size() ? accLics[i].id : accLics[0].id;
            ent.slaProcessid = i < slas.size() ? slas[i].id : slas[0].id;
            ent.startdate = Date.today().addDays(-5);
            ent.enddate = Date.today().addDays(5);
            entitlements.add(ent);
        }
        return entitlements;
    }
    
    // Returns a list of valid mock Account_License__c
    public static List<Account_License__c> buildTestAccountLicenses(Integer count, List<Account> accs) {    
        List<Account_License__c> accLicenses = new List<Account_License__c>();
        for(Integer i = 0; i < count; i++) {
            Account_License__c accLic = new Account_License__c();
            accLic.account__c = accs[math.mod(i,2)].id;
            accLic.name = 'AccLic_' + String.valueOf(i);
            accLic.session_cal__c = 1;
            accLic.named_user_cal__c = 1;
            accLic.document_cal__c = 1;
            accLic.usage_cal__c = 1;
            accLic.uncapped__c = true;
            accLicenses.add(accLic);  
        }
        return accLicenses;
    }

    // Returns a list of valid mock accounts
    public static List<Account> buildTestAccounts(Integer count) {    
        List<Account> accounts = new List<Account>();
        for(Integer i = 0; i < count; i++) {
            Account account = new Account();
            account.name = 'Company_' + String.valueOf(i);
            account.qlikTech_company__c = 'QlikTech Nordic AB';
            //account.support_level__C = math.mod(i,2)==0 ? 'Basic' : 'Enterprise';
            accounts.add(account);  
        }
        return accounts;
    }

    // Returns a list of valid mock contacts
    public static List<Contact> buildTestContacts(Integer count,  List<Account> accounts) {
        
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < count; i++) {
            Contact contact = new Contact();
            contact.firstName = 'Nisse_' + String.valueOf(i);
            contact.lastName = math.mod(i,2)==0 ? 'Jansson_' + String.valueOf(i) : 'Carlsson_' + String.valueOf(i);
            contact.phone = '+4612345698' + String.valueOf(i);
            contact.accountId = i < accounts.size() ? accounts.get(i).id :accounts.get(0).id ;  
            contacts.add(contact);
        }
        return contacts;
    }

    // Returns a list valid mock Cases
    public static List<Case> buildTestCases(Integer count,
                                            List<Entitlement> entitlements, 
                                            BusinessHours bh,
                                            List<Bugs__c> bugs) {
      
        List<Case> cases = new List<Case>();
        for(Integer i = 0; i < count; i++) {
            Case c = new Case();
            c.origin = 'Email';
            Integer k = math.mod(i,3) + 1;
            c.severity__c = String.valueOf(k);
            c.businessHours = bh;
            c.status = 'New';
            c.entitlementId = i < entitlements.size() ? entitlements[i].id : entitlements[0].id;
            c.subject = 'SUBJECT';
            c.description = 'THIS IS A DESCRIPTION';
            c.bug__c = i < bugs.size() ? bugs[i].id : bugs[0].id; 
            cases.add(c);
        } 
        return cases;
    }
}