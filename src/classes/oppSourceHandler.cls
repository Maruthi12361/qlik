/************************************************************************
 * @author  UIN
 * 2017-03-10 Partner Program Project
 *  
 * Added logic to set opportunity source on opportunity creation only.
 * MTM  2017-06-22 QCW-2711 remove sharing option
 * 2019-11-27 offloaded user query to user helper to limit soql queries per transaction 
 *************************************************************************/
public class oppSourceHandler {
	public oppSourceHandler() {
		
	}

	// to be called on both events before Insert and before Update
	public static void handle(List<Opportunity> triggerNew, Boolean isInsert) {
		List<Opportunity> opps = new List<Opportunity>();

		User lUser = UserHelper.getExtendedUser(); 
		for (Opportunity o:triggerNew){
			if(String.isBlank(o.Opportunity_Source__c))
				opps.add(o);
		}

		for(Opportunity opp: opps){
			if('PRM'.equalsIgnoreCase(lUser.Function__c))
				opp.Opportunity_Source__c = 'Partner Created';
			else
				opp.Opportunity_Source__c = 'Qlik Created';
		}

	}	
}