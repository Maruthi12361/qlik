/******************************************************

    Class: LeadAcademicProgramConversionHandler
    
	Description: On lead conversion for RecordType Lead Academic Program,
                an Academic Program Account Record Type and an Academic Program Opp Record Type should be created.

    Changelog:
        2012-10-18  Fluido  Initial Development CR#6184 https://eu1.salesforce.com/a0CD000000NK6am?srPos=0&srKp=a0C
        2012-11-16  Fluido  Added null-check for settings
        2013-02-28  Fluido  Added academic pricebook for converted opportunities
        2013-03-06  RDZ     Adding a check to update only if we have acc or opp to update.
        2014-11-18  Fluido  Added Academic Contact Name field to be set in case of Academic Opportunities (CR-13357)
        2015-11-20  NAD     Replaced opportunity stage name 'Academic Program - Open' with 'AP - Open' per CR 66252
        2017-04-10  Linus Löfberg (4front)  Added check in Semaphores.cls preventing loops.
        2017-10-25  AYS BMW-402 : Migrated from LeadAcademicProgramConversion.trigger.
        2018-06-25  AYS IT-202  : CR# 51988: Academic Program Lead conversion should include billing contact
        2019-04-05  extbad IT-1692 Converted Contact should have Academic_Contact_Name__c for existing accounts also
******************************************************/
public class LeadAcademicProgramConversionHandler {
    private static final String ACADEMIC_PROGRAM_SETTINGS = 'AcademicProgramSetting';
    private static final String BILLING_ADDRESS_TYPE = 'Billing';
    private static final String SHIPPING_ADDRESS_TYPE = 'Shipping';
    private static final String ACADEMIC_PROGRAM_REVENUE_TYPE = 'Academic Program';
    private static final String STAGE_NAME = 'AP - Open';
    private static final String EXISTING_CUSTOMER_TYPE = 'Existing Customer';

    public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
        if (!Semaphores.LeadAcademicProgramConversion_HasRunAfter) {
            Semaphores.LeadAcademicProgramConversion_HasRunAfter = true;

            Academic_Program_Settings__c settings = Academic_Program_Settings__c.getInstance(ACADEMIC_PROGRAM_SETTINGS);
            Set<Id> convAccountIds = new Set<Id>();
            Set<Id> convOppoIds = new Set<Id>();
            Map<Id, Id> oppToContMap = new Map<Id, Id>(); // <Opportunity.Id, Contact.Id>
            Map<Id, Account> convAccsMap = new Map<Id, Account>();
            Map<Id, Id> oppToAccMap = new Map<Id, Id>(); // <Opportunity.Id, Account.Id>

            List<Account> convAccsToUpdate = new List<Account>();
            List<Opportunity> convOppos = new List<Opportunity>();
            List<Opportunity> convOpposToUpdate = new List<Opportunity>();
            List<Address__c> addressesToInsert = new List<Address__c>();


            if (settings != null && settings.LeadRecordTypeId__c != null) {

                for (Integer i = 0; i < triggerNew.size(); i++) {
                    if (triggerNew[i].RecordTypeId == settings.LeadRecordTypeId__c) {
                        if ((triggerNew[i].IsConverted && !triggerOld[i].isConverted)) {
                            convAccountIds.add(triggerNew[i].ConvertedAccountId);
                            convOppoIds.add(triggerNew[i].ConvertedOpportunityId);
                            oppToContMap.put(triggerNew[i].ConvertedOpportunityId, triggerNew[i].ConvertedContactId);
                            oppToAccMap.put(triggerNew[i].ConvertedOpportunityId, triggerNew[i].ConvertedAccountId);
                        }
                    }
                }

                if (!convAccountIds.isEmpty()) {
                    convAccsMap = new Map<Id, Account>(
                        [SELECT Id, RecordTypeId, CreatedDate FROM Account WHERE Id IN :convAccountIds]
                    );

                    for (Lead lead : triggerNew) {
                        if (convAccsMap.containsKey(lead.ConvertedAccountId)) {
                            Account linkedAccount = convAccsMap.get(lead.ConvertedAccountId);
                            if (isAccountJustCteated(linkedAccount.CreatedDate)) {

                                if (lead.Street != null || lead.City != null || lead.State != null ||
                                        lead.PostalCode != null || lead.Country != null) {

                                    Address__c addressBilling = new Address__c();
                                    addressBilling.Account__c = lead.ConvertedAccountId;
                                    addressBilling.Address_1__c = lead.Street;
                                    addressBilling.City__c = lead.City;
                                    addressBilling.State_Province__c = lead.State;
                                    addressBilling.Zip__c = lead.PostalCode;
                                    if (lead.ConvertedContactId != null) {
                                        addressBilling.Address_Contact__c = lead.ConvertedContactId;
                                    }
                                    addressBilling.Address_Type__c = BILLING_ADDRESS_TYPE;
                                    addressBilling.Country__c = lead.Country;
                                    addressBilling.CurrencyIsoCode = lead.CurrencyIsoCode;
                                    addressBilling.Default_Address__c = true;

                                    Address__c addressShipping = addressBilling.clone(false, true, false, false);
                                    addressShipping.Address_Type__c = SHIPPING_ADDRESS_TYPE;

                                    addressesToInsert.add(addressBilling);
                                    addressesToInsert.add(addressShipping);
                                }
                            }
                        }
                    }
                }

                if (!convOppoIds.isEmpty()) {
                    convOppos = [SELECT Id, RecordTypeId, Academic_Contact_Name__c FROM Opportunity WHERE Id IN :convOppoIds];
                }

                for (Account a : convAccsMap.values()) {
                    if (!a.RecordTypeId.equals(settings.AccountRecordTypeId__c)) {
                        a.RecordTypeId = settings.AccountRecordTypeId__c;
                        convAccsToUpdate.add(a);
                    }
                }

                for (Opportunity o : convOppos) {
                    String accountId = oppToAccMap.get(o.Id);
                    Account linkedAccount = convAccsMap.get(accountId);
                    if (!isAccountJustCteated(linkedAccount.CreatedDate)) {
                        o.Type = EXISTING_CUSTOMER_TYPE;
                    }

                    o.RecordTypeId = settings.OpportunityRecordTypeId__c;
                    o.Revenue_Type__c = ACADEMIC_PROGRAM_REVENUE_TYPE;
                    o.StageName = STAGE_NAME;
                    o.Academic_Contact_Name__c = oppToContMap.get(o.Id); // Lookup(Contact)
                    convOpposToUpdate.add(o);
                }

                if (addressesToInsert.size() > 0) {
                    insert addressesToInsert;
                }

                try {
                    if (convAccsToUpdate.size() > 0) {
                        update convAccsMap.values();
                    }
                    if (convOpposToUpdate.size() > 0) {
                        update convOppos;
                    }
                } catch (Exception e) {
                    System.debug(e);
                }
            }
        }
    }

    private static Boolean isAccountJustCteated(DateTime accountCreatedTime) {
        Datetime nowTime = DateTime.now();
        DateTime startTimeFrame = nowTime.addSeconds(-60);
        return accountCreatedTime > startTimeFrame;
    }

}