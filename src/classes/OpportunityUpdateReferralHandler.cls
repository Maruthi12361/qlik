/****************************************************************
*
*  OpportunityUpdateReferralHandler
*
*  2016-05-19 TJG : Migrated from the following trigger
					PRMReferral_OpportunityUpdateReferral 
*  2017-06-22 MTM  QCW-2711 remove sharing option
*  2019-12-09 FEH : Updated to prevent double querying contacts when running after insert and update in the same transaction 
*****************************************************************/
public class OpportunityUpdateReferralHandler {
	

    private static List<Contact> contacts;
    
    private static List<Contact> getContacts(List<Id> ids) {
        if(contacts == null) {
          contacts = [Select Id, OwnerId, Owner.Name, AccountId, Account.Name, Account.Owner.Name, Account_Type__c, Account.Qonnect_Manager__r.Name, Account.Qonnect_Manager__r.Id FROM Contact WHERE Id In :Ids];
        }
        
        return contacts;
    }    
    
    // to be called on both events before Insert and before Update
	public static void handle(Map<Id, Opportunity> triggerOldMap, List<Opportunity> triggerNew, Boolean isInsert, Boolean isUpdate) {
	    system.debug('OpportunityUpdateReferralHandler: Starting');

        
	    List<String> Ids = new List<String>();
	    List<Id> AccountIds = new List<Id>();
	    for (Opportunity o:triggerNew)
	    {
	        ids.add(o.PRM_Referring_Contact__c);      
	    }
	    
	    if(Ids.size()>0)
	    {
	        Map<Id, Contact> contactsMap = New Map<Id, Contact>(getContacts(ids));
	        
	        Boolean UpdatePRMDetailsFlag;
	        
	        for (Opportunity o:triggerNew)
	        {
	            UpdatePRMDetailsFlag=false;
	            if(isInsert)
	            {
	                if (o.PRM_Referring_Contact__c != null )
	                {                   
	                    UpdatePRMDetailsFlag = true;
	                }
	            }
	            if(isUpdate)
	            {
	                Opportunity oldPRMContact = triggerOldMap.get(o.id);
	                    
	                if (o.PRM_Referring_Contact__c != null &&  
	                (oldPRMContact.PRM_Referring_Contact__c != o.PRM_Referring_Contact__c ||
	                 oldPRMContact.StageName != o.StageName))
	                {
	                    UpdatePRMDetailsFlag = true;    
	                }
	            }           
	                    
	            if (UpdatePRMDetailsFlag == true)
	            {
	                system.debug('OpportunityUpdateReferralHandler: PRM Referring contact Id = ' + o.PRM_Referring_Contact__c);
	                
	                Contact pContact = contactsMap.get(o.PRM_Referring_Contact__c);
	                
	                system.debug('OpportunityUpdateReferralHandler: Partner contact is ' + pContact);
	                
	                o.PRM_Id_Referring_PSM_QM__c = pContact.OwnerId; // SAN CR#6037
	                // o.PRM_Referring_PSM_QM__c = pContact.Owner.Name;
	                o.PRM_Referring_PSM_QM__c = pContact.Account.Owner.Name; // TJG CR#7332
	                o.PRM_Referring_Partner__c = pContact.Account.Name; //AccountId
	                o.PRM_Id_Referring_Partner__c = pContact.AccountId; //CCE CR#6703
	                if((o.StageName == 'Closed Won') || (o.StageName == 'OEM - Closed Won') || (o.StageName == 'OEM Pre-Pay Closed Won') || (o.StageName == 'Deal Split-closed Won') || (o.StageName == 'ER - Closed Won') || (o.StageName == 'QSG Closed Won'))
	                {
	                    System.Debug('MTM Referring_Qonnect_Manager_at_Closed_Won__c = ' + o.Referring_Qonnect_Manager_at_Closed_Won__c);
	                    o.Referring_Qonnect_Manager_at_Closed_Won__c = pContact.Account.Qonnect_Manager__r.Name;
	                    o.Referring_Qonnect_Manager_Closed_Won_ID__c = pContact.Account.Qonnect_Manager__r.Id;      
	                }      
	            }           
	            if (o.PRM_Referring_Contact__c == null) 
	            {
	                system.debug('OpportunityUpdateReferralHandler: Remove partner and PSM/QM when contact is blank/deleted SAN(CR6164)');
	                o.PRM_Id_Referring_PSM_QM__c = null;
	                o.PRM_Referring_PSM_QM__c = null;
	                o.PRM_Referring_Partner__c = null;
	                o.PRM_Id_Referring_Partner__c = null;          
	            }
	        }   
	    }
	    system.debug('OpportunityUpdateReferralHandler: Finishing');		
	}
}