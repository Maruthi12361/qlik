/*
    Initial development: MTM
    2015-02-06  MTM CR# 19375  AlertOnKeyExpiry.
    2016-04-14  IRN CR33068 added test to get a better code coverage
	29.03.2017 Rodion Vakulvoskyi fix test failures commenting QuoteTestHelper.createCustomSettings();
*/
@isTest
public class AlertOnKeyExpiryTest {
    static testMethod void AlertOnKeyExpiryBatch()
    {
        test.StartTest();
        AlertOnKeyExpiry schedule = new AlertOnKeyExpiry(2);        
        test.stopTest();
    }
    
    static testMethod void testAlertOnKeyExpiry() 
    {
        CreateAccountLicense();
        test.StartTest();
        AlertOnKeyExpiry batch = new AlertOnKeyExpiry(2);        
        ID batchId = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    public static Account_License__c CreateAccountLicense()
    {        
        Integer numberOfDays =  2 * 7;       
        date expiryDate = Date.today().addDays(numberOfDays);
        User aUser = QTTestUtils.createMockOperationsAdministrator();
        Account acc = QTTestUtils.createMockAccount('AlertOnAccountLicense expiry Test Account', aUser);
        Account_License__c license = new Account_License__c(
                                Name = '123456789',
                                CurrencyIsoCode = 'GBP',
                                Time_Limit__c = expiryDate,
                                Special_Edition__c = 'EVALUATION'
                                );
        insert license;
        return license;
    }

    static testMethod void testExcecuteMethod()
    {   
        QTTestUtils.GlobalSetUp();
       // QuoteTestHelper.createCustomSettings();
        Test.startTest();
        AlertOnKeyExpiry batch = new AlertOnKeyExpiry(2);
        List<Account_License__c> licenseList = new List<Account_License__c>();
        
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        u.Email = 'teat@gmail.com';
        u.FirstName = 'Anna';
        update u;

        Opportunity testNewOpp = QTTestUtils.createMockOpportunity('Test AlertOnKeyExpiry', 'TestOpp - Delte me',  'New Customer', 'Direct', 'Alignment Meeting', '01220000000DNwY', 'USD', u, true);
        Opportunity testNewOpp2 = QTTestUtils.createMockOpportunity('Test AlertOnKeyExpiry', 'TestOpp - Delte me',  'OEM deal', 'OEM deal', 'Alignment Meeting', '01220000000DNwY', 'USD', u, true);
        Opportunity testNewOpp3 = QTTestUtils.createMockOpportunity('Test AlertOnKeyExpiry', 'TestOpp - Delte me',  'New Customer', 'Direct', 'Alignment Meeting', '01220000000DNwY', 'USD', u, true);
        
        Account acc = QTTestUtils.createMockAccount('sName', u, true);
        acc.Owner = u;
        update acc;
        system.debug('ddd account owner ' + acc.Owner);
        system.debug('ddd account owner email' + acc.Owner.Email);
        testNewOpp.Sell_Through_Partner__c  = acc.Id;
        
        Account_License__c license1 = new Account_License__c();
        license1.Name = 'test1';
        license1.Opportunity__r = testNewOpp;
        license1.Time_Limit__c = Date.today().addDays(10);
        licenseList.add(license1);
        Account_License__c license2 = new Account_License__c();
        license2.Name = 'test2';
        license2.Opportunity__r = testNewOpp2;
        license2.Time_Limit__c = Date.today().addDays(10);
        licenseList.add(license2);
        Account_License__c license3 = new Account_License__c();
        license3.Name = 'test3';
        license3.Opportunity__r = testNewOpp3;
        license3.Time_Limit__c = Date.today().addDays(10);
        licenseList.add(license3);
        insert licenseList;  
        
        batch.execute(null, licenseList);
        Test.stopTest();
    }

    static testMethod void testFromAddress(){
         Test.startTest();
         AlertOnKeyExpiry a = new AlertOnKeyExpiry(2);
         System.assertEquals('0D2D0000000GndJKAS', a.FromAddress);
         Test.stopTest();
    }
}