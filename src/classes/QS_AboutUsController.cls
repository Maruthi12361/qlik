/********************************************************
* QS_AboutUsController.cls
* Description:
*
* Change Log:
* 2016-04-13    NAD         Removed Responsible_Partner__r reference and replaced Contact.Account.Responsible_Partner__c reference with Responsible_Partner_count__c per CR# 33068
* 2018-02-28    ext_vos     CHG0032292: Add isGuestUser variable for "Free Webinars" block.
* 2018-11-13    ext_vos     CHG0034901 - Outdated page. Add redirect to 'Other Resources' page.
* 2019-05-05    AIN         IT-1597 Updated for Support Portal Redesign
**********************************************************/
public with sharing class QS_AboutUsController {
    public Boolean isPartnerAccount {get; set;}
    public boolean isCustomer {get; set;}
    public boolean isGuestUser {get; set;}
    
    public QS_AboutUsController() {
    }

    public PageReference redirectToActualPage() {
        PageReference retURL = new PageReference('/QS_Home_Page');
        retURL.setRedirect(true);
        return retURL;
    }
}