/**
*   Test for trigger handler TaskAffectParentObjectsHandler
*
 *  TJG 2014-04-12  Fixed all the failling test methods due to the breaking of custom validation rules.
 *  RDZ 2014-11-18  Fixed test class to ensure campaing name is unique.
 */
@isTest
private class TestTaskTouchParentObject {

    static Id makeAccount() {
     Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
        insert QTComp;
        Account a = new Account(Name='TestAccount', BillingCountry='United Kingdom', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c=QTComp.Id);
        insert a;
        return a.Id;
    }

    static testMethod void testTaskOnAccount() {

        QTTestUtils.GlobalSetUp();

        Id account = makeAccount();
        Task t = new Task(WhatId = account,Subject='Test',CallType='Inbound', Type='TK - Call (Inbound)');
        insert t;

        Account l = [select Last_Activity_Datetime__c from Account where Id=:account];
        System.assert(l.Last_Activity_Datetime__c!=null,'Last_Activity_Datetime__c on Account should be filled');
    }

    static Id makeLead() {
        Lead l = new Lead(FirstName='Test',LastName='Lead',Company='Test', Email='tst@bbc.com', Country='United Kingdom');   // Country_Code__c='a0AL0000000bxhl');
        insert l;
        return l.Id;
    }

    static testMethod void testTaskOnLead() {

        QTTestUtils.GlobalSetUp();

        Id lead = makeLead();
        Task t = new Task(WhoId = lead,Subject='Test',CallType='Inbound', Type='TK - Call (Inbound)');
        insert t;

        Lead l = [select Last_Activity_Datetime__c from Lead where Id=:lead];
        System.assert(l.Last_Activity_Datetime__c!=null,'Last_Activity_Datetime__c on Lead should be filled');
    }
    
    static Id makeContact() {
        Id account = makeAccount();
        Contact l = new Contact(FirstName='Test',LastName='Lead',AccountId=account);
        insert l;
        return l.Id;
    }

    static testMethod void testTaskOnContact() {

        QTTestUtils.GlobalSetUp();

        Id newObj = makeContact();
        Task t = new Task(WhoId = newObj,Subject='Test',CallType='Inbound',Type='Other');
        insert t;

        Contact l = [select Last_Activity_Datetime__c from Contact where Id=:newObj];
        System.assert(l.Last_Activity_Datetime__c!=null,'Last_Activity_Datetime__c on Contact should be filled');
    }
    
    static Id makeOpportunity() {

        Id account = makeAccount();
        Schema.DescribeFieldResult stageDesc = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> stages = stageDesc.getPicklistValues();
        Schema.PicklistEntry firstStage = stages.get(1);
        Opportunity l = new Opportunity(Name='Test',AccountId=account,CloseDate=System.Today(),StageName=firstStage.getValue());
        insert l;
        return l.Id;
    }

    static testMethod void testTaskOnOpportunity() {

        QTTestUtils.GlobalSetUp();

        Id newObj = makeOpportunity();
        Task t = new Task(WhatId = newObj,Subject='Test',CallType='Inbound', Type='TK - Call (Inbound)');
        insert t;

        Opportunity l = [select Last_Activity_Datetime__c from Opportunity where Id=:newObj];
        System.assert(l.Last_Activity_Datetime__c!=null,'Last_Activity_Datetime__c on Opportunity should be filled');
    }
    
    static Id makeCampaign() {
        Campaign l = new Campaign(Name='Test'+System.now().millisecond());
        insert l;
        return l.Id;
    }

    static testMethod void testTaskOnCampaign() {

        QTTestUtils.GlobalSetUp();

        Id newObj = makeCampaign();
        Task t = new Task(WhatId = newObj,Subject='Test',CallType='Inbound', Type='TK - Call (Inbound)');
        insert t;

        Campaign l = [select Last_Activity_Datetime__c from Campaign where Id=:newObj];
        System.assert(l.Last_Activity_Datetime__c!=null,'Last_Activity_Datetime__c on Campaign should be filled');
    }
    
    static Id makeContract() {
        Id account = makeAccount();
        Contract l = new Contract(AccountId=account,StartDate=System.Today(),ContractTerm=12);
        insert l;
        return l.Id;
    }

    static testMethod void testTaskOnContract() {

        QTTestUtils.GlobalSetUp();

        Id newObj = makeContract();
        Task t = new Task(WhatId = newObj,Subject='Test',CallType='Inbound', Type='TK - Call (Inbound)');
        insert t;

        Contract l = [select Last_Activity_Datetime__c from Contract where Id=:newObj];
        System.assert(l.Last_Activity_Datetime__c!=null,'Last_Activity_Datetime__c on Contract should be filled');
    }

    static testMethod void testTaskNotByCTI() {

        QTTestUtils.GlobalSetUp();

        Id lead = makeLead();
        //No calltype
        Task t = new Task(WhoId = lead,Subject='Test', Type='Meeting');
        insert t;

        Lead l = [select Last_Activity_Datetime__c from Lead where Id=:lead];
        System.assert(l.Last_Activity_Datetime__c==null,'Last_Activity_Datetime__c on Lead should be null');
    }

    static testMethod void testMultipleTasksOnOneObject() {

        QTTestUtils.GlobalSetUp();
        
        Id lead = makeLead();
        List<Task> tasks = new List<Task>();

        tasks.add(new Task(WhoId = lead,Subject='Test', Type='Other'));
        tasks.add(new Task(WhoId = lead,Subject='Test2', Type='Meeting'));
        insert tasks;
    }
}