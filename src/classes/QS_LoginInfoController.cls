/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public class QS_LoginInfoController {


    public string logoutUrl {get; set;}
    
    public QS_LoginInfoController()
    {
        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        if(partnerPortalURLs != null && partnerPortalURLs.Support_Portal_Logout_Page_Url__c!=null)
            logoutUrl = partnerPortalURLs.Support_Portal_Logout_Page_Url__c;
        else 
            logoutUrl = '';
    }
}