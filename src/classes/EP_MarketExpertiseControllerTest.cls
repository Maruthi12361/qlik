/* 
Purpose: Market Expertise Controller test
Created Date: 6 Jan 2016
Initial delvelpment MTM 6 Jan 2016
Change log:
 **/
@isTest
public class EP_MarketExpertiseControllerTest {

    Static Account account { get; set; }
    Static Partner_Expertise_Area__c ExpertiseArea {get; set;}
    Static Partner_Expertise_Application_Lookup__c ExpertLookup {get; set;} 
    Static User testUser { get; set; }
    Static Contact testContact { get; set; }
    Static QVM_Product_Data__c QvProductData1 { get; set; }
    Static QVM_Product_Data__c QvProductData2 { get; set; }
    
    static testmethod void test_EP_MarketExpertiseController() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        System.assertEquals(controller.AccountId, account.Id);
        Test.stopTest();
    }
    static testmethod void test_QlikMarkets() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        System.assertEquals(2, controller.QlikMarkets.Size());
        Test.stopTest();
    }
    
    static testmethod void test_ListQlikMarketApp() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        System.assertEquals(2, controller.ListQlikMarketApp.Size());
        Test.stopTest();
    }
    static testmethod void test_ExpertiseAreaMap() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        ExpertiseArea = [Select
                         Id,
                         Expertise_Designation_Status__c,
                         Expertise_Application_Eligibility__c,
                         Expertise_Area__c,
                         Qlik_Market_App_Status__c
                         from Partner_Expertise_Area__c 
                         where Partner_Account_Name__c =: account.Id
                         AND Id =: ExpertiseArea.Id];
        System.assertEquals('Accepted', ExpertiseArea.Expertise_Application_Eligibility__c);
        System.assertNotEquals(NULL, controller.ExpertiseAreaMap.get(ExpertiseArea.Id));
        Test.stopTest();
    }
    
    static testmethod void test_QlikMarketAppMap() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        System.assertNotEquals(NULL, controller.QlikMarketAppMap.get(QvProductData2.Id));
        Test.stopTest();
    }
    
    
    static testmethod void test_ExpertiseAreas() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        System.assertEquals(1, controller.ExpertiseAreas.Size());
        Test.stopTest();
    }
    
    static testmethod void test_ListExpertiseAreas() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        System.assertEquals(1, controller.ListExpertiseArea.Size());
        Test.stopTest();
    }
    
    static testmethod void test_Save() 
    {
        Setup();
        Test.startTest();
        System.currentPageReference().getParameters().put('varAccountID', account.Id);
        EP_MarketExpertiseController controller = new EP_MarketExpertiseController();
        controller.ExpertiseArea = ExpertiseArea.Id;
        controller.QlikMarketApp = QvProductData2.Id;
        controller.Save();
        ExpertiseArea = [Select
                         Id,
                         Expertise_Designation_Status__c,
                         Expertise_Area__c,
                         Qlik_Market_App_Status__c
                         from Partner_Expertise_Area__c 
                         where Partner_Account_Name__c =: account.Id
                         AND Id =: ExpertiseArea.Id];
        System.assertEquals('Completed', ExpertiseArea.Qlik_Market_App_Status__c);
        
        QvProductData2 = [Select Id,
                         Partner_Expertise_Area_ID__c
                         FROM QVM_Product_Data__c
                         WHERE Id =: QvProductData2.Id];
        //DNN - Commenting since Partner Sharing doesnt allow user to update QVM Product Data
        //System.assertEquals(ExpertiseArea.Id, QvProductData2.Partner_Expertise_Area_ID__c);
        Test.stopTest();
    }
    
    public Static void Setup()
    {                    
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        
        insert qtc;
        
        //Create Account
        account = TestDataFactory.createAccount('Test AccountName', qtc);
        insert account;
         
		//Create Contacts
		testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', account.Id);
        insert testContact;
        
        // Retrieve Customer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'PRM - Sales Dependent Territory + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

		// Create User
		testUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert testUser;
        
        testContact.OwnerId = testuser.Id;
        update testContact;
        
        //Create ExpertiseArea
        //ExpertiseArea = EP_TestDataSetup.CreatExpertiseArea(account.Id, 'Communications', testContact);  

        ExpertiseArea = new Partner_Expertise_Area__c(            
            Partner_Account_Name__c = account.Id,
            Expertise_Area__c = 'Communications',
            Dedicated_Expertise_Contact__c = testContact.Id,
            Qlik_Market_App_Status__c = 'Not Assigned',
            Expertise_Designation_Status__c = 'In Process',
            Expertise_Application_Eligibility__c = 'Accepted');
        insert ExpertiseArea;


        //Create ExpertiseArea lookup object
        ExpertLookup = EP_TestDataSetup.CreatExpertiseLookup(account.Id, ExpertiseArea.Id);

        QVM_Partner_Data__c qvmpartner = new QVM_Partner_Data__c(Partner_Account__c=account.Id);
        insert qvmpartner;
        
        QvProductData1 = new QVM_Product_Data__c(
            Product_Name__c='QlikView Prod1', 
            QVM_Partner__c=qvmpartner.Id,
            Type__c ='Application',
            Status__c = 'Active',
            Partner_Expertise_Area_ID__c=ExpertiseArea.Id);
        
        QvProductData2 = new QVM_Product_Data__c(
            Product_Name__c='QlikView Prod2', 
            QVM_Partner__c=qvmpartner.Id,
            Type__c ='Application',
            Status__c = 'Active'
        );
        
        List<QVM_Product_Data__c> qvList = new List<QVM_Product_Data__c> {QvProductData1, QvProductData2};
        insert qvList;
    }

}