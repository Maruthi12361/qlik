/******************************************************

    Class: multiEdit_EventsPlan_Controller
    
    Changelog:
        2013-04-24  CCE     Initial development. CR# 7927 - Rework of Account Plan feature and functionality
                            https://eu1.salesforce.com/a0CD000000YFNC8. Controller class for
                            multiEdit_EventsPlan.page
        
******************************************************/
public with sharing class multiEdit_EventsPlan_Controller{
    
    private ApexPages.StandardController Controller;
    private String accountPlanId;
    List<Event_Plan__c> l_ObjX = new List<Event_Plan__c>();
        
    public multiEdit_EventsPlan_Controller(ApexPages.StandardController stdController)
    {
        //Save the Id so we can navigate back to the record when we do a Save or Cancel
        accountPlanId = ApexPages.currentPage().getParameters().get('accountPlanId');            
    }

    //Create a list of the current Executive Mappings records and add a new empty one to the list
    public List<Event_Plan__c> getEventsPlans()
    {
        List<Event_Plan__c> l_Obj = new List<Event_Plan__c>();
        l_Obj = [select Name, Sales_Objective__c, Customer_Owner__c, QlikTech_Owner__c, Date_Planned__c, Date_Completed__c from Event_Plan__c where Account_Plan__c = :accountPlanId ORDER BY CreatedDate];
        
        l_ObjX.clear(); //so we don't duplicate the list when we addAll()
        l_ObjX.addAll(l_Obj);
        Event_Plan__c obj = new Event_Plan__c(Account_Plan__c = accountPlanId);
        l_ObjX.add(obj);
    
        return l_ObjX;
    }
    
    public PageReference SaveSet() {
        if (!DoTheSave()) return System.currentPageReference(); //if the Save failed we stay on the same page so we see the captured error messages
        
        PageReference pr;       
        if (accountPlanId != null)
        {
            pr = new PageReference('/' + accountPlanId);
        }
        else {pr = new PageReference('/');}     
        pr.setRedirect(true);
        return pr;          
    }
    
    public PageReference CancelSet() {
        PageReference pr;   
        if (accountPlanId != null)
        {
            pr = new PageReference('/' + accountPlanId);
        }
        else {pr = new PageReference('/');}
        pr.setRedirect(true);
        return pr;          
    }
    
    public PageReference QuickSaveSet() {       
        DoTheSave();
        return System.currentPageReference();           
    }
    
    //Save any changes to the page. If no changes have been made to the record we added earlier then we remove it before we save.
    public boolean DoTheSave() {
        boolean errFlg = true;
        try {
            //System.debug('multiEdit_ExecPlan: l_ObjX[l_ObjX.size()-1] = ' + l_ObjX[l_ObjX.size()-1]);
            Event_Plan__c obj = l_ObjX[l_ObjX.size()-1];
            if (obj.Name == null && obj.Sales_Objective__c == null && obj.Customer_Owner__c == null && obj.QlikTech_Owner__c == null && obj.Date_Planned__c == null && obj.Date_Completed__c == null)
            {
                Event_Plan__c delObjX = l_ObjX.remove(l_ObjX.size()-1);  //remove the unused record 
                //System.debug('multiEdit_ExecPlan: delObjX = ' + delObjX);
            }       
            upsert l_ObjX;        
        }
        catch(Exception e) {
            System.debug('ERROR with multiEdit_EventsPlan: ' + e);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR with multiEdit_EventsPlan: ' + e.getMessage());
            ApexPages.addMessage(msg);
            errFlg = false;
        }
        return errFlg;      
    }
    
}