/******************************************************
Authour: Enrico Varriale Fluido Sweden
     
   1. When a Contact change status to "Archived" or "False" or "Contact - Never"
     1.1 update "Archived: Original Owner" with the current owner
     1.2 update the owner to Anna Forsgren Kap
     1.3 The Contact Record Type shall be changed to a new Record Type named "Archived"
     1.4 Let see the previous owner name and record type name 
  

   2. When a user then update the Contact Status value from "Archived" or "False" or "Contact - Never"to another value then the following should happen:
    2.1 update the owner to the "Archived: Original Owner"
    2.2 change the Record Type from "Archived" to the previous Record Type value
    2.3 Let see the previous owner name and record type name
    
   19-07-2013   TJG case 00192149 https://eu1.salesforce.com/500D000000Qqfq4
        Make objects are not null

    2015-06-01  CCE CR# 31870 Adding extra values to initiate updating the owner, also added Semaphore.
2016-04-26  CCE CR# 82611 - Removal of Not Follow-Up Required Reason field
2017-06-30  CCE CHG0031025 - Addition of Follow-Up DisqualifiedJunk
    2017-09-22 - AYS -  BMW-396/CHG0032034 - Added logic for Archived Record Type.
    2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
    2019-04-15 CCE CHG0035936 BMW-1424 Stop Contact Resource RT from being overwritten on Suspect status
******************************************************/ 

public with sharing class ContactSetOwnerHelper {
	
	public static void setOwner(List<Contact> triggernew, Map<id,contact> triggeroldmap){
		// CREATE MAPS
        MAP<id,Contact> toupdate_recordtype = new MAP<id,Contact> ();
        Set< String > structure = new Set< String >{'Archived', 'False', 'Lead - Never', 'Junk', 'Follow-Up RejectedJunk', 
                                                    'Follow-Up RejectedInvalid Job Function', 
                                                    'Follow-Up RejectedCompetitor', 'Follow-Up DisqualifiedNo Fit', 
                                                    'Follow-Up DisqualifiedLeft Company', 'Follow-Up DisqualifiedJunk'};

        //Set< String > structure = new Set< String >{'Archived', 'False', 'Lead - Never', 'Junk', 'Follow-Up RejectedJunk', 
        //                                            'Follow-Up RejectedNot Follow-Up RequiredInvalid Job Function', 
        //                                            'Follow-Up RejectedNot Follow-Up RequiredCompetitor', 'Follow-Up DisqualifiedNo Fit', 
        //                                            'Follow-Up DisqualifiedLeft Company'};  //CR# 82611


        //GET USER ID AND RECORD TYPE ID FROM CUSTOM SETTINGS.
        ContactSetOwner__c cs = ContactSetOwner__c.getValues('contactset');
        ZiftSettings__c csZift = ZiftSettings__c.getInstance();
        
        for (Contact l: triggernew){
            
            //Begin BMW-396
            //If Status is changed to "Suspect" the Record Type changes to the value in the "Archived: Original Record Type" field. 
            //If no value is present make the Record Type = Standard Lead.
            for(Contact contact : triggernew) {
                Contact oldContact = triggeroldmap.get(contact.id);
                if(contact.RecordTypeId != csZift.RT_Resource_Con__c && contact.Contact_Status__c == 'Suspect' && oldContact.Contact_Status__c != 'Suspect'){
                    if(contact.Old_Record_Type_Name__c != null){
                        try{
                            contact.RecordTypeId =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contact.Old_Record_Type_Name__c).getRecordTypeId();
                        }catch(exception e){
                            contact.RecordTypeId = csZift.RT_StandardContact__c;
                            System.debug('debug_Old_Record_Type_Name_error: ' + e.getMessage());
                        }
                    }else{
                        contact.RecordTypeId = csZift.RT_StandardContact__c;
                    }  
                }
            }
            //End BMW-396

            //GET OLD Contact'S RECORD
            Contact  oldContact = triggeroldmap.get(l.Id);

            //Creating combinedStatus string as CR# 31870 asks that we are able to test for various status values with specific reasons              
            string combinedStatus = l.contact_Status__c;
            if (combinedStatus == 'Follow-Up Rejected') {
                combinedStatus += l.Follow_Up_Rejected_Reason__c;
                //if (combinedStatus == 'Follow-Up RejectedNot Follow-Up Required') {combinedStatus += l.Not_Follow_Up_Required_Reason__c;}  //CR# 82611
            } else if (combinedStatus == 'Follow-Up Disqualified') {
                combinedStatus += l.Follow_Up_Disqualified_Reason__c;
            }
            System.debug('ContactSetOwner combinedStatus = ' + combinedStatus);

            //Creating oldcombinedStatus string as CR# 31870 asks that we are able to test for various status values with specific reasons              
            string oldcombinedStatus = oldContact.contact_Status__c;
            if (oldcombinedStatus == 'Follow-Up Rejected') {
                oldcombinedStatus += oldContact.Follow_Up_Rejected_Reason__c;
                //if (oldcombinedStatus == 'Follow-Up RejectedNot Follow-Up Required') {oldcombinedStatus += oldContact.Not_Follow_Up_Required_Reason__c;}  //CR# 82611
            } else if (oldcombinedStatus == 'Follow-Up Disqualified') {
                oldcombinedStatus += oldContact.Follow_Up_Disqualified_Reason__c;
            }
            System.debug('ContactSetOwner oldcombinedStatus = ' + oldcombinedStatus);
            
            if((l.contact_Status__c!=null) && (oldcontact.Contact_Status__c!=null)){
            
                //CHECK IF Contact STATUS IS CHANGED TO 'Archived' OR 'False'        
                if ((structure.contains(combinedStatus)) && (oldcombinedStatus != combinedStatus)
                && ( structure.contains( oldcombinedStatus) == false ) ){                                                      
                //if ((structure.contains(l.contact_Status__c.trim())) && (oldContact.Contact_Status__c!= l.Contact_Status__c)
                //&&( structure.contains( oldcontact.contact_Status__c.trim())== false ) ){                                                      
                    l.Archived_Original_Owner__c= l.OwnerId; 
                    l.OwnerId=cs.ContactOwnerId__c;
                    // RECORD TYPE 
                    l.Old_Record_Type__c=l.recordtypeId; 
                    l.recordtypeid=cs.Contactrecordtype__c;
                    //EDIT 19-07-2013
                    If (l.Old_Record_Type__c!=null){
                        toupdate_recordtype.put(l.Old_Record_Type__c,l);
                    }
                }
                else if ((oldcombinedStatus != combinedStatus) && (l.Archived_Original_Owner__c!=null)
                && (structure.contains( oldcombinedStatus))
                && (structure.contains( combinedStatus) == false  )) {
                    string temp=l.OwnerId;
                    l.OwnerId= l.Archived_Original_Owner__c; 
                    l.Archived_Original_Owner__c= temp;

                    If (l.Old_Record_Type__c!=null){
                        string recordtypetemp=l.recordtypeid;
                        l.recordtypeid=l.Old_Record_Type__c;
                        l.Old_Record_Type__c= recordtypetemp;                   
                        toupdate_recordtype.put(l.Old_Record_Type__c,l);
                    }
                // CLOSE SECOND IF
                }

            }
                 
            // NULL NEW CONTACT STAGE
            if((l.contact_Status__c==null) && (oldcontact.Contact_Status__c!=null)&&(l.Archived_Original_Owner__c!=null)){
                if (structure.contains(oldcombinedStatus)){ 

                    string temp=l.OwnerId;
                    l.OwnerId= l.Archived_Original_Owner__c; 
                    l.Archived_Original_Owner__c= temp;

                    If (l.Old_Record_Type__c!=null){
                        string recordtypetemp=l.recordtypeid;
                        l.recordtypeid=l.Old_Record_Type__c;
                        l.Old_Record_Type__c= recordtypetemp;                   
                        toupdate_recordtype.put(l.Old_Record_Type__c,l);
                    }
                }
            }

            // NULL OLD CONTACT STAGE
            if((l.contact_Status__c!=null) && (oldcontact.Contact_Status__c==null)){
                if (structure.contains(combinedStatus ) ){
                    l.Archived_Original_Owner__c= l.OwnerId; 
                    l.OwnerId=cs.ContactOwnerId__c;
                    // RECORD TYPE 
                    l.Old_Record_Type__c=l.recordtypeId; 
                    l.recordtypeid=cs.Contactrecordtype__c;
                    // EDIT 19-07-2013
                    If (l.Old_Record_Type__c!=null){
                        toupdate_recordtype.put(l.Old_Record_Type__c,l);
                    }
                }
            }
                 
        // END FOR
        }
               
        if(toupdate_recordtype.size()>0){

            Map<id,recordtype> mappart = new Map<id,recordtype>([SELECT Id,name from recordtype where ID in : toupdate_recordtype.keyset()]);
            // USER
            for (Contact lu: toupdate_recordtype.values() ){
                // EDIT 19-07-2013
                RecordType rt = mappart.get(lu.Old_Record_Type__c);
                if (null != rt){
                    lu.Old_Record_Type_Name__c = rt.name;
                }
            }
        }
        System.debug('ContactSetOwner: Finishing'); 
	}
}