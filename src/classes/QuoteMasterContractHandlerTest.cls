/****************************************************************
*
*  QuoteMasterContractHandlerTest
*  2017-06-21 -Srinivasan PR  changes  for QCW -2682
*  2017-07-19 Rodion Vakuovskyi QCW-2506 Fixed unit test failures
*  2019-05-02 Linus Löfberg - adjusting for CPQ upgrade
*****************************************************************/
@isTest
public class QuoteMasterContractHandlerTest{

    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';

    @testSetup
    static void testSetup() {
        QTTestUtils.GlobalSetUp();
    }

    @isTest
    static void OnBeforeCalculateTest() {
        Account testAcc = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAcc.Territory_Country__c = 'France';
        update testAcc;

        Contact testContact = QuoteTestHelper.createContact(testAcc.id);
        insert testContact;

        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , false);
        insert pr;

        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Evaluation_Quote'];
        Test.setMock(WebServiceMock.class, new AdressMockService());

        Address__c billAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Billing');
        insert billAddress;
        Address__c shippAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Shipping');
        insert shippAddress;

        Test.startTest();
        SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);


        SBQQ__Quote__c quoteForSelect = [Select id, second_partner__c, SBQQ__MasterContract__c, SBQQ__StartDate__c,  Revenue_Type__c, Record_Type_Dev_Name__c, Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c from SBQQ__Quote__c where id =:quoteForTest.id];

        SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForSelect.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050,20, '12345', null);
        Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', '12345', '23423', null);
        List<Asset> assets = [select Id, Quote__c from Asset where Quote__c =: quoteForSelect.Id];
        System.assertEquals(0, assets.size());
        AssetUtil au = new AssetUtil();
        Set<Id> quotesToCreateAssetsFor = new Set<Id>();
        quotesToCreateAssetsFor.add(quoteForSelect.Id);
        Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
        quotesMap.put(quoteForSelect.Id, quoteForSelect);
        List<Sales_Channel__c> contractAssetLink = [Select Id from Sales_Channel__c];
        System.assertEquals(0,contractAssetLink.size());
        au.createAsset(quotesToCreateAssetsFor, quotesMap);
        assets = [select Id, Quote__c, NC_Disable_AJAX__c  from Asset where Quote__c =: quoteForSelect.Id];
        contractAssetLink = [Select Id from Sales_Channel__c];
        date endDate = date.today();
        endDate.addMonths(3);
        contractAssetLink[0].Contract_End_Date__c = endDate;
        update contractAssetLink;
        Contract c = new Contract();
        c.AccountId = testAcc.Id;
        c.Support_Percentage__c =21;
        Date d = Date.today();
        d.addMonths(2);
        c.StartDate = d;
        c.ContractTerm = 4;
        c.Sales_Channel__c = contractAssetLink[0].Id;
        insert c;
        c.Status = 'Activated';
        update c;
        System.assertEquals(1, assets.size());

        RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Q2CWCalculator/beforeCalculate/';
        req.addParameter('quoteId', quoteForSelect.Id);
        req.addParameter('allAssetIds', assets[0].Id);
      req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"quoteId": "' + quoteForSelect.Id + '", "allAssetIds": "' + assets[0].Id + '"}');
        RestContext.request = req;
      RestContext.response = res;
        QuoteMasterContractHandler.processRequest();

        Test.stopTest();
    }

    @isTest
    static void tooManySalesChannelsTest(){
        Sales_Channel__c s1 = new Sales_Channel__c();
        Sales_Channel__c s2 = new Sales_Channel__c();
        List<Sales_Channel__c> slist = new List<Sales_Channel__c>();
        sList.add(s1);
        sList.add(s2);
        insert sList;

        List<Asset> aList = new List<Asset>();
        Asset a1 =  new Asset();
        a1.Name = 'sc1';
        a1.Sales_Channel__c = s1.Id;
        aList.add(a1);
        Asset a2 = new Asset();
        a2.Name = 'sc2';
        a2.Sales_Channel__c = s2.Id;
        aList.add(a2);
        insert aList;

        Account testAcc = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAcc.Territory_Country__c = 'France';
        update testAcc;

        Contact testContact = QuoteTestHelper.createContact(testAcc.id);
        insert testContact;
        Test.setMock(WebServiceMock.class, new AdressMockService());

        Address__c billAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Billing');
        insert billAddress;
        Address__c shippAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Shipping');
        insert shippAddress;

        Test.startTest();
        RecordType rType = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Evaluation_Quote'];
        SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
        String assets = a1.Id + ','+a2.Id;
        //QuoteMasterContractHandler.ReturnData data = QuoteMasterContractHandler.populateWhenAmending(assets, quoteForTest.Id);

        RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Q2CWCalculator/beforeCalculate';
        req.addParameter('quoteId', quoteForTest.Id);
        req.addParameter('allAssetIds', assets);
      req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"quoteId": "' + quoteForTest.Id + '", "allAssetIds": "' + assets + '"}');
        RestContext.request = req;
      RestContext.response = res;
        QuoteMasterContractHandler.processRequest();

        Test.stopTest();
    }

    @isTest
    static void assignQuoteVariablesFromContract() {
        Account testAcc = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAcc.Territory_Country__c = 'France';
        update testAcc;

        Contact testContact = QuoteTestHelper.createContact(testAcc.id);
        insert testContact;

        Test.setMock(WebServiceMock.class, new AdressMockService());
        Address__c billAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Billing');
        insert billAddress;
        Address__c shippAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Shipping');
        insert shippAddress;

        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , false);
        insert pr;

        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Evaluation_Quote'];
        QuoteTestHelper.createCustomSettings();

        SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);

        SBQQ__Quote__c quoteForSelect = [SELECT id, SBQQ__MasterContract__c, SBQQ__StartDate__c,
        Revenue_Type__c, Record_Type_Dev_Name__c,
        Evaluation_Expiry_Date__c, Subscription_Start_Date__c,
        Subscription_End_Date__c, SBQQ__Account__c,
        Sell_Through_Partner__c, License_End_Date__c,
        Support_Provided_by_Id__c, Support_provided_by_Qlik__c , second_partner__c
        FROM SBQQ__Quote__c
        WHERE Id =:quoteForTest.Id];

        Test.StartTest();

        SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForSelect.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050,20, '12345', null);
        Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', '12345', '23423', null);

        List<Asset> assets = [select Id, Quote__c from Asset where Quote__c =: quoteForSelect.Id];
        AssetUtil au = new AssetUtil();

        Set<Id> quotesToCreateAssetsFor = new Set<Id>();
        quotesToCreateAssetsFor.add(quoteForSelect.Id);

        Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
        quotesMap.put(quoteForSelect.Id, quoteForSelect);

        List<Sales_Channel__c> contractAssetLink = [Select Id from Sales_Channel__c];
        au.createAsset(quotesToCreateAssetsFor, quotesMap);

        assets = [select Id, Quote__c, NC_Disable_AJAX__c  from Asset where Quote__c =: quoteForSelect.Id];
        contractAssetLink = [Select Id from Sales_Channel__c];
        Date endDate = Date.today();
        endDate.addMonths(3);
        contractAssetLink[0].Contract_End_Date__c = endDate;
        update contractAssetLink;

        Contract c = new Contract();
        c.AccountId = testAcc.Id;
        c.Support_Percentage__c =21;
        Date d = Date.today();
        d.addMonths(-12);
        c.StartDate =date.today();
        c.ContractTerm = 1;
        c.Sales_Channel__c = contractAssetLink[0].Id;
        c.Revenue_Type__c =quoteForSelect.Revenue_Type__c;
        c.Sell_Through_Partner__c=quoteForSelect.Sell_Through_Partner__c;
        c.NS_Status__c ='Active' ;
        insert c;

        c.Status = 'Activated';
        update c;

        RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Q2CWCalculator/beforeCalculate';
        req.addParameter('quoteId', quoteForSelect.Id);
        req.addParameter('allAssetIds', assets[0].Id);
      req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"quoteId": "' + quoteForSelect.Id + '", "allAssetIds": "' + assets[0].Id + '"}');
        RestContext.request = req;
      RestContext.response = res;
        QuoteMasterContractHandler.processRequest();

        Test.stopTest();
    }

    @isTest
    static void OnAfterCalculateTest() {
        Account testAcc = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAcc.Territory_Country__c = 'France';
        update testAcc;

        Contact testContact = QuoteTestHelper.createContact(testAcc.id);
        insert testContact;

        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , false);
        insert pr;

        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Evaluation_Quote'];
        Test.setMock(WebServiceMock.class, new AdressMockService());

        Address__c billAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Billing');
        insert billAddress;
        Address__c shippAddress = QuoteTestHelper.createAddress(testAcc.id, testContact.id, 'Shipping');
        insert shippAddress;

        Test.startTest();
        SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);


        SBQQ__Quote__c quoteForSelect = [Select id, second_partner__c, SBQQ__MasterContract__c, SBQQ__StartDate__c,  Revenue_Type__c, Record_Type_Dev_Name__c, Evaluation_Expiry_Date__c, Subscription_Start_Date__c, Subscription_End_Date__c, SBQQ__Account__c, Sell_Through_Partner__c, License_End_Date__c, Support_Provided_by_Id__c, Support_provided_by_Qlik__c from SBQQ__Quote__c where id =:quoteForTest.id];

        SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForSelect.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Maintenance', 'USD', 9050,20, '12345', null);
        Quote_Line_Sub_Line__c qlsl = TestQuoteUtil.createMockQuoteLineSubLine(quoteLine.Id, '13124', 'USD', '1','32232', '12345', '23423', null);
        List<Asset> assets = [select Id, Quote__c from Asset where Quote__c =: quoteForSelect.Id];
        System.assertEquals(0, assets.size());
        AssetUtil au = new AssetUtil();
        Set<Id> quotesToCreateAssetsFor = new Set<Id>();
        quotesToCreateAssetsFor.add(quoteForSelect.Id);
        Map<ID, SBQQ__Quote__c> quotesMap = new Map<ID, SBQQ__Quote__c>();
        quotesMap.put(quoteForSelect.Id, quoteForSelect);
        List<Sales_Channel__c> contractAssetLink = [Select Id from Sales_Channel__c];
        System.assertEquals(0,contractAssetLink.size());
        au.createAsset(quotesToCreateAssetsFor, quotesMap);
        assets = [select Id, Quote__c, NC_Disable_AJAX__c  from Asset where Quote__c =: quoteForSelect.Id];
        contractAssetLink = [Select Id from Sales_Channel__c];
        date endDate = date.today();
        endDate.addMonths(3);
        contractAssetLink[0].Contract_End_Date__c = endDate;
        update contractAssetLink;
        Contract c = new Contract();
        c.AccountId = testAcc.Id;
        c.Support_Percentage__c =21;
        Date d = Date.today();
        d.addMonths(2);
        c.StartDate = d;
        c.ContractTerm = 4;
        c.Sales_Channel__c = contractAssetLink[0].Id;
        insert c;
        c.Status = 'Activated';
        update c;
        System.assertEquals(1, assets.size());

        RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Q2CWCalculator/afterCalculate/';
        req.addParameter('quoteId', quoteForSelect.Id);
        req.addParameter('netAmount', '1000.0');
      req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"quoteId": "' + quoteForSelect.Id + '", "netAmount": 1000.0}');
        RestContext.request = req;
      RestContext.response = res;
        QuoteMasterContractHandler.processRequest();

        Test.stopTest();
    }
}
