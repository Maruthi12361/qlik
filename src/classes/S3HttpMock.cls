@isTest
public class S3HttpMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public S3HttpMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }
    public static S3HttpMock CreateHTTPMock(integer responseCode)
    {
        system.debug('CreateHTTPMock start');
        
        string body = '<?xml version="1.0" ?> <rss version="2.0"> <channel>   <title>Ajax and XUL</title>   <link>http://www.xul.fr/en/</link>   <description>XML graphical interface etc...</description>   <image>       <url>http://www.xul.fr/xul-icon.gif</url>       <link>http://www.xul.fr/en/index.php</link>   </image>   <item>       <title>News  of today</title>       <link>http://www.xul.fr/en-xml-rss.html</link>       <description>All you need to know about RSS</description>  </item>   <item>      <title>News of tomorrows</title>      <link>http://www.xul.fr/en-xml-rdf.html</link>      <description>And now, all about RDF</description>    </item></channel></rss>';
        Map<String, String> responseHeaders = new Map<String, String>();
        S3HttpMock mock = new S3HttpMock(responseCode, 'Alles gut', body,responseHeaders);
        system.debug('Create HTTP Mock end');
        return mock;
    }

}