/*
@author: Anthony Victorio, Model Metrics
@date: 03/01/2012
@description: global functions used to execute functions from javscript calls in SF buttons
    2017-01-24  CCE CR# 103173 - Remove Magento Connections from SFDC 
*/
global class QVM_Global {
	
	WebService static void archiveMagentoProduct(String productId) {
		
		//String MAGENTO_SESSION_ID = QVM.getMagentoSessionId();
		
        //String DEFAULT_STATUS = '2'; //a status of '2' means disabled
		
		QVM_Product_Data__c product = [select Id, Magento_Product_Id__c, Status__c, Magento_Status__c from QVM_Product_Data__c where Id = :productId];
		
		//String API_ARGUMENTS = '&arg2[product]=' + product.Magento_Product_Id__c +
		//'&arg2[data][status]=' + DEFAULT_STATUS;
		
		//Dom.Document doc = QVM.getMagentoCallDocument(MAGENTO_SESSION_ID, 'product.update', API_ARGUMENTS);
  //      Dom.XMLNode root = doc.getRootElement();
  //      Dom.XMLNode call = root.getChildElements()[0];
        
  //      String updateResults;
        
  //      for(Dom.XMLNode node : call.getChildElements()) {
  //          if('status' == node.getName()) {
  //              updateResults = node.getText();
  //          }
  //      }
        
  //      if(updateResults == 'success') {
            product.Status__c = 'Archived';
            product.Magento_Status__c = 'Not Published';
            //product.Magento_Last_Updated__c = System.now();
            update product;
        //}
		
	}
	
	WebService static void enableMagentoProduct(String productId) {
        
        //String MAGENTO_SESSION_ID = QVM.getMagentoSessionId();
        
        //String DEFAULT_STATUS = '1'; //a status of '1' means enabled
        
        QVM_Product_Data__c product = [select Id, Magento_Product_Id__c, Status__c, Magento_Status__c from QVM_Product_Data__c where Id = :productId];
        
        //String API_ARGUMENTS = '&arg2[product]=' + product.Magento_Product_Id__c +
        //'&arg2[data][status]=' + DEFAULT_STATUS;
        
        //Dom.Document doc = QVM.getMagentoCallDocument(MAGENTO_SESSION_ID, 'product.update', API_ARGUMENTS);
        //Dom.XMLNode root = doc.getRootElement();
        //Dom.XMLNode call = root.getChildElements()[0];
        
        //String updateResults;
        
        //for(Dom.XMLNode node : call.getChildElements()) {
        //    if('status' == node.getName()) {
        //        updateResults = node.getText();
        //    }
        //}
        
        //if(updateResults == 'success') {
        	product.Status__c = 'Active';
        	product.Magento_Status__c = 'Published';
            //product.Magento_Last_Updated__c = System.now();
            update product;
        //}
        
    }

}