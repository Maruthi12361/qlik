/*
	Class: SolutionProfileRedirectController
	
	2013-05-07	CCE		Initial development	for part of CR# 8028 - #Quick - Modify link on Qonnect Portal.
						https://eu1.salesforce.com/a0CD000000YGb1y
						This redirects us to one of two different target pages.
	2013-11-15	CCE		CR# 10096 https://eu1.salesforce.com/a0CD000000d60Zu								
*/
public with sharing class SolutionProfileRedirectController {
	
	static final String PRMBASEProfile = '00eD0000001PFWp';
	static final String ART_OEMPartnerAccount = '01220000000DOG4';	//OEM Partner Account (Account Record Type)
	
	public pagereference checkAndRedirect()// redirects to new page
    {
        Pagereference p;
        
        String accId = ApexPages.currentPage().getParameters().get('acc');
        system.debug('SolutionProfileRedirectController accId = ' + accId);
        
        String userProfile = UserInfo.getProfileId();
        system.debug('SolutionProfileRedirectController userProfile = ' + userProfile);
        
        Account thisAcc = [SELECT Id, RecordTypeId FROM Account WHERE Id=:accId];      
        
        if (userProfile.contains(PRMBASEProfile)) {
        	system.debug('SolutionProfileRedirectController go to Solution Profile list');        
        	p = new Pagereference('/apex/SolutionProfileRelatedList?id=' + accId);        	
        } else if (thisAcc.RecordTypeId == ART_OEMPartnerAccount) {	//CCE CR# 10096
        	system.debug('SolutionProfileRedirectController thisAcc = Yes - ART=OEMPartner');
        	p = new Pagereference('/' + accId);	//the Account page
        } else {
        	system.debug('SolutionProfileRedirectController go to Opps view');
        	p = new Pagereference('/006?fcf=00BD0000007phFW');	//All Closed Won (Opportunities view)        	
        }
        
        return p;        
    }
}