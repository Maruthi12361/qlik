/****************************************************************************************************
   ImageUploadController
   2019-03-05 IT-1390 : page for image uploading
****************************************************************************************************/
public with sharing class ImageUploadController {
    private static final String IMG_FOLDER_NAME = 'Article_Images';
    private static final String IMG_SERVER_LINK = '/servlet/servlet.ImageServer?id=';
    private static final String IMG_SERVER_OID = '&oid=';
    private static final String DELIMITER = '!,!';
    private String baseContentUrl;
    private String orgId;
    public Map<String, String> imagesMap { get; set; }
    public Map<String, String> imagesErrorsMap { get; set; }
    public String folderId { get; set; }
    public Boolean showImages { get; set; }
    public Boolean showErrors { get; set; }

    public ImageUploadController() {
        showImages = false;

        QTCustomSettingsHier__c qtSettings = QTCustomSettingsHier__c.getInstance();
        baseContentUrl = qtSettings.Base_Content_URL__c;
        orgId = [SELECT Id FROM Organization LIMIT 1].Id;

        List<Folder> imgUploaderFolder = [SELECT Id FROM Folder WHERE DeveloperName = :IMG_FOLDER_NAME LIMIT 1];
        if (imgUploaderFolder.size() == 1) {
            folderId = imgUploaderFolder.get(0).Id;
        }

        imagesErrorsMap = new Map<String, String>();
        imagesMap = new Map<String, String>();
    }

    public void addImages() {
        String namesStr = ApexPages.currentPage().getParameters().get('namesParam');
        String idsStr = ApexPages.currentPage().getParameters().get('idsParam');
        String errorNamesStr = ApexPages.currentPage().getParameters().get('errorNamesParam');
        String errorsStr = ApexPages.currentPage().getParameters().get('errorsParam');

        if (idsStr.length() > DELIMITER.length()) {
            List<String> imgNames = namesStr.split(DELIMITER);
            List<String> imgIds = idsStr.split(DELIMITER);

            showImages = true;
            for (Integer i = 0; i < imgNames.size(); i++) {
                imagesMap.put(imgNames.get(i), baseContentUrl + IMG_SERVER_LINK + imgIds.get(i) + IMG_SERVER_OID + orgId);
            }
        }
        if (errorNamesStr.length() > DELIMITER.length()) {
            List<String> errorNames = errorNamesStr.split(DELIMITER);
            List<String> errors = errorsStr.split(DELIMITER);

            showErrors = true;
            for (Integer i = 0; i < errorNames.size(); i++) {
                imagesErrorsMap.put(errorNames.get(i), errors.get(i));
            }
        }
    }

}