/*----------------------------------------------------------------------------------------------------------
Purpose: PAEX-56, 170
Created Date: 28/06/17

Created by: Kumar Navneet
Modified by:Pramod Kumar
Change log:12/07/2017
Purpose:PAEX-207

Modified by:Pramod Kumar
Change log: 16/08/2017
Purpose:PAEX-227,235

Modified by: Linus Löfberg
Change log: 2018-01-08
Purpose: QCW-4664 - Prevent editing PCS-records for terminated partners.

Modified by: Linus Löfberg
Change log: 2018-05-28
Purpose: BSL-372

Modified by: Linus Löfberg
Change log: 2018-10-01
Purpose: BSL-895

--------------------------------------------------------------------------------------------------------------*/

public class PartnerCategoryStatusclass{

    public Boolean isEdit {get; set;}
    public Partner_Category_Status__c ParCat { get; set; }
    public List<SBQQ__Quote__c> quotes1 { get {
        return [SELECT Id, Name, Revenue_Type__c, SBQQ__NetAmount__c, SBQQ__Status__c, CreatedById, CreatedBy.Name, SBQQ__Primary__c FROM SBQQ__Quote__c WHERE PartnerCategoryStatusSTP__c != null AND PartnerCategoryStatusSTP__c = :ParCat.Id AND SBQQ__Status__c != 'Order Placed' AND SBQQ__Status__c != 'Accepted by Customer' AND SBQQ__Status__c != 'Locked - Oppty Closed'];
    } set; }
    public List<SBQQ__Quote__c> quotes2 { get {
        return [SELECT Id, Name, Revenue_Type__c, SBQQ__NetAmount__c, SBQQ__Status__c, CreatedById, CreatedBy.Name, SBQQ__Primary__c FROM SBQQ__Quote__c WHERE PartnerCategoryStatusSecond__c != null AND PartnerCategoryStatusSecond__c = :ParCat.Id AND SBQQ__Status__c != 'Order Placed' AND SBQQ__Status__c != 'Accepted by Customer' AND SBQQ__Status__c != 'Locked - Oppty Closed'];
    } set; }
    public List<zqu__Quote__c> zquotes1 { get {
        return [SELECT Id, Name, zqu__Number__c, Revenue_Type__c, ACV__c, Qlik_TCV__c, Quote_Status__c, CreatedById, CreatedBy.Name, zqu__Primary__c FROM zqu__Quote__c WHERE Partner_Category_Status_STP__c != null AND Partner_Category_Status_STP__c = :ParCat.Id AND Quote_Status__c != 'Order Placed' AND Quote_Status__c != 'Accepted by Customer'];
    } set; }
    public List<zqu__Quote__c> zquotes2 { get {
        return [SELECT Id, Name, zqu__Number__c, Revenue_Type__c, ACV__c, Qlik_TCV__c, Quote_Status__c, CreatedById, CreatedBy.Name, zqu__Primary__c FROM zqu__Quote__c WHERE  Partner_Category_Status_Second__c != null AND Partner_Category_Status_Second__c = :ParCat.Id AND Quote_Status__c != 'Order Placed' AND Quote_Status__c != 'Accepted by Customer'];
    } set; }
    public Account partnerAccount { get; private set; }
    public Boolean hasMessages { get {
        return ApexPages.hasMessages();
    } private set; }
    public Boolean disableModifyRecord { get; private set; }
    ApexPages.standardController m_sc = null;

    public PartnerCategoryStatusclass(ApexPages.StandardController stdController) {
        m_sc = stdController;
        this.ParCat = (Partner_Category_Status__c)stdController.getRecord();

        // ParCat.Partner_Account__c = this.ParCat.Partner_Account__c;

        Id id = apexpages.currentpage().getparameters().get('id');
        String idReturl = apexpages.currentpage().getparameters().get('retURL');
        String PartnerAccID = apexpages.currentpage().getparameters().get('PartnerAccountID');
        If(id != null)
        {
            isEdit = False;
        }
        else
        {
            isEdit = True;
        }

        If(!String.isBlank(idReturl))
        {
            isEdit = True;
        }
        If(!String.isBlank(PartnerAccID))
        {
            ParCat.Partner_Account__c = PartnerAccID ;
        }

        partnerAccount = [SELECT Id, Name, Partner_Status__c FROM Account WHERE Id = :ParCat.Partner_Account__c];
        if (partnerAccount != null) {
            if (partnerAccount.Partner_Status__c != null && partnerAccount.Partner_Status__c.equals('Terminated')) {
                HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
                if (!settings.Sys_Admin_ProfileId__c.contains(UserInfo.getProfileId()) && !settings.Profile_IT_HelpDesk_ID__c.contains(UserInfo.getProfileId())) {
                    disableModifyRecord = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This Partner Account has been terminated. All PCS records must remain inactive and no new PCS records may be created.'));
                }
            }
        }
    }

    // Used for validation of fields

    Public Boolean Validate()
    {
        Boolean ErrorFlag = False;
        String Idfromgetter ;
        If(ParCat.Id != null)
        {
            Idfromgetter = String.valueOf(ParCat.Id) ;
        }

        List<Partner_Category_Status__c> ParCatStatus = [Select Id from Partner_Category_Status__c where Partner_Account__c=: ParCat.Partner_Account__c and Program_Version__c =:ParCat.Program_Version__c and Partner_Category__c =:ParCat.Partner_Category__c and Id !=: Idfromgetter   Limit 1];

        If(ParCatStatus != null && !ParCatStatus.isEmpty() )
        {

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is already an entry for this Program Version - Partner Category. Please update the entry'));
            if(!Test.isRunningTest()){
                return False;
            }
        }

        if(ParCat.Program_Version__c =='QPP' && ParCat.Partner_Category__c=='Resell' && (ParCat.Partner_Level__c =='Authorized' || ParCat.Partner_Level__c =='Select' || ParCat.Partner_Level__c =='Elite' || ParCat.Partner_Level__c =='Authorized Reseller' || ParCat.Partner_Level__c =='Exception') ){

            if (ParCat.Partner_Discount__c == null || ParCat.Subscription_Partner_Discount__c == null){
                ErrorFlag = True;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the perpetual and subscription partner discount fields'));

            }
            if ( ParCat.Partner_Training_Discount__c == null ){
                ErrorFlag = True;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

            }
            if ( ParCat.Partner_Resell_Training_Margin__c == null ){
                ErrorFlag = True;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Resell Training Margin'));

            }
            if ( ParCat.Maintenance_Partner_Discount__c == null ){
                ErrorFlag = True;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Maintenance Partner Discount'));

            }



        }
        else{
            if(ParCat.Program_Version__c =='QPP' && ParCat.Partner_Category__c =='Influence' && (ParCat.Partner_Level__c =='Authorized' || ParCat.Partner_Level__c =='Select' || ParCat.Partner_Level__c =='Elite' ) ){

                if ( ParCat.Partner_Training_Discount__c== null ){
                    ErrorFlag = True;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                }
                if ( ParCat.Influence_Benefit_Type__c== null ){
                    ErrorFlag = True;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Influence Benefit Type'));

                }


            }
            else{
                if(ParCat.Program_Version__c =='QPP' && ParCat.Partner_Category__c =='MSP' && (ParCat.Partner_Level__c =='Select' || ParCat.Partner_Level__c =='Elite' ) ){

                    if ( ParCat.Partner_Training_Discount__c== null ){
                        ErrorFlag = True;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                    }


                }
                else{

                    if(ParCat.Program_Version__c =='Partner Program 4.1' && ParCat.Partner_Category__c =='Solution Provider' && (ParCat.Partner_Level__c =='Registered' || ParCat.Partner_Level__c =='Solution Provider'  || ParCat.Partner_Level__c =='Elite'  || ParCat.Partner_Level__c =='Authorized Reseller'  || ParCat.Partner_Level__c =='Exception' || ParCat.Partner_Level__c =='Public Sector' ) ){


                        if ( ParCat.Partner_Discount__c == null || ParCat.Subscription_Partner_Discount__c == null){
                            ErrorFlag = True;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the perpetual and subscription partner discount fields'));

                        }
                        if ( ParCat.Partner_Training_Discount__c == null ){
                            ErrorFlag = True;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                        }
                        if ( ParCat.Partner_Resell_Training_Margin__c == null ){
                            ErrorFlag = True;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Resell Training Margin'));

                        }
                        if ( ParCat.Maintenance_Partner_Discount__c == null ){
                            ErrorFlag = True;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Maintenance Partner Discount'));

                        }



                    }
                    else{
                        if(ParCat.Program_Version__c =='Technology Partner' && ParCat.Partner_Category__c =='Technology Partner' && (ParCat.Partner_Level__c =='Registered' || ParCat.Partner_Level__c =='Technology Partner'  || ParCat.Partner_Level__c =='Strategic' || ParCat.Partner_Level__c =='Elite') ){

                            if ( ParCat.Partner_Training_Discount__c== null ){
                                ErrorFlag = True;
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                            }


                        }
                        else
                        {
                            if(ParCat.Program_Version__c =='Global SI' && ParCat.Partner_Category__c =='Implementation & Consulting' && (ParCat.Partner_Level__c =='Strategic' ||ParCat.Partner_Level__c =='Authorized'||ParCat.Partner_Level__c =='Select')){

                                if ( ParCat.Partner_Training_Discount__c== null ){
                                    ErrorFlag = True;
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                                }
                                if ( ParCat.Influence_Benefit_Type__c== null ){
                                    ErrorFlag = True;
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Influence Benefit Type'));

                                }

                            }
                            else
                            {
                                if(ParCat.Program_Version__c =='Master Reseller' && ParCat.Partner_Category__c =='Master Reseller' && (ParCat.Partner_Level__c =='Country Reseller' || ParCat.Partner_Level__c =='Master Reseller'  || ParCat.Partner_Level__c =='Elite Master Reseller' ) ){

                                    if ( ParCat.Partner_Discount__c == null || ParCat.Subscription_Partner_Discount__c == null){
                                        ErrorFlag = True;
                                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the perpetual and subscription partner discount fields'));

                                    }
                                    if ( ParCat.Partner_Training_Discount__c == null ){
                                        ErrorFlag = True;
                                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                                    }
                                    if ( ParCat.Partner_Resell_Training_Margin__c == null ){
                                        ErrorFlag = True;
                                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Resell Training Margin'));

                                    }
                                    if ( ParCat.Maintenance_Partner_Discount__c == null ){
                                        ErrorFlag = True;
                                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Maintenance Partner Discount'));

                                    }


                                }

                                else
                                {
                                    if(ParCat.Program_Version__c == 'Master Reseller' && ParCat.Partner_Category__c == 'Master Reseller' && ParCat.Partner_Level__c == 'Authorized Master Reseller' ){

                                        if ( ParCat.Partner_Training_Discount__c == null ){
                                            ErrorFlag = True;
                                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                                        }

                                    }
                                    else
                                    {
                                        if(ParCat.Program_Version__c =='Distributor' && ParCat.Partner_Category__c =='Distributor' && ParCat.Partner_Level__c =='Distributor' ){

                                            if ( ParCat.Partner_Training_Discount__c == null ){
                                                ErrorFlag = True;
                                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                                            }

                                            if ( ParCat.Maintenance_Partner_Discount__c == null ){
                                                ErrorFlag = True;
                                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Maintenance Partner Discount'));

                                            }
                                            if ( ParCat.Distributor_Discount__c == null ){
                                                ErrorFlag = True;
                                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Distributor Discount'));

                                            }
                                            if ( ParCat.Partner_Resell_Training_Margin__c== null ){
                                                ErrorFlag = True;
                                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Resell Training Margin'));

                                            }

                                        }


                                        else
                                        {
                                            if(ParCat.Program_Version__c =='OEM' && ParCat.Partner_Category__c =='OEM' && (ParCat.Partner_Level__c =='OEM' ||ParCat.Partner_Level__c =='Authorized' || ParCat.Partner_Level__c =='Select' || ParCat.Partner_Level__c =='Elite' ) ){

                                                if ( ParCat.Partner_Training_Discount__c == null ){
                                                    ErrorFlag = True;
                                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                                                }
                                            }
                                            // linus: extending the existing validation to include new "Strategic" program version. BSL-895
                                            else {
                                                if (ParCat.Program_Version__c =='Strategic' && ParCat.Partner_Category__c =='MSP' && ParCat.Partner_Level__c =='Strategic') {

                                                    if ( ParCat.Partner_Discount__c == null || ParCat.Subscription_Partner_Discount__c == null){
                                                        ErrorFlag = True;
                                                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the perpetual and subscription partner discount fields'));

                                                    }
                                                    if ( ParCat.Partner_Training_Discount__c == null ){
                                                        ErrorFlag = True;
                                                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a value in the Partner Training Discount'));

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(ErrorFlag)
        {
            ErrorFlag = False;
            return False;
        }else
        {
            return True;
        }

    }

    public PageReference edit()
    {
        isEdit = True;
        return null;
    }

    // Method used for saving the records
    public PageReference save()
    {
        if(ParCat != null){
            if(Validate())
            {
                try {
                    isEdit = False;
                    Upsert ParCat ;
                } catch (DmlException ex) {
                    ApexPages.addMessages(ex);
                }
            }
            else
            {
                return null ;
            }
        }
        return new pagereference('/'+ParCat.Id);
    }

    // method called when we click on Savenew button
    public PageReference saveAndNew()
    {
        if(ParCat != null){

            if(Validate())
            {
                try {
                    isEdit = True;
                    Upsert ParCat ;
                } catch (DmlException ex) {
                    ApexPages.addMessages(ex);
                }
            }
            else
            {
                return null ;
            }
        }

        SObject so = m_sc.getRecord();
        string s = '/' + ('' + so.get('Id')).subString(0, 3) + '/e?PartnerAccountID='+ParCat.Partner_Account__c;
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info, s));
        System.debug('sssssss- '+s);
        return new Pagereference(s);
    }
}