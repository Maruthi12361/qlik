/****************************************************************************************************
  IT-79
    CaseAttachment ContentDocumentLinkPurge
    Description: Test class for CaseContentDocumentLinkPurge
    IT-467 :Custom settings records added to maintain code coverage for CaseAttachmentPurge CRW
    2018-05-16  ext_bad, ext_vos    IT-606: Test for delete-attachment-job behavior.
    IT-2347 : Added Closure Code 'License Delivery' to exceptions - ext_bad 2020-01-29

****************************************************************************************************/
@IsTest
private class CaseContentDocumnetsLinksPurgeTest {
    static testMethod void CaseContentDocumnetsLinksPurge() {
        Account a = new Account(Name = 'Cloud Environment Test');
        insert a;

        Contact con = new Contact(LastName = 'testcontact',AccountId = a.Id);
        insert con;
        List<RecordType> entStdRecType = [SELECT Id FROM RecordType WHERE DeveloperName= 'QlikTech_Master_Support_Record_Type'
        AND SobjectType = 'Case'];
        if (entStdRecType.size() == 0) {
            System.assert(false, 'Missing record type!');
        }
        Case cse = new Case(Type='Electrical', AccountId = a.Id, ContactId = con.Id, Status = 'Closed',
                RecordTypeId = entStdRecType[0].Id);
        insert cse;
        Case cse2 = new Case(Type='Electrical2', AccountId = a.Id, ContactId = con.Id, Status = 'Closed',
                RecordTypeId = entStdRecType[0].Id);
        insert cse2;
        Case cse3 = new Case(Type='Electrical3', AccountId = a.Id, ContactId = con.Id, Status = 'In progress',
                RecordTypeId = entStdRecType[0].Id);
        insert cse3;
        Case cse4 = new Case(Type='Electrical4', AccountId = a.Id, ContactId = con.Id, Status = 'Closed', Closure_Code__c = 'License Delivery',
                RecordTypeId = entStdRecType[0].Id);
        insert cse4;


        ContentVersion contentVersion_1 = new ContentVersion(Description ='toDelete',Title = 'Penguins',PathOnClient = 'Penguins.jpg',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);
        ContentVersion contentVersion_2 = new ContentVersion(Description ='toInform',Title = 'Penguins',PathOnClient = 'Penguins.jpg',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);
        ContentVersion contentVersion_3 = new ContentVersion(Description ='toIgnore',Title = 'Penguins',PathOnClient = 'Penguins.jpg',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);
        ContentVersion contentVersion_4 = new ContentVersion(Description ='toIgnore',Title = 'Penguins',PathOnClient = 'Penguins.jpg',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);

        insert contentVersion_1;
        insert contentVersion_2;
        insert contentVersion_3;
        insert contentVersion_4;

        contentVersion_1 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_2.Id LIMIT 1];
        contentVersion_3 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_3.Id LIMIT 1];
        contentVersion_4 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_4.Id LIMIT 1];

        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        // linked with 2 outdated cases
        ContentDocumentLink cd1 = new ContentDocumentLink(LinkedEntityId= cse.Id, ContentDocumentId=contentVersion_1.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        ContentDocumentLink cd2 = new ContentDocumentLink(LinkedEntityId= cse2.Id, ContentDocumentId=contentVersion_1.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        links.add(cd1);
        links.add(cd2);
        // linked with 1 outdated and 1 actual cases
        ContentDocumentLink cd3 = new ContentDocumentLink(LinkedEntityId= cse2.Id, ContentDocumentId=contentVersion_2.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        ContentDocumentLink cd4 = new ContentDocumentLink(LinkedEntityId= cse3.Id, ContentDocumentId=contentVersion_2.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        links.add(cd3);
        links.add(cd4);
        // linked with 1 outdated case and 1 other entity
        ContentDocumentLink cd5 = new ContentDocumentLink(LinkedEntityId= cse2.Id, ContentDocumentId=contentVersion_3.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        ContentDocumentLink cd6 = new ContentDocumentLink(LinkedEntityId= a.Id, ContentDocumentId=contentVersion_3.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        links.add(cd5);
        links.add(cd6);
        // linked with 1 outdated case with LicenseDelivery Closure Code
        ContentDocumentLink cd7 = new ContentDocumentLink(LinkedEntityId= cse4.Id, ContentDocumentId=contentVersion_4.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        links.add(cd7);
        insert links;

        System.assertEquals(4, [SELECT Id FROM ContentDocument WHERE Description IN ('toDelete', 'toInform', 'toIgnore')].size());

        QTCustomSettings__c settings1 = new QTCustomSettings__c();
        settings1.Name = 'Default';
        settings1.CaseAttachmentPurgeEmail__c = 'test@test.com';
        settings1.QlikNoReplyEmailAddress__c = 'no-reply@test.com';
        insert settings1;

        Test.startTest();
        CaseContentDocumentLinkPurge.TEST_ERROR_ID = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :contentVersion_3.Id].Id;
        CaseContentDocumentLinkPurge job = new CaseContentDocumentLinkPurge();
        Database.executeBatch(job, 5);
        Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM ContentDocument WHERE Description = 'toDelete'].size());
        System.assertEquals(3, [SELECT Id FROM ContentDocument WHERE Description IN ('toInform', 'toIgnore')].size());
    }

    static testMethod void testLiveChatCase() {
        Account a = new Account(Name = 'Cloud Environment Test');
        insert a;

        Contact con = new Contact(LastName = 'testcontact',AccountId = a.Id);
        insert con;
        List<RecordType> liveChatType = [SELECT Id FROM RecordType WHERE DeveloperName= 'Live_Chat_Support_Record_Type'
        AND SobjectType = 'Case'];
        if (liveChatType.size() == 0) {
            System.assert(false, 'Missing record type!');
        }
        Case cse = new Case(Type='Electrical', AccountId = a.Id, ContactId = con.Id, Status = 'Chat Resolved',
                RecordTypeId = liveChatType[0].Id);
        insert cse;
        Case cse2 = new Case(Type='Electrical2', AccountId = a.Id, ContactId = con.Id, Status = 'Chat Resolved',
                RecordTypeId = liveChatType[0].Id);
        insert cse2;
        Case cse3 = new Case(Type='Electrical3', AccountId = a.Id, ContactId = con.Id, Status = 'In progress',
                RecordTypeId = liveChatType[0].Id);
        insert cse3;


        ContentVersion contentVersion_1 = new ContentVersion(Description ='toDelete',Title = 'Penguins',PathOnClient = 'Penguins.jpg',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);
        ContentVersion contentVersion_2 = new ContentVersion(Description ='toInform',Title = 'Penguins',PathOnClient = 'Penguins.jpg',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);
        ContentVersion contentVersion_3 = new ContentVersion(Description ='toIgnore',Title = 'Penguins',PathOnClient = 'Penguins.jpg',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);
        insert contentVersion_1;
        insert contentVersion_2;
        insert contentVersion_3;

        contentVersion_1 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_2.Id LIMIT 1];
        contentVersion_3 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_3.Id LIMIT 1];

        List<ContentDocumentLink> links = new List<ContentDocumentLink>();
        // linked with 2 outdated cases
        ContentDocumentLink cd1 = new ContentDocumentLink(LinkedEntityId= cse.Id, ContentDocumentId=contentVersion_1.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        ContentDocumentLink cd2 = new ContentDocumentLink(LinkedEntityId= cse2.Id, ContentDocumentId=contentVersion_1.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        links.add(cd1);
        links.add(cd2);
        // linked with 1 outdated and 1 actual cases
        ContentDocumentLink cd3 = new ContentDocumentLink(LinkedEntityId= cse2.Id, ContentDocumentId=contentVersion_2.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        ContentDocumentLink cd4 = new ContentDocumentLink(LinkedEntityId= cse3.Id, ContentDocumentId=contentVersion_2.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        links.add(cd3);
        links.add(cd4);
        // linked with 1 outdated case and 1 other entity
        ContentDocumentLink cd5 = new ContentDocumentLink(LinkedEntityId= cse2.Id, ContentDocumentId=contentVersion_3.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        ContentDocumentLink cd6 = new ContentDocumentLink(LinkedEntityId= a.Id, ContentDocumentId=contentVersion_3.ContentDocumentId, ShareType = 'I', Visibility= 'AllUsers');
        links.add(cd5);
        links.add(cd6);
        insert links;

        System.assertEquals(3, [SELECT Id FROM ContentDocument WHERE Description IN ('toDelete', 'toInform', 'toIgnore')].size());

        QTCustomSettings__c settings1 = new QTCustomSettings__c();
        settings1.Name = 'Default';
        settings1.CaseAttachmentPurgeEmail__c = 'test@test.com';
        settings1.QlikNoReplyEmailAddress__c = 'no-reply@test.com';
        insert settings1;

        Test.startTest();
        CaseContentDocumentLinkPurge job = new CaseContentDocumentLinkPurge();
        Database.executeBatch(job, 5);
        Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM ContentDocument WHERE Description = 'toDelete'].size());
        System.assertEquals(2, [SELECT Id FROM ContentDocument WHERE Description IN ('toInform', 'toIgnore')].size());
    }
}