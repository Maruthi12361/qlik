/*
 File Name: CaseClosedSendSurveyTest
 Moved from class TestCaseClosedSendSurvey

    This class test the CaseClosedSendSurveyHandler

    Changelog:
        2011-xx-xx  xxx     Created file by Bluewolf
        2011-04-06  MHG     Updated TestCase to work with updated trigger
                            CR #1855: https://eu1.salesforce.com/a0CD000000EUD0U
        2011-09-23  MHG     Change Support survey trigger
                            CR #2472: https://eu1.salesforce.com/a0CD000000EVfvm?srPos=0&srKp=a0C

        2011-05-28  FLD     CR #4935: Code restructure
        2011-09-20  FLD     Code restructure to avoid SQL limit error
        2012-11-20  SAN     Fix the unit test failure where contact need LeadSource to be mandatory
        2014-03-13  CCE     Adding validation test for CA001_ReasonToSuppressSurveyMandatory (at request of RM)
        2015-06-26  NAD     Modified lines 107/141 to be time zone independent.
		17.03.2017 : Rodion Vakulvsokyi
        2017-03-31  AIN     Moved surveydate to a variable, this class had a very low chance of failure if running over midnight.
        2017-04-02  TJG     Fixed System.Exception: Assertion Failed: Expected: 2016-12-22 22:08:29, Actual: 2016-12-22 00:00:00
                                  Stack Trace: Class.TestCaseClosedSendSurvey.testCaseClosuresTrial2: line 121, column 1
        2017-04-06  AIN     Fixed date assertion by checking both at current date and day before if the test runs over 12pm.
        2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.
        2020-01-29 extbad IT-2347 Added Closure Code 'License Delivery' to exceptions, removed unused code
*/
@IsTest
private class CaseClosedSendSurveyTest {

    public static testMethod void testCaseClosureCode() {
        Semaphores.SetAllSemaphoresToTrue();
        Account testAct = createAndInsertAccount();
        Contact ctct = createAndInsertContact(testAct);
        Case c1 = new Case(
                Status = 'Closed',
                Closure_Code__c = 'License Delivery',
                AccountId = testAct.Id,
                ContactId = ctct.Id
        );

        Test.startTest();
        Semaphores.SetAllSemaphoresToFalse();
        insert c1;
        Test.stopTest();

        ctct = refreshContact(ctct);

        System.assertEquals(null, ctct.Survey_Date_1__c);
        System.assertEquals(null, ctct.Survey_Date_2__c);
        System.assertEquals(null, ctct.Survey_Date_3__c);
        System.assertEquals(null, ctct.Survey_Count__c);
    }

    public static testMethod void testCaseClosures() {
        /* TRIAL 1 */
        Semaphores.SetAllSemaphoresToTrue();
        Account testAct = createAndInsertAccount();
        Contact ctct = createAndInsertContact(testAct);
        Case c1 = createCase(testAct, ctct);
        Semaphores.SetAllSemaphoresToFalse();
        insert c1;

        ctct = refreshContact(ctct);

        System.assert(ctct.Survey_Date_1__c != null);
//Sometimes the test class runs across 12pm, which means today and the survey date, which is set to datetime.now is not the same date
        System.assert((Date.today() == ctct.Survey_Date_1__c.date()) || (Date.today() - 1 == ctct.Survey_Date_1__c.date()));
        System.assertEquals(null, ctct.Survey_Date_2__c);
        System.assertEquals(null, ctct.Survey_Date_3__c);
        System.assertEquals(1, ctct.Survey_Count__c);
    }

    private static Account createAndInsertAccount() {
        Account testAct = new Account(
                Name = 'Test Account',
                Navision_Status__c = 'Something',
                QlikTech_Company__c = 'QlikTech Nordic AB'
        );
        insert testAct;
        return testAct;
    }

    private static Case createCase(Account testAct, Contact ctct) {
        Case c1 = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = ctct.Id
        );
        return c1;
    }

    private static Contact createAndInsertContact(Account testAct) {
        Contact ctct = new Contact(
                Email = 'testEmail@gmail.com',
                FirstName = 'TestFN',
                LastName = 'TestLN',
                AccountId = testAct.Id,
                LeadSource = 'WEB - Web Activity'
        );
        insert ctct;
        return ctct;
    }

    static testMethod void testCaseClosuresTrial2() {
/* TRIAL 2 */
        Semaphores.SetAllSemaphoresToTrue();
        Account a = createAndInsertAccount();

        Contact c = createAndInsertContact(a);

//Datetime surveyDate = (Date.today().addDays(-100));
        Datetime surveyDate = Datetime.newInstance(2017, 04, 06, 10, 52, 00);
        c.Survey_Count__c = null;
        c.Survey_Date_1__c = surveyDate;
        update c;

        Case c1 = createCase(a, c);
        Semaphores.SetAllSemaphoresToFalse();
        insert c1;

        c = refreshContact(c);

        System.assert(c.Survey_Date_1__c != null);
//Sometimes the test class runs across 12pm, which means today and the survey date, which is set to datetime.now is not the same date
        System.assert((Date.today() == c.Survey_Date_1__c.date()) || (Date.today() - 1 == c.Survey_Date_1__c.date()));
        System.assertEquals(surveyDate.date(), c.Survey_Date_2__c);
        System.assertEquals(null, c.Survey_Date_3__c);
        System.assertEquals(1, c.Survey_Count__c);
    }

    static testMethod void testCaseClosuresFLD() {
/* TRIAL FLD*/
        Semaphores.SetAllSemaphoresToTrue();
        Account testAct = createAndInsertAccount();
        Contact ctct = createAndInsertContact(testAct);

        Datetime surveyDate = Datetime.newInstance(2017, 04, 06, 10, 52, 00);


        ctct.Email = 'test@test.com';
//c.Survey_Date_1__c = (DateTime.now().addDays(-100));
        ctct.Survey_Date_1__c = surveyDate;
        ctct.Survey_Count__c = 0;
        update ctct;

//FLD: insert internal case and check that the survey date in contact hasnt been udpated and no survey has been sent
        List<RecordType> internalRecordTypes = [
                SELECT Id
                FROM RecordType
                WHERE Name = 'General IT Support Record Type'
        ];
        Case internalCase = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = ctct.Id,
                RecordTypeId = internalRecordTypes[0].Id
        );
        Semaphores.SetAllSemaphoresToFalse();
        insert internalCase;

        ctct = refreshContact(ctct);

        System.assertEquals(surveyDate, ctct.Survey_Date_1__c);
        System.assertEquals(0, ctct.Survey_Count__c);

        Case notInternalCase = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = ctct.Id
        );
        Semaphores.SetAllSemaphoresToFalse();
        insert notInternalCase;

        ctct = refreshContact(ctct);

//Sometimes the test class runs across 12pm, which means today and the survey date, which is set to datetime.now is not the same date
        System.assert((Date.today() == ctct.Survey_Date_1__c.date()) || (Date.today() - 1 == ctct.Survey_Date_1__c.date()));
        System.assertEquals(1, ctct.Survey_Count__c);
    }

    private static Contact refreshContact(Contact contact) {
        return [
                SELECT Id, Survey_Date_1__c, Survey_Date_2__c, Survey_Date_3__c, Survey_Count__c,
                        Email, FirstName, LastName, AccountId
                FROM Contact
                WHERE Id = :contact.Id
                LIMIT 1
        ];
    }

    static testMethod void setEmailTest() {
        //Populate the Custom setting that holds the list of users email addresses
        QTCustomSettings__c cs = new QTCustomSettings__c(
                Name = 'Default'
        );
        insert cs;
        Account testAct = new Account(
                Name = 'Test Account',
                Navision_Status__c = 'Something',
                QlikTech_Company__c = 'QlikTech Nordic AB'
        );
        insert testAct;

        Contact contact1 = new Contact(
                Email = 'testEmail@test123.com',
                FirstName = 'TestFN',
                LastName = 'TestLN',
                LeadSource = 'WEB - Web Activity',
                AccountId = testAct.Id
        );
        Contact contact2 = new Contact(
                Email = 'testEmail@test123.com',
                FirstName = 'TestFN',
                LastName = 'TestLN',
                LeadSource = 'WEB - Web Activity',
                AccountId = testAct.Id
        );
        Contact contact3 = new Contact(
                Email = 'testEmail@test123.com',
                FirstName = 'TestFN',
                LastName = 'TestLN',
                LeadSource = 'WEB - Web Activity',
                AccountId = testAct.Id
        );
        insert new List<Contact>{
                contact1, contact2, contact3
        };

        Case c1 = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = contact1.Id
        );
        Case c2 = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = contact1.Id
        );
        Case c3 = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = contact2.Id
        );
        Case c4 = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = contact3.Id
        );
        List<Case> cases = new List<Case>{
                c1, c2, c3, c4
        };
        insert cases;

//dont add contact3 to the list
        List<Messaging.SingleEmailMessage> emails = ContactServices.createEmails(new List<Contact>{
                contact1, contact2
        }, cases);
        System.assertEquals(emails.size(), 3);
    }

//CCE validation test for CA001_ReasonToSuppressSurveyMandatory
    static testMethod void emailExceptionTest() {
        Account testAct = new Account(
                Name = 'Test Account',
                Navision_Status__c = 'Something',
                QlikTech_Company__c = 'QlikTech Nordic AB'
        );
        insert testAct;

        Contact contact1 = new Contact(
                Email = 'testEmail@test123.com.sandbox',
                FirstName = 'TestFN',
                LastName = 'TestLN',
                LeadSource = 'WEB - Web Activity'
        );
        insert contact1;

        Boolean eFlg = false;
        String msg;
        Case c1 = new Case(
                Status = 'Closed',
                AccountId = testAct.Id,
                ContactId = contact1.Id,
                Suppress_Survey__c = true
        );
        try {
            insert c1;
        } catch (Exception e) {
            msg = e.getMessage();
            eFlg = true;
            System.debug('emailExceptionTest e = ' + e);
        }
//we know we should have got an exception as validation rule "CA001_ReasonToSuppressSurveyMandatory" should have kicked in
//as we created the Case with the "Suppress_Survey__c = true"
        System.assertEquals(true, eFlg);
        System.assert(msg.contains('Reason to suppress survey'));
    }

}