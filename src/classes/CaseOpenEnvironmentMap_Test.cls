/**********************************************************
*
* 2016-09-06: Ramakrishna Kini : Test Coverage for environment mapping related functionality in CaseTrigger.
* 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.
* 2019-12-19 ext_cqb IT-2279 set Product on Case when Environment.Product = 'QlikView'
*************************************************************/
@IsTest
private class CaseOpenEnvironmentMap_Test {

    /**
    *   Test Data setup for test class
    **/
    @TestSetup static void methodName() {
        Semaphores.SetAllSemaphoresToTrue();
        RecordType caseRt = [SELECT Id,Name FROM RecordType WHERE Name = 'QlikTech Master Support Record Type'];
        RecordType accRt = [SELECT Id,Name FROM RecordType WHERE Name = 'End User Account'];

        Account acc = new Account(Name = 'asasdasdas',RecordTypeId = accRt.Id);
        insert acc;

        Entitlement ent = new Entitlement(Name = 'asadsds',AccountId = acc.Id);
        insert ent;

        Environment__c env = new Environment__c(Account__c = acc.Id,Architecture__c = '64-bit',Clustered__c = true,Customer_Patch_Version__c = 'asasdasd',
                Description__c = 'asasas',Operating_System__c = 'Windows 7',Product__c = 'Qlikview',Type__c = 'Production',Version__c = '2.2.4',
                Virtual__c = true,Product_License__c = ent.Id);
        insert env;

        List<EnvironmentProduct__c> envProd = new List<EnvironmentProduct__c>{
                new EnvironmentProduct__c(
                        Environment__c = env.Id,
                        Name = '2222',
                        Product__c = '2222',
                        Version__c = '13.10.5'
                ),
                new EnvironmentProduct__c(
                        Environment__c = env.Id,
                        Name = '1111',
                        Product__c = '1111',
                        Version__c = '13.20.5'
                )
        };
        insert envProd;

        Semaphores.SetAllSemaphoresToFalse();
        Case c = new Case(RecordTypeId = caseRt.Id, EnvironmentOfCase__c = env.Id, Status = 'Open');
        Case c1 = new Case(RecordTypeId = caseRt.Id, Status = 'New');
        insert new List<Case>{
                c, c1
        };
    }

    /**
    *   Test method to check if data inserts are valid or not.
    **/
    @IsTest
    static void checkInserts() {
        Account acc = [SELECT Id,Name FROM Account WHERE Name = 'asasdasdas'];
        System.assertEquals('asasdasdas', acc.Name);

        List<Case> ct = [SELECT Id FROM Case];
        System.assertEquals(2, ct.size());
        // When
        List<Environment__c> env = [SELECT Id FROM Environment__c];
        System.assertEquals(1, env.size());
    }


    /**
    *   Test method to check if mapping is done properly with no environment products
    **/
    @IsTest
    static void checkFieldMapping() {
        Case c = [SELECT Id,Environment_Operating_System__c FROM Case WHERE Status = 'Open'][0];
        System.assertEquals('Windows 7', c.Environment_Operating_System__c);
        List<Case> ct = [SELECT Id,Status, EnvironmentOfCase__r.Product__c,Environment_Operating_System__c FROM Case WHERE Status = 'Open'];
        System.assertEquals('QlikView', ct[0].EnvironmentOfCase__r.Product__c);

    }

    /**
    *   Test method to check if mapping is done properly with environment products
    **/
    @IsTest
    static void checkEnvProductMapping() {
        List<Account> acc = [SELECT Id FROM Account WHERE Name = 'asasdasdas'];
        List<Environment__c> env = [SELECT Id FROM Environment__c WHERE Account__c = :acc[0].Id];
        List<EnvironmentProduct__c> envProd = [SELECT Name, Version__c FROM EnvironmentProduct__c WHERE Environment__c = :env[0].Id];
        Case c = [SELECT Id,Environment_Product_Details__c FROM Case WHERE Status = 'Open'][0];
        System.assertEquals(envProd[0].Name + ' v' + envProd[0].Version__c + ';' + envProd[1].Name + ' v' + envProd[1].Version__c + ';', c.Environment_Product_Details__c);
    }

    @IsTest
    static void checkCaseUpdFieldMapping() {
        List<Account> acc = [SELECT Id FROM Account WHERE Name = 'asasdasdas'];
        List<Environment__c> env = [SELECT Id FROM Environment__c WHERE Account__c = :acc[0].Id];
        Case c = [SELECT Id FROM Case WHERE Status = 'New'][0];
        Test.startTest();
        c.Status = 'Open';
        c.EnvironmentOfCase__c = env[0].Id;
        update c;
        Test.stopTest();
        Case ct = [SELECT Id,Status, EnvironmentOfCase__r.Product__c,Environment_Operating_System__c, Product__c FROM Case WHERE Status = 'Open' LIMIT 1];
        System.assertEquals(CaseEnvironmentMappingHandler.QLIK_VIEW_ENVIRONMENT_PRODUCT, ct.EnvironmentOfCase__r.Product__c);
        System.assertEquals(CaseEnvironmentMappingHandler.SERVER_PUBLISHER_CASE_PRODUCT, ct.Product__c);
    }
}