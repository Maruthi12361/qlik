/**************************************************
*
* Change Log:
*
* 28.11.17   ext_bad     CR#CHG0030519	Created.
*										Includes 'after insert' logic from TaskCaseComment trigger.
***************************************************/
public class TaskCaseCommentHandler {

    public static void handle(List<Task> newTasks) {
        List<Recordtype> rtypes = [SELECT Id FROM RecordType WHERE Name = 'Support Tasks'];

        if (rtypes.size() <= 0) return;

        //filter all tasks with record type = 'Support Tasks'
        Recordtype supportTasksRecordType = rtypes[0];
        Set<String> subjects = new Set<String>{
                'Call', 'Meeting', 'First Response', 'Other', 'Internal'
        };
        Set<Id> whatIds = new Set<Id>();
        List<Task> tasks = new List<Task>();
        for (Task t : newTasks) {
            if (t.Add_as_Case_Comment__c
                    //&&	t.RecordTypeId == supportTasksRecordType.Id
                    // && subjects.contains(t.Subject)
                    ) {
                if (t.Description.length() > 4000) {
                    t.addError('If Add as Case Comment is checked, notes cannot be longer than 4000 characters. ');
                } else {
                    tasks.add(t);
                    whatIds.add(t.WhatId);
                }
            }
        }

        //query the cases related to the tasks
        Map<Id, Case> cases = new Map<Id, Case>([SELECT Id FROM Case WHERE Id IN :whatIds]);

        //create casecomments for the filtered tasks
        List<CaseComment> caseComments = new List<CaseComment>();
        for (Task t : tasks) {
            Case c = cases.get(t.WhatId);
            if (c != null) {
                caseComments.add(new CaseComment(CommentBody = t.Description, ParentId = c.Id));
            }
        }
        insert caseComments;
    }
}