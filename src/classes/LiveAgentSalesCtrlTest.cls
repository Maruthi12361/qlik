/*******************************************************************************
*  Custom controller test class for the Live_Agent_Sales_Survey visualforce page
*  BSL-349      2018-05-09 - MTM - Sales post chat survey
*  CHG0035405 - 31-01-2019 - ext_vos - add test for saving.
*  IT-1897 -    29.05.2019 - extbad  Update 'Close Chat Survey Id'
*******************************************************************************/
@isTest
public class LiveAgentSalesCtrlTest {
    static testMethod void SalesTranscriptTest() {

        // Test the controller for the Live_Agent_Sales_Survey visualforce page    
        // Create a JSON string with chatdetails
        QuoteTestHelper.createCustomSettings();
        Case c = new Case();
        insert c;
        String cd = '{' + '"userid":"005x0000001JGgr",' + '"agentName":"Mtm"' + '}';   
        String ck = '98989898'; // Create a chat key 
        String transcripttxt = 'Sales Chat Transcript'; // Create a transcript
        LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;
        LiveChatTranscript transcript = new LiveChatTranscript(LiveChatVisitorId = visitor.Id, ChatKey='0394f8fb-6f73-40f0-bad7-9420fac5e39e');
		insert transcript;

        LiveChatTranscript newTranscript = [SELECT Id, ChatKey, Name FROM LiveChatTranscript WHERE Id=:transcript.Id limit 1];

        system.debug('chatkey to put is' + newTranscript.ChatKey);
        PageReference pageRef = Page.Live_Agent_Sales_Survey;
        pageRef.getParameters().put('transcript', transcripttxt);
        pageRef.getParameters().put('chatDetails', cd);
        pageRef.getParameters().put('chatkey', newTranscript.ChatKey);
        pageRef.getParameters().put('attachedRecords', '{"CaseId":"'+ c.Id +'"');

        Test.setCurrentPageReference(pageRef);

        Test.startTest();

        LiveAgentSalesCtrl controller = new LiveAgentSalesCtrl();
        System.assertEquals(newTranscript.Id, controller.TranscriptId);
        System.assertEquals('005x0000001JGgr', controller.sUserId);

        controller.saveTranscript();
        Test.stopTest();

        System.assertEquals(1, [select Id from Live_Agent_Transcript__c where Name = :newTranscript.ChatKey].size());
        QTCustomSettingsHier__c qtCustomSettingsHier = QTCustomSettingsHier__c.getInstance();
        Boolean isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        System.assertEquals(isSandbox? qtCustomSettingsHier.Close_Chat_Survey_Id_QA__c : qtCustomSettingsHier.Close_Chat_Survey_Id__c,
                controller.surveyId);
    }
}