/*
    AddressTriggerHelperTest
    Change log
        2017-04-02 MTM Initial development
        2018-01-26 MTM QCW-2983 Optimize StrikeIron Usage

*/
public class AddressTriggerHelper {
   
   public Static void CheckRestrictedCompany(Set<Id> Ids)
    {             	
        if(!IsApiUser())
        {		
        }       
    }
    public Static void VerifyAddress(Set<Id> Ids)
    {
        if(!IsApiUser())
        {
            List<Id> result = new List<Id>();
            result.addAll(Ids);
            QtStrikeironCom.ArrayOfString addressIds = new QtStrikeironCom.ArrayOfString();
            addressIds.string_x = result;
            QtStrikeironCom.AddressValidationServiceSoap strikeIronservice = new QtStrikeironCom.AddressValidationServiceSoap();
            strikeIronservice.timeout_x = 90000;
			QTWebserviceUtil.SetWebServiceEndpoint(strikeIronservice);
            System.debug('MTM calling strikeIronservice.AdvancedVerify');
            System.debug('Id count = ' + Ids.size());
            if(!test.isRunningTest()) {
            strikeIronservice.AdvancedVerify(addressIds);
            }
        }        
    }

	public static Set<ID> CheckforUpdates(Address__c[] updatedAddresss, Map<ID, Address__c> oldAddressMap){
        Set<ID> updatedAddressIds = new Set<ID>();
		for(Address__c newAdd  : updatedAddresss)
		{
            Address__c oldAdd = oldAddressMap.get(newAdd.Id);
            if(!(oldAdd.Country__c == newAdd.Country__c &&
			   oldAdd.City__c == newAdd.City__c &&
			   oldAdd.Address_1__c == newAdd.Address_1__c &&
			   oldAdd.Zip__c == newAdd.Zip__c &&
			   oldAdd.State_Province_Formula__c == newAdd.State_Province_Formula__c) ||
			   newAdd.Valid_Address__c == false)
			   {
					updatedAddressIds.add(newAdd.Id);
			   }
		}
		return updatedAddressIds;
    }
    public Static Boolean IsApiUser()
    {
        boolean isApiUser = false;
        id apiId = userinfo.getProfileId();
		Profile profile = [select Id, Name from profile where id = :apiId];
        if (profile.Name == 'Custom: Api Only User')
            isApiUser = true;
        return isApiUser;
    }


}