/********
* NAME : ExtOpportunityProductListCSV.cls
* Description: QCW-1832 - Export opportunities primary quote to CSV file.
*
*Change Log:
    6/2/17  Viktor Yaremenko @4front.se QCW-1832 created this class.
	8/3/17 Aslam Kamal QCW-2934 fix for failing test
    9/2/17 Srinivasan PR fix for query error												
    06-09-2017 Linus Löfberg Q2CW-2953 Setting quote recipient to conform with added validation conditions.
    11-10-2017 Shubham Gupta QCW-4102 Changed Asserts to match to QA for test fix at line 110 and 117
    13-02-2019 : Björn Andersson fixed test class error related to new duplicate rules on account and contact
******/

@isTest
public with sharing class ExtOpportunityProductListCSVTest {

    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';

    static testMethod void testExportCSV() {

        QuoteTestHelper.createCustomSettings();
        User testUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, false);

        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        testAccount.BillingCountry = 'France';
        testAccount.Territory_Country__c = 'France';
        insert testAccount;

        QlikTech_Company__c testQtCompany = [SELECT Id, QlikTech_Company_Name__c FROM QlikTech_Company__c LIMIT 1];

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        testContact.LastName = 'Wilson-Hemingway';
        testContact.FirstName = 'Daryl';
        testContact.Email = 'rodion.vakulovskyi@gmail.com';
        insert testContact;

        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
                               where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
        testPartnerAccount.Name = 'PartnerAcc';
        testPartnerAccount.BillingCountry = 'France';
        testPartnerAccount.BillingState = 'Champagn';
        testPartnerAccount.Territory_Country__c = 'France';
        insert testPartnerAccount;

        Contact testPartnerContact = QuoteTestHelper.createContact(testPartnerAccount.id);
        testPartnerContact.LastName = 'Partner Last';
        testPartnerContact.FirstName = 'Partner First';
        testPartnerContact.Email = 'test.partner@contact.com';
        insert testPartnerContact;
		/******* changes for qcw-2934 start *********/
        insert QuoteTestHelper.createPCS(testPartnerAccount);
        /******* changes for qcw-2934 end*********/	

        List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(testAccount.Id, testContact.Id, 'Billing'),
                QuoteTestHelper.createAddress(testPartnerAccount.Id, testPartnerContact.Id, 'Billing'),
                QuoteTestHelper.createAddress(testAccount.Id, testContact.Id, 'Shipping'),
                QuoteTestHelper.createAddress(testPartnerAccount.Id, testPartnerContact.Id, 'Shipping')

        };
        insert addrs;

        RecordType rTypeOpp = [SELECT Id, Name from RecordType where DeveloperName like 'Sales_QCCS'];

        

        Opportunity createdOpp = QuoteTestHelper.createOpportunity(testAccount, testPartnerAccount.Id, rTypeOpp);
        createdOpp.Opportunity_Source__c = 'Partner Registration';
        createdOpp.Partner_Contact__c = testPartnerContact.Id;
        createdOpp.Second_Partner__c = testPartnerAccount.Id;
        createdOpp.Revenue_Type__c = 'Distributor';
        insert createdOpp;

        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
        insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
        insert pbEntryTest;

        RecordType rType = [SELECT Id FROM RecordType WHERE developerName = 'Quote'];

        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testPartnerContact.id, testAccount, testPartnerAccount.Id, 'Distributor', 'Open', 'Quote', false, createdOpp.Id);
        quoteForTest.SBQQ__Primary__c = true;
        quoteForTest.SBQQ__ExpirationDate__c = Date.today().addDays(7);
        quoteForTest.Sector__c = 'Healthcare';
        quoteForTest.Industry__c = 'Healthcare';
        quoteForTest.Function__c = 'Healthcare';
        quoteForTest.Solution_Area__c = 'Healthcare - Emergency Medicine';
        quoteForTest.Quote_Recipient__c = testContact.Id;
        quoteForTest.Second_Partner__c = testPartnerAccount.Id;
        Test.startTest();
        insert quoteForTest;

        quoteForTest = [
                SELECT Id, Name, SBQQ__ExpirationDate__c
                FROM SBQQ__Quote__c
                WHERE Id = :quoteForTest.Id
        ];

        SBQQ__QuoteLine__c ql = QuoteTestHelper.createQuoteLine(quoteForTest.Id, productForTest.id, 'smth', 3,
                'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        insert ql;

        Test.stopTest();

        ApexPages.currentPage().getParameters().put('Id', createdOpp.id);
        ApexPages.StandardController stdOpp = new ApexPages.StandardController(createdOpp);
        ExtOpportunityProductListCSV eopl = new ExtOpportunityProductListCSV(stdOpp);

        system.assert(eopl.isCsvFile, 'Wrong content type');
        system.assert(String.isBlank(eopl.errorMessage));
        system.assertEquals('text/csv#' + quoteForTest.Name + '.csv', eopl.content, 'Wrong file name');
        system.assertEquals(quoteForTest.Name, eopl.pQuote.getQuoteNumber(), 'Wrong Quote Number');
        system.assertEquals(quoteForTest.SBQQ__ExpirationDate__c.format(), eopl.pQuote.getQuoteExpirationDate(), 'Wrong Quote Expiration Date');
        system.assertEquals('PartnerAcc', eopl.pQuote.getDistributorName(), 'Wrong Quote Distributor Name');
        system.assertEquals('Champagn; France', eopl.pQuote.getDistributorAddress(), 'quote.Sell_Through_Partner__r.BillingAddress');
        system.assertEquals('USD', eopl.pQuote.getCurrency(), 'Wrong Currency');
        system.assertEquals(testAccount.Name, eopl.pQuote.getEndUserName(), 'Wrong End User Name');
        system.assertNotEquals(null, eopl.pQuote.getEndUserAddress(), 'Wrong End User Address');
        system.assertEquals('Daryl Wilson-Hemingway', eopl.pQuote.getEUContactName(), 'Wrong End User Contact Name');
        system.assertEquals('rodion.vakulovskyi@gmail.com', eopl.pQuote.getEUEmail(), 'Wrong End User Email');
        system.assertEquals('PartnerAcc', eopl.pQuote.getResellerName(), 'Wrong Reseller Name');
        system.assertEquals('Champagn; France', eopl.pQuote.getResellerAddress(), 'Wrong Reseller Address');
        system.assertEquals('', eopl.pQuote.getResellerContactname(), 'Wrong Reseller Contactname');
        system.assertEquals('', eopl.pQuote.getResellerEmail(), 'Wrong Reseller Email');
        system.assertEquals('yes', eopl.pQuote.getPartnerRegisteredOpp(), 'Wrong Partner Registered Opp');
        system.assertEquals('0.00', eopl.pQuote.getLicenseAmount(), 'Wrong License Amount');
        system.assertEquals('0.00', eopl.pQuote.getQuoteTotal(), 'Wrong Quote Total');
        system.assertEquals('12', eopl.pQuote.getSupportDuration(), 'Wrong Support Duration');
        system.assertEquals('Basic', eopl.pQuote.getMaintenanceLevel(), 'Wrong Maintenance Level');

        List<ExtOpportunityProductListCSV.QuoteLineWrapper> qLines = eopl.getQuoteLines();
        system.assertEquals(1, qLines.size());
        ExtOpportunityProductListCSV.QuoteLineWrapper qLine = qLines[0];
        system.assertEquals('4750', qLine.getItem(), 'Wrong SBQQ__ProductCode__c');
        system.assertEquals('3.00', qLine.getQty(), 'Wrong SBQQ__Quantity__c');
        system.assertEquals('', qLine.getItemDescription(), 'Wrong SBQQ__Description__c');
        system.assertEquals('0.00', qLine.getUnitListPrice(), 'Wrong SBQQ__ListPrice__c');
        system.assertEquals('0.00%', qLine.getQlikUnitDiscount(), 'Wrong SBQQ__PartnerDiscount__c');
        system.assertEquals('0.00', qLine.getUnitNetPrice(), 'Wrong Unit Net Price');
        system.assertEquals('0.00', qLine.getUnitDistributorCost(), 'Wrong Unit Distributor cost');
        system.assertEquals('0.00%', qLine.getDistributorDiscount(), 'Wrong SBQQ__DistributorDiscount__c');
    }

    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                Country_Code_Two_Letter__c = countryAbbr,
                Country_Name__c = countryName,
                Subsidiary__c = subsId);
        return qlikTechIns;
    }
}