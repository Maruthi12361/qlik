/******************************************************

  Class: NFROpportunityExtensionTest
  
  Initiator: MTM CR# 16147 New Opportunity Record Type for NFR requests 
  
  Changelog:
    2015-10-10  MTM    Created file  
    
******************************************************/
@isTest 
public class NFROpportunityExtensionTest {
   
    static testmethod void testNFROpportunityExtension() {
        QTTestUtils.GlobalSetUp();
        SetUpData();
        Test.startTest();                       
        opppage = Page.NFROpportunityPage;
    Test.setCurrentPage(opppage);
    opppage.getParameters().put('Id',account.id);
        List<Opportunity> opps = new List<Opportunity>();
        nfrOpp = New Opportunity (
            Revenue_Type__c = 'Reseller Not For Resale',
            Partner_Contact_for_NFR__c = contact.Id);
        opps.add(nfrOpp);
        NFROpportunityExtension controller = new NFROpportunityExtension(new ApexPages.StandardSetController(opps));
        PageReference newPage = controller.Save();        
        System.assertNotEquals(null, newPage);               
         controller.Cancel();
        Test.stopTest();
    }
    private static void SetUpData()
    {
        user = QTTestUtils.createMockOperationsAdministrator();
        account = QTTestUtils.createMockAccount('NFR Test Account', user);
        contact = QTTestUtils.createMockContact(account.id);                
        }        
    static User user;
    static Account account;
    static Contact contact;
    static Opportunity nfrOpp;
  static PageReference opppage;
    //Static RecordType NFRRecordType = [select Id, Name from RecordType where Name = 'Partner Not For Resale'];   
}