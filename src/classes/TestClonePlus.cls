/*
	Test class: TestClonePlus for ClonePlus and ClonePlus Controller
	
	2013-04-30	CCE		Initial development	of Test class for ClonePlus, part of CR# 7927 - Rework of 
						Account Plan feature and functionality https://eu1.salesforce.com/a0CD000000YFNC8.
									
*/
@isTest
private class TestClonePlus {

    static testMethod void TestClonePlus_WithAccountPlan() {
        System.debug('TestClonePlus: Create test data: Starting');
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
		
		//this will be our head object
		Account_Plan__c TestAccountPlan = new Account_Plan__c(
			Account__c = TestPartnerAccount.Id,
			Name = 'TestAccPlan1',
			Approved_by_Sales_Productivity__c = true,
			Last_Review_Date__c = Date.Today()
		);
		insert TestAccountPlan;
		
		//and add a child object
		Event_Plan__c TestEventPlan = new Event_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Name = 'EventPlan1',
			Sales_Objective__c = 'Some stuff'
		);
		insert TestEventPlan;
		
		//Go to page
		PageReference tpageRef = Page.ClonePlus;
		Test.setCurrentPage(tpageRef);
		
		//Set Parameters that would be passed in 
		ApexPages.currentPage().getParameters().put('Id', TestAccountPlan.Id);
		ApexPages.currentPage().getParameters().put('childobjecttypes', 'Event_Plan__c');
		ApexPages.currentPage().getParameters().put('parentfieldstoblank', 'Reviewer__c,Approved_by_Sales_Productivity__c,Approved_by_Sales_Productivity_Date__c,Last_Review_Date__c,Next_Review_Date__c');
		ApexPages.currentPage().getParameters().put('fieldforrename', 'Name');
		ApexPages.currentPage().getParameters().put('settocurrentdate', 'Creation_Date__c');
		
		
		// Instantiate a new controller with all parameters in place
		ClonePlusController cpc = new ClonePlusController();
		
		//Simulate intial action call on page
		cpc.initialiseObjectsForCloning();
		
		//Simulate Click Button
		//cpc.doClone();	//for generic version uncomment this line
		
		//Check there are now two Account Plans
		Account_Plan__c [] testresults2 = [Select Id, Name from Account_Plan__c WHERE NAME like 'TestAccPlan1%' ORDER BY CREATEDDATE ASC];
		system.assertequals(2, testresults2.size());
		
		//And that one of them has " Copy" appended
		Account_Plan__c [] testresults1 = [Select Id, Name from Account_Plan__c WHERE NAME = 'TestAccPlan1 Copy' ORDER BY CREATEDDATE ASC];
		system.assertequals(1, testresults1.size());
		
		//check we also have two Event plans
		Event_Plan__c [] testresultsEv = [Select Id, Account_Plan__c from Event_Plan__c WHERE Account_Plan__c IN :testresults2 ORDER BY CREATEDDATE ASC];
		system.assertequals(2, testresultsEv.size());
		
		//and check they are attached to different Account Plans
		system.assertequals(testresults2[0].Id, testresultsEv[0].Account_Plan__c);
		system.assertequals(testresults2[1].Id, testresultsEv[1].Account_Plan__c);
    }
}