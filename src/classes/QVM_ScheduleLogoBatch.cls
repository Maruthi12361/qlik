/**********************
*
* 2015-10-30 AIN Callout moved to the start method of QVM_LogoBatch to avoid callout in schedulable apex
*
*********************/
global class QVM_ScheduleLogoBatch implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        //Initialize the batch
        //Callout moved to the start method of QVM_LogoBatch to avoid callout in schedulable apex
        QVM_LogoBatch batch = new QVM_LogoBatch();
        database.executebatch(batch,1);
    }
}