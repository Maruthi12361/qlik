/******************************************************

    Class: CaseSharingTest
    
    Changelog:
        2016-01-07  TJG     Created file
        2016-02-29  TJG     Update to handle phase 2 changes such as sell through partner
        2016-09-20  TJG     Fix test error for Q2CW project
                            System.DmlException: Update failed. First exception on row 0 with id 5002600000BpHLnAAN; 
                            first error: CANNOT_EXECUTE_FLOW_TRIGGER, The record couldn’t be saved because it failed 
                            to trigger a flow. A flow trigger failed to execute the flow with version ID 301D00000000Fz4. 
                            Flow error messages: <b>An unhandled fault has occurred in this flow</b>
                            <br>An unhandled fault has occurred while processing the flow. Please contact your system 
                            administrator for more information. Contact your administrator for help.: [] 
                            Stack Trace: Class.CaseSharingTest.testCaseEntitlement: line 205, column 1
        2018-12-04  CTS     SCU-5 Removal of variables used in Inactive Triggers.
******************************************************/
@isTest
public class CaseSharingTest {
    public static Account portalAccount;
    static final Integer NBR_OF_ACCOUNTS = 4;
    static final Integer NBR_OF_CONTACTS = 10;
    static final Id SysAdminProfileId = '00e20000000yyUzAAI';
    static final Id PortalUserRoleId  = '00E20000000vrJSEAY';
    //PRM - Sales Dependent Territory + QlikBuy
    static final Id PrmProfileId = '00e20000001OyLwAAK';
    // Partner Account Record Type ID
    static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';
    //End User Account Record Type Id
    static final Id EndUserAccRecTypeId = '01220000000DOFuAAO';
    // Qlikbuy CCS Standard II 
    static final Id OppRecTypeId = '012D0000000KEKOIA4';
    //PRM - Independent Territory + QlikBuy
    static final Id MasterResellerProfId = '00e20000001OyLmAAK';
    //01220000000DZEYAA4  Customer & Partner Portal Account Record Type
    static final Id CustomerPortalAccRecTypeId = '01220000000DZEYAA4';

    //00eD0000001PmQbIAK    Customer Portal Case Logging Access + QlikBuy
    static final Id CPCaseLoggingProfId = '00eD0000001PmQbIAK';
    
    //UIN
    @testSetup static void setup() {
        System.runAs(new User(Id = Userinfo.getUserId())) 
        {
        QuoteTestHelper.createCustomSettings();
        }

    }
    
    private static testMethod void testCaseSharing(){
        QTTestUtils.GlobalSetUp();
        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;
        ApexSharingRules.TestingParnershare = true;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //Semaphores.CaseUpdateSupportContact_HasRunAfterDelete = true;
        //Semaphores.CaseClosedSendSurvey_HasRunAfterUpdate = true;
        
        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User myUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        //User myUser = createMockPatnerUser();
        List<Contact> ctList = new List<Contact>();

        List<Account> cpAccounts = createCustomerPortalAccounts(myUser, ctList);

        List<User> pUsers = createUserFromProfileId(CPCaseLoggingProfId, ctList); 

        Integer iLen = ctList.size();

        List<Case> testCases = new List<Case>();

        for (integer i = 0; i < iLen; i++)
        {
            integer j = i > 0 ? (i - 1) : (iLen - 1);
            testCases.add(new Case(
                AccountId = cpAccounts[i].Id, 
                Subject = 'Test Case ' + i, 
                ContactId = ctList[i].Id, 
                Status = 'New', 
                OwnerId = me.Id, 
                Account_Origin__c = cpAccounts[j].Id
            ));     
        }

        Insert testCases;

        List<Id> caseIds = new List<Id>();

        for (integer i = 0; i < iLen; i++)
        {
            caseIds.add(testCases[i].Id);
        }

        List<CaseShare> caseShares = [
            SELECT
                Id, 
                UserOrGroupId, 
                CaseId, 
                CaseAccessLevel
            FROM 
                CaseShare 
            WHERE 
                RowCause = 'Manual' AND CaseId in : caseIds
            ];

        System.debug('caseShares ' + caseShares);
        
        System.assertNotEquals(0, caseShares.size());

        for (integer i = 0; i < iLen; i++)
        {
            testCases[i].Account_Origin__c = cpAccounts[i].Id;
        }

        //testCases[0].Account_Origin__c = cpAccounts[iLen-1].Id;

        Update testCases;

        Test.stopTest();
    }

    private static testMethod void testCaseEntitlement(){
        QTTestUtils.GlobalSetUp();
        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;
        ApexSharingRules.TestingParnershare = true;
        
        //CTS SCU-5 Removal of Inactive Triggers
        //Semaphores.CaseUpdateSupportContact_HasRunAfterDelete= true;
        //Semaphores.CaseUpdateSupportContact_HasRunAfterUpdate= true;
        //Semaphores.CaseClosedSendSurvey_HasRunAfterUpdate = true;

        Test.startTest();

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User myUser = createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        //User myUser = createMockPatnerUser();
        List<Contact> ctList = new List<Contact>();

        List<Account> cpAccounts = createCustomerPortalAccounts(myUser, ctList);

        List<User> pUsers = createUserFromProfileId(CPCaseLoggingProfId, ctList); 

        Integer iLen = ctList.size();

        List<Account_License__c> accLicenses = new List<Account_License__c>();

        List<Entitlement> entitlements = new List<Entitlement>();

        for (Integer j = 0; j < iLen; j++)
        {
            String licenseRef = '490135046026752' + j;
            Integer k = j < (iLen - 2) ? (j + 2) : (j - iLen + 2);
            integer i = j < (iLen - 3) ? (j + 3) : (j - iLen + 3);
            accLicenses.add(createAccountLicense(cpAccounts[j].Id, licenseRef, cpAccounts[k], cpAccounts[i]));
        }

        Insert accLicenses;


        for (Integer j = 0; j < iLen; j++)
        {
            String eName = 'License ' + j;
            entitlements.add(createEntitlement(cpAccounts[j].Id, accLicenses[j], eName));
        }

        Insert entitlements;

        List<Case> testCases = new List<Case>();

        for (integer i = 0; i < iLen; i++)
        {
            integer j = i == (iLen - 1) ? 0 : (i + 1);
            testCases.add(new Case(
                AccountId = cpAccounts[i].Id, 
                Subject = 'Test Case ' + i, 
                ContactId = ctList[i].Id, 
                EntitlementId = entitlements[i].Id,
                Entitlement = entitlements[i],
                Status = 'New', 
                OwnerId = me.Id, 
                Account_Origin__c = cpAccounts[j].Id
            ));     
        }

        Insert testCases;

        List<Id> caseIds = new List<Id>();

        for (integer i = 0; i < iLen; i++)
        {
            caseIds.add(testCases[i].Id);
        }

        List<CaseShare> caseShares = [
            SELECT
                Id, 
                UserOrGroupId, 
                CaseId, 
                CaseAccessLevel
            FROM 
                CaseShare 
            WHERE 
                RowCause = 'Manual' AND CaseId = : testCases[0].Id
            ];

        System.debug('caseShares ' + caseShares);
        
        System.assertNotEquals(0, caseShares.size());

        for (integer i = 0; i < iLen; i++)
        {
            testCases[i].Account_Origin__c = cpAccounts[i].Id;
            testCases[i].Account_License__c = null;
            Id spbId = accLicenses[i].Support_Provided_By__c;
            accLicenses[i].Support_Provided_By__c = accLicenses[i].Selling_Partner__c;
            accLicenses[i].Selling_Partner__c = spbId;
        }

        Update accLicenses;

        //testCases[0].Account_Origin__c = cpAccounts[iLen-1].Id;

        Update testCases;

        Test.stopTest();
    }

    private static User createMockPatnerUser(User me){

        //UserRole portalRole = [Select Id From UserRole Where PortalType = 'Partner'and PortalRole = 'Executive' Limit 1];

        //System.debug('portalRole ' + portalRole);

        User aUser = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');
        
        aUser.IsPortalEnabled = true;
        
        System.runAs ( me ) 
        {
            update aUser;
        }

        return aUser;
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me ) 
    {
        User u 
            = new User(
               alias = 'adminUsr', email='adminUser@tjtest.com.test',
                Emailencodingkey='UTF-8', lastname='adminTest', 
                Languagelocalekey='en_US', localesidkey='en_US',
                Profileid = profId,
                Timezonesidkey='America/Los_Angeles', 
                //ContactId = cont.Id,
                UserRoleId = roleId,
                Username= System.now().millisecond() + '_' + '_newuser@jttest.com.test'
            );

        System.runAs ( me ) 
        {
            insert u;
        }

        return u;
    }

    // create customer portal accounts
    private static List<Account> createCustomerPortalAccounts(User aUser, List<Contact> clst)
    {
        List<Account> alst = new List<Account>();
        User user1 = createMockPatnerUser(aUser);
        Boolean oddIndex = false;
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser){
            for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++){

                Account pAccount = new Account(
                    Billing_Country_Code__c = qtComp.Id,
                    QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                    Name = 'partneracc' + i,
                    RecordTypeId = CustomerPortalAccRecTypeId
                    );
                if (oddIndex)
                {
                    pAccount.Owner = user1;
                }
                oddIndex = !oddIndex;
                alst.add(pAccount);  
            }
            Insert alst;

            for (Integer i = 0; i<NBR_OF_ACCOUNTS; i++){
                Contact contact = new Contact( FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = alst[i].Id,
                Email = 'tc' + i + '@test.com');
                clst.add(contact);  
            }
            Insert clst;
        } 
        return alst;
    }

    private static List<Account> createEndUserAccounts(User aUser, List<Contact> clst)
    {
        List<Account> alst = new List<Account>();

        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('MockQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser){
            for (Integer i = 0; i < NBR_OF_CONTACTS; i++){

                Account endAccount = new Account(
                    Billing_Country_Code__c = qtComp.Id,
                    QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                    Name = 'partneracc' + i,
                    RecordTypeId = EndUserAccRecTypeId
                    );
                alst.add(endAccount);  
            }
            Insert alst;

            for(Integer i = 0; i < NBR_OF_CONTACTS; i++){
                Contact contact = new Contact( FirstName = 'EUserCon'+i,
                Lastname = 'testsEU',
                AccountId = alst[i].Id,
                Email = 'nuser' + i + '@test.com');
                clst.add(contact);  
            }
            Insert clst;
        } 
        return alst;
    }

    public static List<User> createUserFromProfileId(Id profId, List<Contact> clist) {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<User> usrs = new List<User>();
        System.runAs ( thisUser ) {
            integer ndx = 1;
            for (Contact ct : clist)
            {
                User u = new User(
                    alias = 'nuUser' + ndx, 
                    email = 'puser'+ ndx + '@tjtest.com.test',
                    Emailencodingkey = 'UTF-8', 
                    lastname = 'Test' + ndx, 
                    Languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',
                    Profileid = profId,
                    Timezonesidkey='America/Los_Angeles',
                    ContactId = ct.Id,
                    Username= 'puser' + ndx + '@tjtest.com.test'
                );

                usrs.add(u);
                
                ndx++;
            }

            insert usrs;

            for (integer i = 0; i < usrs.size(); i++)
            {
                usrs[i].IsPortalEnabled = true;                        
            }

            update usrs;
        }

        return usrs;
    }    

    public static Account_License__c createAccountLicense(Id accountId, String licReference, Account cstp, Account spb)
    {
        Account_License__c aLicense =
            new Account_License__c(
                Name = licReference,
                Selling_Partner__c = cstp.Id, 
                Product__c = 'QSERVER', 
                Account__c = accountId,
                Support_From__c = Date.today().addDays(-10), 
                Support_To__c = Date.today().addDays(100),
                Support_Provided_By__c = spb.Id,
                O_S__c = 'Windows 2008', 
                Environment__c = 'MS Windows', 
                Application__c = 'QVSERVER'
            );
        return aLicense;
    }

    public static Entitlement createEntitlement(Id accountId, Account_License__c aLicense, String entName)
    {
        Entitlement myEntitlement = new Entitlement(
            AccountId = accountId,
            Automatic__c = true, 
            Account_License__c = aLicense.Id,
            Name = entName, 
            StartDate=System.today() - 50,
            Type = 'Phone Support', 
            EndDate = System.today() + 50);
        return myEntitlement;
    }
}