/**
    This class contains utility methods for creating test data
    
*/

public class TestDataFactory {

    public static QlikTech_Company__c createQlikTechCompany(String name, String companyName, String countryName, String currencyIsoCode) {
        Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c qtc = new QlikTech_Company__c(
                name = name,
                Subsidiary__c = testSubs.id,
                QlikTech_Company_Name__c = companyName,
                Country_Name__c = countryName,
                CurrencyIsoCode = currencyIsoCode);
        return qtc;
    }


    public static account createAccount(String accountName, QlikTech_Company__c qtc) {
        Account act = new Account(
                name=accountName,
                QlikTech_Company__c = qtc.QlikTech_Company_Name__c,
                Billing_Country_Code__c = qtc.Id,
                BillingStreet = 'Street 123',
                BillingCity = 'City 123',
                BillingState = 'S 123',
                BillingCountry = qtc.Country_Name__c,

                ShippingStreet = 'Street 123',
                ShippingCity = 'City 123',
                ShippingState = 'S 123',
                ShippingCountry = qtc.Country_Name__c,
                Account_Creation_Reason__c = 'Test');
        return act;
    }

    public static Diagnostic__kav createDiagnosticKnowledgeArticle(String title, String summary, String urlName, String language) {
        Diagnostic__kav diagnosticKav = new Diagnostic__kav(Title = title, Summary = summary, URLName = urlName, Language = language);
        return diagnosticKav;
    }

    public static Basic__kav createBasicKnowledgeArticle(String title, String summary, String urlName, String language) {
        Basic__kav diagnosticKav = new Basic__kav(Title = title, Summary = summary, URLName = urlName, Language = language);
        return diagnosticKav;
    }

    public static User createUser(String profileId, String userName, String nickname, String contactId) {
        User user = new User(
                LastName = 'Smith',
                Alias = 'jsmith',
                Email = 'jsmith@email.com',
                Username = userName,
                CommunityNickname = nickname,
                ProfileId = profileId,
                TimeZoneSidKey = 'Europe/London',
                LocaleSidKey = 'en_GB',
                EmailEncodingKey = 'ISO-8859-1',
                LanguageLocaleKey = 'en_US',
                ContactId = contactId);//,
        //UserPermissionsKnowledgeUser  = true);

        return user;
    }

    public static Contact createContact(String firstName, String lastName, String email, String phoneNumber, String accountId) {
        Contact cont = new Contact ( firstName = firstName, lastName = lastName, email = email, phone = phoneNumber, accountId = accountId);

        return cont;
    }

    public static Category_Lookup__c createCategoryLookups(String attachmentUpload, String categoryLevel1, String categoryLevel2,
            String categoryLevel3, Boolean environment, String channel, String question1,
            String question2, String question3, String question4, String question5, String articleNumber,
            String severity, Boolean unauthenticated, String recordTypeName, String caseOwnerName) {

        Category_Lookup__c categoryLookup = new Category_Lookup__c (
                Attachment_Upload_Prompt__c = attachmentUpload,
                Name = categoryLevel1,
                Category_Level__c = categoryLevel2,
                Category_Level_3__c = categoryLevel3,
                Environment__c = environment,
                //Channel__c = channel,
                Question_Prompt_1__c = question1,
                Question_Prompt_2__c = question2,
                Question_Prompt_3__c = question3,
                Question_Prompt_4__c = question4,
                Question_Prompt_5__c = question5,
                Article__c = articleNumber,
                Severity__c = severity,
                Unauthenticated__c = unauthenticated,
                Record_Type_Name__c = recordTypeName,
                Case_Owner__c = caseOwnerName);

        return categoryLookup;
    }

    public static Entitlement createLicense(String accountId, String licenseReference, String entitlementName) {

        Entitlement productLicense = new Entitlement(License_Reference__c = licenseReference, AccountId = accountId, Type = 'Phone Support', EndDate = System.today()+30, StartDate=System.today() - 30, Name = entitlementName, Automatic__c = true);
        return productLicense;
    }
    public static Entitlement createExpiredLicense(String accountId, String licenseReference, String entitlementName) {

        Entitlement productLicense = new Entitlement(License_Reference__c = licenseReference, AccountId = accountId, Type = 'Phone Support', EndDate = System.today()-30, StartDate=System.today() - 60, Name = entitlementName, Automatic__c = true);
        return productLicense;
    }

    public static Environment__c createEnvironment(String accountId, String environmentName, String operatingSystem, String productLicense, String type, String version) {
        Environment__c environment = new Environment__c(Account__c = accountId, Name = environmentName, Operating_System__c = operatingSystem, Product_License__c = productLicense, Type__c = type, Version__c = version);

        return environment;
    }

    public static Case createCase(String subject, String description, String ownerId, String severity,
            String contactId, String accountId, String recordTypeId, String origin,
            Boolean businessCritical, String businessJustification, String environmentOfCaseId,
            String entitlementId, String licenseNo, String environment, String os) {

        Case caseObj = new Case(Subject = subject, Description = description, OwnerId = ownerId, Severity__c = severity,
                ContactId = contactId, AccountId = accountId, RecordTypeId = recordTypeId, Origin = origin,
                Business_Critical__c = businessCritical, Business_Justification__c = businessJustification, EnvironmentOfCase__c = environmentOfCaseId,
                EntitlementId = entitlementId, License_No__c = licenseNo, Environment__c = environment, O_S__c = os);

        return caseObj;
    }

    public static FeedItem createFeedItem(String body, String parentId) {
        FeedItem feedItem = new FeedItem(ParentId = parentId, Body = body);
        return feedItem;
    }

    public static EntitySubscription createEntitySubscription(String networkId, String subscriberId, String ParentId) {
        EntitySubscription entitySubscription = new EntitySubscription(NetworkId = networkId, SubscriberId = subscriberId, ParentId = ParentId);
        return entitySubscription;
    }

    public static Survey__c createSurvey(String accountId, String caseId, String name, String notes, String contactId, String caseNumber, String caseSubject) {

        Survey__c survey = new Survey__c(Account_ID__c = accountId,Contact_ID__c = contactId, Notes__c = notes, Name = name, Onboard_Q21__c = 'Test', Case_Number__c = caseNumber, Case_ID__c = caseId, Case_Subject__c = caseSubject);
        return survey;
    }

    public static QS_Persona_Category_Mapping__c createPersonaCategoryMapping(String personaName, String articleCategories, String communityForums) {

        QS_Persona_Category_Mapping__c personaCategoryMapping = new QS_Persona_Category_Mapping__c(Name = personaName, Article_Categories__c = articleCategories, Community_Forums__c = communityForums);
        return personaCategoryMapping;
    }

    public static QS_HomePage_Chatter_Announcement__c createChatterAnnouncement(String name, String body, String postedBy, String title) {
        QS_HomePage_Chatter_Announcement__c chatterAnnouncement = new QS_HomePage_Chatter_Announcement__c(Name = name, Body__c = body, Posted_By__c = postedBy, Title__c = title);
        return chatterAnnouncement;
    }

    public static CollaborationGroup createChatterGroup(Id ownerId, Id networkId, String groupName, String infTitle, String infBody, String infDescription) {

        CollaborationGroup chatterGroup = new CollaborationGroup(OwnerId = ownerId, NetworkId = networkId, Name = groupName, InformationTitle = infTitle, InformationBody = infBody, Description = infDescription, CollaborationType = 'Public');
        //
        return chatterGroup;
    }


    public static QS_Qoncierge_Country_License_Support__c createCountryLicenseSupport(String countryName, String accountId) {
        QS_Qoncierge_Country_License_Support__c countrySupportLicense =
                new QS_Qoncierge_Country_License_Support__c(Name = countryName,Temp_Account__c = accountId);
        return countrySupportLicense;
    }

    public static QS_Support_Contact_Info__c supportContactInfo(String countryName, String phone, String email, String hoursInfo) {
        QS_Support_Contact_Info__c supportContactInfo = new QS_Support_Contact_Info__c(Name = countryName, Phone__c = phone, Hours_Information__c = hoursInfo, Email__c = email);
        return supportContactInfo;
    }

    /*public static CollaborationGroupFeed createGroupFeed(String body, Id collaborationGroupId, String title) {
        
        CollaborationGroupFeed collaborationGroupFeed = new CollaborationGroupFeed(Body = body, Type= 'TextPost', NetworkScope = 'AllNetworks', ParentId = collaborationGroupId, Title = title, Visibility = 'AllUsers');
        return collaborationGroupFeed;
    }*/

}