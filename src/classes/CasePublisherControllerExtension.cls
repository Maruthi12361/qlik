/*************************************************************
*Author: MTM
* 
* 2014-03-11 MTM CR# 8444 MTM Automation of Lead generation from Cases
* 2015-05-27 CCE CR# 32912 - Update Custom Publisher Action for Lead Generation on Cases
* 2018-04-18  ext_bad CHG0033720   Change org wide email, select it from custom settings.
*
**************************************************************/

public class CasePublisherControllerExtension 
{
        public CasePublisherControllerExtension(ApexPages.StandardController stdController) 
        {
                caseController = stdController;
                if(!Test.isRunningTest()) {
                    List<String> fieldNames = new List<String>{'Contact.Name', 'Origin', 'CaseNumber', 'Responsible_Partner__c', 'Account_Type_Formula__c', 'Contact', 'Contact.Phone', 'Contact.Email', 'Account.Name', 'Account.Owner.Email'};
                    caseController.Addfields(fieldNames);
                }
                
                CaseObj = (Case)stdController.getRecord();
                Description = '';            
                Email = CaseObj.Contact.Email;
                PhoneNumber = CaseObj.Contact.Phone;
                AccountName = CaseObj.Account.Name;
                CaseId = CaseObj.Id;
                ConsentCheckBox = false;
                NotInSFCheckBox = false;
                bNext = false;
                bSubmit = true;
                ContactDetails = new Contact();
        }
               
        public PageReference NextStep()
        { 
              if(CaseObj.ContactId != null)
              {
                bNext=true;
                contactDetails.id = CaseObj.ContactId;
                System.debug('contactDetails.Name' + contactDetails.Name);
              }
              return null;                     
        }            
         /*******************************************************
         *  Submit button Action
         * Create Campaigs member
         * Notify by E-mail
         * Notify by Chatter message
         *
         *******************************************************/               
        public void SubmitCampaign()
        {                               
            System.Debug('SubmitCampaign TypeOfLead  = ' + TypeOfLead);
            System.Debug('SubmitCampaign Description  = ' + Description);
            
            Comments = String.Format('{0}\n Origin: Support Case {1}, UserName: {2}, User Email: {3}', new String[]{Description, CaseObj.CaseNumber, UserInfo.getName(),UserInfo.getUserEmail()});
            
            if (TypeOfLead == System.Label.Software_or_License) {            
                    CampaignName = System.Label.Global_Service_Campaign;
            }
            else {  // “Training/Consulting/Support”
                    CampaignName = System.Label.Global_Service_Other_Campaign;
            }
               
            if(!NotInSFCheckBox)
            {
                List<Campaign> campaignIds = new List<Campaign>();
                CampaignMember cMember = new CampaignMember ();                                
                
                campaignIds  = [select Name, Id from Campaign where Name =: CampaignName ];
                
                if (campaignIds.Size() >0)
                {
                    cMember.campaignId = campaignIds[0].Id;
                    cMember.ContactId = ContactDetails.Id;//CaseObj.ContactId;
                    cMember.Comments__c = Comments;                    
                    cMember.Status = 'Target';                    
                    try
                    {
                        insert cMember ;
                    }
                    catch(System.DMLException ex)
                    {
                        ApexPages.addMessages(ex);
                        return;                                           
                    }
                    cMemberId = cMember.Id;
                    System.Debug('SubmitCampaign Comments__c  = ' + cMember.Comments__c);                      
                }
                else
                {
                    System.Debug('Campaign does not exist');
                    ApexPages.Message errMessage = new ApexPages.Message(ApexPages.Severity.Warning, 'Campaign does not exist');
                    ApexPages.addMessage(errMessage);
                    return;
                }            
            }
            SendEmail();
                            
            CreateChatter();                                                   
        }
        /*******************************************************
         *  Notify servicesales@qlikview.com and Account owner about the lead generation 
         *  by E-mail to Account owner and servicesales@qlikview.com 
         *
         *******************************************************/        
        private void SendEmail()
        {               
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String messageBody = '';
                String[] toAddresses = new String[] {System.Label.Qlik_service_email_address, ContactDetails.Account.Owner.Email};//{'servicesales@qlikview.com', ContactDetails.Account.Owner.Email};                
                mail.setToAddresses(toAddresses); 
                
                mail.setSaveAsActivity(false);
        
                //set the FROM address by setting the org-wide address to use
                String Qlik_noreply_email_address = '';
                if(QTCustomSettings__c.getInstance('Default') != null) {
                    Qlik_noreply_email_address = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
                    System.Debug('SendEmail: Qlik_noreply_email_address = ' + Qlik_noreply_email_address);                
                }
                OrgwideEmailAddress[] orgwideaddress = [select id from orgwideEmailAddress where displayname = :Qlik_noreply_email_address Limit 1]; 
                //OrgwideEmailAddress[] orgwideaddress = [select id from orgwideEmailAddress where displayname = :System.Label.Qlik_noreply_email_address]; 
                mail.setOrgWideEmailAddressId(orgwideaddress[0].id);

                mail.setSubject('New Lead for ' + CampaignName); 
                mail.setUseSignature(false);
                if(!NotInSFCheckBox)
                {
                    messageBody = String.Format('Campaign Member created. To view Campaign Member <a href={0}/{1}> Click here.</a> <br>', new String[] {URL.getSalesforceBaseUrl().toExternalForm(),cMemberId});
                }
                else
                {
                    messageBody = System.Label.Lead_Not_Matched + '\n';
                }
                messageBody += CampaignDetails.replaceAll('\n', '<br>');
                
                System.Debug('MessageBody' + messageBody);
                mail.setHtmlBody(messageBody);
                
                try {
                    
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                }
                catch (system.EmailException ex) {
                    // If the sending of the email fails, due to non existing
                    // email address, do nothing
                }
        }
        /*******************************************************
         *  Notify the lead generation by chatter
         *   
         *
         *******************************************************/         
        
        private void CreateChatter()
        {
                FeedItem post = new FeedItem();
                post.ParentId = CaseId;
                String chatterBody = '';
                System.Debug('CreateChatter CaseObj.Id: ' + CaseObj.Id);
                if(!NotInSFCheckBox)
                {
                    chatterBody = System.Label.Lead_Submitted;
                }
                else
                {
                    chatterBody= System.Label.Lead_Not_Matched;
                }
                post.Body = chatterBody +  '\n' + CampaignDetails;
                insert post;
        }
     /*******************************************************
     *  Lead generation details
     *   
     *
     *******************************************************/       
        private String CampaignDetails
        {
            get
            {
                if (NotInSFCheckBox) {
                    return String.Format('Contact: {0}\nCompany (Account): {1}\nPhone: {2}\nEmail: {3}\nType of Lead: {4}\nComment: {5}',
                                                      new String[] {ContactName, AccountNameNotInSF, PhoneNumberNotInSF, EmailNotInSF, TypeOfLead, Comments});
                } else {
                    return String.Format('Contact: {0}\nCompany (Account): {1}\nPhone: {2}\nEmail: {3}\nType of Lead: {4}\nComment: {5}',
                                                      new String[] {contactDetails.Name, AccountName, PhoneNumber, Email, TypeOfLead, Comments});
                }
            }
        }
        
        public String getMessage()
        {
                return System.Label.Lead_Warning_Message;
                
        }
        public Boolean IsPartner 
        {
                get
                {               
                        return (String.isNotBlank(caseObj.Account_Type_Formula__c) && caseObj.Account_Type_Formula__c.contains('Partner')) || 
                                   String.isNotBlank(caseObj.Responsible_Partner__c);
                }
                set;
        }
        public Contact ContactDetails
        {
            get
            {
                    if(CaseObj.ContactId != null)
                    {
                            ContactDetails = [SELECT Id, Name, EMail, Phone, Account.Name, Account.Owner.Email FROM Contact where Id=:CaseObj.ContactId];
                            EMail = ContactDetails.Email;
                            PhoneNumber = ContactDetails.Phone;
                            AccountName = ContactDetails.Account.Name;
                            return ContactDetails;
                    }
                    return null;                  
            }
            set;
        }
        
        public Boolean EnableNextButton
        {
            get
            {               
                return (ConsentCheckBox &&
                    ((String.isNotBlank(ContactName) && NotInSFCheckBox) || (String.isNotBlank(CaseObj.ContactId) && !NotInSFCheckBox)) &&
                    ((String.isNotBlank(AccountNameNotInSF) && NotInSFCheckBox) || (String.isNotBlank(AccountName) && !NotInSFCheckBox)) &&
                    ((String.isNotBlank(PhoneNumberNotInSF) && NotInSFCheckBox) || (String.isNotBlank(PhoneNumber) && !NotInSFCheckBox)) &&
                    ((String.isNotBlank(EMailNotInSF) && NotInSFCheckBox) || (String.isNotBlank(Email) && !NotInSFCheckBox))
                );
            }                          
        }
        
        public Boolean RenderNext
        {
            get{ return (!IsPartner) && (!bNext) ;}
        }
        public String Description
        { 
          get; 
          set
          {
              Description = value; 
              if ( String.IsBlank(value))
              {
                  bSubmit = true;
              }
              else
                  bSubmit = false;
              
          }
        }
        
        private Case CaseObj;
        private Id CaseId;
        public Id cMemberId;
        private String Comments;
        public String CampaignName;
        private ApexPages.StandardController  caseController;
        public String ContactName { get; set; }       
        public String AccountName { get; set; }
        public String AccountNameNotInSF { get; set; }
        public String PhoneNumber { get; set; }        
        public String PhoneNumberNotInSF { get; set; }
        public String EMail { get; set; }
        public String EMailNotInSF { get; set; }
        public String TypeOfLead {get; set; }
        public Boolean NotInSFCheckBox {get; set;}
        public Boolean ConsentCheckBox {get; set;}
        public Boolean bNext{get; set;}
        public Boolean bSubmit{get; set;}
}