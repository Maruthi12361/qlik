/*
* File CustomInfluencerLookupControllerTest
    * @description : Unit test for CustomInfluencerLookupController
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       04.01.2018    Pramod Kumar V     Created Class

*/
@isTest(seeAlldata=false)
private class CustomInfluencerLookupControllerTest{    
   
    static testMethod void test_CustomInfluencerLookupController() {
    User testUser = [Select id From User where id =: UserInfo.getUserId()];
    Account  testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true); 
     Contact cont = QuoteTestHelper.createContact(testAccount.id);
         
          insert cont;
    
    Account_Plan__c ap=new Account_Plan__c(Account__c=testAccount.id,name='test');
    insert ap;
    PageReference pageRef = Page.CustomInfluencerLookup;
    pageRef.getParameters().put('lksrch', 'test');
    pageRef.getParameters().put('accplanid', String.valueOf(ap.Id));
    //pageRef.getParameters().put('accplanid', 'TestCompany');

    Test.setCurrentPage(pageRef);
 
    CustomInfluencerLookupController CiLc=new CustomInfluencerLookupController();
    CiLc.getFormTag();
    CiLc.getTextBox();
    CiLc.search();
      }
    }