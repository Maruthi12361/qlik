/*
* File ModifyInActiveUsersTest
    * @description : Unit test for ModifyInActiveUsers
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       05.08.2018   ext_bjd             Created Class
    2       11.06.2019   ext_bjd             Added fix to avoid System.LimitException: Too many query rows: 50001 from
    *                                        CustomUserTriggerHandler.processUserAssignment
*/

@IsTest
public class ModifyInActiveUsersTest {
    @IsTest
    public static void inActiveUserTest() {
        List<User> updatedTestUsers = new List<User>();
        List<User> testUsers = new List<User>();

        //disable CustomUserTriggerHandler.processUserAssignment
        Q2CWSettings__c customSettings = Q2CWSettings__c.getInstance(UserInfo.getOrganizationId());
        if (customSettings.Disable_User_trigger__c == false) {
            customSettings.Disable_User_trigger__c = true;
            upsert customSettings;
        }

        User userAnotherCountry = new User();
        for (Integer i = 0; i <= 2; i++) {
            testUsers.add(QTTestUtils.createMockUserForProfile('PRM - Base', true));
        }
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User thisObjUser = new User(Alias = 'standt', Email = 'standtest@qliktech.com.appdev',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles', username = 'standtest@qliktech.com.appdev');
        insert thisObjUser;

        //Create QlikTech Company
        User thisUserOpAdmin = QTTestUtils.createMockOperationsAdministrator();
        System.runAs (thisUserOpAdmin) {
            // for Sweden
            Subsidiary__c subSweden = new Subsidiary__c(
                    Legal_Country__c = 'Sweden',
                    Legal_Entity_Name__c = 'QlikTech Sweden');
            insert subSweden;

            QlikTech_Company__c swedenCompany = new QlikTech_Company__c(
                    Name = 'Sweden',
                    QlikTech_Company_Name__c = 'Nordics',
                    Country_Name__c = 'Sweden',
                    QlikTech_Region__c = 'Sweden',
                    QlikTech_Sub_Region__c = 'Sweden',
                    QlikTech_Operating_Region__c = 'Sweden',
                    Language__c = 'Sweden',
                    SLX_Sec_Code_ID__c = 'Sweden',
                    Subsidiary__c = subSweden.Id);
            insert swedenCompany;

            // for USA
            Subsidiary__c subUsa = new Subsidiary__c(
                    Legal_Country__c = 'USA',
                    Legal_Entity_Name__c = 'QlikTech USA');
            insert subUsa;

            QlikTech_Company__c usaCompany = new QlikTech_Company__c(
                    Name = 'USA',
                    QlikTech_Company_Name__c = 'USA Nord',
                    Country_Name__c = 'USA',
                    QlikTech_Region__c = 'USA',
                    QlikTech_Sub_Region__c = 'USA',
                    QlikTech_Operating_Region__c = 'USA',
                    Language__c = 'USA',
                    SLX_Sec_Code_ID__c = 'US',
                    Subsidiary__c = subUsa.Id);
            insert usaCompany;
        }

        User thisUserAdmin = QTTestUtils.createMockSystemAdministrator();
        System.runAs (thisUserAdmin) {
            // update users
            for (User us : testUsers) {
                us.Country = 'Sweden';
                us.IsActive = false;
                us.Email = us.Email + '.invalid';
                us.activeOnlyInSandboxes__c = true;
                us.Last_Profile__c = 'PRM - Base (READ ONLY)';
                updatedTestUsers.add(us);
            }
            update updatedTestUsers;

            // update Country(USA) for one user
            userAnotherCountry = updatedTestUsers.get(0);
            userAnotherCountry.Country = 'USA';
            update userAnotherCountry;

            // delete QlikTech Company which name is USA
            delete [SELECT Id, Country_Name__c FROM QlikTech_Company__c WHERE Country_Name__c = 'USA'];
            delete [SELECT Id, Legal_Country__c FROM Subsidiary__c WHERE Legal_Country__c = 'USA'];
        }

        Test.startTest();

        System.runAs (thisObjUser) {
            ModifyInActiveUsers.runUserUpdatesWithLicenses();
        }

        Test.stopTest();

        List<User> restUsrWithSweden = [SELECT Id, Name, Country, Email, IsActive, Profile.Name FROM User WHERE Id IN :updatedTestUsers AND Country = 'Sweden'];
        for (User us : restUsrWithSweden) {
            System.debug('!!! test user is : ' + us);
            System.assertEquals(true, us.IsActive);
            System.assertEquals('Sweden', us.Country);
            System.assertEquals('PRM - Base (READ ONLY)', us.Profile.Name);
            System.assertEquals(false, us.Email.contains('.invalid'));
        }

        User testUsrWithUsa = [SELECT Id, Name, Country, Email, IsActive, Profile.Name FROM User WHERE Id = :userAnotherCountry.Id LIMIT 1];
        System.debug('!!! testUsrWithUSA is : ' + testUsrWithUsa);
        System.assertEquals(true, testUsrWithUsa.IsActive);
    }
}
