/**     * File Name:AccLicenseCreateProdLicenseHandlerTest
        * Description : Test class for AccLicenseCreateProdLicenseHandler
        * @author : AIN
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 April 18th 2017 AIN Added test logic
        
        10 April 2018 Pramod Kumar V Test Class Coverage ITRM-20 
        5 July   2019 IT-1995 ext_bad  Add Subscription__c sync
*/
@isTest
private class AccLicenseCreateProdLicenseHandlerTest {
    
    public static Account_License__c accLic;
    public static string testLicenseNumber = '1234567890123456';
    @isTest static void test_method_one() {
        InsertTestData(true);

        insert accLic;

        accLic.INT_NetSuite_Support_Level__c = 'Premium';
        update accLic;
        //Increase code coverage
        List<Account_License__c> inputList=new List<Account_License__c> ();
        Map<Id, Account_License__c> inputMap=new Map<Id, Account_License__c>();
        inputList.add(accLic);
        inputMap.put(accLic.id,accLic);
        AccLicenseCreateProdLicenseHandler.onAfterInsert(inputList,inputMap);
       
        //pramod
        Entitlement ent = [select id, name, RecordType.Name, Account_License__c, Subscription__c from Entitlement where name = :testLicenseNumber and Account_License__c =: accLic.Id limit 1];
        System.debug('Ent.RecordType.Name: ' + ent.RecordType.Name);
        system.assertEquals(ent.Account_License__c, accLic.Id);
        system.assertEquals(ent.Subscription__c, accLic.Subscription__c);
        system.assertEquals(ent.RecordType.Name, 'Standard');

        delete accLic;
        ent = [select id, name, RecordType.Name, Account_License__c from Entitlement where name = :testLicenseNumber limit 1];
        system.assertEquals(ent.RecordType.Name, 'Obsolete');
    }
    
    @isTest static void test_method_two() {

        InsertTestData(true);

        insert accLic;

        Entitlement ent = [select id, name, Account_License__c from Entitlement where name = :testLicenseNumber];
        delete ent;

        accLic.INT_NetSuite_Support_Level__c = 'Premium';
        update accLic;

        delete accLic;
    }
    @isTest static void test_method_three() {

        InsertTestData(false);

        insert accLic;

        Entitlement ent = [select id, name from Entitlement where name = :testLicenseNumber];
        delete ent;

        accLic.INT_NetSuite_Support_Level__c = 'Premium';
        update accLic;

        delete accLic;
    }
    public static void InsertTestData(boolean createSupportOffice)
    {
        AccLicenseCreateProdLicenseHandler handler = new AccLicenseCreateProdLicenseHandler();
        QTTestUtils.GlobalSetUp();
        Date today = Date.Today();
        
        User user = [select id from user where id = :UserInfo.getUserId()];

        Id lundBusinesshours = [select Id from BusinessHours where Name = 'Lund' limit 1].Id;
        Support_Office__c cancunSupportOffice = createAndInsertSupportOffice(lundBusinesshours);

        Account acc = QTTestUtils.createMockAccount('Test account', user);
        if(createSupportOffice)
            acc.Support_Office__c = cancunSupportOffice.Id;
        else
            acc.Support_Office__c = null;
        update acc;

        Zuora__CustomerAccount__c bill = Z_TestFactory.createBillingAccount(acc, 'testBill');
        insert bill;
        Zuora__Subscription__c sub = Z_TestFactory.MakeSubscription(bill, acc, 'testSub');
        insert sub;

        accLic = new Account_License__c();
        accLic.Name = testLicenseNumber;
        accLic.Account__c = acc.Id;
        accLic.Support_From__c = today;
        accLic.Support_To__c = today+1;
        acclic.INT_NetSuite_Support_Level__c = 'Basic';
        accLic.Subscription__c = sub.Id;
    }
    public static Support_Office__c createAndInsertSupportOffice(Id businessHours) {
        
        Id pId = [Select Id From Profile Where Name Like 'System Administrator' Limit 1].Id;
        User johnBrown = [select Id from User where profileId=:pId and isActive=true limit 1];
                
        Support_Office__c cancunSupportOffice = 
            new Support_Office__c(Name = 'Cancun', Regional_Support_Manager__c = johnBrown.Id,
                                    Business_Hours__c = businessHours);
        insert cancunSupportOffice; return cancunSupportOffice;
    }
    
}