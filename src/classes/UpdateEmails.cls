/*******************************************************************
* Name: UpdateEmails
* Description: this batch job can be run as an alternative way to mask email addresses on Contact
* and Lead objects after refreshing full copy or partial sandboxes
*
* Log History:
*  12.12.2019  ext_bjd	ITRM-383 Initial Development
********************************************************************/

global class UpdateEmails implements Database.Batchable<SObject>, Database.Stateful {

    public static final String LEAD_UPDATE = 'leadUpdate';
    public static final String CONTRACT_UPDATE = 'contactUpdate';

    private String typeUpdate = LEAD_UPDATE;
    private Map<Id, String> errorMap = new Map<Id, String>();
    private Map<Id, SObject> IdToSObjectMap = new Map<Id, SObject>();

    public UpdateEmails(String typeUpdate) {
        this.typeUpdate = typeUpdate;
    }

    public static Boolean isSandbox = PostRefreshHandler.isSandboxFinder();

    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (typeUpdate == CONTRACT_UPDATE) {
            return Database.getQueryLocator([
                    SELECT Id, Name, Email, FirstName, LastName
                    FROM Contact
                    WHERE Email != NULL
                    ORDER BY Id
            ]);
        } else if (typeUpdate == LEAD_UPDATE) {
            return Database.getQueryLocator([
                    SELECT Id, Name, Email, FirstName, LastName
                    FROM Lead
                    WHERE Email != NULL
                    AND IsConverted = FALSE
                    ORDER BY Id
            ]);
        } else {
            return null;
        }
    }

    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        Boolean isFirstRecord = true;
        if (isSandbox || (!isSandbox && Test.isRunningTest())) {
            List<SObject> modifiedSObjects = new List<SObject>();
            for (SObject sobj : scope) {
                if (String.isNotBlank((String) sobj.get('Email'))) {
                    String invalidEmail = sobj.get('Email') + '.invalid';
                    if (String.isBlank((String) sobj.get('FirstName'))) {
                        sobj.put('FirstName', sobj.get('LastName'));
                    }
                    if (String.isBlank((String) sobj.get('LastName')) && String.isNotBlank((String) sobj.get('FirstName'))) {
                        sobj.put('LastName', sobj.get('FirstName'));
                    }
                    Integer emailLength = sobj.getSObjectType() == Contact.getSObjectType() ?
                            Contact.Email.getDescribe().getLength() : Lead.Email.getDescribe().getLength();
                    if (invalidEmail.length() > emailLength) {
                        sobj.put('Email', ((String) sobj.get('Email')).substringBefore('@'));
                        sobj.put('Email', sobj.get('Email') + '@test.invalid');
                        if (((String) sobj.get('Email')).length() > emailLength) {
                            sobj.put('Email', 'invalid@test.invalid');
                        }
                        modifiedSObjects.add(sobj);
                    } else {
                        sobj.put('Email', invalidEmail);
                        modifiedSObjects.add(sobj);
                    }
                    if (isFirstRecord && Test.isRunningTest()) {
                        isFirstRecord = false;
                        sobj.put('Email', '1234567890qwertyuiopasdfghjklzxcvbnmASDFGHJKLZXC@qwertyuiopasdfghjklzxcvbnqwertyuiop.com');
                    }
                }
            }
            if (modifiedSObjects.size() > 0) {
                Database.SaveResult[] srList = Database.update(modifiedSObjects, false);
                Integer index = 0;
                for (Database.SaveResult dsr : srList) {
                    if (!dsr.isSuccess()) {
                        String errMsg = dsr.getErrors()[0].getMessage();
                        errorMap.put(modifiedSObjects[index].Id, errMsg);
                        IdToSObjectMap.put(modifiedSObjects[index].Id, modifiedSObjects[index]);
                    }
                    index++;
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        if (!errorMap.isEmpty()) {
            AsyncApexJob a = [
                    SELECT Id, ApexClassId,
                           JobItemsProcessed, TotalJobItems,
                           NumberOfErrors, CreatedBy.Email
                    FROM AsyncApexJob
                    WHERE Id = :bc.getJobId()
            ];
            String body = 'Your batch job '
                    + 'UpdateEmails '
                    + 'has finished. \n'
                    + 'There were '
                    + errorMap.size()
                    + ' errors. Please find the error list attached to the Case.';

            // Creating the CSV file
            String finalStr = 'Id, Name, Error \n';
            String subject = 'Apex Batch Error List';
            String attName = 'Errors.csv';
            for (Id id : errorMap.keySet()) {
                String err = errorMap.get(id);
                SObject sobj = IdToSObjectMap.get(id);
                String recordString = '"' + id + '","' + (String)sobj.get('Name') + '","' + err + '"\n';
                finalStr = finalStr + recordString;
            }

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

            // Create the email attachment
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalStr));

            // Sets the paramaters of the email
            email.setSubject( subject );
            email.setToAddresses( new String[] {a.CreatedBy.Email} );
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
    }
}