/**
* Change log:
*
* 2018-10-08	ext_vos	CHG0033956: initial development. testSettings().
*/
@isTest
public class ForecastSnapshotsIframeTest {

	static testmethod void testSettings() {
        ForecastSnapshotsIframeController contr = new ForecastSnapshotsIframeController();
        System.assertEquals(null, contr.historyUrl);

        QTCustomSettings__c settings = new QTCustomSettings__c(Name = 'Default');
        settings.ForecastHistoryURL__c = 'testURL';
        insert settings;

        contr = new ForecastSnapshotsIframeController();
        System.assertEquals('testURL', contr.historyUrl);
	}
}