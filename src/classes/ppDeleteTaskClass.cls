/*************************************************************************************************************
 Name:	ppDeleteTaskClass
 Author: Santoshi Mishra
 Purpose: This classwill redirect the portal users to new Home page after task deletion on the partner portal and 
		all other users to the salesorce main home page.
 Created Date : March 2012
*************************************************************************************************************/
public class ppDeleteTaskClass {
	 Task tskObj;
	 public ppDeleteTaskClass(ApexPages.StandardController controller) 
	 {
	 	 this.tskObj=(Task) controller.getRecord();// The id is passed in URL for all incomplet tasks.This works fine everywhere except on deleting completed tasks.
	 	 if(System.currentPageReference().getParameters().get(';delID')!=null)// The id is not passed for completed task so this code pulls the completed task record.
	 	 {
	 	 	this.tskObj=[select id from task where id =:System.currentPageReference().getParameters().get(';delID')];
	 	 }
	 	 
	 }  
     public PageReference redirectTask()// This method redirects to portal homepage for portal users and standard home page for all other users 
     { 
        PageReference newPage=null;
	    delete(this.tskObj);
       	if (UserInfo.getUserType().equals('PowerPartner'))// Checks if the logged in user is Partner Portal User and redirects to new Partner Portal Home Page.
		 {
		 		newPage = Page.PPHome;
		 }
       	else
       	{
       		newPage = new Pagereference('/home/home.jsp');// Checks if the logged in user is non-partner user and redirects to standard home page.
       	}
       return newPage;       
    }  

}