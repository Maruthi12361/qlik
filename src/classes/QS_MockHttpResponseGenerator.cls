/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@IsTest

global class QS_MockHttpResponseGenerator implements HttpCalloutMock {

	global HTTPResponse respond(HTTPRequest req) {
        
              
        StaticResource sr = [select Body from StaticResource where Name = 'QS_JiveResults'];
        System.debug('xxbody' + sr.Body.toString());
         
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(sr.Body.toString());
        res.setStatusCode(200);
        return res;
        
    }
	
}