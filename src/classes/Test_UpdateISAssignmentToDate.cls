/*
    Name:  Test_UpdateISAssignmentToDate.cls


    Purpose:
    -------
    Test Classes for UpdateISAssignmentToDate.trigger
    
    History
    ------- 
    VERSION AUTHOR      DATE        DETAIL
    1.0     CCE         2012-11-29  Initial development.
    
*/
@isTest
private class Test_UpdateISAssignmentToDate {

    @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
       }

    static testMethod void TestAssignedToCalculation() {
        
        //Create an IS Resource request
        IS_ResourceRequest__c ISResReq = new IS_ResourceRequest__c(	Name = 'UpAssTest1',
        															Total_Requested_Days__c = 7.0
        															);
        insert(ISResReq);
        ISResReq = [select Id, Name from IS_ResourceRequest__c where Name = 'UpAssTest1'];
        
        //Create an IS Assignment. We will start from a known date for Assigned From then when we add the 7 days
        // we will know the expected Assigned To date. We are also allowing only Monday to Thursday to be valid
        // to exercise more of the code.
        Date myDate = date.newinstance(2012, 11, 22);
        IS_Assignment__c ISA = new IS_Assignment__c(
                                    IS_Resource_request__c = ISResReq.Id,
                                    Assigned_From__c = myDate,
                                    Assigned_Days__c = 7.0,
                                    Monday__c = True, 
                                    Tuesday__c = True,
                                    Wednesday__c = True, 
                                    Thursday__c = True, 
                                    Friday__c = False,
                                    Saturday__c = False,
                                    Sunday__c = False                                  
                                    );
		insert(ISA);
		ISA = [select Id, Assigned_To__c from IS_Assignment__c where Id = :ISA.Id];
		
		Date expectedDate = date.newinstance(2012, 12, 04);
        System.assertEquals(expectedDate, ISA.Assigned_To__c);              
    }
    
    //If no days are selected we should default to the weekdays
    static testMethod void TestAssignedToCalculationWithNoDays() {
        
        //Create an IS Resource request
        IS_ResourceRequest__c ISResReq = new IS_ResourceRequest__c(	Name = 'UpAssTest1',
        															Total_Requested_Days__c = 7.0
        															);
        insert(ISResReq);
        ISResReq = [select Id, Name from IS_ResourceRequest__c where Name = 'UpAssTest1'];
        
        //Create an IS Assignment. We will start from a known date for Assigned From then when we add the 7 days
        // we will know the expected Assigned To date. If no days are selected we default to using the weekdays.
        Date myDate = date.newinstance(2012, 11, 22);
        IS_Assignment__c ISA = new IS_Assignment__c(
                                    IS_Resource_request__c = ISResReq.Id,
                                    Assigned_From__c = myDate,
                                    Assigned_Days__c = 7.0,
                                    Monday__c = False, 
                                    Tuesday__c = False,
                                    Wednesday__c = False, 
                                    Thursday__c = False, 
                                    Friday__c = False,
                                    Saturday__c = False,
                                    Sunday__c = False                                  
                                    );
		insert(ISA);
		ISA = [select Id, Assigned_To__c from IS_Assignment__c where Id = :ISA.Id];
		
		Date expectedDate = date.newinstance(2012, 11, 30);
        System.assertEquals(expectedDate, ISA.Assigned_To__c);              
    }
}