public with sharing class TaskControllerExtension { 
 // 2015-05-05 TJG Created for CR 20082 https://eu1.salesforce.com/a0CD000000nqsw3 

    //private final sObject mysObject; 

 public TaskControllerExtension(ApexPages.StandardController stdController) { 
 //this.mysObject = (sObject)stdController.getRecord(); 
 } 

 // retrieve the base url from call center adapterUrl, for example 
 // if adapterUrl is https://cloud11.contact-world.net/CallCentre/ServiceCloud/AgentInterface 
 // we return https://cloud11.contact-world.net 
 public String getCaUrlBase() { 

 User me = [select CallCenterId from user where Id =:UserInfo.getUserId()]; 

 if (me.CallCenterId == null) 
 { 
 return null; 
 } 

 CallCenter cCenter = [select AdapterUrl from CallCenter where Id = :me.CallCenterId limit 1]; 

 if (cCenter.AdapterUrl == null) 
 { 
 return null; 
 } 

 String baseUrl = cCenter.AdapterUrl; 
 integer endIndex = baseUrl.IndexOf('/', 8); 
 if (endIndex < 0) 
 { 
 return baseUrl; 
 } 

 return baseUrl.Substring(0, endIndex); 
 } 
}