/*********************************************
Change Log:

2018-07-09 ext_bad CHG0034165: create to populate Qlikview_Support_Escalation__c fields.
********************************************/
public with sharing class CaseEscalationHandler {

    public static void populateFields(List<Qlikview_Support_Escalation__c> escalations) {
        Set<String> caseIds = new Set<String>();
        for (Qlikview_Support_Escalation__c esc : escalations) {
            caseIds.add(esc.Case__c);
        }

        Map<Id, Case> casesMap = new Map<Id, Case>([
                SELECT Id, Account_Name__c, Account_Origin__c, OwnerId
                FROM Case
                WHERE Id IN :caseIds
        ]);


        for (Qlikview_Support_Escalation__c esc : escalations) {
            esc.Status__c = 'New';
            Case cs = casesMap.get(esc.Case__c);
            if (cs != null) {
                if (esc.Account_Origin_Lookup__c == null) {
                    esc.Account_Origin_Lookup__c = cs.Account_Origin__c;
                }
                String sobjectType = cs.OwnerId.getSObjectType().getDescribe().getName();

                if (sobjectType == 'User') {
                    esc.CaseOwner__c = cs.OwnerId;
                }
            }
        }

    }

    public static void populateContactRole(List<Qlikview_Support_Escalation__c> escalations,
            Map<Id, Qlikview_Support_Escalation__c> oldMap) {
        Set<String> contactIds = new Set<String>();
        for (Qlikview_Support_Escalation__c esc : escalations) {
            contactIds.add(esc.ContactName__c);
        }

        Map<Id, Contact> contactsMap = new Map<Id, Contact>([
                SELECT Id, Job_Title__c
                FROM Contact
                WHERE Id IN :contactIds
        ]);

        for (Qlikview_Support_Escalation__c esc : escalations) {
            Contact cont = contactsMap.get(esc.ContactName__c);
            Qlikview_Support_Escalation__c oldesc = oldMap.get(esc.Id);
            if (cont != null && (esc.Contact_Role__c == null || (oldesc != null && esc.ContactName__c != oldesc.ContactName__c))) {
                esc.Contact_Role__c = cont.Job_Title__c;
            }
        }

    }

    public static void sendEmails(List<Qlikview_Support_Escalation__c> escalations) {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Group grp = [SELECT Id FROM Group WHERE DeveloperName = 'Escalation_Management' LIMIT 1];
        if (grp != null) {
            List<GroupMember> members = [
                    SELECT UserOrGroupId
                    FROM GroupMember
                    WHERE GroupId = :grp.Id
                    AND UserOrGroupId IN (SELECT Id FROM User WHERE IsActive = TRUE)
            ];

            List<Id> groupUserIds = new List<Id>();
            for (GroupMember gm : members) {
                groupUserIds.add(gm.UserOrGroupId);
            }
            List<User> groupUsers = [SELECT Email FROM User WHERE Id IN :groupUserIds];

            List<String> emails = new List<String>();
            for (User usr : groupUsers) {
                emails.add(usr.Email);
            }

            Set<String> caseIds = new Set<String>();
            Set<String> bugIds = new Set<String>();
            Set<String> approverIds = new Set<String>();
            Set<String> accountIds = new Set<String>();
            for (Qlikview_Support_Escalation__c esc : escalations) {
                caseIds.add(esc.Case__c);
                if (String.isNotBlank(esc.Bug_ID__c)) {
                    bugIds.add(esc.Bug_ID__c);
                }
                approverIds.add(esc.Qlik_Management_Approver__c);
                accountIds.add(esc.Account_Origin_Lookup__c);
            }

            Map<Id, User> approversMap = new Map<Id, User>([
                    SELECT Id, Name
                    FROM User
                    WHERE Id IN :approverIds
            ]);

            Map<Id, Account> accountsMap = new Map<Id, Account>([
                    SELECT Id, Name
                    FROM Account
                    WHERE Id IN :accountIds
            ]);

            Map<Id, Case> casesMap = new Map<Id, Case>([
                    SELECT Id, Subject, CaseNumber, Severity__c
                    FROM Case
                    WHERE Id IN :caseIds
            ]);

            List<EmailTemplate> etlist = [
                    SELECT id, Body, Subject, HtmlValue
                    FROM EmailTemplate
                    WHERE DeveloperName = :'New_Case_Escalation'
            ];
            if (etlist.size() > 0) {
                EmailTemplate et = etlist.get(0);
                for (Qlikview_Support_Escalation__c esc : escalations) {
                    Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
                    Case cs = casesMap.get(esc.Case__c);
                    String subject = et.Subject.replace('{!Case.CaseNumber}', cs != null && cs.CaseNumber != null ? cs.CaseNumber : '');

                    String htmlText = et.HtmlValue.replace('{!Case.CaseNumber}', cs != null && cs.CaseNumber != null ? cs.CaseNumber : '');

                    htmlText = htmlText.replaceAll('CASE_URL',
                            cs != null ? URL.getSalesforceBaseUrl().toExternalForm() + '/' + cs.Id : '');
                    htmlText = htmlText.replaceAll('ESCALATION_URL',
                            URL.getSalesforceBaseUrl().toExternalForm() + '/' + esc.Id);

                    htmlText = htmlText.replace('{!Case.Severity__c}', cs != null && cs.Severity__c != null ? cs.Severity__c : '');

                    htmlText = htmlText.replace('{!Qlikview_Support_Escalation__c.Bug_ID__c}', esc.Bug_ID__c != null && esc.Bug_ID__c != null ? esc.Bug_ID__c : '');

                    htmlText = htmlText.replace('{!Qlikview_Support_Escalation__c.Escalation_Subject__c}',
                            esc.Escalation_Subject__c != null ? esc.Escalation_Subject__c : '');
                    htmlText = htmlText.replace('{!Qlikview_Support_Escalation__c.Name}',
                            esc.Name != null ? esc.Name : '');
                    htmlText = htmlText.replace('{!Qlikview_Support_Escalation__c.Business_Case__c}',
                            esc.Business_Case__c != null ? esc.Business_Case__c : '');

                    Account acc = accountsMap.get(esc.Account_Origin_Lookup__c);
                    htmlText = htmlText.replace('{!Qlikview_Support_Escalation__c.Account_Origin_Lookup__c}',
                            (acc != null && acc.Name != null) ? acc.Name : '');
                    htmlText = htmlText.replace('END_USER_URL',
                            acc != null ? URL.getSalesforceBaseUrl().toExternalForm() + '/' + acc.Id : '');

                    htmlText = htmlText.replace('{!Qlikview_Support_Escalation__c.Escalation_Approved_by__c}',
                            (approversMap.get(esc.Qlik_Management_Approver__c) != null && approversMap.get(esc.Qlik_Management_Approver__c).Name != null)
                                    ? approversMap.get(esc.Qlik_Management_Approver__c).Name : '');
                    htmlText = htmlText.replace('{!User.Name}', UserInfo.getName());

                    emailMessage.setToAddresses(emails);
                    emailMessage.setHtmlBody(htmlText);
                    emailMessage.setSubject(subject);
                    mails.add(emailMessage);
                }

                if (!mails.isEmpty()) {
                    try {
                        List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(mails);
                        for (Messaging.SendEmailResult res : sendres) {
                            system.debug(LoggingLevel.DEBUG, '[CaseEscalationHandler] Result: ' + res);
                        }
                    } catch (System.EmailException ex) {
                        system.debug(LoggingLevel.DEBUG, '[CaseEscalationHandler] Error when sending mail: ' + ex.getMessage());
                    }
                }
            }
        }
    }
}