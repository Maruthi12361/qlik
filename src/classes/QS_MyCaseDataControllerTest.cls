/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_MyCaseDataControllerTest {
    @isTest static void test_method_one() {
        Category_Lookup__c catLook = new Category_Lookup__c();
        catLook.Name = 'I have a setup and configuration issue';
        catLook.Category_Level__c = 'I have an issue building a custom authentication solution';
        catLook.Severity__c = '2';
        catLook.Case_Owner__c = 'Team Technical';
        catLook.Service_Request_Type__c = 'Test';
        catLook.Question_Prompt_1__c = 'Line 1';
        catLook.Question_Prompt_2__c = 'Line 2';
        catLook.Question_Prompt_3__c = 'Line 3';
        catLook.Question_Prompt_4__c = 'Line 4';
        catLook.Question_Prompt_5__c = 'Line 5';
        catLook.Record_Type_Name__c = 'QlikTech Master Support Record Type';
        insert catLook;
        
        QS_MyCaseDataController controller = new QS_MyCaseDataController();
        controller.selectedFilter1 ='I have a setup and configuration issue';
        controller.filterCategoryLookupList();
        controller.processRequests();
        List<SelectOption> ENVOptionList = controller.ENVOptionList;
        string UserId = controller.UserId;

        Account Acc = controller.Acc;
        List<Case> resultsOpen=controller.resultsOpen;
        Date FromDate = controller.FromDate;
        Date ToDate = controller.ToDate;
        integer accountListSize = controller.accountListSize ;
        boolean DSEAccess = controller.DSEAccess;
        String DSEName = controller.DSEName;
    }
}