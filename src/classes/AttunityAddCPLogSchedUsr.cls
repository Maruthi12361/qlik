/**
* AttunityAddCPLogSchedUsr
* 
* Description:  Activates Attunity users that were disabled and adds the correct federationidentifier.
* Added:        2019-11-18 - Ain - IT-2287
*
* Change log:
* 09-11-2018 - Ain - IT-2287 - Initial Implementation
* 17-12-2019 - extcqb - IT-2321 - new profile
*
*/

global class AttunityAddCPLogSchedUsr implements Database.Batchable<SObject>, Schedulable{

    private String query = 'select id, contactid, federationidentifier, isactive from user where contactid != null and (profile.name = \'Customer Portal Case Logging Access - Attunity\' or profile.name = \'Customer Portal Case Viewing Access - Attunity\') and (FederationIdentifier = null or isActive = false)';
    public Id CPLogId;
    
    global AttunityAddCPLogSchedUsr(){
	
		System.debug('In AttunityAddCPLogSchedUsr.Constructor');
		
        List<ULC_Level__c> CPLogIds = [SELECT Id FROM ULC_Level__c WHERE Name = 'CPLOG' AND Status__c = 'Active'];
        if (CPLogIds.size() == 1) {
            CPLogId = CPLogIds[0].Id;
        } else {
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No CPLOG level found!');
            throw e;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('In AttunityAddCPLogSchedUsr.start');
        if(Test.isRunningTest()){
            query = query + ' and Alias = \'007\'';
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
	
		System.debug('In AttunityAddCPLogSchedUsr.execute: ' + scope.size());
		
        List<User> users = (List<User>)scope;
        Map<Id, String> UserToULC = new Map<Id, String>();
        Map<Id, Id> contactToUserId = new Map<Id, Id>();
        
        
        
        //Gather all contactIds and connect them to the Users
        for(User u : users) {
            contactToUserId.put(u.ContactId, u.Id);
        }
        
        //Add connection between User to ULC name
        List<ULC_Details__c> UlcDetails = [SELECT Id, ULCName__c, ContactId__c FROM ULC_Details__c WHERE ContactId__c IN :contactToUserId.keySet() AND ULCStatus__c = 'Active'];
        for (ULC_Details__c ulcDetail : UlcDetails){
            if (ulcDetail.ULCName__c != null && ulcDetail.ULCName__c != '') {
                UserToULC.put(contactToUserId.get(ulcDetail.ContactId__c), ulcDetail.ULCName__c);
            }
        }
        
        List<User> usersToUpdate = new List<User>();
        
        for(User u : users) {
            Boolean updateUser = false;
            if((u.FederationIdentifier == null || u.FederationIdentifier == '') && UserToULC.containsKey(u.Id) ) {
                u.FederationIdentifier = UserToULC.get(u.Id);
                updateUser = true;
            }
            if(u.IsActive == false){
                u.IsActive = true;
                updateUser = true;
            }
            if(updateUser){
                usersToUpdate.add(u);
            }
        }
                       
		update usersToUpdate;
    }
    
    global void execute(SchedulableContext SC){
        AttunityAddCPLogSchedUsr batchJob = new AttunityAddCPLogSchedUsr();
        Id batchprocessid = Database.executeBatch(batchJob);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}