/********************************************************
ChangeLog:
2012-02-27	RDZ	Add Qliktech company as its required to create account
18.09.2012 	Fluido: If Record Type is "Academic Program Account", QlikTech Company should be "QlikTech International AB" -> prefill the field
					when creating new account.

24.05.2013	YKA:	CR# 8169 Modify Account Search Criteria and Matching Results on Account record. 
						Removing Account Site and adding Billing City, Billing State/ Province & Named Account to the search page and criteria.
						Re-ordering the results as per the requirements
						Passing on the additional values into the url for new page 
					
17.06.2013	YKA:	CR# 8375 #Quick - Modify Matching Results view on account search
						Replacing the Segment__c field with Segment_New__c in this class and in AccountSearch Page (Line 109).
						
25.06.2013	YKA:	CR# 8355 Modify End User Account creation process
					Adding Pending Validation (Pending_Validation__c) to the search page / criteria and results		
06.02.2017  RVA :   changing CreateAcounts methods	
						 	pendingValidationCheck
11.10.2017   ext_bad CHG0031114 - Academic Program accounts no longer should be using Qliktech Company "QlikTech International AB"
***********************************************************/

public with sharing class AccountSearchControllerExtension

{
	public static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    private final Account acnt; 
    
    public AccountSearchControllerExtension(ApexPages.StandardController stdController)
    {
        this.acnt = (Account)stdController.getRecord();
    }
    //Properties
	public Account[] results
	{
		get
		{
			if(results == null) results = new Account[0];
			return results;
		}
		set {results = value;}
	}
    
	public Boolean hasSearched
	{
		get
		{
			if(hasSearched == null) hasSearched = false;
			return hasSearched;
		}
		set {hasSearched = value;}
	}
    
	public Boolean resultsFound
	{
		get
		{
			if(results.size() == 0)
				return false;
			else
				return true;
		}
	}
    
	public Boolean accountNameStartsWith
	{
		get
		{
			if(accountNameStartsWith == null) accountNameStartsWith = true;
			return accountNameStartsWith;
		}
		set {accountNameStartsWith = value;}
	}
  
    // YKA: Adding for BillingCity
    public Boolean BillingCityStartsWith
	{
		get
		{
			if(BillingCityStartsWith == null) BillingCityStartsWith = true;
			return BillingCityStartsWith;
		}
		set {BillingCityStartsWith = value;}
	}
    
    // YKA: Adding for BillingState
    public Boolean BillingStateStartsWith
	{
		get
		{
			if(BillingStateStartsWith == null) BillingStateStartsWith = true;
			return BillingStateStartsWith;
		}
		set {BillingStateStartsWith = value;}
	}
    
    // YKA: Adding for Named Account
    public Boolean namedAccountCheck
    {
    	get
    	{
    		if(namedAccountCheck == null) namedAccountCheck = false;
    		return namedAccountCheck;
    	}
    	set
    	{
    		namedAccountCheck = value;
    	}
    }
    
    // YKA: Adding for Pending Validation
    public Boolean pendingValidationCheck
    {
    	get
    	{
    		if(pendingValidationCheck == null) pendingValidationCheck = false;
    		return pendingValidationCheck;
    	}
    	set
    	{
    		pendingValidationCheck = value;
    	}
    }
    
    
    
	public Boolean postalCodeStartsWith
	{
		get
		{
			if(postalCodeStartsWith == null) postalCodeStartsWith = true;
			return postalCodeStartsWith;
		}
		set {postalCodeStartsWith = value;}
	}
    
	public Boolean countryStartsWith
	{
		get
		{
			if(countryStartsWith == null) countryStartsWith = true;
			return countryStartsWith;
		}
		set {countryStartsWith = value;}
	}
	//Actions
    public PageReference searchAccounts()
    {
    	results.clear();
    	hasSearched = true;
    	
    	// YKA
    	
    	//Validation rules
    	if(acnt.Name == null)
        {
            acnt.Name.addError('Please enter Account Name value then click Search again.');
            hasSearched = false;
            return null;
        }
    	        
        if(acnt.Name != null && acnt.Name.length() == 1)
        {
            acnt.Name.addError('Please enter a minimum of 2 characters then click Search again.');
            hasSearched = false;
            return null;
        }
        //Validation rules passsed, continue
        //Prepare SOQL Query
        // YKA: Updating query to include more fields
        // YKA: CR# 8375 Segment__c to Segment_New__c
        String soqlQuery = 'select Id, Name, Site, BillingCity, BillingState, BillingPostalCode, BillingCountry, Billing_Country_Code__c,  Named_Account__c, Pending_Validation__c, RecordType.Name, Segment_New__c, Navision_Status__c, Status__c, Type, OwnerId, Owner.Name, QlikTech_Company__c from Account where Id != null ';
        if(acnt.Name != null)
        {
	        if(accountNameStartsWith)
	        	soqlQuery += 'and Name like \'' + acnt.Name.replace('\'', '\\\'') + '%\' ';
	        else
	        	soqlQuery += 'and Name like \'%' + acnt.Name.replace('\'', '\\\'') + '%\' ';
        }
                
        // YKA: Adding Billing City, Billing State to the query criteria        
        if(acnt.BillingCity != null)
        {
	        if(BillingCityStartsWith)
	        	soqlQuery += 'and BillingCity like \'' + acnt.BillingCity.replace('\'', '\\\'') + '%\' ';
	        else
	        	soqlQuery += 'and BillingCity like \'%' + acnt.BillingCity.replace('\'', '\\\'') + '%\' ';
        }
        
        // BillingStateStartsWith
        if(acnt.BillingState != null)
        {
	        if(BillingStateStartsWith)
	        	soqlQuery += 'and BillingState like \'' + acnt.BillingState.replace('\'', '\\\'') + '%\' ';
	        else
	        	soqlQuery += 'and BillingState like \'%' + acnt.BillingState.replace('\'', '\\\'') + '%\' ';
        }
        
        
        
        if(acnt.BillingPostalCode != null)
        {
	        if(postalCodeStartsWith)
	        	soqlQuery += 'and BillingPostalCode like \'' + acnt.BillingPostalCode.replace('\'', '\\\'') + '%\' ';
	        else
	        	soqlQuery += 'and BillingPostalCode like \'%' + acnt.BillingPostalCode.replace('\'', '\\\'') + '%\' ';
        }
        if(acnt.BillingCountry != null)
        {
	        if(countryStartsWith)
	        	soqlQuery += 'and BillingCountry like \'' + acnt.BillingCountry.replace('\'', '\\\'') + '%\' ';
	        else
	        	soqlQuery += 'and BillingCountry like \'%' + acnt.BillingCountry.replace('\'', '\\\'') + '%\' ';
        }
        
        
        // YKA: Adding Named Account to the query criteria
             
        if(acnt.Named_Account__c != null)
        {
	        if(namedAccountCheck)
	        	soqlQuery += 'and Named_Account__c = ' + acnt.Named_Account__c;	        
        }
        
        // YKA: Adding Pending Validation to the query criteria
             
        if(acnt.Pending_Validation__c != null)
        {
	        if(pendingValidationCheck)
	        	soqlQuery += 'and Pending_Validation__c = ' + acnt.Pending_Validation__c;	        
        }
        
        
        soqlQuery += ' order by Name limit 1000';
        System.debug('soqlQuery' + soqlQuery);
        
        // YKA Adding into try catch
        try
        {
        	results = Database.query(soqlQuery);
        }
        catch(System.Exception ex)
        {
        	System.debug('Account Search Error' + ex.getMessage());
        	acnt.addError('Account Search Error ');
        }
        System.debug('results' + results);
        

        if (results.size() == 0)
            acnt.Name.addError('No Accounts found. Please modify the search criteria and click Search again or click Create New.');

        return null;
    }
    
    public PageReference newAccount()
    {
    	String recordType = ApexPages.currentPage().getParameters().get('RecordType');
    	String retURL = ApexPages.currentPage().getParameters().get('retURL');
    	
    	//ID ProfileId = userInfo.getProfileId();    	
    	//String ProfileName = [Select Name From Profile where id = :ProfileId].Name;
    	
    	String url = '/001/e?nooverride=1';
    	url += recordType == null? '' : '&RecordType=' + EncodingUtil.urlEncode(recordType, 'UTF-8');
    	url += retURL == null? '' : '&retURL=' + EncodingUtil.urlEncode(retURL, 'UTF-8');
    	url += acnt.Name == null ? '' : '&acc2=' + EncodingUtil.urlEncode(acnt.Name, 'UTF-8');
    	
    	url += acnt.BillingCity == null ? '' : '&acc17city=' + EncodingUtil.urlEncode(acnt.BillingCity, 'UTF-8');
    	url += acnt.BillingState == null ? '' : '&acc17state=' + EncodingUtil.urlEncode(acnt.BillingState, 'UTF-8');
    	url += acnt.BillingPostalCode == null ? '' : '&acc17zip=' + EncodingUtil.urlEncode(acnt.BillingPostalCode, 'UTF-8');
    	url += acnt.BillingCountry == null ? '' : '&acc17country=' + EncodingUtil.urlEncode(acnt.BillingCountry, 'UTF-8');
    	
    	//  To Do Remove YKA: Setting the Pending Validation checkbox to True for Internal Users
    	//if(!(ProfileName.contains('PRM')))
    	//{
    	//	url += '&00NL0000002hZDn=' + EncodingUtil.urlEncode('1', 'UTF-8');
    		// on QTTest 00NL0000002hZDn
    		// on SysQTDev 00NM0000000osT2
    		
    	//}
    	
    	    	
    	// EDIT: 18.09.2012. If Record Type is "Academic Program Account", QlikTech Company should be "QlikTech International AB" -> prefill the field
    	// EDIT: 11.10.2017  
        //if(recordType != null){
    	//	// get information from custom settings
    	//	Academic_Program_Settings__c settings = Academic_Program_Settings__c.getInstance('AcademicProgramSetting');
    	//	if(settings != null){
    	//		if(recordType == settings.AccountRecordTypeId__c) url += '&'+settings.QlikTech_Company_field_Id__c+'=' + EncodingUtil.urlEncode(settings.QlikTech_Company__c, 'UTF-8');
    	//	}
    	//} 
		
        PageReference newAccountPage = new PageReference(url);
        newAccountPage.setRedirect(true);
        return newAccountPage;
    }
    
    
	//Test method
	public static testmethod void testAccountSearchControllerExtension()
	{
		Test.setCurrentPageReference(Page.AccountSearch);
		Account testAccount = new Account();
		ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);

		Test.startTest();

		AccountSearchControllerExtension asce = new AccountSearchControllerExtension(sc);
		
		//Test properties
		asce.hasSearched = null;
		Boolean hasSearched = asce.hasSearched;
		System.assert(!hasSearched);
		asce.hasSearched = true;
		hasSearched = asce.hasSearched;
		System.assert(hasSearched);

		Account[] accountArray = new Account[0];
		accountArray.add(new Account());
		accountArray.add(new Account());
		asce.results = accountArray;
		System.assertEquals(accountArray, asce.results);
		System.assertEquals(2, asce.results.size());
		asce.results = null;
		System.assertNotEquals(null, asce.results);

		asce.accountNameStartsWith = null;
		Boolean accountNameStartsWith = asce.accountNameStartsWith;
		System.assert(accountNameStartsWith);
		asce.accountNameStartsWith = false;
		accountNameStartsWith = asce.accountNameStartsWith;
		System.assert(!accountNameStartsWith);


		asce.BillingCityStartsWith = null;
		Boolean BillingCityStartsWith = asce.BillingCityStartsWith;
		System.assert(BillingCityStartsWith);
		asce.BillingCityStartsWith = false;
		BillingCityStartsWith = asce.BillingCityStartsWith;
		System.assert(!BillingCityStartsWith);


		asce.BillingStateStartsWith = null;
		Boolean BillingStateStartsWith = asce.BillingStateStartsWith;
		System.assert(BillingStateStartsWith);
		asce.BillingStateStartsWith = false;
		BillingStateStartsWith = asce.BillingStateStartsWith;
		System.assert(!BillingStateStartsWith);
		


		asce.postalCodeStartsWith = null;
		Boolean postalCodeStartsWith = asce.postalCodeStartsWith;
		System.assert(postalCodeStartsWith);
		asce.postalCodeStartsWith = false;
		postalCodeStartsWith = asce.postalCodeStartsWith;
		System.assert(!postalCodeStartsWith);

		asce.countryStartsWith = null;
		Boolean countryStartsWith = asce.countryStartsWith;
		System.assert(countryStartsWith);
		asce.countryStartsWith = false;
		countryStartsWith = asce.countryStartsWith;
		System.assert(!countryStartsWith);
		
		
		Boolean namedAccountCheck;
		
		asce.namedAccountCheck = false;
		namedAccountCheck = asce.namedAccountCheck;
		System.assert(!namedAccountCheck);

		//Test validation rules
		asce.acnt.Name = '1';
		PageReference searchPage = asce.searchAccounts();
		System.assertEquals(null, searchPage);
		
		//Test search with all search criteria blank
		asce.acnt.Name = '';
		
		asce.acnt.BillingCity = '';
		asce.acnt.BillingState = '';
		asce.acnt.BillingPostalCode = '';
		asce.acnt.BillingCountry = '';		
		
		searchPage = asce.searchAccounts();
		System.assertEquals(null, searchPage);
		
		//Test search with all set to starts with search
		asce.acnt.BillingCity = 'test';
		asce.acnt.BillingState = 'test';
			
		asce.acnt.BillingPostalCode = 'test';
				
		asce.acnt.Named_Account__c = true;
		asce.accountNameStartsWith = true;

		asce.BillingCityStartsWith = true;
		asce.BillingStateStartsWith = true;
		asce.namedAccountCheck = true;
		
		asce.postalCodeStartsWith = true;
		asce.countryStartsWith = true;
		searchPage = asce.searchAccounts();
		System.assertEquals(null, searchPage);
		
		//Test search with all set to contains search
		asce.accountNameStartsWith = false;
		asce.BillingCityStartsWith = false;
		asce.BillingStateStartsWith = false;
		asce.namedAccountCheck = false;

		asce.postalCodeStartsWith = false;
		asce.countryStartsWith = false;
		searchPage = asce.searchAccounts();
		System.assertEquals(null, searchPage);
		
		//Test search with zero results
		asce.acnt.Name = 'aasdfadssdfsfasdf';
		asce.acnt.BillingCountry = 'testabc';
		searchPage = asce.searchAccounts();
		System.assertEquals(null, searchPage);
		System.assert(!asce.resultsFound);
		
		//Test search with one result
		/*
		Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c QTComp = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

		RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
		*/
		 User testUser = [Select id From User where id =: UserInfo.getUserId()];
		testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAccount;
       // testAccount = QuoteTestHelper.createAccount(QTComp, rTypeAcc, 'EndUser');
         //   insert  testAccount;
		Account tstAccount = [select Id, BillingCity, BillingState, BillingPostalCode, BillingCountry from Account where Id = :testAccount.Id limit 1];
		System.debug(tstAccount.Id + ',' + tstAccount.BillingCity + ',' + tstAccount.BillingState + ',' + tstAccount.BillingPostalCode + ',' + tstAccount.BillingCountry);

		asce.acnt.Name = 'testN';
		// To Do
		//asce.acnt.Site = 'test';
		asce.acnt.BillingCity = 'Radnor';
		asce.acnt.BillingState = 'PA';
		asce.acnt.BillingPostalCode = '19087';
		asce.acnt.BillingCountry = 'USA';
		searchPage = asce.searchAccounts();
		System.assertEquals(null, searchPage);
   			
		//Test newAccount no query params
		asce.acnt.Name = null;
		//asce.acnt.Site = null;
		asce.acnt.BillingCity = null;
		asce.acnt.BillingState = null;
		
		asce.acnt.BillingPostalCode = null;
		asce.acnt.BillingCountry = null;
		String newPageUrl = asce.newAccount().getUrl();
		System.assert(newPageUrl.contains('/001/e'));
		System.assert(newPageUrl.contains('nooverride=1'));
		System.assert(!newPageUrl.contains('RecordType=123'));
		System.assert(!newPageUrl.contains('retURL=123'));
		System.assert(!newPageUrl.contains('acc2'));
		
		//System.assert(!newPageUrl.contains('acc23'));
		System.assert(!newPageUrl.contains('acc17city'));
		System.assert(!newPageUrl.contains('acc17state'));
				
		System.assert(!newPageUrl.contains('acc17zip'));
		System.assert(!newPageUrl.contains('acc17country'));

		//Test newAccount with query params
		asce.acnt.Name = 'testN';
		asce.acnt.BillingCity = 'testN';
		asce.acnt.BillingState = 'testN';
		asce.acnt.BillingPostalCode = '1234';

		
		//asce.acnt.BillingPostalCode = 'test';
		asce.acnt.BillingCountry = 'testN';
		PageReference testPageReference = new PageReference(Page.AccountSearch.getUrl() + '?RecordType=123&retURL=123');
		Test.setCurrentPageReference(testPageReference);
		newPageUrl = asce.newAccount().getUrl();
		System.assert(newPageUrl.contains('/001/e'));
		System.assert(newPageUrl.contains('nooverride=1'));
		System.assert(newPageUrl.contains('RecordType=123'));
		System.assert(newPageUrl.contains('retURL=123'));
		System.assert(newPageUrl.contains('acc2=testN'));
		
		System.assert(newPageUrl.contains('acc17city=testN'));
		System.assert(newPageUrl.contains('acc17state=testN'));
		
		System.assert(newPageUrl.contains('acc17zip=1234'));
		System.assert(newPageUrl.contains('acc17country=testN'));


		Test.stopTest();
	}
    // EDIT: 11.10.2017
	/*public static testmethod void testAcademicProgram(){
		//Test newAccount if Record Type is "Academic Program Account", QlikTech Company should be "QlikTech International AB"
		List<RecordType> academicRecTypeFound = [select Id, DeveloperName from RecordType where SobjectType ='Account' and DeveloperName = 'Academic_Program_Account'];
    	RecordType oppRecordType = [select Id from RecordType where SobjectType ='Opportunity' LIMIT 1];
    	String recType = String.valueOf(academicRecTypeFound[0].Id).substring(0,15);
    	Academic_Program_Settings__c settingsFound = Academic_Program_Settings__c.getInstance('AcademicProgramSetting');
    	Boolean found = true;
    	String notFoundFieldId = 'TestingFieldNameId';
    	if(settingsFound == null){
    		found = false;
    		settingsFound = new Academic_Program_Settings__c(Name='AcademicProgramSetting',
    														 AccountRecordTypeId__c = recType, 
    														 OpportunityRecordTypeId__c = oppRecordType.Id,
    		 												 QlikTech_Company__c = 'QlikTech International AB',
    		 												 QlikTech_Company_field_Id__c = notFoundFieldId);
    		insert settingsFound;
    	}
    	Test.setCurrentPageReference(Page.AccountSearch);
		Account testAccount = new Account();
		ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);

		Test.startTest();

		AccountSearchControllerExtension asce = new AccountSearchControllerExtension(sc);
		
		asce.acnt.Name = 'testAcademicProgramAccount';
		asce.acnt.Site = 'test';
		asce.acnt.BillingPostalCode = 'test';
		asce.acnt.BillingCountry = 'test';
		PageReference testPageReference2 = new PageReference(Page.AccountSearch.getUrl() + '?RecordType='+settingsFound.AccountRecordTypeId__c+'&retURL=123');
		Test.setCurrentPageReference(testPageReference2);
		String newPageUrl = asce.newAccount().getUrl();
		System.assert(newPageUrl.contains('/001/e'));
		//00N20000001bqcS=QlikTech%2BInternational%2BAB
		String expected = settingsFound.QlikTech_Company_field_Id__c+'='+settingsFound.QlikTech_Company__c.replace(' ','%2B');
		System.assert(newPageUrl.contains(expected));
		test.stopTest();
	}*/
}