/*
@date:	2013-06-27	TJG CR 8221	https://eu1.salesforce.com/a0CD000000YITlq
		Modify the limit on QlikMarket Reponder Download list
@description: Controller for the QVM customers list for exporting to Excel
*/
public without sharing class QVM_CustomersConCsv {
	public QVM_CustomersConCsv(){
	}

	public List<QVM_Customer_Products__c> CusProducts {
		get {
			String SoqlQuery = System.currentPagereference().getParameters().get('s');
			System.debug('SoqlQuery=' + SoqlQuery);
			if (null == CusProducts && null <> SoqlQuery) {
				CusProducts = Database.query(EncodingUtil.urlDecode(SoqlQuery, 'UTF-8'));
			}
			return CusProducts; 
		}
		set;
	}
	//export to excel - returns a page reference to the AccountDataExcel page
	public PageReference exportCustomersExcel() {
	   return Page.QVM_CustomersCSV;
	}	
}