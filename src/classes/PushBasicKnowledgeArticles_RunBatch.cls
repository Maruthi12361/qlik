/**
* PushBasicKnowledgeArticles_RunBatch
*
* Description:  Scheduler to launch the job for pushing Diagnostic Knowledge Articles to the public knowledge base.
* Added:        20-07-2018 - ext_vos - CHG0034282
*
* Change log:
*
*/
global class PushBasicKnowledgeArticles_RunBatch implements Schedulable {
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new PushBasicKnowledgeArticlesToKPB(), 50);
    }

    // to run job daily
    global void execute() {
        // Tests for scheduled jobs can fail if the same job is already executing in Salesforce with the same name        
        String jobName = Test.isRunningTest() ? 'PushBasicKnowledgeArticlesToKPB_Test' : 'PushBasicKnowledgeArticlesToKPB';
        System.schedule(jobName, '0 0 2 * * ?', new PushBasicKnowledgeArticles_RunBatch());
    }
}