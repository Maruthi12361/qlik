/********************************************************
* ScheduleCreationBatch
* Description: This is the class use to create opp line item schedules for opportunities where the schedules ahve been deleted
* by user actions but never recreated.
* Also This jobs resets the flag on already processed opportunities.
*
* Change Log: 22-09-2017 v1 UIN Added Initial logic
* Change Log: 29-096-2017 v2 MTM Made batch schedulable instead of a separate class
* Apex to run once
* Cron job to schedule the class every 15 minutes
* 
ScheduleCreationBatch a = new ScheduleCreationBatch();
String sch = '0 5 * * * ?';
String jobID = system.schedule('ScheduleCreationBatch Job 1', sch, a);
sch = '0 20 * * * ?';
jobID = system.schedule('ScheduleCreationBatch Job 2', sch, a);
sch = '0 35 * * * ?';
jobID = system.schedule('ScheduleCreationBatch Job 3', sch, a);
sch = '0 50 * * * ?';
jobID = system.schedule('ScheduleCreationBatch Job 4', sch, a);
*********************************************************/
global class ScheduleCreationBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful{
	
	String query;
	global List<String> exception_List;

	global ScheduleCreationBatch() {
		if(exception_List == null)
            exception_List = new List<String>();
		query = 'select id, name, Test_Opp_Schedule_Deleted__c, product2.SBQQ__SubscriptionType__c from opportunitylineitem where Test_Opp_Schedule_Deleted__c = true and product2.SBQQ__SubscriptionType__c = \'Renewable\'';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<OpportunityLineItem> lstOppLineItems = new list<OpportunityLineItem>(); 
		List<Id> lOppLineIds = new list<Id>();
		//Map<Id, List<>> mLineItemIdOppLineCount = new Map<Id, Integer>();
		Set<Id> sLineItemIdsWithSched = new Set<Id>();
		Set<Id> sLineItemIdsWithoutSched = new Set<Id>();
		for(sObject s:scope)
			lOppLineIds.add(s.Id);
		for(OpportunityLineItemSchedule sched: [select id,opportunitylineitem.Test_Opp_Schedule_Deleted__c from OpportunityLineItemSchedule where OpportunityLineItemId in :lOppLineIds]){
			sLineItemIdsWithSched.add(sched.OpportunityLineItemId);
		}
		if(!sLineItemIdsWithSched.isEmpty()){
			for(Id lId: lOppLineIds){
				if(!sLineItemIdsWithSched.contains(lId))
					sLineItemIdsWithoutSched.add(lId);
			}
		}else
			sLineItemIdsWithoutSched.addAll(lOppLineIds);

		for(OpportunityLineItem lItem:[select id, Test_Opp_Schedule_Deleted__c from opportunitylineitem where Test_Opp_Schedule_Deleted__c = true and id in :sLineItemIdsWithSched]){
			lItem.Test_Opp_Schedule_Deleted__c = false;
			lstOppLineItems.add(lItem);
		}
		if(!lstOppLineItems.isEmpty()){
			//update lstOppLineItems;
			List<Database.SaveResult> sResults = Database.update(lstOppLineItems, false);
			for(integer i =0; i<sResults.size();i++){
            String msg='';
            If(!sResults[i].isSuccess()){
                msg += 'Update Failed for Opportunity Line Item with id '+ lstOppLineItems.get(i).id + ' due to the Error: "';        
                for(Database.Error err: sResults[i].getErrors()){  
                     msg += err.getmessage()+'<br/>';
                } 
            }
            if(msg!='' && msg.contains('Error:'))
                exception_List.add(msg);
            }
			lstOppLineItems.clear();
		}

		if(!sLineItemIdsWithoutSched.isEmpty()){
			lstOppLineItems = [select id,opportunityId,Test_Opp_Schedule_Deleted__c,Product2Id,Net_Price__c from opportunitylineitem where id in :sLineItemIdsWithoutSched];
			if(!lstOppLineItems.isEmpty()){
				Set<String> setOfOppsid = new Set<String>();
				for(OpportunityLineItem lItem:lstOppLineItems){
					if(String.isNotBlank(lItem.OpportunityId))
						setOfOppsid.add(lItem.OpportunityId);
				}
				Map<String, SBQQ__Quote__c> mapQuoteFromOpp = new Map<String, SBQQ__Quote__c>();
				mapQuoteFromOpp = OpportunityScheduleCreationHandler.queryQuoteFromOpp(setOfOppsid);
				//OpportunityScheduleCreationHandler oppschedhandler = new OpportunityScheduleCreationHandler();
				List<OpportunityLineItemSchedule> itemsToInsert = new List<OpportunityLineItemSchedule>();
				itemsToInsert = OpportunityScheduleCreationHandler.processSchdeuleSubsUnits(lstOppLineItems, mapQuoteFromOpp);
				System.debug(itemsToInsert);
				if(!itemsToInsert.isEmpty()){
					List<Database.SaveResult> sResults_act = Database.insert(itemsToInsert, false);
					for(integer i =0; i<sResults_act.size();i++){
	                String msg='';
	                If(!sResults_act[i].isSuccess()){
	                    msg += 'Schedule creation failed for Opportunity Line Item with id'+ itemsToInsert.get(i).Opportunitylineitemid + 'failed due to the Error: "';        
	                    for(Database.Error err: sResults_act[i].getErrors()){  
	                         msg += err.getmessage()+'<br/>';
	                    } 
	                }
		            if(msg!='' && msg.contains('Error:'))
		                exception_List.add(msg);
		            } 
		            if(System.Test.isRunningTest()){
						exception_List.add('efgh');
						sendErrorMails(exception_List);
					}
	        	}
			}
		}
		if(exception_List.size() > 0)
			sendErrorMails(exception_List);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

    global void execute(SchedulableContext sc)
	{ 
		ScheduleCreationBatch b = new ScheduleCreationBatch();
        ID batchId = database.executebatch(b,50); 
	}

	private void sendErrorMails(List<String> exceptions){
        String errMsg = 'The Schedule Creation batch Apex job errors for Records processed till now are' +'<br/>';
        String temp='';
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[] {((String)Label.QSD_Mailing_List).split(';')};
        if(System.Test.isRunningTest()){
        	String[] toAddresses = new String[] {'ullalramakrishna.kini@qlik.com'};
        	mail.setToAddresses(toAddresses);
        }
        else{
 			mail.setToAddresses(((String)Label.Opp_Sched_Batch_Mailing_List).split(';'));
        }
        mail.setSubject('Opportunity Product Schedule Creation Batch Status In-Progress');
        if(!exceptions.isEmpty()){
            for(String str:exceptions)
                temp = temp + str;
            mail.setHTMLBody(errMsg + temp);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            exception_List = new List<String>();
        }  
    }
	
}