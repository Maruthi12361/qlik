// CR# 10215 - Apex util class to clone child related lists in an Opportunity
// Change log:
// May 27, 2014 - Madhav Kakani - Fluido Oy - Initial Implementation
//
public with sharing class OpportunityCloneUtils {   
    public static void cloneChildren(Map<Id, Id> mapOppIds) {
        List<SObject> lstClone = new List<SObject>();
        for(Id oppId: mapOppIds.keySet()) {
            List<SObject> lstTmp1 = cloneChild('Sphere_of_Influence__c', 'Opportunity__c', mapOppIds.get(oppId), oppId);
            for(SObject obj : lstTmp1) lstClone.add(obj);

            List<SObject> lstTmp2 = cloneChild('OpportunityLineItem', 'OpportunityId', mapOppIds.get(oppId), oppId);
            for(SObject obj : lstTmp2) lstClone.add(obj);

            List<SObject> lstTmp3 = cloneChild('OpportunityTeamMember', 'OpportunityId', mapOppIds.get(oppId), oppId);
            for(SObject obj : lstTmp3) lstClone.add(obj);
        }

        if(!lstClone.isEmpty()) insert lstClone;

        // reset the to be cloned flag to false
        List<Opportunity> lOpps = [SELECT Id, To_be_Cloned__c FROM Opportunity WHERE Id IN :mapOppIds.keySet()];
        for(Integer i = 0 ; i < lOpps.size(); i++) lOpps[i].To_be_Cloned__c = false;
        if(!lOpps.isEmpty()) update lOpps;
    }

    public static List<SObject> cloneChild(String childName, String sKey, Id origParentId, Id cloneParentId) {
        List<String> sObjectFields = new List<String>{};

        // Get all the fields from the selected object type
        if(childName == 'Sphere_of_Influence__c') {
            sObjectFields.addAll(Schema.sObjectType.Sphere_of_Influence__c.fields.getMap().keySet());
        }
        else if(childName == 'OpportunityTeamMember') {
            sObjectFields.addAll(Schema.sObjectType.OpportunityTeamMember.fields.getMap().keySet());
        }
        else if(childName == 'OpportunityLineItem') {
            sObjectFields.addAll(Schema.sObjectType.OpportunityLineItem.fields.getMap().keySet());
            // One can only specify either UnitPrice or TotalPrice while inserting Opportunity line items
            // So let us remove TotalPrice from the insert fields list
            for(Integer i = 0; i < sObjectFields.size(); i++) {
                if(sObjectFields[i] == 'TotalPrice') sObjectFields.remove(i);
            }
        }
        
        // Prepare the query to get all the fields
        String sObjQuery = 'SELECT ' + sObjectFields.get(0); 
    
        for(Integer i = 1; i < sObjectFields.size(); i++) {
            sObjQuery += ', ' + sObjectFields.get(i);
        }
    
        sObjQuery += ' FROM ' + childName + ' WHERE ' + sKey + ' = \'' + origParentId + '\'';         
        List<SObject> lstObj = Database.query(sObjQuery); // execute the query
        
        List<SObject> lstClone = new List<SObject>();
        for(SObject obj : lstObj) {
            SObject objClone = obj.clone(false);
            objClone.put(sKey, cloneParentId);
            lstClone.add(objClone);
        }
        
        return lstClone;
    }
}