@isTest
private class SuMoDisplayboardControllerTest {
    public static testMethod void testSuMoDisplayboardController(){

        DisplayBoard__c displayboard = new Displayboard__c(Name = 'Test Displayboard');
        insert displayboard;
        
        ID thisUser = UserInfo.getUserId();
             
        Test.startTest();

        ApexPages.Standardcontroller d = new ApexPages.Standardcontroller(displayboard);   
        SuMoDisplayboardController controller = new SuMoDisplayboardController(d);
    
        String leaderboard ='{"leaderboard": [{"userid": "' + thisUser +'","username": "Test User","experience": "10"}]}';
        Integer top = 5;
        SuMoDisplayboardController.fetchUserDisplayboardData(leaderboard,top);
        
        Test.StopTest();
  
    }  
}