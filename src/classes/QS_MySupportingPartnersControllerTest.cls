/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_MySupportingPartnersControllerTest {
    
    @isTest static void test_method_one() {
        QS_MySupportingPartnersController controller = new QS_MySupportingPartnersController();
        string UserId = controller.UserId;
        boolean selectedAccountNameHasChanged = controller.selectedAccountNameHasChanged;
        String selectedPartner = controller.selectedPartner;
        String selectedPartnerCon = controller.selectedPartnerCon;
        boolean ListFlag = controller.ListFlag;
        List<Case> resultsOpen = controller.resultsOpen;
        boolean resultsLicensesNeedsUpdate = controller.resultsLicensesNeedsUpdate;
        boolean resultsCaseNeedsUpdate = controller.resultsCaseNeedsUpdate;
        List<SelectOption> accountOptionListFiltered2 = controller.accountOptionListFiltered2;
        string AccountFilterText = controller.AccountFilterText;
        List<SelectOption> SpartnerConList = controller.SpartnerConList;
        String Contact_partner = controller.UserId;
        boolean Flag = controller.Flag;

        controller.selectedAccountName = 'Andy';
        string selectedAccountName = controller.selectedAccountName;


        controller.filterPartnerList();
        List<Entitlement> resultsOriginalLicenses = controller.resultsOriginalLicenses;
        List<Case> resultsCase = controller.resultsCase;
        List<QS_EntitlementWrapper> resultsLicenses = controller.resultsLicenses;
        integer SpartnerListSize = controller.SpartnerListSize;
        integer accountListSize = controller.accountListSize;
        controller.refreshLicenseList();
        controller.getCases();
    }
}