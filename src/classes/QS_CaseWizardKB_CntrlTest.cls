/*******************************************
*
*   Test class for QS_CaseWizardKB_Cntrl
*
*   2018-12-11 ext_bad  Add article feedback saving.
*   2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*
*******************************************/
@isTest
private class QS_CaseWizardKB_CntrlTest {

    private static Id recTypeId;

    @TestSetup
    static void setupData() {
        Organization org = [SELECT Id, IsSandbox, InstanceName FROM Organization LIMIT 1];
        String baseUrl = org.IsSandbox
                ? 'https://qlik--' + UserInfo.getUserName().substringAfterLast('.') + '.' + org.InstanceName + '.my.salesforce.com/'
                : 'https://qlik.my.salesforce.com';
        QTCustomSettingsHier__c qtSettings = new QTCustomSettingsHier__c();
        qtSettings.Base_URL__c = baseUrl;
        insert qtSettings;
    }
    
    // Unit Test for QS_CaseWizardKB_Cntrl which is referenced by QS_CaseWizard

    static testMethod void myUnitTestKBArticleCntrl() {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Employee';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Account partnerAccount = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile partnerProfileRec = [select id from profile where Name = 'PRM - Base' limit 1];
        String partnerProfileId = partnerProfileRec != null && partnerProfileRec.Id != null ? partnerProfileRec.Id : null;

        // Create Community User
        User partnerUser = TestDataFactory.createUser(partnerProfileId, 'testSandboxPartner@qlikTech.com', 'tPart', String.valueOf(partnerContact.Id));
        insert partnerUser;

        //in order to get the KnowledgeArticleId
        //Basic__kav basicKav = [SELECT Id, KnowledgeArticleId, ArticleNumber FROM Basic__kav
        //WHERE PublishStatus = 'Online' AND Language = 'en_US' AND IsVisibleInPrm = true
        //AND IsVisibleInPkb = true AND IsVisibleInCsp = true AND IsVisibleInApp = true
        //AND IsLatestVersion = true
        //LIMIT 1]; //WHERE Id = :diagnosticKav.Id];

        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('first apex', 'first from apex', 'first-test-apex-Basic', 'en_US');
        basicKav.IsVisibleInCsp = true;
        basicKav.IsVisibleInPkb = true;
        basicKav.IsVisibleInPRM = true;
        insert basicKav;

        basicKav = [SELECT KnowledgeArticleId, ArticleNumber FROM Basic__kav WHERE Id = :basicKav.Id];

        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);

        knowledgeArticleVersion knowledgeArticleVersion = [
                Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                From KnowledgeArticleVersion
                Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                Order By LastPublishedDate DESC, VersionNumber DESC
                LIMIT 1
        ];

        system.debug('knowledgeArticleVersion.PublishStatus: ' + knowledgeArticleVersion.PublishStatus);
        system.debug('knowledgeArticleVersion.IsLatestVersion: ' + knowledgeArticleVersion.IsLatestVersion);


        // Create Different Categories for 'QS_CaseWizard' Page
        Category_Lookup__c categoryLookupObj = TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level', 'Third Category Level Type', true, 'Please email urgently', 'question1', 'question2', 'question3', 'question4', 'question5', knowledgeArticleVersion.ArticleNumber, null, false, 'QlikTech Master Support Record Type', 'Systems Queue');
        insert categoryLookupObj;

        Basic__kav newBasicKav = TestDataFactory.createBasicKnowledgeArticle('test apex', 'test from apex', 'test-apex-Basic', 'en_US');
        insert newBasicKav;

        Diagnostic__kav diagnosticKav = TestDataFactory.createDiagnosticKnowledgeArticle('test apex', 'test from apex', 'test-apex', 'en_US');
        insert diagnosticKav;

        diagnosticKav = [Select Id, KnowledgeArticleId, ArticleNumber From Diagnostic__kav where Id = :diagnosticKav.Id LIMIT 1];
        //publish it
        KbManagement.PublishingService.publishArticle(diagnosticKav.KnowledgeArticleId, true);

        KnowledgeArticleVersion diagnosticKnowledgeArticleVersion = [
                Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                From KnowledgeArticleVersion
                Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :diagnosticKav.KnowledgeArticleId AND ArticleNumber = :diagnosticKav.ArticleNumber
                LIMIT 1
        ];


        Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                false, null, null, null, null, null, null);

        insert caseObj;

        //Run as current user
        ApexPages.currentPage().getParameters().put('id', knowledgeArticleVersion.Id);
        ApexPages.currentPage().getParameters().put('unauthenticated', 'true');
        QS_CaseWizardKB_Cntrl kbControllerCurrentUser = new QS_CaseWizardKB_Cntrl();
        kbControllerCurrentUser = new QS_CaseWizardKB_Cntrl();
        System.assertEquals(kbControllerCurrentUser.unauthenticatedUser, true);


        Test.startTest();
        System.runAs(communityUser) {

            ApexPages.currentPage().getParameters().put('id', knowledgeArticleVersion.Id);
            ApexPages.currentpage().getParameters().put('subject', 'Test_questionmark__questionmark__newline__newline_Subject');
            ApexPages.currentpage().getParameters().put('description', 'Test_questionmark__questionmark__newline__newline_Description');
            QS_CaseWizardKB_Cntrl kbControllerId = new QS_CaseWizardKB_Cntrl();

            string checkTestString = kbControllerId.basicArticleTypeDescription;
            checkTestString = kbControllerId.basicArticleTypeCause ;
            checkTestString = kbControllerId.basicArticleTypeResolution;
            checkTestString = kbControllerId.basicArticleTypeKnowledgeArticleId;
            checkTestString = kbControllerId.diagnosticArticleTypeDescription;
            checkTestString = kbControllerId.diagnosticArticleTypeCause;
            checkTestString = kbControllerId.diagnosticArticleTypeResolution ;
            checkTestString = kbControllerId.diagnosticArticleTypeKnowledgeArticleId;

            boolean checkTestBoolean = kbControllerId.showException;

            //kbControllerId.closeCase();
            // check if there's any exception
            //kbControllerId.closeCase();

            // Pass the parameters and open a knowledge article page from case wizard
            ApexPages.currentPage().getParameters().put('articleId', knowledgeArticleVersion.Id);
            ApexPages.currentPage().getParameters().put('unauthenticated', 'false');
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'true');
            ApexPages.currentpage().getParameters().put('subject', 'Test_questionmark__questionmark__newline__newline_Subject');
            ApexPages.currentpage().getParameters().put('description', 'Test_questionmark__questionmark__newline__newline_Description');

            ApexPages.currentPage().getParameters().put('severity', '3');
            ApexPages.currentPage().getParameters().put('recordTypeName', 'QlikTech Master Support Record Type');
            ApexPages.currentPage().getParameters().put('categoryId', categoryLookupObj.Id);

            QS_CaseWizardKB_Cntrl kbController = new QS_CaseWizardKB_Cntrl();
            System.assertEquals(kbController.isOpenCase, false);
            System.assertEquals(kbController.isCaseExists, false);
            System.assertEquals(kbController.unauthenticatedUser, false);
            System.assertEquals(kbController.invokeCaseWizard, true);
            kbController.closeCase();

            // Check if the status is set to what has been specified in the custom label
            //Doesn't work while workflow "Notify Reps that customer has responded via portal" is active
            //System.assertEquals([Select Id, CaseId, Case.Status, KnowledgeArticleId From CaseArticle where KnowledgeArticleId = :knowledgeArticleVersion.KnowledgeArticleId ORDER BY CreatedDate DESC LIMIT 1][0].Case.Status, System.label.QS_CloseCase);

            // Populate and pass all the paramteres to create a case and close a case when the user is redirected from the case wizard page
            ApexPages.currentPage().getParameters().put('articleId', knowledgeArticleVersion.Id);
            ApexPages.currentPage().getParameters().put('unauthenticated', 'false');
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'true');
            //ApexPages.currentpage().getParameters().put('subject', 'Test_questionmark__questionmark__newline__newline_Subject');
            //ApexPages.currentpage().getParameters().put('description', 'Test_questionmark__questionmark__newline__newline_Description');
            ApexPages.currentPage().getParameters().put('subject', 'Test Subject');
            ApexPages.currentPage().getParameters().put('description', 'Test Description');

            kbController = new QS_CaseWizardKB_Cntrl();

            System.assertEquals(kbController.isOpenCase, false);
            System.assertEquals(kbController.isCaseExists, false);
            System.assertEquals(kbController.unauthenticatedUser, false);
            System.assertEquals(kbController.invokeCaseWizard, true);
            // Unable to test this scenario as we've got System.LimitException: Too many SOQL queries: 101 in one of the triggers on case
            //kbController.closeCase();

            // Test if an unauthenticated user can close a case when redirected from case wizard page
            ApexPages.currentPage().getParameters().put('articleId', knowledgeArticleVersion.Id);
            ApexPages.currentPage().getParameters().put('unauthenticated', 'true');
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'true');
            kbController = new QS_CaseWizardKB_Cntrl();
            System.assertEquals(kbController.isOpenCase, false);
            System.assertEquals(kbController.isCaseExists, false);
            System.assertEquals(kbController.unauthenticatedUser, true);
            System.assertEquals(kbController.invokeCaseWizard, true);
            // Unable to test this scenario as we've got System.LimitException: Too many SOQL queries: 101 in one of the triggers on case
            //kbController.closeCase();

            // When we don't pass unauthenticated paramter for the user and try to close a case when the user isn't on case wizard page
            ApexPages.currentPage().getParameters().put('articleId', knowledgeArticleVersion.Id);
            //ApexPages.currentPage().getParameters().put('unauthenticated', 'true');
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'false');
            kbController = new QS_CaseWizardKB_Cntrl();
            System.assertEquals(kbController.isOpenCase, false);
            System.assertEquals(kbController.isCaseExists, false);
            //System.assertEquals(kbController.unauthenticatedUser, true);
            System.assertEquals(kbController.invokeCaseWizard, false);
            // Unable to test this scenario as we've got System.LimitException: Too many SOQL queries: 101 in one of the triggers on case
            //kbController.closeCase();

            // Check if caseArticles are created which are related to the article and the cases
            //List<CaseArticle> caseArticleList = [Select Id, CaseId, KnowledgeArticleId From CaseArticle where KnowledgeArticleId = :knowledgeArticleVersion.KnowledgeArticleId];
            //system.debug('caseArticleList.Size: ' + caseArticleList.Size());


            // Check if the close case button is available to already closed case
            ApexPages.currentPage().getParameters().put('articleId', knowledgeArticleVersion.Id);
            ApexPages.currentPage().getParameters().put('unauthenticated', 'false');
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'false');
            //ApexPages.currentpage().getParameters().put('caseId', caseArticleList[0].caseId+'/');
            ApexPages.currentpage().getParameters().put('caseId', caseObj.Id);


            kbController = new QS_CaseWizardKB_Cntrl();
            //Doesn't work while workflow "Notify Reps that customer has responded via portal" is active
            //System.assertEquals(kbController.isOpenCase, false);
            System.assertEquals(kbController.isCaseExists, true);
            System.assertEquals(kbController.unauthenticatedUser, false);
            System.assertEquals(kbController.invokeCaseWizard, false);

            // Unable to test this scenario as we've got System.LimitException: Too many SOQL queries: 101 in one of the triggers on case
            //kbController.closeCase();


            // Check if close case button is available and if we're successfully able to close an open case
            ApexPages.currentPage().getParameters().put('articleId', knowledgeArticleVersion.Id);
            ApexPages.currentPage().getParameters().put('unauthenticated', 'false');
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'false');
            ApexPages.currentpage().getParameters().put('caseId', caseObj.Id);

            kbController = new QS_CaseWizardKB_Cntrl();
            System.assertEquals(kbController.isOpenCase, true);
            System.assertEquals(kbController.isCaseExists, true);
            System.assertEquals(kbController.unauthenticatedUser, false);
            System.assertEquals(kbController.invokeCaseWizard, false);
            kbController.closeCase();
            Case caseRetrieveObj = [Select Id, Status, (Select Id From CaseArticles) From Case where Id = :caseObj.Id];
            //Doesn't work while workflow "Notify Reps that customer has responded via portal" is active
            //System.assertEquals(caseRetrieveObj.Status, System.label.QS_CloseCase);
            System.assert(caseRetrieveObj.CaseArticles != null && !caseRetrieveObj.CaseArticles.isEmpty());
            // Unable to test this scenario as we've got System.LimitException: Too many SOQL queries: 101 in one of the triggers on case
            //closeCase();

            // diagnosticKnowledgeArticleVersion is not shared with the community user so it will not be accessible to the user
            ApexPages.currentPage().getParameters().put('articleId', diagnosticKnowledgeArticleVersion.Id);
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'false');
            ApexPages.currentpage().getParameters().put('caseId', caseObj.Id);
            try {
                kbController = new QS_CaseWizardKB_Cntrl();
            } catch (Exception ex) {
                System.assertEquals(ex.getTypeName(), 'System.QueryException');
            }
        }

        // Running as the current user where the knowledge article is available to the user
        ApexPages.currentPage().getParameters().put('articleId', diagnosticKnowledgeArticleVersion.Id);
        ApexPages.currentpage().getParameters().put('fromCaseWizard', 'false');
        ApexPages.currentpage().getParameters().put('caseId', caseObj.Id);
        QS_CaseWizardKB_Cntrl kbController = new QS_CaseWizardKB_Cntrl();
        //Doesn't work while workflow "Notify Reps that customer has responded via portal" is active
        //System.assertEquals(kbController.isOpenCase, false);
        System.assertEquals(kbController.isCaseExists, true);
        System.assertEquals(kbController.unauthenticatedUser, false);
        System.assertEquals(kbController.invokeCaseWizard, false);


        System.runAs(partnerUser) {
            ApexPages.currentPage().getParameters().put('Id', knowledgeArticleVersion.Id);
            ApexPages.currentpage().getParameters().put('subject', 'Test_questionmark__questionmark__newline__newline_Subject');
            ApexPages.currentpage().getParameters().put('description', 'Test_questionmark__questionmark__newline__newline_Description');
            ApexPages.currentpage().getParameters().put('fromCaseWizard', 'true');
            ApexPages.currentPage().getParameters().put('severity', '3');
            ApexPages.currentPage().getParameters().put('recordTypeName', 'QlikTech Master Support Record Type');
            ApexPages.currentPage().getParameters().put('categoryId', categoryLookupObj.Id);
            try {
                QS_CaseWizardKB_Cntrl kbControllerPartnerTest = new QS_CaseWizardKB_Cntrl();
                kbControllerPartnerTest.closeCase();
            } catch (Exception ex) {
                System.assertEquals(ex.getTypeName(), 'System.QueryException');
            }
        }

        Test.stopTest();


    }

    static testMethod void testSetKBArticleReference() {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Employee';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Account partnerAccount = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile partnerProfileRec = [select id from profile where Name = 'PRM - Base' limit 1];
        String partnerProfileId = partnerProfileRec != null && partnerProfileRec.Id != null ? partnerProfileRec.Id : null;

        // Create Community User
        User partnerUser = TestDataFactory.createUser(partnerProfileId, 'testSandboxPartner@qlikTech.com', 'tPart', String.valueOf(partnerContact.Id));
        insert partnerUser;

        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('first apex', 'first from apex', 'first-test-apex-Basic', 'en_US');
        basicKav.IsVisibleInCsp = true;
        basicKav.IsVisibleInPkb = true;
        basicKav.IsVisibleInPRM = true;
        insert basicKav;

        basicKav = [SELECT KnowledgeArticleId, ArticleNumber FROM Basic__kav WHERE Id = :basicKav.Id];

        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);

        knowledgeArticleVersion knowledgeArticleVersion = [
                Select Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId, Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInApp
                From KnowledgeArticleVersion
                Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' AND knowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                Order By LastPublishedDate DESC, VersionNumber DESC
                LIMIT 1
        ];

        system.debug('knowledgeArticleVersion.PublishStatus: ' + knowledgeArticleVersion.PublishStatus);
        system.debug('knowledgeArticleVersion.IsLatestVersion: ' + knowledgeArticleVersion.IsLatestVersion);


        // Create Different Categories for 'QS_CaseWizard' Page
        Category_Lookup__c categoryLookupObj = TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level', 'Third Category Level Type', true, 'Please email urgently', 'question1', 'question2', 'question3', 'question4', 'question5', knowledgeArticleVersion.ArticleNumber, null, false, 'QlikTech Master Support Record Type', 'Systems Queue');
        insert categoryLookupObj;

        Basic__kav newBasicKav = TestDataFactory.createBasicKnowledgeArticle('test apex', 'test from apex', 'test-apex-Basic', 'en_US');
        insert newBasicKav;

        Diagnostic__kav diagnosticKav = TestDataFactory.createDiagnosticKnowledgeArticle('test apex', 'test from apex', 'test-apex', 'en_US');
        insert diagnosticKav;

        diagnosticKav = [Select Id, KnowledgeArticleId, ArticleNumber From Diagnostic__kav where Id = :diagnosticKav.Id LIMIT 1];
        //publish it
        KbManagement.PublishingService.publishArticle(diagnosticKav.KnowledgeArticleId, true);

        //Run as current user
        ApexPages.currentPage().getParameters().put('id', knowledgeArticleVersion.Id);
        QS_CaseWizardKB_Cntrl kbControllerCurrentUser = new QS_CaseWizardKB_Cntrl();
        kbControllerCurrentUser = new QS_CaseWizardKB_Cntrl();
        System.assertEquals(kbControllerCurrentUser.unauthenticatedUser, false);


        Test.startTest();
        System.runAs(communityUser) {

            ApexPages.currentPage().getParameters().put('id', knowledgeArticleVersion.Id);
            ApexPages.currentpage().getParameters().put('subject', 'Test_questionmark__questionmark__newline__newline_Subject');
            ApexPages.currentpage().getParameters().put('description', 'Test_questionmark__questionmark__newline__newline_Description');
            QS_CaseWizardKB_Cntrl kbControllerId = new QS_CaseWizardKB_Cntrl();
            Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', null, '3',
                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                    false, null, null, null, null, null, null);
            kbControllerId.caseObj = caseObj;
            kbControllerId.setKBArticleReference();

        }
    }
    
    static testMethod void testFeedback() {
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Employee';
        insert testContact;

        Basic__kav basicKav = TestDataFactory.createBasicKnowledgeArticle('first apex', 'first from apex', 'first-test-apex-Basic', 'en_US');
        basicKav.IsVisibleInCsp = true;
        basicKav.IsVisibleInPkb = true;
        basicKav.IsVisibleInPRM = true;
        basicKav.Description__c = '<a href="/articles/Basic/test-apex-Basic?articleId=987654" ></a>';
        insert basicKav;

        basicKav = [SELECT KnowledgeArticleId, Description__c, ArticleNumber FROM Basic__kav WHERE Id = :basicKav.Id];
        System.assert(basicKav.Description__c.contains('articleId=987654'));

        KbManagement.PublishingService.publishArticle(basicKav.KnowledgeArticleId, true);

        KnowledgeArticleVersion knowledgeArticleVersion = [
                SELECT Id, VersionNumber, UrlName, Title, Summary, PublishStatus, LastPublishedDate, KnowledgeArticleId,
                        Language, IsLatestVersion, ArticleType, ArticleNumber, IsVisibleInPrm, IsVisibleInPkb, IsVisibleInCsp,
                        IsVisibleInApp
                FROM KnowledgeArticleVersion
                WHERE IsLatestVersion = TRUE AND Language = 'en_US' AND PublishStatus = 'Online'
                                        AND KnowledgeArticleId = :basicKav.KnowledgeArticleId AND ArticleNumber = :basicKav.ArticleNumber
                ORDER BY LastPublishedDate DESC, VersionNumber DESC
                LIMIT 1
        ];

        // Create Different Categories for 'QS_CaseWizard' Page
        Category_Lookup__c categoryLookupObj = TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level', 'Third Category Level Type', true, 'Please email urgently', 'question1', 'question2', 'question3', 'question4', 'question5', knowledgeArticleVersion.ArticleNumber, null, false, 'QlikTech Master Support Record Type', 'Systems Queue');
        insert categoryLookupObj;

        //Run as current user
        ApexPages.currentPage().getParameters().put('id', knowledgeArticleVersion.Id);
        ApexPages.currentPage().getParameters().put('articleId', knowledgeArticleVersion.ArticleNumber);

        QS_CaseWizardKB_Cntrl ctrl = new QS_CaseWizardKB_Cntrl();
        System.assert(!ctrl.basicArticleType.Description__c.contains('articleId=987654'));

        ctrl.commentText = 'test';

        System.assertEquals(0, [SELECT Id FROM Case].size());

        Test.startTest();
        ctrl.submitComment();
        Test.stopTest();

        System.assert(ctrl.showThankYou);
        System.assert(!ctrl.showComment);
        System.assertNotEquals(0, [SELECT Id FROM Case WHERE Knowledge_Article__c = :knowledgeArticleVersion.Id].size());
        System.assert(ctrl.emailSent);

    }

    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if (recTypeId != null) {
            return recTypeId;
        } else {

            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }


}