/********
* NAME : ResponsiblePartners
* Description: Populate partners to the responsible partner custom field
* Create by: IRN 2015-12-03
*
*Change Log:
2016-04-05	IRN CR33068 fix for only adding partner accounts as a responsible partner if they is set as support provided by or reseller on an account
2016-04-12 	IRN CR33068 fix duplicate responsible partner bug 
2016-05-19 	IRN CR85489 populating new field  Responsible_Partner_Highlights__c with maximum 225 characters
******/
global class ResponsiblePartners implements Database.Batchable<sObject>, schedulable{

	global void execute (SchedulableContext sc)
	{
		System.Debug('--excecute--');
        ID batchId = Database.executeBatch(this); 
	}

	global Database.QueryLocator start(Database.BatchableContext bc){
        System.Debug('Starting: batchable responsible partner');
        return Database.getQueryLocator([Select Id, End_User__c, Reseller__c, Responsible_Partner__c From NS_Support_Contract__C]);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope){
    	List<NS_Support_Contract__C> contracts = (List<NS_Support_Contract__C>)scope;
    	Set<Id> uniqueEndUserSet = new Set<Id>();
		for(Integer i = 0;  i< contracts.size(); i++){
			if(contracts[i].End_User__c != null){
				uniqueEndUserSet.add(contracts[i].End_User__c);	
			}
		}
		List<Id> uniqueEndUser = new List<Id>();
		uniqueEndUser.addAll(uniqueEndUserSet);  
		populateResponsiblePartners(uniqueEndUser);
		
		//update sharing for NS Support Contracts
		NSSupportContractSharing nsSupportContractSharing = new nsSupportContractSharing();
		Map<Id, List<Id>> uniquePartners = ApexSharingRules.getUniquePartnersToShareWith(contracts);
		nsSupportContractSharing.shareContractsToPartner(uniquePartners);
    }

    global void finish(Database.BatchableContext bc){
    }

	public void populateResponsiblePartners(List<Id> endUsers){
		System.debug('----populateResponsiblePartners----');
		updateResponsiblePartnersRelatedRecords(endUsers);
		populatePartnerAccountNameAndNbr(endUsers);
	}

	public Responsible_Partner__c createRelatedListRecord(Id endUser, Id partner){
		Responsible_Partner__c resp_partner = new Responsible_Partner__c(End_User__c = endUser,  Partner__c = partner);
		return resp_partner;
	}

	public Responsible_Partner__c deleteRelatedListRecord(Id endUser, Id partner){
		List<Responsible_Partner__c> respPartnerRecord = [Select Id, End_User__c, Partner__c from responsible_Partner__c where End_User__c =: endUser and Partner__c =: partner];
		if(respPartnerRecord != null && respPartnerRecord.size()>0){
			return respPartnerRecord[0];	
		}
		return null;
	}

	public void updateResponsiblePartnersRelatedRecords(list<Id> endUsers){
		List<responsible_Partner__c> responsiblePartnersList = [Select Id, End_User__c, Partner__C from Responsible_Partner__c where End_User__c in: endUsers];
		List<NS_Support_Contract__c> ns_contracts = [Select Id, Name, End_User__c, Reseller__c, Responsible_Partner__c From NS_Support_Contract__C where End_User__c in: endUsers];
		
		//get all Responsible_Partner__c account data and if type is a partner account add it
		Set<Id>responsiblePartnersInContracts = new Set<Id>(); 
		for(Integer i = 0; i<ns_contracts.size(); i++)
		{
			if(ns_contracts[i].Responsible_Partner__c != null){
				responsiblePartnersInContracts.add(ns_contracts[i].Responsible_Partner__c);
			}

			if(ns_contracts[i].Reseller__c != null){
				responsiblePartnersInContracts.add(ns_contracts[i].Reseller__c);
			}
		}
		List<Account> accountsForResponsiblePartnersInContracts  = [Select Id, IsPartner from Account where Id in : responsiblePartnersInContracts];
		Map<Id, Boolean> isPartnerAccount = new Map<Id, Boolean>();
		for(Account acc : accountsForResponsiblePartnersInContracts){
			isPartnerAccount.put(acc.Id, acc.IsPartner);
		}
		
		Map<Id, Set<Id>> respPartnerSet = new Map<Id, Set<Id>>(); 
		for(Integer i = 0; i<responsiblePartnersList.size(); i++){
			if(respPartnerSet.containsKey(responsiblePartnersList[i].End_User__c)){
				Set<Id> temp = respPartnerSet.get(responsiblePartnersList[i].End_User__c);
				temp.add(responsiblePartnersList[i].Partner__c);
			}else{
				Set<Id> temp = new Set<Id>();
				temp.add(responsiblePartnersList[i].Partner__c);
				respPartnerSet.put(responsiblePartnersList[i].End_User__c, temp);
			}
		}

		Map<Id, Set<Id>> partnersIdIncontracts = new Map<Id, Set<Id>>();
		List<Responsible_Partner__c> recordsToInsert = new List<Responsible_Partner__c>();
		for(Integer i = 0; i<ns_contracts.size(); i++)
		{
			System.debug('ns_contracts ' + ns_contracts[i]);
			//selltrhough partner
			if(ns_contracts[i].Reseller__c != null && !partnerAlreadyExistInList(partnersIdIncontracts, respPartnerSet, ns_contracts[i].Reseller__c, ns_contracts[i].End_User__c)
				&& isPartnerAccount.get(ns_contracts[i].Reseller__c)){
				recordsToInsert.add(createRelatedListRecord(ns_contracts[i].End_User__c, ns_contracts[i].Reseller__c));
			}
			System.debug('isPartnerAccount ' + isPartnerAccount);
			//support provided by
			if(ns_contracts[i].Responsible_Partner__c != null  && !partnerAlreadyExistInList(partnersIdIncontracts, respPartnerSet, ns_contracts[i].Responsible_Partner__c, ns_contracts[i].End_User__c) 
				&& isPartnerAccount.get(ns_contracts[i].Responsible_Partner__c) && ns_contracts[i].Responsible_Partner__c != ns_contracts[i].Reseller__c ){
				recordsToInsert.add(createRelatedListRecord(ns_contracts[i].End_User__c, ns_contracts[i].Responsible_Partner__c));
			}

			if(ns_contracts[i].Reseller__c != null && isPartnerAccount.get(ns_contracts[i].Reseller__c)){
				if(partnersIdIncontracts.containsKey(ns_contracts[i].End_User__c)){
					Set<Id> temp = partnersIdIncontracts.get(ns_contracts[i].End_User__c);
					temp.add(ns_contracts[i].Reseller__c);
				}else{
					Set<Id> temp = new Set<Id>();
					temp.add(ns_contracts[i].Reseller__c);
					partnersIdIncontracts.put(ns_contracts[i].End_User__c, temp);
				}
			}
			if(ns_contracts[i].Responsible_Partner__c != null && isPartnerAccount.get(ns_contracts[i].Responsible_Partner__c)){
				if(partnersIdIncontracts.containsKey(ns_contracts[i].End_User__c)){
					Set<Id> temp = partnersIdIncontracts.get(ns_contracts[i].End_User__c);
					temp.add(ns_contracts[i].Responsible_Partner__c);
				}else{
					Set<Id> temp = new Set<Id>();
					temp.add(ns_contracts[i].Responsible_Partner__c);
					partnersIdIncontracts.put(ns_contracts[i].End_User__c, temp);
				}	
			}
		}
		
		Integer batchsize = 200;
		if(recordsToInsert != null && recordsToInsert.size()>0){
			for(Integer i = 0; i<(recordsToInsert.size()/batchsize)+1; i++){
				 List<Responsible_Partner__c> tempList;
            	if (recordsToInsert.size() > ((i + 1) * BATCHSIZE))
            	{
                	tempList = getRange(recordsToInsert, i * BATCHSIZE, BATCHSIZE);
            	}
            	else
            	{
                	tempList = getRange(recordsToInsert, i * BATCHSIZE, recordsToInsert.size() - (i * BATCHSIZE));
            	}

            	System.debug('before sharing ' + tempList);
            	try{
            		AccountSharing.addRelatedListAccountSharing(tempList);
            		insert tempList;
            	}catch(Exception e){
            			System.debug('ERROR when updateing addRelatedListAccountSharing :' + e);
                		System.debug('ERROR tempList.size :' + tempList.size());
                	String idResponsiblePartner = '';
                	for(Responsible_Partner__c r : tempList){
                		idResponsiblePartner = idResponsiblePartner + '(Id=' + r.Id +' endUser= ' + r.End_User__c + ' partner= '+ r.Partner__c + '), ';
                	}
                	System.debug('ERROR responsible partners id to insert :' + tempList);
            		}
			}
		}
		
		List<Responsible_Partner__c> recordsToDelet = new List<Responsible_Partner__c>();
		for(Id enduser : respPartnerSet.keySet()){
			Set<Id> oldRespPartner = respPartnerSet.get(endUser);
			Set<Id> newRespPartner = partnersIdIncontracts.get(endUser);
			if(newRespPartner == null || (newRespPartner!= null && !newRespPartner.containsAll(oldRespPartner))){		
				//remove all partners that is not in the 
				for (Id partner : oldRespPartner){
					Responsible_Partner__c record = deleteRelatedListRecord(endUser, partner);
					if(newRespPartner == null || (!newRespPartner.contains(partner) && record != null)){
						recordsToDelet.add(record);
					}
				}
			}
		}
		if(recordsToDelet != null && recordsToDelet.size()>0){
			for(Integer i = 0; i<(recordsToDelet.size()/batchsize)+1; i++){
				 List<Responsible_Partner__c> tempList;
            	if (recordsToDelet.size() > ((i + 1) * BATCHSIZE))
            	{
                	tempList = getRange(recordsToDelet, i * BATCHSIZE, BATCHSIZE);
            	}
            	else
            	{
                	tempList = getRange(recordsToDelet, i * BATCHSIZE, recordsToDelet.size() - (i * BATCHSIZE));
            	}
            	try{
            		AccountSharing.deleteRelatedListAccountSharing(tempList);
            		System.debug('before deleting records ' + tempList);
					delete tempList;
            	}
            	catch(Exception e){
            		System.debug('ERROR when updateing addRelatedListAccountSharing :' + e);
                	System.debug('ERROR tempList.size :' + tempList.size());
                	System.debug('ERROR tempList.size :' + tempList);
            	}
				
				
			}
		}
	}	

	public List<Responsible_Partner__c> getRange(List<Responsible_Partner__c> fromList, Integer fromIndex, Integer amount)
    {
        List<Responsible_Partner__c> returnList = new List<Responsible_Partner__c>();
        for(Integer i = fromIndex; i < fromIndex + amount; i++){
            returnList.add(fromList.get(i));
        }
        return returnList;
    }

	public void deleteResponsiblePartnerRecord(List<NS_Support_Contract__C> deletedContracts, List<Id> endUsers){
		System.debug('----deleteResponsiblePartnerRecord----');
		List<Responsible_Partner__c> responsiblePartnersList = [Select Id, End_User__c, Partner__C from Responsible_Partner__c where End_User__c in: endUsers];
		Map<Id, List<Responsible_Partner__c>> respPartnerMap = new Map<Id, List<Responsible_Partner__c>>();
		for(Responsible_Partner__c rp : responsiblePartnersList){
			if(respPartnerMap.containsKey(rp.End_User__c)){
				List<Responsible_Partner__c> temp = respPartnerMap.get(rp.End_User__c);
				temp.add(rp);
			}else{
				List<Responsible_Partner__c> temp = new List<Responsible_Partner__c>();
				temp.add(rp);
				respPartnerMap.put(rp.End_User__c, temp);
			}
		}
		List<responsible_Partner__c> recordsToDelete = new List<Responsible_Partner__c>();
		for(Integer i = 0; i<deletedContracts.size(); i++){
			Set<Id> partnersToDelete = new Set<Id>();
			if(deletedContracts[i].Reseller__c != null){
				partnersToDelete.add(deletedContracts[i].Reseller__c);	
			}
			if(deletedContracts[i].responsible_Partner__c != null){
				partnersToDelete.add(deletedContracts[i].responsible_Partner__c);
			}
			List<Responsible_Partner__c> respPartnersForEndUser = respPartnerMap.get(deletedContracts[i].End_User__c);
			if(partnersToDelete != null && partnersToDelete.size()>0 && respPartnersForEndUser != null && respPartnersForEndUser.size()>0){
				for(Id p : partnersToDelete){
					for(Responsible_Partner__c respPartner : respPartnersForEndUser){
						if(respPartner.Partner__c == p){
							recordsToDelete.add(respPartner);
							break;
						}	
					}
					
				}
			}

		}
		system.debug('records to delete ' + recordsToDelete);
		if(recordsToDelete != null && recordsToDelete.size() > 0){
			delete recordsToDelete;	
		}
		populatePartnerAccountNameAndNbr(endUsers);
	}

	public boolean partnerAlreadyExistInList(Map<Id, Set<Id>> partnersIdIncontracts, Map<Id, Set<Id>> respPartnerSet, Id partner, Id endUser){
		System.debug('partnerAlreadyExistInList ' + partnersIdIncontracts + ' respPartnerSet' + respPartnerSet + '  partner ' + partner +' endUser ' + endUser);
		boolean result = false;
		if(respPartnerSet.containsKey(endUser)){
			Set<Id> temp = respPartnerSet.get(endUser);
			if(temp.contains(partner)){
				result = true;
			}
		}
		if(partnersIdInContracts.containsKey(endUser)){ //IRN CR33068 changed else if to if to fix duplicate responsible partner bug 
			Set<Id> temp = partnersIdInContracts.get(endUser);
			if(temp.contains(partner)){
				result = true;
			}
		}
		System.debug('partnerAlreadyExistInList ' + result);
		return result;
	}

	 public void populatePartnerAccountNameAndNbr(List<Id> accountsIds){
	    List<Responsible_Partner__c> respPartnerNames = [Select Id, Partner__c, End_user__c, Partner__r.Name from Responsible_Partner__c where End_user__c in : accountsIds];
	    List<Account> accounts = [Select Id, Responsible_Partner_Account_Name__c, Responsible_Partner_count__c, Responsible_Partner_Highlights__c from Account where Id in : accountsIds];
	    System.debug('respPartnerNames ' + respPartnerNames);
	    Map<Id, List<String>> accountResponsiblePartnersNameMap = new Map<Id, List<String>>();

	    for(Responsible_Partner__c partner: respPartnerNames){
	      if(accountResponsiblePartnersNameMap.containsKey(partner.End_user__c)){
	        List<String> temp = accountResponsiblePartnersNameMap.get(partner.End_user__c);
	        temp.add(partner.Partner__r.Name);
	      }else{
	        List<String> temp = new List<String>();
	        temp.add(partner.Partner__r.Name);
	        accountResponsiblePartnersNameMap.put(partner.End_user__c, temp);
	      }
	    }

	    List<Account> tobeupdated = new List<Account>();
	    System.debug('accounts ' +accounts);
	    for(Account a : accounts){
	      if(accountResponsiblePartnersNameMap.containsKey(a.Id)){
	        List<String> partnerNamesList = accountResponsiblePartnersNameMap.get(a.Id);
	        String partnerNames = '';
	        String partnerNamesMax225 = '...';
	        Boolean maxIsreached = false;
	        for(Integer i = 0; i<partnerNamesList.size(); i++){
	        	String temp = partnerNames;
	          	if(partnerNames == ''){
	            	partnerNames = partnerNamesList[i];
	          	}else{
	            	partnerNames = partnerNames + ', ' + partnerNamesList[i];    
	          	}
	          	Integer nameSize = partnerNames.length() +3;
	        	if(nameSize<225){
	        		partnerNamesMax225 = partnerNames;    
	          	}else if(!maxIsreached){
	          		maxIsreached = true;
	          		partnerNamesMax225 = temp + '...';
	          	}
	        }
	        if(a.Responsible_Partner_Account_Name__c != partnerNames){
	          a.Responsible_Partner_Account_Name__c = partnerNames;
	          a.Responsible_Partner_Highlights__c = partnerNamesMax225;
	          a.Responsible_Partner_count__c = partnerNamesList.size();
	          tobeupdated.add(a);
	        }
	      }else{
	        a.Responsible_Partner_Account_Name__c = '';
	        a.Responsible_Partner_Highlights__c = '';
	        a.Responsible_Partner_count__c = 0;
	        tobeupdated.add(a);
	      }

	    }

	    if(tobeupdated.size()>0){
	      update tobeupdated;
	    }  
	  }
	
}

/*

Run theses lines in Debug console - > open Excecute anonymous window

ResponsiblePartners r = new ResponsiblePartners();
String sch = '0 0 11,23 * * ?';
String jobID = system.schedule('Populate responsible partners Job', sch, r);

press excecute

*/