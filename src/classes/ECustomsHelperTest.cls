/**
29.03.2017 Rodion Vakulvoskyi fix test failures commenting QuoteTestHelper.createCustomSettings();
2017-05-30 Rodion Vakulovskyi edited to fix teset error qcw 2496
2017-08-04 Aslam Kamal code coverage update for QCW-2934
2017-09-01 Srinivasan PR  fix for query error	
06-09-2017 Linus Löfberg Q2CW-2953 setting quote recipient to conform with added validation conditions.
2019-02-13 Björn Andersson fixed test class error related to new duplicate rules on account and contact
**/
@isTest
private class ECustomsHelperTest {

	@testSetup
	static void testSetup() {
		QTTestUtils.GlobalSetUp();
		QTTestUtils.SetupNSPriceBook('USD');
		//QuoteTestHelper.createCustomSettings();

		Profile p = [select id from profile where name='System Administrator'];
		User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, isActive = true,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com');
		insert u;
		Id AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id;

        System.runAs(u) {
			Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
			testAccount.Name = 'Test Val Rule 1';
			testAccount.BillingCountry = 'France';
	        update testAccount;
			Contact testUserContact = QTTestUtils.createMockContact();
            testUserContact.AccountId = testAccount.id;
            update testUserContact;

			QlikTech_Company__c companyQT = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c];
	        Contact testContact = QTTestUtils.createMockContact(); // contact lastname = 'RDZTestLastName'
	        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query errors
	        Account testPartnerAccount = QuoteTestHelper.createAccount(companyQT, rTypeAcc2, 'Partner'); // acc name = 'PartnerAcc'
	        testPartnerAccount.Name = 'PartnerAcc';
	        testPartnerAccount.Partner_Margin__c = 12;
	        testPartnerAccount.Navision_Status__c = 'Partner';
			insert	testPartnerAccount;
			testContact.AccountId = testPartnerAccount.id;
			update testContact;
			
		   /******* changes for qcw-2934 start *********/
           insert QuoteTestHelper.createPCS(testPartnerAccount);
           /******* changes for qcw-2934 end *********/

	        Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , true);
	        System.assertNotEquals(null, pr.Id);
	        System.assertEquals(true, pr.Generate_License__c);

	    }
	}
    
    @isTest
	static void TestNewVisualComplianceEnabled()
    {
        User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
        Test.startTest();
        System.runAs(u) {
            Boolean status = ECustomsHelper.NewVisualComplianceEnabled;
            System.assertEquals(false, status); 
        }
        Test.stopTest();
    }
    
    @isTest
    static void TestVCStatus()
    {
        User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
        Test.startTest();
        System.runAs(u) {
            Boolean status = ECustomsHelper.VCStatus('Direct', 'No Matches', 'Cleared','No Matches', 'Cleared');
            System.assertEquals(true, status);
            status = ECustomsHelper.VCStatus('Reseller', 'No Matches', 'Cleared','No Matches', 'Cleared');
            System.assertEquals(false, status);
            status = ECustomsHelper.VCStatus('Reseller', 'Matches', 'Cleared','Matches', 'Alert');
            System.assertEquals(false, status);
        }
        Test.stopTest();
    }
    @isTest
    static void TestVCPartnerAccountApproved()
    {
        User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
        Test.startTest();
        System.runAs(u) {
            Boolean status = ECustomsHelper.VCPartnerAccountApproved('Direct', 'No Matches', 'Cleared');
            System.assertEquals(true, status);
            status = ECustomsHelper.VCPartnerAccountApproved('Reseller', 'No Matches', 'Cleared');
            System.assertEquals(true, status);
            status = ECustomsHelper.VCPartnerAccountApproved('Reseller', 'Matches', 'Alert');
            System.assertEquals(false, status);
        }
        Test.stopTest();
    }
    
	@isTest
	static void test_CheckVisualCompliance() {
		Test.startTest();

		User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, ECUSTOMS__RPS_Status__c, ECUSTOMS__RPS_Date__c,Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, ECUSTOMS__RPS_Status__c, ECUSTOMS__RPS_Date__c, Name From Account Where Name = 'PartnerAcc'];
        Contact testContact = [Select Id, Email, Name From Contact Where AccountId = :testPartnerAccount.Id];
        Product2 pr = [Select Id, Name, Family, isActive, Generate_License__c From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];
        System.assertEquals('Licenses', pr.Family);
        System.assertEquals(true, pr.isActive);
        System.assertEquals(true, pr.Generate_License__c);

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
		quoteForTest.Quote_Recipient__c = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id LIMIT 1].Id;
		insert quoteForTest;
		System.assertNotEquals(null, quoteForTest.Id);
		System.debug('Test quote: ' + quoteForTest);

		SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050, 20, '12345', null);
		System.assertNotEquals(null, quoteLine.Id);
        SBQQ__Quote__c quoteApproved = quoteForTest.Clone(true);
        quoteApproved.ApprovalStatus__c = 'Approved';
        List<SBQQ__Quote__c> newquotes = new List<SBQQ__Quote__c> {quoteApproved};
        Map<ID, SBQQ__Quote__c> OldSBQQQuoteMap = new Map<ID, SBQQ__Quote__c>{ quoteForTest.Id => quoteForTest };
		System.runAs(u) {
            ECustomsHelper eCustomsHelper= new ECustomsHelper();
			eCustomsHelper.CheckVisualCompliance(newquotes, OldSBQQQuoteMap);
		}

		Test.stopTest();
	}

    @isTest
    static void test_CheckVisualComplianceRedCountry() {
        Test.startTest();

        User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
        Account testAccount = [Select Id, ECUSTOMS__RPS_Status__c, ECUSTOMS__RPS_Date__c,Name From Account Where Name = 'Test Val Rule 1'];
        testAccount.BillingCountry ='Cuba';
        update testAccount;
        Account testPartnerAccount = [Select Id, ECUSTOMS__RPS_Status__c, ECUSTOMS__RPS_Date__c, Name From Account Where Name = 'PartnerAcc'];
        testPartnerAccount.BillingCountry ='Iran';
        update testPartnerAccount;
        Contact testContact = [Select Id, Email, Name From Contact Where AccountId = :testPartnerAccount.Id];
        Product2 pr = [Select Id, Name, Family, isActive, Generate_License__c From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];
        System.assertEquals('Licenses', pr.Family);
        System.assertEquals(true, pr.isActive);
        System.assertEquals(true, pr.Generate_License__c);

        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
        quoteForTest.Quote_Recipient__c = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id LIMIT 1].Id;
        insert quoteForTest;
        System.assertNotEquals(null, quoteForTest.Id);
        System.debug('Test quote: ' + quoteForTest);

        SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050, 20, '12345', null);
        System.assertNotEquals(null, quoteLine.Id);
        SBQQ__Quote__c quoteApproved = quoteForTest.Clone(true);
        quoteApproved.ApprovalStatus__c = 'Approved';
        List<SBQQ__Quote__c> newquotes = new List<SBQQ__Quote__c> {quoteApproved};
        Map<ID, SBQQ__Quote__c> OldSBQQQuoteMap = new Map<ID, SBQQ__Quote__c>{ quoteForTest.Id => quoteForTest };
        System.runAs(u) {
            ECustomsHelper eCustomsHelper= new ECustomsHelper();
            eCustomsHelper.CheckVisualCompliance(newquotes, OldSBQQQuoteMap);
        }

        Test.stopTest();
    }

	@isTest
	static void test_UpdateECustomsStatus() {

		// Placeholder to cover method that is currently mostly commented out.

		Test.startTest();

		User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name = 'PartnerAcc'];
        Contact testContact = [Select Id, Name, Email From Contact Where AccountId = :testPartnerAccount.Id];
        Product2 pr = [Select Id, Name, Family, isActive, Generate_License__c From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];
        System.assertEquals('Licenses', pr.Family);
        System.assertEquals(true, pr.isActive);
        System.assertEquals(true, pr.Generate_License__c);

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
		quoteForTest.Quote_Recipient__c = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id LIMIT 1].Id;
		insert quoteForTest;
		System.assertNotEquals(null, quoteForTest.Id);
		System.debug('Test quote: ' + quoteForTest);

		SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050, 20, '12345', null);
		System.assertNotEquals(null, quoteLine.Id);

		List<SBQQ__Quote__c> qList = new List<SBQQ__Quote__c>();
		qList.add(quoteForTest);



		Test.stopTest();
	}

	@isTest
	static void test_UpdateObjectStatus() {

		Test.startTest();

		User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name = 'PartnerAcc'];
        Contact testContact = [Select Id, Name, Email From Contact Where AccountId = :testPartnerAccount.Id];
        Product2 pr = [Select Id, Name, Family, isActive, Generate_License__c From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];
        System.assertEquals('Licenses', pr.Family);
        System.assertEquals(true, pr.isActive);
        System.assertEquals(true, pr.Generate_License__c);

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
		quoteForTest.Quote_Recipient__c = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id LIMIT 1].Id;
		insert quoteForTest;
		System.assertNotEquals(null, quoteForTest.Id);
		System.debug('Test quote: ' + quoteForTest);

		SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050, 20, '12345', null);
		System.assertNotEquals(null, quoteLine.Id);

		Address__c a = QuoteTestHelper.createAddress(testPartnerAccount.Id, testContact.Id, 'Shipping');
		insert a;

		List<Address__c> addressList = new List<Address__c>();
		addressList.add(a);

		Map<Id, Address__c> addressMap = new Map<Id, Address__c>();
		addressMap.put(a.Id, a);

		

		Test.stopTest();
	}

	@isTest
	static void test_IsinRedCountryList() {

		Test.startTest();

		ECustomsHelper ecHelper = new ECustomsHelper();
		

		Test.stopTest();
	}

	@isTest
	static void test_IsinYellowCountryList() {

		Test.startTest();

		ECustomsHelper ecHelper = new ECustomsHelper();
		
		Test.stopTest();
	}

	@isTest
	static void test_IsWithinTwoDays() {

		Test.startTest();

		ECustomsHelper ecHelper = new ECustomsHelper();
		
		Test.stopTest();
	}

	@isTest
	static void test_updateCheckRedCountry() {

		Test.startTest();

		User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name = 'PartnerAcc'];
        Contact testContact = [Select Id, Email, Name From Contact Where AccountId = :testPartnerAccount.Id];
        Product2 pr = [Select Id, Name, Family, isActive, Generate_License__c From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];
        System.assertEquals('Licenses', pr.Family);
        System.assertEquals(true, pr.isActive);
        System.assertEquals(true, pr.Generate_License__c);

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
		quoteForTest.Quote_Recipient__c = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id LIMIT 1].Id;
		insert quoteForTest;
		System.assertNotEquals(null, quoteForTest.Id);
		System.debug('Test quote: ' + quoteForTest);

		SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050, 20, '12345', null);
		System.assertNotEquals(null, quoteLine.Id);

		Address__c a = QuoteTestHelper.createAddress(testPartnerAccount.Id, testContact.Id, 'Shipping');
		insert a;

		List<Address__c> addressList = new List<Address__c>();
		addressList.add(a);
		
		Test.stopTest();
	}

	@isTest
	static void test_IsApiUser() {

		Test.startTest();

		Profile p = [select id from profile where name='Custom: Api Only User'];
		User u = new User(alias = 'apite', email='standarduser@testorgtest.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, isActive = true,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr1.com');
		insert u;
		

		Test.stopTest();

	}
	
	@isTest
	static void test_CheckRestrictedCompany() {
		Test.startTest();

		User u = [Select Id, Lastname, Email From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name = 'PartnerAcc'];
        Contact testContact = [Select Id, Email, Name From Contact Where AccountId = :testPartnerAccount.Id];
        Product2 pr = [Select Id, Name, Family, isActive, Generate_License__c From Product2 Where Name = 'QlikView Enterprise Edition Server' Limit 1];
        System.assertEquals('Licenses', pr.Family);
        System.assertEquals(true, pr.isActive);
        System.assertEquals(true, pr.Generate_License__c);

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.Id, 'Reseller', 'Open', 'Quote', false, '');
		quoteForTest.Quote_Recipient__c = [SELECT Id FROM Contact WHERE AccountId = :testAccount.Id LIMIT 1].Id;
		insert quoteForTest;
		System.assertNotEquals(null, quoteForTest.Id);
		System.debug('Test quote: ' + quoteForTest);

		SBQQ__QuoteLine__c quoteLine = TestQuoteUtil.createMockQuoteLine(quoteForTest.Id, pr.Id, 'QlikView Enterprise Edition Server', 1, 'SENSE', 'QlikView', 'Licenses', 'USD', 9050, 20, '12345', null);
		System.assertNotEquals(null, quoteLine.Id);
		
		

		Test.stopTest();
	}


    @isTest
    static void test_CheckVisualCompliancezuora() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'No Matches';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'No Matches';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';

        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 339', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'No Matches';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
		partnerContact.FirstName = 'James';    
        partnerContact.LastName = 'Testsson';    
        partnerContact.Email = 'stevetest@testdsds.com';    
        partnerContact.Phone = '414-123-4444';               
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;
        Test.startTest();
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        zquote.ApprovalStatus__c = 'Pending';
        insert zquote;
        
        zqu__Quote__c quoteApproved = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        quoteApproved.zqu__BillToContact__c = endUserContact.id;
        quoteApproved.zqu__SoldToContact__c = endUserContact.id;
        quoteApproved.ApprovalStatus__c = 'Approved';
        insert quoteApproved;
        zqu__Quote__c zquote13 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,ApprovalStatus__c,Sell_Through_Partner__c,Sell_Through_Partner_Id__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :quoteApproved.Id];
        List<zqu__Quote__c> newquotes = new List<zqu__Quote__c> {zquote13};
        system.debug('gggg'+newquotes);
        Test.stopTest();
        zqu__Quote__c zquote12 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,ApprovalStatus__c,Sell_Through_Partner__c,Sell_Through_Partner_Id__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        
       
        Map<ID, zqu__Quote__c> OldSBQQQuoteMap = new Map<ID, zqu__Quote__c>{ zquote13.Id => zquote12 };
        ECustomsHelper eCustomsHelper= new ECustomsHelper();
            eCustomsHelper.zuora_CheckVisualCompliance(newquotes, OldSBQQQuoteMap);
        
    }

    
    //Testing red countries for zuora visual compliance.
    @isTest
    static void test_CheckVisualCompliancezuoraRedCountry() {
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'No Matches';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        endUserAccount.BillingCountry='Cuba';
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'No Matches';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';
        endUserAccount.BillingCountry='Iran';

        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 339', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'No Matches';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
		partnerContact.FirstName = 'Jasper';    
        partnerContact.LastName = 'Johnsson';    
        partnerContact.Email = 'jonsontest@partners.com';    
        partnerContact.Phone = '156-717-1123';          
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;
        Test.startTest();
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        zquote.ApprovalStatus__c = 'Pending';
        insert zquote;
        
        zqu__Quote__c quoteApproved = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        quoteApproved.zqu__BillToContact__c = endUserContact.id;
        quoteApproved.zqu__SoldToContact__c = endUserContact.id;
        quoteApproved.ApprovalStatus__c = 'Approved';
        insert quoteApproved;
        zqu__Quote__c zquote13 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,ApprovalStatus__c,Sell_Through_Partner__c,Sell_Through_Partner_Id__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :quoteApproved.Id];
        List<zqu__Quote__c> newquotes = new List<zqu__Quote__c> {zquote13};
        system.debug('gggg'+newquotes);
        Test.stopTest();
        zqu__Quote__c zquote12 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,ApprovalStatus__c,Sell_Through_Partner__c,Sell_Through_Partner_Id__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        
       
        Map<ID, zqu__Quote__c> OldSBQQQuoteMap = new Map<ID, zqu__Quote__c>{ zquote13.Id => zquote12 };
        ECustomsHelper eCustomsHelper= new ECustomsHelper();
            eCustomsHelper.zuora_CheckVisualCompliance(newquotes, OldSBQQQuoteMap);
        
    }

}