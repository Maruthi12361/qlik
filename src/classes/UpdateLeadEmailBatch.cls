/**     * File Name:UpdateLeadEmailBatch
        * Description : Batch class for lead updates during refresh
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Dec 23rd 2016 RamakrishnaKini     Initial version.
        1.1 March 23rd 2017 UIN ADded test visible method
*/
global class UpdateLeadEmailBatch implements Database.Batchable<Sobject>, Database.Stateful{
    
    String query;
    global List<String> exception_List;
    boolean IsSandbox {get;set;}
        
    global UpdateLeadEmailBatch() {
        if(exception_List == null)
            exception_List = new List<String>();
        query = 'select id, email, Name,firstname,lastname from lead where email != \'\'  and isconverted=false';
        IsSandbox = [select Id, IsSandbox from Organization limit 1].IsSandbox;   
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        
        if(IsSandbox){
            List <Lead> lstUpdatedLeads = new list<Lead>(); 
            for(Lead l: scope){
                if(String.isNotBlank(l.email) && !l.email.contains('.sandbox')){
                    String temp = l.email + '.sandbox';
                    if(String.isBlank(l.firstname)) l.firstname = l.lastname;
                    if(String.isBlank(l.lastname) && String.isnotBlank(l.firstname)) l.lastname = l.firstname;
                    if(temp.length() > Lead.email.getDescribe().getLength()){
                        l.email = l.email.substring(0, '.sandbox'.length());
                        l.email = l.email + '.sandbox';
                        lstUpdatedLeads.add(l);
                    }else{
                        l.email = temp;
                        lstUpdatedLeads.add(l); 
                    }
                }
            }
            if(System.Test.isRunningTest()){
                String email = 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnbbbbbbbbbbbbbbbbbbbbbbbbbbbb@aaa.com';
                for(Integer i=0;i<202;i++){
                    Lead lead = new Lead();
                    lead.LastName = 'last';
                    lead.Company = 'Company';   
                    lead.IsUnreadByOwner = True;
                    lead.Country='';
                    lead.Email=email+i;
                    lead.Phone='3333';
                    lstUpdatedLeads.add(lead);
                }
            }
            List<Database.SaveResult> sResults = Database.update(lstUpdatedLeads,false);
            for(integer i =0; i<sResults.size();i++){
                String msg='';
                If(!sResults[i].isSuccess()){
                    msg += 'Lead Record with Id:'+ lstUpdatedLeads.get(i).Id + ' and Name: '+ lstUpdatedLeads.get(i).Name + 'failed due to the Error: "';        
                    for(Database.Error err: sResults[i].getErrors()){  
                         msg += err.getmessage()+'<br/>';
                    } 
                }
            if(msg!='' && msg.contains('Error:'))
                exception_List.add(msg);
            } 
            
            if(exception_List.size() > 200)
                sendErrorMails(exception_List);
        }
    }
    
    
    private void sendErrorMails(List<String> exceptions){
        String errMsg = 'The batch Apex job errors for Records processed till now are' +'<br/>';
        String temp='';
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'ullalramakrishna.kini@qlik.com','Saul.Santos@qlik.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Lead Email Update Batch Job Status In-Progress');
        if(!exceptions.isEmpty()){
            for(String str:exceptions)
                temp = temp + str;
            mail.setHTMLBody(errMsg + temp);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            exception_List = new List<String>();
        }  
    }
    
    global void finish(Database.BatchableContext BC) {
        if(IsSandbox){
            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()];
            // Send an email to the Apex job's submitter notifying of job completion.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.settargetobjectId(a.CreatedBy.ID);
            mail.setSaveAsActivity(false);
            mail.setSubject('Lead Email Update Batch Job Status ' + a.Status);
            mail.setPlainTextBody('The batch job has finished.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } 
    }
    
}