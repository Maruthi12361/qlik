/**************************************************
* Unit test to test OppGIAcceptanceCtrl trigger
*
* Change Log:
* 2015-07-07 CCE: CR# 7333 Initial Development
* 2015-10-26 CCE  Updated api version to 31, removed seeAllData, - for Winter 16 SF upgrade       
* 2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring    
**************************************************/
@isTest
private class OppGIAcceptanceCtrlTest {

    static testMethod void ChangingStageNameValue_Test() {

        QTTestUtils.GlobalSetUp();  //setup Custom settings
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('OppGIAcceptanceCtrlTest: ChangingStageNameValue_Test: act = ' + act.Id);

            //Create test Opportunity
            Opportunity testOpp = CreateOpp(act.Id);
            

            Opportunity retOpp = refreshOpp(testOpp.Id);
            System.debug('OppGIAcceptanceCtrlTest: ChangingStageNameValue_Test: retOpp = ' + retOpp);
            System.assertEquals(null, retOpp.GI_Acceptance_Start_Date__c);
			Semaphores.OpportunityTriggerBeforeUpdate = false;
            testOpp.StageName = 'Goal Identified';
            update testOpp;

            retOpp = refreshOpp(testOpp.Id);
            System.debug('OppGIAcceptanceCtrlTest: ChangingStageNameValue_Test: retOpp = ' + retOpp);
            System.assertNotEquals(null, retOpp.GI_Acceptance_Start_Date__c);

            test.startTest();
			Semaphores.OpportunityTriggerBeforeUpdate = false;
            testOpp.StageName = 'Goal Confirmed';
            testOpp.Included_Products__c = 'Qlik Sense';
            update testOpp;

            retOpp = refreshOpp(testOpp.Id);
            System.debug('OppGIAcceptanceCtrlTest: ChangingStageNameValue_Test: retOpp = ' + retOpp);
            System.assertEquals('GI Accepted', retOpp.GI_Acceptance_Status__c);

            test.stopTest();

        }

    }

    static testMethod void ChangingGIAcceptanceStatusValue_GIRejected_Test() {

        QTTestUtils.GlobalSetUp();  //setup Custom settings
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('OppGIAcceptanceCtrlTest: ChangingGIAcceptanceStatusValue_GIRejected_Test: act = ' + act.Id);
         
            //Create test Opportunity
            Opportunity testOpp = CreateOpp(act.Id);

            Opportunity retOpp = refreshOpp(testOpp.Id);
            System.debug('OppGIAcceptanceCtrlTest: ChangingGIAcceptanceStatusValue_GIRejected_Test: retOpp = ' + retOpp);
            System.assertEquals(null, retOpp.GI_Acceptance_Start_Date__c);
			Semaphores.OpportunityTriggerBeforeUpdate = false;
            testOpp.StageName = 'Goal Identified';
            update testOpp;
			
            retOpp = refreshOpp(testOpp.Id);
            System.debug('OppGIAcceptanceCtrlTest: ChangingGIAcceptanceStatusValue_GIRejected_Test: retOpp = ' + retOpp);
            System.assertNotEquals(null, retOpp.GI_Acceptance_Start_Date__c);
            System.assertEquals(null, retOpp.GI_Acceptance_Stop_Date__c);

            test.startTest();
			Semaphores.OpportunityTriggerBeforeUpdate = false;
            testOpp.GI_Acceptance_Status__c = 'GI Rejected';
            testOpp.GI_Rejected_Reason__c = 'Created in Error';
            update testOpp;

            retOpp = refreshOpp(testOpp.Id);
            System.debug('OppGIAcceptanceCtrlTest: ChangingGIAcceptanceStatusValue_GIRejected_Test: retOpp = ' + retOpp);
            System.assertNotEquals(null, retOpp.GI_Acceptance_Stop_Date__c);

            test.stopTest();
        }

    }

    private static Opportunity refreshOpp (Id id) {
        return [SELECT Id, GI_Acceptance_Status__c, GI_Acceptance_Start_Date__c, GI_Acceptance_Stop_Date__c, GI_Qualification_Start_Date__c, GI_Qualification_Stop_Date__c  FROM Opportunity WHERE Id = :id];
    }

    private static Opportunity CreateOpp (Id AccntId) {
        //Create test Opportunity
        Opportunity testOpp = New Opportunity (
            Short_Description__c = 'TestOpp - delete me',
            Name = 'TestOpp - Delete me',
            Type = 'New Customer',
            Revenue_Type__c = 'Direct',
            CloseDate = Date.today().addDays(2),
            StageName = 'Goal Discovery',
            CurrencyIsoCode = 'GBP',
            Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
            AccountId = AccntId,
            function__c = 'Other',  //For OP006
            solution_area__c = 'Other',  //For OP006
            Signature_Type__c = 'Digital Signature',
            Data_source_systems__c = 'Other'  //For OP006
        );
        insert testOpp;
        System.debug('OppGIAcceptanceCtrlTest: CreateOpp = ' + testOpp.Id);
        return testOpp;
    }
}