/*
 *  OpporunityOverviewControllerTest
 *
 *	Test class for OpporunityOverviewController
 *
 *	Changelog:
 *		2016-01-12	AIN		Test class split from OpporunityOverviewController, fixed issue with user that left the company, replaced with mock
        2016-06-08    Andrew Lokotosh Commented Forecast_Amount fields line  671
		24.03.2017 Rodion Vakulovskyi  
		29.03.2017 Rodion Vakulvoskyi fix test failures commenting additional  Qttestutils.GlobalSetUp(); 
 */

@istest
public class OpporunityOverviewControllerTest {
	public static String oppRtypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
	public static TestMethod void Test_getSalesView()
	{
		Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
		Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
    	Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
    	Semaphores.Opportunity_ManageForecastProductsIsInsert = true;  
    	Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;

		Qttestutils.GlobalSetUp();
		Qttestutils.SetupNSPriceBook('USD');

		List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: QlikBuy Sales Manager' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	Profile profile = plist[0];

		List<User> users = new List<User>();
		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));

		insert users;
		users[1].ManagerId = users[0].Id;
		users[2].ManagerId = users[0].Id;
		update users;

		Account acc1 = Qttestutils.createMockAccount('RDZTestAcc', users[1]);
		Account acc2 = Qttestutils.createMockAccount('RDZTestAcc', users[2]);

		List<Opportunity> oppsForUser2 = new List<Opportunity>();
		
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[2], acc2));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[2], acc2));
		System.runAs(users[2]) {
			insert oppsForUser2;
		}
		oppsForUser2[0].StageName = 'Goal Identified';
		oppsForUser2[1].StageName = 'Goal Identified';
		//Uncomment after spring 16
		//Test.setCreatedDate(oppsForUser2[0].Id, DateTime.now() - 11);
		//Test.setCreatedDate(oppsForUser2[1].Id, DateTime.now() - 11);
		update oppsForUser2;

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		/*Opportunity opp = createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', '012D0000000KEKO', 'USD', users[1], acc1);
		System.runAs(users[1]) {
			insert opp;
		}

		//Uncomment after spring 16
		//Test.setCreatedDate(opp.Id, DateTime.now() - 11);
		//Stage is updated after as it's not the supplied value
		opp.StageName = 'Goal Identified';
		update opp;*/

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		

		System.debug('Limits.getQueries(): ' + Limits.getQueries());
		
		test.startTest();

		List<Opportunity> opps = [select CreatedDate, Owner.Profile.Name, Owner.Id, StageName from Opportunity];

		for(Opportunity oppRec : opps)
		{
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Owner.Id: ' + oppRec.Owner.Id);
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Id: ' + oppRec.Id);
			System.debug('opp.CreatedDate: ' + oppRec.CreatedDate);
			System.debug('opp.StageName: ' + oppRec.StageName);
		}

		System.debug('opps.Size: ' + opps.Size());
		
	
		OpporunityOverviewController ThisTest = new OpporunityOverviewController();
		ThisTest.myLimit = 150;
		
		System.RunAs(users[0]) {    
			List<OpporunityOverviewController.SalesViewObject> testSalesView = ThisTest.getSalesView();
			System.assert(null != testSalesView);	
		}
		test.stopTest();
	}
		
	public static TestMethod void Test_getMarketingView()
	{
		Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
		Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
    	Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
    	Semaphores.Opportunity_ManageForecastProductsIsInsert = true;  
    	Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;

		Qttestutils.GlobalSetUp();
		Qttestutils.SetupNSPriceBook('USD');

		List<User> users = new List<User>();

		//Opp creators
		List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: Marketing Director' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	Profile profile = plist[0];

    	users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));

    	//Opp owners
    	plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: QlikBuy Sales Manager' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	profile = plist[0];

		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));

		
		users[1].ManagerId = users[0].Id;
		users[2].ManagerId = users[0].Id;
		users[3].ManagerId = users[0].Id;
		users[4].ManagerId = users[0].Id;
		insert users;
		//update users;


		Account acc1 = Qttestutils.createMockAccount('RDZTestAcc', users[1]);
		Account acc2 = Qttestutils.createMockAccount('RDZTestAcc', users[2]);

		List<Opportunity> oppsForUser2 = new List<Opportunity>();
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[2], acc2));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[2], acc2));
		System.runAs(users[2]) {
			insert oppsForUser2;
		}
		oppsForUser2[0].StageName = 'Goal Identified';
		oppsForUser2[1].StageName = 'Goal Identified';
		oppsForUser2[0].OwnerId = users[4].Id;
		oppsForUser2[1].OwnerId = users[4].Id;
		//Uncomment after spring 16
		//Test.setCreatedDate(oppsForUser2[0].Id, DateTime.now() - 11);
		//Test.setCreatedDate(oppsForUser2[1].Id, DateTime.now() - 11);
		update oppsForUser2;

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		/*Opportunity opp = createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', '012D0000000KEKO', 'USD', users[1], acc1);
		System.runAs(users[1]) {
			insert opp;
		}

		//Uncomment after spring 16
		Test.setCreatedDate(opp.Id, DateTime.now() - 11);
		//Stage is updated after as it's not the supplied value
		opp.StageName = 'Goal Identified';
		opp.OwnerId = users[3].Id;
		update opp;*/

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		List<Opportunity> opps = [select CreatedBy.Profile.Name, CreatedBy.Id, CreatedDate, Owner.Profile.Name, Owner.Id, StageName from Opportunity];

		for(Opportunity oppRec : opps)
		{
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Owner.Id: ' + oppRec.Owner.Id);
			System.debug('opp.Id: ' + oppRec.Id);
			System.debug('opp.CreatedDate: ' + oppRec.CreatedDate);
			System.debug('opp.CreatedBy.Profile.Name: ' + oppRec.CreatedBy.Profile.Name);
			System.debug('opp.CreatedBy.Id: ' + oppRec.CreatedBy.Id);
			System.debug('opp.StageName: ' + oppRec.StageName);
		}
		System.debug('opps.Size: ' + opps.Size());

		test.startTest();

		OpporunityOverviewController ThisTest = new OpporunityOverviewController();
		
		System.RunAs(users[0]) {    
			List<OpporunityOverviewController.MarketingViewObject> testMarketingView = ThisTest.getMarketingView();
			System.assert(null != testMarketingView);
		}
		test.stopTest();
	}
	
	public static TestMethod void Test_getSummaryView1()
	{
		//Test for IMV = 0, IMUV = 0
		Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
		Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
    	Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
    	Semaphores.Opportunity_ManageForecastProductsIsInsert = true;  
    	Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;

		Qttestutils.GlobalSetUp();
		Qttestutils.SetupNSPriceBook('USD');

		List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: QlikBuy Sales Manager' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	Profile profile = plist[0];

		List<User> users = new List<User>();
		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));

		insert users;
		users[1].ManagerId = users[0].Id;
		update users;

		Account acc = Qttestutils.createMockAccount('RDZTestAcc', users[1]);

		List<Opportunity> oppsForUser2 = new List<Opportunity>();
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[1], acc));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[1], acc));
		System.runAs(users[1]) {
			insert oppsForUser2;
		}
		oppsForUser2[0].StageName = 'Goal Identified';
		oppsForUser2[1].StageName = 'Goal Identified';
		//Uncomment after spring 16
		//Test.setCreatedDate(oppsForUser2[0].Id, DateTime.now() - 11);
		//Test.setCreatedDate(oppsForUser2[1].Id, DateTime.now() - 11);
		update oppsForUser2;

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		test.startTest();

		List<Opportunity> opps = [select CreatedDate, Owner.Profile.Name, Owner.Id, StageName from Opportunity];

		for(Opportunity oppRec : opps) {
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Owner.Id: ' + oppRec.Owner.Id);
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Id: ' + oppRec.Id);
			System.debug('opp.CreatedDate: ' + oppRec.CreatedDate);
			System.debug('opp.StageName: ' + oppRec.StageName);
		}

		System.debug('opps.Size: ' + opps.Size());

		OpporunityOverviewController ThisTest = new OpporunityOverviewController();
		
		ThisTest.myLimit = 150;
		
		System.RunAs(users[1]) {    
			System.currentPagereference().getParameters().put('Uid', users[1].Id);
			System.currentPagereference().getParameters().put('LT', '10');				
			System.currentPagereference().getParameters().put('IMV', '0'); //Not manager view
			System.currentPagereference().getParameters().put('IMUV', '0');
			System.currentPagereference().getParameters().put('C', '0');
			List<OpporunityOverviewController.SummaryViewObject> testSummaryView = ThisTest.getSummaryView();		
			System.assert(null != testSummaryView);
		}
		test.stopTest();
	}
	
	public static TestMethod void Test_getSummaryView2()
	{
		//Test for IMV = 0, IMUV = 1
		Qttestutils.GlobalSetUp();

		Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
		Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
    	Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
    	Semaphores.Opportunity_ManageForecastProductsIsInsert = true;  
    	Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;

		//Qttestutils.GlobalSetUp();
		Qttestutils.SetupNSPriceBook('USD');

		List<User> users = new List<User>();

		//Opp creators
		List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: Marketing Director' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	Profile profile = plist[0];

    	users.Add(createMockUser(profile.Id)); //Manager
    	users.Add(createMockUser(profile.Id)); //Creator

    	//Opp owners
    	plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: QlikBuy Sales Manager' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	profile = plist[0];

		users.Add(createMockUser(profile.Id)); //Owner
		users.Add(createMockUser(profile.Id)); //Owner

		insert users;
		users[1].ManagerId = users[0].Id;
		users[2].ManagerId = users[0].Id;
		users[3].ManagerId = users[0].Id;
		update users;

		Account acc = Qttestutils.createMockAccount('RDZTestAcc', users[1]);

		List<Opportunity> oppsForUser2 = new List<Opportunity>();
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[1], acc));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[1], acc));
		System.runAs(users[1]) {
			insert oppsForUser2;
		}
		oppsForUser2[0].StageName = 'Goal Identified';
		oppsForUser2[1].StageName = 'Goal Identified';
		//Uncomment after spring 16
		//Test.setCreatedDate(oppsForUser2[0].Id, DateTime.now() - 11);
		//Test.setCreatedDate(oppsForUser2[1].Id, DateTime.now() - 11);
		oppsForUser2[0].OwnerId = users[2].Id;
		oppsForUser2[1].OwnerId = users[3].Id;
		update oppsForUser2;

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		test.startTest();

		List<Opportunity> opps = [select CreatedDate, Owner.Profile.Name, Owner.Id, StageName from Opportunity];

		for(Opportunity oppRec : opps) {
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Owner.Id: ' + oppRec.Owner.Id);
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Id: ' + oppRec.Id);
			System.debug('opp.CreatedDate: ' + oppRec.CreatedDate);
			System.debug('opp.StageName: ' + oppRec.StageName);
		}

		System.debug('opps.Size: ' + opps.Size());

		OpporunityOverviewController ThisTest = new OpporunityOverviewController();
		
		ThisTest.myLimit = 150;
		
		System.RunAs(users[1]) {    
			System.currentPagereference().getParameters().put('Uid', users[1].Id);
			System.currentPagereference().getParameters().put('LT', '10');				
			System.currentPagereference().getParameters().put('IMV', '0');
			System.currentPagereference().getParameters().put('IMUV', '1');
			System.currentPagereference().getParameters().put('C', '0');
			List<OpporunityOverviewController.SummaryViewObject> testSummaryView = ThisTest.getSummaryView();		
			System.assert(null != testSummaryView);
		}
		test.stopTest();
	}	
	
	public static TestMethod void Test_getSummaryView3()
	{
		//Test for IMV = 1, C = 0
		Qttestutils.GlobalSetUp();

		Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
		Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
    	Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
    	Semaphores.Opportunity_ManageForecastProductsIsInsert = true;  
    	Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;

		//Qttestutils.GlobalSetUp();
		Qttestutils.SetupNSPriceBook('USD');

		List<User> users = new List<User>();

		//Opp creators
		List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: Marketing Director' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	Profile profile = plist[0];

    	users.Add(createMockUser(profile.Id));

    	//Opp owners
    	plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: QlikBuy Sales Manager' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	profile = plist[0];

		users.Add(createMockUser(profile.Id));

		insert users;
		users[1].ManagerId = users[0].Id;
		update users;

		Account acc = Qttestutils.createMockAccount('RDZTestAcc', users[1]);

		List<Opportunity> oppsForUser2 = new List<Opportunity>();
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[1], acc));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[1], acc));
		System.runAs(users[1]) {
			insert oppsForUser2;
		}
		oppsForUser2[0].StageName = 'Goal Identified';
		oppsForUser2[1].StageName = 'Goal Identified';
		//Uncomment after spring 16
		//Test.setCreatedDate(oppsForUser2[0].Id, DateTime.now() - 11);
		//Test.setCreatedDate(oppsForUser2[1].Id, DateTime.now() - 11);
		update oppsForUser2;

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		test.startTest();

		List<Opportunity> opps = [select CreatedDate, Owner.Profile.Name, Owner.Id, StageName from Opportunity];

		for(Opportunity oppRec : opps) {
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Owner.Id: ' + oppRec.Owner.Id);
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Id: ' + oppRec.Id);
			System.debug('opp.CreatedDate: ' + oppRec.CreatedDate);
			System.debug('opp.StageName: ' + oppRec.StageName);
		}

		System.debug('opps.Size: ' + opps.Size());
		
		OpporunityOverviewController ThisTest = new OpporunityOverviewController();
		
		ThisTest.myLimit = 150;
		
		System.RunAs(users[1]) {    
			System.currentPagereference().getParameters().put('Uid', users[1].Id);
			System.currentPagereference().getParameters().put('LT', '0');				
			System.currentPagereference().getParameters().put('IMV', '1');
			System.currentPagereference().getParameters().put('IMUV', '0');
			System.currentPagereference().getParameters().put('C', '0');
			List<OpporunityOverviewController.SummaryViewObject> testSummaryView = ThisTest.getSummaryView();		
			System.assert(null != testSummaryView);
		}
		test.stopTest();
	}	
	
	public static TestMethod void Test_getSummaryView4()
	{
		//Test for IMV = 1, C = 1
		Qttestutils.GlobalSetUp();

		Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
		Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
    	Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
    	Semaphores.Opportunity_ManageForecastProductsIsInsert = true;  
    	Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;

		//Qttestutils.GlobalSetUp();
		Qttestutils.SetupNSPriceBook('USD');

		List<User> users = new List<User>();

		//Opp creators
		List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: Marketing Director' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	Profile profile = plist[0];

    	users.Add(createMockUser(profile.Id));

    	//Opp owners
    	plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: QlikBuy Sales Manager' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	profile = plist[0];

		users.Add(createMockUser(profile.Id));

		insert users;
		users[1].ManagerId = users[0].Id;
		//users[2].ManagerId = users[0].Id;
		update users;

		Account acc = Qttestutils.createMockAccount('RDZTestAcc', users[1]);

		List<Opportunity> oppsForUser2 = new List<Opportunity>();
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Confirmed', oppRtypeId, 'USD', users[1], acc));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Confirmed', oppRtypeId, 'USD', users[1], acc));
		System.runAs(users[1]) {
			insert oppsForUser2;
		}
		oppsForUser2[0].StageName = 'Goal Confirmed';
		oppsForUser2[1].StageName = 'Goal Confirmed';
		//Uncomment after spring 16
		//Test.setCreatedDate(oppsForUser2[0].Id, DateTime.now() - 11);
		//Test.setCreatedDate(oppsForUser2[1].Id, DateTime.now() - 11);
		update oppsForUser2;

		System.debug('Limits.getQueries(): ' + Limits.getQueries());

		test.startTest();

		List<Opportunity> opps = [select CreatedDate, Owner.Profile.Name, Owner.Id, StageName from Opportunity];

		for(Opportunity oppRec : opps) {
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Owner.Id: ' + oppRec.Owner.Id);
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Id: ' + oppRec.Id);
			System.debug('opp.CreatedDate: ' + oppRec.CreatedDate);
			System.debug('opp.StageName: ' + oppRec.StageName);
		}

		System.debug('opps.Size: ' + opps.Size());
		
		OpporunityOverviewController ThisTest = new OpporunityOverviewController();
		
		ThisTest.myLimit = 150;
		
		System.RunAs(users[1]) {    
			System.currentPagereference().getParameters().put('Uid', users[1].Id);
			System.currentPagereference().getParameters().put('LT', '0');				
			System.currentPagereference().getParameters().put('IMV', '1');
			System.currentPagereference().getParameters().put('IMUV', '0');
			System.currentPagereference().getParameters().put('C', '1');
			List<OpporunityOverviewController.SummaryViewObject> testSummaryView = ThisTest.getSummaryView();		
			System.assert(null != testSummaryView);
		}
		test.stopTest();
	}	
	
	public static TestMethod void Test_getManagerView()
	{
		//User user = [select id from User where alias='lby'];

		Semaphores.Opportunity_Legal_CompanyCheckHasRun = true;
		Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
    	Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
    	Semaphores.Opportunity_ManageForecastProductsIsInsert = true;  
    	Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;

		Qttestutils.GlobalSetUp();
		Qttestutils.SetupNSPriceBook('USD');

		List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Custom: QlikBuy Sales Manager' LIMIT 1];
    	if(plist.size()< 0)	
    		system.assert(false, 'Profile missing');
    	Profile profile = plist[0];

		List<User> users = new List<User>();
		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));
		users.Add(createMockUser(profile.Id));

		insert users;
		users[1].ManagerId = users[0].Id;
		users[2].ManagerId = users[0].Id;
		users[3].ManagerId = users[0].Id;
		update users;

		Account acc1 = Qttestutils.createMockAccount('RDZTestAcc', users[1]);
		Account acc2 = Qttestutils.createMockAccount('RDZTestAcc', users[2]);

		/*Opportunity opp = createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Confirmed', '012D0000000KEKO', 'USD', users[1], acc1);
		System.runAs(users[1]) {
			insert opp;
		}*/
		//Uncomment after spring 16
		//Test.setCreatedDate(opp.Id, DateTime.now() - 40);
		//Stage is updated after as it's not the supplied value
		//opp.StageName = 'Goal Confirmed';
		//update opp;

		List<Opportunity> oppsForUser2 = new List<Opportunity>();
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Identified', oppRtypeId, 'USD', users[2], acc2));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Confirmed', oppRtypeId, 'USD', users[2], acc2));
		oppsForUser2.add(createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct QSG', 'Goal Confirmed', oppRtypeId, 'USD', users[2], acc2));
		System.runAs(users[2]) {
			insert oppsForUser2;
		}
		oppsForUser2[0].StageName = 'Goal Identified';
		oppsForUser2[1].StageName = 'Goal Confirmed';
		oppsForUser2[2].StageName = 'Goal Confirmed';
		//Uncomment after spring 16
		//Test.setCreatedDate(oppsForUser2[0].Id, DateTime.now() - 11);
		//Test.setCreatedDate(oppsForUser2[1].Id, DateTime.now() - 40);
		//Test.setCreatedDate(oppsForUser2[2].Id, DateTime.now() - 40);

		update oppsForUser2;

		System.debug('Limits.getQueries(): ' + Limits.getQueries());
		
		test.startTest();

		List<Opportunity> opps = [select CreatedDate, Owner.Profile.Name, Owner.Id, StageName from Opportunity];

		for(Opportunity oppRec : opps) {
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Owner.Id: ' + oppRec.Owner.Id);
			System.debug('opp.Owner.Profile.Name: ' + oppRec.Owner.Profile.Name);
			System.debug('opp.Id: ' + oppRec.Id);
			System.debug('opp.CreatedDate: ' + oppRec.CreatedDate);
			System.debug('opp.StageName: ' + oppRec.StageName);
		}
		
		OpporunityOverviewController ThisTest = new OpporunityOverviewController();
		ThisTest.myLimit = 150;
		System.RunAs(users[0]) {    
			List<OpporunityOverviewController.ManagerViewObject> testMarketingView = ThisTest.getManagerView();
			System.assert(null != testMarketingView);
		}
		test.stopTest();
	}
	public static TestMethod void TestCoverage()
	{
		OpporunityOverviewController.SummaryViewObject summaryViewObject = new OpporunityOverviewController.SummaryViewObject();
		summaryViewObject.Name = 'test';
		summaryViewObject.OppName = 'test';
		summaryViewObject.OppId = 'test';
		summaryViewObject.CreatedDate = Date.Today();
		summaryViewObject.Age = 1;
		summaryViewObject.CreatedBy = 'test';

		OpporunityOverviewController.MarketingViewObject marketingViewObject = new OpporunityOverviewController.MarketingViewObject();
		marketingViewObject.Name = 'test';
		marketingViewObject.Id = null;
		marketingViewObject.Cnt10 = 0;
		marketingViewObject.Cnt7 = 0;
		marketingViewObject.Cnt = 0;

		OpporunityOverviewController.ManagerViewObject managerViewObject = new OpporunityOverviewController.ManagerViewObject();
		managerViewObject.Name = 'test';
		managerViewObject.Id = 'test';
		managerViewObject.Cnt10 = 0;
		managerViewObject.CntConfirmed21 = 0;

		OpporunityOverviewController.SalesViewObject salesViewObject = new OpporunityOverviewController.SalesViewObject();
		salesViewObject.Name = 'test';
		salesViewObject.Id = null;
		salesViewObject.Cnt = 0;

	}
	private static Integer NumCreated = 0;
	static User createMockUser(Id profileId) {
    	NumCreated++;
    	User u = new User(alias = 'newUser', email='newuser@rdztest.com.test',
    	Emailencodingkey='UTF-8', lastname='Testing' + NumCreated, 
    	Languagelocalekey='en_US', localesidkey='en_US',
    	Profileid = profileId,
    	Timezonesidkey='America/Los_Angeles', 
    	Username= System.now().millisecond() + '_' + NumCreated +'_newuser@qlikview.com.test');
    	
	   	return  u; 
  	}
  	public static Opportunity createMockOpportunity(String sShortDescription, String sName, String sType, String sRevenueType, String sStage, String sRecordTypeId, String sCurrencyIsoCode, User user, Account acc)
  	{          
    	Opportunity opp = New Opportunity (
            Short_Description__c = sShortDescription,
            Name = sName,
            Type = sType,
            Revenue_Type__c = sRevenueType,
            CloseDate = Date.today().addDays(5),//Adding closing date as today+ 5 to ensure test classes do not fail on overnight release
            StageName = sStage, 
            RecordTypeId = sRecordTypeId, 
            CurrencyIsoCode = sCurrencyIsoCode,
            AccountId = acc.Id,
            Customers_Business_Pain__c = 'First project scheduled',
         //   Consultancy_Forecast_Amount__c = 1,
		    Function__c = 'Communications',
			Solution_Area__c ='Utilities - Smart Metering',
            Signature_Type__c = 'Digital Signature',
			Included_Products__c = 'Qlik Sense'
            ); 
              
    	return opp;
  	}
}