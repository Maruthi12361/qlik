/*
----------------------------------------------------------------------------
|  Class: BugWeightSys_Scheduler
|
|  Filename: BugWeightSys_Scheduler.cls
|
|  Author: Peter Friberg, Fluido Sweden AB
|
|  Description:
|    Class implementing the Schedulable class.
|    This class creates the query that will find all open Bugs.
|    It then starts a batch job that will calculate the weight of the Bugs.
|
| Change Log:
| 2013-10-25  PetFri  Initial Development
| 2013-10-28  PetFri  Updates each Bug with number of cases
| 2013-10-29  PetFri  Uses active Entitlements to find Account License
| 2013-11-07  PetFri  Uses active Entitlement/SLA Process to find SLA level
| 2013-11-21  PetFri  Made CronExp and query global and static for use in test
|                     New algorithmn that creates a every 3rd hour schedule
|                     upon first execution 
----------------------------------------------------------------------------
*/

global class BugWeightSys_Scheduler implements Schedulable {
    
    // Every 3rd hour, for ever
    public static String CRON_EXP = '0 0 0/3 * * ?';

    // Create the Bugs query, that is find all Bugs not closed
    public static String query =
        'SELECT id,' +
        '       name,' +
        '       severity__c,' +
        '       status__c,' +
        '       bug_weight__c,' +
        '       number_of_cases__c,' +
        '       createddate,' +
        '    (SELECT id,' +
        '            priority,' +
        '            severity__c,' +
        '            casenumber,' +
        '            designated_support_engineer__c,' +
        '            entitlementid,' +
        '            entitlement.slaprocess.name,' +
        '            entitlement.account_license__c,' +
        '            entitlement.account_license__r.name,' +
        '            entitlement.status,' +
        '            entitlement.session_cal__c,' +
        '            entitlement.named_user_cal__c,' +
        '            entitlement.document_cal__c,' +
        '            entitlement.usage_cal__c,' +
        '            entitlement.uncapped__c' +
        '     FROM Cases__r)' +
        'FROM Bugs__c WHERE NOT(status__c LIKE \'Close%\')';


    global void execute(SchedulableContext sc) {

        // Detect if this job was started by the single schedule
        // or own CRON entry
        Id ctId = sc.getTriggerId();
        System.debug('TRIGGER ID=' + ctId);

        CronTrigger ct = [SELECT id, CronExpression FROM CronTrigger WHERE id = :ctId];
        System.debug('CRONTRIGGER=' + ct);

        if (ct.CronExpression == CRON_EXP) {
            System.debug('=== OK ==> Running in 3 hour interval');

            // Create the batch job object that will execute in a scheduled
            // context, and invoking the BugWeightSys_Manager class.
            BugWeightSys_BatchJob batchApex = new BugWeightSys_BatchJob(query);           
            Id batchprocessid = Database.executeBatch(batchApex); 
        }
        else {
            System.debug('=== NULL ==> Create new 3 hour schedule');
            System.abortJob(ctId);
            Double rnd = Math.random();
            System.schedule('BugWeight Auto Calc ' + rnd, CRON_EXP, new BugWeightSys_Scheduler());
        }
    }
}