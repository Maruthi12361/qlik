/**********************
*
* 2015-10-30 AIN Callout moved from QVM_ScheduleLogoBatch to the start method of QVM_LogoBatch to avoid callout 
* in schedulable apex
* 07.02.2017   RVA :   changing methods 
*  09.03.2017 : Bala : fix for query error
'  2019-03-05  AIN Moved custom settings to QuoteTestHelper
*********************/
@isTest
private class QVM_ScheduleLogoBatchTest {
	
	static testmethod void testSchedule() {
    	
        QTTestUtils.GlobalSetUp();

        Test.startTest();

        String jobId = system.schedule('QVM_ScheduleLogoBatch', '0 0 0 * * ?', new QVM_ScheduleLogoBatch());
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered,NextFireTime from CronTrigger WHERE id = :jobId];
        system.assertEquals('0 0 0 * * ?', ct.CronExpression);
        system.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
    static testmethod void testBatch()
    {
        QTTestUtils.GlobalSetUp();

		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = testSubs1.id           
        );
        insert QTComp;
        */
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc', 'USA', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        RecordType PARecordType = [select Id, Name from RecordType 
								where Name = 'Partner Account' and sobjecttype='Account'];// fix for query error

        system.debug('PARecordType.Name: ' + PARecordType.Name);

        Account acc = new Account(Name='TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId =PARecordType.Id);
        insert acc;

        acc = [select id, name, recordType.name from account];

        Attachment attach=new Attachment();
        attach.Name='logo_Test';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=acc.id;
        insert attach;

        List<Account> accounts=[Select Id, (Select Id,Name from Attachments where Name like 'logo_%') from Account where RecordType.Name = 'Partner Account'];
        System.assertEquals(1, accounts.size());

        QVM_LogoBatch batch = new QVM_LogoBatch();

        Test.startTest();
        database.executebatch(batch);
        Test.stopTest();
    }

	
}