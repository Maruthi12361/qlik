/*******************************************************************
* Name: InitCustomMetadataType
* Description: InitCustomMetadataType is a class containing logic related with Custom Metadata Type
*
* Log History:
*
*  25.09.2018  ext_bjd	ITRM-225 Added methods which created necessary entities to fix tests related with Appirio logic.
*  24.01.2019  ext_bjd	ITRM-225 Added asynchronous invoke
********************************************************************/

public class InitCustomMetadataType {

    @Future
    public static void runInsertCustomMetadata(){
        List<appirio_core__Config_Group__c> groups = createAppirioCoreConfigurationGroup();
        List<appirio_core__Config_Option__c> options = createAppirioCoreConfigurationOption(groups);
        createAppirioCoreConfigurationValue(options);
        List<Appirio_core__Currency__c> currencies = createAppirioCoreCurrency();
        createAppirioCoreCurrencyExchangeRate(currencies);
    }

    private static List<appirio_core__Config_Group__c> createAppirioCoreConfigurationGroup() {
        Map<Id, Appirio_core_Configuration_Group__mdt> idToObject = new Map<Id, Appirio_core_Configuration_Group__mdt>([
                SELECT Id, Name__c,
                        CurrencyIsoCode__c, appirio_core_Description__c,
                        appirio_core_Key__c, appirio_core_Version__c
                FROM Appirio_core_Configuration_Group__mdt
        ]);

        List<appirio_core__Config_Group__c> configGroups = new List<appirio_core__Config_Group__c>();

        for (Appirio_core_Configuration_Group__mdt item : idToObject.values()) {
            appirio_core__Config_Group__c appirioCongigGroup = new appirio_core__Config_Group__c();
            appirioCongigGroup.Name = item.Name__c;
            appirioCongigGroup.CurrencyIsoCode = item.CurrencyIsoCode__c;
            appirioCongigGroup.appirio_core__Description__c = item.appirio_core_Description__c;
            appirioCongigGroup.appirio_core__Key__c = item.appirio_core_Key__c;
            appirioCongigGroup.appirio_core__Version__c = item.appirio_core_Version__c;
            configGroups.add(appirioCongigGroup);
        }

        if (!configGroups.isEmpty()) {
            insert configGroups;
        }
        return configGroups;
    }

    private static List<appirio_core__Config_Option__c> createAppirioCoreConfigurationOption(List<appirio_core__Config_Group__c> groups){
        Map<String, appirio_core__Config_Group__c> nameToObject = new Map<String, appirio_core__Config_Group__c>();
        for (appirio_core__Config_Group__c item : groups) {
            nameToObject.put(item.Name, item);
        }

        List<appirio_core__Config_Option__c> configOptions = new List<appirio_core__Config_Option__c>();

        Database.SaveResult[] srList = null;

        for (Appirio_core_Configuration_Option__mdt item : [
                SELECT Id, Name__c, appirio_core_Config_Group__c, appirio_core_Picklist_Values__c, appirio_core_Type__c,
                        appirio_core_Version__c, appirio_core_Description__c, CurrencyIsoCode__c,
                        appirio_core_Allow_Multiple_Values__c, appirio_core_Config_Group_Name__c
                FROM Appirio_core_Configuration_Option__mdt
        ]) {
            System.debug('!!! Name__c : ' + item.appirio_core_Config_Group_Name__c);
            if (nameToObject.containsKey(item.appirio_core_Config_Group_Name__c)) {
                appirio_core__Config_Option__c configOption = new appirio_core__Config_Option__c();
                configOption.appirio_core__Config_Group__c = nameToObject.get(item.appirio_core_Config_Group_Name__c).Id;
                configOption.Name = item.Name__c;
                configOption.appirio_core__Allow_Multiple_Values__c = item.appirio_core_Allow_Multiple_Values__c;
                configOption.appirio_core__Description__c = item.appirio_core_Description__c;
                configOption.appirio_core__Picklist_Values__c = item.appirio_core_Picklist_Values__c;
                configOption.appirio_core__Type__c = item.appirio_core_Type__c;
                configOption.appirio_core__Version__c = item.appirio_core_Version__c;
                configOption.CurrencyIsoCode = item.CurrencyIsoCode__c;
                configOptions.add(configOption);
            }
        }

        System.debug('!!! currency which should be created' + configOptions);

        if (!configOptions.isEmpty()) {
            srList = Database.insert(configOptions, false);
            if (!srList.isEmpty()) {
                for (Integer i = 0; i < srList.size(); i++) {
                    getDebugSaveResults(srList.get(i));
                }
            }
        }
        return configOptions;
    }

    private static void createAppirioCoreConfigurationValue(List<appirio_core__Config_Option__c> options){

        List<appirio_core__Config_Value__c> configValues = new List<appirio_core__Config_Value__c>();

        Database.SaveResult[] srList = null;

        for (appirio_core__Config_Option__c item : options) {
            System.debug('!!! Option Name : ' + item.Name);
            appirio_core__Config_Value__c configOValue = new appirio_core__Config_Value__c();
            if (item.Name.contains('Use_Dated_Exchange_Rates')) {
                configOValue.appirio_core__Config_Option__c = item.Id;
                configOValue.appirio_core__Value__c = 'true';
            } else {
                configOValue.appirio_core__Config_Option__c = item.Id;
                configOValue.appirio_core__Value__c = 'false';
            }
            configValues.add(configOValue);
        }

        System.debug('!!! currency which should be created' + configValues);

        if (!configValues.isEmpty()) {
            srList = Database.insert(configValues, false);
            if (!srList.isEmpty()) {
                for (Integer i = 0; i < srList.size(); i++) {
                    getDebugSaveResults(srList.get(i));
                }
            }
        }
    }

    private static List<Appirio_core__Currency__c> createAppirioCoreCurrency() {
        Map<Id, Appirio_core_Currency__mdt> currencyIdsToObject = new Map<Id, Appirio_core_Currency__mdt>();

        for (Appirio_core_Currency__mdt item : [
                SELECT Id, appirio_core_Currency_Code__c,
                        CurrencyIsoCode__c, Name__c,
                        appirio_core_Is_Corporate_Currency__c, Owner__c
                FROM Appirio_core_Currency__mdt
        ]) {
            currencyIdsToObject.put(item.Id, item);
            System.debug('!!! Currency ID is : ' + item.Id);
        }

        List<Appirio_core__Currency__c> currencies = new List<Appirio_core__Currency__c>();

        for (Appirio_core_Currency__mdt item : currencyIdsToObject.values()) {
            Appirio_core__Currency__c appirioCurrency = new Appirio_core__Currency__c();
            appirioCurrency.Name = item.Name__c;
            appirioCurrency.appirio_core_Currency_External_ID__c = item.Id;
            appirioCurrency.CurrencyIsoCode = item.CurrencyIsoCode__c;
            appirioCurrency.appirio_core__Currency_Code__c = item.appirio_core_Currency_Code__c;
            appirioCurrency.appirio_core__Is_Corporate_Currency__c = item.appirio_core_Is_Corporate_Currency__c;
            currencies.add(appirioCurrency);
        }

        if (!currencies.isEmpty()) {
            insert currencies;
        }
        return currencies;
    }

    private static void createAppirioCoreCurrencyExchangeRate(List<Appirio_core__Currency__c> currencies){
        Map<String, Appirio_core__Currency__c> externalIdsByCurrencies = new Map<String, Appirio_core__Currency__c>();

        for (Appirio_core__Currency__c item : currencies) {
            externalIdsByCurrencies.put(item.appirio_core_Currency_External_ID__c, item);
        }

        List<appirio_core__Currency_Exchange_Rate__c> currenciesExchangeRates = new List<appirio_core__Currency_Exchange_Rate__c>();

        Database.SaveResult[] srList = null;

        for (Appirio_core_Currency_Exchange_Rate__mdt item : [
                SELECT Id, Name__c, appirio_core_Effective_Date__c,
                        appirio_core_Currency__c, appirio_core_Rate__c, CurrencyIsoCode__c
                FROM Appirio_core_Currency_Exchange_Rate__mdt
        ]) {
            System.debug('!!! appirio_core_Currency__c : ' + item.appirio_core_Currency__c);
            if (externalIdsByCurrencies.containsKey(item.appirio_core_Currency__c)) {
                appirio_core__Currency_Exchange_Rate__c currencyExchangeRate = new appirio_core__Currency_Exchange_Rate__c();
                currencyExchangeRate.appirio_core__Currency__c = externalIdsByCurrencies.get(item.appirio_core_Currency__c).Id;
                currencyExchangeRate.CurrencyIsoCode = item.CurrencyIsoCode__c;
                currencyExchangeRate.appirio_core__Effective_Date__c = item.appirio_core_Effective_Date__c;
                currencyExchangeRate.appirio_core__Rate__c = item.appirio_core_Rate__c;
                currenciesExchangeRates.add(currencyExchangeRate);
            }
        }

        System.debug('!!! currency which should be created' + currenciesExchangeRates);

        if (!currenciesExchangeRates.isEmpty()) {
            srList = Database.insert(currenciesExchangeRates, false);
            if (!srList.isEmpty()) {
                for (Integer i = 0; i < srList.size(); i++) {
                    getDebugSaveResults(srList.get(i));
                }
            }
        }
    }

    private static void getDebugSaveResults(Database.SaveResult sr) {
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully created the record, record ID is : ' + sr.getId());
        } else if (!sr.isSuccess()) {
            // Operation failed, so get all errors
            for (Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Field that affected this error: ' + err.getFields());
            }
        }
    }
}
