/***************************************************
Class: UpdateSubscriptionTableOnLeadConvertTest
Object: Lead
Description: Test class for UPdateSubscriptionTableOnLeadConvert.trigger
Change Log:
 20151028    CCE Initial development
******************************************************/
@isTest
private class UpdateSubscriptionTableOnLeadConvertTest {

	static testMethod void UpdateSubscriptionTable() {
		string statusToUse = 'Lead - Converted';

        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');

        System.RunAs(mockSysAdmin) {
            //Create a test lead
            Lead l = new Lead(LastName='LastTest',
                                            FirstName='FirstTest',
                                            Country='United Kingdom',
                                            Email='asd@asd.com',
                                            Segment__c = 'Enterprise - Target',
                                            Country_Code__c = qtComp.Id,
                                            Company ='Testing');
            insert l;
            system.assert(l.Id != null);
            Semaphores.TriggerHasRun('UpdateSubscriptionTableOnLeadConvert', 1);

            //Create a Subscription record and add the lead to it
            Subscription__c subscriptionRecord = new Subscription__c(Lead__c = l.Id);
            insert subscriptionRecord;
            system.assert(subscriptionRecord.Id != null);

            Test.startTest();
            //Convert the lead
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
            Semaphores.LeadTriggerHandlerAfterUpdate = false;
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.Id);
            lc.setConvertedStatus('Lead - Converted');
            lc.setDoNotCreateOpportunity(true);


            Database.LeadConvertResult lcr = Database.convertLead(lc);
            system.assert(lcr.isSuccess());

            System.debug('UpdateSubscriptionTableOnLeadConvertTest: lcr.getContactId() = ' + lcr.getContactId());
            Id contactId = lcr.getContactId();

            //check that the Contact Id has been added to the Subscription record and the Lead Id has been removed
            subscriptionRecord = [Select Id, Lead__c, Contact__c from Subscription__c where Id = :subscriptionRecord.Id];
            System.debug('UpdateSubscriptionTableOnLeadConvertTest: subscriptionRecord = ' + subscriptionRecord);
            system.assertEquals(contactId, subscriptionRecord.Contact__c);
            system.assertEquals(null, subscriptionRecord.Lead__c);
            Test.stopTest();
        }
	}
}