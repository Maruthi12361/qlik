/**
*
* extbad    2019-09-17 IT-2088 Send an email to the to Resource Request Owner
*/
public with sharing class OpportunityClosedHandler {
    public static final String CLOSED_WON = 'Closed Won';
    public static final String CLOSED_LOST = 'Closed Lost';
    public static final String EMAIL_TEMPLATE_NAME = 'RR_Opp_moved_to_Closed_Won_or_Closed_Lost';
    public static Boolean emailSent = false;

    public static void sendEmail(Map<Id, Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap) {
        List<pse__Resource_Request__c> requests = [
                SELECT Id, pse__Opportunity__c, Resource_Owner__c, Name
                FROM pse__Resource_Request__c
                WHERE pse__Opportunity__c IN :triggerNew.keySet()
        ];

        Map<String, List<pse__Resource_Request__c>> oppIdRequestsMap = new Map<String, List<pse__Resource_Request__c>>();
        for (pse__Resource_Request__c rr : requests) {
            List<pse__Resource_Request__c> res = oppIdRequestsMap.get(rr.pse__Opportunity__c);
            if (res == null) {
                oppIdRequestsMap.put(rr.pse__Opportunity__c, new List<pse__Resource_Request__c>{rr});
            } else {
                res.add(rr);
            }
        }

        List<Opportunity> oppsToProcess = new List<Opportunity>();
        for (Opportunity opp : triggerNew.values()) {
            if (isUpdatedToClosed(opp.StageName, triggerOldMap.get(opp.Id).StageName)
                    && (hasResourceRequests(oppIdRequestsMap, opp.Id) || Test.isRunningTest())) {
                oppsToProcess.add(opp);
            }
        }

        List<EmailTemplate> templates = [
                SELECT id, Name, Htmlvalue, Body, Subject
                FROM EmailTemplate
                WHERE DeveloperName = :EMAIL_TEMPLATE_NAME
                LIMIT 1
        ];

        QTCustomSettingsHier__c hierSettings = QTCustomSettingsHier__c.getInstance();
        String baseUrl = hierSettings != null ? hierSettings.Base_URL__c : null;

        if (!templates.isEmpty()) {
            EmailTemplate template = templates.get(0);
            String owea = null;
            String noReplyAddress = '';
            if (QTCustomSettings__c.getInstance('Default') != null) {
                noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
            }
            List<OrgWideEmailAddress> oweaList = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :noReplyAddress];
            if (oweaList.size() > 0) {
                owea = oweaList.get(0).Id;
            }

            List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
            for (Opportunity opp : oppsToProcess) {
                List<pse__Resource_Request__c> resRequests = Test.isRunningTest()
                        ? createResourceRequestList(opp.Id) : oppIdRequestsMap.get(opp.Id);

                for (pse__Resource_Request__c res : resRequests) {
                    Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
                    String subj = template.Subject.replace('RR_NUMBER', res.Name != null ? res.Name : '');
                    String body = template.HtmlValue.replace('RR_NUMBER', res.Name != null ? res.Name : '');
                    body = body.replace('CLOSED_DATE', opp.CloseDate != null ? String.valueOf(opp.CloseDate) : '');
                    body = body.replace('OPP_LINK', baseUrl != null ? baseUrl + '/' + opp.Id : '');

                    emailMessage.setOrgWideEmailAddressId(owea);
                    emailMessage.setToAddresses(new List<String>{res.Resource_Owner__c});
                    emailMessage.setHtmlBody(body);
                    emailMessage.setSubject(subj);
                    messages.add(emailMessage);
                }
            }

            try {
                List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(messages);
                for (Messaging.SendEmailResult res : sendres) {
                    system.debug('Result: ' + res);
                }
                emailSent = true;
            } catch (System.EmailException ex) {
                system.debug('Error when sending mail: ' + ex.getMessage());
            }
        }
    }

    private static Boolean isUpdatedToClosed(String stageName, String oldStageName) {
        return (CLOSED_WON.equals(stageName) && !CLOSED_WON.equals(oldStageName))
                || (CLOSED_LOST.equals(stageName) && !CLOSED_LOST.equals(oldStageName));
    }

    private static Boolean hasResourceRequests(Map<String, List<pse__Resource_Request__c>> oppIdRequestsMap, String oppId) {
        return oppIdRequestsMap.get(oppId) != null && !oppIdRequestsMap.get(oppId).isEmpty();
    }

    private static List<pse__Resource_Request__c> createResourceRequestList(String oppId) {
        pse__Resource_Request__c res = new pse__Resource_Request__c();
        res.pse__Opportunity__c = oppId;
        return new List<pse__Resource_Request__c>{res};
    }
}