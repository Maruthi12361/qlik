/* @author: Andriy Lokotosh (4front)
    Created: 8.2016
    Description: Set OEM_Partner_Discount__c from OEM_Partner_Account__c. 
*/

public class OEMPartnerDiscountHandler
{
     public static void onBeforeInsert(List<Opportunity> inputList){
             System.debug('*OEMPartnerDiscountHandler.onBeforeInsert');
		PopulateOEMPartnerDiscount(inputList); 
    }
 
    public static void onBeforeUpdate(List<Opportunity> inputList) {
            System.debug('*OEMPartnerDiscountHandler.onBeforeUpdate');
       PopulateOEMPartnerDiscount(inputList);
    }
    
    private static void PopulateOEMPartnerDiscount(List<Opportunity> inputList){
        System.debug('PopulateOEMPartnerDiscount started...');
        Set<ID> accountIds = new Set<Id>();
        Map<ID,Account> idToAccount;
        
		for(Opportunity opp: inputList){
            System.debug('!!!Show me opp ' + opp);
            if(opp.OEM_Partner_Account__c != null) {
                accountIds.add(opp.OEM_Partner_Account__c);
            }
        }
        System.debug('!!!ListOEMAccs ' + accountIds);
        if(!accountIds.isEmpty()) {
            idToAccount = new Map<ID,Account>([Select id,Partner_Margin__c From Account where id in : accountIds]);
        }
        for(Opportunity opp: inputList){
            
            if(String.isNotBlank(opp.OEM_Partner_Account__c) && idToAccount.containsKey(opp.OEM_Partner_Account__c)){
                opp.OEM_Partner_Discount__c= idToAccount.get(opp.OEM_Partner_Account__c).Partner_Margin__c;
                System.debug('*OEMPartnerDiscountHandler.onBeforeUpdate: '+ opp.OEM_Partner_Account__r.Partner_Margin__c);
            }
            else{
                System.debug('*OEMPartnerDiscountHandler.onBeforeUpdate: -OEM Account is blank');
            }
        }
        System.debug('PopulateOEMPartnerDiscount finished...');
    }
    
}