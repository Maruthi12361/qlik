/*****************************************************
Class: UpdateHQLResponseTest
Object: CampaignMember
Description: CR# 84987 – Enable Tracking for HQL Campaigns

Change Log:
2017-04-18 CCE CR# 84987 - Initial development - Test class for UpdateHQLResponse
******************************************************/
@isTest
private class UpdateHQLResponseTest {
	static Campaign camp = new Campaign();        
    static Lead ld = new Lead();
    static Account acct = new Account();
    static Contact ct = new Contact();
    static User mockSysAdmin = new User();

    @isTest static void insertingCampaignMember() {
        
        mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {
            Setup();
            Test.startTest();
        
            CampaignMember cmLead = new CampaignMember(); // Add a lead campaign member
            cmLead.CampaignId = camp.Id;
            cmLead.Status = 'Responded';
            cmLead.LeadId = ld.Id;
            cmLead.High_Quality_Campaign_Response__c = true;
            insert cmLead;
            system.assert(cmLead.Id != null);
            ld = [SELECT Id, High_Quality_Campaign_Response__c FROM Lead WHERE Id = :ld.Id];
            system.assert(ld.High_Quality_Campaign_Response__c == true);

            CampaignMember cmCt = new CampaignMember(); // Add a contact campaign member
            cmCt.CampaignId = camp.Id;
            cmCt.Status = 'Responded';
            cmCt.ContactId = ct.Id;
            cmCt.High_Quality_Campaign_Response__c = true;
            insert cmCt;
            system.assert(cmCt.Id != null);
            ct = [SELECT Id, High_Quality_Campaign_Response__c FROM Contact WHERE Id = :ct.Id];
            system.assert(ct.High_Quality_Campaign_Response__c == true);
            Test.stopTest();
        }
    }

    @isTest static void updatingCampaignMemberToTrue() {
        
        mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {
            Setup();
            Test.startTest();
        
            // Add a lead campaign member
            CampaignMember cmLead = new CampaignMember();
            cmLead.CampaignId = camp.Id;
            cmLead.Status = 'Responded';
            cmLead.LeadId = ld.Id;
            insert cmLead;
            system.assert(cmLead.Id != null);
            
            // Add a contact campaign member
            CampaignMember cmCt = new CampaignMember();
            cmCt.CampaignId = camp.Id;
            cmCt.Status = 'Responded';
            cmCt.ContactId = ct.Id;
            insert cmCt;
            system.assert(cmCt.Id != null);
            
            // Update a lead campaign member
            cmLead.High_Quality_Campaign_Response__c = true;
            update cmLead;
            ld = [SELECT Id, High_Quality_Campaign_Response__c FROM Lead WHERE Id = :ld.Id];
            system.assert(ld.High_Quality_Campaign_Response__c == true);

            // Update a contact campaign member
            cmCt.High_Quality_Campaign_Response__c = true;
            update cmCt;
            ct = [SELECT Id, High_Quality_Campaign_Response__c FROM Contact WHERE Id = :ct.Id];
            system.assert(ct.High_Quality_Campaign_Response__c == true);

            Test.stopTest();
        }
    }

    @isTest static void updatingCampaignMemberToFalse() {
        
        mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {            
            Setup();            
            Test.startTest();
        
            // Add a lead campaign member
            CampaignMember cmLead = new CampaignMember();
            cmLead.CampaignId = camp.Id;
            cmLead.Status = 'Responded';
            cmLead.LeadId = ld.Id;
            cmLead.High_Quality_Campaign_Response__c = true;
            insert cmLead;
            system.assert(cmLead.Id != null);
            
            // Add a contact campaign member
            CampaignMember cmCt = new CampaignMember();
            cmCt.CampaignId = camp.Id;
            cmCt.Status = 'Responded';
            cmCt.ContactId = ct.Id;
            cmCt.High_Quality_Campaign_Response__c = true;
            insert cmCt;
            system.assert(cmCt.Id != null);
            
            // Update a lead campaign member
            cmLead.High_Quality_Campaign_Response__c = false;
            update cmLead;
            ld = [SELECT Id, High_Quality_Campaign_Response__c FROM Lead WHERE Id = :ld.Id];
            system.assert(ld.High_Quality_Campaign_Response__c == false);

            // Update a contact campaign member
            cmCt.High_Quality_Campaign_Response__c = false;
            update cmCt;
            ct = [SELECT Id, High_Quality_Campaign_Response__c FROM Contact WHERE Id = :ct.Id];
            system.assert(ct.High_Quality_Campaign_Response__c == false);

            Test.stopTest();
        }
    }

    static void Setup()
    {
        // Create a sample campaign
        camp.Name = 'Test Campaign';
        camp.Publishable_Name__c = 'Test Campaign';
        camp.IsActive = true;
        camp.Type = 'DM - Direct Marketing';
        camp.Campaign_Sub_Type__c = 'DM - Email Campaign';
        camp.LOB__c = 'Field Marketing';
        camp.Campaign_Objective__c = 'Brand Awareness';
        //camp.Campaign_Audience__c = 'Prospects';
        camp.QlikTech_Company__c = 'QlikTech Inc';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today() + 30;
        camp.Planned_Target_Audience__c = 100;
        camp.Planned_Leads__c = 100;
        camp.Planned_Opportunities__c = 10;
        camp.Planned_Opportunity_Value__c = 100000;
        camp.Campaign_Sector__c = 'Cross Sector';
        camp.Campaign_Job_Function__c = 'Executive';
        camp.Data_Source_Category__c = 'Microsoft';
        camp.Partner_Involvement__c = 'No Partner';
        camp.Campaign_Theme__c = 'Cross - Initiative';
        camp.High_Quality_Campaign__c = true;
        insert camp;
        system.assert(camp.Id != null);

        ld.Firstname = 'Test';
        ld.Lastname = 'Lead';
        ld.Company = 'Test Company';
        ld.LeadSource = 'Qlikmarket';
        ld.Country = 'USA';
        ld.Email = 'test@gmail.com';
        ld.Status = 'TEST';
        insert ld;
        system.assert(ld.Id != null);

        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert setting;

        acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
        system.assert(acct.Id != null);
        ct = QTTestUtils.createMockContact(acct.Id);
        system.assert(ct.Id != null);
    }
}