/**
* 2014-10-02 TJG     First created for CR #17488 https://eu1.salesforce.com/a0CD000000lWgd6
*                    This replaces the existing test class OppLockingTestClass.
* 2015-10-26 IRN     Winter 16 Issue- added call to globalSetup and SetupNSPriceBook and update api version
*
* 2016-06-03 TJG     Increase the coverage and fix issues during clean up project
 2016-09-19  MTM     Funcationality not releveant in Q2CW 
 */

@isTest
private class OpportunityLockTest {

 /*
    static testMethod void OpportunityLockOptOutTest() {
        QTTestUtils.GlobalSetUp();

        User userStd = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Sales Std User');
        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United Kingdom');
        QlikTech_Company__c QTCUsa = QTTestUtils.createMockQTCompany('QlikTech Inc.',   'USA', 'USA');
        //Account testAcnt = QTTestUtils.createMockAccount('Imperial Venture', userStd, true);
        //Contact testCon = QTTestUtils.createMockContact(testAcnt.Id);
        Opportunity testNewOpp = QTTestUtils.createMockOpportunity('Test for Opp Lock', 'TestOpp - Delte me', 
            'New Customer', 'Direct', 'Alignment Meeting', '01220000000DNwY', 'USD', userStd, true);

        testNewOpp.CloseDate = Date.today();
        testNewOpp.ForecastCategoryName = 'Omitted';
        testNewOpp.CurrencyIsoCode = 'GBP';
        testNewOpp.StageName = 'Closed Won';
        testNewOpp.Finance_Approved__c = true;              
        update testNewOpp;
        Test.startTest();    
        System.RunAs(userStd)
        {    
            boolean GotException = false;
            System.debug('Running as QlikBuy Sales Std User');
            try 
            {
                testNewOpp.Name = 'Test for Opp Lock update';
                update testNewOpp;
                System.debug('--OpportunityLockTest UpdatedName--' + testNewOpp.Name);
            }
            catch(DmlException dex)
            {
                System.debug('Got Exception on updating testNewOpp - ' + dex.getMessage());
                GotException = true;               
            }
            catch(Exception ex)
            {
                System.debug('Got Exception on updating testNewOpp - ' + ex.getMessage());
                GotException = true;
            }
            System.debug('--GotException: ' + GotException);
            System.assertEquals(false, GotException);
        }
        // this user should be allowed to update the opp
        User user0 = [select Id, name from user where Id in ('005D0000002SU5X', '005D0000001rW2q', '00520000000z3Of', '005D0000002TkRy') limit 1];
        System.RunAs(user0)
        {
            System.debug('RunAs user0 = ' + user0.name);
            testNewOpp.StageName = 'Trial';
            update testNewOpp;
        }
        Test.stopTest();  
        System.debug('--OpportunityLockTest Finished--');                      
    }


    static testMethod void OpportunityLockAllowedTest() {
        QTTestUtils.GlobalSetUp();
        System.debug('--OpportunityLockAllowedTest Started--');

        Profile p = [select id from profile where name='Custom: QlikBuy Sales Std User' limit 1];
        User userStd = new User(
            alias = 'standt', 
            email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', 
            lastname='Testing', 
            languagelocalekey='en_US',
            localesidkey='en_US', 
            profileid = p.Id,
            timezonesidkey='America/Los_Angeles', 
            username='standtest@qliktech.com.qtweb'
        );
        User me = [Select Id From User Where Id =:UserInfo.getUserId()];

        Insert userStd;
        
        System.assertNotEquals(null, userStd.Id);
        System.debug('--StdUser: ' + userStd);
        System.runAs(userStd) {
            QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United Kingdom');
            QlikTech_Company__c QTCUsa = QTTestUtils.createMockQTCompany('QlikTech Inc.',   'USA', 'USA');
        }

        Test.startTest();
        Opportunity testNewOpp;    
        System.RunAs(userStd)
        {    
            testNewOpp = QTTestUtils.createMockOpportunity(
                'Test for Opp Lock', 
                'TestOpp - Delte me', 
                'New Customer', 
                'Direct', 
                'Alignment Meeting', 
                '01220000000DNwY', 
                'USD', 
                userStd, 
                true
            );
        }

        testNewOpp.CloseDate = Date.today();
        testNewOpp.ForecastCategoryName = 'Omitted';
        testNewOpp.CurrencyIsoCode = 'GBP';
        testNewOpp.StageName = 'Closed Won';
        testNewOpp.Finance_Approved__c = true;              
        System.runAs(me) {
            update testNewOpp;
        }
        
        // find a user of the profile - Custom: QlikBuy Finance Manager with Change Control
        //User user1 = QTTestUtils.createMockUser('00eD0000001P4Ya', null);
        System.runAs(me) {
            User user1 = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Finance Manager with Change Control');
            testNewOpp.OwnerId = user1.Id;
        
            update testNewOpp;
        }
        User user2 = [select Id, name from user where Id in ('005D0000002SBno', '005D0000002SOYI', '00520000000z3Oi', '005D0000004Bsiq') limit 1];
        System.RunAs(user2)
        {
            System.debug('RunAs user2 = ' + user2.name);
            testNewOpp.StageName = 'Trial';
            update testNewOpp;
        }
        Profile pUser3 = [select id from profile where name='Custom: Api Only User'];
        User user3 = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pUser3.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com');
        //User user3 = QTTestUtils.createMockUserForProfile('Custom: Api Only User');
        System.RunAs(user3)
        {
            System.debug('RunAs user3 = ' + user3.name);
            testNewOpp.StageName = 'ER - Quote';
            update testNewOpp;
        }
        Test.stopTest();  
        System.debug('--OpportunityLockAllowedTest Finished--');                      
    }

    static testMethod void OpportunityLockValidUserTest() {
        QTTestUtils.GlobalSetUp();
        System.debug('--OpportunityLockValidUserTest Started--');

        Profile p = [select id from profile where name='Custom: QlikBuy Sales Std User'];
        User userStd = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com');
        insert userStd;
        User me = [Select Id From User Where Id =:UserInfo.getUserId()];
        System.assertNotEquals(null, userStd.Id);
        System.debug('--StdUser: ' + userStd);
        //User userStd1 = [select id from User where IsActive = true and Profile__c = 'Custom: QlikBuy Sales Std User' LIMIT 1];
        //User userStd = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Sales Std User');
        System.runAs(userStd) {
            QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United Kingdom');
            QlikTech_Company__c QTCUsa = QTTestUtils.createMockQTCompany('QlikTech Inc.',   'USA', 'USA');
        }
        //Account testAcnt = QTTestUtils.createMockAccount('Imperial Venture', userStd, true);
        //Contact testCon = QTTestUtils.createMockContact(testAcnt.Id);
        Test.startTest();
        Opportunity testNewOpp;    
        System.RunAs(me)
        {    
            testNewOpp = QTTestUtils.createMockOpportunity('Test for Opp Lock', 'TestOpp - Delte me', 
                'New Customer', 'Direct', 'Alignment Meeting', '01220000000DNwY', 'USD', userStd, true);
        }

        testNewOpp.CloseDate = Date.today();
        testNewOpp.ForecastCategoryName = 'Omitted';
        testNewOpp.CurrencyIsoCode = 'GBP';
        testNewOpp.StageName = 'Closed Won';
        testNewOpp.Finance_Approved__c = true;              
        System.runAs(me) {
            update testNewOpp;
        }
        // this user is not allowed 
        User user4 = [select Id, name from user where Id in ('005D0000002TKBs', '005D0000003Zjlt', '00520000000z6te', '005D0000002UCNS') limit 1];

        System.RunAs(user4)
        {    
            boolean GotException = false;
            System.debug('---Running as not allowed user4');
            try 
            {
                testNewOpp.Name = 'Test for Opp Lock update';
                update testNewOpp;
                System.debug('---OpportunityLockValidUserTest UpdatedName--' + testNewOpp.Name);
            }
            catch(Exception ex)
            {
                System.debug('Got Exception updating testNewOpp - ' + ex.getMessage());
                GotException = true;
            }
            System.debug('--GotException: ' + GotException);
            System.assertEquals(true, GotException);

            testNewOpp.Expertise_Area__c = 'Healthcare';
            // now it should work
            Update testNewOpp;
        }
        Test.stopTest();
    }

    static testMethod void OpportunityLockAcademicTest() {
        QTTestUtils.GlobalSetUp();
        System.debug('--OpportunityLockAcademicTest Started--');

        Profile p = [select id from profile where name='Custom: QlikBuy Sales Std User'];
        User userStd = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com');
        insert userStd;
        User me = [Select Id From User Where Id =:UserInfo.getUserId()];
        System.assertNotEquals(null, userStd.Id);
        System.debug('--StdUser: ' + userStd);
        //User userStd1 = [select id from User where IsActive = true and Profile__c = 'Custom: QlikBuy Sales Std User' LIMIT 1];
        //User userStd = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Sales Std User');
        System.runAs(userStd) {
            QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United Kingdom');
            QlikTech_Company__c QTCUsa = QTTestUtils.createMockQTCompany('QlikTech Inc.',   'USA', 'USA');
        }
        //Account testAcnt = QTTestUtils.createMockAccount('Imperial Venture', userStd, true);
        //Contact testCon = QTTestUtils.createMockContact(testAcnt.Id);
        Test.startTest();
        Opportunity testNewOpp;    
        // Academic program OpportunityRecordTypeId__c = '012D0000000KB2N';
        System.RunAs(me)
        {    
            testNewOpp = QTTestUtils.createMockOpportunity('Test for Opp Lock', 'TestOpp - Delte me', 
                'New Customer', 'Direct', 'Alignment Meeting', '012D0000000KB2N', 'USD', userStd, true);
        }

        testNewOpp.CloseDate = Date.today();
        testNewOpp.ForecastCategoryName = 'Omitted';
        testNewOpp.CurrencyIsoCode = 'GBP';
        testNewOpp.StageName = 'Closed Won';
        testNewOpp.Finance_Approved__c = true;              
        System.runAs(me) {
            update testNewOpp;
        }
        // this user is not allowed, but for Academic program it's fine
        User user5 = [select Id, name from user where Id in ('005D0000002TKBs', '005D0000003Zjlt', '00520000000z6te', '005D0000002UCNS') limit 1];

        System.RunAs(user5)
        {    
            boolean GotException = false;
            System.debug('---Running as not allowed user user5');
            try 
            {
                testNewOpp.Name = 'Test for Opp Lock update';
                update testNewOpp;
                System.debug('---OpportunityLockAcademicTest UpdatedName--' + testNewOpp.Name);
            }
            catch(Exception ex)
            {
                System.debug('Got Exception updating testNewOpp - ' + ex.getMessage());
                GotException = true;
            }
            System.debug('--GotException: ' + GotException);
            System.assertEquals(false, GotException);
        }
        Test.stopTest();
    }

    //static testMethod void testLineitems() {
    //    QTTestUtils.GlobalSetUp();
    //    QTTestUtils.SetupNSPriceBook('GBP');

    //    test.startTest();
        
    //    //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
    //    User user = QTTestUtils.createMockSystemAdministrator();
    //    Account testAcc = QTTestUtils.createMockAccount('TestAccount', user);
    //    Contact testCon = QTTestUtils.createMockContact(testAcc.Id);

    //    Opportunity testNewOpp = New Opportunity (
    //        Short_Description__c = 'TestOpp Again',
    //        Name = 'TestOpp Again',
    //        Type = 'New Customer',
    //        Revenue_Type__c = 'Direct',
    //        CloseDate = Date.today(),
    //        StageName = 'Alignment Meeting',
    //        ForecastCategoryName = 'Omitted',   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
    //        RecordTypeId = '01220000000DNwY',
    //        AccountId = testCon.AccountId,
    //        Signature_Type__c = 'Digital Signature',
    //        Customers_Business_Pain__c='CustomerBusinessPain', //CR#16608 Adding customer business pain for large deals (FIELD_CUSTOM_VALIDATION_EXCEPTION, Alignment section must be completed on large deals.: [Customers_Business_Pain__c])
    //        CurrencyIsoCode = 'GBP'
    //    );

    //    insert testNewOpp;
        
    //    OpportunityLineItem LineItem = QTTestUtils.createMockOppLineItem(testNewOpp, 'Licenses', '4730', 
    //                                 'QlikView Small Business Edition Server','QVSSBE', 'QVSSBE', 2,
    //                                 595,595, null,
    //                                 0, 'Added', true, false); 


    //    System.debug(LineItem);
                        
    //    test.stopTest();   
    //}   
    */
}