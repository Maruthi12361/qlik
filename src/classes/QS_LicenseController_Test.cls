/************************************************************************
 * @author  SAN
 * 2014-10-30 customer portal project
 *  
 * Test Class for License Pages QS_LicenseController_Test.
 * 
 * CR# 90369: Added 2 new test method to test for requestcontrol number scenarios Date Added: Feb 17th 2017
 *
 * 2018-04-05 AYS IT-117 Added test set up method
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 *************************************************************************/

@isTest
private class QS_LicenseController_Test {

    @testSetup static void testSetup() {
        insert new QS_License__c(QS_LicenseQueryLimit__c = 1000, SetupOwnerId=UserInfo.getOrganizationId());

        Folder f = [SELECT Id FROM Folder WHERE DeveloperName = 'Images_and_Logos'];
        Document imgFooter = new Document();
        imgFooter.DeveloperName = 'Qlik_Email_Footer_TEST';
        imgFooter.Name = 'Qlik_Email_Footer_TEST';
        imgFooter.FolderId = f.Id;
        insert imgFooter;
    }

    static private User createCommunityUser()
    {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        return communityUser;
    }


    @isTest static void test_Constructor() {

        User communityUser = createCommunityUser();

        Test.startTest();
        // Running as the current user
        QS_LicenseController licenseController = new QS_LicenseController();
        System.assert(licenseController.displayPopup == false);

        QS_EntitlementWrapper entitlementWrapper = new QS_EntitlementWrapper();
        System.assert(entitlementWrapper.checked == false);



        // Running as the community user
        System.runAs(communityUser) {

            licenseController = new QS_LicenseController();


        }


        Test.stopTest();
    }

    @isTest static void test_method_SimpleCheck16_impl() {
        QS_LicenseController licenseController = new QS_LicenseController();
        integer checkSum = 12357;
        System.assert(licenseController.Chk == 4711);
        licenseController.SimpleCheck16_impl('1234123412341234');
        System.debug(licenseController.Chk);
        System.assertEquals(licenseController.Chk, checkSum);

    }

    // calling web service method so usually skipped for uni ttest results, but it's here for code coverage
    @isTest static void test_method_generateLEF() {
        QS_LicenseController licenseController = new QS_LicenseController();

        //licenseController.getresultsLicenses();
        //licenseController.openDownloadLEFPage();
        //licenseController.GenerateLEF();
        // for code coverage only
        String result = licenseController.getLicenseNamesReference();
        //PageReference tempLefPageRef = licenseController.lefPageReference();
        //System.assert(tempLefPageRef != null);
        //ApexPages.currentPage().getParameters().put('licenseNames', tempLefPageRef.getParameters().get('licenseNames'));
        ApexPages.currentPage().getParameters().put('licenseNames', '');
        QS_License_DownloadLEF_Cntrl downloadLEFCntrl = new QS_License_DownloadLEF_Cntrl();
        List<String> letterLines = downloadLEFCntrl.getLetterLines();
        //System.assert(downloadLEFCntrl.getLetterLines() == null || downloadLEFCntrl.getLetterLines().isEmpty());

        //List<String> result = licenseController.getLetterLines();

        //System.assertEquals(result.get(0),'No_License_selected');

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        Entitlement productLicense2 = TestDataFactory.createLicense(testAccount.Id, 'licenseReference2', '1234123412344354');
        insert productLicense2;

        licenseController.resultsOriginalLicenses.add(productLicense);
        licenseController.resultsOriginalLicenses.add(productLicense2);
        //licenseController.getresultsLicenses();

        if (!licenseController.resultsLicenses.isEmpty())
        {

            //licenseController.GenerateLEF();
            result = licenseController.getLicenseNamesReference();
            //PageReference lefPageRef = licenseController.lefPageReference();
            //System.assert(lefPageRef != null);
            //ApexPages.currentPage().getParameters().put('licenseNames', lefPageRef.getParameters().get('licenseNames'));
            ApexPages.currentPage().getParameters().put('licenseNames', '1234123412341234-regex-1234123412344354');
            downloadLEFCntrl = new QS_License_DownloadLEF_Cntrl();
            letterLines = downloadLEFCntrl.getLetterLines();
            //System.assert(downloadLEFCntrl.getLetterLines() != null);
        }
    }

    // calling web service method so usually skipped for uni ttest results, but it's here for code coverage
    @isTest static void test_method_generateLEF_Multi() {
        QS_LicenseController licenseController = new QS_LicenseController();



        //licenseController.getresultsLicenses();
        //licenseController.openDownloadLEFPage();

        //PageReference tempLefPageRef = licenseController.lefPageReference();
        //System.assert(tempLefPageRef != null);
        ApexPages.currentPage().getParameters().put('licenseNames', '');
        QS_License_DownloadLEF_Cntrl downloadLEFCntrl = new QS_License_DownloadLEF_Cntrl();
        List<String> letterLines = downloadLEFCntrl.getLetterLines();
        //System.assert(downloadLEFCntrl.getLetterLines() == null || downloadLEFCntrl.getLetterLines().isEmpty());

        // for code coverage only

        String resultURL = licenseController.getLicenseNamesReference();

        //System.assertEquals(resultURL,'');

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;


        licenseController.myLicense = productLicense;
        //licenseController.openDownloadLEFPage();
        //resultURL = licenseController.getLicenseNamesReference();
        //PageReference lefPageRef = licenseController.lefPageReference();
        //System.assert(lefPageRef != null);
        ApexPages.currentPage().getParameters().put('licenseNames', '1234123412341234-regex-1234123412344354');
        downloadLEFCntrl = new QS_License_DownloadLEF_Cntrl();
        letterLines = downloadLEFCntrl.getLetterLines();
        //System.assert(downloadLEFCntrl.getLetterLines() != null);


    }
    @isTest static void test_method_generateLEFExpired() {
        QS_LicenseController licenseController = new QS_LicenseController();

        //licenseController.getresultsExpiredLicenses();
        //licenseController.openDownloadLEFPage();
        //licenseController.GenerateLEF();
        // for code coverage only

        ApexPages.currentPage().getParameters().put('licenseNames', '');
        QS_License_DownloadLEF_Cntrl downloadLEFCntrl = new QS_License_DownloadLEF_Cntrl();
        List<String> letterLines = downloadLEFCntrl.getLetterLines();
        //System.assert(downloadLEFCntrl.getLetterLines() == null || downloadLEFCntrl.getLetterLines().isEmpty());

        //List<String> result = licenseController.getLetterLines();

        //System.assertEquals(result.get(0),'No_License_selected');

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Entitlement productLicense = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        Entitlement productLicense2 = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference2', '1234123412344354');
        insert productLicense2;

        licenseController.selectedAccountName = testAccount.Name;


        licenseController.resultsOriginalLicenses.add(productLicense);
        licenseController.resultsOriginalLicenses.add(productLicense2);
        //licenseController.getresultsExpiredLicenses();

        List<Entitlement> listEnt = licenseController.resultsOriginalLicenses;
        if (!licenseController.resultsExpiredLicenses.isEmpty())
        {
            licenseController.resultsExpiredLicenses[0].checked = true;
            licenseController.resultsExpiredLicenses[1].checked = true;



            //licenseController.GenerateLEF();
            ApexPages.currentPage().getParameters().put('licenseNames', '1234123412341234-regex-1234123412344354');
            downloadLEFCntrl = new QS_License_DownloadLEF_Cntrl();
            letterLines = downloadLEFCntrl.getLetterLines();
            //System.assert(downloadLEFCntrl.getLetterLines() != null);
        }
    }

    @isTest static void test_method_ClosePopup() {
        QS_LicenseController licenseController = new QS_LicenseController();
        licenseController.displayPopup = true;
        System.assert(licenseController.displayPopup == true);
        licenseController.closePopup();
        System.assert(licenseController.displayPopup == false);

    }

    @isTest static void test_method_getLetterLines() {
        QS_License_DownloadLEF_Cntrl licenseLEFController = new QS_License_DownloadLEF_Cntrl();
        List<String> result = licenseLEFController.getLetterLines();
        System.assertEquals(licenseLEFController.letterBody, 'No_License_selected');
        licenseLEFController.letterBody = null;
        result = licenseLEFController.getLetterLines();
        licenseLEFController.generateLEF(null);
        //System.assert(result.isEmpty() == '');

    }

    @isTest static void test_method_ControlNumberRequest() {
        QS_LicenseController licenseController = new QS_LicenseController();
        //licenseController.getresultsLicenses();
        licenseController.ControlNumberRequest();
        // for code coverage only

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        licenseController.resultsOriginalLicenses.add(productLicense);
        //licenseController.getresultsLicenses();

        licenseController.myLicense = productLicense;
        licenseController.ControlNumberRequest();

    }


    @isTest static void test_method_ControlNumberRequest_Multi() {
        QS_LicenseController licenseController = new QS_LicenseController();
        //licenseController.getresultsLicenses();
        licenseController.ControlNumberRequest();
        // for code coverage only

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        licenseController.resultsOriginalLicenses.add(productLicense);
        //licenseController.getresultsLicenses();

        if(!licenseController.resultsLicenses.isEmpty())
        {
            licenseController.resultsLicenses[0].checked = true;
        }

        licenseController.ControlNumberRequest();



    }


    @isTest static void test_method_SaveLicense() {
        QS_LicenseController licenseController = new QS_LicenseController();

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        licenseController.myLicense = productLicense;

        System.assertEquals(licenseController.myLicense.License_Reference__c,'licenseReference');

        licenseController.SaveLicense();


    }

    @isTest static void testProperties()
    {
        QS_LicenseController licenseController = new QS_LicenseController();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        List<SelectOption> options = licenseController.ProductOptionListFiltered;
        options = licenseController.SupportingPartnerOptionListFiltered;
        options = licenseController.SupportLevelOptionListFiltered;
        QS_LicenseController.navtoLicensePage('test');
        licenseController.SetSelectedLicenseListToActive();
        licenseController.SetSelectedLicenseListToExpired();

        string proname = QS_LicenseController.proname;
        Environment__c e = licenseController.myEnv;

        List<SelectOption> filterList = new List<SelectOption>();
        filterList.add(new selectOption('test','test'));
        licenseController.SpartnerList = filterList;
        licenseController.FilterPartners();
        integer size = licenseController.SpartnerListSize;

        List<SelectOption> ProductOptionListFiltered = new List<SelectOption>();
        ProductOptionListFiltered.add(new selectOption('test','test'));
        licenseController.ProductOptionListFiltered = ProductOptionListFiltered;
    }

    @isTest static void test_method_getExpiredLicenses() {

        system.debug('test_method_getExpiredLicenses start');


        //licenseController.getresultsExpiredLicenses();
        //licenseController.openDownloadLEFPage();
        //licenseController.GenerateLEF();
        // for code coverage only

        //System.assert(downloadLEFCntrl.getLetterLines() == null || downloadLEFCntrl.getLetterLines().isEmpty());

        //List<String> result = licenseController.getLetterLines();

        //System.assertEquals(result.get(0),'No_License_selected');

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Entitlement productLicense = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        Entitlement productLicense2 = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference2', '1234123412344354');
        insert productLicense2;

        QS_LicenseController licenseController;
        system.runAs(communityUser) {
            licenseController = new QS_LicenseController();
            licenseController.selectedAccountName = testAccount.Name;

            licenseController.selectedAccountNameHasChanged =true;
            licenseController.selectedAccountName = testAccount.Id;
            system.debug('test_method_getExpiredLicenses before get licenses');

            List<Entitlement> listEnt = licenseController.resultsOriginalLicenses;
            if (!licenseController.resultsExpiredLicenses.isEmpty()) {
                licenseController.resultsExpiredLicenses[0].checked = true;
                licenseController.resultsExpiredLicenses[1].checked = true;
            }
        }
    }
    @isTest static void test_method_getExpiredLicensesOneLicenseSelected() {

        system.debug('test_method_getExpiredLicenses start');


        //licenseController.getresultsExpiredLicenses();
        //licenseController.openDownloadLEFPage();
        //licenseController.GenerateLEF();
        // for code coverage only

        //System.assert(downloadLEFCntrl.getLetterLines() == null || downloadLEFCntrl.getLetterLines().isEmpty());

        //List<String> result = licenseController.getLetterLines();

        //System.assertEquals(result.get(0),'No_License_selected');

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Entitlement productLicense = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        Entitlement productLicense2 = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference2', '1234123412344354');
        insert productLicense2;

        QS_LicenseController licenseController;
        system.runAs(communityUser) {
            licenseController = new QS_LicenseController();
            licenseController.selectedAccountName = testAccount.Name;

            licenseController.selectedAccountNameHasChanged =true;
            licenseController.selectedAccountName = testAccount.Id;
            QS_LicenseController.selectedLicense1 = productLicense.Id;
            system.debug('test_method_getExpiredLicenses before get licenses');

            List<Entitlement> listEnt = licenseController.resultsOriginalLicenses;

        }
    }

    //CR# 90369: RequestControlNumber Valid Testing for alert mail generation
    @isTest static void test_Controlnumbermethods() {

        system.debug('test_method_getExpiredLicenses start');


        //licenseController.getresultsExpiredLicenses();
        //licenseController.openDownloadLEFPage();
        //licenseController.GenerateLEF();
        // for code coverage only

        //System.assert(downloadLEFCntrl.getLetterLines() == null || downloadLEFCntrl.getLetterLines().isEmpty());

        //List<String> result = licenseController.getLetterLines();

        //System.assertEquals(result.get(0),'No_License_selected');

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.INT_NetSuite_InternalID__c ='1111';
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Entitlement productLicense = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        Entitlement productLicense2 = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference2', '1234123412344354');
        insert productLicense2;
        string licenseIds = productLicense.Id + ';' + productLicense2.Id;

        List<QS_DataCapture__c> requests = new List<QS_DataCapture__c>();
        for(integer i=0;i<12;i++){
            QS_DataCapture__c request = new QS_DataCapture__c();
            request.Licenses__c = licenseIds;
            request.Control_Number_Request_Time__c = datetime.now();
            request.ip_address__c = '1.1.1.1';
            request.ownerid = communityUser.Id;
            request.RecordtypeId = Schema.SObjectType.QS_DataCapture__c.getRecordTypeInfosByName().get('Request Control Number').getRecordTypeId();
            requests.add(request);
        }
        for(integer i=0;i<6;i++){
            QS_DataCapture__c request = new QS_DataCapture__c();
            request.Licenses__c = licenseIds;
            request.Control_Number_Request_Time__c = datetime.now()-7;
            request.ip_address__c = '1.1.1.1';
            request.ownerid = communityUser.Id;
            request.RecordtypeId = Schema.SObjectType.QS_DataCapture__c.getRecordTypeInfosByName().get('Request Control Number').getRecordTypeId();
            requests.add(request);
        }
        insert requests;



        QS_LicenseController licenseController;
        system.runAs(communityUser) {
            licenseController = new QS_LicenseController();
            string commId = communityUser.Id;
            //licenseController.genExceptionEmails(communityUser.Id, '1111');
            string orgId = UserInfo.getOrganizationId();
            QS_LicenseController.invokeBoomiProcess(orgId, licenseIds, '1111', communityUser.Id, 'a@b.com', '1.1.1.1', 'test');
            //licenseController.createRequestRecord(licenseIds, communityUser.Id, datetime.now(), '1.1.1.1');
            licenseController.openDownloadLEFPage();

            licenseController.selectedAccountNameHasChanged =true;
            licenseController.selectedAccountName = testAccount.Id;
            QS_LicenseController.selectedLicense1 = productLicense.Id;
            system.debug('test_method_getExpiredLicenses before get licenses');

            List<Entitlement> listEnt = licenseController.resultsOriginalLicenses;

        }
    }

    //CR# 90369: RequestControlNumber Valid Testing Method
    @isTest static void test_RequestNumberValidOrNot() {
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.INT_NetSuite_InternalID__c ='1111';
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Entitlement productLicense = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        Entitlement productLicense2 = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference2', '1234123412344354');
        insert productLicense2;
        string licenseIds = productLicense.Id + ';' + productLicense2.Id;

        QS_LicenseController licenseController;
        system.runAs(communityUser) {
            licenseController = new QS_LicenseController();
            string commId = communityUser.Id;
            string orgId = UserInfo.getOrganizationId();
            QS_LicenseController.checkReqNumberValidOrNot(testAccount.INT_NetSuite_InternalID__c, testContact.ID, '');
        }

    }

    //CR# 90369: RequestControlNumber Negative Testing Method
    @isTest static void test_RequestNumberValidOrNotNegative() {
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        Account partnerAccount  = TestDataFactory.createAccount('Test Partner Account', qtc);
        partnerAccount.Website = 'https://test.salesforce.com';
        insert partnerAccount;

        Contact partnerContact = TestDataFactory.createContact('test_FName', 'test_PName', 'testSandboxPartner@qlikTech.com', '+44-7878787878', partnerAccount.Id);
        partnerContact.Persona__c = 'Decision Maker';
        insert partnerContact;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.INT_NetSuite_InternalID__c ='1111';
        insert testAccount;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        Entitlement productLicense = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference', '1234123412341234');
        insert productLicense;

        Entitlement productLicense2 = TestDataFactory.createExpiredLicense(testAccount.Id, 'licenseReference2', '1234123412344354');
        insert productLicense2;
        string licenseIds = productLicense.Id + ';' + productLicense2.Id;

        QS_LicenseController licenseController2;
        system.runAs(communityUser) {
            licenseController2 = new QS_LicenseController();
            string commId = communityUser.Id;
            string orgId = UserInfo.getOrganizationId();
            QS_LicenseController.checkReqNumberValidOrNot('1121', testContact.ID, '');
        }

    }
}