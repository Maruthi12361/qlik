/**************************************************
* CR# 97533 – Relayware triggers
* Change Log:
* 2016-10-31 BAD Initial creation of test class
2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
**************************************************/
@isTest
private class ContactPartnerPortalRelaywareHandlerTest
{
	
	@isTest
	static void test_Contact_portal_access_Relayware_approved()
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        //we don't use this instance, but create it to execute the class constructor
        ContactPartnerPortalRelaywareHandler tempObj = new ContactPartnerPortalRelaywareHandler();

        System.RunAs(mockSysAdmin)
        { 
			Account acc = QTTestUtils.createMockAccount('Test Account', mockSysAdmin, false);
			acc.Navision_Status__c = 'Partner';
			insert acc;
			system.assert(acc.Id != null);

			//Contact con = QTTestUtils.createMockContact(acc.Id);
    		Contact con = new Contact(firstName='RDZTestContact', 
                   lastName='RDZTestLastName', 
                   Email='RDZTest@test.com.sandbox',
                   HasOptedOutOfEmail=false,
                   AccountId = acc.Id
                   );
    		insert con;
			system.assert(con.Id != null);
			 System.debug('--Check QLIKDID1: ' + con.QlikID__c);

			ULC_Details__c ulc = QTTestUtils.createMockULCDetail(con.Id, 'Active');
			system.assert(ulc.Id != null);	        
			System.debug('--Check QLIKDID2: ' + con.QlikID__c);
               
	        Test.startTest();
			
			Semaphores.TriggerHasRun(1);
			Semaphores.ContactTriggerHandlerBeforeUpdate = false;
			con.QlikID__c = ulc.Id;
			con.ActiveULC__c = true;
	        update con;
	        System.debug('--Check QLIKDID3: ' + con.QlikID__c);
	        System.debug('--Check ERP: ' + acc.Navision_Status__c);
	        System.debug('--Check Terminated: ' + acc.Terminated__c);
	        System.debug('--Check Active ulc: ' + con.ActiveULC__c);

	        Contact conTemp=[select Synch_with_Relayware__c,Partner_Contact_Approval_Status__c from Contact where id=:con.id];
	        system.assertEquals(true, conTemp.Synch_with_Relayware__c);
	        system.assertEquals(true, conTemp.Partner_Contact_Approval_Status__c);

	        Test.stopTest();
		}        
	}


	@isTest
	static void test_Contact_portal_access_Relayware_terminated()
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin)
        { 
			Account acc = QTTestUtils.createMockAccount('Test Account', mockSysAdmin, false);
			acc.Navision_Status__c = 'Partner';
			insert acc;
			system.assert(acc.Id != null);

			//Contact con = QTTestUtils.createMockContact(acc.Id);
  			Contact con = new Contact(
  					FirstName='RDZTestContact', 
                   	LastName='RDZTestLastName', 
                   	Email='RDZTest@test.com.sandbox',
                   	AccountId = acc.Id,
                   	Partner_Contact_Approval_Status__c = true
                   	);			
			insert con;
			system.assert(con.Id != null);
			System.debug('--Check Left: ' + con.Left_Company__c);
			System.debug('--Check app: ' + con.Partner_Contact_Approval_Status__c);
			
			ULC_Details__c ulc = QTTestUtils.createMockULCDetail(con.Id, 'Active');
			system.assert(ulc.Id != null);	        
            
			Semaphores.TriggerHasRun(1);
			Semaphores.ContactTriggerHandlerBeforeUpdate = false;
			con.Left_Company__c = true;
	        update con;

	        Test.startTest();

	        System.debug('--Check Left2: ' + con.Left_Company__c);
	        Contact conTemp=[select Left_Company__c,Partner_Contact_Approval_Status__c from Contact where Account.Navision_Status__c = 'Partner'  and id=:con.id];
	        System.debug('--Check Left3: ' + conTemp.Left_Company__c);
	        //system.assertEquals(true, conTemp.Synch_with_Relayware__c);
	        system.assertEquals(false, conTemp.Partner_Contact_Approval_Status__c);

	        Test.stopTest();
		}        
	}


}