/*
    AddressTriggerHelperTest
    Change log
    2018-01-26 MTM Initial development QCW-2983 Optimize StrikeIron Usage

*/
@isTest
public class AddressTriggerHelperTest 
{
    public static testMethod void TestCheckforUpdates()
    {
        Init();
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            List<Address__c> updatedAddresss = new List<Address__c>();
            Map<ID, Address__c> oldAddressMap = new Map<ID, Address__c>();
            Test.startTest();
            Address__c updatedAddress = TestAddress;
            updatedAddress.State_Province__c = 'Lyon';
            updatedAddresss.Add(updatedAddress);
            oldAddressMap.put(TestAddress.Id, TestAddress);
            Set<Id> updateId = AddressTriggerHelper.CheckforUpdates(updatedAddresss,oldAddressMap);
            System.assertEquals(updateId.size(),1);
            Test.stopTest();
        }
    }

    public static testMethod void TestIsApiUser()
    {
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            System.assertEquals(false,AddressTriggerHelper.IsApiUser());
        }
    }

    public static void Init()
    {
        QuoteTestHelper.createCustomSettings();
        User testUser = QTTestUtils.createMockOperationsAdministrator();
        Account testAccount = QTTestUtils.createMockAccount('Test account', testUser, false);        
        testAccount.BillingCountry = 'France';
        insert testAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        insert testContact;
        Test.setMock(WebServiceMock.class, new AdressMockService());        
        TestAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
        insert TestAddress;        
    }

    private Static Address__c TestAddress;

}