/*************************************************************************************************************
 Name:PPMarketingPortalClassTest
 Author: Santoshi Mishra
 Purpose: This is test class for PPMarketingPortalClass
 Created Date : April 2012
 
 Log History:
 2012-08-23  	Santoshi Mishra Add Select Stament to createUser to get a test contact.
 2012-10-16  	SAN Fix the unit test to pass the future call limit problem
 2103-06-07 	SLH fixed line 67 due to new master-detail on Business Plan
 2013-06-20	 	MHG fix for test failure, Non Partner users cannot be linked to a contact.
 				Adding GoldPartner to the query on createUser
 2014-12-10   RDZ fix for test failure due to refering to more than 10 namespaces 
 2016-01-25   TJG fix the error of FIELD_CUSTOM_VALIDATION_EXCEPTION, Account can be owned only by Master Reseller Partners.  
 2016-02-24   CCE  CR# 74266 New Calculations for Most Pop Resources - updating test class
 07.02.2017   RVA :   changing QT methods
 2017-03-31   BAD  Removed dead code for Q2CW deployment
 2018-10-12   Cognizant  BSL-1040 Commented code as part of Clean Up Activity
*************************************************************************************************************/
@isTest
private class PPMarketingPortalClassTest {
    
    //RDZ Separating test methods to test things in different methods.
    //test method for PPSearchComponentClass
    static testmethod void NewsAndEventsTest()
    {
        List <What_s_New__c> whatsNewList = new List <What_s_New__c>();
        List <Upcoming_Events__c> upcoimngEventsList=new List <Upcoming_Events__c>();
        boolean showNews = false;
        //System.test.starttest();

        for(integer i=0;i<=6;i++)
          {
              What_s_New__c w = new What_s_New__c();
              w.Headline__c='news'+String.ValueOf(i);
              w.Link__c='https://www.salesforce.com';
              w.Active__c=true;
              whatsNewList.add(w);
              Upcoming_Events__c ue = new Upcoming_Events__c();
              ue.Event_Name__c='events'+String.ValueOf(i);
              ue.Link__c='https://www.salesforce.com';
              ue.active__c=true;
              upcoimngEventsList.add(ue);
          }

        if(whatsNewList.size()>5)
        {
         whatsNewList.remove(whatsNewList.size()-1);
         showNews=true;
        }

         insert(whatsNewList);
         insert(upcoimngEventsList);

         //Check whatsNew and Events
         //TODO
    }


    static testmethod  void MarketingPortalClassTest() // test method for PPMarketingPortalClass
    {
        QTTestUtils.GlobalSetUp();
        PPMDFSettings__c csMDF = new PPMDFSettings__c();
        csmdf.MDF_Terms_DocumentID__c = 'test';
        csMDF.Vistex__c = 'test';
        insert csMDF;
        User u=  getUser();//Retrieves Partner Portal User
        User u1 = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');
        System.debug('checking-11---'+u.ProfileId + '--'+ u.Id);
        
        User u1Ret = [select Id, Profile__c, UserType from User where Id = : u1.Id];
        System.debug('MarketingPortalClassTest: u1Ret = ' + u1Ret); 
        // SAN 16/10/2012 To stop PPMarketingPortalClassTest test failing due to @future callout
        // the semaphore will be looked at by lastModifiedUpdate and OpportunitySharingRulesOnSellThroughPartner        
        Semaphores.PPMarketingPortalClassTestHasRun = true;
        //SAN 16/10/2012 Create test Account to make sure Opportunity can be created with company and billing code
		    /*Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
            Country_Name__c = 'USA',
			Subsidiary__c = testSubs1.id         
        );
        insert QTComp;*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c, Country_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account testAcnt = new Account(Name='TestAccountWhenEISCreatesOpps1', OwnerId = u1.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert testAcnt;
		 Test.startTest();
        Opportunity op = New Opportunity (
            Short_Description__c = 'TestOpp - delete me',
            Name = 'TestOpp - DeleteMe',
            CloseDate = Date.today().adddays(10),
            StageName = 'Open',
            Sell_Through_Partner__c=u.AccountId,Amount=100,
            AccountId = testAcnt.Id
        );                    
            
        insert op;
        //Start test before inserting fund request to avoid issue with namespace governor limit
       
        /* 2018-10-12 Cognizant BSL-1040 Commented code as part of Clean Up Activity - Commenting below line
		CTSK_PFM__Fund_Request__c fund = new CTSK_PFM__Fund_Request__c(CTSK_PFM__Account__c=u.AccountId,CTSK_PFM__Funding_Approved__c=10);
        insert fund;
		*/
      Test.stopTest();
        //SLH 201306-07 changed to testAcnt.Id instead of u.Accountid - u.AccountId was not populated
        Business_Plan__c b = new Business_Plan__c(Q1_Revenue_Target__c=10,Q2_Revenue_Target__c=10,Q3_Revenue_Target__c=10,Q4_Revenue_Target__c=10,Yearly_Revenue_Target__c=100,Partner_Account__c=testAcnt.Id);
        insert b;
        System.runas(u)
        {
          Task t = new Task (Subject='Call',Status='New',OwnerId=u.Id,Type='Call');
          insert(t);
          System.debug('ppppppppp--'+UserInfo.getUserType());
          setCustomSetting();
          System.debug('checking contactid---'+u.ContactId);
          
          PPMarketingPortalClass m = new PPMarketingPortalClass();
           
         // m.getContentVersions();
          m.getTask();
          m.redirect();
          m.newTask();
          m.editPartnerProfile();
          m.frameUrl();
        }
              
        Semaphores.PPMarketingPortalClassTestHasRun = false;
        

    }

    static testMethod void PPSearchComponentTest()
    {
      User u=  getUser();//Retrieves Partner Portal User
      System.debug('checking-11---'+u.ProfileId + '--'+ u.Id);
      System.runAs(u)
      {  
        PPSearchComponentClass sr = new PPSearchComponentClass();
        sr.srchtext='test';
        sr.srchknowledge='test';
        sr.srchContent='test';
        sr.redirectKnowledge();
        sr.redirectSearch();
        sr.redirectContent();
      }
    }
    
    //CCE  CR# 74266 New Calculations for Most Pop Resources - updating test class
    static testMethod void testContent()
    {
        System.test.starttest();
        List<ContentVersionHistory> conlist;
        ContentVersion contentVersion = setupContent(); // This method inserts content and download history record.
        system.debug('testContent: contentVersion = ' + contentVersion);
        PPMarketingPortalClass m1 = new PPMarketingPortalClass();
        List<ContentVersion> converList = m1.getContentVersions();
        system.debug('testContent: converList = ' + converList);
        //As the PopularLibraryDownload__c record we create has only one Id in the list we should on get a result array back containing 1 item
        System.assertequals(1, converList.size());
        System.test.Stoptest();    
    }
    
    /*static testMethod void testContent()
    {
        System.test.starttest();
        List<ContentVersionHistory> conlist;
        ContentVersion contentVersion = setupContent(); // This method inserts content and download history record. In order to test method run successfully user running this test class should have Content permissions.
        PPMarketingPortalClass m1 = new PPMarketingPortalClass();
        m1.getContentVersions();
        System.test.Stoptest();
     
    
    }*/
    static testMethod void redirectTaskTest() // Test method for ppDeleteTaskClass
    {
        System.test.starttest();
        List<Task> taskList = new List<Task>();
        Task t = new Task (Subject='Call',Status='New',OwnerId=System.Userinfo.getUserId(),Type='Call');
        Task t2 = new Task (Subject='Call',Status='New',OwnerId=System.Userinfo.getUserId(),Type='Call');
        taskList.add(t2);
        taskList.add(t);
        insert(taskList);
        PageReference VFpage = Page.ppDeleteTaskPage;
        VFpage.getParameters().put(';delID',t2.Id);
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(t2);
        ppDeleteTaskClass CLS = new ppDeleteTaskClass(VFpage_Extn);
        Pagereference p = CLS.redirectTask();
        VFpage = Page.ppDeleteTaskPage;
        test.setCurrentPage(VFpage);
        VFpage_Extn = new ApexPages.StandardController(t);
        CLS = new ppDeleteTaskClass(VFpage_Extn);
        p = CLS.redirectTask();
        System.assertequals(p.getURL(),'/home/home.jsp');// Checks that redirection is done on standard home page for non partner portal users.
        User u=  getUser();
        System.debug('checking----'+u.ProfileId + '--'+ u.Id);
        System.runas(u)
               {
                 Task t1 = new Task (Subject='Call',Status='New',OwnerId=u.Id,Type='Call');
                 insert(t1);
                 VFpage = Page.ppDeleteTaskPage;
                 test.setCurrentPage(VFpage);
                 VFpage_Extn = new ApexPages.StandardController(t1);
                 CLS = new ppDeleteTaskClass(VFpage_Extn);
                 p = CLS.redirectTask();
                 System.debug('---'+p+'1----'+p.getURL());
                 System.assertequals(p.getURL(),'/apex/pphome');// Checks that redirection is done on new home page for partner portal.
               }
        System.test.stoptest();
    }
    
    static testMethod void AccountTest() // Test method for PPAccountClass
    {
        System.test.starttest();
        PPAccount ac1 = new PPAccount();
        Pagereference p1=ac1.redirectUser();
        User u=  getUser();
        System.runas(u)
        {
            PPAccount ac = new PPAccount();
            Pagereference p=ac.redirectUser();
         }
         System.test.stoptest();
           
      }


    
    public static User getUser()
     {
       User u = createUser();
       // User u= retrieveUser();
        return u;
     }
    public static User createUser()// creates partner portal user
     {
		/*Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c q = new QlikTech_Company__c(Country_Name__c='United States',Name='USA',Language__c='English',QlikTech_Company_Name__c='CT', Subsidiary__c = testSubs1.id);
        insert(q);*/
		QlikTech_Company__c q = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'USA');
        Account acct = new Account(Name='Test Account1', BillingCountry='United States',BillingState='WWT', CurrencyISOCode='USD',QlikTech_Company__c='CT',QlikTech_Region__c='CT',Billing_Country_Code__c=q.Id,Website='www.qt.com',BillingPostalCode='566676',BillingStreet='teststreet',BillingCity='testcity');
        insert acct;
        System.debug('account insertted--');
        Contact cntact= new Contact(LastName='ContactName',AccountId=acct.id, Email = 'aaa@bbb.com',FirstName='Contact FirstName',Phone='123456',T_C_MDF_Accepted__c = true);
        insert cntact;
        System.debug('con insertted--');
        acct.IsPartner = true; 
        update acct;
        Profile p = [select id from Profile where name like '%PRM%' and UserLicense.Name = 'Gold Partner' limit 1];   
		User u;
        User u1 = [select Id,DefaultCurrencyIsoCode,CurrencyIsoCode,ContactId,AccountId from User where Id=:System.UserInfo.getUserId()  limit 1 ];
        System.runas(u1)
          {
            UserRole roleId = new Userrole(PortalType='Partner',PortalAccountId =acct.Id);
            insert(roleId);
            System.debug('role---------'+roleId);
            u = new User(alias = 'standt', email='standarduser@testorg.com', emailencodingkey='UTF-8', languagelocalekey='en_US', 
                    localesidkey='en_US',timezonesidkey='America/Los_Angeles', username='standarduser2315@testorg.com', lastname = 'lname',
                    contactid= cntact.id, profileId = p.Id,UserRoleId=roleId.Id, CompanyName = acct.id);//
            insert u; 
             System.debug('users111----'+u);
          }  
         
          System.debug('users----'+u);
          PPMarketingPortalClass.testContact = [SELECT Id, Name, FirstName, LastName, Email, Phone, OwnerId, Account_Country_Code__c, Account_Street__c,Account_City__c, Account_State_Province__c, Account_Zip_Postal_Code__c, pse__Region__c FROM Contact Where Id=:u.ContactId limit 1];
          return u;
     }
     public static User retrieveUser()// retrieves partner user from system
     {
        Map<Id,Profile> ProfileMap = new Map<Id,Profile>([select id from Profile where name like '%PRM%' ]);
        User u = [select Id,DefaultCurrencyIsoCode,UserRoleId,CurrencyIsoCode,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey,ContactId,AccountId,ProfileId,LanguageLocaleKey from User where ContactId !=null and isactive=true and ProfileId in :ProfileMap.keySet()  limit 1 ];
        return u;
      }
     public static void setCustomSetting()// sets customsetting if there is no value in custom setting.
      {
        List<PPConfigSettings__c> idList = [select Id__c from PPConfigSettings__c];
        if(idList.isEmpty())
            {
                PPConfigSettings__c e = new PPConfigSettings__c(Name='optyListView',Id__c='');
                PPConfigSettings__c e1 = new PPConfigSettings__c(Name='caseListView',Id__c='');
                PPConfigSettings__c e2 = new PPConfigSettings__c(Name='custListView',Id__c='');
                PPConfigSettings__c e3 = new PPConfigSettings__c(Name='eventsListView',Id__c='');
                PPConfigSettings__c e4 = new PPConfigSettings__c(Name='NewsListView',Id__c='');
                idList.add(e);
                idList.add(e1);
                idList.add(e2);
                idList.add(e3);
                idList.add(e4);
                insert idList;
            }
           
        }
        
    public static ContentVersion setupContent() // inserts content and download history
    { 
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.VersionData = Blob.valueOf('testString');      
        contentVersion.PathOnClient = 'test.txt';
         insert contentVersion; 
        List<ContentVersionHistory> hisList = new List<ContentVersionHistory>();     
        ContentVersionHistory cvh = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');   
        ContentVersionHistory cvh1 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        ContentVersionHistory cvh2 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        ContentVersionHistory cvh3 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        hisList.add(cvh);
        hisList.add(cvh1);
        hisList.add(cvh2);
        hisList.add(cvh3);
        insert hisList; 
        System.debug('hislist---'+hisList);      
        
        return contentVersion;  
    }

 }