/*****************************************************************************************************************
 Change Log:

 20130129	TJG CR 6560 https://eu1.salesforce.com/a0CD000000NL6PY
 			Send a reminder email message on Monday if consultant has not complete timecard for the week just ended
******************************************************************************************************************/

global class sendWeeklyTimecardReminderEmail implements Schedulable{
	static final String DEBUGPRIF = '--TCTCTC-- ';
	static final string CLSNAME = 'sendWeeklyTimecardReminderEmail';
	global void execute(SchedulableContext sc) {
		EmailTemplate etemp = [SELECT id FROM EmailTemplate WHERE DeveloperName = 'Timecard_reminder' Limit 1];
		if (null == etemp) {
			System.debug(DEBUGPRIF + CLSNAME + ' Email template for timecard reminder not found.');
			return;
		}
		String filterDate = System.Now().format('yyyy-MM-dd');
		System.debug(DEBUGPRIF + CLSNAME + ' filterDate = ' + filterDate);
		
		// possible incomplete timecards
		List<pse__Timecard_Header__c> piTcs = 
		Database.query('SELECT pse__Resource__c, pse__Total_Hours__c, pse__Resource__r.pse__Work_Calendar__r.pse__Week_Total_Hours__c From pse__Timecard_Header__c  where pse__Start_Date__c <= ' +filterDate + ' and pse__End_Date__c >= ' + filterDate + ' and pse__Total_Hours__c < 40.0');
			
		// contacts with actual incomplete timecards
		List<ID> contactIds = new List<ID>();
		for(pse__Timecard_Header__c tcard : piTcs) {
			if (tcard.pse__Total_Hours__c < tcard.pse__Resource__r.pse__Work_Calendar__r.pse__Week_Total_Hours__c) {
				contactIds.add(tcard.pse__Resource__c);
			}
		}
		//contactIds.add('003D000000y8uW1IAI');
		System.debug(DEBUGPRIF + CLSNAME + ' piTcs=' + piTcs.size());
		System.debug(DEBUGPRIF + CLSNAME + ' contactIds.size() = ' + contactIds.size());
		
		List<pse__Timecard_Header__c> tcHeaders = 
		Database.query('SELECT pse__Resource__c From pse__Timecard_Header__c where pse__Start_Date__c <= ' +filterDate + ' and pse__End_Date__c >= ' + filterDate);
		
		List<ID> contactsWithTc = new List<ID>();
		
		for (pse__Timecard_Header__c tch :tcHeaders) {
			contactsWithTc.add(tch.pse__Resource__c);
		}
		//contactsWithTc.add('003D000000y8uW1IAI');
		System.debug(DEBUGPRIF + CLSNAME + ' contactsWithTc.size() = ' + contactsWithTc.size());

		// get a list of recipients
		List<Contact> consultants = [select Email, pse__Region__r.Name, Left_Company__c, pse__Exclude_From_Missing_Timecards__c, pse__Is_Resource__c, pse__Is_Resource_Active__c
			from contact
			where pse__Exclude_From_Missing_Timecards__c <> true and pse__Is_Resource__c = true and pse__Is_Resource_Active__c = true and Email <> null and Left_Company__c <> true
			and((Id in :contactIds ) OR (Id NOT in :contactsWithTc ))];
		
		if (0 >= consultants.size()) {
			System.debug(DEBUGPRIF + CLSNAME + ' no recipients found.');
			return;
		}
		
		List<Messaging.SingleEmailMessage> mailMsgs = new List<Messaging.SingleEmailMessage>();

        try { Messaging.reserveSingleEmailCapacity(consultants.size()); } // can we still send all our messages?
        catch (Exception e) {
            System.debug(DEBUGPRIF + CLSNAME + ' : unable to reserve ' + consultants.size() + ' single email messages.');
            return;
        }
        //List<string> ccAddresses = new list<string>{'louise.dugdale@qlikview.com'};
        for (Contact cntct :consultants){
        	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        	//mail.setCcAddresses(ccAddresses);
        	mail.setSaveAsActivity(false);
        	mail.setUseSignature(false);
        	mail.setSenderDisplayName('QSM Timecard Administrator');
        	mail.setTargetObjectId(cntct.id);
        	mail.setTemplateId(etemp.id);
        	mailMsgs.add(mail);
        }
        
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(mailMsgs);
        for (Messaging.SendEmailResult res : results) {
            if (!res.Success) {
                System.debug(DEBUGPRIF + CLSNAME + ' ' + res.getErrors());
            }
        }        
	}
}