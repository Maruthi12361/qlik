/****************************************************************
*
*  TestCreateOrderFormController
*
*  06.02.2017 : RVA :   changing CreateAcounts methods
*  17.03.2017 : Rodion Vakulvsokyi
*  02.09.2017 : Srinivasan PR- fix for query error
*  04.12.2017 : Linus Löfberg QCW-4421
*  06.12.2017 : Neha Gupta QCW-4421
*  30.01.2018 : Linus Löfberg Test optimizations as part of reverting QCW-4421
*  18-04-2018 : AIN BSL-10 fix
   2018-06-04 MTM BSL-490 New visual compliance implementations
*****************************************************************/

@isTest
public class TestCreateOrderFormController {

    //private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';


	//List of accounts to be processed in test methods
	public Static List<Account> accounts = new List<Account>();


    @testSetup
    public static void testSetup() {

    	//Creating custom setting
        QuoteTestHelper.createCustomSettings();

        //User
        User testUser = new User(Id = UserInfo.getUserId());

        //Test Account
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);

        //Subsidary
        Subsidiary__c testSubs = [select Id From Subsidiary__c Limit 1];

        //QlikTech company
        createQlikTech('France', 'FR', testSubs.id);
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c where Country_Name__c = 'France' limit 1];

        //Partner Record Type
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and sobjecttype='Account' ];

        //Partner Account
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
        testPartnerAccount.Territory_Country__c = 'France';
        insert  testPartnerAccount;

        insert QuoteTestHelper.createPCS(testPartnerAccount);

        //Add both accounts in one list
        accounts.add(testAccount);
        accounts.add(testPartnerAccount);

        //List of contacts
        List<Contact> contacts = new List<Contact>();

        //Partner contact
        Contact testContact2 = QuoteTestHelper.createContact(testPartnerAccount.id);

		//Contact
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        contacts.add(testContact2);
        contacts.add(testContact);

        insert contacts;

        //List of billing and shipping addresses
        List<Address__c> addresses = new List<Address__c>();
        addresses.add(new Address__c(Account__c = testPartnerAccount.Id,
							         Address_Type__c = 'Billing',
							         Valid_Address__c = true,
							         Country__c = 'France',
							         State_Province__c = 'Alaska',
							         City__c = 'test',
							         Zip__c = '99999',
							         Address_Contact__c = testContact2.Id,
							         Address_1__c = 'test address',
							         Legal_Approval_Status__c = 'Auto Approved',
							         Legal_Approval_Time__c = System.Now(),
							         Default_Address__c = true));

		addresses.add(new Address__c(Account__c = testPartnerAccount.Id,
							         Address_Type__c = 'Shipping',
							         Valid_Address__c = true,
							         Country__c = 'France',
							         State_Province__c = 'Alaska',
							         City__c = 'test',
							         Zip__c = '99999',
							         Address_Contact__c = testContact2.Id,
							         Address_1__c = 'test address',
							         Legal_Approval_Status__c = 'Auto Approved',
							         Legal_Approval_Time__c = System.Now(),
							         Default_Address__c = true));
        insert addresses;
    }


	/** This method is used to test the functionality of Direct Revenue Type**/
	private static testMethod void testCreateFormDirect() {

		//Query data from setup
        List<RecordType> rType = [Select Id From RecordType Where developerName = 'Quote'];
        List<Contact> contacts  = [Select Id From Contact ];
        List<Account> accounts = [Select Id From Account];
        List<Address__c> addresses = [Select Id From Address__c];

		 //Test start here
		 Test.startTest();

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType[0], contacts[1].id, accounts[0], '', 'Direct', 'Approved', 'Quote', false, '');
        quoteForTest.Maintenance_is_Co_Termed__c = true;
        quoteForTest.SBQQ__StartDate__c  = Date.Today().addDays(5);
        insert quoteForTest;

		//Test stop here
		Test.stopTest();

		//Constructer initilization
		ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
        CreateOrderFormController createOrderForm = new CreateOrderFormController(sc);

        //Assert for results
        Boolean result = createOrderForm.ECustomsCleared;
        System.assertEquals(true, result);

        //Calling controller method
        createOrderForm.validateMethod();

        //Calling controller method
		Pagereference pg = createOrderForm.returnToQuote();
		System.assert(pg != null);
	}

    /** This method is used to test the functionality of MSP Revenue Type**/
	private static testMethod void testCreateFormMSP() {

		//Query data from setup
        List<RecordType> rType = [Select Id From RecordType Where developerName = 'Quote'];
        List<Contact> contacts  = [Select Id From Contact ];
        List<Account> accounts = [Select Id From Account];
        List<Address__c> addresses = [Select Id From Address__c];

		 //Test start here
		 Test.startTest();

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType[0], contacts[0].id, accounts[1], '', 'Direct', 'Approved', 'Quote', false, '');
        quoteForTest.Maintenance_is_Co_Termed__c = true;
        quoteForTest.SBQQ__StartDate__c  = Date.Today().addDays(5);
        insert quoteForTest;

		//Test stop here
		Test.stopTest();

		//Constructer initilization
		ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
        CreateOrderFormController createOrderForm = new CreateOrderFormController(sc);

        //Assert for results
        Boolean result = createOrderForm.ECustomsCleared;
        System.assertEquals(true, result);

        //Calling controller method
        quoteForTest.Revenue_Type__c = 'MSP';
        createOrderForm.validateMethod();
	}

	/** This method is used to test the functionality of Reseller Revenue Type**/
	private static testMethod void testCreateFormReseller() {

		//Query data from setup
        List<RecordType> rType = [Select Id From RecordType Where developerName = 'Quote'];
        List<Contact> contacts  = [Select Id From Contact ];
        List<Account> accounts = [Select Id From Account];
        List<Address__c> addresses = [Select Id From Address__c];
        Subsidiary__c testSub = [select Id From Subsidiary__c Limit 1];

		 //Test start here
		 Test.startTest();

         Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(accounts[0], accounts[1].Id, [Select id From RecordType where DeveloperName ='Sales_QCCS']);
         oppCreatedForUpdate.CurrencyIsoCode = 'USD';
         oppCreatedForUpdate.Partner_Contact__c = contacts[0].Id;
         oppCreatedForUpdate.Revenue_Type__c = 'Reseller';
         insert oppCreatedForUpdate;

		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType[0], contacts[0].id, accounts[0], accounts[1].Id, 'Reseller', 'Approved', 'Quote', false, oppCreatedForUpdate.Id);
        quoteForTest.Maintenance_is_Co_Termed__c = true;
        quoteForTest.Quote_Recipient__c = contacts[1].Id;
        quoteForTest.SBQQ__StartDate__c  = Date.Today().addDays(5);
        insert quoteForTest;

		CreateOrderFormController.myMethod(quoteForTest.SBQQ__Account__c,quoteForTest.Revenue_Type__c,true,'Auto Approved','Auto Approved', 'No Matches', '', 'No Matches', '');

		quoteForTest.Legal_Approval_Triggered__c = true;
        quoteForTest.Sell_Through_Partner__c = accounts[1].Id;
        quoteForTest.Shipping_Address__c = addresses[1].Id;
        quoteForTest.Select_Billing_Address__c= addresses[0].Id;
        update quoteForTest;

		//Test stop here
		Test.stopTest();

		//Constructer initilization
		ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
        CreateOrderFormController createOrderForm = new CreateOrderFormController(sc);

        //Assert for results
        Boolean result = createOrderForm.ECustomsCleared;
        System.assertEquals(true, result);

        //Cpntroller emethods
        createOrderForm.updateRerunMaintanence();
        createOrderForm.getQTCustomSettingsHier();

        accounts[0].Subsidiary__c =  testSub.Id;
		update accounts[0];

		quoteForTest = [SELECT SBQQ__Primary__c, SBQQ__Status__c , Order_Form_Status__c , IS_Expired__c, SBQQ__Account__r.Subsidiary__c,
		 					 Select_Billing_Address__r.Legal_Approval_Status__c , Shipping_Address__r.Legal_Approval_Status__c, Legal_Approval_Triggered__c,
		 					 Revenue_Type__c,SBQQ__Account__c FROM SBQQ__Quote__c WHERE ID =: quoteForTest.id];

		 sc = new ApexPages.StandardController(quoteForTest);
         createOrderForm = new CreateOrderFormController(sc);

        result = createOrderForm.ECustomsCleared;
        System.assertEquals(true, result);

		quoteForTest.SBQQ__Primary__c = true;
	    quoteForTest.SBQQ__Status__c ='Approved';
        quoteForTest.Order_Form_Status__c = 'Out For Signature';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'External Review';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'Quote Approvals Needed';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'Sales Review';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'Legal Review';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'Orders Review';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'Orders Approval';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'Fully Executed';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'Canceled';
        createOrderForm.validateMethod();
        quoteForTest.Order_Form_Status__c = 'test';
        createOrderForm.validateMethod();
        quoteForTest.SBQQ__Status__c = 'Contracting';
        createOrderForm.validateMethod();

        CreateOrderFormController.PartnerAddressesApproved(accounts[1].Id, quoteForTest.Revenue_Type__c);
	}


    private static void createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr,
                                            Country_Name__c = countryName,
                                            Subsidiary__c = subsId);
        insert qlikTechIns;
    }
}