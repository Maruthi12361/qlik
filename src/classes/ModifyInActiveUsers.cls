/*
* File ModifyInActiveUsers
    * @description : create the necessary logic to work with user profile
    * @author : Maksim Karpitski
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       18.07.2018   ext_bjd             Created Class
    2       25.01.2019   ext_bjd             ITRM-301 Added logic for activation of users with license limit exceeded
  */

public class ModifyInActiveUsers {
    public static void runUserUpdatesWithLicenses() {
        Map<Id, User> inActiveUsers = new Map<Id, User>([
                SELECT Id, Name, Country, Email, Last_Profile__c, Profile.Name, IsActive, activeOnlyInSandboxes__c
                FROM User
                WHERE activeOnlyInSandboxes__c = TRUE AND IsActive = FALSE
        ]);

        if (!inActiveUsers.isEmpty()) {
            Integer amountInActiveUsersForActivation = inActiveUsers.size();

            //Define the list of profiles which are can use in order not to exceed the limit on available licenses
            List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name = 'Custom: QlikBuy Sales Std User'];

            //Define the list of users to be identified as an exceptional users
            Set<Id> exceptionalUserIds = exceptionalUserIds();

            //Define the number of available user licenses
            Integer availableLicenses = availableLicenses();

            //Set previous Profile
            ModifyInActiveUsers.setPreviousProfile(inActiveUsers.values());

            //Deactivate necessary users for get additional licenses
            if (availableLicenses < amountInActiveUsersForActivation) {
                Integer amountAdditionalLicenses = amountInActiveUsersForActivation - availableLicenses;
                List<User> notUsedUsers = identifyNotUsedUsers(inActiveUsers.keySet(), profiles, exceptionalUserIds, amountAdditionalLicenses);
                deactivateUsers(notUsedUsers);
            }

            //Activate inActive users
            ModifyInActiveUsers.activateUsers(inActiveUsers.values());

            //Change invalid emails
            ModifyInActiveUsers.modifyInvalidEmailsUsr(inActiveUsers.values());
        }
    }

    public static void modifyInvalidEmailsUsr(List<User> users) {
        List<User> userEmailList = new List<User>();
        for (User uc : users) {
            String addedPhraseInvalid = '.invalid';
            uc.Email = uc.Email.remove(addedPhraseInvalid);
            userEmailList.add(uc);
        }
        if (!userEmailList.isEmpty()) {
            Database.update(userEmailList, false);
        }
    }

    public static void activateUsers(List<User> activateUsers) {
        List<User> updateUsers = new List<User>();
        Database.SaveResult[] srList = null;
        Set<Id> idsWithErrors = new Set<Id>();
        for (User us : activateUsers) {
            if (us.Country != 'Sweden'){
                us.Country = null;
            }
            us.IsActive = true;
            updateUsers.add(us);
        }
        if (!updateUsers.isEmpty()) {
            srList = Database.update(updateUsers, false);
        }
        // Iterate through each returned result
        if (!srList.isEmpty()) {
            for (Integer i = 0; i < srList.size(); i++) {
                getDebugSaveResults(srList.get(i));
                if (!srList[i].isSuccess()) {
                    idsWithErrors.add(updateUsers[i].Id);
                }
            }
        }
    }

    public static List<User> identifyNotUsedUsers(Set<Id> activeUsersIds, List<Profile> profiles, Set<Id> excludedUserIds, Integer amountAdditionalLicenses) {
        Set<Id> profileIds = new Set<Id>();
        for (Profile prf : profiles) {
            profileIds.add(prf.Id);
        }
        List<User> notUsedUsers = new List<User>([
                SELECT Id, Name, Country, IsActive, Approval_Manager__c, Prime_User__c
                FROM User
                WHERE Id NOT IN :activeUsersIds
                AND IsActive = TRUE
                AND Id NOT IN :excludedUserIds
                AND Profile.Id IN :profileIds
                AND Prime_User__c = NULL
                AND Approval_Manager__c = NULL
                LIMIT :amountAdditionalLicenses
        ]);
        return notUsedUsers;
    }

    public static void deactivateUsers(List<User> deactivateUsers) {
        List<User> updateUsers = new List<User>();
        List<User> updateUsersWithAnotherCountry = new List<User>();
        List<User> additionalUpdateUsers = new List<User>();
        Database.SaveResult[] srList = null;
        Database.SaveResult[] srListForFailedUsersUpdate = null;
        Set<Id> idsWithErrors = new Set<Id>();
        for (User us : deactivateUsers) {
            if (us.Country != 'Sweden') {
                us.Country = null;
                updateUsersWithAnotherCountry.add(us);
            }
        }
        for (User us : deactivateUsers) {
            us.IsActive = false;
            updateUsers.add(us);
        }

        if (!updateUsersWithAnotherCountry.isEmpty()){
            Database.update(updateUsersWithAnotherCountry, false);
        }

        if (!updateUsers.isEmpty()) {
            srList = Database.update(updateUsers, false);
        }
        // Iterate through each returned result
        if (!srList.isEmpty()) {
            for (Integer i = 0; i < srList.size(); i++) {
                getDebugSaveResults(srList.get(i));
                if (!srList[i].isSuccess()) {
                    idsWithErrors.add(updateUsers[i].Id);
                }
            }
        }
        // Iterate through each failed result and update it if Country != Sweden
        if (!idsWithErrors.isEmpty()) {
            additionalUpdateUsers = updateFailedInActiveUsers(idsWithErrors);
            if (!additionalUpdateUsers.isEmpty()) {
                srListForFailedUsersUpdate = Database.update(additionalUpdateUsers, false);
                // Additional SaveResults Debug
                for (Integer i = 0; i < srListForFailedUsersUpdate.size(); i++) {
                    getDebugSaveResults(srListForFailedUsersUpdate.get(i));
                }
            }
        }
    }

    public static Set<Id> exceptionalUserIds() {
        Set<Id> userIds = new Set<Id>();
        for (Exceptional_User__mdt exceptionalUserUser : [SELECT User_Id__c FROM Exceptional_User__mdt]) {
            userIds.add(exceptionalUserUser.User_Id__c);
        }
        return userIds;
    }

    public static Integer availableLicenses() {
        UserLicense userLicenses = [SELECT TotalLicenses, UsedLicenses FROM UserLicense WHERE Name = 'Salesforce' LIMIT 1];
        Integer availableLicenses = (!Test.isRunningTest()) ? (userLicenses.TotalLicenses - userLicenses.UsedLicenses) : 0;
        return availableLicenses;
    }

    public static List<User> updateFailedInActiveUsers(Set<Id> userIds) {
        List<User> usersForUpdate = new List<User>();
        List<User> inActiveUsers = [SELECT Id, Country, IsActive FROM User WHERE Id IN :userIds];
        for (User us : inActiveUsers) {
            if (us.IsActive == true) {
                if (us.Country != 'Sweden') {
                    us.Country = null;
                }
                us.IsActive = false;
                usersForUpdate.add(us);
            }
        }
        return usersForUpdate;
    }

    public static void getDebugSaveResults(Database.SaveResult sr) {
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully update user. User ID: ' + sr.getId());
        } else if (!sr.isSuccess()) {
            System.debug('Error in update user. User ID: ' + sr.getId());
            // Operation failed, so get all errors
            for (Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('User fields that affected this error: ' + err.getFields());
            }
        }
    }

    public static void setPreviousProfile(List<User> usersWithPreviousProfiles) {
        List<User> usersForUpdate = new List<User>();
        List<String> profilesNames = new List<String>();
        for (User us : usersWithPreviousProfiles) {
            if (String.isNotBlank(us.Last_Profile__c)) {
                profilesNames.add(us.Last_Profile__c);
            }
        }
        Map<String, Id> profileNameById = new Map<String, Id>();
        List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name IN :profilesNames];
        for (Profile p : profiles) {
            profileNameById.put(p.Name, p.Id);
        }
        for (User us : usersWithPreviousProfiles) {
            if (String.isNotBlank(us.Last_Profile__c) && profileNameById.containsKey(us.Last_Profile__c)) {
                us.ProfileId = profileNameById.get(us.Last_Profile__c);
                usersForUpdate.add(us);
            }
        }
        if (!usersForUpdate.isEmpty()) {
            Database.update(usersForUpdate, false);
        }
    }
}