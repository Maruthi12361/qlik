/****************************************************************
*
*  SearchProductsTest 
*
*  06.02.2017   RVA :   changing CreateAcounts methods
*  29.03.2017 : Roman Dovbush  : Added QTTestUtils.GlobalSetUp() to create custom settings; createCustomSettings() commented out as duplicate
*****************************************************************/
@isTest
private class SearchProductsTest {
	static final String AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id; //'01220000000DOFu';
	@isTest static void testSearchProducts() {
		QTTestUtils.GlobalSetUp();
	/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id			
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account acc = new Account();
		acc.Name = 'Test';
		// Needs a Billing and Shippin Addr
		acc.BillingCity = 'London';
		acc.BillingCountry ='United States';
		acc.BillingState = 'State';
		acc.BillingStreet = 'Street';
		acc.BillingPostalCode = 'pc';
		
		acc.ShippingCity = 'London';
		acc.ShippingCountry ='United States';
		acc.ShippingState = 'State';
		acc.ShippingStreet = 'Street';
		acc.ShippingPostalCode = 'pc';
		
		acc.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
		acc.Billing_Country_Code__c = QTComp.Id;
		insert acc;
	*/
		User userForTest =  [select id From User where Id=:UserInfo.getUserId()];
		Account  acc = QTTestUtils.createMockAccount('TestCompany', userForTest, true);
	        acc.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update acc;
		QTCustomSettings__c Settings = new QTCustomSettings__c();
        Settings.Name = 'Default';
        Settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        Settings.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        Settings.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        Settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert Settings;
        //QuoteTestHelper.createCustomSettings(); // duplicate - we call it from QTTestUtils.GlobalSetUp();
		//PriceBook2 priceBook = [Select Name, IsStandard, IsDeleted, IsActive, Id From Pricebook2 p where Name = 'QlikView 9' LIMIT 1];
		PriceBook2 priceBook =new PriceBook2 ();
		pricebook.Name='QlikView 9';
		insert pricebook;
		Opportunity opp = new Opportunity();
		opp.AccountId = acc.Id;
		opp.StageName = 'Alignment Meeting';
		opp.Name = 'test';
		opp.CloseDate = System.today().addDays(7);
		opp.Pricebook2Id = pricebook.Id;
		insert opp;
		
		Test.setCurrentPage(new PageReference('/apex/SearchProducts'));
		ApexPages.currentPage().getParameters().put('id',opp.Id);
		
		SearchProducts sPageController = new SearchProducts();
		
		sPageController.setOpportunityName('New Name');
		sPageController.getOpportunityName();
		
		sPageController.getPricebookOptions();
		
		sPageController.setPriceBook(priceBook.Id);
		sPageController.getPriceBook();
		
		sPageController.getProductFamily();
		
		sPageController.setProductFamily('Server');
		
		sPageController.getSelectedPricebookEntries();
		
		sPageController.searchProducts();

		sPageController.getOliNav();
		sPageController.setOliNav(''); 
	
		sPageController.selectProducts();
		
		sPageController.getLicenseColumnName();
		
		sPageController.cancelSelection();
		sPageController.getTaskLst();
		
		sPageController.getAllAccountLicenses();
		sPageController.getAccountLookupLicenses();
		sPageController.accountHasLookupLicenses();
	
		sPageController.setOliUIs(null);
		sPageController.getOliUIs();
		
		sPageController.saveProducts();
		sPageController.saveAndMoreProducts();
		
		sPageController.cancelSave();
		
		sPageController.ResetVariables();
		
		sPageController.getRenderNoPricebookMessage();
	}
	
	@isTest static void testSearchProducts2() {
		QTTestUtils.GlobalSetUp();
	/*
		Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = sub.id			
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
		Account acc = new Account();
		acc.Name = 'Test';
		// Needs a Billing and Shippin Addr
		acc.BillingCity = 'London';
		acc.BillingCountry ='United States';
		acc.BillingState = 'State';
		acc.BillingStreet = 'Street';
		acc.BillingPostalCode = 'pc';
		
		acc.ShippingCity = 'London';
		acc.ShippingCountry ='United States';
		acc.ShippingState = 'State';
		acc.ShippingStreet = 'Street';
		acc.ShippingPostalCode = 'pc';
		
		acc.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
		acc.Billing_Country_Code__c = QTComp.Id;
		insert acc;
	*/
		// Need to add a License Lookup
		User userForTest =  [select id From User where Id=:UserInfo.getUserId()];
		Account  acc = QTTestUtils.createMockAccount('TestCompany', userForTest, true);
	        acc.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update acc;
		Account_License__c accLic = new Account_License__c();
		accLic.Name = 'Lookup';
		accLic.Lookup_License__c = true;
		accLic.Account__c = acc.Id;
		
		insert accLic;
		
		PriceBook2 priceBook =new PriceBook2 ();
		pricebook.Name='QlikView 9';
		pricebook.isActive=true;
		insert pricebook;
		
		QTCustomSettings__c Settings = new QTCustomSettings__c();
        Settings.Name = 'Default';
        Settings.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
        Settings.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        Settings.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        Settings.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert Settings;
        //QuoteTestHelper.createCustomSettings(); // duplicate - we call it from QTTestUtils.GlobalSetUp();
        Test.startTest();
		Opportunity opp = new Opportunity();
		opp.AccountId = acc.Id;
		opp.StageName = 'Alignment Meeting';
		opp.Name = 'test';
		opp.CloseDate = System.today().addDays(7);
		opp.Pricebook2Id = Test.getStandardPricebookId();
		//opp.CurrencyIsoCode = 'EUR';
		insert opp;
		
		// Product2 prod = [Select Id from Product2 where IsActive = true and Navision_Type]
		//PricebookEntry pbe = [select id,UnitPrice,Name,ProductCode,Product2Id from PricebookEntry where Pricebook2Id = :pricebook.Id and Product2.Navision_Type__c = 'Product' and CurrencyIsoCode = 'EUR'  AND isActive = true LIMIT 1];
		PricebookEntry pbe =new PricebookEntry ();
		pbe.UnitPrice=10000;
		pbe.Pricebook2Id=Test.getStandardPricebookId();
		pbe.UseStandardPrice  =false;
		pbe.isActive=true;
		/*
		Product2 prodid=new Product2();
		prodid.Name='test';
		prodid.Deferred_Revenue_Account__c = '26000 Deferred Revenue';
		prodid.Income_Account__c = '26000 Deferred Revenue';
		insert prodid;
		*/
		Product2 pr = TestQuoteUtil.createMockProduct('QlikView Enterprise Edition Server', 'Licenses', 0 , false);
		//	Product2 pr1 = TestQuoteUtil.createMockProduct('Basic', 'Maintenance', 20 , false);
	    //    Product2 pr2 = TestQuoteUtil.createMockProduct('Enterprise', 'Maintenance', 23 , false);
			insert pr;
		pbe.Product2Id=pr.Id;
		insert pbe;
	
		OpportunityLineItem oli = new OpportunityLineItem();
		oli.OpportunityId = opp.Id;
		oli.Quantity = 1.0;
		oli.PricebookEntryId = pbe.Id;
		oli.TotalPrice = 99;
		insert oli;
		Test.stopTest();
		Test.setCurrentPage(new PageReference('/apex/SearchProducts'));
		ApexPages.currentPage().getParameters().put('id',opp.Id);
		
		SearchProducts sPageController = new SearchProducts();
		sPageController.getSelectedPricebookEntries();
		
		
		sPageController.getAllAccountLicenses();
		sPageController.getAccountLookupLicenses();
		sPageController.accountHasLookupLicenses();
		sPageController.getTaskLst();
		
		
		List<PricebookEntryUIElement> priceBookEntriesUiElms = new List<PricebookEntryUIElement>();
		
		PricebookEntryUIElement pricebookEntryUIElement = new PricebookEntryUIElement();
        pricebookEntryUIElement.setPbeIsSelected(true); 
		pricebookEntryUIElement.setPbe(pbe);
		priceBookEntriesUiElms.add(pricebookEntryUIElement);
		
		sPageController.selectedPricebookEntries = priceBookEntriesUiElms; 
		
		sPageController.getOliUIs();
		sPageController.getLicenseColumnName();
		sPageController.saveProducts();
		
		List<OpportunityLineItemUIElement> olliUIElms = new List<OpportunityLineItemUIElement>();
		OpportunityLineItem oliTest = new OpportunityLineItem();
		oliTest.Quantity = -1.0;
		
		OpportunityLineItemUIElement olUiElem = new OpportunityLineItemUIElement();
		olUiElem.setOli(oliTest);
		olliUIElms.add(olUiElem);
				
		sPageController.setOliUIs(olliUIElms);
		sPageController.saveProducts();
		
		oliTest.Quantity = 1.0;
		oliTest.UnitPrice = null;
		olUiElem.setOli(oliTest);
		olliUIElms.clear();
		olliUIElms.add(olUiElem);
		sPageController.saveProducts();
		sPageController.saveAndMoreProducts();
		
		
		 	//Following code added by MHG 100112 to get acceptable code coverage
		 
		olUiElem.setListPrice('Not used');
		olUiElem.setCurrCode('USD');		
		System.assertEquals(olUiElem.getCurrencyCode(), 'USD');
		olUiElem.setOliProductName('QlikView 2000');
		System.assertEquals(olUiElem.getOliProductName(), 'QlikView 2000');
		olUiElem.setOliProductCode('QlikView 2002');
		System.assertEquals(olUiElem.getOliProductCode(), 'QlikView 2002');		
		olUiElem.setOliNav('QlikView 2004');
		System.assertEquals(olUiElem.getOliNav(), 'QlikView 2004');		
		System.debug(olUiElem.getListPrice());
	}
	
	
	
}