/* 
Purpose: Partner Expertise test setup
Created Date: 4 Jan 2016
Initial delvelpment 4 Jan 2016
Change log:
 **/
public class EP_TestDataSetup {

    public Static Solution_Profiles__c CreateSolutionProfile(String name, Id accountId, Id expertise)
    {
        Solution_Profiles__c solProf = new Solution_Profiles__c();
        solProf.Partner_Expertise_Area_ID__c = expertise;
        solProf.Account_Name__c = accountId;
        solProf.Status__c = 'In Process';
        solProf.Name = name;
        insert solProf;
        return solProf;
    }
    
    public Static Partner_Expertise_Area__c CreatExpertiseArea(Id accountId, String sector, Contact dedicatedContact)
    {
        Partner_Expertise_Area__c PartnerExpertiseArea = new Partner_Expertise_Area__c(            
            Partner_Account_Name__c = accountId,
            Expertise_Area__c = sector,
            Dedicated_Expertise_Contact__c = dedicatedContact.Id,
            Qlik_Market_App_Status__c = 'Not Assigned',
            Expertise_Designation_Status__c = 'In Process',
            Expertise_Application_Eligibility__c = 'Under Review');
        insert PartnerExpertiseArea;
        return PartnerExpertiseArea;
    }
    
    public Static Partner_Expertise_Application_Lookup__c  CreatExpertiseLookup(Id accountId, Id expertiseId)
    {
        Partner_Expertise_Application_Lookup__c lookupObject = new Partner_Expertise_Application_Lookup__c(
        Account_Name__c = accountId,
        Expertise_Area__c = expertiseId);
        insert lookupObject;
        return lookupObject;
    }
}