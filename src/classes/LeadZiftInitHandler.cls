/*********************************************
 Class: LeadZiftInitHandler

	Description:
	- Add latest campaign interaction from CampaignMembers, Lead need to have responded to campaign
	- Reset fields when Lead Owner is changed to Zift API User

	Log History:
	2017-11-08    BAD    Created - CHG0032328/BMW-408
 
**********************************************/
public class LeadZiftInitHandler {

	public LeadZiftInitHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, Map<Id, Lead> triggerOldMap){
		
		System.debug('Start Zift Init handler');
		Set<Id> leadIDs = new Set<Id>();
		Map<String,String> leadToCampMemberMap = new Map<String,String>{};
		Map<Id,CampaignMember> campMembersMap = new Map<Id,CampaignMember>{};

        for (Integer i = 0; i < triggerNew.size(); i++){
			if(triggerNew[i].Lead_Owner_name__c == 'Zift API User' && triggerNew[i].Lead_Owner_name__c != triggerOldMap.get(triggerNew[i].id).Lead_Owner_name__c){
				leadIDs.Add(triggerNew[i].Id);
			}
		}

		for (Lead lead : [select id, (select Id, Campaign_Name__c, Campaign.Type, Campaign.Campaign_Sub_Type__c from CampaignMembers where HasResponded = true order by FirstRespondedDate DESC Limit 1) from Lead where Id in :leadIDs]){
    		if(lead.CampaignMembers.size() > 0){
        		CampaignMember camp = lead.CampaignMembers;
    			leadToCampMemberMap.put(lead.Id, camp.Id);
				campMembersMap.put(camp.Id, camp);
				System.debug('Found campaign: ' + camp.Campaign_Name__c);
			}
		}
	
	    for(Lead lead : triggerNew) {
	      	if(lead.Lead_Owner_name__c == 'Zift API User' && lead.Lead_Owner_name__c != triggerOldMap.get(lead.id).Lead_Owner_name__c){
	      		
	      		CampaignMember camp = campMembersMap.get(leadToCampMemberMap.get(lead.Id));
	      		if(camp != null){
					lead.Latest_Campaign_Interaction__c = camp.Campaign_Name__c == null ? '' : camp.Campaign_Name__c ;
					lead.Latest_Campaign_Type__c = camp.Campaign.Type == null ? '' : camp.Campaign.Type;
	                lead.Latest_Campaign_Sub_Type__c = camp.Campaign.Campaign_Sub_Type__c == null ? '' : camp.Campaign.Campaign_Sub_Type__c;
	      		}
        		lead.Zift_Lead_Partner__c = null;
        		lead.Zift_Distribution_Status__c = 'Send to Zift';
        		lead.Zift_Lead_Status__c = null;
        		lead.Zift_Rejection_Reason__c = null;
        		lead.Zift_Unqualified_Invalid_Reason__c = null;
        		lead.Zift_Sales_Rep_Email__c = null;
        		lead.Zift_Sales_Rep_Name__c = null;
        		lead.Zift_Registered_Lead__c = false;
        		lead.Zift_SSO_CompanyID__c = null;
        		lead.Zift_Partner_Id__c = null;
        		lead.Zift_Partner_Name__c = null;
	      	}
	    }

	}

}