/******************************************************
Name:  Kumar Navneet 

Purpose:
-------
    Updating currency
======================================================

   Date : 2017/04/20   

******************************************************/

public with sharing class InfuencingPartnerHandler {
  

	public InfuencingPartnerHandler () {    
	}

  	public static Boolean Inserthandler(List<Influencing_Partner__c> triggerNew) { 
  
		Map<ID, String> OppMap = new Map<ID, String>();      
		Set<Id> InfluenceSet = new Set<Id>();     
        
		for (Influencing_Partner__c Ipc : triggerNew){
			if(String.isNotBlank(Ipc.Opportunity__c))
				InfluenceSet.add(Ipc.Opportunity__c);         
		}
     
		for(Opportunity opp : [Select Id, CurrencyIsoCode FROM Opportunity WHERE Id IN:InfluenceSet ]){
			OppMap.put(opp.Id,opp.CurrencyIsoCode);
		}   
      
		for (Influencing_Partner__c Ipc : triggerNew){
			if(OppMap.containsKey(Ipc.Opportunity__c)){
				Ipc.currencyisocode = OppMap.get(Ipc.Opportunity__c);
			}
		}     
        
      	return true;
	}
     
     
  /*  public static Boolean Updatehandler(List<Influencing_Partner__c> triggerNew , Map<Id, Influencing_Partner__c> triggerNewMap ,  Map<Id, Influencing_Partner__c> triggerOldMap) {        
      
        Map<ID, String> OppMap = new Map<ID, String>();      
        Set<Id> InfluenceSet = new Set<Id>();     
        
        for (Influencing_Partner__c Ipc : triggerNew)
      {
         InfluenceSet.add(Ipc.Opportunity__c);         
      }
     
      for(Opportunity opp : [Select Id, CurrencyIsoCode FROM Opportunity WHERE Id IN:InfluenceSet ]){
         OppMap.put(opp.Id,opp.CurrencyIsoCode);
       }   
      
       for (Influencing_Partner__c Ipc : triggerNew)
       {
        if (triggerNewMap.get(Ipc.Id).currencyisocode != triggerOldMap.get(Ipc.Id).currencyisocode){
          if(OppMap.containsKey(Ipc.Opportunity__c))
          {
           Ipc.currencyisocode = OppMap.get(Ipc.Opportunity__c);
          }
         }
      
       }     
        
       
      return true;
     }
     */
}