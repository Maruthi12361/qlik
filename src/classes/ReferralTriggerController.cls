public class ReferralTriggerController {
    
    public static void validateOpportunityAssociationBeforeApproval(List<Referral__c> refs, Map<Id, Referral__c> oldMap){
        Map<Id, Referral__c> approvedReferrals = new Map<Id, Referral__c>();
        for(Referral__c ref: refs){
            Referral__c oldRef = oldMap.get(ref.Id);
            if (oldRef.Referral_Status__c == Label.Status_Pending && ref.Referral_Status__c == Label.Status_Accepted && ref.Opportunity__c == null){ 
                    approvedReferrals.put(ref.Id, ref);
                }
        }
        for (ProcessInstance pi : [SELECT TargetObjectId, 
                                   (  
                                       SELECT Id, StepStatus, Comments 
                                       FROM Steps
                                       WHERE StepStatus = :Label.Status_Approved
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1 
                                   )
                                   FROM ProcessInstance
                                   WHERE TargetObjectId In 
                                   :approvedReferrals.keySet()
                                   ORDER BY CreatedDate DESC
                                  ]) {     
            if (pi.Steps.size()>0){  approvedReferrals.get(pi.TargetObjectId).addError(Label.Must_Associate_Opportunity);}
        }
        
    }
    
    public static void validateRejectionReasonForRejectedReferral(List<Referral__c> refs, Map<Id, Referral__c> oldMap){
        Map<Id, Referral__c> rejectedReferrals = new Map<Id, Referral__c>();
        for(Referral__c ref: refs){
            Referral__c oldRef = oldMap.get(ref.Id);
            if (oldRef.Referral_Status__c == Label.Status_Pending && ref.Referral_Status__c == Label.Status_Rejected && ref.Rejection_Reason__c == null){ 
                    rejectedReferrals.put(ref.Id, ref);
                }
        }
        for (ProcessInstance pi : [SELECT TargetObjectId, 
                                   (  
                                       SELECT Id, StepStatus, Comments 
                                       FROM Steps
                                       WHERE StepStatus = :Label.Status_Rejected
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1 
                                   )
                                   FROM ProcessInstance
                                   WHERE TargetObjectId In 
                                   :rejectedReferrals.keySet()
                                   ORDER BY CreatedDate DESC
                                  ]) {     
            if (pi.Steps.size()>0){  rejectedReferrals.get(pi.TargetObjectId).addError(Label.Must_Provide_Rejected_Reason);}
        }
        
    }
}