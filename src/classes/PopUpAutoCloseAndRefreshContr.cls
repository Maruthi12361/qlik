public class PopUpAutoCloseAndRefreshContr{
	public static final String RESTYPE = 'Reseller';
	public static final String FULTYPE = 'Fulfillment';
	public static final String DISTYPE = 'Distributor';
	public static final String NFRTYPE = 'Not for Resale';
	public static final String PARTTYPE = 'Partner Purchase';
	public static final String DIRECTTYPE = 'Direct';
	public static final String SEECOMMTYPE = 'See Comment';
	public static final String OEMTYPE = 'OEM';
	public static final String DIRQSGTYPE = 'Direct QSG';
	public static final String RESQSGTYPE = 'Reseller QSG';
	public static final String ACPROGRAMTYPE = 'Academic Program';
	public static final String TERMSMESSAGE = 'Do you accept the terms and conditions?';
	public static final String PARTMANAGERMESSAGE = 'Order has special terms.Partner manager must send  order form to sign.';
	public static final String ORDPLACEMESSAGE = 'Order is placed.';
	public static final String REVENUEMESSAGE = 'Not applicable for this Revenue Type.';
	public static final String QUOTESTATUSMESSAGE = 'The order is already placed or Quote status is not "Open" or "Approved".';
	public static final String ORDERFORMDRAFTMESSAGE = 'Order Forms can only be drafted for primary quotes.';
	public static final String ORDERNOTAPPROVEMESSAGE = 'Order Forms can only be drafted for Approved Quotes.';	
	public static final String STARTLAUNCHEEMESSAGE1 = 'Please start QSG document launcher';	
	public static final String STARTLAUNCHEEMESSAGE2 = 'Please start Academic Program document launcher';	
	public static final String STARTLAUNCHEEMESSAGE3 = 'Please start Direct Order Form document launcher';	
	public static final String STARTLAUNCHEEMESSAGE4 = 'Please start Partner Order Form document launcher';	
	public static final String STATUSOPEN = 'Open';
	public static final String STATUSAPPROVED = 'Approved';
	public static final String STATUSORDPLACED = 'Order Placed';
	public static final String PLACEORDERBUTT = 'Place Order';
	public static final String CREATEORDERBUTT = 'Create Order';

    public String message {get;set;}
    public String checkParam {get;set;}
	public String buttonType{get;set;}
	public String quoteId{get;set;}
	public SBQQ__Quote__c quote{get;set;}
	public Boolean showDefaultMessage{get;set;}
	public Boolean showPlaceOrderMessage{get;set;}
	public String link{get;set;}
	public Boolean docLauncherButton{get;set;}


	/* public PopUpAutoCloseAndRefreshContr(ApexPages.StandardController stdController) {
        quoteId = stdController.getRecord().id;
		    }*/

    public PopUpAutoCloseAndRefreshContr(){
	    quoteId = Apexpages.currentPage().getParameters().get('id');
		buttonType = Apexpages.currentPage().getParameters().get('type');
		quote = [Select id, Name, SBQQ__Status__c, SBQQ__Primary__c, SBQQ__Account__c, SBQQ__Opportunity2__c, Revenue_Type__c, Sell_Through_Partner__c, Partner_Special_Term_Request__c From SBQQ__Quote__c where id =:quoteId];

	}
    public void checkingWindowVisibility() {
		if(buttonType == PLACEORDERBUTT) {
			createPlaceOrderVisibility();
		}
		if(buttonType == CREATEORDERBUTT) {
			createOrderFormVisibility();
		}
   }

   public Pagereference updateStatus(){
		quote.SBQQ__Status__c = STATUSORDPLACED;
		if(!Test.isRunningTest()) {
			update quote;
		}
		return null;
   }

   private void createOrderFormVisibility() {
		if(!quote.SBQQ__Primary__c) {
			message = ORDERFORMDRAFTMESSAGE;
			showDefaultMessage = true;
		}
		if(quote.SBQQ__Primary__c) {
			if(quote.SBQQ__Status__c != STATUSAPPROVED) {
				message = ORDERNOTAPPROVEMESSAGE;
				showDefaultMessage = true;
			}
			if(quote.SBQQ__Status__c == STATUSAPPROVED) {
				if(quote.Revenue_Type__c == DIRQSGTYPE || quote.Revenue_Type__c == RESQSGTYPE) {
					link = 'https://uatna11.springcm.com/atlas/doclauncher/eos/Create%20Order%20Form?aid=8154&eos[0].Id='+quote.id+'&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name=Q-00619&eos[0].ScmPath=/Salesforce/Accounts/'+quote.SBQQ__Account__c+'/Opportunities/'+quote.SBQQ__Opportunity2__c+'/Quotes';
					message = STARTLAUNCHEEMESSAGE1;
					docLauncherButton = true;	
				} else if(quote.Revenue_Type__c == ACPROGRAMTYPE) {
					link = 'https://uatna11.springcm.com/atlas/doclauncher/eos/Create%20Order%20Form?aid=8154&eos[0].Id='+quote.id+'&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name=Q-00619&eos[0].ScmPath=/Salesforce/Accounts/'+quote.SBQQ__Account__c+'/Opportunities/'+quote.SBQQ__Opportunity2__c+'/Quotes';
					message = STARTLAUNCHEEMESSAGE2;
					docLauncherButton = true;
				} else if(quote.Revenue_Type__c == DIRECTTYPE) {
					link = 'https://uatna11.springcm.com/atlas/doclauncher/eos/Create%20Order%20Form?aid=8154&eos[0].Id='+quote.id+'&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name=Q-00619&eos[0].ScmPath=/Salesforce/Accounts/'+quote.SBQQ__Account__c+'/Opportunities/'+quote.SBQQ__Opportunity2__c+'/Quotes';
					message = STARTLAUNCHEEMESSAGE3;
					docLauncherButton = true;
				}
			}
		}
   }

   private void createPlaceOrderVisibility() {
	if(quote.Revenue_Type__c == RESTYPE || quote.Revenue_Type__c == FULTYPE || quote.Revenue_Type__c == DISTYPE || quote.Revenue_Type__c == NFRTYPE || quote.Revenue_Type__c == PARTTYPE || quote.Revenue_Type__c.contains(OEMTYPE)) {
		if(!String.isEmpty(quote.Sell_Through_Partner__c) && quote.SBQQ__Status__c == STATUSAPPROVED) {
			if(quote.Partner_Special_Term_Request__c == false) {
				showPlaceOrderMessage = true;
				message = TERMSMESSAGE;
			}
        if(quote.Partner_Special_Term_Request__c == true) {
			showDefaultMessage = true;
			message = PARTMANAGERMESSAGE;
        }
     } else {
        if(quote.SBQQ__Status__c == STATUSOPEN || quote.SBQQ__Status__c == STATUSAPPROVED) {
				updateStatus();
			showDefaultMessage = true;
			message = ORDPLACEMESSAGE;
        } else {
			showDefaultMessage = true;
			message = QUOTESTATUSMESSAGE;
        }
      }
    } else {
		showDefaultMessage = true;
        message = REVENUEMESSAGE;
    }
   }

   /*
   private void createPlaceOrderVisibility() {
	if(quote.Revenue_Type__c == RESTYPE || quote.Revenue_Type__c == FULTYPE || quote.Revenue_Type__c == DISTYPE || quote.Revenue_Type__c == NFRTYPE || quote.Revenue_Type__c == PARTTYPE || quote.Revenue_Type__c.contains(OEMTYPE)) {
		if(!String.isEmpty(quote.Sell_Through_Partner__c) && quote.SBQQ__Status__c == STATUSOPEN || quote.SBQQ__Status__c == STATUSAPPROVED) {
			if(quote.Partner_Special_Term_Request__c == false) {
				showPlaceOrderMessage = true;
				message = TERMSMESSAGE;
			}
        if(quote.Partner_Special_Term_Request__c == true) {
			showDefaultMessage = true;
			message = PARTMANAGERMESSAGE;
        }
     } else {
        if(quote.SBQQ__Status__c == STATUSOPEN || quote.SBQQ__Status__c == STATUSAPPROVED) {
				updateStatus();
			showDefaultMessage = true;
			message = ORDPLACEMESSAGE;
        } else {
			showDefaultMessage = true;
			message = QUOTESTATUSMESSAGE;
        }
      }
    } else if(quote.Revenue_Type__c == SEECOMMTYPE){
		showDefaultMessage = true;
        message = REVENUEMESSAGE;
    }
   }
   */
}