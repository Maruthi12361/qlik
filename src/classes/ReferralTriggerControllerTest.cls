@IsTest
public class ReferralTriggerControllerTest {
    
    public static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
    public static Schema.DescribeSObjectResult accDescribe= gd.get('Account').getDescribe();
    public static Schema.DescribeSObjectResult oppDescribe= gd.get('Opportunity').getDescribe();
    
    public static String END_USER_ACCOUNT_RT = accDescribe.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
    public static String PARTNER_ACCOUNT_RT =  accDescribe.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
    public static String SALES_QCCS_OPP_RT = oppDescribe.getRecordTypeInfosByName().get('Qlikbuy CCS Standard').getRecordTypeId();
    
    @TestSetup
    static void createTestData() {
        
        /* Set up partner user - START*/
        Id p = [SELECT Id FROM Profile WHERE Name='PRM - Sales Dependent Territory + QlikBuy'].Id;
        Id apId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        
        Account acc = new Account(name ='Test Partner Account', RecordTypeId=PARTNER_ACCOUNT_RT) ;
        /* Test End User Account - START */
        
        insert acc;
            
            /* Test End User Account - END */
            
            Contact con = new Contact(LastName ='Test Partner Contact',AccountId = acc.Id,
                                     SFDCAccessGranted__c='LeadsOppsQuotes');
        
        insert con;  
        
        User partner = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='atester123@noemail.com');
        
         User accountManager = new User(alias = 'mgr123', email='manager123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = apId, IsActive =true,
                             timezonesidkey='America/Los_Angeles', username='adminer123@noemail.com');
        
        insert new List<User>{partner,accountManager};
            
        Account eua1 = new Account(name ='Test EUA1', RecordTypeId=END_USER_ACCOUNT_RT, OwnerId=accountManager.Id) ;
        insert eua1;
        
        // Share the End User Account and Account Manager user with the partner, without it partner can not create Referral
        insert new AccountShare(AccountId = eua1.Id, AccountAccessLevel='Read',RowCause='Manual',
                                UserOrGroupId=partner.Id, OpportunityAccessLevel='Read',
                               CaseAccessLevel='Read', ContactAccessLevel='Read');
        insert new UserShare(UserId = accountManager.Id, UserAccessLevel='Read',RowCause='Manual',
                                UserOrGroupId=partner.Id);
        
    }
    
    @isTest
    public static void testReferralOpportunityAssociation(){

        Test.startTest();
        User partner = [SELECT Id, Name,alias FROM User WHERE alias='test123' LIMIT 1];
        User accountManager = [SELECT Id, Name,alias FROM User WHERE alias='mgr123' LIMIT 1];
        Account ptAcc = [SELECT Id FROM Account WHERE name ='Test Partner Account' LIMIT 1];
        Account euAcc = [SELECT Id FROM Account WHERE name ='Test EUA1' LIMIT 1];
        Approval.ProcessResult result ;   
        Referral__c ref = new Referral__c(End_User_Account__c= euAcc.Id,Referring_Partner_Account__c= ptAcc.Id,
                                              End_User_Contact_Email__c='test@noemail.com', End_User_Contact_Name__c='test contact',
                                              OwnerId= partner.Id, Accepted_Terms_and_Conditions__c=true,
                                              //Partner_Manager__c= accountManager.Id,
                                              Referral_Status__c = 'Draft');
        insert ref;

        ref.Referral_Status__c='Pending';
        update ref;
        ref.Referral_Status__c='Accepted';
        try{
            update ref;
        } catch(Exception ex){}
      	
        ref.Referral_Status__c='Rejected';
        try{
            update ref;
        } catch(Exception ex){}
        Test.stopTest();
    }

}