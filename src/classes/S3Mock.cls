/*****************************************************************************************
* 2015-09-28 AIN Webservice Mock for class S3, using WebServiceMockDispatcher is prefered, see
* that class for details.
****************************************************************************************/
@isTest
public class S3Mock implements WebServiceMock {

  public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    system.debug('S3Mock Start');
    if(request instanceof S3.DeleteObject_element)
    {
    	system.debug('instanceof S3.DeleteObject_element');
      	response.put('response_x', new S3.DeleteObjectResponse_element());

    }
    else if(request instanceof S3.ListBucket_element)
    {
    	system.debug('instanceof S3.ListBucket_element');
      	response.put('response_x', new S3.ListBucketResponse_element());
    }
    else if(request instanceof S3.ListAllMyBuckets_element)
    {
    	  system.debug('instanceof S3.ListAllMyBuckets_element');
        S3.ListAllMyBucketsResponse_element responseElement = new S3.ListAllMyBucketsResponse_element();
        S3.ListAllMyBucketsResult bucketResult = new S3.ListAllMyBucketsResult();
        S3.CanonicalUser canonicalUser = new S3.CanonicalUser();
        canonicalUser.ID = 'Test';
        bucketResult.Owner = canonicalUser;
        responseElement.ListAllMyBucketsResponse = bucketResult;

      	response.put('response_x', responseElement);
    }
    else if(request instanceof S3.CreateBucket_element)
    {
    	 system.debug('instanceof S3.CreateBucket_element');
        response.put('response_x', new S3.CreateBucketResponse_element());
    }
    else if(request instanceof S3.DeleteBucket_element)
    {
    	  system.debug('instanceof S3.DeleteBucket_element');
        response.put('response_x', new S3.DeleteBucketResponse_element());
    }
    else if(request instanceof S3.PutObjectInline_element)
    {
        system.debug('instanceof S3.PutObjectInline_element');
        response.put('response_x', new S3.PutObjectInlineResponse_element()); 
    }

    system.debug('S3Mock End');
       
  }
}