/**************************************************
* CR# 19391 – Add in Counters to Calculate Days and Date Stamp Entry Point into Waterfall Stage
* Change Log:
* 2015-02-26 Madhav Kakani: Test case for CampaignMemberResponsed_Trigger
* 2015-08-05 Linus Löfberg @ 4front - CR# 32086 - Add Field to Campaign Member Table - Retreive and store lead/contact stus on campaign response.
*
**************************************************/
@isTest
private class TestCampaignMemberResponsed_Trigger {
    
    static testmethod void testCampaignMemberResponsed_Trigger() {
        Campaign camp = new Campaign(); // Create a sample campaign
        camp.Name = 'Test Campaign';
        camp.Publishable_Name__c = 'Test Campaign';
        camp.IsActive = true;
        camp.Type = 'DM - Direct Marketing';
        camp.Campaign_Sub_Type__c = 'DM - Email Campaign';
        camp.LOB__c = 'Field Marketing';
        camp.Campaign_Objective__c = 'Brand Awareness';
        //camp.Campaign_Audience__c = 'Prospects';
        camp.QlikTech_Company__c = 'QlikTech Inc';
        camp.StartDate = Date.today();
        camp.EndDate = Date.today() + 30;
        camp.Planned_Target_Audience__c = 100;
        camp.Planned_Leads__c = 100;
        camp.Planned_Opportunities__c = 10;
        camp.Planned_Opportunity_Value__c = 100000;
        camp.Campaign_Sector__c = 'Cross Sector';
        camp.Campaign_Job_Function__c = 'Executive';
        camp.Data_Source_Category__c = 'Microsoft';
        camp.Partner_Involvement__c = 'No Partner';
        insert camp;
        system.assert(camp.Id != null);

        Campaign camp2 = new Campaign(); // Create a sample campaign for updates
        camp2.Name = 'Test Campaign 2';
        camp2.Publishable_Name__c = 'Test Campaign 2';
        camp2.IsActive = true;
        camp2.Type = 'DM - Direct Marketing';
        camp2.Campaign_Sub_Type__c = 'DM - Email Campaign';
        camp2.LOB__c = 'Field Marketing';
        camp2.Campaign_Objective__c = 'Brand Awareness';
        //camp2.Campaign_Audience__c = 'Prospects';
        camp2.QlikTech_Company__c = 'QlikTech Inc';
        camp2.StartDate = Date.today();
        camp2.EndDate = Date.today() + 30;
        camp2.Planned_Target_Audience__c = 100;
        camp2.Planned_Leads__c = 100;
        camp2.Planned_Opportunities__c = 10;
        camp2.Planned_Opportunity_Value__c = 100000;
        camp2.Campaign_Sector__c = 'Cross Sector';
        camp2.Campaign_Job_Function__c = 'Executive';
        camp2.Data_Source_Category__c = 'Microsoft';
        camp2.Partner_Involvement__c = 'No Partner';
        insert camp2;
        system.assert(camp2.Id != null);
		//Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        //QlikTech_Company__c QTComp = new QlikTech_Company__c(Name = 'USA', Country_Name__c = 'USA', QlikTech_Company_Name__c = 'QlikTech Inc', Subsidiary__c = testSubs1.id);
        //insert QTComp;
        QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert setting; 

        Lead ld = new Lead();
        ld.Firstname = 'Test';
        ld.Lastname = 'Lead';
        ld.Company = 'Test Company';
        ld.LeadSource = 'Qlikmarket';
        ld.Country = 'USA';
        ld.Email = 'test@gmail.com';
        ld.Status = 'TEST';
        insert ld;
        system.assert(ld.Id != null);

        Account newAccount = new Account(Name='Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert(newAccount);
        system.assert(newAccount.Id != null);

        Contact ct = new Contact(AccountId = newAccount.Id, FirstName = 'Test', LastName = 'Contact', Contact_Status__c = 'TEST');
        insert(ct);
        system.assert(ct.Id != null);

        Test.startTest();
        
        CampaignMember cmLead = new CampaignMember(); // Add a lead campaign member
        cmLead.CampaignId = camp.Id;
        cmLead.Status = 'Responded';
        cmLead.LeadId = ld.Id;
        insert cmLead;
        system.assert(cmLead.Id != null);
        ld = [SELECT Id, Status, Inquiry_Start_Date__c FROM Lead WHERE Id = :ld.Id];
        system.assert(ld.Inquiry_Start_Date__c != null);

        CampaignMember cmCt = new CampaignMember(); // Add a contact campaign member
        cmCt.CampaignId = camp.Id;
        cmCt.Status = 'Responded';
        cmCt.ContactId = ct.Id;
        insert cmCt;
        system.assert(cmCt.Id != null);
        ct = [SELECT Id, Contact_Status__c, Inquiry_Start_Date__c FROM Contact WHERE Id = :ct.Id];
        system.assert(ct.Inquiry_Start_Date__c != null);

        // TEST UPDATE //
        cmLead = new CampaignMember(); // Add a lead campaign member
        cmLead.CampaignId = camp2.Id;
        cmLead.Status = 'Sent';
        cmLead.LeadId = ld.Id;
        insert cmLead;

        cmCt = new CampaignMember(); // Add a contact campaign member
        cmCt.CampaignId = camp2.Id;
        cmCt.Status = 'Sent';
        cmCt.ContactId = ct.Id;
        insert cmCt;

        cmLead.Status = 'Responded';
        cmCt.Status = 'Responded';

        update cmLead;
        update cmCt;

        cmLead = [SELECT Id, Responded_Lead_Contact_Status__c, CM_Source_Partner__c, CM_Source_Site__c, CM_Source_ID2__c, CM_Keywords__c, CM_Source_URL__c FROM CampaignMember WHERE Id = :cmLead.Id];
        System.assertEquals(cmLead.Responded_Lead_Contact_Status__c, ld.Status);

        cmCt = [SELECT Id, Responded_Lead_Contact_Status__c, CM_Source_Partner__c, CM_Source_Site__c, CM_Source_ID2__c, CM_Keywords__c, CM_Source_URL__c FROM CampaignMember WHERE Id = :cmCt.Id];
        System.assertEquals(cmCt.Responded_Lead_Contact_Status__c, ct.Contact_Status__c);

        Test.stopTest();
    }
}