/*
	SortLineItemsController
	
	Author:
		Martin Haagen, mhg@qlikview.com
		
	Changes:
		2009-09-01 MHG	Created function.

	Description:
		A custom controller to a Visualforce component sorting the Sequence of Events
		records related to a Opportunity record. 

	Ref:
		http://forums.sforce.com/sforce/board/message?board.id=Visualforce&message.id=6861&query.id=142810#M6861

 */
public with sharing class SortLineItemsController {

    public Opportunity opportunity { get; set; }
    public Sequence_of_Event__c[] Sequence_of_Event { get; set; }
    
    public Sequence_of_Event__c[] getSorted() {
    	
    	System.debug('SortLineItemsController.getSorted: Starting.');
        
        if ((opportunity == null || opportunity.Sequence_of_events__r == null) && (Sequence_of_Event == null)) {
            return null;
        }
        
        List<Sequence_of_Event__c> result;
        if (Sequence_of_Event != null)
        	result = Sequence_of_Event.clone();        	
		else 
			result = opportunity.Sequence_of_events__r.clone();
			
		System.debug('SortLineItemsController.getSorted: List to be sorted contains '+result.size()+' elements');
			
        // Sort the list using Bubble sort
        boolean Swap;        
        do {
        	Swap = false;
        	
        	for (integer i=0;i<result.size() - 1;i++) {
        		Sequence_of_Event__c SoE1 = result.get(i);
        		Sequence_of_Event__c SoE2 = result.get(i+1);
        		
        		if (SoE1.S_o_E_Date__c > SoE2.S_o_E_Date__c) {
        			result.set(i, SoE2);
        			result.set(i+1, SoE1);
        			System.debug('SortLineItemsController.getSorted: SoE1.S_o_E_Date__c (' + SoE1.S_o_E_Date__c +') > SoE2.S_o_E_Date__c (' + SoE2.S_o_E_Date__c + ') - Swapping!');
        			Swap = true;
        		}
        		
        	} 
        	
        } while(Swap);
        
        System.debug('SortLineItemsController.getSorted: Finnished');
        
        return result;
   }

}