@isTest
private class GoogleTranslateUtilsTest {
    
    static testMethod void parseGoogleJson() {
        String jsonString = '{' +
        '    "data": {' +
        '        "detections": [' +
        '            [' +
        '                {' +
        '                    "language": "en",' +
        '                    "isReliable": false,' +
        '                    "confidence": 0.114892714' +
        '                }' +
        '            ]' +
        '        ]' +
        '    },' +
        '    "data": {' +
        '        "detections": [' +
        '            [' +
        '                {' +
        '                    "language": "zh-TW",' +
        '                    "isReliable": false,' +
        '                    "confidence": 0.7392313,' +
        '                    "skipthisitem": 0.7392313' +
        '                }' +
        '            ]' +
        '        ]' +
        '    }' +
        '}';
        List<List<GoogleTranslateUtils.GoogleTranslateResult>> results = 
            GoogleTranslateUtils.parseResults(jsonString);
        System.assertEquals(2, results.size());
        
        System.assertEquals(1, results[0].size());
        System.assertEquals('en', results[0][0].language);
        System.assertEquals(false, results[0][0].reliable);
        System.assertEquals(0.114892714, results[0][0].confidence);
        
        System.assertEquals(1, results[1].size());
        System.assertEquals('zh-TW', results[1][0].language);
        System.assertEquals(false, results[1][0].reliable);
        System.assertEquals(0.7392313, results[1][0].confidence);
        
    }
    
    static testMethod void toParamString() {

        GoogleTranslateUtilsMock mock = new GoogleTranslateUtilsMock(); 
        test.setMock(HttpCalloutMock.class, mock);
       
        List<String> paramStrings = GoogleTranslateUtils.toParamStrings(new List<String>{'text 1','text 2'});
        System.assertEquals('&q=text+1&q=text+2',paramStrings[0]);
        
        Map<String,String> idsDescriptions = new Map<String,String>();
        idsDescriptions.put('text 1','text 2');
        
        GoogleTranslateUtils.detectLanguages(idsDescriptions);
       
    }
    
  //  private static List<GoogleTranslateResult> detectLanguagesParams(List<String> params)
//private static List<GoogleTranslateResult> detectLanguagesRequest(String params)
//private static List<GoogleTranslateResult> flatten(List<List<GoogleTranslateResult>> results)
//private static HttpResponse getRequest(String url)
    
    static testMethod void toParamStringMaxLength() {
        //create a long text
        String chars200 = '';
        for (Integer i = 0; i < 200; i++) chars200 += 'c';
        
        //create a long list of long texts
        List<String> params = new List<String>();
        Integer times = 100;
        for (Integer i = 0; i < times; i++) params.add(chars200);
        
        //test that the text have been splitted into the correct number of strings
        List<String> paramsStrings = GoogleTranslateUtils.toParamStrings(params);
        System.assertEquals(( GoogleTranslateUtils.maxStringLength * times / 
                              Decimal.valueOf(GoogleTranslateUtils.paramsCharacterLimit))
                              .round(roundingMode.CEILING), paramsStrings.size());
        
        //test that no params string is longer than the maximum allowed 
        Integer i = 0;
        for (String paramsString : paramsStrings) {
            System.debug('=====>'+paramsString.length());
            System.assert(paramsString.length() <= GoogleTranslateUtils.paramsCharacterLimit);
            if (i < paramsStrings.size()-1) System.assertEquals(GoogleTranslateUtils.paramsCharacterLimit, paramsString.length());
            i++;
        }
    }
    
    static testMethod void updateCasesWithLanguages() {
        Case c = new Case(Language__c = 'DE'); insert c;
        GoogleTranslateUtils.GoogleTranslateResult res = new GoogleTranslateUtils.GoogleTranslateResult('EN', false, 1);
        GoogleTranslateUtils.updateCasesWithLanguages(new Set<String>{c.Id}, 
                                                      new List<GoogleTranslateUtils.GoogleTranslateResult>{ res });
        System.assertEquals('English'.toLowerCase(), [SELECT Language__c FROM Case WHERE Id = :c.Id][0].Language__c.toLowerCase());     
        
        //update with a locale, the locale is ignored and only the two first characters are used
        res = new GoogleTranslateUtils.GoogleTranslateResult('es-ES', false, 1);
        GoogleTranslateUtils.updateCasesWithLanguages(new Set<String>{c.Id}, 
                                                      new List<GoogleTranslateUtils.GoogleTranslateResult>{ res });
        System.assertEquals('Spanish'.toLowerCase(), [SELECT Language__c FROM Case WHERE Id = :c.Id][0].Language__c.toLowerCase());
        
    }
    
    static testMethod void bestResult() {
        List<GoogleTranslateUtils.GoogleTranslateResult> results = 
            new List<GoogleTranslateUtils.GoogleTranslateResult> {googleResult('EN', 1), googleResult('DE', 0.5), 
                                                                  googleResult('ES', 2), googleResult('SE', 1.8)};
        GoogleTranslateUtils.GoogleTranslateResult best = GoogleTranslateUtils.bestResult(results);
        System.assertEquals('ES'.toLowerCase(), best.language.toLowerCase());
    }
    
    private static GoogleTranslateUtils.GoogleTranslateResult googleResult(String language, Decimal confidence) {
        return new GoogleTranslateUtils.GoogleTranslateResult(language, false, Double.valueOf(confidence));
    }
    
    //only for coverage
    static testMethod void detectLanguageTrigger() {
        Case c = new Case();
        insert c;
    }
    
}