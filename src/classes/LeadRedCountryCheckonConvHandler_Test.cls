/******************************************************

    Test class: LeadQSD_Test
    
    Initiator: Ram
    
    Changelog:
        2019-05-12  Ram            Test class for LeadRedCountryCheckonConversionHandler
        2019-12-19  extbjd ITRM-388 add SeeAllData=true because lead convert with
                                new object creation throws an error. it is because Validation rules
                                does not work with hierarchy custom settings during this process.
                
******************************************************/
@isTest(SeeAllData = true)//do not remove. SeeAllData=false will cause errors. more details in the comments above
private class LeadRedCountryCheckonConvHandler_Test {
    
    @isTest static void taskCreationTest() {
        //Create Lead
        Lead testLead = new Lead(); 
        testLead.FirstName = 'test_FName';
        testLead.LastName = 'test_LName';
        testLead.Country = 'Cuba';
        testLead.Email = 'qsdtest@gmail.com';
        testLead.Company = 'testcompany';
        testLead.QSD_total_logins__c = 6;
        insert testLead;
        system.debug('ggg1'+testLead.QSD_total_logins__c);
        Database.LeadConvert lc = new database.LeadConvert();
lc.setLeadId(testLead.id);
lc.setDoNotCreateOpportunity(true);
lc.setConvertedStatus('Lead - Converted');

Database.LeadConvertResult lcr = Database.convertLead(lc);
LeadRedCountryCheckonConversionHandler lc1 = new LeadRedCountryCheckonConversionHandler();
    }
    
}