/**
*	Class Name: AccountSharingHandlerTest
* 	Test class for AccountSharingHandler/AccountSharingBatch | Coverage 92%/100%
*	2018-04-01 | AYS | BMW-739 | Created 
*/
@isTest
private class AccountSharingHandlerTest {
	
	@isTest static void  accountSharingTest() {
		insert new Account_Settings__c(
			Rerun_Sharing_Profiles__c = UserInfo.getProfileId(),
			Rerun_Sharing_Rules_Limit__c = 200
		);

		List<RecordType> l_RecordType = new List<RecordType>([
			SELECT id, DeveloperName
			FROM RecordType
			WHERE DeveloperName = 'End_User_Account' OR DeveloperName = 'Partner_Account'
		]);
		Map<String, Id> m_RTByName = new Map<String, Id>();
		for(RecordType rt : l_RecordType) {
			m_RTByName.put(rt.DeveloperName, rt.Id);
		}
		
        Account accPartner = new Account(
        	Name='Partner',
        	RecordTypeId = m_RTByName.get('Partner_Account')
        );		
        Account accEndUser = new Account(
        	Name='End User',
        	RecordTypeId = m_RTByName.get('End_User_Account')
        );

        insert new List<Account>{accPartner, accEndUser};

        insert new Responsible_Partner__c(
        	End_User__c = accEndUser.id,
        	Partner__c = accPartner.id
        );

        Test.startTest();
	        Semaphores.CustomAccountTriggerBeforeUpdate = false;
	        accPartner.Rerun_Sharing_Rules__c = true;
	        update accPartner;
        Test.stopTest();

	}
	

}