/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
*
* Change log:
*   2012-10-16  SAN Fix the unit test to pass the creation of Opportunity
*   2012-11-21  RDZ Spliting the unit test in 2 parts so we don't reach too many calls outs.
*   2013-10-28  CCE Added Semaphore flag reset to match change made in OppPartnerContactTrigger. Also
*                   added myUnitTest2 to improve coverage. 
*   2015-10-23  CCE Updated api version to 31, removed seeAllData, created test data - for Winter 16 SF upgrade
*   2017-03-28  Roman Dovbush (4front) : Fixing hardcoded profile IDs and code coverage.
*	2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring 
*11-1-2018 Shubham Added bypass rule true to pass validation error  
 */
@isTest
private class TEST_ActivePSMUpdate {

    static testMethod void myUnitTest() {
        
        QTTestUtils.GlobalSetUp();  //setup Custom settings

        // Create Test Accounts and Contacts
        Id AccRecordTypeId_PartnerAccount = QuoteTestHelper.getRecordTypebyDevName('Partner_Account').Id; //'01220000000DOFz';  //Partner Account 
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        User UserPartnerAccountOwner = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Std User');
        Account accP = new Account(
            Name = 'TestP',
            OwnerId = UserPartnerAccountOwner.Id,
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
            Navision_Customer_Number__c = '12345', 
            Legal_Approval_Status__c = 'Legal Approval Granted', 
            Billing_Country_Code__c = qtComp.Id);
        insert accP;

        
        // Create Partner Contact to add to the Opp
        Account acc = new Account(
            Name='TestAccount', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert acc;
        
        Contact PartnerContact = new Contact();
        PartnerContact.FirstName = 'Christest';
        PartnerContact.LastName = 'Clarke';
        PartnerContact.accountId = acc.Id;
        insert PartnerContact;


        //Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        User user = QTTestUtils.createMockOperationsAdministrator();
        Opportunity testOpp = QTTestUtils.createMockOpportunity('CompTests', 'CompOpp', 'New Customer', 'Reseller', 'Goal Identified', '01220000000DNwY', 'GBP', user);
        //Insert testOpp;
        testOpp.Sell_Through_Partner__c = accP.Id;
        testOpp.Active_PSM__c = UserPartnerAccountOwner.Id;
        testOpp.Partner_Contact__c = PartnerContact.Id;
        testOpp.StageName = 'Closed Lost';
        testOpp.Primary_reason_lost__c = 'Opportunity entered in error';
      

        System.Debug('TEST_ActivePSMUpdate.myUnitTest: testOpp = ' + testOpp);
        

        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];
        Account testAcc = new Account(
            Name='testAcc', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert testAcc;
        
        Contact testCon = new Contact();
        testCon.FirstName = 'James';
        testCon.LastName = 'Jury';
        testCon.accountId = testAcc.Id;
        insert testCon;
        testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE Id = :testCon.Id];
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: Contact James Jury Id = ' + testCon.Id);


        Opportunity testNewOpp = New Opportunity (
            Short_Description__c = 'TestOpp - delete me',
            Name = 'TestOpp - Delte me',
            Type = 'New Customer',
            Revenue_Type__c = 'Reseller',
            CloseDate = Date.today(),
            StageName = 'Alignment Meeting',
            RecordTypeId = '01220000000DNwY',
            AccountId = testAcc.Id,
            Sell_Through_Partner__c = testAcc.Id,
            Partner_Contact__c = testCon.Id,
            Opportunity_Origin__c = 'SI Driven Solution'
        );
       
       
        
        test.startTest();
        Update testOpp;
        testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c,StageName,Bypass_Rules__c FROM Opportunity WHERE Id = :testOpp.Id];
        Insert testNewOpp;

        System.assertEquals(testNewOpp.Short_Description__c, 'TestOpp - delete me');
        
        testOpp.Active_PSM__c = null;
        testOpp.Active_Referring_PSM__c = null;

        System.Debug('TEST_ActivePSMUpdate.myUnitTest: TestCase 1 - Tests with out Partner_Contact__c and Referring_Contact__c');                       
        Semaphores.Opportunity_OppPartnerContactTriggerHasRun = false;  //reset flag required as we need a new instance for the second update
        system.debug('prev stage is '+ testOpp.StageName);
        testOpp.Bypass_Rules__c = true;
        update testOpp;
        
        System.assertEquals(testOpp.Active_PSM__c, null );
        System.assertEquals(testOpp.Active_Referring_PSM__c, null );
        
        testOpp.Partner_Contact__c = testCon.Id;
 
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: TestCase 2 - Tests with out Partner_Contact__c ');
        Semaphores.Opportunity_OppPartnerContactTriggerHasRun = false;  //reset flag required as we need a new instance for the second update
        Semaphores.OpportunityTriggerBeforeUpdate = false;
       	Semaphores.OpportunityTriggerAfterUpdate = false;
        update testOpp;
        //testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c, Opportunity_Origin__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c, Opportunity_Origin__c FROM Opportunity WHERE Id = :testOpp.Id];
        
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: testOpp.Active_PSM__c = ' + testOpp.Active_PSM__c);
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: testCon.OwnerId = ' + testCon.OwnerId);
        System.assertEquals(testOpp.Active_PSM__c, testCon.OwnerId);
        
        testOpp.Referring_Contact__c = testCon.Id;
        //testOpp.Opportunity_Origin__c = 'SI Driven Solution';
        
        //System.Debug('TEST_ActivePSMUpdate.myUnitTest: TestCase 3 - Tests with Partner_Contact__c and Referring_Contact__c');
        //update testOpp;
        //testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        
        //System.assertEquals(testOpp.Active_Referring_PSM__c, testCon.OwnerId);
        
        test.stopTest();

    }
    /*SLH Removed test as Active_Referring_PSM__c no longer used
    //RDZ Spliting the test in 2 parts.
    static testMethod void updateOppWhenOppOriginChanges() {
        
        Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];
        
        testOpp = [SELECT Id, Active_PSM__c, Referring_Contact__c, Active_Referring_PSM__c, Opportunity_Origin__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        
        testOpp.Referring_Contact__c = testCon.Id;
        testOpp.Opportunity_Origin__c = 'SI Driven Solution';
        
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: TestCase 3 - Tests with Partner_Contact__c and Referring_Contact__c');
        update testOpp;
        testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        
        System.assertEquals(testOpp.Active_Referring_PSM__c, testCon.OwnerId);
       
        
    }*/
    
    static testMethod void myUnitTest2() {
   
        // Create Test Accounts and Contacts
        //Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];

        QTTestUtils.GlobalSetUp();

        // Create Test Accounts and Contacts
        Id AccRecordTypeId_PartnerAccount = QuoteTestHelper.getRecordTypebyDevName('Partner_Account').Id; //'01220000000DOFz';  //Partner Account 
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        User UserPartnerAccountOwner = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Std User');
        Account accP = new Account(
            Name = 'TestP',
            OwnerId = UserPartnerAccountOwner.Id,
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
            Navision_Customer_Number__c = '12345', 
            Legal_Approval_Status__c = 'Legal Approval Granted', 
            Billing_Country_Code__c = qtComp.Id);
        insert accP;

        
        // Create Partner Contact to add to the Opp
        Account acc = new Account(
            Name='TestAccount', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert acc;
        
        Contact PartnerContact = new Contact();
        PartnerContact.FirstName = 'Christest';
        PartnerContact.LastName = 'Clarke';
        PartnerContact.accountId = acc.Id;
        insert PartnerContact;


        //Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        User user = QTTestUtils.createMockOperationsAdministrator();
        Opportunity testOpp = QTTestUtils.createMockOpportunity('CompTests', 'CompOpp', 'New Customer', 'Reseller', 'Goal Identified', '01220000000DNwY', 'GBP', user);
        System.assertNotEquals(null, testOpp.Id);
        //Insert testOpp;
        testOpp.Sell_Through_Partner__c = accP.Id;
        testOpp.Active_PSM__c = UserPartnerAccountOwner.Id;
        testOpp.Partner_Contact__c = PartnerContact.Id;
        testOpp.StageName = 'Closed Won';
        testOpp.Primary_reason_lost__c = 'Opportunity entered in error';
        testOpp.Active_EIS__c = UserPartnerAccountOwner.Id;
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: testOpp = ' + testOpp);        

        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];
        Account testAcc = new Account(
            Name='testAcc', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert testAcc;
        
        Contact testCon = new Contact();
        testCon.FirstName = 'James';
        testCon.LastName = 'Jury';
        testCon.accountId = testAcc.Id;
        insert testCon;
        testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE Id = :testCon.Id];
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: Contact James Jury Id = ' + testCon.Id);

        Opportunity testNewOpp = New Opportunity (
            Short_Description__c = 'TestOpp2 - delete me',
            Name = 'TestOpp - Delte me',
            Type = 'New Customer',
            Revenue_Type__c = 'Reseller',
            CloseDate = Date.today(),
            StageName = 'Alignment Meeting',
            RecordTypeId = '012D0000000KB2N',   //Academic Program Opportunity
            AccountId = testAcc.Id,
            Sell_Through_Partner__c = testAcc.Id,
            Partner_Contact__c = testCon.Id,
            Opportunity_Origin__c = 'SI Driven Solution',
            Included_Products__c = 'Qlik Sense'
        );

        test.startTest(); 
		Update testOpp;
        testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Id = :testOpp.Id];

             
        Insert testNewOpp;
        System.assertEquals(testNewOpp.Short_Description__c, 'TestOpp2 - delete me');
        test.stopTest();
    }
    
    static testMethod void myUnitTestacademidProgramOpportunityId () {

        // Create Test Accounts and Contacts
        //Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];

        QTTestUtils.GlobalSetUp();

        // Create Test Accounts and Contacts
        Id AccRecordTypeId_PartnerAccount = QuoteTestHelper.getRecordTypebyDevName('Partner_Account').Id; //'01220000000DOFz';  //Partner Account 
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        User UserPartnerAccountOwner = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Std User');
        Account accP = new Account(
            Name = 'TestP',
            OwnerId = UserPartnerAccountOwner.Id,
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
            Navision_Customer_Number__c = '12345', 
            Legal_Approval_Status__c = 'Legal Approval Granted', 
            Billing_Country_Code__c = qtComp.Id);
        insert accP;

        
        // Create Partner Contact to add to the Opp
        Account acc = new Account(
            Name='TestAccount', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert acc;
        
        Contact PartnerContact = new Contact();
        PartnerContact.FirstName = 'Christest';
        PartnerContact.LastName = 'Clarke';
        PartnerContact.accountId = acc.Id;
        insert PartnerContact;

        Id academidProgramOpportunityId;
	        if(Academic_Program_Settings__c.getInstance('AcademicProgramSetting') != null) {
	            academidProgramOpportunityId = Academic_Program_Settings__c.getInstance('AcademicProgramSetting').OpportunityRecordTypeId__c;
	        }

        //Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        User user = QTTestUtils.createMockOperationsAdministrator();
        Opportunity testOpp = QTTestUtils.createMockOpportunity('CompTests', 'CompOpp', 'New Customer', 'Direct', 'Goal Identified', academidProgramOpportunityId, 'GBP', user);
        System.assertNotEquals(null, testOpp.Id);
        //Insert testOpp;
        //testOpp.Sell_Through_Partner__c = accP.Id;
        //testOpp.Active_PSM__c = UserPartnerAccountOwner.Id;
        //testOpp.Partner_Contact__c = PartnerContact.Id;
        testOpp.StageName = 'Closed Won';
        testOpp.Included_Products__c = 'Qlik Sense';
        testOpp.Primary_reason_lost__c = 'Opportunity entered in error';
        //testOpp.Active_EIS__c = UserPartnerAccountOwner.Id;
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: testOpp = ' + testOpp);        

        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];
        Account testAcc = new Account(
            Name='testAcc', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert testAcc;
        
        Contact testCon = new Contact();
        testCon.FirstName = 'James';
        testCon.LastName = 'Jury';
        testCon.accountId = testAcc.Id;
        insert testCon;
        testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE Id = :testCon.Id];
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: Contact James Jury Id = ' + testCon.Id);

        Opportunity testNewOpp = New Opportunity (
            Short_Description__c = 'TestOpp2 - delete me',
            Name = 'TestOpp - Delte me',
            Type = 'New Customer',
            Revenue_Type__c = 'Reseller',
            CloseDate = Date.today(),
            StageName = 'Alignment Meeting',
            RecordTypeId = '012D0000000KB2N',   //Academic Program Opportunity
            AccountId = testAcc.Id,
            Sell_Through_Partner__c = testAcc.Id,
            Partner_Contact__c = testCon.Id,
            Active_EIS__c = UserPartnerAccountOwner.Id,
            Active_MIS__c  = UserPartnerAccountOwner.Id,
            PSM_Nominated__c  = UserPartnerAccountOwner.Id,
            Referring_Sale_Rep_for_OEM__c = UserPartnerAccountOwner.Id,
            Active_PSM__c = UserPartnerAccountOwner.Id,
            Opportunity_Origin__c = 'SI Driven Solution'
        );

        test.startTest(); 
        Update testOpp;
        testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Id = :testOpp.Id];

             
        Insert testNewOpp;
        System.assertEquals(testNewOpp.Short_Description__c, 'TestOpp2 - delete me');
        test.stopTest();
    }    
    
    static testMethod void myUnitTestIncreasecoverage () {

        // Create Test Accounts and Contacts
        //Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];

        QTTestUtils.GlobalSetUp();

        // Create Test Accounts and Contacts
        Id AccRecordTypeId_PartnerAccount = QuoteTestHelper.getRecordTypebyDevName('Partner_Account').Id; //'01220000000DOFz';  //Partner Account 
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        User UserPartnerAccountOwner = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Std User');
        Account accP = new Account(
            Name = 'TestP',
            OwnerId = UserPartnerAccountOwner.Id,
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
            Navision_Customer_Number__c = '12345', 
            Legal_Approval_Status__c = 'Legal Approval Granted', 
            Billing_Country_Code__c = qtComp.Id);
        insert accP;

        
        // Create Partner Contact to add to the Opp
        Account acc = new Account(
            Name='TestAccount', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert acc;
        
        Contact PartnerContact = new Contact();
        PartnerContact.FirstName = 'Christest';
        PartnerContact.LastName = 'Clarke';
        PartnerContact.accountId = acc.Id;
        insert PartnerContact;

        //Opportunity testOpp = [SELECT Id, Active_PSM__c, Active_Referring_PSM__c FROM Opportunity WHERE Name = 'Steves Test Reg-Phase1'];
        User user = QTTestUtils.createMockOperationsAdministrator();
        Opportunity testOpp = QTTestUtils.createMockOpportunity('CompTests', 'CompOpp', 'New Customer', 'Reseller', 'Goal Identified', '01220000000DNwY', 'GBP', user);
        System.assertNotEquals(null, testOpp.Id);
        //Insert testOpp;
        testOpp.Sell_Through_Partner__c = accP.Id;
        testOpp.Active_PSM__c = UserPartnerAccountOwner.Id;
        testOpp.Partner_Contact__c = PartnerContact.Id;
        testOpp.StageName = 'Closed Won';
        testOpp.Primary_reason_lost__c = 'Opportunity entered in error';
        testOpp.Active_EIS__c = UserPartnerAccountOwner.Id;
        testOpp.Active_MIS__c  = UserPartnerAccountOwner.Id;
        testOpp.PSM_Nominated__c  = UserPartnerAccountOwner.Id;
        testOpp.Referring_Sale_Rep_for_OEM__c = UserPartnerAccountOwner.Id;
        testOpp.Active_PSM__c = UserPartnerAccountOwner.Id;
        testOpp.Included_Products__c = 'Qlik Sense';
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: testOpp = ' + testOpp);        

        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];
        Account testAcc = new Account(
            Name='testAcc', 
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
            Billing_Country_Code__c = qtComp.Id, 
            RecordTypeId = AccRecordTypeId_PartnerAccount,
            Navision_Status__c = 'Partner');
        insert testAcc;
        
        Contact testCon = new Contact();
        testCon.FirstName = 'James';
        testCon.LastName = 'Jury';
        testCon.accountId = testAcc.Id;
        insert testCon;
        testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE Id = :testCon.Id];
        System.Debug('TEST_ActivePSMUpdate.myUnitTest: Contact James Jury Id = ' + testCon.Id);

        Opportunity testNewOpp = New Opportunity (
            Short_Description__c = 'TestOpp2 - delete me',
            Name = 'TestOpp - Delte me',
            Type = 'New Customer',
            Revenue_Type__c = 'Reseller',
            CloseDate = Date.today(),
            StageName = 'QSG Closed Won',
            RecordTypeId = '01220000000DNwY',   
            AccountId = testAcc.Id,
            Sell_Through_Partner__c = testAcc.Id,
            Partner_Contact__c = testCon.Id,
            Active_EIS__c = UserPartnerAccountOwner.Id,
            Active_MIS__c  = UserPartnerAccountOwner.Id,
            PSM_Nominated__c  = UserPartnerAccountOwner.Id,
            Referring_Sale_Rep_for_OEM__c = UserPartnerAccountOwner.Id,
            Active_PSM__c = UserPartnerAccountOwner.Id,
            Opportunity_Origin__c = 'SI Driven Solution'
        );

        test.startTest(); 
        Update testOpp;
        testOpp = [SELECT Id, Active_PSM__c, Partner_Contact__c, Active_Referring_PSM__c, StageName FROM Opportunity WHERE Id = :testOpp.Id];

             
        Insert testNewOpp;
        System.assertEquals(testNewOpp.Short_Description__c, 'TestOpp2 - delete me');
        
        List<Opportunity> triggerOld = new List<Opportunity>();
        List<Opportunity> triggerNew = new List<Opportunity>();
        triggerOld.add(testOpp);
        triggerNew.add(testNewOpp);
        
        ActivePSMUpdate.UpdatePSM(triggerNew, triggerOld);
        test.stopTest();
    }        
}