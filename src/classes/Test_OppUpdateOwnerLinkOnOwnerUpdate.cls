/******************************************************

    Test_OppUpdateOwnerLinkOnOwnerUpdate
    
    Test for the OppUpdateAccOwnerName trigger
    
    Changelog:
        2012-05-01  CCE     Initial development for CR# 4613 - #Quick - fix field source on Opportunity page
        2013-03-06  TJG     Replacing inactive opp record type with active one - Direct / Reseller - Std Sales Process
        2013-10-31  CCE     CR# 8968 Added Semaphore to clear the flag before next update.
        2013-12-27  MTM     CR# 10209 Modify Pick list value and Sales Credit Info trigger for Influencing Partners
        2014-08-04  TJG     CR# 15694 Fix sales credit information on Closed Won, DO NOT reset Semaphores.OppUpdateAccOwnerNameHasRun 
                            https://eu1.salesforce.com/a0CD000000kBvVI
        2015-10-28  IRN     Winter 16 Issue- removed seeAllData, called globalSetUp and SetupPriceBook,  created a mockuser with qttestUtil instead of the Util class, 
        2016-06-16  roman@4front.se Method testOppUpdateAccOwnerName is updated, two Influence Partners added
        2016-07-19  NAD     CR 87308 Add Acc Owner Name at Closed Won for Qlik Online Purchase Opportunities
        2016-09-22  TJG     Fix test error for Q2CW project
                            Test_OppUpdateOwnerLinkOnOwnerUpdate testOppUpdateAccOwnerName
                                System.LimitException: Too many SOQL queries: 101 
                                Stack Trace: Class.OppIfPartnerDealRecPSMAtClosedWonHandler.handle: line 163, column 1 
                                            Trigger.OpportunityTrigger: line 49, column 1
******************************************************/
@isTest(seeAllData=false)
private class Test_OppUpdateOwnerLinkOnOwnerUpdate
{
    static TestMethod void testOppUpdateAccOwnerName()
    {
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        System.debug('testOppUpdateAccOwnerName: Starting');
        
        //Create our test data
        //Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        //UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
        
        //Create a test user with fullname 'Testy6 Testy6'
        User testUser = QTTestUtils.createMockUserForProfile('System Administrator');
       User userToTest = [Select id From User where id =: UserInfo.getUserId()];
       System.RunAs(userToTest)
        {
		 Subsidiary__c subs = new Subsidiary__c(
            Legal_Entity_Name__c = 'test',
            Netsuite_Id__c = 'test'
            );
			insert subs;
            // Adding an account with BillingCountryCode and Qliktech Company
            QlikTech_Company__c Q = new QlikTech_Company__c(Name = 'GBR',
                QlikTech_Company_Name__c = 'QlikTech UK Ltd' ,Subsidiary__c = subs.id);
            insert Q;
              test.startTest(); 
            Account testAcc = new Account (
            OwnerId = testUser.Id,
            Name= 'Chriss Test 42',
            Navision_Customer_Number__c = '12345',
            QlikTech_Company__c = Q.QlikTech_Company_Name__c,
            
            Billing_Country_Code__c = Q.Id,
            BillingStreet = '417',
            BillingState ='CA',
            BillingPostalCode = '94104',
            BillingCountry ='United States',
            BillingCity = 'SanFrancisco',
            
            Shipping_Country_Code__c = Q.Id,
            ShippingStreet = '417',
            ShippingState ='CA',
            ShippingPostalCode = '94104',
            ShippingCountry ='United States',
            ShippingCity = 'SanFrancisco'); 
            insert testAcc;
            
            Contact c = new Contact();
            c.FirstName = 'Junk';
            c.LastName = 'Boy';
            c.accountId = testAcc.Id;
            c.Email = 'mattman@test.com';
            
            insert c;
                     
            //Create the test opportunity - this should cause our
            //trigger to fire and populate the Acc_Owner_Name__c field

            Opportunity opp1 = new Opportunity(
            Short_Description__c = 'TestOpp - delete me',
            name='Test Opp 1',
            Type = 'New Customer',
            Revenue_Type__c = 'Direct',
            // recordtypeid='01220000000DNwd',  // Direct / Reseller - Std Sales Process
            recordTypeId='01220000000DNwY', //
            StageName = 'Marketing Qualified',
            Customers_Business_Pain__c = 'Alignment meeting must be set for large deals',
            CurrencyIsoCode = 'GBP',
            CloseDate = Date.today(),
            Signature_Type__c = 'Digital Signature',   
            AccountId = testAcc.Id);
            
            //Added opportunity per CR 87308
            //Create the test opportunity - this should cause our
            //trigger to fire and populate the Acc_Owner_Name__c field
            Opportunity opp2 = new Opportunity(
            Short_Description__c = 'TestOpp - delete me',
            name='Test Opp 2',
            Type = 'New Customer',
            Revenue_Type__c = 'Direct',
            // recordtypeid='012D0000000KJRS',  // Qlik Online Purchase Opportunities
            recordTypeId='012D0000000KJRS', //
            //recordtypeid = rt.Id, 
            StageName = 'Closed Won',
            Customers_Business_Pain__c = 'Alignment meeting must be set for large deals',
            CurrencyIsoCode = 'GBP',
            CloseDate = Date.today(),
            Signature_Type__c = 'Digital Signature',   
            AccountId = testAcc.Id
            //Sell_Through_Partner__c = testAcc.Id
            );

            List<Opportunity> ListOpps = new List<Opportunity>();
            ListOpps.add(opp1);
            //ListOpps.add(opp2);
            Insert(ListOpps);

            System.debug('---Inserted ListOpps---');
            //We need to add some items before we can set the Opp to Closed Won 
            OpportunityLineItem LineItem = new OpportunityLineItem();
            LineItem.OpportunityId = opp1.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;        
            insert LineItem;  
            System.debug('---Inserted LineItem---');
            // 1st IP
            Influencing_Partner__c InfluencingPartner = new Influencing_Partner__c();
            InfluencingPartner.Opportunity__c = opp1.id;
            InfluencingPartner.Influencing_Partner_Contact__c = c.Id;
            InfluencingPartner.Opportunity_Origin__c = 'SI Driven Solution';   
            //insert InfluencingPartner;  
            
            // create 2nd IP for test reason
            Influencing_Partner__c InfluencingPartnerSecond = new Influencing_Partner__c();
            InfluencingPartnerSecond.Opportunity__c = opp1.id;
            InfluencingPartnerSecond.Influencing_Partner_Contact__c = c.Id;
            InfluencingPartnerSecond.Opportunity_Origin__c = 'SI Driven Solution';   
            //insert InfluencingPartnerSecond; 

            // create 3rd IP for test reason
            Influencing_Partner__c InfluencingPartnerThird = new Influencing_Partner__c();
            InfluencingPartnerThird.Opportunity__c = opp1.id;
            InfluencingPartnerThird.Influencing_Partner_Contact__c = c.Id;
            InfluencingPartnerThird.Opportunity_Origin__c = 'SI Driven Solution';   
            //insert InfluencingPartnerThird; 

            List<Influencing_Partner__c> listIPs = new List<Influencing_Partner__c>();
            ListIPs.add(InfluencingPartner);
            ListIPs.add(InfluencingPartnerSecond);
            ListIPs.add(InfluencingPartnerThird);
            Insert ListIPs;
            System.debug('---Inserted ListIPs---');
            //Check that the Acc_Owner_Name__c field has not been populated
            Opportunity oppRes = [SELECT Id, Acc_Owner_Name__c FROM Opportunity WHERE Id = :opp1.Id limit 1];
            System.assertEquals(oppRes.Acc_Owner_Name__c, null);
           
            //set the StageName to Closed Won so that the trigger will update the required field
            opp1.StageName = 'Closed Won';
            opp1.Included_Products__c = 'Qlik Sense';
            
            
            //// 2014-08-04 Semaphores.OppUpdateAccOwnerNameHasRun = false; //simulate the resetting of the flag between updates
            //// DO NOT reset the flag CR 15694
            update opp1;
test.stopTest();
            System.debug('---Updated opp1---');
                    
            //Check that the Acc_Owner_Name__c field has been populated
            /*opp1 = [SELECT Id, Acc_Owner_Name__c, Influencing_PSM_QM_at_Closed_Won_1__c FROM Opportunity WHERE Id = :opp1.Id];
            opp2 = [SELECT Id, Acc_Owner_Name__c FROM Opportunity WHERE Id = :opp2.Id];

            System.assertEquals(opp1.Acc_Owner_Name__c, 'Testing');
            System.assertEquals(opp2.Acc_Owner_Name__c, 'Testing');
            
            System.assertEquals(opp1.Influencing_PSM_QM_at_Closed_Won_1__c, 'Testing');
            */
            
            System.debug('testOppUpdateAccOwnerName: Finishing');    

        }
    }


//Never used    
/*  static TestMethod void testOwnerLink()
    { 
        // Grab two Users
        User[] users = [select Id from User where Isactive = true limit 2];
        User u1 = users[0];
        User u2 = users[1];
 
        // Create an Opportunity
        System.debug('Creating Opportunity');
        Opportunity o1 = new Opportunity(CloseDate = Date.newInstance(2008, 01, 01), Name = 'Test Opportunity', StageName = 'Goal Identified', OwnerId = u1.Id);
        insert o1;
 
        // Test: Owner_Link should be set to user 1
        Opportunity o2 = [select id, OwnerId, Owner_Link__c from Opportunity where Id = :o1.Id];
        System.assertEquals(u1.Id, o2.OwnerId);
        System.assertEquals(u1.Id, o2.Owner_Link__c);
 
  //Doesn't work yet - TODO - if we end up using the associated trigger
        // Modify Owner
//        o2.OwnerId = u2.Id;
//        update o2;
 
        // Test: Owner_Link should be set to user 2
//        Opportunity o3 = [select id, OwnerId, Owner_Link__c from Opportunity where Id = :o2.Id];
//        System.assertEquals(u2.Id, o3.OwnerId);
//        System.assertEquals(u2.Id, o3.Owner_Link__c);
    }*/
}