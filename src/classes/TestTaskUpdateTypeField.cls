/*
Name:  TestTaskUpdateTypeField.cls

Purpose:
-------
CR# 3984 - Activity Type Tracking Part 2
======================================================

History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     CCE			2012-02-16  Initial development.
		RDZ			2012-04-11  Creating User using TestMethod from Util class.
								And changing test method for more suitable name it was myUnitTest
		CCE			2013-12-18  Changed TK - Call (Voicemail) to TK - Call Attempt due to CR 9755
								https://eu1.salesforce.com/a0CD000000bSsB7
*/
@isTest
private class  TestTaskUpdateTypeField {
	 
	// static testMethod void testTaskUpdateTypeFieldTKEMail() {
	// 	QTTestUtils.GlobalSetUp();

	// 	Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
 //       UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
 //       User thisUserForRunAs = [SELECT Id, Country FROM User WHERE Id =: UserInfo.getUserId()];

	// 	//Create test Account
 //       QlikTech_Company__c QTComp = new QlikTech_Company__c(
	//		Name = 'USA',
	//		QlikTech_Company_Name__c = 'QlikTech Inc',
	//		//Country_Name__c = thisUserForRunAs.Country
	//		Country_Name__c = 'USA'
	//	);
	//	insert QTComp;

	// 	//if(thisUserForRunAs.Country != 'USA')
 //  //     {
	//  //      QTComp = new QlikTech_Company__c(
	//  //          Name = 'USA',
	//  //          Country_Name__c = 'USA',
	//  //          QlikTech_Company_Name__c = 'QlikTech Inc'           
	//  //      );
	//  //      insert QTcomp;
 //  //     }

 //       //Create test User
 //       //User testUser = new User(FirstName = 'Test1', 
 //       //                         LastName= 'User1',
 //       //                         Alias = 'tUser1', 
 //       //                         Email = 'testUser1@testorg.com', 
 //       //                         UserName = 'testUser1@testorg.com', 
 //       //                         EmailEncodingKey='UTF-8', 
 //       //                        LanguageLocaleKey='en_US', 
 //       //                         LocaleSidKey='en_US', 
 //       //                         TimeZoneSidKey='America/Los_Angeles',
 //       //                         ProfileId = testProfile.Id,
 //       //                       UserRoleId = testUserRole.Id,
 //       //                         Country = 'USA');
 //       //insert testUser;
 //       //Create test data
 //       //Create test User using SFDC Recomendation to avoid duplicate username
 //       User testUser = Util.createUser('TestUserName', 'Testttt3', testProfile.Id, testUserRole.Id);
 //       insert testUser;
        
 //       System.runAs(thisUserForRunAs)
 //       {
        	
	//		QTComp = [select Id, Name, QlikTech_Company_Name__c, Country_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
	//		Account testAcnt = new Account(Name='Test1 Account1', OwnerId = testUser.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
	//        insert testAcnt;
	        
	//        //create a test lead
	// 		Lead TestLead = New Lead (
 //       		FirstName = 'Lead',
 //       		LastName = 'Test1001',
 //       		Company = 'Test',
 //       		Country = 'USA',
 //       		Email = 'asd@asd.com'
 //       	);		
	//		insert TestLead;
			
	//		//create a task
	//		//we will test that when the subject contains 'Goal Identification Letter' the Type is set to 'TK - Email - GI Letter' 
	//		Task task1 = new Task();
	//		task1.Subject = 'Email: Goal Identification Letter';
	//		task1.WhatId = testAcnt.Id;
	//		//task1.WhoId = TestLead.Id; //OwnerId;
	//		task1.Status = 'Completed';
	//		insert task1;
		
	//		List<Task> tasks = [select id, isClosed, Subject, Type from Task where whatid =: testAcnt.Id limit 1];
	//		System.assertEquals(1, tasks.size());
	//		System.assertEquals('TK - Email - GI Letter', tasks[0].Type);
		
	//		//create a task
	//		//we will test that when the subject contains 'contact' we do not update the Type field' 
	//		Task task2 = new Task();
	//		task2.Subject = 'contact';
	//		task2.WhatId = testAcnt.Id;
	//		//task2.WhoId = TestLead.Id; //OwnerId;
	//		task2.Status = 'Completed';
	//		//we set a value here as a validation rule runs that stops the insert as the default Type will be set to 'select'
	//		task2.Type = 'TK - Call Attempt';	//was 'TK - Call (Left Voicemail)'; but changed due to CR 9755 (CCE)
	//		insert task2;
		
	//		List<Task> tasks2 = [select Id, isClosed, Subject, Type from Task where whatid =: testAcnt.Id order by Id limit 2];
	//		System.assertEquals(2, tasks2.size());
	//		System.debug('Type[0] = ' + tasks2[0].Type + ' Subject =' + tasks2[0].Subject);
	//		System.debug('Type[1] = ' + tasks2[1].Type + ' Subject =' + tasks2[1].Subject);
	//		System.assertEquals('TK - Call Attempt', tasks2[1].Type);	//was 'TK - Call (Left Voicemail)'; but changed due to CR 9755 (CCE)	
		
	//		//create a task
	//		//we will test that when the subject contains 'Email: blah blah' we do set the Type field to 'TK - Email' 
	//		Task task3 = new Task();
	//		task3.Subject = 'Email: blah blah';
	//		task3.WhatId = testAcnt.Id;
	//		//task3.WhoId = TestLead.Id; //OwnerId;
	//		task3.Status = 'Completed';
	//		insert task3;
		
	//		List<Task> tasks3 = [select Id, isClosed, Subject, Type from Task where whatid =: testAcnt.Id order by Id limit 3];
	//		System.assertEquals(3, tasks3.size());
	//		System.debug('Type[0] = ' + tasks3[0].Type + ' Subject =' + tasks3[0].Subject);
	//		System.debug('Type[1] = ' + tasks3[1].Type + ' Subject =' + tasks3[1].Subject);
	//		System.debug('Type[2] = ' + tasks3[2].Type + ' Subject =' + tasks3[2].Subject);
			
	//		System.assertEquals('TK - Email', tasks3[2].Type);			
			
	// 	}
	//}
}