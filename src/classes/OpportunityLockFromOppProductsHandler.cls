/******************************************************

    Class: OpportunityLockFromOppProductsHandler

    Changelog:
        2016-05-24  Roman@4front      Migrated from OpportunityLockFromOppProducts.trigger.

******************************************************/


public with sharing class OpportunityLockFromOppProductsHandler {
	public OpportunityLockFromOppProductsHandler() {

	}

	public static void handle(List<OpportunityLineItem> triggerNew, List<OpportunityLineItem> triggerOld, Boolean isDelete) {
		// qtwebm, Systems Administrator or API Only
		System.debug('--- Started: OpportunityLockFromOppProductsHandler');
		HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
		if (UserInfo.getUserName().contains('qtweb') || settings.Sys_Admin_ProfileId__c.contains(UserInfo.getProfileId()) || UserInfo.getProfileId().substring(0, 15) == '00e20000000zFfp')
		{
			return;
		}

	    List<string> Ids = new List<string>();
	    List<OpportunityLineItem> LI = null;

	    if (isDelete)
	    {
	        LI = triggerOld;
	    }
	    else
	    {
	        LI = triggerNew;
	    }

	    for (OpportunityLineItem Item : LI)
	    {
	        Ids.add(Item.OpportunityId);
	    }

	    Map<Id, Opportunity> Opps = new Map<Id, Opportunity>([SELECT Id, Finance_Approved__c FROM Opportunity WHERE Id In :Ids]);

		for (OpportunityLineItem Item:LI)
		{
			if (!Opps.containsKey(Item.OpportunityId))
			{
				continue;
			}

			Opportunity Opp = Opps.get(Item.OpportunityId);
			if (Opp.Finance_Approved__c == true)
			{
				Item.addError('Opportunity is Locked');
			}
		}
		System.debug('--- Finished: OpportunityLockFromOppProductsHandler');
	}
}