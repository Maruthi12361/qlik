/*
    Initial development: MTM
    2014-06-27  MTM CR# XXXX  UpdateDealSplitChildOpportunities.
    2015-01-27  MTM   Fix test failures
	24.03.2017 Rodion Vakulovskyi   
*/
@isTest
public with sharing class Test_UpdateDealSplitChildOpp {
    static testMethod void testUpdateDealSplitChildOppBatch()
    {
        SetUpData();
        Test.StartTest();            

    
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
        UpdateDealSplitChildOpportunities batch = new UpdateDealSplitChildOpportunities();
        ID batchId = Database.executeBatch(batch);

      
        Test.StopTest();
    }
    
    static testMethod void testUpdateDealSplitChildOppSchedulable()
    {
        Test.StartTest();
        UpdateDealSplitChildOppsSchedulable  schedule1 = new UpdateDealSplitChildOppsSchedulable();
        string sch = '0 30 * * * ?';
        //system.schedule('UpdateDealSplitChildOppsSchedulable', sch, schedule1);
        Test.StopTest();
    }
    
    private static void SetUpData()
    {
        user = QTTestUtils.createMockOperationsAdministrator();//createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.createMockAccount('Deal Split Test Account', user);
        contact = QTTestUtils.createMockContact(account.id);

        //QTTestUtils.GlobalSetUp();

        Pricebook2 nspb = new Pricebook2();
        nspb.Name = 'NS Pricebook';
        insert nspb;

        Id standardPriceBookId = Test.getStandardPricebookId();


        Product2 p2 = new Product2(Name='Consulting', isActive=true, Family='Consulting', Deferred_Revenue_Account__c = '26000 Deferred Revenue', Income_Account__c = '26000 Deferred Revenue');
        Product2 p3 = new Product2(Name='Education', isActive=true, Family='Education', Deferred_Revenue_Account__c = '26000 Deferred Revenue', Income_Account__c = '26000 Deferred Revenue');
        insert p2;
        insert p3;
        //01sD0000000JOeZ
        
        PriceBook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];
            
        // set up PricebookEntry and Verify that the results are as expected.
        PricebookEntry pbe = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p2, 'GBP');
        PricebookEntry pbe2 = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p3, 'GBP');

        NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = nspb.Id;
        settings.UserRecordTypeIdsToExclude__c = '005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
        insert settings;

        QTCustomSettings__c settings2 = new QTCustomSettings__c();
        settings2.Name = 'Default';
        settings2.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
        settings2.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
        settings2.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
        insert settings2;
        QuoteTestHelper.createCustomSettings();
        //System.runAs(QTTestUtils.createMockOperationsAdministrator())
        //System.runAs(user)
        {          
            Id QBuy2RecordType;
            if(NS_Settings_Detail__c.getInstance('NSSettingsDetail') != null) {
                QBuy2RecordType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
            }
            dsParent = createOpportunity('DSTests', 'DS Parent test Opp', 'New Customer', 'Direct', 'Deal Split Create', QBuy2RecordType, 'GBP', user, account);
        }        
                    
        //System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            dsChild1 = QTTestUtils.createOpportunity('Child1 Tests', 'DS Child1 test Opp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account, dsParent, 10);
            System.assertEquals(dsChild1.StageName, 'Goal Identified');           
        }        
    }
    
    private static Opportunity createOpportunity(String sShortDescription, String sName, String sType, String sRevenueType, String sStage, Id sRecordTypeId, String sCurrencyIsoCode, User user, Account acc)
    {
        Opportunity opp = New Opportunity (
                Short_Description__c = sShortDescription,
                Name = sName,
                Type = sType,
                Revenue_Type__c = sRevenueType,
                CloseDate = Date.today(),
                StageName = sStage, 
                RecordTypeId = sRecordTypeId, 
                CurrencyIsoCode = sCurrencyIsoCode,
                AccountId = acc.Id,
                Split_Opportunity__c = true,
           //     Education_Forecast_Amount__c = 10000,
                Signature_Type__c = 'Digital Signature'
             //   Consultancy_Forecast_Amount__c = 20000
                ); 
           insert opp;
           return opp;
    }
    static User user;
    static Account account;
    static Contact contact;
    static Opportunity dsParent;
    static Opportunity dsChild1;
    static Opportunity dsChild2;    
}