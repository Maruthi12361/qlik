/**
* Class: PopulatePartnerDiscountDescriptOnQuote.
* @description Once QuoteLine is inserted/updated and field Partner_Discount_Description__c is calculated then
*              loop through every line item, take value from Partner_Discount_Description__c and check if it already exist in
*              Partner_Discount_Description__c field on Quote object and if it is not - then add it to that field (not override).
*              Q2CW project.
*
* Changelog:
*    2016-11-17 : Roman Dovbush (4front) : Initial development.
*    2017-01-03 : Roman Dovbush (4front) : Update. createDescriptionList method is created.
*    2017-10-05 : Linus Löfberg QCW-4035 Add support for product family Subscription in query on line 37
*    2017-12-12 : Bala Egambaram QCW-4191 : added MSP revenue type in the quoteToLine SELECT statement
*/


public with sharing class PopulatePartnerDiscountDescriptOnQuote {

    public static void onAfterInsertUpdateDelete(List<SBQQ__QuoteLine__c> triggerNew) {
        System.debug('populatePartnerDiscountDescription started...');
        System.debug('!!!QLines: ' + triggerNew);
        Set<Id> quoteIdSet = new Set<Id>();
        Set<String> descriptions = new Set<String>();
        List<SBQQ__Quote__c> quoteToLine = new List<SBQQ__Quote__c>();
        System.debug('quote1: ' + triggerNew[0].SBQQ__Quote__c);

        for (SBQQ__QuoteLine__c qLine : triggerNew) {
            quoteIdSet.add(qLine.SBQQ__Quote__c);
        }

        Boolean generateLicense;
        if(Test.isRunningTest()) {
            generateLicense = false;
        } else {
            generateLicense = true;
        }

        if(!quoteIdSet.isEmpty()) {
            quoteToLine = [Select Id, Revenue_Type__c, Partner_Discount_Description__c, (Select Id, SBQQ__Quote__c, Partner_Discount_Description__c, Generate_License__c From SBQQ__LineItems__r where (SBQQ__Product__r.Family = 'Licenses' OR SBQQ__Product__r.Family = 'Subscription') AND Generate_License__c = :generateLicense And isDeleted = false)
                           From SBQQ__Quote__c Where Id IN :quoteIdSet And Revenue_Type__c IN ('Reseller', 'Fulfillment', 'Distributor', 'OEM', 'OEM Recruitment', 'MSP')];
        }

        Set<SBQQ__Quote__c> quoteToUpdate = new Set<SBQQ__Quote__c>();
        System.debug('!!!QuoteToLineList: ' + quoteToLine);
        if(!quoteToLine.isEmpty()) {
            //System.debug('!!!LineItems: ' + quoteToLine.LineItems);
            for(SBQQ__Quote__c quote : quoteToLine) {
                System.debug('!!!QuoteLineItem: ' + quote.SBQQ__LineItems__r);
               if(quote.SBQQ__LineItems__r.isEmpty()) {               
                    quote.Partner_Discount_Description__c = '';
                    quoteToUpdate.add(quote);
                }
                else {
                    descriptions = createDescriptionList(quote.SBQQ__LineItems__r);
                    System.debug('Discount Description: ' + descriptions);
                    if(!descriptions.isEmpty()) {
                        quote.Partner_Discount_Description__c = '';
                        for(String s : descriptions) {
                            if(quote.Partner_Discount_Description__c == null || quote.Partner_Discount_Description__c == '') {
                                quote.Partner_Discount_Description__c = s;
                                quoteToUpdate.add(quote);
                            }
                            else if(!quote.Partner_Discount_Description__c.contains(s)) {
                                quote.Partner_Discount_Description__c += ', ' + s;
                                quoteToUpdate.add(quote);
                            }
                        }
                    }
                }
            }

            System.debug('!!!QuotesToUpdate: ' + quoteToUpdate);
            if(!quoteToUpdate.isEmpty()) {
                update new List<SBQQ__Quote__c>(quoteToUpdate);
            }
            quoteToLine.clear();
        }

        System.debug('populatePartnerDiscountDescription finished...');
    }

    private static Set<String> createDescriptionList(List<SBQQ__QuoteLine__c> quoteItemsList) {
        Set<String> partnerDescription = new Set<String>();
        for(SBQQ__QuoteLine__c qItem : quoteItemsList) {
            System.debug('!!!QGenerate_License__c: ' + qItem.Generate_License__c);
            if(qItem.Partner_Discount_Description__c != null || qItem.Partner_Discount_Description__c != '' ||
                    qItem.Partner_Discount_Description__c != '-')
            {
                System.debug('Partn discount desc field ' + qItem.Partner_Discount_Description__c);
                partnerDescription.addAll(qItem.Partner_Discount_Description__c.split(','));
            }
        }
        return partnerDescription;
    }

    public static void onAfterDelete(Set<ID> deletedSBQQQuoteLineIDs) {
        List<SBQQ__QuoteLine__c> quoteLines = [SELECT Id, Name, Partner_Discount_Description__c, SBQQ__Quote__c From SBQQ__QuoteLine__c Where Id in :deletedSBQQQuoteLineIDs ALL ROWS];
        if(!quoteLines.isEmpty()) {
            onAfterInsertUpdateDelete(quoteLines);
        }
    }
}