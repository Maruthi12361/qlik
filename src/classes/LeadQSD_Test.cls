/******************************************************

    Test class: LeadQSD_Test
    
    Initiator: Ram
    
    Changelog:
        2017-03-06  Ram            Test class for QSD trigger LeadQSD 
                
******************************************************/
@isTest
private class  LeadQSD_Test {
    
    @isTest static void taskCreationTest() {
   
        //Create Lead
        Lead testLead = new Lead(); 
        testLead.FirstName = 'test_FName';
        testLead.LastName = 'test_LName';
        testLead.Country = 'Sweden';
        testLead.Email = 'qsdtest@gmail.com';
        testLead.Company = 'testcompany';
        testLead.QSD_total_logins__c = 6;
        insert testLead;
        system.debug('ggg1'+testLead.QSD_total_logins__c);
        List<Lead> testLeads = [select id,QSD_total_logins__c  from Lead where id = :testLead.ID];
        system.debug('ggg'+testLeads);
		Semaphores.TriggerHasRun(1);
        testLeads[0].QSD_total_logins__c= 15;
        system.debug('ggg3'+testLeads);
        update testLeads;
    }
    
}