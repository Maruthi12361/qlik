/********************************************************
* CLASS: CreateQSLoginDetails
* DESCRIPTION: Batch class for fixing failed web activities due to insufficient access on campaign 
* or contact being created with account owned by inactive user.
* CHANGELOG:    
*	2019-10-01 - CRW - Added Initial logic
*********************************************************/
global class WebActivityRetry implements Database.Batchable<sObject> {
	WebActivitySettings__c csWebActivity = WebActivitySettings__c.getInstance();
    Decimal retryLimit = csWebActivity.Number_of_Retries__c;
    String batchLimit = csWebActivity.Retry_Batch_Limit__c;
    List<String> errorListException = csWebActivity.Error_Exception_List__c.split(',');
    String query;
    String executionType;
    global WebActivityRetry(String Type){
        executionType = Type;
        query = 'SELECT Id,Audit_Trail__c,Content__c,No_of_retries__c,Status__c,Name,Sender__c,Type__c FROM Web_Activity__c WHERE Status__c = \'Failed\' LIMIT ' + batchLimit;
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
	}
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Web_Activity__c> lstWebActivity = (List<Web_Activity__c>)(scope);
        List<Web_Activity__c> lstWAToInsert = new List<Web_Activity__c>();
        if (executionType == 'Recurring'){
            for (Web_Activity__c WA:lstWebActivity){
                if ((WA.No_of_retries__c == null || WA.No_of_retries__c <= retryLimit) && !WA.Type__c.contains('FirstLogin') && !WA.Type__c.contains('RecordDownload')){
                    For (String errors:errorListException){
                        if (WA.Audit_Trail__c.Contains(errors)){
                            if (WA.No_of_retries__c == null){WA.No_of_retries__c = 0;}
                            WA.No_of_retries__c = WA.No_of_retries__c + 1;
                            WA.Status__c = 'Pending';
                            lstWAToInsert.add(WA);
                        }
                    }    
                }
            }
        }else if(executionType == 'twiceADay'){
            for (Web_Activity__c WA:lstWebActivity){
                if (WA.No_of_retries__c <= retryLimit){
                    WA.No_of_retries__c = WA.No_of_retries__c + 1;
                    WA.Status__c = 'Pending';
                    lstWAToInsert.add(WA);
                }
            }
        }
        //WebActivityHandler.handleBeforeInsert(lstWAToInsert);
        if (lstWAToInsert.size() > 0){
            List<Database.SaveResult > updateResults = Database.update(lstWAToInsert, false);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        system.debug('Finish');
	}

}