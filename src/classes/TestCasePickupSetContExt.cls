@isTest

/***************************************************************************************************************************************
TestCasePickupSetContExt
	Changelog:
		2012-02-14  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
		2012-08-21	RDZ		CR#	5396 https://eu1.salesforce.com/a0CD000000NHAqd Related with change number 3, on owner change set status to new.				
		2012-09-12  SAN		Handle multiple cases with various record type
		06.02.2017  RVA :   changing CreateAcounts methods	
****************************************************************************************************************************************/
private class TestCasePickupSetContExt 

{
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    static testMethod void testCasePickup()
    {
		List<Case> validTestCases = createTestCases();
    	Customizations__c settings = Customizations__c.getOrgDefaults();
    	settings.Pickup_Case_Status__c = 'In Process';
    	upsert settings Customizations__c.Id; 


    	final String IN_PROCESS = 'In Process';
    	final String STATUS_NEW = 'New';
    	
        Test.setCurrentPageReference(Page.CasePickup);    
        
        //Positive testing data  
        Set<Id> validTestCaseIds = new Set<Id>();
        
        for(Case c:validTestCases)
        {
        	validTestCaseIds.add(c.Id);
        }        
        ApexPages.StandardSetController sscTestValid = new ApexPages.StandardSetController(validTestCases);    
        sscTestValid.setSelected(validTestCases);
        
        //Negative testing data
        List<Case> invalidTestCases = new List<Case>();
        ApexPages.StandardSetController sscTestInvalid = new ApexPages.StandardSetController(invalidTestCases);    
        sscTestInvalid.setSelected(invalidTestCases);
        
        Test.startTest();
        
        //Block of code for positive tests
		CasePickupSetContExt cpsc = new CasePickupSetContExt(sscTestValid);       
		System.assert(cpsc.areCasesSelected);
		System.assertEquals(7, cpsc.pickupCases.size());
		PageReference pgRef = cpsc.updateCasesForPickup();
		validTestCases = [SELECT Id, Status, OwnerId FROM Case WHERE Id IN : validTestCaseIds];	

        // internal cases, status should be switched to New		
		//System.assertEquals(STATUS_NEW, validTestCases.get(0).Status);
		System.assertEquals(UserInfo.getUserId(), validTestCases.get(0).OwnerId);
		//System.assertEquals(STATUS_NEW, validTestCases.get(1).Status);
		System.assertEquals(UserInfo.getUserId(), validTestCases.get(1).OwnerId);
		//System.assertEquals(STATUS_NEW, validTestCases.get(2).Status);
		System.assertEquals(UserInfo.getUserId(), validTestCases.get(2).OwnerId);
		//System.assertEquals(STATUS_NEW, validTestCases.get(3).Status);
		System.assertEquals(UserInfo.getUserId(), validTestCases.get(3).OwnerId);
		
        // other support, status should stay as in process		
		System.assertEquals(IN_PROCESS, validTestCases.get(4).Status);
		System.assertEquals(UserInfo.getUserId(), validTestCases.get(4).OwnerId);
		System.assertEquals(IN_PROCESS, validTestCases.get(5).Status);
		System.assertEquals(UserInfo.getUserId(), validTestCases.get(5).OwnerId);
		System.assertEquals(IN_PROCESS, validTestCases.get(6).Status);
		System.assertEquals(UserInfo.getUserId(), validTestCases.get(6).OwnerId);
		
		//Block of code for negative tests	
		cpsc = new CasePickupSetContExt(sscTestInvalid);
		System.assert(!cpsc.areCasesSelected);
		System.assertEquals(null, cpsc.pickupCases);
								     
        Test.stopTest();    
    }
    
    
    static List<Case> createTestCases()
    {

        //Create test data
        //Create test User
        /*User testUser = new User(FirstName = 'Test1', 
                                 LastName= 'User1',
                                 Alias = 'tUser1',
                                 Email = 'testUser1@testorg.com', 
                                 UserName = 'testUser1@testorg.com', 
                                 EmailEncodingKey='UTF-8', 
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US', 
                                 TimeZoneSidKey='America/Los_Angeles',
                                 ProfileId = testProfile.Id,
                                 UserRoleId = testUserRole.Id);
        insert testUser;*/
        
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        
        List<Case> testCasesRet = new List<Case>();
        System.runAs(testUser)
        {	/*remove
			Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
	        //Create test Account
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
				Name = 'USA',
				QlikTech_Company_Name__c = 'QlikTech Inc',
				Subsidiary__c = sub.id			
			);
			insert QTComp;
			QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
			Account testAcnt = new Account(Name='Test1 Account1', OwnerId = testUser.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
	        insert testAcnt;
	        */
			Account  testAcnt = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAcnt.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAcnt;
	        //Create test Contact
	        Contact testContact = new Contact(FirstName='Test1', LastName='Contact1', AccountId=testAcnt.Id, OwnerId=testUser.Id);
	        insert testContact;
	        
	        
	        
	        //Create test Case
	        List<Case> testCases = new List<Case>();
	        // internal cases, status should be switched to New
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000Ddyi'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DdyY'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DdyT'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000IE3k'));
	        // other support, status should stay as in process
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000Ddp2'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DZgI'));
	        testCases.add(new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id, RecordTypeId='01220000000DZqG'));

	        insert testCases;
	        testCasesRet = testCases;
        }    
        return testCasesRet;
    }
}