/**
* AttunityAddCPLogSchedCon
* 
* Description:  Adds CPLog to Attunity contacts and activates Attunity users that were disabled.
* Added:        2019-11-18 - Ain - IT-2287
*
* Change log:
* 09-11-2018 - Ain - IT-2287 - Initial Implementation
* 17-12-2019 - extcqb - IT-2321 - new profile
*
*/

global class AttunityAddCPLogSchedCon implements Database.Batchable<SObject>, Schedulable {

    private String query = 'select id, contactid, federationidentifier, isactive from user where contactid != null and (profile.name = \'Customer Portal Case Logging Access - Attunity\' or profile.name = \'Customer Portal Case Viewing Access - Attunity\')';
    public Id CPLogId;
    
    global AttunityAddCPLogSchedCon(){
	
		System.debug('In AttunityAddCPLogSchedCon.Constructor');
		
        List<ULC_Level__c> CPLogIds = [SELECT Id FROM ULC_Level__c WHERE Name = 'CPLOG' AND Status__c = 'Active'];
        if (CPLogIds.size() == 1) {
            CPLogId = CPLogIds[0].Id;
        }
        else{
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No CPLOG level found!');
            throw e;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('In AttunityAddCPLogSchedCon.start');
        if(Test.isRunningTest()){
            query = query + ' and Alias = \'007\'';
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
	
		System.debug('In AttunityAddCPLogSchedCon.execute: ' + scope.size());
        
        List<User> users = (List<User>)scope;
        List<Assigned_ULC_Level__c> levelsToAssign = new List<Assigned_ULC_Level__c>();
        Map<Id, Id> contactToUserId = new Map<Id, Id>();
        
        
        
        //Gather all contactIds and connect them to the Users
        for(User u : users) {
            contactToUserId.put(u.ContactId, u.Id);
        }
        
        List<Id> UsersWithAssignedULCLevel = new List<Id>();
        
        List<Assigned_ULC_Level__c> assignedULCLevels = [SELECT Id, ULCLevelId__c, Status__c, ContactId__c FROM Assigned_ULC_Level__c WHERE ContactId__c IN :contactToUserId.keySet() AND ULCLevelId__c=:CPLogId];
        
        for(Assigned_ULC_Level__c assignedULCLevel : assignedULCLevels){
            UsersWithAssignedULCLevel.add(contactToUserId.get(assignedULCLevel.ContactId__c));
        }
        
        for(User u : users) {
            if (UsersWithAssignedULCLevel.contains(u.Id)) {
                continue;
            }
            Assigned_ULC_Level__c levelToAssign = new Assigned_ULC_Level__c();
            levelToAssign.ContactId__c = u.ContactId;
            levelToAssign.Status__c = 'Approved - No trigger';
            levelToAssign.ULCLevelId__c	= CPLogId;
            levelsToAssign.add(levelToAssign);
        }
        
        insert levelsToAssign;
    }
    
    global void execute(SchedulableContext SC){
        AttunityAddCPLogSchedCon batchJob = new AttunityAddCPLogSchedCon();
        Id batchprocessid = Database.executeBatch(batchJob);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}