/**
	Test class: Test_SolutionProfileRedirectController for SolutionProfileRedirectController
	
	2013-05-07	CCE		Initial development	of Test class, part of CR# 8028 - #Quick - Modify link on Qonnect Portal.
						https://eu1.salesforce.com/a0CD000000YGb1y
	2013-11-15	CCE		Added to test change to SolutionProfileRedirectController.cls for CR# 10096
						https://eu1.salesforce.com/a0CD000000d60Zu
    								
 */
@isTest
private class Test_SolutionProfileRedirectController {

	static final String ART_OEMPartnerAccount = '01220000000DOG4';	//OEM Partner Account (Account Record Type)
	
	static User createMockUserForProfile(String profileName, Id ContactId) {
		Profile p = [SELECT Id FROM profile WHERE name like :profileName];
		return createMockUser(p.id, ContactId);
	}
	 
	static User createMockUser(Id profileId, Id ContactId) {
		User u = new User(alias = 'newUser', email='newuser@tgerm.com.test',
		emailencodingkey='UTF-8', lastname='Testing', 
		languagelocalekey='en_US', localesidkey='en_US', profileid = profileId, ContactId = ContactId,
		timezonesidkey='America/Los_Angeles', username= System.now().millisecond() + 'newuser@tgerm.com.test');
		insert u;
		return  u; 
	}
	
	static void testAsPartnerUserByProfileName(String ProfileName ) {
        // create test data        
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
        Account acc = new Account(Name='TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
	    insert acc;
	        
        Contact c = new Contact();
    	c.FirstName = 'Tao';
    	c.LastName = 'Handy';
    	c.accountId = acc.Id;
    	c.Email = 'ccetestmockcontact@test.com';	    	
    	insert c;
	    	
        System.runAs(createMockUserForProfile(ProfileName, c.Id))
        {
	        //Go to page
			PageReference tpageRef = Page.SolutionProfileRedirect;
			Test.setCurrentPage(tpageRef);
			//Set Parameters that would be passed in 
			ApexPages.currentPage().getParameters().put('acc', acc.Id);
			
			// Instantiate a new controller with all parameters in place
			SolutionProfileRedirectController sprc = new SolutionProfileRedirectController();
			//Simulate intial action call on page
			Pagereference p = sprc.checkAndRedirect();
			system.debug('Test_SolutionProfileRedirectController>createMockUserForProfile ProfileName = ' + ProfileName);
			system.debug('Test_SolutionProfileRedirectController>createMockUserForProfile p = ' + p);
        	
			if (ProfileName == 'PRM - Base') {
				Pagereference expectedResult = new Pagereference('/apex/SolutionProfileRelatedList?id=' + acc.Id);
    			system.assertEquals(expectedResult.getUrl(), p.getUrl());    	
			} else {
				Pagereference expectedResult = new Pagereference('/006?fcf=00BD0000007phFW');	//All Closed Won (Opportunities view) 
    			system.assertEquals(expectedResult.getUrl(), p.getUrl());				
			}			
        }
    }
    
	static testmethod void  testAsPartnerUserPRMBASE() {
    	test.startTest();
    	testAsPartnerUserByProfileName('PRM - Base');
    	test.stopTest();
    }    
    
    static testmethod void  testAsPartnerUserIND_QLIKBUY() {
		test.startTest();
    	testAsPartnerUserByProfileName('PRM - Independent Territory + QlikBuy');
    	test.stopTest();
    }
    
    //CCE 20121115 Added to test change for CR# 10096  
    static void testAsPartnerUserByProfileNameWithOEMPartnerAccount(String ProfileName ) {
        // create test data        
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
    
        Account acc = new Account(Name='TestAccount',
        							QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
        							RecordTypeId = ART_OEMPartnerAccount,
        							Billing_Country_Code__c = QTComp.Id);
	    insert acc;
	        
        Contact c = new Contact();
    	c.FirstName = 'Tao';
    	c.LastName = 'Handy';
    	c.accountId = acc.Id;
    	c.Email = 'ccetestmockcontact@test.com';	    	
    	insert c;
	    	
        System.runAs(createMockUserForProfile(ProfileName, c.Id))
        {
	        //Go to page
			PageReference tpageRef = Page.SolutionProfileRedirect;
			Test.setCurrentPage(tpageRef);
			//Set Parameters that would be passed in 
			ApexPages.currentPage().getParameters().put('acc', acc.Id);
			
			// Instantiate a new controller with all parameters in place
			SolutionProfileRedirectController sprc = new SolutionProfileRedirectController();
			//Simulate intial action call on page
			Pagereference p = sprc.checkAndRedirect();
			system.debug('Test_SolutionProfileRedirectController>createMockUserForProfile ProfileName = ' + ProfileName);
			system.debug('Test_SolutionProfileRedirectController>createMockUserForProfile p = ' + p);
        	
			Pagereference expectedResult = new Pagereference('/' + acc.Id);
    		system.assertEquals(expectedResult.getUrl(), p.getUrl());						
        }
    }
    
    //CCE 20121115 Added to test change for CR# 10096  
    static testmethod void  testAsPartnerUserIND_QLIKBUYWithOEMAccount() {
		test.startTest();
    	testAsPartnerUserByProfileNameWithOEMPartnerAccount('PRM - Independent Territory + QlikBuy');
    	test.stopTest();
    }    
    
}