/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_UtilityTest {

    @testSetup static void testsetup() {
        QuoteTestHelper.createCustomSettings();
    }
    
    public static testMethod void TestGetQS_Persona_Category() {
        List <QS_Persona_Category_Mapping__c> pcms = new List <QS_Persona_Category_Mapping__c>();
        QS_Persona_Category_Mapping__c pcm1 = TestDataFactory.createPersonaCategoryMapping('Business User', 'CategoryTest1', 'Nothing');
        QS_Persona_Category_Mapping__c pcm2 = TestDataFactory.createPersonaCategoryMapping('Server Administrator', 'CategoryTest2', 'Nothing');

        pcms.add(pcm1);
        pcms.add(pcm2);

        insert pcms;

        Test.startTest();

        string searchCategories = QS_Utility.GetQS_Persona_Category('Business User;Server Administrator');

        system.debug('searchCategories: ' + searchCategories);

        system.assert(searchCategories.contains('CategoryTest1'));
        system.assert(searchCategories.contains('CategoryTest2'));

        Test.stopTest();
    }
    public static testMethod void  TestHasULCLevel()
    {
        ULC_Level__c level1 = new ULC_Level__c();
        level1.Name = 'CPVIEW';
        level1.Status__c = 'Active';
        
        ULC_Level__c level2 = new ULC_Level__c();
        level2.Name = 'CPLOG';
        level2.Status__c = 'Active';
        
        ULC_Level__c level3 = new ULC_Level__c();
        level3.Name = 'CPINACTIVE';
        level3.Status__c = 'Pending';

        ULC_Level__c level4 = new ULC_Level__c();
        level4.Name = 'CPNOTASSIGNED';
        level4.Status__c = 'Active';

        List<ULC_Level__c> levels = new List<ULC_Level__c>();

        levels.add(level1);
        levels.add(level2);
        levels.add(level3);
        levels.add(level4);

        insert levels;

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;


        Assigned_ULC_Level__c assignedULCLevel1 = new Assigned_ULC_Level__c();
        assignedULCLevel1.ContactId__c = testContact.Id;
        assignedULCLevel1.Status__c = 'Approved';
        assignedULCLevel1.ULCLevelId__c = level1.Id;

        Assigned_ULC_Level__c assignedULCLevel2 = new Assigned_ULC_Level__c();
        assignedULCLevel2.ContactId__c = testContact.Id;
        assignedULCLevel2.Status__c = 'Rejected';
        assignedULCLevel2.ULCLevelId__c = level2.Id;


        Assigned_ULC_Level__c assignedULCLevel3 = new Assigned_ULC_Level__c();
        assignedULCLevel3.ContactId__c = testContact.Id;
        assignedULCLevel3.Status__c = 'Approved';
        assignedULCLevel3.ULCLevelId__c = level3.Id;

        List<Assigned_ULC_Level__c> assignedLevels = new List<Assigned_ULC_Level__c>();
        assignedLevels.add(assignedULCLevel1);
        assignedLevels.add(assignedULCLevel2);
        assignedLevels.add(assignedULCLevel3);

        Insert assignedLevels;

        system.assert(QS_Utility.HasULCLevel(testContact.Id, 'CPVIEW'));
        system.assert(!QS_Utility.HasULCLevel(testContact.Id, 'CPLOG'));
        system.assert(!QS_Utility.HasULCLevel(testContact.Id, 'CPINACTIVE'));
        system.assert(!QS_Utility.HasULCLevel(testContact.Id, 'CPNOTASSIGNED'));



    }

}