/********
* NAME :ResponsiblePartnersTest
* Description: Tests for Helper class to populate responsible partners
* 
*
*Change Log:
    IRN      2015-12-07   Partner share project Created test class
    ext_abk  2015-12-15
    IRN      2016-04-05   Updated two test to have account as partner
    IRN      2016-04-08   Updated test to have the account as partner when they are responsible partners
    IRN      2016-15-19   Added a test TestResponsiblePartnerHighlightsField 
******/
@isTest
private class ResponsiblePartnersTest {

    private static List<NS_Support_Contract__C> contracts;
    private static Id endUser;

    private static void Setup(){
    }

    private static void callPopulateResponsiblePartner(){
        Datetime adayago = Datetime.now().addDays(-1);
        contracts = [Select Id, End_User__c, Reseller__c, Responsible_Partner__c From NS_Support_Contract__C Where LastModifiedDate >: adayago ];
        Set<Id> uniqueEndUserSet = new Set<Id>();
        for(Integer i = 0;  i< contracts.size(); i++){
            if(contracts[i].End_User__c != null){
                uniqueEndUserSet.add(contracts[i].End_User__c);   
            }
        }
        List<Id> uniqueEndUser = new List<Id>();
        uniqueEndUser.addAll(uniqueEndUserSet);
        ResponsiblePartners resp = new ResponsiblePartners();
        resp.populateResponsiblePartners(uniqueEndUser);
    }
 
    private static testMethod void TestPopulateResponsiblePartners(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        createMockNSSupportContract();
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(1, respPartners.size());
        test.stopTest();
    }

    private static void createMockNSSupportContract(){
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        acc.IsPartner = true;
        update acc;
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        NS_Support_Contract__c ns = new NS_Support_Contract__c(Name='test', End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns;
    }
    
    
      private static testMethod void TestPopulateResponsiblePartners1()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        createMockNSSupportContract1();
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(2, respPartners.size());
        test.stopTest();
    }

    private static void createMockNSSupportContract1()
    {
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        Account acc3 = QTTestUtils.createMockAccount('account name3', u);
        acc3.IsPartner = true;
        update acc3;
        acc.IsPartner = true;
        update acc;
        NS_Support_Contract__c ns1 = new NS_Support_Contract__c(Name='test1',Responsible_Partner__c=acc3.Id, End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns1;
    }



    private static testMethod void TestPartnerIsBothSupportProvidedByAndSellThroughPartner()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        createMockNSSupportContract2();
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(1, respPartners.size());
        test.stopTest();
    }

    private static void createMockNSSupportContract2()
    {
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        acc.IsPartner = true;
        update acc;
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        acc2.IsPartner = true;
        update acc2;
        NS_Support_Contract__c ns2 = new NS_Support_Contract__c(Name='test2',Responsible_Partner__c=acc.Id, End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns2;
    }



     private static testMethod void RemoveSupportProvidedByAndTestPopulateResponsiblePartners()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Setup();
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        acc.IsPartner = true;
        update acc;
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        acc2.IsPartner = true;
        update acc2;
        NS_Support_Contract__c ns3 = new NS_Support_Contract__c(Name='test3', Responsible_Partner__c=acc.Id, End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns3;
        System.assertNotEquals(null, ns3.Id);

        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(1, respPartners.size());
        ns3.Responsible_Partner__c = null;
        update ns3;
        callPopulateResponsiblePartner();
        respPartners = [Select Id, Partner__c from responsible_Partner__c];
        System.assertEquals(1, respPartners.size());
        test.stopTest();
    }

    private static testMethod void TestTwoContractsWithSameEndUserAndSameSellThroughPartner()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Setup();
        createMockNSSupportContract4();
        
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
       system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(1, respPartners.size());
        test.stopTest();
    }

     private static void createMockNSSupportContract4()
    {
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        acc.IsPartner = true;
        update acc;
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        NS_Support_Contract__c ns4 = new NS_Support_Contract__c(Name='test4', End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns4;
        NS_Support_Contract__c ns5 = new NS_Support_Contract__c(Name='test5', End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns5;
    }


 private static testMethod void TestTwoContractsWithSameEndUserAndSameResponsiblePartner()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Setup();
        
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        acc.IsPartner = true;
        update acc;
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        NS_Support_Contract__c ns4 = new NS_Support_Contract__c(Name='test4', End_User__c = acc2.Id, Responsible_Partner__c = acc.Id);
        insert ns4;
        NS_Support_Contract__c ns5 = new NS_Support_Contract__c(Name='test5', End_User__c = acc2.Id, Responsible_Partner__c = acc.Id);
        insert ns5;
        
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(1, respPartners.size());
        test.stopTest();
    }


    private static testMethod void TestThreeContractsWithSameEndUserAndDifferentSellThroughPartner()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Setup();
        createMockNSSupportContract5();
        
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
       system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(3, respPartners.size());
        test.stopTest();
    }

    private static void createMockNSSupportContract5()
    {
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        Account acc3 = QTTestUtils.createMockAccount('account name3', u);
        Account acc4 = QTTestUtils.createMockAccount('account name4', u);
        acc.isPartner = true;
        acc3.isPartner = true;
        acc4.isPartner = true;
        update acc;
        update acc3; 
        update acc4;
        NS_Support_Contract__c ns6 = new NS_Support_Contract__c(Name='test6', End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns6;
        NS_Support_Contract__c ns7 = new NS_Support_Contract__c(Name='test7', End_User__c = acc2.Id, Reseller__c = acc3.Id);
        insert ns7;
        NS_Support_Contract__c ns8 = new NS_Support_Contract__c(Name='test8', End_User__c = acc2.Id, Reseller__c = acc4.Id);
        insert ns8;
    }

    private static testMethod void TestTwoContractsWithSameEndUserAndDifferentSellThroughPartner()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Setup();

        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        acc.IsPartner = true;
        update acc;
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        Account acc3 = QTTestUtils.createMockAccount('account name3', u);
        acc3.IsPartner = true;
        update acc3;
        NS_Support_Contract__c ns9 = new NS_Support_Contract__c(Name='test4', End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns9;
        NS_Support_Contract__c ns10 = new NS_Support_Contract__c(Name='test5', End_User__c = acc2.Id, Reseller__c = acc3.Id);
        insert ns10;
        
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(2, respPartners.size());
        ns9.Reseller__c = acc3.Id;
        update ns9;
        callPopulateResponsiblePartner();
        respPartners = [Select Id, Partner__c from responsible_Partner__c];
        System.assertEquals(1, respPartners.size());
        test.stopTest();
     }

    private static testMethod void TestAddingAndThenDeleteingAContract()
    {
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Setup();

        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        acc.IsPartner = true; 
        update acc;
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        Account acc3 = QTTestUtils.createMockAccount('account name3', u);
        NS_Support_Contract__c ns9 = new NS_Support_Contract__c(Name='test4', End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns9;
        
        callPopulateResponsiblePartner();
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(1, respPartners.size());
        delete ns9;
        respPartners = [Select Id, Partner__c from responsible_Partner__c];
        System.assertEquals(0, respPartners.size());
        test.stopTest();
     }

    private static testMethod void TestPopulateResponsiblePartnersWithExecuteMethod(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        createMockNSSupportContract();
        ResponsiblePartners resp = new ResponsiblePartners();
        List<NS_Support_Contract__C> scope = [Select Id, End_User__c, Reseller__c, Responsible_Partner__c From NS_Support_Contract__C];
        resp.execute(null, scope);
        List<responsible_Partner__c> respPartners = [Select Id, Partner__c from responsible_Partner__c];
        system.debug('TEST resp partners ' + respPartners);
        System.assertEquals(1, respPartners.size());
        test.stopTest();
    }


    private static testMethod void TestResponsiblePartnerHighlightsField(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        createMockNSSupportContractForHighlightsField();
        callPopulateResponsiblePartner();
        List<Account> acc = [Select Id, Responsible_Partner_Highlights__c from Account where Id = :endUser];
        system.debug('TEST acc ' + acc);
        System.assertNotEquals(null, acc[0].Responsible_Partner_Highlights__c);
        test.stopTest();
    }

    private static void createMockNSSupportContractForHighlightsField()
    {
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account namenamenamenamenaee9999999999999999999911111111111111111111111111111111111111111111222222222222222222222222222222222222222222222222222222222222222222222222', u);
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        Account acc3 = QTTestUtils.createMockAccount('account name3333333333333333333333333333333333333333333333333333', u);
        acc3.IsPartner = true;
        update acc3;
        acc.IsPartner = true;
        update acc;
        NS_Support_Contract__c ns1 = new NS_Support_Contract__c(Name='test1',Responsible_Partner__c=acc3.Id, End_User__c = acc2.Id, Reseller__c = acc.Id);
        insert ns1;
        endUser = acc2.Id;
    }
    
}