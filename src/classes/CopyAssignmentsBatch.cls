// Batch program to copy Assignments to Assignment_SE__c custom objects
// NOTE: USE IT ONLY ONCE
// Initial Implementation - November 11, 2013 - Madhav Kakani
global class CopyAssignmentsBatch implements Database.Batchable<sObject> {
 
    global CopyAssignmentsBatch() {
    }
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, Name, pse__Resource__c, pse__Project__c, pse__End_Date__c FROM pse__Assignment__c';
        if(Test.isRunningTest()) query += ' LIMIT 1';
        return Database.getQueryLocator(query); 
    }
 
    global void execute(Database.BatchableContext BC, List<sObject> scope){ 
        List<pse__Assignment__c> lstAss = scope;
     
        Set<Id> projIds = new Set<Id>(); // collect the list of projects ids
        for(pse__Assignment__c ass : lstAss) projIds.add(ass.pse__Project__c);
        
        // Get the list of matching Project_SE objects
        List<Project_SE__c> projSE = [SELECT Master_Project_Id__c, Id FROM Project_SE__c
                                        WHERE Master_Project_Id__c IN :projIds];

        // create a map of master project id and projSE ids
        Map<Id, Id> mapProj = new Map<Id, Id>();
        for(Project_SE__c proj: projSE) mapProj.put(proj.Master_Project_Id__c, proj.Id);
        
        List<Assignment_SE__c> lstAssignSE = new List<Assignment_SE__c>();

        for(pse__Assignment__c ass : lstAss) {
            Assignment_SE__c ase = new Assignment_SE__c();
            ase.Name = ass.Name;
            ase.Resource__c = ass.pse__Resource__c;
            ase.Master_Assignment_Id__c = ass.Id;
            ase.Project_SE__c = mapProj.get(ass.pse__Project__c);
            ase.End_Date__c = ass.pse__End_Date__c;
            lstAssignSE.add(ase);
        }
        if((lstAssignSE != null) && (lstAssignSE.size() > 0)) database.insert(lstAssignSE, false);
    }
 
    global void finish(Database.BatchableContext BC){
    }
 
}