/*
ChangeLog
    2013-08-13  CCE Change to test class as Simon McAllister is on holiday (a validation rule has had a name
                    change from Simon to Josefine so we need to use her ID)
    2013-09-12  SAN CR#9570 to undo previous CCE CR#9280 Change to test class as Simon McAllister is on holiday (a validation rule has had a name
                    change from Simon to Josefine so we need to use her ID) 
 
                    
*/
public with sharing class ChangeControlUtil {

    public static void AssignApproverToSpecification(List<Specification__c> Specifications)
    {
        List<string> ParentIDs = new List<string>();
        for (Specification__c Spec : Specifications)
        {
            if (Spec.Change_Control__c != null)
            {
                ParentIDs.add(Spec.Change_Control__c);              
            }
        }

        if (ParentIDs.size() == 0)
        {
            System.debug('AssignApproverToSpecification: No parents found');
            return;
        }
        
        Map<string, SLX__Change_Control__c> ParentIdToParent = new Map<string, SLX__Change_Control__c>();
        for (SLX__Change_Control__c ChangeControl : [select Id, SLX__Requestor__c from SLX__Change_Control__c where Id in :ParentIDs])
        {
            ParentIdToParent.put(ChangeControl.Id, ChangeControl);
        }
        

        for (Specification__c Spec : Specifications)
        {
            if (Spec.Change_Control__c != null)
            {
                ParentIDs.add(Spec.Change_Control__c);              
            }
        }

        for (Specification__c Spec : Specifications)
        {
            if (Spec.Change_Control__c == null || !ParentIdToParent.containsKey(Spec.Change_Control__c))
            {
                continue;               
            }
            
            Spec.Requestor__c = (ParentIdToParent.get(Spec.Change_Control__c)).SLX__Requestor__c;           
        }   
    }
    
    public static TestMethod void Test1()
    {
        SLX__Change_Control__c ChangeControl = new SLX__Change_Control__c(
        //Business_Owner__c = '00520000001Ce2K',  //use Josefine Elmqvist as
        Business_Owner__c = '005D0000001qrhb',    //Simon McAllister is on holiday
        Business_Area__c = 'Sales');

        ChangeControl.Name = 'My test change control';
        ChangeControl.SLX__Requestor__c = UserInfo.getUserId();
        
        insert ChangeControl;

        test.startTest();
                
        Specification__c Specification = new Specification__c();
        Specification.Change_Control__c = ChangeControl.Id;
        Specification.Name = 'Requirement 1';
        insert Specification;
        
        Specification = [select Id, Name, Requestor__c from Specification__c where Id = :Specification.Id];
        
        System.assertEquals(UserInfo.getUserId(), Specification.Requestor__c);
        
        test.stopTest();
    }

}