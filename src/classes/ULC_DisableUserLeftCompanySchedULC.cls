/********************************************************

ULC_DisableUserLeftCompanySchedULC
Scheduler for ULC_DisableUserLeftCompany
Runs the method DeactivateULCDetails and DeactivateAssignedULCLevels which must be run in a different context 
from the user method.

ChangeLog:
2015-02-10	AIN 	Initial implementation
***********************************************************/

global with sharing class ULC_DisableUserLeftCompanySchedULC implements Schedulable {
	global static void execute (SchedulableContext sc)
	{
		ULC_DisableUserLeftCompany.DeactivateULCDetails();
		ULC_DisableUserLeftCompany.DeactivateAssignedULCLevels();
	}
}