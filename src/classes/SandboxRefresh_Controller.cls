/*******************************************************
* Change History
* 20140000  Kauser  Initial development for CR# 10111
* 20140924  RDZ     Modifying QTTEST to QA on test CR# 16608
* 20141022  RDZ     Modifying how controller gets created as sandbox name does not work on production.
* 20141112  RDZ     Adding static method to get Sandbox name from username. getSandboxName();
* 20150812  RDZ     Adding to where stament on GetCaseList more atributes to avoid test class failure.
*                   RecordType for new starters cases and isdeleted=false
* 20160905  IRN     Added subsidary_c to QlikTech Company due to that is now a required field
* 20170210  AIN     Removed update for training users when running test classes as it was using the licenses in production
* 20180621  EXTBJD  Added the creation of a particular Campaign object and Parent Campaign object.
* 20190124  EXTBJD  Added method runCreationCampaign
* 20190225  CRW     BMW-1216 Added creation HardcodedValuesQ2CW__c for test TaskAddLeadContactToCampaignTest

*******************************************************/
public with sharing class SandboxRefresh_Controller {

    public static string TrainingUserFormatStr = 'Training User{0} (Retired)';
    public static final String CAMPAIGN_OWNER_ID = [SELECT Id FROM User WHERE Username LIKE 'qtmetadata@qlik.com%' LIMIT 1].Id;


    list <String> ApexClassInputObjList = new List<String>();
    public static String VfPageUrlValue;
    public String gSandboxName;

    public static String getSandboxName() {
        //Get the User Name to extract the Sandbox Name
        String lSandBoxName = UserInfo.getUserName();

        String gSandboxName;

        //If Production gSandboxName will be LIVE
        if(UserInfo.getOrganizationId().startsWith('00D20000000IGPX')) {
            gSandboxName = 'LIVE';
        } else{
            if(lSandBoxName.contains('com.')){
                    List <String> lSandBoxSplit = lSandBoxName.split('com.',2);
                    gSandboxName= lSandBoxSplit[1];
            }
            else{
                gSandboxName = 'test';
            }

        }
        system.debug('gSandboxName inside getSandboxName: ' + gSandboxName);
        return gSandboxName;
    }

    public  SandboxRefresh_Controller ()
    {

        gSandboxName = SandboxRefresh_Controller.getSandboxName();

    }

    /***********************************************************************************
    ********************* Email Template Section***************************************
    ***********************************************************************************/

    List <Sandbox_Refresh__c> gEmailTempList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();
    List <EmailTemplate> gEmailTempList= new List<EmailTemplate>();
    List <EmailTemplate> gEmailTempList_Backup= new List<EmailTemplate>();

    // Return the Email Templates list to the UI
    public list<EmailTemplate> getEmailTempList_ToModify() {
        list <EmailTemplate> leTObjList = new List<EmailTemplate>();
        List<String> lemailTempNameList = new List<String>();

        gEmailTempList_Of_SandBoxObj= [Select Name From Sandbox_Refresh__c where  Component__c='EmailTemplate'  ];

        if(!gEmailTempList_Of_SandBoxObj.isEmpty()) {
            /* gEmailTempList_Of_SandBoxObjis not - EmailTemplates Name read from Sandbox_Refresh__c */
            for(Sandbox_Refresh__c temp:gEmailTempList_Of_SandBoxObj ) {
                lemailTempNameList.add(temp.Name);
            }
        } else {
            /* gEmailTempList_Of_SandBoxObj is Empty - Emails Templates Hard Coded.
            Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done.*/
            lemailTempNameList.add('Account - Orbis - send to Navision');
            lemailTempNameList.add('Account - Orbis - send OEM to Navision');
            lemailTempNameList.add('PRM: Partner Qualified Orbis Trigger');
            lemailTempNameList.add('PRM: Partner Rejected Trigger');
            lemailTempNameList.add('CCS Trigger for SoE to XL');
            lemailTempNameList.add('Q2O Realign Account Licenses');
            lemailTempNameList.add('KMH - Sandbox Test Template');
        }

        leTObjList = [Select Id,Name,subject from EmailTemplate where name=:lemailTempNameList];
        //if(!EmailTempObjList.isEmpty() )
        {
            //clear and build the new list
            gEmailTempList.Clear();
            gEmailTempList_Backup.Clear();
        }

        for(EmailTemplate  template:leTObjList) {
            if(template.Subject !=NULL) {
                List <String> lSubsplit  = template.Subject.split(' ',2);

                //Add Org id only if its not been added
                if(!lSubsplit[0].equals(gSandboxName)) {
                    template.Subject = gSandboxName + ' ' + template.Subject;
                }
            } else {
                //  template.Subject = gSandboxName;
            }
            gEmailTempList.add(template);
        }

        gEmailTempList_Backup= gEmailTempList.deepClone(true,true,true);

        return gEmailTempList;
    }

    // Method to update EmailTempalte
    public void Update_EmailTempalteRefresh_All() {
        System.debug('Size of gEmailTempList'+gEmailTempList.size());
        for(integer i=0; i<gEmailTempList.size();i++ ) {
            if(!gEmailTempList[i].Subject.equals(gEmailTempList_Backup[i].Subject)) {
                gEmailTempList_Backup[i] = gEmailTempList[i];
            }
        }
        update(gEmailTempList_Backup);
    }

    /*********************End of Email Template Section*************************************/



    /***********************************************************************************
         ********************* Common Functions ***************************************
      ***********************************************************************************/

     /*public list<String> getApexClassList()
     {
       ApexClassInputObjList.add('ulcv3QlikvieCom.cls');
       ApexClassInputObjList.add('efQTPaymentGateway.cls');
       ApexClassInputObjList.add('sharinghandlerQlikviewCom.cls ');
       ApexClassInputObjList.add('qtvoucherQliktechCom.cls');
       ApexClassInputObjList.add('qtecustomsQliktechCom.cls ');
       ApexClassInputObjList.add('Test_Triggers_PRM_Referral.cls ');

       return ApexClassInputObjList ;
     }*/

     /* QlikTech Companies - To create record for Sweden
        need for Sandbox App to run.
     */
    public void InitFunc() {
        // First Create the Sweden company record
        Create_QlikComp();
    }

    public void Create_QlikComp() {
        list  <QlikTech_Company__c> QComp= [Select Name,ID,QlikTech_Company_Name__c,Country_Name__c,QlikTech_Region__c,QlikTech_Sub_Region__c,
            QlikTech_Operating_Region__c,Language__c,SLX_Sec_Code_ID__c
            From QlikTech_Company__c WHERE Name = 'SWE' Limit 1 ];

        if (QComp.isempty()) {
            Subsidiary__c sub = [Select Id from Subsidiary__c where Legal_Country__c = 'Sweden' limit 1];
            QlikTech_Company__c LqComp = new QlikTech_Company__c (
                Name = 'SWE',
                QlikTech_Company_Name__c='Nordics',
                Country_Name__c='Sweden',
                QlikTech_Region__c='Sweden',
                QlikTech_Sub_Region__c='Sweden',
                QlikTech_Operating_Region__c='Sweden',
                Language__c='Swedish',
                SLX_Sec_Code_ID__c    ='SW',
                Subsidiary__c = sub.Id);
            insert LqComp;

            System.Debug('Sweden Company Name added');
        }
    }

    //Creation of depended Campaign, Parent Campaign, CampaignMemberStatus
    public static void runCreationCampaign() {
        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert setting;

        Campaign newParentCampaign = createParentCampaign();
        Campaign newCampaign = createCampaign(newParentCampaign);
        createCampaignMemberStatuses(newCampaign);
        createParentCampaignMemberStatuses(newParentCampaign);
    }

    public static Campaign createCampaign(Campaign parentCampaign) {
        Campaign campaign = new Campaign(
                Name = 'Global-TM-Inbound Call',
                Publishable_Name__c = 'Global TM Inbound Call',
                ParentId = parentCampaign.Id,
                OwnerId = CAMPAIGN_OWNER_ID,
                Type = 'TM - Telemarketing',
                Campaign_Sub_Type__c = 'TM - Telemarketing',
                LOB__c = 'Corporate Marketing',
                Budgeted_Cost__c = 0.00,
                Qchi_OwnerId__c = CAMPAIGN_OWNER_ID,
                IsActive = true,
                Status = 'In Progress',
                Campaign_Audience__c = 'All',
                Description = 'This Campaign is meant to capture all individuals who have an inbound call into our hub.',
                Campaign_Theme__c = 'Cross - Initiative',
                QlikTech_Company__c = 'QlikTech Global',
                StartDate = Date.newInstance(2011, 01, 01),
                EndDate = Date.newInstance(2018, 12, 31), Planned_Leads__c = 0,
                Campaign_Sector__c = 'Cross Sector',
                Campaign_Job_Function__c = 'Other',
                Partner_Involvement__c = 'No Partner',
                RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Marketing Desktop Campaign Layout').getRecordTypeId());
        insert campaign;
        System.debug('!!! MAIN CAMPAIGN IS : ' + campaign);
        System.debug('!!! MAIN CAMPAIGN IS : ' + [SELECT Id, Name, Type,IsActive FROM Campaign WHERE Id = :campaign.Id]);
        return campaign;
    }

    public static Campaign createParentCampaign() {
        Campaign parentCampaign = new Campaign(
                Name = 'GLOBAL WEB',
                Publishable_Name__c = 'GLOBAL WEB',
                OwnerId = CAMPAIGN_OWNER_ID,
                Type = 'DG - Digital',
                Campaign_Sub_Type__c = 'DG - Web',
                LOB__c = 'Corporate Marketing',
                Qchi_OwnerId__c = CAMPAIGN_OWNER_ID,
                IsActive = true,
                Status = 'In Progress',
                Campaign_Audience__c = 'All',
                QlikTech_Company__c = 'QlikTech Australia Pty Ltd;QlikTech Benelux;QlikTech Denmark ApS;QlikTech Finland OY;' +
                        'QlikTech France SARL;QlikTech GmbH;QlikTech Iberia SL;QlikTech Inc;QlikTech International AB;' +
                        'QlikTech Japan KK;QlikTech Nordic AB; QlikTech Norway AS;QlikTech UK Ltd',
                StartDate = Date.newInstance(2011, 01, 01),
                EndDate = Date.newInstance(2018, 12, 31),
                Planned_Leads__c = 0,
                Campaign_Sector__c = 'Cross Sector',
                Campaign_Job_Function__c = 'Other',
                Partner_Involvement__c = 'No Partner',
                RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Marketing Desktop Campaign Layout').getRecordTypeId());
        insert parentCampaign;
        System.debug('!!! PARENT CAMPAIGN IS : ' + parentCampaign);
        System.debug('!!! PARENT CAMPAIGN IS : ' + [SELECT Id, Name, Type,IsActive FROM Campaign WHERE Id = :parentCampaign.Id]);
        return parentCampaign;
    }

    public static void createCampaignMemberStatuses(Campaign campaign) {
        List<CampaignMemberStatus> existCampaignMemberStatus = getExistCampaignMemberStatus(campaign.Id);
        CampaignMemberStatus cms1 = new CampaignMemberStatus(CampaignId = campaign.Id, HasResponded = false, Label = 'Target', SortOrder = 3, IsDefault = true);
        CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId = campaign.Id, HasResponded = false, Label = 'Called Not Reached', SortOrder = 4);
        CampaignMemberStatus cms3 = new CampaignMemberStatus(CampaignId = campaign.Id, HasResponded = false, Label = 'Reached - Not Now', SortOrder = 5);
        CampaignMemberStatus cms4 = new CampaignMemberStatus(CampaignId = campaign.Id, HasResponded = false, Label = 'Reached - Never', SortOrder = 6);
        CampaignMemberStatus cms5 = new CampaignMemberStatus(CampaignId = campaign.Id, HasResponded = true, Label = 'Reached - GI', SortOrder = 7);
        CampaignMemberStatus cms6 = new CampaignMemberStatus(CampaignId = campaign.Id, HasResponded = true, Label = 'Reached - Respond to Offer', SortOrder = 8);
        insert new List<CampaignMemberStatus>{
                cms1, cms2, cms3, cms4, cms5, cms6
        };
        System.debug('!!! CampaignMemberStatus are : ' + cms1 +', '+ cms2 +', '+ cms3 +', '+ cms4 +', '+ cms5+', '+ cms6+', the end');
        System.debug('!!! CampaignMemberStatus IS : ' + [SELECT id, Label, SortOrder, IsDefault from CampaignMemberStatus]);
        delete existCampaignMemberStatus;

    }

    public static void createParentCampaignMemberStatuses(Campaign parentCampaign) {
        List<CampaignMemberStatus> existCampaignMemberStatus = getExistCampaignMemberStatus(parentCampaign.Id);
        CampaignMemberStatus cms1 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = false, Label = 'Target', SortOrder = 3, IsDefault = true);
        CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = false, Label = 'Opened Email', SortOrder = 4);
        CampaignMemberStatus cms3 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = false, Label = 'Clicked Link in Email', SortOrder = 5);
        CampaignMemberStatus cms4 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = true, Label = 'Form Fill Out', SortOrder = 6);
        CampaignMemberStatus cms5 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = false, Label = 'Not Sent Email', SortOrder = 7);
        CampaignMemberStatus cms6 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = false, Label = 'Email Bounced', SortOrder = 8);
        CampaignMemberStatus cms7 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = false, Label = 'Unsubscribed', SortOrder = 9);
        CampaignMemberStatus cms8 = new CampaignMemberStatus(CampaignId = parentCampaign.Id, HasResponded = false, Label = 'xxDoNotUse MKTOTrigger Form Fill Out', SortOrder = 10);
        insert new List<CampaignMemberStatus>{
                cms1, cms2, cms3, cms4, cms5, cms6, cms7, cms8
        };
        System.debug('!!! CampaignParentMemberStatus are : ' + cms1 +', '+ cms2 +', '+ cms3 +', '+ cms4 +', '+ cms5+', '+ cms6+', '+ cms7+', '+ cms8);
        System.debug('!!! CampaignParentMemberStatus IS : ' + [SELECT id, Label, SortOrder, IsDefault from CampaignMemberStatus]);
        delete existCampaignMemberStatus;
    }

    public static List<CampaignMemberStatus> getExistCampaignMemberStatus(Id campaignId) {
        List<CampaignMemberStatus> cmsDelete = new List<CampaignMemberStatus>();
        for (CampaignMemberStatus cms : [
                SELECT Id, Label, CampaignId
                FROM CampaignMemberStatus
                WHERE CampaignId = :campaignId
        ]) {
            if (cms.Label == 'Sent' || cms.Label == 'Responded') {
                cmsDelete.add(cms);
            }
        }
        System.debug('!!! CampaignMemberStatus for deletion are : ' + cmsDelete);
        return cmsDelete;
    }

    // Redirect to Edit page
    public PageReference Edit() {
        Pagereference p;
        p = new Pagereference('/apex/SandboxRefresh_Edit');
        p.setRedirect(true);
        return p;
    }

    // Refresh all the Components
    public PageReference Refresh_All() {
        Pagereference p;
        UpdateWorkFlows();
        UpdateCases();
        Update_EmailTempalteRefresh_All();
        Update_TrainingUsers_All();
        Update_CustomSetting(VfPageUrlValue );

        p = new Pagereference('/apex/SandboxRefresh_Report');
        p.setRedirect(true);
        return p;
    }
    /*********************End of Common Function *************************************/



    /***********************************************************************************
    ********************* Qlikbuy Section***************************************
    ***********************************************************************************/
    // Custom Settings

    public String getVfPageUrlValue() {
        /* Check Sandbox Name and update URL in the
        CPQ Pages(CPQ.page, CPQQuoteEdit.page, CPQQuoteNew.page, Quotes_In_CPQ.page )
        Assign
        QA -> sandbox
        QTDev,QTRelation,QTSysdev  -> s60test
        */
        if( gSandboxName.toUpperCase() == 'LIVE' ) {
            VfPageUrlValue = 'v6';
        }
        else if( gSandboxName.toUpperCase() == 'QA' ) {
            VfPageUrlValue = 'sandbox';
        }
        else {
            VfPageUrlValue = 'v60test';
        }
        return VfPageUrlValue;
    }

    public void setVfPageUrlValue(String nd) {
        VfPageUrlValue = nd;
    }

    @future
    public static void Update_CustomSetting(String lvfPageUrlValue) {
        RefreshSandbox__c rSandb = new RefreshSandbox__c ();
        if(rSandb == null) {
            rSandb = new RefreshSandbox__c (setupOwnerId = System.Userinfo.getOrganizationId());
            rSandb.VFPageUrlValue__c= lvfPageUrlValue;
        }
        else {
            //rSandb =  RefreshSandbox__c.getvalues(System.UserInfo.getOrganizationId());
            rSandb =  RefreshSandbox__c.getinstance(System.UserInfo.getOrganizationId());
            if(rSandb.id == null) {
                System.debug('Kauser Custom setting null');
            } else {
                rSandb.VFPageUrlValue__c= lvfPageUrlValue;
            }
        }
        if(rSandb.id == null) {
            insert rSandb;
        }
        else {
            update(rSandb);
        }
    }
    /*********************End of Qlikbuy Section************************************* /



    /***********************************************************************************
    ********************* Marketo Section***************************************
    ***********************************************************************************/
    //Marketo Configuration
    public Boolean getCheckSandboxName() {
        Boolean MarketoFlag= false;
        if(gSandboxName.toUpperCase() == 'QA') {
            MarketoFlag= true;
        }
        else {
            MarketoFlag = true;
        }
        return MarketoFlag;
    }

    public void updateMarketoConfigPage() {
        // Marketo Config Parameters
        String MarketoEnable= 'true';
        String Marketo_Host = 'https://na-n.marketo.com';
        String Marketo_API_URL= 'https://na-n.marketo.com/soap/mktows';
        String Marketo_API_User_Id='qliktechsandbox4-654-XUG-581';
        String Marketo_Secret_Key ='qliktech23';

        list <mkto_si__Marketo_Sales_Insight_Config__c > stemp = new List<mkto_si__Marketo_Sales_Insight_Config__c>();
        mkto_si__Marketo_Sales_Insight_Config__c stemp1 = new mkto_si__Marketo_Sales_Insight_Config__c();

        stemp = [Select ID,mkto_si__API_Secret_Key__c,mkto_si__API_URL__c,mkto_si__API_User_Id__c,mkto_si__Enable_Marketo_API__c,
            mkto_si__Marketo_Host__c from mkto_si__Marketo_Sales_Insight_Config__c limit 1];

        if(stemp.isEmpty()) {
            stemp1.mkto_si__Enable_Marketo_API__c= true;
            stemp1.mkto_si__Marketo_Host__c = 'https://na-n.marketo.com';
            stemp1.mkto_si__API_URL__c= 'https://na-n.marketo.com/soap/mktows';
            stemp1.mkto_si__API_User_Id__c='qliktechsandbox4-654-XUG-581';
            stemp1.mkto_si__API_Secret_Key__c='qliktech23';

            stemp.add(stemp1);
        }
        else
        {
            stemp[0].mkto_si__Enable_Marketo_API__c= true;
            stemp[0].mkto_si__Marketo_Host__c = 'https://na-n.marketo.com';
            stemp[0].mkto_si__API_URL__c= 'https://na-n.marketo.com/soap/mktows';
            stemp[0].mkto_si__API_User_Id__c='qliktechsandbox4-654-XUG-581';
            stemp[0].mkto_si__API_Secret_Key__c='qliktech23';
        }
        upsert(stemp );
    }

    /*********************End of Marketo Section*************************************/

    /***********************************************************************************
    ********************* Training User Section************************************
    ***********************************************************************************/

    // Training User
    // public static List <Sandbox_Refresh__c> gTrainingUserNameList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();

    // public static List<User> TrainUserList  = new List<User>(); //Global Training user list
    // public static List<User> TrainUserList_ToUpdate  = new List<User>(); //Global Training user list

    List <Sandbox_Refresh__c> gTrainingUserNameList_Of_SandBoxObj = new List<Sandbox_Refresh__c>();

    List<User> TrainUserList  = new List<User>(); //Global Training user list
    List<User> TrainUserList_ToUpdate  = new List<User>(); //Global Training user list

    public List<User> getTrainUserInfo() {
        List<String> lTrainUserList= new List<String>();

        gTrainingUserNameList_Of_SandBoxObj = [Select Name From Sandbox_Refresh__c where  Component__c='User'  ];

        if(!gTrainingUserNameList_Of_SandBoxObj.isEmpty()) {

            /* gTrainingUserNameList_Of_SandBoxObj  not - UserName read from Sandbox_Refresh__c */
            for(Sandbox_Refresh__c temp:gTrainingUserNameList_Of_SandBoxObj) {
                lTrainUserList.add(temp.Name);
            }
        }
        else {
            /* gTrainingUserNameList_Of_SandBoxObj is Empty - User list Hard Coded.
            Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done.*/
            for (integer i=1; i < 16; i++) {
                lTrainUserList.add(String.format(TrainingUserFormatStr, new String[]{''+i})); //Name for Training Users 1-15
            }
        }

        TrainUserList = [SELECT Id,IsActive,Name,ProfileId,Trigger_CPQ_user_creation__c,UserRoleId, Country FROM User WHERE Name =:lTrainUserList];

        for(User tuser:TrainUserList) {
            tuser.ProfileId = '00e20000001OyLc'; //Qlikbuy Sales Std User

            tuser.UserRoleId = '00E20000000vwic' ; // UK - Presales
            tuser.IsActive = true;
            tuser.Trigger_CPQ_user_creation__c = true;
        }

        TrainUserList_ToUpdate = TrainUserList.deepClone(true,true,true);

        return TrainUserList;
    }

    // Update Training Users in Sandbox
    public void Update_TrainingUsers_All() {
        for(integer i=0; i<TrainUserList_ToUpdate.size();i++ ) {
            if(TrainUserList_ToUpdate[i].ProfileId != TrainUserList[i].ProfileId ||
                TrainUserList_ToUpdate[i].UserRoleId != TrainUserList[i].UserRoleId ||
                TrainUserList_ToUpdate[i].IsActive != TrainUserList[i].IsActive ||
                TrainUserList_ToUpdate[i].Trigger_CPQ_user_creation__c != TrainUserList[i].Trigger_CPQ_user_creation__c) {
                    TrainUserList_ToUpdate[i] = TrainUserList[i];
            }
        }
        if(!Test.isRunningTest())
            update(TrainUserList_ToUpdate);
    }



    /*********************End of Training User Section*************************************/

    /***********************************************************************************
    ********************* WorkFlow Section************************************
    ***********************************************************************************/

    //WorkFlow
    public static List <Sandbox_Refresh__c> gWorkFlowList= new List<Sandbox_Refresh__c>();

    public List <Sandbox_Refresh__c> getWorkflowList() {
        List <Sandbox_Refresh__c> lWorkFlowList= new List<Sandbox_Refresh__c>();

        lWorkFlowList = [SELECT Id,isActive__c,Name,Component__c,Value_To_Change__c FROM Sandbox_Refresh__c WHERE Component__c='WorkFlow' ];


        if(!lWorkFlowList.isEmpty()) {
            /* lWorkFlowList is not Empty - WorkFlow Name read from Sandbox_Refresh__c */
            for(Sandbox_Refresh__c temp:lWorkFlowList) {
                gWorkFlowList.Clear();
                gWorkFlowList.add(temp);
            }
        }
        else {
            /* lWorkFlowlist empty so hard code the values-
            Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done. */
            gWorkFlowList.Clear();
            Sandbox_Refresh__c temp = new Sandbox_Refresh__c();
            temp.Name= 'PRM - Partner User Created requires Forecasting setup';
            gWorkFlowList.add(temp);
        }
        return gWorkFlowList;
    }

    // This is API is used to for MetaData API access
    public static MetadataService.MetadataPort createService() {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }


    public static void RemoteSettings() {
        // Salesforce VF page remote setting needs to be setup before we try to deactivate the Workflow from VF page

        MetadataService.MetadataPort service = createService();

        MetadataService.RemoteSiteSetting remoteSiteSettings = new MetadataService.RemoteSiteSetting();
        remoteSiteSettings.fullName = 'SalesforceVFPage';
        //remoteSiteSettings.url = 'https://c.cs8.visual.force.com';
        remoteSiteSettings.url = 'https://test.com';
        remoteSiteSettings.description='Salesforce VF Page Access';
        remoteSiteSettings.isActive=true;
        remoteSiteSettings.disableProtocolSecurity=false;

        MetadataService.AsyncResult[] results0 = service.create(new List<MetadataService.Metadata> { remoteSiteSettings });
        MetadataService.AsyncResult[] checkResults0 = service.checkStatus(new List<string> {string.ValueOf(results0[0].Id)});
        system.debug('RemoteSetting chk' + checkResults0 );
    }

    public static void UpdateWorkFlows() {

        if (gWorkFlowList!= null && gWorkFlowList.size()>0) {
            UpdatePRMWorkFlow( gWorkFlowList[0].isActive__c);
        }
    }
    public static void UpdatePRMWorkFlow(Boolean isActive) {
        //Now Set up WorkFlow that needs to be updated
        MetadataService.MetadataPort service = createService();

        MetadataService.WorkflowRule workflowRule1 = new MetadataService.WorkflowRule();

        workflowRule1.fullName='Contact.PRM - Partner User Created requires Forecasting setup';
        workflowRule1.active=isActive;
        workflowRule1.booleanFilter='1 AND 2 AND 3 AND 4';
        workflowRule1.triggerType='onCreateOrTriggeringUpdate';

        MetadataService.FilterItem wcriteriaItem1 = new MetadataService.FilterItem();
        wcriteriaItem1.field= 'Contact.Allow_Partner_Portal_Access__c';
        wcriteriaItem1.operation= 'equals';
        wcriteriaItem1.value= 'true';


        MetadataService.FilterItem wcriteriaItem2 = new MetadataService.FilterItem();
        wcriteriaItem2.field= 'Contact.Partner_Portal_Status__c';
        wcriteriaItem2.operation= 'contains';
        wcriteriaItem2.value='Complete';


        MetadataService.FilterItem wcriteriaItem3 = new MetadataService.FilterItem();
        wcriteriaItem3.field= 'Contact.ULC_Password__c';
        wcriteriaItem3.operation= 'notEqual';
        wcriteriaItem3.value= '';


        MetadataService.FilterItem wcriteriaItem4 = new MetadataService.FilterItem();
        wcriteriaItem4.field= 'Contact.Premier_Support_Mail_Sent__c';
        wcriteriaItem4.operation= 'equals';
        wcriteriaItem4.value= '';

        workflowRule1.criteriaItems = new list<metadataservice.FilterItem>{wcriteriaItem1,wcriteriaItem2,wcriteriaItem3,wcriteriaItem4  };


        MetadataService.updateMetadata ut = new MetadataService.updateMetadata();
        ut.currentName='Contact.PRM - Partner User Created requires Forecasting setup';
        ut.metadata= workflowRule1;
        MetadataService.AsyncResult[] results = service.updateMetadata(new List<MetadataService.updateMetadata> {ut});

        MetadataService.AsyncResult[] checkResults_workflowRule1 = service.checkStatus(new List<string> {string.ValueOf(results[0].Id)});

        System.debug('Kauser checkResults_workflowRule1: '+ checkResults_workflowRule1 );
    }

    /*********************WorkFlow Section*************************************/

    /***********************************************************************************
    ********************* Case Section************************************
    ***********************************************************************************/


    public static List<Case> gCaseList  = new List<Case>();
    public static List <Sandbox_Refresh__c> gCaseNameList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();

    public static List<ID> gCaseID_To_Delete  = new List<ID>();

    public static List<Case> getCaseList() {
        gCaseList .Clear();

        gCaseList   = [SELECT Id,CaseNumber,Subject,Employee_First_Name__c,Employee_Last_Name__c, RecordTypeId
            From    Case
            WHERE
                Confidential_New_Starter__c ='true'
                and RecordTypeId='01220000000DdyTAAS'
                and isDeleted=false];

        /*  for(Case caseTemp:gCaseList_To_Delete )
        {
            System.debug('Kauser Case:'+ caseTemp.CaseNumber);
            System.debug('Kauser Case:'+ caseTemp.Subject);
        }*/

        return gCaseList  ;
    }


    public static void UpdateCases() {
        for(Case caseTemp:gCaseList) {
            gCaseID_To_Delete.add(caseTemp.Id);
        }
        DeleteCases(gCaseID_To_Delete);
    }

    @future
    public static void DeleteCases(List<Id> idToDelete) {
        List<Case> lCaseList_To_Delete  = new List<Case>();

        lCaseList_To_Delete  = [SELECT Id,CaseNumber,Subject,Employee_First_Name__c,Employee_Last_Name__c From Case WHERE Id =:idToDelete ];

        delete lCaseList_To_Delete  ;

    }
    /*********************Case Section*************************************/
}
