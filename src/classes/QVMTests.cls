/*
@author: Anthony Victorio, Model Metrics
@date: 03/05/2012
@description: Houses all tests units related to the QVM partner portal

slh - Changed profile from PRM - Base + QlikMarket (this is no longer used) to PRM - Base
2015-12-07  |  CCE  |  CR# 40489 Qlik Market - add notice to register to additional pages. Update various tests and improve coverage
2017-02-01  |  CCE  | CR# 103173 - updates tests
2019-03-05  AIN Moved custom settings to QuoteTestHelper
*/
@isTest
private class QVMTests {
  
  static testMethod void QVM_ConfigurationTest() {
    QVM_Configuration con = new QVM_Configuration();
    con.loadDefaults();
    con.saveSettings();
  }

    static testMethod void QVM_CasesTest() {

        QttestUtils.GlobalSetUp();
      
      String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            
        }
        
        Test.startTest();
        
        System.runAs(testUser) {
            QVM_Cases con = new QVM_Cases();
            con.sort();
            con.createNewCase();
            con.goToQVMControlPanel();
        }
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ControlPanelConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        
        Test.startTest();
        
        System.runAs(testUser) {
            
            PageReference pageRef = Page.QVM_ControlPanel;
            Test.setCurrentPage(pageRef);
            
            QVM_ControlPanelCon con = new QVM_ControlPanelCon();
            
            list<QVM_Customer_Products__c> customerlist = con.getCustomerList();
            list<QVM_Product_Data__c> productdata = con.getProductList();
            list<Case> caselist = con.getCaseList();
            
            con.editPartnerProfile();
            con.viewAllCustomers();
            con.exportCustomersExcel();
            con.viewAllProducts();
            con.viewArchivedProducts();
            con.createNewProduct();
            con.viewAllCases();
            con.createNewCase();

        }
        
        System.runAs (thisUser) {
            
            PageReference pageRef = Page.QVM_ControlPanel;
            Test.setCurrentPage(pageRef);
            
            QVM_ControlPanelCon con = new QVM_ControlPanelCon();
            
        }
            
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ControlPanelMainConTest() {

        QttestUtils.GlobalSetUp();
        
        Test.startTest();
        
        QVM_ControlPanelMainCon con = new QVM_ControlPanelMainCon();
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ControlPanelRegistrationConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        Test.startTest();
        
            System.runAs(testUser) {
                
                QVM_ControlPanelRegistrationCon con = new QVM_ControlPanelRegistrationCon();
                String regEdit = con.getRegistrationEditProfileMsg();
                con.submitForm();
                
                con.partnerData.Agreed_to_T_C__c = true;
                con.submitForm();

            }
            
            System.runAs(testUser) {
                
                QVM_ControlPanelRegistrationCon con = new QVM_ControlPanelRegistrationCon();
                String regSubmit = con.getRegistrationSubmittedMsg();
                
            }
                
        
        Test.stopTest();

    }
    
    static testMethod void QVM_CreateCaseTest() {

        QttestUtils.GlobalSetUp();

        Test.startTest();
      
        QVM_CreateCaseCon con = new QVM_CreateCaseCon();
        con.cancel();
        con.saveCase();
      
        Test.stopTest();
      
    }
    
    static testMethod void QVM_CustomerDetailConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        QVM_Customer_Products__c testCustomer = QVMTest.newCustomerProduct(testContact.Id, null, testPartner.Id, testProduct.Id);
        insert testCustomer;
        
        Test.startTest();
        
            PageReference pageRef = Page.QVM_CustomerDetail;
            pageRef.getParameters().put('id',testCustomer.Id);
            Test.setCurrentPage(pageRef);
            
            QVM_CustomerDetailCon con = new QVM_CustomerDetailCon();
            
            con.convertToLead();
            con.viewAllCustomers();
            con.viewQlikMarket();
        
        Test.stopTest();
        
    }

    static testMethod void QVM_CustomersConTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        QVM_Customer_Products__c testCustomer = QVMTest.newCustomerProduct(testContact.Id, null, testPartner.Id, testProduct.Id);
        insert testCustomer;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            testPartner.Approved__c = true;
            update testPartner;
        }
        
        Test.startTest();
            
            System.runAs(testUser) {
                QVM_CustomersCon con = new QVM_CustomersCon();
                
                con.customers[0].selected = true;
                con.createLead();
                
                con.addTableFilter();
                con.clearTableFilter();

                con.exportCustomersExcel();
                con.goToQVMControlPanel();
                list<QVM_CustomersCon.customerWrapper> customers = con.getCustomerList();
                
                con.startDate = '05/27/2012';
                con.endDate = '05/27/2012';
                con.addTableFilter();
                con.sort();
                con.clearTableFilter();
                
                con.firstPage();
                con.prevPage();
                con.nextPage();
                con.lastPage();

                PageReference pageRef = con.checkRegistration();
            }
                
        
        Test.stopTest();

    }
    
    
    static testMethod void QVM_GlobalTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        Test.startTest();
        
            QVM_Global.archiveMagentoProduct(testProduct.Id);
            QVM_Global.enableMagentoProduct(testProduct.Id);
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_PartnerProfileTest1() {

        QttestUtils.GlobalSetUp();
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        testPartner.Approved__c = true;
        update testPartner;

        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;

        Test.startTest();
            System.runAs (testUser) {
                QVM_PartnerProfile con = new QVM_PartnerProfile();
                con.savePartner();
                
                QVM_PartnerProfile con2 = new QVM_PartnerProfile();
                con2.cancel();
            }
            
        Test.stopTest();
        
    }
    static testMethod void QVM_PartnerProfileTest2() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            testPartner.Approved__c = true;
            update testPartner;
        }
        
        Test.startTest();

            System.runAs (testUser) {
                PageReference pageRef = Page.QVM_PartnerProfile;
                pageRef.getParameters().put('id',testPartner.Id);
                Test.setCurrentPage(pageRef);
                
                QVM_PartnerProfile con = new QVM_PartnerProfile();
                
                con.partner.Partner_Website__c = 'www.google.com/';
                con.savePartner();
                
                con.partner.Support_URL__c = 'www.google.com/';
                con.savePartner();
                
                con.partner.Partner_Website__c = 'http://www.google.com/';
                con.partner.Support_URL__c = 'http://www.google.com/';
                con.savePartner();
                
                con.cancel();

                pageRef = con.checkRegistration();
                
            }
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ProductBuilderStep1TestA() {

        QttestUtils.GlobalSetUp();
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        testPartner.Approved__c = true;
        update testPartner;

        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        Test.startTest();
            System.runAs (testUser) {
                QVM_ProductBuilderStep1 con = new QVM_ProductBuilderStep1();
                con.saveProduct();
                con.nextPage();
            }
        
        Test.stopTest();

    }
    
    static testMethod void QVM_ProductBuilderStep1TestB() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        testPartner.Approved__c = true;
        update testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        String profileId = QVMTest.getProfileId('PRM - Base');

        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        Test.startTest();
        System.runAs (testUser) {
            PageReference pageRef = Page.QVM_ProductBuilderStep1;
            pageRef.getParameters().put('id',testProduct.Id);
            Test.setCurrentPage(pageRef);

            QVM_ProductBuilderStep1 con = new QVM_ProductBuilderStep1();
            con.saveProduct();
            con.product.Short_Description__c = '<strong>something</strong>';
            con.saveProduct();
            con.product.Short_Description__c = 'something';
            con.product.Overview__c = 'Something';
            con.product.Ajax_Demo_Link__c = 'www.google.com';
            con.saveProduct();
            con.product.Ajax_Demo_Link__c = 'http://www.google.com/';
            con.saveProduct();
            con.nextPage();
            con.cancel();

            pageRef = con.checkRegistration();
        }
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ProductBuilderStep2Test() {

        QttestUtils.GlobalSetUp();

        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        Test.startTest();
        
            PageReference pageRef = Page.QVM_ProductBuilderStep2;
            pageRef.getParameters().put('id',testProduct.Id);
            Test.setCurrentPage(pageRef);
            
            QVM_ProductBuilderStep2 con = new QVM_ProductBuilderStep2();
            
            try {
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.ProductTypeId = '15';
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.IndustryIds = new String[] {'15'};
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.FunctionIds = new String[] {'15'};
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            try {
                con.VersionIds = new String[] {'15'};
                con.checkForExceptions();
            } catch (Exception e) {
                System.debug(e);
            }
            
            con.saveProduct();
            con.backPage();
            con.nextPage();
            con.previewPage();

            //for coverage as this function does not appear to be used
            String rets = QVM.htmlEncoder('Test');
            //for coverage
            rets = QVM.getLineBreaksToBR('Test');

        Test.stopTest();
        
        
    }
    
    static testMethod void QVM_ProductBuilderStep3Test() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        Test.startTest();
        
        PageReference pageRef = Page.QVM_ProductBuilderStep3;
        pageRef.getParameters().put('id',testProduct.Id);
        Test.setCurrentPage(pageRef);
        
        QVM_ProductBuilderStep3 con = new QVM_ProductBuilderStep3();
        
        String policy = con.getPolicy();
        String signedPolicy = con.getSignedPolicy();
        
        list<SelectOption> productmethods = con.getProductMethodOptions();
        
        con.product.Referral_Link__c = null;
        con.quickSave();
        
        con.product.Referral_Link__c = 'google.com';
        con.quickSave();
        
        con.product.Referral_Link__c = 'http://www.google.com/';
        con.quickSave();
        
        con.removeProductReferral();
        
        QVM_ProductBuilderStep3.setupFileName('product.zip', testProduct.Id);
        
        con.alternateUploadFile();
        con.removeProductFile();
        
        con.backPage();
        con.saveProduct();
        con.nextPage();
        
        con.product.Referral_Link__c = null;
        con.product.Product_Download_Link__c = null;
        con.nextPage();
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ProductBuilderStep4Test() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        Test.startTest();
        
        PageReference pageRef = Page.QVM_ProductBuilderStep4;
        pageRef.getParameters().put('id',testProduct.Id);
        Test.setCurrentPage(pageRef);

        QVM_ProductBuilderStep4 con = new QVM_ProductBuilderStep4();
        
        list<QVM_ProductBuilderStep4.fileWrapper> images = con.images;
        
        QVM_ProductBuilderStep4.fileWrapper image = new QVM_ProductBuilderStep4.fileWrapper();
        image.fileName = 'image1.jpg';
        image.fileSize = 1024;
        image.fileBody = Blob.valueOf('123456789');
        String dBody = image.docBody();
        
        con.image1 = image;
        con.image2 = image;
        con.image3 = image;
        con.image4 = image;
        con.image5 = image;
        
        con.uploadImage1();
        con.uploadImage2();
        con.uploadImage3();
        con.uploadImage4();
        con.uploadImage5();
        
        con.removeImage1();
        con.removeImage2();
        con.removeImage3();
        con.removeImage4();
        con.removeImage5();
        
        con.backPage();
        con.saveProduct();
        con.nextPage();
        
        Test.stopTest();
        
    }
    static testMethod void QVM_ProductPreviewConExceptionsTest() {

        QttestUtils.GlobalSetUp();
      
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        Test.startTest();
        
        System.runAs (testUser) {
          QVM_ProductPreviewCon con = new QVM_ProductPreviewCon();
            con.submitApproval();
            con.performPublishToMagento();
            con.editProduct();
        }
        
        Test.stopTest();

    }
    static testMethod void QVM_ProductPreviewConTest() {

        QttestUtils.GlobalSetUp();
      
      String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        insert testProduct;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
        }
        
        Test.startTest();
        
        System.runAs(testUser) {
          PageReference pageRef = Page.QVM_ProductPreview;
            pageRef.getParameters().put('id',testProduct.Id);
            Test.setCurrentPage(pageRef);
            
            QVM_ProductPreviewCon con = new QVM_ProductPreviewCon();
            con.product.Overview__c = 'some kind of overview';
            con.product.Short_Description__c = 'some kind of description';
            con.product.Type__c = 'Application';
            con.product.Industry__c = 'Education';
            con.product.Job_Function__c = 'Executive';
            con.product.Version__c = '1.0';
            con.product.QlikView_Version__c = '9.0';
            con.product.Referral_Link__c = 'http://www.google.com/';
            
            con.editProduct();
            con.goToQVMControlPanel();
            con.submitApproval();
            con.performPublishToMagento();
            
            con.product.Product_Download_Link__c  = 'http://qlikview-market-dev.s3.amazonaws.com/live/product.zip';
            con.product.Image_1__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image1.jpg';
            con.product.Image_2__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image2.jpg';
            con.product.Image_3__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image3.jpg';
            con.product.Image_4__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image4.jpg';
            con.product.Image_5__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image5.jpg';
            
            con.performPublishToMagento();
        }
        
        Test.stopTest();
        
    }
    static testMethod void QVM_ProductsTest() {

        QttestUtils.GlobalSetUp();
        
        String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        String profileId = QVMTest.getProfileId('PRM - Base');
        
        User testUser = QVMTest.newUser(profileId, testContact.Id);
        insert testUser;
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            testUser.IsPortalEnabled = true;
            update testUser;
            testPartner.Approved__c = true;
            update testPartner;
        }
        
        Test.startTest();
            testUser = [select Id, IsPortalEnabled from User where Id = :testUser.Id];
            System.runAs (testUser) {
                QVM_Products con = new QVM_Products();
                con.sort();
                con.goToQVMControlPanel();

                PageReference pageRef = con.checkRegistration();
            }

        Test.stopTest();
        
    }
    
    
    static testMethod void QVM_TableTest() {

        QttestUtils.GlobalSetUp();
        
        Test.startTest();
        
        
        
        Test.stopTest();
        
    }
    
    static testMethod void QVMTest() {

        QttestUtils.GlobalSetUp();
        
        Test.startTest();
        
        String[] arrayFromString = QVM.getArrayFromString('something;something', ';');
        
        list<SelectOption> options = new list<SelectOption> {
          new SelectOption('20','something')
        };
        
        map<String,String> catMap = QVM.getMagentoCategoryMap(options);
        
        Test.stopTest();
        
    }
    
    static testMethod void QVM_ProductDataTriggersTests() {

        QttestUtils.GlobalSetUp();
      
      String accountRecordTypeId = QVMTest.getRecordType('Account', 'Partner Account');
        
        Account testAccount = QVMTest.newAccount(accountRecordTypeId, 'Test Account');
        insert testAccount;
        
        String contactRecordTypeId = QVMTest.getRecordType('Contact', 'Business Contact');
        
        Contact testContact = QVMTest.newContact(contactRecordTypeId, testAccount.Id);
        insert testContact;
        
        QVM_Partner_Data__c testPartner = QVMTest.newPartnerData(testAccount.Id, testContact.Id);
        insert testPartner;
        
        QVM_Product_Data__c testProduct = QVMTest.newProductData(testPartner.Id);
        testProduct.Status__c = 'Draft';
        testProduct.Overview__c = 'some kind of overview';
        testProduct.Short_Description__c = 'some kind of description';
        testProduct.Type__c = 'Application';
        testProduct.Industry__c = 'Education';
        testProduct.Job_Function__c = 'Executive';
        testProduct.Version__c = '1.0';
        testProduct.QlikView_Version__c = '9.0';
        testProduct.Referral_Link__c = 'http://www.google.com/';
        testProduct.Product_Download_Link__c  = 'http://qlikview-market-dev.s3.amazonaws.com/live/product.zip';
        testProduct.Image_1__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image1.jpg';
        testProduct.Image_2__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image2.jpg';
        testProduct.Image_3__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image3.jpg';
        testProduct.Image_4__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image4.jpg';
        testProduct.Image_5__c = 'http://qlikview-market-dev.s3.amazonaws.com/live/image5.jpg';
        insert testProduct;
        
        testProduct.Status__c = 'Ready to Publish';
        update testProduct;
        
        testProduct.Magento_Product_Id__c = '1';
        testProduct.Status__c = 'Draft';
        update testProduct;
        
        testProduct.Status__c = 'Ready to Publish';
        update testProduct;
        
    }
    
}