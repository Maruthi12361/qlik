public class TimecardController_QC {

    @AuraEnabled
    public static String getBaseURLForCommunity(){
        return Network.getSelfRegUrl(Network.getNetworkId()).substringBefore('/QlikCommerce/s/login/SelfRegister');
    }
}