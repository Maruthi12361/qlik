/************************************************************************
*
*   ClearProductsAndPricebookController
*   
*   This controller is reached from a button (via a pass-through vf page) and is used to
*   convert an non-Qlikbuy II Opp to the Qlikbuy II Opp type by removing any products, resetting the Pricebook 
*   and changing the Opp Record Type to Qlikbuy II. 
*       
*   Changelog:
*       2013-09-23  CCE     Initial development. CR 7524 https://eu1.salesforce.com/a0CD000000XN6U6
*       2013-11-28  SLH     Should run even when pricebook is null
*                           Updated logic to map existing amount fields to new forecast amount fields                 
*       2014-07-24  MAW     If closed date is already populated then remove that otherwise it will close the opportunity.         
*       2016-06-08    Andrew Lokotosh Commented Forecast_Amount fields line   52,59,64,65
		2016-09-21  MTM disabled fo Q2CW 
*************************************************************************/
public with sharing class ClearProductsAndPricebookController {

    public ClearProductsAndPricebookController() {
        
    }
/*
    public Opportunity Opp {get; private set;}

    public ClearProductsAndPricebookController(ApexPages.StandardController stdController) {
        this.Opp = (Opportunity)stdController.getRecord();
        //Get the stuff we need to get at OpportunityLineItems for this Opportunity.
        this.Opp = [SELECT Id, Amount, Consultancy_Forecast_Amount__c, Education_Forecast_Amount__c, Total_Training_Amount__c, Is_Services_Opportunity__c, RecordTypeId, Pricebook2Id,Closed_date_for_conga__c, (select Id, OpportunityId, Name__c, PricebookEntryId, UnitPrice from OpportunityLineItems) FROM Opportunity WHERE Id = :Opp.Id LIMIT 1];               
    }
    
    public pagereference doTheClear() // do the clear
    {
        //Get some previously stored Id's - these are from a Custom Settings object so we can move between Orgs easily.                        
        Id ID_NS_Pricebook;
        if(NS_Settings_Detail__c.getInstance('NSSettingsDetail') != null) {
            ID_NS_Pricebook = NS_Settings_Detail__c.getInstance('NSSettingsDetail').NSPricebookRecordTypeId__c;
        }
        System.debug('ClearProductsAndPricebookController ID_NS_Pricebook is: ' + ID_NS_Pricebook);
        
        Id ID_QB2_OppRecordType;
        // SLH - new var for Satandalone Services opps
        Id ID_QB2_Service_OppRecordType;
        if(NS_Settings_Detail__c.getInstance('NSSettingsDetail') != null) {
            ID_QB2_OppRecordType = NS_Settings_Detail__c.getInstance('NSSettingsDetail').Qlikbuy_II_Opp_Record_Type__c;
            ID_QB2_Service_OppRecordType = NS_Settings_Detail__c.getInstance('NSSettingsDetail').Qlikbuy_II_Opp_Record_Type_Service__c;
        }
        System.debug('ClearProductsAndPricebookController ID_QB2_OppRecordType is: ' + ID_QB2_OppRecordType);
               

        // SLH - if ((Opp.Pricebook2Id != null) && (Opp.Pricebook2Id != ID_NS_Pricebook))
        if ((Opp.Pricebook2Id == null) || (Opp.Pricebook2Id != ID_NS_Pricebook)) { //Pricebook not set to "NS Pricebook so we need to do some work"
            //Check if we already have Products added - if so we want to delete them
            list<OpportunityLineItem> existing_oli = Opp.OpportunityLineItems;
            if (existing_oli.size() > 0) {
                System.debug('ClearProductsAndPricebookController existing_oli.size() is: ' + existing_oli.size());
                delete existing_oli;    //delete the existing products       
            }
            if(Opp.Is_Services_Opportunity__c == true){
                Opp.RecordTypeId = ID_QB2_OppRecordType;    //Change the Opp Record Type to Qlikbuy II Standalone services
               // Opp.Consultancy_Forecast_Amount__c = Opp.Amount; //Add a value to the LFA to avoid the trigger generated error we get when we try to save an Opp with no forecast amount
            }    
            //else if (Opp.Is_Onsite_Training_Opp__c == 'Yes'){
               // Opp.RecordTypeId = ID_QB2_OppRecordType;    //Change the Opp Record Type to Qlikbuy II Standalone services
               // Opp.Education_Forecast_Amount__c = Opp.Amount;    //Add a value to the LFA to avoid the trigger generated error we get when we try to save an Opp with no forecast amount
                // Did not use Opp.Total_Training_Amount__c here as this applies to QB opps only - onsite trainign is not QB
            //}
            else {
                Opp.RecordTypeId = ID_QB2_OppRecordType;    //Change the Opp Record Type to Qlikbuy II
                //Opp.License_Forecast_Amount__c = Opp.Amount;    //Add a value to the LFA to avoid the trigger generated error we get when we try to save an Opp with no forecast amount
                //Opp.Education_Forecast_Amount__c = Opp.Total_Training_Amount__c;
            }

            //MAW - 2014-07-24- if closed date is already populated then remove that otherwise it will close the opportunity.
            if(opp.Closed_date_for_conga__c != null){
                opp.Closed_date_for_conga__c = null;
            }
            
            //Clear pricebook after the above as Is_Onsite_Training_Opp__c is formula field based on pricebook
            Opp.Pricebook2Id = null;    //remove the Pricebook
            //Opportunity_ManageForecastProductsHasRun may have already run so we need to clear the flags as we want it to run again for the update - this adds the Amount to the LFA field
            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
            System.debug('ClearProductsAndPricebookController Before update 1');
            update Opp; //Update the Opp
            //Opportunity_ManageForecastProductsHasRun has already run so we need to clear the flags as we want it 
            //to run again for the update - this will set the NS priocebook and create the product
            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = false;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After = false;
            System.debug('ClearProductsAndPricebookController Before update 2');
            update Opp; //Update the Opp
        }
        
        Pagereference p;        
        return new PageReference('/'+ Opp.Id);
    }
	*/
}