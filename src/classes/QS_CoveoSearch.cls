/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
* 2019-11-21 AIN IT-2320 Added filter expression for Coveo
************************************************************/
public class QS_CoveoSearch {
    public Contact userContact { get; set; }
    public String accounttype { get; set; }
    public String loginUrl { get; set; }
    public string CoveoFilterExpression { get; set; }
    
    public QS_CoveoSearch() {
        User userRec;
        if (UserInfo.getUserId() != null) {
            userRec = [
                    SELECT Id, ContactId, Contact.AccountId
                    FROM User
                    WHERE Id = :UserInfo.getUserId()
            ];
        }
        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        
        if (UserInfo.getUserId() == null || userRec == null || userRec.ContactId == null || userRec.Contact.AccountId == null) {
            String LoginPageURL = partnerPortalURLs.Support_Portal_Login_Page_Url__c;
            loginUrl = LoginPageURL + '?u=' + URL.getSalesforceBaseUrl().toExternalForm() + '/QS_CoveoSearch';
        }
        CoveoFilterExpression  = partnerPortalURLs.Coveo_Filter_Expression__c;                   

        try{
            accounttype= [
                Select Account_Type__c
                From Contact
                Where Id In (Select ContactId
                             From User
                             Where Id = :UserInfo.getUserId())][0].Account_Type__c;
        }
        catch(Exception ex) {}
    }

}