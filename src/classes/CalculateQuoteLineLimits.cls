/**
* Class: CalculateQuoteLineLimits
* @description Class to calculate quote line limits so as to not overload NS
* 
* Changelog:
*    2017-11-17 : UIN Ramakrishna Kini QCW-4282 : Initial development.
*    2018-01-19 : UIN Ramakrishna Kini QCW-4282 : Modified to show errors on save.
*    
*/
public with sharing class CalculateQuoteLineLimits {
    public CalculateQuoteLineLimits() {
        
    }


    public static void onBeforeInsert(List<SBQQ__QuoteLine__c> triggerNew) {
        Q2CWSettings__c qSettings = Q2CWSettings__c.getInstance();
        Map<Id, Integer> mParentId = new Map<Id, Integer>();// Map of child record parent id and the corresponding quantity
        Map<String, String> mParentNameChAttrib = new Map<String, String>();// Map of parent record names and the corresponding no charge attributes 
        String concateCharNoChar ='';
        Integer totChargeUnits = 0;
        List<String> lIndAttributes = new List<String>();
        Decimal totQlUnits =0;
        Map<Id, List<SBQQ__QuoteLine__c>> mQIdQLine = new Map<Id, List<SBQQ__QuoteLine__c>>();
        Set<Id> sQuoteIds = new Set<Id>();
        Set<Id> sProductIds = new Set<Id>();
        for(SBQQ__QuoteLine__c ql:triggerNew){
        	system.debug('aaaa'+ql.SBQQ__Description__c);
        	system.debug('aaaa1'+ql.SBQQ__Product__c);
            if('Licenses'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c) || 'Subscription'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c)){
                if(String.isNotBlank(ql.SBQQ__Quote__c))
                    sQuoteIds.add(ql.SBQQ__Quote__c);
                if(String.isNotBlank(ql.SBQQ__Product__c))
                    sProductIds.add(ql.SBQQ__Product__c);
            }
        }
        List<Product2> lProducts = [select id,SBQQ__Component__c from Product2 where id in :sProductIds];
        Map<Id, Boolean> mProdIdsComps = new Map<Id, Boolean>();
        for(Product2 prod: lProducts){
        	if(!mProdIdsComps.containsKey(prod.Id))
        		mProdIdsComps.put(prod.Id, prod.SBQQ__Component__c);
        }

        System.debug('QLines42: Inside before Insert'+sQuoteIds);
        //Parent Quote Lines Loop
        for(SBQQ__QuoteLine__c ql:triggerNew){
            system.debug('UIN Added 7'+ ql.SBQQ__ProductFamily__c);
            if('Licenses'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c) || 'Subscription'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c)){
                system.debug('UIN Added 6'+ String.isBlank(ql.SBQQ__RequiredBy__c));
                system.debug('UIN Added 8'+ calcUnits(ql));
                if(String.isBlank(ql.SBQQ__RequiredBy__c) && !mProdIdsComps.get(ql.SBQQ__Product__c)){
                    ql.Total_Units_Per_Call__c = ql.SBQQ__Quantity__c * (25 + calcUnits(ql));
                }else if(String.isBlank(ql.SBQQ__RequiredBy__c) && mProdIdsComps.get(ql.SBQQ__Product__c)){
                    ql.Total_Units_Per_Call__c = (calcUnits(ql));
                    /*Commenting out as quantity already comes updated
                    if(ql.Parent_Quantity__c > 1)
                        ql.Total_Units_Per_Call__c = ql.Parent_Quantity__c * ql.Total_Units_Per_Call__c;*/
                }
                system.debug('UIN Added 9'+ ql.Total_Units_Per_Call__c);
                if(mQIdQLine.containsKey(ql.SBQQ__Quote__c))
                    mQIdQLine.get(ql.SBQQ__Quote__c).add(ql);
                else if(!mQIdQLine.containsKey(ql.SBQQ__Quote__c))
                    mQIdQLine.put(ql.SBQQ__Quote__c, new List<SBQQ__QuoteLine__c>{ql});
            }
        }
        Boolean limitCrossed = false;
        Id errorQid; 
        if(!sQuoteIds.isEmpty()){
            for(Id qid: sQuoteIds){
                if(!mQIdQLine.isEmpty() && mQIdQLine.containsKey(qid)){
                    for(SBQQ__QuoteLine__c ql: mQIdQLine.get(qid)){
                         totQlUnits +=ql.Total_Units_Per_Call__c; 
                    }
                    if(totQlUnits > qSettings.Max_Units_Consumed_Limit__c){
                        limitCrossed = true;
                        totQlUnits = 0;
                        errorQid = qId;
                        break;
                    }else
                        totQlUnits = 0;  
                }
            }
        }
        
        if(limitCrossed){
            
            limitCrossed = false;
            for(SBQQ__QuoteLine__c ql:triggerNew){
                if(ql.SBQQ__Quote__c == errorQid){
                    ql.addError(qSettings.Max_Units_Hit_Error_Messag__c);
                    errorQid = null;
                }
            }
        }           
    }

    public static void onBeforeUpdate(List<SBQQ__QuoteLine__c> triggerNew, List<SBQQ__QuoteLine__c> triggerOld, Map<ID, SBQQ__QuoteLine__c> triggerNewMap, Map<ID, SBQQ__QuoteLine__c> triggerOldMap) {
        Q2CWSettings__c qSettings = Q2CWSettings__c.getInstance();
        System.debug('QLines22: Inside before update');
        Map<Id, Decimal> mParentQLIdQty = new Map<Id, Decimal>();
        Set<Id> sQuoteIds = new Set<Id>();
        for(SBQQ__QuoteLine__c ql:triggerNew){
        	system.debug('bbbb1'+ql);
            if('Licenses'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c) || 'Subscription'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c)){
                if(String.isNotBlank(ql.SBQQ__Quote__c))
                    sQuoteIds.add(ql.SBQQ__Quote__c);
            }
        }
        List<SBQQ__QuoteLine__c> lAllQuoteLines = new List<SBQQ__QuoteLine__c>();
        Map<Id, List<SBQQ__QuoteLine__c>> mChgQIdQLine = new Map<Id, List<SBQQ__QuoteLine__c>>();//Map containing quote line items whose quantities are changed along with respective quote ids
        Map<Id, List<SBQQ__QuoteLine__c>> mUnChgQIdQLine = new Map<Id, List<SBQQ__QuoteLine__c>>();//Map containing existing quote line items whose quantities are not changed along with respective quote ids
        lAllQuoteLines = [select id, Total_Units_Per_Call__c, SBQQ__Quote__c from SBQQ__QuoteLine__c where SBQQ__Quote__c in :sQuoteIds and (SBQQ__ProductFamily__c ='Licenses' or SBQQ__ProductFamily__c ='Subscription')];
        system.debug('UIN Added 55'+ lAllQuoteLines.size());
        if(!lAllQuoteLines.isEmpty()){
            system.debug('UIN Added 56'+ lAllQuoteLines);
            for(SBQQ__QuoteLine__c ql: lAllQuoteLines){
                if(!triggerNewMap.containsKey(ql.id)){
                    if(mUnChgQIdQLine.containsKey(ql.SBQQ__Quote__c))
                        mUnChgQIdQLine.get(ql.SBQQ__Quote__c).add(ql);
                    else if(!mUnChgQIdQLine.containsKey(ql.SBQQ__Quote__c))
                        mUnChgQIdQLine.put(ql.SBQQ__Quote__c, new List<SBQQ__QuoteLine__c>{ql});    
                }
            }
        }
        system.debug('UIN Added 57'+ mUnChgQIdQLine);
        for(SBQQ__QuoteLine__c ql:triggerNew){
            if('Licenses'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c) || 'Subscription'.equalsIgnoreCase(ql.SBQQ__ProductFamily__c)){
                system.debug('UIN Added 9'+ ql.Id + triggerOldMap.get(ql.Id).SBQQ__Quantity__c);
                if((ql.SBQQ__Quantity__c != triggerOldMap.get(ql.Id).SBQQ__Quantity__c) && String.isBlank(ql.SBQQ__RequiredBy__c)){
                    if(!mParentQLIdQty.containsKey(ql.Id))
                        mParentQLIdQty.put(ql.Id, ql.SBQQ__Quantity__c);
                }
                if(ql.SBQQ__Quantity__c != triggerOldMap.get(ql.Id).SBQQ__Quantity__c){
                    System.debug('QLines32: Inside Child lines calculate');
                    if(String.isBlank(ql.SBQQ__RequiredBy__c)){
                    	system.debug('QLines91: Inside Child lines calculate'+ql.Id+ql.SBQQ__RequiredBy__c);
                        ql.Total_Units_Per_Call__c = ql.SBQQ__Quantity__c * (25 + calcUnits(ql));
                        system.debug('QLines92: Inside Child lines calculate'+ql.Total_Units_Per_Call__c);
                    }else if(String.isNotBlank(ql.SBQQ__RequiredBy__c)){
                    	system.debug('QLines93: Inside Child lines calculate'+ql.Id+ql.SBQQ__RequiredBy__c);
                        if(!mParentQLIdQty.isEmpty() && mParentQLIdQty.containsKey(ql.SBQQ__RequiredBy__c))
                            ql.Total_Units_Per_Call__c = mParentQLIdQty.get(ql.SBQQ__RequiredBy__c) * (calcUnits(ql));
                    	system.debug('QLines94: Inside Child lines calculate'+ql.Total_Units_Per_Call__c);
                    }
                }
                
                if(mChgQIdQLine.containsKey(ql.SBQQ__Quote__c)){
                	List<SBQQ__QuoteLine__c> lQuoteLines = mChgQIdQLine.get(ql.SBQQ__Quote__c);
                    lQuoteLines.add(ql);
                    mChgQIdQLine.put(ql.SBQQ__Quote__c, lQuoteLines);
                }
                else if(!mChgQIdQLine.containsKey(ql.SBQQ__Quote__c))
                    mChgQIdQLine.put(ql.SBQQ__Quote__c, new List<SBQQ__QuoteLine__c>{ql});
            }
        }
        system.debug('UIN Added 58'+ mChgQIdQLine);
        Boolean limitCrossed = false;
        Id errorQid; 
        Decimal totQlUnits =0;
        if(!sQuoteIds.isEmpty()){
            for(Id qid: sQuoteIds){
                //Summing up units consumed totals for line items whose quantities are changed
                if(!mChgQIdQLine.isEmpty() && mChgQIdQLine.containsKey(qid)){
                    for(SBQQ__QuoteLine__c ql: mChgQIdQLine.get(qid)){
                         totQlUnits +=ql.Total_Units_Per_Call__c; 
                    }
                    system.debug('UIN Added 59'+ totQlUnits);
                    if(totQlUnits > qSettings.Max_Units_Consumed_Limit__c){
                        limitCrossed = true;
                        totQlUnits = 0;
                        errorQid = qId;
                        break;
                    }  
                }
                system.debug('UIN Added 60'+ mUnChgQIdQLine);
                //Summing up units consumed totals for line items whose quantities are changed along with the existing quote line items
                if(!mUnChgQIdQLine.isEmpty() && mUnChgQIdQLine.containsKey(qid)){
                    system.debug('UIN Added 61'+ totQlUnits);
                    for(SBQQ__QuoteLine__c ql: mUnChgQIdQLine.get(qid)){
                         totQlUnits +=ql.Total_Units_Per_Call__c; 
                    }
                    system.debug('UIN Added 62'+ totQlUnits);
                    if(totQlUnits > qSettings.Max_Units_Consumed_Limit__c){
                        limitCrossed = true;
                        totQlUnits = 0;
                        errorQid = qId;
                        break;
                    }  
                }
                totQlUnits = 0;
            }
        }
        system.debug('UIN Added 63'+ limitCrossed+errorQid);
        if(limitCrossed){
            limitCrossed = false;
            for(SBQQ__QuoteLine__c ql:triggerNew){
                if(ql.SBQQ__Quote__c == errorQid){
                    ql.addError(qSettings.Max_Units_Hit_Error_Messag__c);
                    errorQid = null;
                }
            }
        }
    }


    private static Integer calcUnits(SBQQ__QuoteLine__c ql){
        String concateCharNoChar ='';
        Integer totChargeUnits = 0;
        List<String> lIndAttributes = new List<String>();
        if(String.isNotBlank(ql.LEF_Text__c))
            concateCharNoChar += ql.LEF_Text__c;
        if(String.isNotBlank(ql.LEF_NoCharge_Formula_1__c))
                concateCharNoChar += ql.LEF_NoCharge_Formula_1__c;
        if(String.isNotBlank(ql.LEF_NoCharge_Formula_2__c))
            concateCharNoChar += ql.LEF_NoCharge_Formula_2__c;
        if(String.isNotBlank(ql.LEF_NoCharge_Formula_3__c))
            concateCharNoChar += ql.LEF_NoCharge_Formula_3__c;
        system.debug('UIN Added 3'+ concateCharNoChar);
        if(String.isNotBlank(concateCharNoChar)){
            for(String s:concateCharNoChar.split(';'))
                lIndAttributes.add(s);
        }
        system.debug('UIN Added 4'+ lIndAttributes);
        if(!lIndAttributes.isEmpty()){
            totChargeUnits = 10* lIndAttributes.size();
        }
        system.debug('UIN Added 4'+ totChargeUnits);
        concateCharNoChar = '';
        lIndAttributes = new List<String>();
        return totChargeUnits;
    }  
}