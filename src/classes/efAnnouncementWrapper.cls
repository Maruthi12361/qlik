//*********************************************************/
// Author: Mark Cane&
// Creation date: 23/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efAnnouncementWrapper{
	private Announcement__c a;
			
	public efAnnouncementWrapper(Announcement__c a) { this.a=a; }	
	public Announcement__c getAnnouncement() { return a; }

	public String getFormatedTime(){
     	DateTime dt = a.LastModifiedDate ;     	
     	String formatedDate = '';
     	if(dt!=null)
     		formatedDate = dt.format('MM/dd/yyyy') + ' - ' +  dt.format('H:mm a');
    	return formatedDate;
   	}	
     
   	public String getAnnouncementName(){
    	return efUtility.giveValue(this.a.Name) ;     	
   	}
     
   	public String getAnnouncementText(){
    	return efUtility.giveValue(this.a.Announcement_text__c);
    }       
}