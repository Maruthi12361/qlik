@isTest
private class Test_Triggers_toAdd {

    /******************************************************

        test_Opportunity_MandatorySoIOnUpdate

        This method makes unit test of the Opportunity_MandatorySoIOnUpdate
        trigger.

        Changelog:
            2011-11-10  MHG     Created method
            2012-02-14  CCE     Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
            2012-12-05  TJG     Replace CCS - Customer Centric Selling record type with Qlikbuy CCS Standard
            2014-06-04  TJG     Fix test error
                                System.DmlException: Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY,
                                Record Type ID: this ID value isn't valid for the user: 01220000000J1KRAA0: [RecordTypeId]
            2015-04-20  SLH     Added function and solution area for validation rule OP006
            2016-06-14  Roman Dovbush method test_Opportunity_MandatorySoIOnUpdate updated with new assertions.
                                     method test_Opportunity_NotMandatoryProfile was created
			06.02.2017  RVA :   changing QT methods
            2017-24-03  Roman Dovbush  Replacing hardcoded record type with SOQL;
            2018-06-09  ext_vos: Create test user instead of pick up it from the environment.
    ******************************************************/
    static Id OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id; //QTNS '012f0000000CuglAAC'; //Qlikbuy CCS Standard II on QTNS  2014-06-04
    static testMethod void test_Opportunity_MandatorySoIOnUpdate()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('test_Opportunity_MandatorySoIOnUpdate: Starting');

        // QTCustomSettings__c setting = new QTCustomSettings__c(
            //Name = 'Default',
            //MandatorySoIOnUpdateExcludeProfiles__c =  '00e20000000zFfp,00e20000001ODnH,00eD0000001PFWp,00e20000000zhMi,00e20000001OyLm,00e20000000zhMC,00e20000001OyLw,00e20000000zhMB,00e20000001OyLw,00e20000001OyLI,00eD0000001P4Yf,00e20000000yyUz'
        //);
        //Insert setting;
		/*
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id
		);
        insert QTComp;*/

        // just to execute constructor
        OpportunityMandatorySoIOnUpdateHandler tempObj = new OpportunityMandatorySoIOnUpdateHandler();

        QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'United States', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account Parent = new Account(Name = 'TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert Parent;
        Profile p = [select id from profile where name='Custom: Marketing Std User'];
        User user = createMockUser(p.Id);
        User me = [Select Id From User Where Id =:UserInfo.getUserId()];
        Profile myProfile = [Select Id From Profile Where Id = :UserInfo.getProfileId()];

        Contact testContact = new Contact (
            AccountId = Parent.Id,
            OwnerId = me.Id,
            LastName = 'Wilson-Hemingway',
            FirstName = 'Daryl',
            Email = 'dwy@qlikview.com'
        );
        insert testContact;

        QTCustomSettings__c mySettings = new QTCustomSettings__c();
        mySettings.Name = 'Default';
        mySettings.MandatorySoIOnUpdateExcludeProfiles__c = myProfile.Id;
        mySettings.OEM_Record_Types__c = OppRecordTypeId;
        mySettings.Value_Below_Which_SoI_Not_Mandatory__c = 5000.0;
        insert mySettings;
        test.startTest();

        Opportunity Opp = new Opportunity();
        Opp.AccountId = Parent.Id;
        Opp.OwnerId = user.Id;
        Opp.Short_Description__c = 'My Test Description';
        Opp.RecordTypeId = OppRecordTypeId;  // Qlikbuy CCS Standard II on QTNS 2014-06-04
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'GBP';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Confirmed';
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Signature_Type__c = 'Digital Signature';
        Opp.Included_Products__c = 'Qlik Sense';
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        insert Opp;

        //Opp.Amount = 0;
        //Opp.CurrencyIsoCode = 'SEK';

        Opp = [select Id, CurrencyIsoCode, Amount from Opportunity where Id = :Opp.Id];
        // change record type to trigger an error
        opp.RecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Qlikbuy_CCS_Standard').Id;
        // try to update Opp in try-catch block
        System.runAs(user) {
            Opp.Amount = 7200;
            Opp.CurrencyIsoCode = 'GBP';

            Boolean CaughtException = false;
            Semaphores.Opportunity_ManageForecastProductsIsInsert = false;
            try
            {
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 1: update Opp');
                update Opp; //this update sets IsNew__c to false
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 2: update Opp');
                update Opp;
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 3: update Opp');
            }
            catch (System.Exception Ex)
            {
                CaughtException = true;
                System.debug('CaughtException: ' + Ex.getMessage());
            }
         //   System.assertEquals(true, CaughtException);
            System.assertEquals(7200, opp.Amount);
            System.assertEquals(false, Opp.IsNew__c);
        }

        Sphere_of_Influence__c sph = new Sphere_of_Influence__c(
            Opportunity__c = Opp.Id,
            Contact__c = testContact.Id,
            Role__c = 'Decision Maker;User'
        );
        insert sph;

        System.runAs(me)
        {
            Opp.Amount = 4000;
            Boolean CaughtException = false;
            Semaphores.Opportunity_ManageForecastProductsIsInsert = false;
            try
            {
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 1: update Opp');
                update Opp; //this update sets IsNew__c to false
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 2: update Opp');
                update Opp;
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 3: update Opp');
            }
            catch (System.Exception Ex)
            {
                CaughtException = true;
                System.debug('CaughtException: ' + Ex.getMessage());
            }
            System.assertEquals(false, CaughtException);
            System.assertEquals(false, Opp.IsNew__c);
        }


        test.stopTest();
        System.debug('test_Opportunity_MandatorySoIOnUpdate: Finishing');
    }

    static testMethod void test_Opportunity_NotMandatoryProfile()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('test_Opportunity_MandatorySoIOnUpdate: Starting');
       /*
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc'
        );
        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
        insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'United States', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account parentAcc = new Account(Name = 'TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert parentAcc;

        Profile p = [select id from profile where name='Custom: QlikBuy Sales Std User'];
        User user = createMockUser(p.Id);
        User me = [Select Id From User Where Id =:UserInfo.getUserId()];
        Profile myProfile = [Select Id From Profile Where Id = :UserInfo.getProfileId()];
        test.startTest();

        Contact testContact = new Contact (
            AccountId = ParentAcc.Id,
            OwnerId = UserInfo.getUserId(),
            LastName = 'Wilson-Hemingway',
            FirstName = 'Daryl',
            Email = 'dwy@qlikview.com'
        );
        insert testContact;

        QTCustomSettings__c mySettings = new QTCustomSettings__c();
        mySettings.Name = 'Default';
        mySettings.MandatorySoIOnUpdateExcludeProfiles__c = myProfile.Id;
        mySettings.OEM_Record_Types__c = OppRecordTypeId;
        mySettings.Value_Below_Which_SoI_Not_Mandatory__c = 5000.0;
        insert mySettings;
        Opportunity Opp = new Opportunity();
        Opp.AccountId = parentAcc.Id;
        Opp.OwnerId = user.Id;
        Opp.Short_Description__c = 'My Test Description';
        Opp.RecordTypeId = OppRecordTypeId;  // Qlikbuy CCS Standard II on QTNS 2014-06-04
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'GBP';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Confirmed';
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Signature_Type__c = 'Digital Signature';
        Opp.Included_Products__c = 'Qlik Sense';
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        //Opp.IsNew__c = true;
        insert Opp;

        //Opportunity opp1 = [Select Id, Amount, CurrencyIsoCode From Opportunity Where Id =:opp.Id];

        System.runAs(me)
        {
            Opp.CurrencyIsoCode = 'GBP';
            Opp.Amount = 3000;
            Boolean CaughtException = false;
            Semaphores.Opportunity_ManageForecastProductsIsInsert = false;
            try
            {
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 1: update Opp');
                update Opp; //this update sets IsNew__c to false
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 2: update Opp');
                update Opp;
                System.debug('test_Opportunity_MandatorySoIOnUpdate: 3: update Opp');
            }
            catch (System.Exception Ex)
            {
                CaughtException = true;
                System.debug('CaughtException: ' + Ex.getMessage());
            }
            System.assertEquals(false, CaughtException);
            System.assertEquals(3000, opp.Amount);
            System.assertEquals(false, Opp.IsNew__c);
        }
    }

    //*****************************************************
    //
    //  test_Opportunity_SetDefaultValue
    //
    //  This method makes unit test of the Opportunity_SetDefaultValue
    //  trigger.

    //  Changelog:
    //      2011-10-26  MHG     Created method
    //      2012-02-14  CCE     Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account

    //***************************************************
    static testMethod void test_Opportunity_SetDefaultValue()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('test_Opportunity_SetDefaultValue: Starting');
        User me = [Select Id From User Where Id =:UserInfo.getUserId()];
        /*
       Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id
		);
        insert QTComp;*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'United States', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account Parent = new Account(Name = 'TestAccount', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert Parent;

        test.startTest();

        QTCustomSettings__c MySetting = QTCustomSettings__c.getValues('Default');
        System.assert(MySetting != null);
        decimal Val  = MySetting.Opp_Default_License_Amount_For_Direct__c;
        Opportunity Opp = new Opportunity();
        Opp.AccountId = Parent.Id;
        Opp.Short_Description__c = 'My Test Description';
        Opp.Name = '.';
        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'USD';
        Opp.CloseDate = Date.today().addDays(30);
        Opp.StageName = 'Goal Confirmed';
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.ForecastCategoryName = 'Omitted';   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
        Opp.Revenue_Type__c = 'Direct';
        Opp.Included_Products__c = 'Qlik Sense';
        Opp.Signature_Type__c = 'Digital Signature';
        insert Opp;

        Contact cont = new Contact();
        cont.FirstName = 'Test';
        cont.LastName = 'testt';
        Insert cont;
        //Contact cont = [select Id, Name from Contact LIMIT 1];
        Opp = [select Id, CurrencyIsoCode, Amount from Opportunity where Id = :Opp.Id];

        Sphere_of_Influence__c sph = new Sphere_of_Influence__c(
            Opportunity__c = Opp.Id,
            Contact__c = cont.Id,
            Role__c = 'Decision Maker;User');
        System.runAs(me) {
            insert sph;
        }

        //System.assertEquals(Val, Opp.Amount);

        Opp.Amount = 0;
        Opp.CurrencyIsoCode = 'SEK';
        update Opp;

        Opp = [select Id, CurrencyIsoCode, Amount from Opportunity where Id = :Opp.Id];
        //System.assert(Opp.Amount > Val);


        test.stopTest();
        System.debug('test_Opportunity_SetDefaultValue: Finishing');

    }

    private static User createMockUser(String pId) {
        User user = new User(Alias = 'oppUser', Email = 'opp' + System.now().millisecond() + '@ttest.com.test',
                            Emailencodingkey = 'UTF-8', Lastname = 'Testing', Languagelocalekey = 'en_US',
                            Localesidkey = 'en_US', Timezonesidkey='America/Los_Angeles', Profileid = pId,
                            Username = 'opp' + System.now().millisecond() + '@ttest.com.test');
        insert user;
        return user;
    }
}
