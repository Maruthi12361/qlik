/*
    CR 7097 https://eu1.salesforce.com/a0CD000000U8WRR
            Partner Lead sharing after submssion
    2013-03-15  TJG Created
    2013-03-26  TJG Addressed feedback about when ownership changes from queue to user
                    the sharing needs to recreated.
    2014-11-06  TJG Recreated from backup.
    2017-10-25 AYS BMW-402 : Migrated from PartnerLeadSharingAfterSubmissionTrigger.trigger.
 */

public class LeadPartnerSharingAfterSubmissionHandler {
	public LeadPartnerSharingAfterSubmissionHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {

		QTCustomSettings__c qtSettings = QTCustomSettings__c.getValues('Default');
	    String  partnerOppReg  = qtSettings != null ? qtSettings.LeadRecordTypePartnerOppReg__c : '012D0000000JsWA'; // Lead Record Type - 'Partner Opp Reg'
	    String  partnerReferralOppReg = qtSettings != null ? qtSettings.LeadRecordTypePartnerReferralOppReg__c : '012D0000000K781'; // Partner Referral Opp Reg
	    String  QualifiedB = qtSettings != null ? qtSettings.LeadStatusQualifiedB__c : 'Qualified - B';
	    String[] SubmittedLeads = new String[] {};
	    
	    for (integer i = 0; i < triggerNew.size(); i++) {
	        Lead leadNew = triggerNew[i];
	        Lead leadOld = triggerOld[i];
	        if ((leadNew.RecordTypeId == partnerOppReg || leadNew.RecordTypeId == partnerReferralOppReg) 
	            && leadNew.Status == QualifiedB
	            && leadNew.OwnerId != leadNew.CreatedById
	            && ((leadNew.Partner_Opp_Reg_Submit_Date__c != null 
	                    && (leadNew.OwnerId != leadOld.OwnerId || leadNew.Partner_Opp_Reg_Submit_Date__c != leadOld.Partner_Opp_Reg_Submit_Date__c))
	                || (leadNew.Partner_Referral_Opp_Reg_Submit_Date__c != null 
	                    && (leadNew.OwnerId != leadOld.OwnerId || leadNew.Partner_Referral_Opp_Reg_Submit_Date__c != leadOld.Partner_Referral_Opp_Reg_Submit_Date__c)))) 
	        {
	                SubmittedLeads.Add(leadNew.Id);
	        }
	    }
	    if (SubmittedLeads.size() > 0)
	    {
	        System.Debug('PartnerLeadSharingAfterSubmissionTrigger: before call web service ');
	        
	        if (Test.isRunningTest())
	        {
	            // in test context, the web service won't be called, let's make one up
	            LeadShare ls = new LeadShare(LeadAccessLevel='Read', LeadId=SubmittedLeads[0], UserOrGroupId = UserInfo.getUserId());
	            Insert ls;
	        }
	        else
	        {
	            ApexSharingRules.PartnerLeadShareWithCreaterAfterSubmission(SubmittedLeads);
	        }
	    }
	}
}