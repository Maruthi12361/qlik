/*****************************************************************************************************************
 Change Log:

 20130509	TJG CR 7707 https://eu1.salesforce.com/a0CD000000XOYhM
 			Unit test for class DeletePartnerUsersFromAccountTeam
******************************************************************************************************************/
@isTest
public class DeletePartnerUsersFromAccountTeamTest {

    static testMethod void myUnitTest() {
 		test.startTest();
		DeletePartnerUsersFromAccountTeam delPATM = new DeletePartnerUsersFromAccountTeam();
		String schedule = '0 30 * * * ?';
		system.schedule('delPATM', schedule, delPATM);
		test.stopTest();
    }
}