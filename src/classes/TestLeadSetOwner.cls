/******************************
2015-06-03 CCE  Updated to include testing for CR# 31870
2015-08-19 CCE  Updated for revised testing required for changes to the trigger as part of CR# 20788
2016-04-26 CCE  Updated for revised testing required for changes to the trigger as part of CR# 82611
2017-01-08 Pramod Kumar V  Test class code coverage
******************************/
@isTest
private class TestLeadSetOwner {

     @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
       }
    
    static testMethod void TestLeadSetOwner_FU_Rejected() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());
     
              
        User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.debug('TestLeadSetOwner u2 = ' + u2);         
        //CREATE A LEAD 1
        System.runAs(u2) { 
        
           
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
            insert q1;
            System.debug('TestLeadSetOwner q1 = ' + q1);         
        
            //Archived record type
            RecordType ldRTArchived = [select Id, Name From RecordType where SobjectType = 'lead' AND name =: 'Archived' LIMIT 1];

            SetOwner__c mycs = SetOwner__c.getValues('standardset'); 
            if(mycs == null) {             
                mycs = new SetOwner__c(Name= 'standardset');
                mycs.OwnerId__c = u2.id;
                mycs.RecordtypeID__c = ldRTArchived.id;
                insert mycs;
            }                 
            
            //For CR# 31870 - Create a lead for testing of Status "Follow-Up Rejected"
            Lead lead = createNewLead(q1.QueueID);
            lead.Status ='Unknown';
            insert lead;
            lead = refreshLead(lead.Id);
            Id OriginalLeadRecordTypeId = lead.recordtypeid;    //we can use this to test against later
            Semaphores.TriggerHasRun('LeadSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            
            test.startTest();
            lead.Status ='Follow-Up Rejected';
            lead.Follow_Up_Rejected_Reason__c = 'Competitor'; // CR# 82611 was'Not Follow-Up Required';
            //lead.Not_Follow_Up_Required_Reason__c ='Competitor'; CR# 82611
            update lead;

            lead = refreshLead(lead.Id);

            System.assertEquals(ldRTArchived.Id, lead.recordtypeid);    //should be Archived

            //And now we will test switching it back again
            Semaphores.TriggerHasRun('LeadSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            lead.Status ='Unknown';
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
           // lead.Status ='Suspect';
            update lead;
            lead = refreshLead(lead.Id);

            System.assertEquals(OriginalLeadRecordTypeId, lead.recordtypeid);   //should be back to the original RecordType when the lead was created.

            test.stopTest();        
        }
    }

    static testMethod void TestLeadSetOwner_FU_Disqualified() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());
     
              
        User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.debug('TestLeadSetOwner u2 = ' + u2);         
        //CREATE A LEAD
        System.runAs(u2) { 
        
           
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
            insert q1;
            System.debug('TestLeadSetOwner q1 = ' + q1);         
        
            //Archived record type
            RecordType ldRTArchived = [select Id, Name From RecordType where SobjectType = 'lead' AND name =: 'Archived' LIMIT 1];

            SetOwner__c mycs = SetOwner__c.getValues('standardset'); 
            if(mycs == null) {             
                mycs = new SetOwner__c(Name= 'standardset');
                mycs.OwnerId__c = u2.id;
                mycs.RecordtypeID__c = ldRTArchived.id;
                insert mycs;
            }                 
            
            //For CR# 31870 - Create a lead for testing of Status "Follow-Up Rejected"
            Lead lead = createNewLead(q1.QueueID);
            lead.Status ='Unknown';
            insert lead;
            lead = refreshLead(lead.Id);
            Id OriginalLeadRecordTypeId = lead.recordtypeid;    //we can use this to test against later
            Semaphores.TriggerHasRun('LeadSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            
            test.startTest();
            lead.Status ='Follow-Up Disqualified';
            lead.Follow_Up_Disqualified_Reason__c ='No Fit';
            update lead;

            lead = refreshLead(lead.Id);

            System.assertEquals(ldRTArchived.Id, lead.recordtypeid);    //should be Archived

            //And now we will test switching it back again
            Semaphores.TriggerHasRun('LeadSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            lead.Status ='Unknown';
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
            update lead;
            lead = refreshLead(lead.Id);

            System.assertEquals(OriginalLeadRecordTypeId, lead.recordtypeid);   //should be back to the original RecordType when the lead was created.

            test.stopTest();        
        }
    }

    static testMethod void TestLeadSetOwner_withUserOwner() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());
     
        //CR# 20788 We have to change this to a hardcoded value as it needs to match the value used in the new Workflow (Lead: SetOwner) field update (Lead Update Owner)
        User LeadOwnerForAchive = [ select Id from User where Id = '00520000000z3Pr'];
        //User LeadOwnerForAchive = QTTestUtils.createMockUserForProfile('System Administrator');      
        User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.debug('TestLeadSetOwner u2 = ' + u2);         
        //CREATE A LEAD
        System.runAs(u2) { 

            //Archived record type
            RecordType ldRTArchived = [select Id, Name From RecordType where SobjectType = 'lead' AND name =: 'Archived' LIMIT 1];

            SetOwner__c mycs = SetOwner__c.getValues('standardset'); 
            if(mycs == null) {             
                mycs = new SetOwner__c(Name= 'standardset');
                mycs.OwnerId__c = LeadOwnerForAchive.id;
                mycs.RecordtypeID__c = ldRTArchived.id;
                insert mycs;
            }                 
            
            //For CR# 31870 - Create a lead for testing of Status "Follow-Up Rejected"
            Lead lead = createNewLead(u2.Id);
            lead.Status ='Unknown';
            insert lead;
            lead = refreshLead(lead.Id);
            Id OriginalLeadRecordTypeId = lead.recordtypeid;    //we can use this to test against later
            Semaphores.TriggerHasRun('LeadSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            
            test.startTest();
            lead.Status ='Follow-Up Disqualified';
            lead.Follow_Up_Disqualified_Reason__c ='No Fit';
            update lead;
            
            lead = refreshLead(lead.Id);

            System.assertEquals(LeadOwnerForAchive.id, lead.OwnerId);    //should be the User we created earlier for Achiving

            //And now we will test switching it back again
            Semaphores.TriggerHasRun('LeadSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            lead.Status ='Unknown';
            Semaphores.LeadTriggerHandlerBeforeUpdate = false;
            update lead;
            lead = refreshLead(lead.Id);

            System.assertEquals(u2.Id, lead.OwnerId);   //should be back to the original lead owner from when the lead was created.

            test.stopTest();        
        }
    }

    private static Lead createNewLead(Id ownerId) {
        Lead lead = new Lead();
        lead.OwnerId = ownerId;
        lead.LastName = 'last';
        lead.FirstName = 'First';
        lead.Company = 'Company';   
        lead.IsUnreadByOwner = True;
        lead.Country='Sweden';
        lead.Email='shshdkjd@fkfk.fi';
        lead.Phone='3333';
        lead.MobilePhone='3333';
        lead.Switchboard__c='3333';
        return lead;        
    }

    private static Lead refreshLead (Id id) {
        return [SELECT Id, OwnerId, Archived_Original_Owner_ID__c, recordtypeid, Archived_Original_Owner_Name__c, OldRecordType__c FROM Lead WHERE Id = :id];
    }
    
    
    static testMethod void TestLeadSetOwner_All() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());  
              
        User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.debug('TestLeadSetOwner u2 = ' + u2);         
        //CREATE A LEAD 1
        System.runAs(u2) { 
        
           
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
            insert q1;
            System.debug('TestLeadSetOwner q1 = ' + q1);         
        
            //Archived record type
            RecordType ldRTArchived = [select Id, Name From RecordType where SobjectType = 'lead' AND name =: 'Archived' LIMIT 1];

            SetOwner__c mycs = SetOwner__c.getValues('standardset'); 
            if(mycs == null) {             
                mycs = new SetOwner__c(Name= 'standardset');
                mycs.OwnerId__c = u2.id;
                mycs.RecordtypeID__c = ldRTArchived.id;
                insert mycs;
            }                 
            
            
            Lead lead = createNewLead(q1.QueueID);
            lead.Status ='Follow-Up Rejected';
            insert lead;
            lead.Status ='Follow-Up Disqualified';
            update lead;
            Lead lead1 = createNewLead(q1.QueueID);
           
            lead1.Status ='Suspect';
            insert lead1;
            List<Lead> triggerNew=new List<Lead>();
            triggerNew.add(lead);
            triggerNew.add(lead1);
            Map<Id,Lead> triggerOldMap=new  Map<Id,Lead>();
            triggerOldMap.put(lead.id,lead);
            //triggerOldMap.put(lead.id,lead);
            triggerOldMap.put(lead1.id,lead1);

            LeadSetOwnerHandler.handle(triggerNew,triggerOldMap);
            
            
            lead.Status ='Suspect';
            update lead;
            List<Lead> triggerNew1=new List<Lead>();
            triggerNew1.add(lead);
            Map<Id,Lead> triggerOldMap1=new  Map<Id,Lead>();
            triggerOldMap1.put(lead.id,lead);
            LeadSetOwnerHandler.handle(triggerNew1,triggerOldMap1);
            }
          }


    
    /*static testMethod void TestLeadSetOwner() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());
     
              
        User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];          
        //CREATE A LEAD 1
        System.runAs(u2) { 
        
           
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
            insert q1;
    
          
            //RECORD TYPE ID
            RecordType rtype = [Select Name, Id From RecordType where sObjectType='lead' LIMIT 1]; 

            SetOwner__c mycs = SetOwner__c.getValues('standardset'); 
            if(mycs == null) {             
                mycs = new SetOwner__c(Name= 'standardset');
                mycs.OwnerId__c =u2.id;
                mycs.RecordtypeID__c=rtype.id;
                insert mycs;
            }                 
            Lead lead = createNewLead(u2.id);
            lead.Status ='false';                      
            lead.Archived_Original_Owner_ID__c = u2.Id;           
            insert lead;
            
            //CREATE A LEAD 2
            Lead lead2 = createNewLead(u2.Id);
            lead2.Status ='new';
            insert lead2;
            
            
            //CREATE A LEAD 3
            Lead lead3 = createNewLead(q1.QueueID);
            lead3.Archived_Original_Owner_ID__c = q1.QueueID;
            lead3.Status ='false'; 
            insert lead3;
            
            //CREATE A LEAD 4
            Lead lead4 = createNewLead(q1.QueueID);
            lead4.Status ='new'; 
            lead4.Archived_Original_Owner_ID__c = q1.QueueID; 
            insert lead4;
                        
            test.startTest();
    
            lead.Status = 'Unqualified';
            lead.OldRecordType__c=rtype.id;
            update lead;
    
           
            
            lead2.Status = 'false';
            update lead2;
            
            lead.Status = 'Contacted';
            lead.OwnerId=lead.Archived_Original_Owner_ID__c;
            update lead;
         
            lead3.Status ='new';
            update lead3;
     
            
            lead4.Status ='false';
            update lead4;
    
            test.stopTest();        
        }
    }*/

}