public with sharing class SandboxRefresh_ReportController {

    public static list <EmailTemplate> EmailTempObjList = new List<EmailTemplate>();
    public static list <mkto_si__Marketo_Sales_Insight_Config__c> MarkObjList = new List<mkto_si__Marketo_Sales_Insight_Config__c>();
    list <String> ApexClassInputObjList = new List<String>(); 
       
    
    public with sharing class SandboxRefresh_Controller {
   
    }
 
    // Redirect to Report VF page 
     public PageReference Refresh_All()
     {
      return System.currentPageReference();
     }
    
    
    // Edit VF Page 
     public PageReference Edit()
     {
        Pagereference p;        
        
        p = new Pagereference('/apex/SandboxRefresh_Edit');
        //p = new Pagereference('/');
              
        p.setRedirect(true); 
        return p;      
     }
     
// Email Templates
   
   List <Sandbox_Refresh__c> gEmailTempList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();
     
    public list<EmailTemplate> getEmailTempList_Report()
    {
      
       List<String> lemailTempNameList= new List<String>();
       
       gEmailTempList_Of_SandBoxObj= [Select Name From Sandbox_Refresh__c where  Component__c='EmailTemplate'  ];
   
       if(!gEmailTempList_Of_SandBoxObj.isEmpty())
       {
         /* gEmailTempList_Of_SandBoxObjis not - EmailTemplates Name read from Sandbox_Refresh__c */
         for(Sandbox_Refresh__c temp:gEmailTempList_Of_SandBoxObj )
         {
           lemailTempNameList.add(temp.Name); 
         }
       }
       else
       {
         /* gEmailTempList_Of_SandBoxObj is Empty - Emails Templates Hard Coded. 
            Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done.*/
         lemailTempNameList.add('Account - Orbis - send to Navision');
         lemailTempNameList.add('Account - Orbis - send OEM to Navision');
         lemailTempNameList.add('PRM: Partner Qualified Orbis Trigger');
         lemailTempNameList.add('PRM: Partner Rejected Trigger');
         lemailTempNameList.add('CCS Trigger for SoE to XL');
         lemailTempNameList.add('Q2O Realign Account Licenses'); 
         lemailTempNameList.add('KMH - Sandbox Test Template');      
       }
      
       
       EmailTempObjList.Clear(); 
            
       EmailTempObjList = [Select Id,Name,subject from EmailTemplate where name=:lemailTempNameList];
       
       return EmailTempObjList ;
     }
     
     // Training User 
      public static List<User> TrainUserList  = new List<User>(); //Global Training user list
      public static List <Sandbox_Refresh__c> gTrainingUserNameList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();
 
      public static List<User> getTrainUserList_Report()
      {
         List<String> lTrainUserList= new List<String>();     
         
         gTrainingUserNameList_Of_SandBoxObj = [Select Name From Sandbox_Refresh__c where  Component__c='User'  ];
     
         if(!gTrainingUserNameList_Of_SandBoxObj.isEmpty())
         {
           /* gTrainingUserNameList_Of_SandBoxObj  not - UserName read from Sandbox_Refresh__c */
           for(Sandbox_Refresh__c temp:gTrainingUserNameList_Of_SandBoxObj )    
           {
             lTrainUserList.add(temp.Name); 
           }
         }
         else
         {
           lTrainUserList.add('Training User1'); //Id's of Training Users 1-15
           lTrainUserList.add('Training User2');
           lTrainUserList.add('Training User3');
           lTrainUserList.add('Training User4');
           lTrainUserList.add('Training User5');
           lTrainUserList.add('Training User6');
           lTrainUserList.add('Training User7');
           lTrainUserList.add('Training User8');
           lTrainUserList.add('Training User9');
           lTrainUserList.add('Training User10');
           lTrainUserList.add('Training User11');
           lTrainUserList.add('Training User12');
           lTrainUserList.add('Training User13');
           lTrainUserList.add('Training User14');
           lTrainUserList.add('Training User15');
         }
         
         TrainUserList = [SELECT Id,IsActive,Name,ProfileId,Trigger_CPQ_user_creation__c,UserRoleId FROM User WHERE Name =:lTrainUserList];
         
         return TrainUserList;
      }
      
      //Cases
      
      public static List<Case> gCaseList_To_Delete  = new List<Case>();
      public static List <Sandbox_Refresh__c> gCaseNameList_Of_SandBoxObj= new List<Sandbox_Refresh__c>();
       public list<Case> getCaseList_Report()
       {
       
          gCaseList_To_Delete.Clear();
          
          gCaseList_To_Delete = [SELECT Id,CaseNumber,Subject,Employee_First_Name__c,Employee_Last_Name__c From Case WHERE Confidential_New_Starter__c ='true' ];
   
          return gCaseList_To_Delete ;
       }
       
     
 //WorkFlow
  List <Sandbox_Refresh__c> gWorkFlowList= new List<Sandbox_Refresh__c>();

  public List <Sandbox_Refresh__c> getWorkflowList_Report()
  {
    List <Sandbox_Refresh__c> lWorkFlowList= new List<Sandbox_Refresh__c>();
    
    lWorkFlowList = [SELECT Id,Name,Component__c,Value_To_Change__c FROM Sandbox_Refresh__c WHERE Component__c='WorkFlow' ];
    
    
    if(!lWorkFlowList.isEmpty())
    {
      /* lWorkFlowList is not Empty - WorkFlow Name read from Sandbox_Refresh__c */
      for(Sandbox_Refresh__c temp:lWorkFlowList )
      {
        gWorkFlowList.Clear();  
        gWorkFlowList.add(temp); 
      }
    }
    else
    {
       /* lWorkFlowlist empty so hard code the values- 
         Delete once we have Mechanism Copy the Object Records to New Sandbox before Refresh is done. */
       gWorkFlowList.Clear();     
       Sandbox_Refresh__c temp = new Sandbox_Refresh__c();
       temp.Name= 'PRM - Partner User Created requires Forecasting setup';
       gWorkFlowList.add(temp);
     
    }  
   
    return gWorkFlowList  ;    
 }


}