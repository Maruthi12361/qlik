/**
* Description - Redirect Partner user to Support Portal when Product Support tab
**/
public with sharing class PPToSupportPortalController
{
     public PageReference ReturnUrl()
     {
          QS_Partner_Portal_Urls__c cs = QS_Partner_Portal_Urls__c.getInstance();
          String url = cs.Support_Portal_Url__c+'QS_Home_Page';
          PageReference p = new PageReference(url);
          return p;
      }
}