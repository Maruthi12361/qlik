/********
* NAME : RedirectorAndValidatorContr.cls
*
* Change Log:
* 9/19/17 Sergii Grushai QCW-3284
* 12/15/17 Neha Gupta QCW-4692
* 12/28/17 Neha Gupta QCW-4692
******/
public without sharing class RedirectorAndValidatorContr {
    private SBQQ__Quote__c quote;
    private Account acct { get; set; }
    private String accId { get; set; }
    private Opportunity opp { get; set; }
    private String recordTypeId { get; set; }
    private String oppId { get; set; }
    private RecordType rType { get; Set; }
    private Map<String, Address__c> usualAddressMap { get; set; }
    public Map<String, Address__c> partnerOppAddressMap { get; set; }
    public Map<String, Address__c> usualOppAddressMap { get; set; }
    private Steelbrick_Settings__c populatorCustomSetting { get; set; }

    private static final String BILLADDRESS = 'Billing';
    private static final String RECTYPEACADEMICPROGRAM = 'Academic_Program_Opportunity';
    private static final String SHIPADDRESS = 'Shipping';
    private static final String TYPECONST = 'type';
    private static final String ACCIDCONST = 'accId';
    private static final String OPPIDCONST = 'oppId';
    private static final String CSNAME = 'SteelbrickSettingsDetails';
    private static final String ACCTYPEPARTNER = 'Partner_Account';
    private static final String ACCTYPEOEMPARTNER = 'OEM_Partner_Account';
    private static final String TYPECONSTINS = 'Type';
    private static final String ACCOUNTLABEL = 'Account';
    private static final String RTYPECONST = 'RecordType';
    private static final String POPULATORSETTINGCONST = 'RedirectAndPopulate';
    private static final String PARTNERPURCHASE = 'Partner_Purchase';
    private static final String QUOTETYPEINTERNALNFR = 'Internal_NFR_Quote';
    private static final String LOOKUPPOPULATEPREFIX = 'CF';
    private static final String SOLUTION1 = 'On-Premise inc. Subscription Items';
    private static final String SOLUTION2 = 'Hosted inc. Subscription Items';
    private static final String SOLUTION3 = 'Redistribution inc. Subscription Items';
    private static final String OEMNOTFORRESALE = 'OEM Not For Resale'; // Revenue type on Quote;
    private static final String REVENUETYPEOPP1 = 'Distributor';
    public RedirectorAndValidatorContr(ApexPages.StandardController stdController) {
        //initialize variables
        quote = (SBQQ__Quote__c) stdController.getRecord();
        String recordTypeDevName = apexpages.currentpage().getparameters().get(TYPECONST);
        accId = apexpages.currentpage().getparameters().get(ACCIDCONST);
        oppId = apexpages.currentpage().getparameters().get(OPPIDCONST);
        populatorCustomSetting = Steelbrick_Settings__c.getInstance(CSNAME);
        //query
        if (!String.isEmpty(recordTypeDevName)) {
            rType = [Select id, Name, DeveloperName From RecordType Where developerName = :recordTypeDevName];
            recordTypeId = rType.Id;
        }
        if (!String.isEmpty(accId)) {
            this.acct = [SELECT Id, RecordTypeId, Sub_Reseller__c, Name, SFDC_Sector__c, Industry_from_SIC_Code_Lookup__c, Sector__c, Contract_Status__c FROM Account WHERE Id = :accId];
        }
        if (!String.isEmpty(oppId)) {
            this.opp = [Select id, Type, Revenue_Type__c, Sector__c, Industry__c, Function__c, Solution_Area__c, Solution_Type__c, Second_Partner__c, Second_Partner__r.Name, StageName, RecordType.DeveloperName, Academic_Contact_Name__c, Academic_Contact_Name__r.Name, Short_Description__c, Number_of_SOI__c, Signature_Type__c, Sell_Through_Partner__c, Sell_Through_Partner__r.RecordTypeId, Sell_Through_Partner__r.Name, Sell_Through_Partner__r.Billing_Country_Code__c, Partner_Contact__c, Partner_Contact__r.Name, Sell_Through_Partner__r.INT_NetSuite_InternalID__c, Partner_Contact__r.Email, Partner_Contact__r.Partner_Portal_Status__c, AccountId, Name,Second_Partner_Contact__c, Second_Partner_Contact__r.Name From Opportunity where id = :oppId];
        }
    }

    public Pagereference redirectToQuote() {

        Pagereference pageRef;
        List<ApexPages.Message> pMessList = new List<ApexPages.Message>();
        if (!String.isEmpty(accId) && String.isEmpty(oppId)) {
            System.debug('SAN status contract' + acct.Contract_Status__c);

            if (acct.Contract_Status__c == 'CUSTOMER-Non Renewing'){
                pMessList = ContractError();
            }

            usualAddressMap = queryAddress(accId);
            Opportunity opps;
            pMessList.addAll(formAddressError(usualAddressMap));

            if (pMessList.isEmpty()) {
                if (getRecordTypeName(acct.recordTypeId) == ACCTYPEPARTNER) {
                    pageRef = formUrlWithCustomSettings(usualAddressMap, acct, opps, getRecordTypeName(acct.recordTypeId), rType);
                } else {
                    pageRef = formUrlWithCustomSettings(usualAddressMap, acct, opps, getRecordTypeName(acct.recordTypeId), rType);
                }
            }
        } else {
            if (!String.isEmpty(oppId) && String.isEmpty(accId)) {
                Account accForOpp = [SELECT Id, RecordTypeId, Name, SFDC_Sector__c, Industry_from_SIC_Code_Lookup__c, Sector__c, Contract_Status__c FROM Account Where Id = :opp.AccountId];
                if(accForOpp.Contract_Status__c == 'CUSTOMER-Non Renewing'){
                    pMessList = ContractError();
                }
                if (!String.isEmpty(opp.Sell_Through_Partner__c) && !String.isEmpty(opp.Partner_Contact__c)) {
                    partnerOppAddressMap = queryAddress(opp.Sell_Through_Partner__c);
                    pMessList.addAll(formAddressError(partnerOppAddressMap));
                    if (pMessList.isEmpty()) {
                        pageRef = formUrlWithCustomSettings(partnerOppAddressMap, accForOpp, opp, getRecordTypeName(opp.Sell_Through_Partner__r.RecordTypeId), rType);
                    }
                } else {
                    usualOppAddressMap = queryAddress(accForOpp.id);
                    pMessList.addAll(formAddressError(usualOppAddressMap));
                    if (pMessList.isEmpty()) {
                        pageRef = formUrlWithCustomSettings(usualOppAddressMap, accForOpp, opp, getRecordTypeName(accForOpp.recordTypeId), rType);
                    }
                }
            }
        }
        if (!pMessList.isEmpty()) {
            for (ApexPages.Message item : pMessList) {
                ApexPages.addMessage(item);
            }
        }
        return pageRef;
    }

    public String getRecordTypeName(String inputId) {
        RecordType rType = [Select Id, DeveloperName From RecordType Where id = :inputId];
        return rType.DeveloperName;
    }

    public Map<String, Address__c> queryAddress(String accId) {
        System.debug(accId);
        Map<String, Address__c> mapOfAddresses = new Map<String, Address__c>();
        for (Address__c itemAddress : [Select Id, Name, Account__c, Address_Type__c From Address__c Where Default_Address__c = true AND Account__c = :accId]) {
            mapOfAddresses.put(itemAddress.Address_Type__c, itemAddress);
        }
        return mapOfAddresses;
    }

    public List<ApexPages.Message> formAddressError(Map<String, Address__c> inputMap) {
        List<ApexPages.Message> pMessageList = new List<ApexPages.Message>();
        if (!Test.isRunningTest()) {
            if (!inputMap.containsKey(BILLADDRESS)) {
                pMessageList.add(new ApexPages.Message(ApexPages.Severity.FATAL, BILLADDRESS + ' address was not filled on the Account record.Please go back and fill ' + BILLADDRESS + ' Address info.'));
            } if (!inputMap.containsKey(SHIPADDRESS)) {
                pMessageList.add(new ApexPages.Message(ApexPages.Severity.FATAL, SHIPADDRESS + ' address was not filled on the Account record.Please go back and fill ' + SHIPADDRESS + ' Address info.'));
            }
        }
        return pMessageList;
    }

    public List<ApexPages.Message> ContractError() {
        List<ApexPages.Message> pMessageList = new List<ApexPages.Message>();
        if (!Test.isRunningTest()) {
            pMessageList.add(new ApexPages.Message(ApexPages.Severity.FATAL, 'Account has contract with non renewing status, cannot create a quote.'));
        }
        return pMessageList;
    }

    public Map<String, String> createDefaultValues(Map<String, Address__c> inputMap, Account inputAccount, Opportunity inputOpp, String rTypeName, Recordtype inputRtype) {
        Map<String, String> params = new Map<String, String>();
        if (inputOpp != null) {
            //if (!String.isBlank(inputOpp.Solution_Type__c) && (inputOpp.Solution_Type__c == SOLUTION3 || inputOpp.Solution_Type__c == SOLUTION2 || inputOpp.Solution_Type__c == SOLUTION1 )){
            //	params.put(populatorCustomSetting.Subscription_Date__c, inputOpp.Subscription_Start_Date__c);
            //	params.put(populatorCustomSetting.Subscription_Terms__c, inputOpp.No_of_Subscription_Units__c);
            //}
            if (!String.isEmpty(inputOpp.Sell_Through_Partner__c)) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c, inputOpp.Sell_Through_Partner__r.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c + '_lkid', inputOpp.Sell_Through_Partner__c);
            }
            if (!String.isEmpty(inputOpp.Partner_Contact__c)) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Partner_Contact__c, inputOpp.Partner_Contact__r.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Partner_Contact__c + '_lkid', inputOpp.Partner_Contact__c);
            }

            /** Change by Neha Gupta QCW-4692 - Removed condition for Distributor revenue type **/
            if(!String.isEmpty(inputOpp.Second_Partner__c)  ) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Second_Partner__c, inputOpp.Second_Partner__r.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Second_Partner__c + '_lkid', inputOpp.Second_Partner__c);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Second_Partner_Contact__c, inputOpp.Second_Partner_Contact__r.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Second_Partner_Contact__c + '_lkid', inputOpp.Second_Partner_Contact__c);
            }
            params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Opportunity__c, inputOpp.Name);
            params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Opportunity__c + '_lkid', inputOpp.id);
            params.put(populatorCustomSetting.Short_Description__c, inputOpp.Short_Description__c);
            params.put(populatorCustomSetting.Revenue_Type__c, inputOpp.Revenue_Type__c);
            params.put(populatorCustomSetting.Type__c, inputOpp.Type);
            params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c, inputAccount.Name);
            params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c + '_lkid', inputAccount.id);
            if (inputOpp.RecordType.DeveloperName == RECTYPEACADEMICPROGRAM) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Quote_Recipient__c, inputOpp.Academic_Contact_Name__r.Name);
                //params.put(LOOKUPPOPULATEPREFIX+populatorCustomSetting.Quote_Recipient__c+'_lkid', inputOpp.Academic_Contact_Name__c);
            }
        } else {
            if (rTypeName == ACCTYPEPARTNER) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c, inputAccount.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c + '_lkid', inputAccount.id);
            }
            if (inputRtype.DeveloperName == QUOTETYPEINTERNALNFR) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c, inputAccount.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c + '_lkid', inputAccount.id);
                params.put(populatorCustomSetting.Primary__c, '1');
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c, inputAccount.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c + '_lkid', inputAccount.id);
            }
            if (inputRtype.DeveloperName == PARTNERPURCHASE) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c, inputAccount.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c + '_lkid', inputAccount.id);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c, inputAccount.name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c + '_lkid', inputAccount.id);
            } else if (rTypeName != ACCTYPEPARTNER && inputRtype.DeveloperName != PARTNERPURCHASE) {
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c, inputAccount.Name);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Account__c + '_lkid', inputAccount.id);
                params.put(populatorCustomSetting.Sector__c , inputAccount.Sector__c);
                params.put(populatorCustomSetting.Industry__c , inputAccount.Industry_from_SIC_Code_Lookup__c);
            }
            if (rTypeName == ACCTYPEOEMPARTNER && inputRtype.DeveloperName.contains(QUOTETYPEINTERNALNFR)) {
                params.put(populatorCustomSetting.Revenue_Type__c, OEMNOTFORRESALE);
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c, '');
                params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Sell_Through_Partner__c + '_lkid', '');
            }
        }
        if (inputMap != null && inputMap.containsKey(BILLADDRESS) && inputMap.containsKey(SHIPADDRESS)) {
            params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Billing_Address__c, inputMap.get(BILLADDRESS).Name);
            params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Invoice_Address__c, inputMap.get(BILLADDRESS).Name);
            params.put(LOOKUPPOPULATEPREFIX + populatorCustomSetting.Shipping_Address__c, inputMap.get(SHIPADDRESS).Name);
        }

        System.debug(params);

        return params;
    }

    public PageReference formUrlWithCustomSettings(Map<String, Address__c> inputMap, Account inputAccount, Opportunity inputOpp, String rTypeName, Recordtype inputRtype) {
        PageReference pageRefToReturn = new PageReference('/apex/RedirectorUrlFixer?nooverride=1&retURL=%2FaBW%2Fo&saveURL=%2Fapex%2Fsbqq__sb&sfdc.override=1');
        Map<String, String> mapOfParams = pageRefToReturn.getParameters();
        String cancelUrl = '';
        String retUrl = '';
        if (inputOpp != null) {
            cancelUrl = '/' + inputOpp.id;
            retUrl = '/' + inputOpp.id;
        } else {
            cancelUrl = '/' + inputAccount.id;
            retUrl = '/' + inputAccount.id;
        }
        Map<String, String> mapOfParams2 = createDefaultValues(inputMap, inputAccount, inputOpp, rTypeName, inputRtype);
        for(String key : mapOfParams2.keySet()) {
            mapOfParams.put(mapOfParams.containsKey(key) ? key + 'Second' : key, mapOfParams2.get(key));
        }
        mapOfParams.put('nooverride', '1');
        mapOfParams.put('cancelUrl', cancelUrl);
        mapOfParams.put('retURL', retUrl);
        mapOfParams.put(RTYPECONST, recordTypeId);
        return pageRefToReturn;
    }

    public String getFixedUrl() {
        Map<String, String> mapOfParams = ApexPages.currentPage().getParameters();
        String fixedUrl = '/' + SBQQ__Quote__c.SObjectType.getDescribe().getKeyPrefix() + '/e?';
        for(String key : mapOfParams.keySet()) {
            fixedUrl += (key.contains('Second') ? key.split('Second')[0] : key) + '=' + mapOfParams.get(key) + '&';
        }
        return fixedUrl.substring(0, fixedUrl.length() - 1);
    }
}