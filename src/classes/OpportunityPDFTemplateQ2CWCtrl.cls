// Q2CW
// controller class behind the visualforce page: Opportunity_PDF_Template_Q2CW
// Change log: IRN 2016-06-09 file created

public with sharing class OpportunityPDFTemplateQ2CWCtrl {
    private Opportunity opp;
    public List<OpportunityLineItem> lItems {get;set;}
    public List<Non_Licence_Related_ProductsCO_Non_Li__c> lNRPItems {get;set;}
    public Boolean showItems1 {get;set;}    
    public Boolean showItems2 {get;set;}
    
    public OpportunityPDFTemplateQ2CWCtrl(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();

        showItems1 = false;
        lItems =   [SELECT Product_Family__c, Name__c, Description, Quantity, NS_List_Price__c, Discount__c, 
                    NS_item_Price__c, Partner_Margin__c, Net_before_partner_margin_NS__c, TotalPrice, Product_Group__c, Account_License__c from OpportunityLineItem where OpportunityId = : opp.Id];
       if(lItems.size() > 0) showItems1 = true;

        showItems2 = false;
        lNRPItems = [SELECT Name, Application__c, Description__c, Qty__c, List_Price__c,
                       Discount_perc__c, Net_Price__c, Product_Family__c
                       FROM Non_Licence_Related_ProductsCO_Non_Li__c WHERE Opportunity__c = :opp.Id];
       if(lNRPItems.size() > 0) showItems2 = true;
    }
}