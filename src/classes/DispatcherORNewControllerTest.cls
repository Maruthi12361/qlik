/*************************************************************************************************************
 Name: DispatcherORNewControllerTest
 Author: CCE
 Purpose: This is test class for DispatcherORNewController
  
 Log History:
 2016-09-14 Initial Development
 2019-02-13    CCE CHG0035276 - update test class for requested change to DispatcherORNewController.cls.
*************************************************************************************************************/
@isTest
private class DispatcherORNewControllerTest {
    
    static testmethod void DispatcherORNewController_TestForMarketoSupportRequestsRecordType()
    {
        System.debug('DispatcherORNewController_TestForMarketoSupportRequestsRecordType: Starting');

        ApexPages.StandardController stdController = new ApexPages.StandardController(new Marketing_Request__c());
        DispatcherORNewController controller = new DispatcherORNewController(stdController);

        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('RecordType', '012D0000000KJMD');   //Marketo Support Request record type
        
        PageReference pageref = controller.getRedir();
        //if the record type is "Marketo Support Request" pageref should be populated.
        System.assertNotEquals(null, pageref);

        Test.stopTest();

        System.debug('DispatcherORNewController_TestForMarketoSupportRequestsRecordType: Finishing');        
    }

    static testmethod void DispatcherORNewController_TestForNotAMarketoSupportRequestsRecordType()
    {
        System.debug('DispatcherORNewController_TestForNotAMarketoSupportRequestsRecordType: Starting');

        ApexPages.StandardController stdController = new ApexPages.StandardController(new Marketing_Request__c());
        DispatcherORNewController controller = new DispatcherORNewController(stdController);

        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('RecordType', '012D0000000KEZf');   //Web Request record type
        
        PageReference pageref = controller.getRedir();
        //if the record type is not "Marketo Support Request" or "Marketing Data Request" pageref should not be populated.
        System.assertEquals(null, pageref);

        Test.stopTest();

        System.debug('DispatcherORNewController_TestForNotAMarketoSupportRequestsRecordType: Finishing');        
    }
}