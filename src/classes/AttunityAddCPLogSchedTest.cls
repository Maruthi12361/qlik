/**
* AttunityAddCPLogSchedTest
* 
* Description:  Test class for AttunityAddCPLogSchedCon and AttunityAddCPLogSchedUsr
* Added:        2019-11-18 - Ain - IT-2287
*
* Change log:
* 09-11-2018 - Ain - IT-2287 - Initial Implementation
*
*/
@isTest
public class AttunityAddCPLogSchedTest {
    @isTest static void testAttunityAddCPLogSchedUsr() { 
		QTTestUtils.GlobalSetUp();
		
		ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }

	
		List<User> usersToBeActivated = [select id, isActive, FederationIdentifier, ContactId from user where FirstName = 'James' and LastName = 'Bond' and alias = '007'];
		if(usersToBeActivated.size() == 0){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No test user found');
            throw e;
		}
		if(usersToBeActivated.size() >= 2){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('Too many test users found');
            throw e;
		}
		User userToBeActivated = usersToBeActivated[0];
		
		List<ULC_Level__c> cplogs = [select id from ULC_Level__c where name = 'CPLOG'];
		cplog = cplogs[0];
		
		
		Test.startTest();		
        
        AttunityAddCPLogSchedUsr batchJobUsr = new AttunityAddCPLogSchedUsr();
		DataBase.executeBatch(batchJobUsr);
        
		Test.stopTest();
		
		userToBeActivated = [select id, isActive, FederationIdentifier, ContactId from user where id =: userToBeActivated.Id];
		system.assertEquals(userToBeActivated.isActive, true, 'User is not activated');
		system.assertEquals(userToBeActivated.FederationIdentifier, 'testiemctest', 'User has the wrong federationidentifier');
    }
    @isTest static void testAttunityAddCPLogSchedCon() { 
		QTTestUtils.GlobalSetUp();
		
		ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }

	
		List<User> usersToBeActivated = [select id, isActive, FederationIdentifier, ContactId from user where FirstName = 'James' and LastName = 'Bond' and alias = '007'];
		if(usersToBeActivated.size() == 0){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No test user found');
            throw e;
		}
		if(usersToBeActivated.size() >= 2){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('Too many test users found');
            throw e;
		}
		User userToBeActivated = usersToBeActivated[0];
		
		List<ULC_Level__c> cplogs = [select id from ULC_Level__c where name = 'CPLOG'];
		cplog = cplogs[0];
		
		
		Test.startTest();		
        
        AttunityAddCPLogSchedCon batchJobCon = new AttunityAddCPLogSchedCon();
		DataBase.executeBatch(batchJobCon);

		Test.stopTest();
		
		List<Assigned_ULC_Level__c> assignedulclevels = [select id, ULCLevelId__c, ContactId__c from Assigned_ULC_Level__c where ContactId__c =: userToBeActivated.ContactId];
		system.assertEquals(assignedulclevels.size(), 1, 'No assigned ulc level found');
		system.assertEquals(assignedulclevels[0].ULCLevelId__c, cplog.Id, 'Assigned ULC level is not CPLOG');
    }
    /* test for exceptions */
    @isTest static void NegTestUsr1() { 
        QTTestUtils.GlobalSetUp();
		
		/*ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;*/
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }
        
        
        
        Test.startTest();		
        try {
            AttunityAddCPLogSchedUsr batchJobUsr = new AttunityAddCPLogSchedUsr();
            DataBase.executeBatch(batchJobUsr);
        }
        catch (NoDataFoundException ex) {
            
        }
       
		Test.stopTest();
    
    }
    
    @isTest static void NegTestCon1() { 
        QTTestUtils.GlobalSetUp();
		
		/*ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;*/
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }
        
        Test.startTest();
        try {
            AttunityAddCPLogSchedCon batchJobCon = new AttunityAddCPLogSchedCon();
            DataBase.executeBatch(batchJobCon);
        }
        catch (NoDataFoundException ex) {
        }
		Test.stopTest();
    
    }
    
    /* test for cplog already exists */
    @isTest static void NegTestUsr2() { 
        QTTestUtils.GlobalSetUp();
		
		ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }
        
        Assigned_ULC_Level__c levelToAssign = new Assigned_ULC_Level__c();
        levelToAssign.ContactId__c = con1.Id;
        levelToAssign.Status__c = 'Approved - No trigger';
        levelToAssign.ULCLevelId__c	= cplog.Id;
        insert levelToAssign;
        
        
        
        Test.startTest();		
        try {
            AttunityAddCPLogSchedUsr batchJobUsr = new AttunityAddCPLogSchedUsr();
            DataBase.executeBatch(batchJobUsr);
        }
        catch (NoDataFoundException ex) {
            
        }
		Test.stopTest();
    
    }
    /* test for cplog already exists */
    @isTest static void NegTestCon2() { 
        QTTestUtils.GlobalSetUp();
		
		ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }
        
        Assigned_ULC_Level__c levelToAssign = new Assigned_ULC_Level__c();
        levelToAssign.ContactId__c = con1.Id;
        levelToAssign.Status__c = 'Approved - No trigger';
        levelToAssign.ULCLevelId__c	= cplog.Id;
        insert levelToAssign;
        
        
        
        Test.startTest();
        
        try {
            AttunityAddCPLogSchedCon batchJobCon = new AttunityAddCPLogSchedCon();
            DataBase.executeBatch(batchJobCon);
        }
        catch (NoDataFoundException ex) {
        }
		Test.stopTest();
    
    }
    /* test schedule */
    @isTest static void SchedTestUsr() { 
        QTTestUtils.GlobalSetUp();
		
		ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }

	
		List<User> usersToBeActivated = [select id, isActive, FederationIdentifier, ContactId from user where FirstName = 'James' and LastName = 'Bond' and alias = '007'];
		if(usersToBeActivated.size() == 0){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No test user found');
            throw e;
		}
		if(usersToBeActivated.size() >= 2){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('Too many test users found');
            throw e;
		}
		User userToBeActivated = usersToBeActivated[0];
		
		List<ULC_Level__c> cplogs = [select id from ULC_Level__c where name = 'CPLOG'];
		cplog = cplogs[0];
		
		
		Test.startTest();		
        
        AttunityAddCPLogSchedUsr myClass = new AttunityAddCPLogSchedUsr ();   
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, myClass);

		Test.stopTest();    
    }
    
    /* test schedule */
    @isTest static void SchedTestCon() { 
        QTTestUtils.GlobalSetUp();
		
		ULC_Level__c cplog = new ULC_Level__c();
		cplog.Name = 'CPLOG';
		cplog.Status__c = 'Active';
		insert cplog;
		
        List<Profile> profiles = [select id from profile where name='Customer Portal Case Logging Access - Attunity'];
        if(profiles.size() == 0){
            NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No profile with name Customer Portal Case Logging Access - Attunity found!');
            throw e;
        }
        Profile profile = profiles[0];
		
		Account acc1 = new Account();
		acc1.name = 'Acc';
		insert acc1;
		
		Contact con1 = new Contact();
		con1.FirstName = 'Test';
		con1.LastName = 'Testersson';
		con1.AccountId = acc1.Id;
		insert con1;
		
		ULC_Details__c ulcdetail = new ULC_Details__c();
		ulcdetail.ContactId__c = con1.id;
		ulcdetail.ULCStatus__c = 'Active';
		ulcdetail.ULCName__c = 'testiemctest';
		insert ulcdetail;
		
		User currentUser = [select Id from User where id =: System.Userinfo.getUserId()];

		system.runAs(currentUser){
            User userToBeActivated = new User();
            userToBeActivated.FirstName = 'James';
            userToBeActivated.LastName = 'Bond';
            userToBeActivated.Alias = '007';
            userToBeActivated.ProfileId = profile.Id;
            userToBeActivated.isActive = false;
            userToBeActivated.username = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com.test';
            userToBeActivated.email = System.now().millisecond() + 'frodobaggins_asdqqweqwdqwdqqqqq@shire.com';
            userToBeActivated.ContactId = con1.Id;
            userToBeActivated.Emailencodingkey = 'UTF-8';
            userToBeActivated.Languagelocalekey = 'en_US';
            userToBeActivated.localesidkey = 'en_US';
            userToBeActivated.Timezonesidkey='America/Los_Angeles';
            insert userToBeActivated;
        }

	
		List<User> usersToBeActivated = [select id, isActive, FederationIdentifier, ContactId from user where FirstName = 'James' and LastName = 'Bond' and alias = '007'];
		if(usersToBeActivated.size() == 0){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('No test user found');
            throw e;
		}
		if(usersToBeActivated.size() >= 2){
			NoDataFoundException e = new NoDataFoundException();
            e.setMessage('Too many test users found');
            throw e;
		}
		User userToBeActivated = usersToBeActivated[0];
		
		List<ULC_Level__c> cplogs = [select id from ULC_Level__c where name = 'CPLOG'];
		cplog = cplogs[0];
		
		
		Test.startTest();		
        
        AttunityAddCPLogSchedCon myClass2 = new AttunityAddCPLogSchedCon ();   
        String chron = '0 0 23 * * ?';   
        system.schedule('Test Sched 2', chron, myClass2);
        
		Test.stopTest();    
    }
    
}