/* 
Purpose: Partner Expertise Main Progress bar
Created Date: 13 Nov 2015
Initial delvelpment MTM 13 Nov 2015
Change log:
 **/

public with sharing class EP_MainProgressBarController {
	private string accId;
    public String AccountId
    {       
        get{ return accId;}
        set{        
            if(value != null && value != '')
            {
                accId = value;
            }            
        }
    }
    
	public EP_MainProgressBarController() {

		User u = [select AccountId, ContactId, Contact.ActiveULC__c, ProfileId from User where id = :UserInfo.getUserId()];
        if(u.AccountId!=null) 
        {
            AccountId=u.AccountId;
        }
        ListofLookups = getApplicationLookups();
        ExpertiseAreaMap = getMapofExpertiseArea();
        MapofExperttoSolutionProfiles = getExpertToSolProfMaps(ExpertiseAreaMap.keySet());
       // MapofExpertToApplicationLookup = getExpertToApplicationLookupMap(ExpertiseAreaMap.keySet());
	}
    
    public void SetAllSolutionProfilesStatus(Id expertId, String status)
    {
        if(MapofExperttoSolutionProfiles.containskey(expertId))
        {
            List<Solution_Profiles__c> listSols = new List<Solution_Profiles__c>();
            listSols = MapofExperttoSolutionProfiles.get(expertId);
            List<Solution_Profiles__c> solutionstoUpdate = new List<Solution_Profiles__c>();
            
            for(Solution_Profiles__c s : listSols)
            {
                if(s.Status__c == 'Ready for Review')
                {
                    s.Status__c = status;
                    solutionstoUpdate.add(s);
                }
            }
            try
            {
                System.Debug('Updating Solution profiles...');  
                update solutionstoUpdate;
            }
            catch(Exception ex)
            {
                System.Debug('Exception on Updating Solution profiles');               
            }
        }
        
    }
    
    public Boolean SetExpertiseDesignationStatus(Id expertId, String status)
    {
        Partner_Expertise_Area__c expertiseArea = ExpertiseAreaMap.get(expertId);
        expertiseArea.Expertise_Designation_Status__c = status;
        try
            {
                System.Debug('Updating expertise area...');  
                update expertiseArea;
            }
            catch(Exception ex)
            {
                System.Debug('Exception on Updating expertise area');
               	return false;
            }
        return true;
    }
    public String expert {get; set;}
    public PageReference SubmitforReview()
    {
        String expertId = expert;
        PageReference ref = null;
        System.debug('expertId =' + expertId);        
        String newSolProfStatus = 'Under Review';
        String newExpertiseDesignationStatus = 'Under Review';        
        SetAllSolutionProfilesStatus(expertId, newSolProfStatus);
        SetExpertiseDesignationStatus(expertId, newExpertiseDesignationStatus);
        return ref;
    }
    
    public Map<Id, Partner_Expertise_Area__c> ExpertiseAreaMap{get; set;}
    
    /*Do not call this method directly. Use the Property instead. 
     * This method is to be called only in Constructor
     * */
    public Map<Id, Partner_Expertise_Area__c> getMapofExpertiseArea()
    {
        Map<Id,Partner_Expertise_Area__c> mapofExp = new Map<Id,Partner_Expertise_Area__c> ([Select
                                                                                             Id, 
                                                                                             Expertise_Area__c,
                                                                                             Apply_Date__c,
                                                                                             Dedicated_Expertise_Contact__c,
                                                                                             Dedicated_Expertise_Contact__r.Name,
                                                                                             Expertise_Start_Date__c,
                                                                                             Expertise_Application_Eligibility__c,
                                                                                             Partner_Account_Name__r.Partner_Type__c,
                                                                                             Qlik_Market_Application__c,
                                                                                             Expertise_Designation_Status__c,
                                                                                             Qlik_Market_App_Status__c
                                                                                             from Partner_Expertise_Area__c where Partner_Account_Name__c=: AccountId]);                
        return mapofExp;
            
    }
        
    Public List<Partner_Expertise_Application_Lookup__c> ListofLookups { get; set; }
	public List<Partner_Expertise_Application_Lookup__c> getApplicationLookups()
    {   
        List <Partner_Expertise_Application_Lookup__c> Partnerlookupobjs = [Select Name,
                                                                          Account_Name__r.Name,
                                                                          Account_Name__r.Id,
                                                                          Account_Name__c,
                                                                          Expertise_Area__c,
                                                                          Expertise_Area__r.Name,
                                                                          Expertise_Area__r.Id,
                                                                          Expertise_Area__r.Expertise_Area__c,
                                                                          Expertise_Area__r.Status__c,
                                                                          Expertise_Area__r.Expertise_Application_Eligibility__c,
                                                                          Expertise_Area__r.Expertise_Designation_Status__c,
                                                                          Expertise_Area__r.Dedicated_Expertise_Contact__r.Name,
                                                                          Expertise_Area__r.Qlik_Market_App_Status__c,
                                                                          Solution_Profile_1_Name__r.Name,
                                                                          Solution_Profile_1_Name__r.Id,
                                                                          Solution_Profile_2_Name__r.Name,
                                                                          Solution_Profile_2_Name__r.Id,
                                                                          Solution_Profile_3_Name__r.Name,
                                                                          Solution_Profile_3_Name__r.Id,
                                                                          Solution_Profile_4_Name__r.Name,
                                                                          Solution_Profile_4_Name__r.Id,
                                                                          Cumulative_Profiles_Status__c,
                                                                          Ready_to_Submit_for_Review__c
                                                                          from Partner_Expertise_Application_Lookup__c  where Account_Name__c=:AccountId
                                                                           and Expertise_Area__r.Expertise_Designation_Status__c != 'Rejected' 
                                                                           and Expertise_Area__r.Expertise_Application_Eligibility__c != 'Rejected'];
        return Partnerlookupobjs;
    }
          
    
    public Map<Id, List<Solution_Profiles__c>> MapofExperttoSolutionProfiles{get; set;}
    
    /*Do not call this method directly. Use the Property instead. 
     * This method is to be called only in Constructor
     * */
    public Map<Id, List<Solution_Profiles__c>> getExpertToSolProfMaps(Set<Id> expertIds)
    {
        Map<Id, List<Solution_Profiles__c>> expertToSolProfMap = new Map<Id, List<Solution_Profiles__c>>();
        List<Solution_Profiles__c> listSols = [Select
                                               Id, 
                                               Partner_Expertise_Area_ID__c,
                                               Status__c
                                               From Solution_Profiles__c s where  Account_Name__c =: AccountId];
        for(Id expertId : expertIds)
        {
            List<Solution_Profiles__c> solsforExpertId = new List<Solution_Profiles__c>();
            
            for(Solution_Profiles__c s : listSols)
            {
                if(s.Partner_Expertise_Area_ID__c == expertId)
                    solsforExpertId.add(s);
            }
            expertToSolProfMap.put(expertId, solsforExpertId);
        }
        return expertToSolProfMap;
    }
     /*
      * 
      * 
      * public Solution_Profiles__c SolProfile{get; private set;}

	Public Boolean NoApplications {
        get
        {
            return ListofLookups.isEmpty();
        }
	}	
	public List<Partner_Expertise_Area__c> ListofExpertiseArea{
        get {return ExpertiseAreaMap.values();}
    }    
	public Map<Id, Partner_Expertise_Application_Lookup__c> MapofExpertToApplicationLookup {get; set;}
    public Map<Id, Partner_Expertise_Application_Lookup__c> getExpertToApplicationLookupMap(Set<Id> expertIds)
    {
        Map<Id, Partner_Expertise_Application_Lookup__c> expertToLookupMap = new Map<Id, Partner_Expertise_Application_Lookup__c>();
        List<Partner_Expertise_Application_Lookup__c> lookups = getApplicationLookups();
        for(Id expertId : expertIds)
        {
            for(Partner_Expertise_Application_Lookup__c p : lookups)
            {
                if(p.Expertise_Area__c == expertId)
                    expertToLookupMap.put(expertId, p);
            }
        }
        return expertToLookupMap;
        
    }
    public Map<Id, Solution_Profiles__c> getMapofSolutionProfiles(Id expetId)
    {
        Map<Id,Solution_Profiles__c> mapofSolProf = new Map<Id,Solution_Profiles__c> ([Select
                                                                                       Id, 
                                                                                       Partner_Expertise_Area_ID__c,
                                                                                       Status__c
                                                                                       From Solution_Profiles__c s where  Account_Name__c =: AccountId
                                                                                       and Partner_Expertise_Area_ID__c =: expetId]);            
        return mapofSolProf;     
    }
    public List<Solution_Profiles__c> getListofSolutionProfiles(Id expertId)
    {
        List<Solution_Profiles__c> lsitofSolProf =[Select
                                                  Id, 
                                                  Partner_Expertise_Area_ID__c,
                                                  Status__c
                                                  From Solution_Profiles__c s where  Account_Name__c =: AccountId
                                                  and Partner_Expertise_Area_ID__c =: expertId];
        return lsitofSolProf;     
    }
    
    public Boolean IsAllProfilesReadyToSubmitforReview(Id expertId)
    {
        if(MapofExperttoSolutionProfiles.containskey(expertId))
        {
            List<Solution_Profiles__c> listSols = MapofExperttoSolutionProfiles.get(expertId);
            for(Solution_Profiles__c s : listSols)
            {
                if(s.Status__c == 'Rejected' || s.Status__c == 'In Process')
                    return false;                    
            }
        }
        else { return false; }
        return true;
    }
     
    public Boolean IsAllProfilesApproved(Id expertId)
    {
        if(MapofExperttoSolutionProfiles.containskey(expertId))
        {
            List<Solution_Profiles__c> listSols = MapofExperttoSolutionProfiles.get(expertId);
            for(Solution_Profiles__c s : listSols)
            {
                if(s.Status__c != 'Approved')
                    return false;         
            }
        }
        else { return false; }
        return true;
    }
    
    public Id currentExpertId {get; set;}
     
    public String getAllProfilesStatus()
    {
        Id expertId = currentExpertId;
        if(MapofExperttoSolutionProfiles.containskey(expertId))
        {
            List<Solution_Profiles__c> listSols = MapofExperttoSolutionProfiles.get(expertId);
            
            Set<String> status = new Set<String>();
            for(Solution_Profiles__c s : listSols)
            {
                status.add(s.Status__c);
            }
            if(status.contains('In Process')) return 'In Process';
            else if(status.contains('Ready for Review')) return 'Ready for Review';
            else if(status.contains('Under Review')) return 'Under Review';
            else if(status.contains('Rejected')) return 'Rejected';
        }
        return 'Approved';
        
    }
     
    public Boolean getQlikMarketAppStatus()
    {
        Id expertId = currentExpertId;
        System.debug('expertId =' + expertId);
        Partner_Expertise_Area__c obj;
    	obj=  ExpertiseAreaMap.get(expertId);
        System.debug('expert obj =' + obj);
        if(obj != null)
        return obj.Qlik_Market_Application__c == null? false:true;
        return false;
    }
    public Boolean IsQlikMarketAppAssigned(Id ExpertiseId)
    {
        Partner_Expertise_Area__c obj;
    	obj=  ExpertiseAreaMap.get(ExpertiseId);
        return obj.Qlik_Market_Application__c == null? false:true;
    }

    public void SetAllApplicationLookupSolProfilesStatus(Id expertId, String newStatus)
    {
        if(MapofExpertToApplicationLookup.containskey(expertId))
        {
            Partner_Expertise_Application_Lookup__c lookup  = MapofExpertToApplicationLookup.get(expertId);
            lookup.Solution_Profile_1_Status__c = newStatus;
            lookup.Solution_Profile_2_Status__c = newStatus;
            lookup.Solution_Profile_3_Status__c = newStatus;
            lookup.Solution_Profile_4_Status__c = newStatus;
            try
            {
                System.Debug('Updating lookup SolProfiles To Approved...');  
                update lookup;
            }
            catch(Exception ex)
            {
                System.Debug('Exception on Updating lookup Solution profiles');
               // ApexPages.Message errMessage = new ApexPages.Message(ApexPages.Severity.Warning, 'Exception on Updating lookup Solution profiles');
               // ApexPages.addMessage(errMessage);                
            }
        }
    }
    
    
    public void SendAllProfilesforApprovals(Id expertId)
    {
        if(MapofExperttoSolutionProfiles.containskey(expertId))
        {
            List<Solution_Profiles__c> listSols = new List<Solution_Profiles__c>();
            listSols = MapofExperttoSolutionProfiles.get(expertId);
            
            for(Solution_Profiles__c s : listSols)
            {
                Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
                request.setObjectId(s.Id);
                request.setComments('Submitting for your kind review and approval');
                request.setSubmitterId('005D0000002Sg2f');
                //request.setNextApproverIds(new Id[] {'005D0000002Sg2f'});
                Approval.ProcessResult result = Approval.process(request);              
            }
            try
            {
                System.Debug('Sending Solution profiles for approvals ...');  
                update listSols;
            }
            catch(Exception ex)
            {
                System.Debug('Exception on Sending for approvals');               
            }
        }
        
    }
    public Boolean EnableSubmitforReview
    {
        get
        {
            Id expertId = currentExpertId;
            return IsAllProfilesReadyToSubmitforReview(expertId) && IsQlikMarketAppAssigned(expertId);
        }
    }
    
*/  

}