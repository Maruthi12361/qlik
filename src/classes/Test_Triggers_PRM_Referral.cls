@isTest
private class Test_Triggers_PRM_Referral
{
    /******************************************************

        Test_Lead_PRMReferral_UpdateVersionNum

        This method makes unit test of the Test_Lead_PRMReferral_UpdateVersionNum trigger.
        When we check the Accept T&C box on the Partner Referral Opp Reg page the T&C version
        field should be updated from a CustomSetting.

        Changelog:
            2012-02-14  CCE     Created
            2013-06-20  YKA     Updated testMethod TEST_ConfirmLeadAccepted to be in sync with Opportunity validation Rule - OP009_Referring_Accnt_must_be_a_partner
            2014-06-04  TJG     Fix test error
                                System.DmlException: Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY,
                                Record Type ID: this ID value isn't valid for the user: 01220000000J1KRAA0: [RecordTypeId]
			17.03.2017 : Rodion Vakulvsokyi
            2017-03-26  TJG     Fix INVALID_CROSS_REFERENCE_KEY, Record Type ID: this ID value isn't valid for the user: 012D0000000KEKOIA4
    ******************************************************/
    //static final String OppRecordTypeId = '012D0000000KEKO'; // QTNS '012f0000000CuglAAC'; //Qlikbuy CCS Standard II on QTNS  2014-06-04
    static testMethod void Test_Lead_PRMReferral_UpdateVersionNum()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('Test_Lead_PRMReferral_UpdateVersionNum: Starting');
        ID stdLeadRecordType = [select Id from RecordType where Name = 'Standard lead' LIMIT 1].Id;
        ID prorLeadRecordType = [select Id from RecordType where Name = 'Partner Referral Opp Reg' LIMIT 1].Id;

        List<Lead> leads = new List<Lead>();

        //Create a bunch of leads, a few with a standard lead record type and the rest with the lead type
        //  we are interested in (Partner Referral Opp Reg)
        for (integer i=0; i<20; i++) {

            Lead lead = new Lead(   FirstName = 'Test Lead_PRMReferral_UpdateVersionNum',
                                    Country = 'Sweden',
                                    Email = 'asd@asd.com',
                                    LastName = '' + i,
                                    City = 'Stockholm',
                                    Street = 'Street 12-'+i,
                                    Segment__c = 'Enterprise - Target',
                                    Company = 'Company '+i
                                    );

            if (i < 3)
            {
                lead.RecordTypeId = stdLeadRecordType;
            }
            else if (i < 10)
            {
                lead.RecordTypeId = prorLeadRecordType;
                //Make sure the T&C box is checked - to test the insert
                lead.Accepted_T_and_C__c = true;
            }
            else
            {
                lead.RecordTypeId = prorLeadRecordType;
                //Make sure the T&C box is unchecked - to test the update
                lead.Accepted_T_and_C__c = false;
            }
            leads.add(lead);
        }

        test.startTest();

        // insert the leads, then get them back and check to see if the T&C version has been updated
        insert(leads);
        leads = [select Id, FirstName, RecordTypeId, PRM_Referral_T_C_Version__c, Accepted_T_and_C__c from Lead where FirstName = 'Test Lead_PRMReferral_UpdateVersionNum' and Accepted_T_and_C__c = true];

        System.assertEquals(7, leads.size(), 'We expected 7 records to have been updated');
        for (Lead l : leads)
        {
            if (l.RecordTypeId == prorLeadRecordType)
            {
                System.assertNotEquals(null, l.PRM_Referral_T_C_Version__c, 'The version number should have been updated');
            }
        }

        leads = [select Id, FirstName, RecordTypeId, PRM_Referral_T_C_Version__c from Lead where FirstName = 'Test Lead_PRMReferral_UpdateVersionNum'];

        for (Lead l : leads)
        {
            if (l.RecordTypeId == prorLeadRecordType)
            {
                l.Accepted_T_and_C__c = true;
            }
        }

        //When we Accept the T&C the Version field (PRM_Referral_T_C_Version__c) should be updated (it will not be null anymore - the
        // actual value will depend on what is set up in Custom settings)
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update(leads);
        leads = [select Id, FirstName, RecordTypeId, PRM_Referral_T_C_Version__c from Lead where FirstName = 'Test Lead_PRMReferral_UpdateVersionNum'];

        for (Lead l : leads)
        {
            if (l.RecordTypeId == prorLeadRecordType)
            {
                System.assertNotEquals(null, l.PRM_Referral_T_C_Version__c, 'The version number should have been updated');
            }
        }
        test.stopTest();
    }

    /******************************************************

        TEST_ConfirmLeadAccepted

        This method tests that we are unable to convert a PRM referral
        lead unless the Lead Accepted checkbox has been ticked.

        Changelog:
            2012-05-17  CCE     Created

    ******************************************************/
    public static testMethod void TEST_ConfirmLeadAccepted()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('TEST_ConfirmLeadAccepted: Starting');

        List<Lead> insertList = new List<Lead>();
        List<Database.Leadconvert> leadConvertList = new List<Database.Leadconvert>();

        string statusToUse_Raw = 'Raw - Prospect';
        string statusToUse_QualB = 'Qualified - B';

        //string sRecordTypeId = '012L00000004JyJ'; //PRM Referral lead qttest=012L00000004JyJ
        //if (UserInfo.getOrganizationId().startsWith('00D20000000IGPX'))   //Live
        //{
        string sRecordTypeId = '012D0000000K781IAC';    //PRM Referral lead live=012D0000000K781IAC
        //}
        //System.debug('TEST_ConfirmLeadAccepted UserInfo.getOrganizationId()= ' + UserInfo.getOrganizationId());
        //System.debug('TEST_ConfirmLeadAccepted sRecordTypeId= ' + sRecordTypeId);
        //-------------------------------------------------------------------------------
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id
		);
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        //YKA: To deal with validation rule OP009_Referring_Accnt_must_be_a_partner
        // setting the Account.RecordTypeId = "01220000000DOFz or 01220000000DOG4 i.e. Partner Account or OEM Partner Account
        ID AcRecId = [Select Id From RecordType where SobjectType = 'Account' AND Name ='Partner Account'].Id;

        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id,
            RecordTypeId = AcRecId // For Partner Account record type
        );

        List<Account> AccToInsert = new List<Account>();
        insert TestPartnerAccount;

        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact;



        for(Integer i=0; i<2; i++)
        {
            Lead newLead = new Lead(LastName = 'LastTest',
                                    FirstName = 'FirstTest',
                                    Country = 'United States',
                                    Email = 'asd@asd.com',
                                    Status = statusToUse_Raw,
                                    Segment__c = 'Enterprise - Target',
                                    Country_Code__c = QTComp.Id,
                                    Company ='Testing',
                                    RecordTypeId = sRecordTypeId,   //PRM Referral lead qttest=012L00000004JyJ live=012D0000000K781IAC
                                    Lead_Accepted__c = false,
                                    G_P_N_linked_to_person_s_job__c = 'No',
                                    G_P_N_Relevant_to_our_Solution__c = 'No',
                                    Is_the_person_paid_on_this__c = 'No',
                                    Goal_Problem_Need_Description__c = 'Just a test',
                                    Estimated_Net_License_Revenue__c = 100.00,
                                    Estimated_Close_Date__c = date.today().addDays(1),
                                    Estimated_Number_of_Users__c = 2,
                                    Opportunity_Description__c = 'Just a test',
                                    Partner_Contact__c = PartnerContact.Id,
                                    Street = '1 Test Road',
                                    City = 'Testington',
                                    PostalCode = '12345',
                                    Accepted_T_and_C__c = true
                                    );

            insertList.add(newLead);
        }
        insert insertList;

        test.startTest();

        //Now update the Status field as a validation rule stops us doing the first save with it set to 'Qualified - B'
        //for(Lead l: insertList)
        //{
        //  l.Status = statusToUse_QualB;
        //}
        //upsert insertList;

        //Convert lead
        LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true and MasterLabel = :statusToUse_QualB limit 1];
        for(Lead l: insertList)
        {
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(l.id);
			lc.setDoNotCreateOpportunity(True);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            leadConvertList.add(lc);
            System.assertEquals(false, l.Lead_Accepted__c);
        }

        System.debug('TEST_ConfirmLeadAccepted  leadConvertList.size = ' + leadConvertList.size());

        //We are expecting to get an exception when we try to do the Convert as the Lead Accepted flag has not been ticked
        Boolean hadException = false;

        try
        {
            List<Database.LeadConvertResult> lcr = Database.convertLead(leadConvertList);
        }
        catch (Exception e)
        {
            hadException = true;
        }
        System.assertEquals(true, hadException);

        //Now we will tick the Lead Accepted flag and try and do the Convert again
        for(Lead l: insertList)
        {
            l.Lead_Accepted__c = true;
            l.Send_Submit_Email_For_Referral__c = true; //to stop the PRM_Referral_Cannot_Set_Qual_B validation rule firing
        }
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        upsert insertList;

        List<Database.LeadConvertResult> lcr2 = Database.convertLead(leadConvertList);


        for(Database.LeadConvertResult lc: lcr2)
        {
            System.assert(lc.isSuccess());
        }

        test.stopTest();
        System.debug('TEST_ConfirmLeadAccepted: Finishing');
    }

    /******************************************************

        TEST_InfluencingPartnerContactTrigger

        This method tests that we are autopopulate the Influencing Partner Account
        and Influencing PSM/QM fields on the Influencing Partner custom object when
        the Influencing Partner Contact field is filled in and saved.

        Changelog:
            2012-05-21  CCE     Created
            2012-10-16  SAN     Fix the unit test to pass the creation of Opportunity
            2014-12-02  ATC     CR# 9893, adding required field Opportunity_Origin__c

    ******************************************************/
    public static testMethod void TEST_InfluencingPartnerContactTrigger()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('TEST_InfluencingPartnerContactTrigger: Starting');

        Account testAcc = new Account();
        testAcc.name = 'Test';
        testAcc.Navision_Status__c = 'Partner';
        insert testAcc;

        Contact testCon = new Contact();
        testCon.LastName = 'Jury';
        testCon.FirstName = 'James';
        testcon.AccountId = testAcc.Id;
        insert testCon;
        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];

        Opportunity testOpp = New Opportunity (
            Short_Description__c = 'TestOpp - delete me',
            Name = 'TestOppPRM - Delete me',
            Type = 'New Customer',
            Revenue_Type__c = 'Reseller',
            CloseDate = Date.today(),
            StageName = 'Alignment Meeting',
            RecordTypeId = '01220000000DNwY',   //Direct / Reseller - Std Sales Process
            Sell_Through_Partner__c = testAcc.Id,
            Partner_Contact__c = testCon.Id,
            AccountId = testAcc.Id,
            ForecastCategoryName = 'Omitted',
            Signature_Type__c = 'Digital Signature',
			Partner_Fee_Type__c = 'Influence Fee',
            Opportunity_Origin__c = 'SI Driven Solution'
        );
        Insert testOpp;

        test.startTest();

        Influencing_Partner__c testIP = new Influencing_Partner__c(
            Opportunity__c = testOpp.Id,
            Opportunity_Origin__c = testOpp.Opportunity_Origin__c,
            Influencing_Partner_Contact__c = testCon.Id
        );
        Insert testIP;

        Influencing_Partner__c  RetIP = [SELECT Id, Influencing_Partner_Account__c, Influencing_PSM_QM__c  FROM Influencing_Partner__c WHERE Id = :testIP.Id];

        System.assertNotEquals(null, RetIP.Influencing_Partner_Account__c, 'Influencing_Partner_Account__c should not be null');
        System.assertNotEquals(null, RetIP.Influencing_PSM_QM__c, 'Influencing_PSM_QM__c should not be null');

        test.stopTest();
        System.debug('TEST_InfluencingPartnerContactTrigger: Finishing');
    }


    /******************************************************

        TEST_InfluencingPartnerContactTrigger

        This method tests that we are autopopulate the Influencing Partner Account
        and Influencing PSM/QM fields on the Influencing Partner custom object when
        the Influencing Partner Contact field is filled in and saved.

        Changelog:
            2012-05-21  CCE     Created

    ******************************************************/
    public static testMethod void TEST_PRMReferral_OpportunityUpdateReferral()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('TEST_PRMReferral_OpportunityUpdateReferral: Starting');

        //Contact testCon = [SELECT Id, OwnerID, AccountId  FROM Contact WHERE LastName = 'Jury' and FirstName = 'James'];
        //Account testAcc = [SELECT Id, Name  FROM Account WHERE Id = :testCon.AccountId];

        Account testAcc = new Account();
        testAcc.name = 'Test';
        testAcc.Navision_Status__c = 'Partner';
        insert testAcc;

        Contact testCon = new Contact();
        testCon.LastName = 'Jury';
        testCon.FirstName = 'James';
        testcon.AccountId = testAcc.Id;
        insert testCon;
        RecordType rt = [SELECT Id FROM RecordType WHERE Name like '%Sales_QCCS%' limit 1];
        Opportunity testOpp = New Opportunity (
            Short_Description__c = 'TestOpp - delete me',
            Name = 'TestOppPRM - Delete me',
            Type = 'New Customer',
            Revenue_Type__c = 'Direct',
            CloseDate = Date.today(),
            StageName = 'Alignment Meeting',
            //RecordTypeId = OppRecordTypeId,  //'01220000000J1KR',   Qlikbuy CCS Standard -> Qlikbury CSS Standard II on QTNS 2014-06-04
            RecordTypeId = rt.Id,
            AccountId = testAcc.Id,
            Opportunity_Origin__c = 'SI Driven Solution',
            ForecastCategoryName = 'Omitted',
            PRM_Referring_Contact__c = testCon.Id,
            Signature_Type__c = 'Digital Signature',
			Partner_Fee_Type__c = 'Influence Fee',
            Standard_Referral_Margin__c = '0.0'
        );
        Insert testOpp;

        test.startTest();

        Opportunity RetOpp = [SELECT Id, PRM_Referring_Partner__c, PRM_Referring_PSM_QM__c  FROM Opportunity WHERE Id = :testOpp.Id];

        System.assertNotEquals(null, RetOpp.PRM_Referring_PSM_QM__c, 'PRM_Referring_PSM_QM__c should not be null');
        System.assertNotEquals(null, RetOpp.PRM_Referring_Partner__c, 'PRM_Referring_Partner__c should not be null');

        test.stopTest();

        System.debug('TEST_PRMReferral_OpportunityUpdateReferral: Finishing');
    }
        /******************************************************

        TEST_Lead_PRMReferral_UpdatePRMReferringContactField

        This method tests that the PRM_Referring_Contact_new__c field is
        updated from the Partner_Contact__c field.

        Changelog:
            2012-05-31  CCE     Created

    ******************************************************/
    public static testMethod void TEST_Lead_PRMReferral_UpdatePRMReferringContactField()
    {
        QTTestUtils.GlobalSetUp();
        System.debug('TEST_Lead_PRMReferral_UpdatePRMReferringContactField: Starting');

        List<Lead> insertList = new List<Lead>();
        List<Database.Leadconvert> leadConvertList = new List<Database.Leadconvert>();

        string statusToUse = 'Raw - Prospect';  //'Qualified - B';

        //string sRecordTypeId = '012L00000004JyJ'; //PRM Referral lead qttest=012L00000004JyJ
        //if (UserInfo.getOrganizationId().startsWith('00D20000000IGPX'))   //Live
        //{
        string sRecordTypeId = '012D0000000K781IAC';    //PRM Referral lead live=012D0000000K781IAC
        //}

        //-------------------------------------------------------------------------------
        Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id
		);
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];


        Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id
        );

        List<Account> AccToInsert = new List<Account>();
        insert TestPartnerAccount;

        Contact PartnerContact = new Contact(
            FirstName = 'Mr Partner',
            LastName = 'PP',
            AccountId = TestPartnerAccount.Id
        );
        insert PartnerContact;



        for(Integer i=0; i<10; i++)
        {
            Lead newLead = new Lead(LastName = 'LastTest',
                                    FirstName = 'FirstTest',
                                    Country = 'USA',
                                    Email = 'asd@asd.com',
                                    Status = statusToUse,
                                    Segment__c = 'Enterprise - Target',
                                    Country_Code__c = QTComp.Id,
                                    Company ='Testing',
                                    RecordTypeId = sRecordTypeId,   //PRM Referral lead qttest=012L00000004JyJ live=012D0000000K781IAC
                                    Lead_Accepted__c = false,
                                    G_P_N_linked_to_person_s_job__c = 'No',
                                    G_P_N_Relevant_to_our_Solution__c = 'No',
                                    Is_the_person_paid_on_this__c = 'No',
                                    Goal_Problem_Need_Description__c = 'Just a test',
                                    Estimated_Net_License_Revenue__c = 100.00,
                                    Estimated_Close_Date__c = date.today().addDays(1),
                                    Estimated_Number_of_Users__c = 2,
                                    Opportunity_Description__c = 'Just a test',
                                    Partner_Contact__c = PartnerContact.Id
                                    );

            insertList.add(newLead);
        }
        insert insertList;

        Lead LeadRead = [Select Id, PRM_Referring_Contact_new__c, Partner_Contact__c from Lead where FirstName = 'FirstTest' limit 1];
        System.debug('TEST_Lead_PRMReferral_UpdatePRMReferringContactField: Partner_Contact__c =' + LeadRead.Partner_Contact__c);
        System.debug('TEST_Lead_PRMReferral_UpdatePRMReferringContactField: PRM_Referring_Contact_new__c =' + LeadRead.PRM_Referring_Contact_new__c);
        System.assertEquals(LeadRead.PRM_Referring_Contact_new__c,LeadRead.Partner_Contact__c);

        System.debug('TEST_Lead_PRMReferral_UpdatePRMReferringContactField: Finishing');
    }

}