global class DisableDuplicateULCs
{
	
}
/*
    Initial development: MTM
    2013 12 03  MTM CR# 8336  Disable duplicate ULCs assigned to a single contact.


global class DisableDuplicateULCs implements Database.Batchable<ULC_Details__c> 
{
    global DisableDuplicateULCs(){System.Debug('Starting: DisableDuplicateULCs');}
    
    global Iterable<ULC_Details__c> start(Database.BatchableContext BC) {        
        // Get all contacts with multiple ULCs attached to it
        System.Debug('Starting: DisableDuplicateULCs:Start collecting ULCs');
        List<Id> contactIds = new List<Id>();
        for (AggregateResult aResult : [Select count(u.Id) IdCount,u.ContactId__c From ULC_Details__c u
             where u.ULCStatus__c = 'Active' and IsDeleted = false and u.ContactId__c != null            
             group by u.ContactId__c
             having count(u.Id) > 1 LIMIT 48])
                    
        {
                contactIds.Add((Id)aResult.get('ContactId__c'));
        }
        
        List<ULC_Details__c> ulcToUpdate = new List<ULC_Details__c>();
        for (Id  contactId: contactIds)
        {
            Map<Id, ULC_Details__c> ulcs = new Map<Id, ULC_Details__c>([select Id, ContactId__c, ContactId__r.Allow_Partner_Portal_Access__c, ContactId__r.Name, ContactId__r.Is_LMS_User__c, Name, ULCStatus__c, IsDeleted, u.LastModifiedDate, u.LastActivityDate, u.CreatedDate,u.ULCName__c 
            From ULC_Details__c u where ContactId__c = : contactId]);

//Select most active ULC from the list          
            List<AggregateResult> ulcHistory =  [Select ULC_Detail__c, max(Timestamp__c) time
                                                 From ULC_Login_History__c 
                                                 where ULC_Detail__c = : ulcs.keySet()
                                                 group by ULC_Detail__c         
                                                 order by max(Timestamp__c) desc limit 1];

// Don't remove any ULC if there is no login history                                                 
            if(ulcHistory.Size() > 0)
            {
                Id ulcId = (Id)ulcHistory[0].get('ULC_Detail__c');              
                ulcs.remove(ulcId); //Don't remove most active ULC
                ulcToUpdate.AddAll(ulcs.values());                
            }       
        }
        System.Debug('ULC to disable count = ' + ulcToUpdate.size());
        System.Debug('Ending: DisableDuplicateULCs:Start collecting ULCs');
        return(ulcToUpdate);        
    }
    
    global void execute(Database.BatchableContext BC, List<ULC_Details__c> scope) {
        
        System.Debug('Starting: DisableDuplicateULCs');
        List<ULC_Details__c> ulcToUpdate = new List<ULC_Details__c>();
        System.debug('Disabling {0} ULCs ' + scope.Size());
//Disable duplicate ULC user        
        for(ULC_Details__c ulc: scope)
        {
            System.debug(String.Format('Disabling ULC with ID {0} and Name {1}', new String[] {ulc.Id, ulc.ULCName__c} ));
            ulc.ULCStatus__c = 'Disabled';
            ulcToUpdate.add(ulc);
        }
        update ulcToUpdate;
        System.Debug('Finishing: DisableDuplicateULCs');    
    }
    
    global void finish(Database.BatchableContext BC) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email, 'rdz@qlikview.com'};
        
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex DisableDuplicateULCs status ' + a.Status);
        mail.setPlainTextBody
        ('The batch Apex job  DisableDuplicateULCs processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    } 

}*/