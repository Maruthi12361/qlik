/**
*   extcqb  2020-01-30 IT-2196 Make chatter posts about case attachments 'QlikTech Only'
*/

@IsTest
private class CaseAttachmentHandlerTest {
    @IsTest
    static void runTest() {
        String liveChatRecordTypeId;
        RecordTypeInfo liveChatRecordTypeInfo = Schema.SObjectType.Case.getRecordTypeInfosByName()
                .get('Live Chat Support Record Type');
        if (liveChatRecordTypeInfo != null) {
            liveChatRecordTypeId = liveChatRecordTypeInfo.getRecordTypeId();
        }

        Case caseObj = new Case();
        caseObj.RecordTypeId = liveChatRecordTypeId;
        insert caseObj;

        FeedItem feedItem1 = new FeedItem(Type = 'CreateRecordEvent', ParentId = caseObj.Id);
        insert feedItem1;
        FeedItem feedItem2 = new FeedItem(Type = 'CreateRecordEvent', ParentId = caseObj.Id);
        insert feedItem2;

        Test.startTest();
        Attachment attachment = new Attachment(
                Body = Blob.valueOf('sdfsdfsdfsdf'),
                Name = 'test.html',
                ParentId = caseObj.Id
        );
        insert attachment;
        Test.stopTest();

        List<FeedItem> feedItems = [SELECT Id, Visibility FROM FeedItem WHERE Type = 'CreateRecordEvent' ORDER BY CreatedDate DESC];
        System.assertEquals(CaseAttachmentHandler.INTERNAL_VISIBILITY, feedItems[0].Visibility);
    }
}