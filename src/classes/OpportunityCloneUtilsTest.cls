// CR# 10215
// Test OpportunityCloneUtils class
// Change log:
// October 21, 2014 - Initial Implementation - Madhav Kakani - Fluido Denmark
// Nov  14, 2014 MTM fixed test failures
// 2015-10-27 IRN Winter 16 Issue- removed seeAllData
//24.03.2017 Rodion Vakulovskyi       
@isTest 
private class OpportunityCloneUtilsTest {
    static testMethod void testOpportunityCloneUtils() {
        QTTestUtils.GlobalSetUp();
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        User user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');

        System.RunAs(user) {                                 
            Id qbId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
            system.assert(qbId != null);
                        
            Opportunity opp = QTTestUtils.createMockOpportunity('Test Opp', 'Test Opp', 'New Customer', 'Direct', 'Prospecting', qbId, 'GBP', user, true);
            	    
            system.assert(opp.Id != null);  

            Sphere_Of_Influence__c soi = QTTestUtils.createMockSOI(opp, true);
            system.assert(soi.Id != null);
            
            Test.StartTest();

            Opportunity oppClone = opp.clone(false, true);
            system.assert(oppClone != null);
            insert oppClone;
            system.assert(oppClone.Id != null);

            Map<Id, Id> mapIds = new Map<Id, Id>();
            mapIds.put(oppClone.Id, opp.Id);
            OpportunityCloneUtils.cloneChildren(mapIds);

            // verify that the SOI has been copied as a child of the cloned opportunity
            soi = [SELECT Id FROM Sphere_Of_Influence__c WHERE Opportunity__c = :oppClone.Id];
            system.assert(soi.Id != null);
            
            Test.StopTest();                               
        }
    } // testOpportunityCloneUtils  
} // class OpportunityCloneUtilsTest