/**************************************************
* CR# 19391 – Add in Counters to Calculate Days and Date Stamp Entry Point into Waterfall Stage
* Change Log:
* 2015-02-26 Madhav Kakani: Test case for ContactStatusAge_Trigger
** 2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
* 2019-03-18 CCE CHG0035745 BMW-1364 Update test. Setting New FU Req field as new validation rule LE020 requires it
**************************************************/
@isTest
private class ContactStatusAge_TriggerTest {

    @testSetup
    public static void Setup() {
        QuoteTestHelper.createCustomSettings();
    }
    // test going forwards with the stages
    static testmethod void testContactStatusTrigger1(){
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {                      
            Account acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
            system.assert(acct.Id != null);
            Contact ct = QTTestUtils.createMockContact(acct.Id);
            system.assert(ct.Id != null);
            
            Test.startTest();

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.New_Follow_Up_Required__c = true; //Setting New FU Req field as new validation rule CO020 requires it
            ct.Contact_Status__c = 'Follow-Up Required';
            update ct;
            Contact ctTmp = [SELECT Inquiry_Start_Date_Current__c, AQL_Start_Date_Current__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.Inquiry_Start_Date_Current__c != null);
            system.assert(ctTmp.AQL_Start_Date_Current__c != null);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;

            ct.Contact_Status__c = 'Goal Identified';
            update ct;
            ctTmp = [SELECT MQL_Start_Date_Current__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.MQL_Start_Date_Current__c != null);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Goal Confirmed';
            update ct;
            ctTmp = [SELECT MQL_Start_Date_Current__c, AQL_to_MQL__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.MQL_Start_Date_Current__c != null);
            system.assert(ctTmp.AQL_to_MQL__c == true);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Champion';
            update ct;
            ctTmp = [SELECT SAL_Start_Date_Current__c, SQL_Start_Date_Current__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.SAL_Start_Date_Current__c != null);
            system.assert(ctTmp.SQL_Start_Date_Current__c != null);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Closed Won';
            update ct;
            ctTmp = [SELECT Closed_Won_Date__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.Closed_Won_Date__c != null);

            Test.stopTest();
        }
    }

    // test going backwards with the stages
    static testmethod void testContactStatusTrigger2(){
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) {                      
            Account acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
            system.assert(acct.Id != null);
            Contact ct = QTTestUtils.createMockContact(acct.Id);
            system.assert(ct.Id != null);
            
            Test.startTest();

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Closed Won';
            update ct;
            Contact ctTmp = [SELECT Closed_Won_Date__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.Closed_Won_Date__c != null);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Goal Confirmed';
            update ct;
            ctTmp = [SELECT SQL_to_Closed_Won__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.SQL_to_Closed_Won__c == false);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Goal Identified';
            update ct;
            ctTmp = [SELECT MQL_to_SAL__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.MQL_to_SAL__c == false);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.New_Follow_Up_Required__c = true; //Setting New FU Req field as new validation rule CO020 requires it
            ct.Contact_Status__c = 'Follow-Up Required';
            update ct;
            ctTmp = [SELECT AQL_to_MQL__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.AQL_to_MQL__c == false);

            //Semaphores.TriggerHasRun(1);
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Suspect';
            update ct;
            ctTmp = [SELECT Inquiry_to_AQL__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.Inquiry_to_AQL__c == true);

            Test.stopTest();
        }
    }

}