@isTest

/***************************************************************************************************************************************

	Changelog:
		2012-02-15  CCE		Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
                            (Moved from TestPseProjectClosedTrigger.cls)
        2017-11-16  ext_vos Added the test cases for testAfterActions().                      
****************************************************************************************************************************************/
// ext_vos     2017-11-09   was moved to PseProjectTriggerTest.cls
// to be deleted
private class TestPseProjectClosedTrigger {

    /*static testMethod void testAfterActions() {
        QTTestUtils.GlobalSetUp();
        // sample user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert u;
        // Run as user with permission for assignments
        System.runAs(u) {
            // setup account
             Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
             QlikTech_Company__c QTComp = new QlikTech_Company__c(
                                    			Name = 'GBR',
                                    			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
                                    			Country_Name__c = 'United Kingdom',
                                    			Subsidiary__c = testSubs1.id);
			insert QTComp;
			QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
		
			Account testAccount = new Account(Name = 'Test Account', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        	insert testAccount;
    
            // setup Project
            List<pse__Proj__c> projects = new List<pse__Proj__c>();
            for (Integer j = 0; j <= 2; j ++) {
                projects.add(new pse__Proj__c(Name = 'Name' + j, pse__Stage__c = 'Open', 
                                                pse__Account__c = testAccount.Id,
                                                Engagement_Partner__c = testAccount.Id,
                                                pse__Start_Date__c = Date.today().addDays(-10),
                                                pse__End_Date__c = Date.today().addDays(30),
                                                Key_Engagement_Features__c = 'QlikView',
                                                Purpose_of_Engagement__c = 'Test',
                                                Sales_Classification__c = 'Internal',
                                                Customer_Critial_Success_Factors__c = 'Test',
                                                Invoicing_Type__c = 'Deferred'));
            }
            Semaphores.PseProjectTriggerAfterInsert = false;
            insert projects;

            // setup resource
            Contact resource = new Contact(LastName = 'Test Resource', pse__Is_Resource__c = true, 
                                    pse__Is_Resource_Active__c = true, AccountId = testAccount.Id);
            insert resource;
    
            // permission
            pse__Permission_Control__c pc = new pse__Permission_Control__c(pse__User__c = u.Id, pse__Resource__c = resource.Id, 
                                                    pse__Cascading_Permission__c = true, pse__Staffing__c = true);
            insert pc;
            List<pse__Permission_Control__c> controls = new List<pse__Permission_Control__c>();
            for (pse__Proj__c pr : projects) {
                controls.add(new pse__Permission_Control__c(pse__User__c = u.Id, pse__Cascading_Permission__c = true, 
                                pse__Staffing__c = true, pse__Project__c = pr.Id));
            }
            insert controls;
    
            // setup assignments
            List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();
            for (pse__Proj__c pr : projects) {
                assignments.add(new pse__Assignment__c(Name = 'Test Assignment' + pr.Name, 
                    pse__Project__c = pr.Id, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                    pse__Resource__c = resource.Id));
            }   
            insert assignments;

            // Test closing project           
            // cancel
            pse__Proj__c pr = projects[0];
            pr.pse__Stage__c = 'Cancelled';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            String testStatus = [SELECT pse__Status__c FROM pse__Assignment__c WHERE pse__Project__c = :pr.Id limit 1].pse__Status__c;
            System.assertEquals('Closed', testStatus);

            // complete
            pr = projects[1];
            pr.pse__Stage__c = 'Completed';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            testStatus = [SELECT pse__Status__c FROM pse__Assignment__c WHERE pse__Project__c = :pr.Id limit 1].pse__Status__c;
            System.assertEquals('Closed', testStatus);

            // close
            pr = projects[2];
            pr.pse__Stage__c = 'Closed';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            testStatus = [SELECT pse__Status__c FROM pse__Assignment__c WHERE pse__Project__c = :pr.Id limit 1].pse__Status__c;
            System.assertEquals('Closed', testStatus);

            // test update SE_Project
            Project_SE__c seProj = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c = :pr.Id];
            system.assertNotEquals(null, seProj);

            pr.Name = 'new Name 2';
            Semaphores.PseProjectTriggerAfterUpdate = false;
            update pr;

            seProj = [Select Id, Name FROM Project_SE__c WHERE Master_Project_Id__c = :pr.Id];
            system.assertEquals(pr.Name, seProj.Name);

            // test delete project
            Semaphores.PseProjectTriggerAfterDelete = false;
            delete pr;
            List<Project_SE__c> seProjects = [Select Id FROM Project_SE__c WHERE Master_Project_Id__c = :pr.Id];
            system.assertEquals(0, seProjects.size());            
        }
    }*/
    
}