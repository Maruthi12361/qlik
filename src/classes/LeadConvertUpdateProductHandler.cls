/***************************************************
Object: Lead
Description: For Digital Buyers Journey (DBJ) Project. BMW-1637. There are two ID fields on the Product Trial and Product User tables.
             One field holds the lead ID, if the lead is converted the ID needs to be deleted 
             and the contact ID be placed instead. 
Change Log:
 20190724    CCE Initial development
 20190729    DKF Added code to populate the Account on the Product Trial
******************************************************/
public class LeadConvertUpdateProductHandler {
	public LeadConvertUpdateProductHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
		System.debug('LeadConvertUpdateProductHandler: starting');
	    if (!Semaphores.TriggerHasRun('LeadConvertUpdateProductHandler'))
	    {
	        System.debug('LeadConvertUpdateProductHandler: passed semaphore.');
	        List<Product_Trial__c> ProductTrialToBeUpdated = new List<Product_Trial__c>();
	        List<Product_User__c> ProductUserToBeUpdated = new List<Product_User__c>();
	        Map<Id, Id> LeadToContact = new Map<Id, Id>();
            Map<Id, Id> LeadToAccount = new Map<Id, Id>();

	        for (integer i = 0; i < triggerNew.size(); i++)
	        {
	            if (triggerNew[i].IsConverted == true && triggerOld[i].IsConverted == false && triggerNew[i].ConvertedContactId != null)
	            {
	                LeadToContact.put(triggerNew[i].Id, triggerNew[i].ConvertedContactId);
	            }
                
                if (triggerNew[i].IsConverted == true && triggerOld[i].IsConverted == false && triggerNew[i].ConvertedAccountId != null)
                {
                    LeadToAccount.put(triggerNew[i].Id, triggerNew[i].ConvertedAccountId);
                }
	        }
	        System.debug('LeadConvertUpdateProductHandler: LeadToContact = ' + LeadToContact);    

	        if (!LeadToContact.isEmpty()) { //only continue if we have something to work with
	            for (Product_Trial__c pt : [select Id, Lead__c, Contact__c, Contact_Account__c from Product_Trial__c where Lead__c in :LeadToContact.keySet()])
	            {
	                pt.Contact__c = LeadToContact.get(pt.Lead__c);
                    pt.Contact_Account__c = LeadToAccount.get(pt.Lead__c); 
	                pt.Lead__c = null;                    
	                ProductTrialToBeUpdated.Add(pt);
	            }
	            if (ProductTrialToBeUpdated.size() > 0)
	            {
	                update ProductTrialToBeUpdated;     
	            }    
	            for (Product_User__c pu : [select Id, Lead__c, Contact__c from Product_User__c where Lead__c in :LeadToContact.keySet()])
	            {
	                pu.Contact__c = LeadToContact.get(pu.Lead__c);
	                pu.Lead__c = null;
	                ProductUserToBeUpdated.Add(pu);
	            }
	            if (ProductUserToBeUpdated.size() > 0)
	            {
	                update ProductUserToBeUpdated;     
	            }    
	        }

	    }
	    System.debug('LeadConvertUpdateProductHandler: finishing');
	}
}