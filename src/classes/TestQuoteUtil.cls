public class TestQuoteUtil {

    static Id AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id; // End User Account, QA '01220000000DOFu'
    static Id AccRecordTypeId_PartnerAccount = '01220000000DOFu'; // not used in this class, but ID should be in QA '01220000000DOFz'
    static Id OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id; // Sales QCCS, QA '012260000000YTi'
    
    public static QlikTech_Company__c createQCompany(String subsId) {
           QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'United Kingdom',
            CurrencyIsoCode = 'GBP',
            Language__c = 'English',
            Subsidiary__c = subsId,
            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
        );
        insert QTComp;
    return QTComp;
    }
    
    public static Account createReseller() {
          Account TestResellerAccount = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Valrules Test Ltd.',
                Sub_Reseller__c = false,
                Navision_Status__c = 'Partner',
                QlikTech_Company__c = 'QlikTech Inc',
                Payment_Terms__c = '30 days',
                INT_NetSuite_InternalID__c = '12345'

                //LockAccount__c = true               
            );
        insert TestResellerAccount;
    return TestResellerAccount;
    }
    
    public static Contact createContact(String ressellerAccountId) {
         Contact TestContact = new Contact (
                AccountId = ressellerAccountId,
                OwnerId = UserInfo.getUserId(),
                LastName = 'Wilson-Hemingway',
                FirstName = 'Daryl',
                Email = 'dwy@google.com'
         );
            
    insert TestContact;
    return TestContact;
    }
    
    public static Account createAccount(String QTCompId) {
        System.debug('createAccount');
         QlikTech_Company__c QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTCompId];
        System.debug('QTComp' + QTComp);
        Account acc = new Account(
            Name = 'Test Val Rule 1',
            RecordTypeId = AccRecordTypeId_EndUserAccount,
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Legal_Approval_Notes__c = 'Some legal stuff',
            Billing_Country_Code__c = QTComp.Id,
            Payment_Terms__c =  '30 days',
            INT_NetSuite_InternalID__c = '12345'
            //LockAccount__c = true
        );
        insert acc;
        System.debug('after insert account ' + acc);
    return acc;
    }


    public static Opportunity createOpportunity(Account acc, Account TestResellerAccount) {
       Opportunity Opp = new Opportunity(
          RecordTypeId = OppRecordTypeId,
          Name = 'Test Val Rule Opp',
          CurrencyIsoCode = 'USD',
          Short_Description__c = 'Partner Reseller',
          //pse__Is_Services_Opportunity__c = false,
          //Competitors__c = null,
          StageName = 'Champion',
          CloseDate = Date.today().addDays(7),
          Type = 'New Customer',
          AccountId = acc.Id,
          ForecastCategoryName = 'Omitted',
          Included_Products__c = 'Qlik Sense',
          Sell_Through_Partner__c = TestResellerAccount.Id
        );
        insert Opp;
    return Opp;
    }
    
    public static Subsidiary__c createSubsidiary() {
        Subsidiary__c subs = new Subsidiary__c(
            Legal_Entity_Name__c = 'test',
            Netsuite_Id__c = 'test',
            Legal_Country__c = 'Sweden'
            );
        insert  subs;
    return subs;
    }
     
     
     public static void createCustomSettings() {
          QTCustomSettings__c cs = new  QTCustomSettings__c();
              cs.Name = 'Default';
              cs.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
          insert cs;
          QTCustomSettings__c cs2 = new QTCustomSettings__c();
              cs2.Name = 'Default';
              cs2.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW,012D0000000KIknIAG,012D0000000KIkxIAG';
          insert cs2;
          Steelbrick_Settings__c cs3 = new Steelbrick_Settings__c();
              cs3.Name = 'SteelbrickSettingsDetails';
              cs3.SBPricebookId__c = '01s20000000E0PWAA0';
           insert cs3;
           Steelbrick_Settings__c stlSett = new Steelbrick_Settings__c();
              stlSett.Name = 'SteelbrickSettingsDetails';
              stlSett.ExcludedCountries__c = 'Caledonia,Oman,Qatar,Saudi Arabia,Sri Lanka,Tajikistan';
           insert stlSett;
     }

     public static SBQQ__Quote__c createQuote(RecordType rType, Account testAcc) {
      Contact c = new Contact();
      c.LastName = 'lastName';
      c.FirstName = 'firstName';
      c.LeadSource = 'Qonnect';
      c.Contact_Status__c = 'Unknown';
      c.AccountId = testAcc.Id;
      c.Email = 'test@test.com';
      insert c;
        SBQQ__Quote__c quoteForTest = new SBQQ__Quote__c(
            SBQQ__Account__c = testAcc.id,
            SBQQ__Status__c = 'Open',
            SBQQ__Type__c = 'Quote',
            Revenue_Type__c = 'Direct',
            RecordTypeId = rType.id,
            Short_Description__c = 'test',
            Partner_Special_Term_Request__c = false,
            Legal_Approval_Triggered__c = false,
            Quote_Recipient__c = c.Id,
            TypeUnmanaged__c = 'Existing Customer',
            CurrencyIsoCode  = 'SEK'
        );
        insert quoteForTest;
        return quoteForTest;
     }


     public static SBQQ__Quote__c createQuotePlaced(RecordType rType, Account testAcc) {
        Contact c = new Contact();
            c.LastName = 'lastName';
            c.FirstName = 'firstName';
            c.LeadSource = 'Qonnect';
            c.Contact_Status__c = 'Unknown';
            c.AccountId = testAcc.Id;
        insert c;
        SBQQ__Quote__c quoteForTest = new SBQQ__Quote__c(
            SBQQ__Account__c = testAcc.id,
            SBQQ__Status__c = 'Open',
            SBQQ__Type__c = 'Quote',
            Revenue_Type__c = 'Direct',
            Partner_Special_Term_Request__c = true,
            RecordTypeId = rType.id,
            Quote_Recipient__c = c.Id,
            Short_Description__c = 'test',
            Legal_Approval_Triggered__c = false,
            Sell_Through_Partner__c = testAcc.id
        );
        insert quoteForTest;
        return quoteForTest;
     }

    public static SBQQ__QuoteLine__c createMockQuoteLine(Id quoteId, Id productId, String serverSiteSetup, Integer quantity, String licenseCode, String groupText, String family, String currencyIsoCode, Integer price, Integer percent, String licenseId, String lefDetail){
        SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
        quoteLine.SBQQ__Quantity__c = quantity;
        //quoteLine.Server_Site_Setup__c = serverSiteSetup;
        quoteLine.License_Code__c = licenseCode;
        quoteLine.SBQQ__Quote__c = quoteId;
        quoteLine.SBQQ__Product__c = productId;
        quoteLine.Product_Group_Text__c = groupText;
        quoteLine.Family__c = family;
        quoteLine.CurrencyIsoCode = currencyIsoCode;
        quoteLine.SBQQ__CustomerPrice__c = price;
        quoteLine.SBQQ__DefaultSubscriptionTerm__c = 12;
        quoteLine.SBQQ__ListPrice__c = price;
        quoteLine.SBQQ__NetPrice__c = price;
        quoteLine.SBQQ__Number__c = 4;
        quoteLine.SBQQ__PartnerPrice__c = price;
        quoteLine.SBQQ__PricingMethod__c = 'List';
        quoteLine.SBQQ__SubscriptionPercent__c = percent;
        quoteLine.SBQQ__SubscriptionPricing__c = 'Percent Of Total';
        quoteLine.SBQQ__SubscriptionScope__c = 'Quote';
        quoteline.Disable_AJAX__c = false;
        quoteLine.LicenseProductID__c = licenseId;
        quoteline.LEFDetailID__c = lefDetail;
        insert quoteLine;
        return quoteline;
    }

    public static Quote_Line_Sub_Line__c createMockQuoteLineSubLine(Id quoteLine, String controlNbr, String currencyIsoCode, String licenseNbr, String lp_LEFquoteId, String lpId, String rb_licenseProdcutId, String  lefDetail){
        Quote_Line_Sub_Line__c qlsl = new Quote_Line_Sub_Line__c();
        qlsl.Control_Number__c = controlNbr;
        qlsl.CurrencyIsoCode = currencyIsoCode;
        qlsl.License_Number__c = licenseNbr;
        qlsl.LicenseProduct_LEF_Quote_ID__c = lp_LEFquoteId;
        qlsl.LicenseProductID__c = lpId;
        qlsl.Quote_Line__c = quoteline;
        qlsl.Required_by_LicenseProductID__c = rb_licenseProdcutId;
        qlsl.LEFDetailID__c = lefDetail;
        insert qlsl;
        return qlsl;
    }
    public static Product2 createMockProduct(String name, String family, Integer percent, Boolean doInsert ){
      Subsidiary__c testSubs2 = QuoteTestHelper.createSubsidiary();
            insert testSubs2;
        
      List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs2.id)};
            insert listOfcreate;
        
      Product2 product = new Product2();
      product.Name=name;
      product.Accounts_Specific__c=FALSE;
      product.All_Countries_Visibility__c=FALSE;
      //product.Application_Name__c='QVSERVER';
      product.CanUseRevenueSchedule=TRUE;
      product.Contest_Promo_Code__c=FALSE;
      product.CPQ_Product__c=TRUE;
      product.CurrencyIsoCode='USD';
      product.Deferred_Revenue_Account__c='26010 Deferred Revenue : Deferred revenue - license';
      product.Family=family;
      product.Generate_License__c=TRUE;
      product.Income_Account__c='40000 License revenue';
      product.IsActive=TRUE;
      product.LEF_Format_Text__c='PRODUCTLEVEL:10';
      product.License_Code__c='QVSERVER';
      product.Product_Group__c='QlikView';
      product.Product_Level__c=10;
      product.ProductCode='4750';
      product.pse__IsServicesProduct__c=FALSE;
      product.SBQQ__AssetConversion__c='One per unit';
      product.SBQQ__BlockPricingField__c='Quantity';
      product.SBQQ__Component__c=FALSE;
      product.SBQQ__ConfigurationEvent__c='Always';
      product.SBQQ__ConfigurationType__c='Required';
      product.SBQQ__CostEditable__c=FALSE;
      product.SBQQ__CustomConfigurationRequired__c=FALSE;
      product.SBQQ__DefaultQuantity__c=1;
      product.SBQQ__DescriptionLocked__c=FALSE;
      product.SBQQ__ExcludeFromMaintenance__c=FALSE;
      product.SBQQ__ExcludeFromOpportunity__c=FALSE;
      product.SBQQ__ExternallyConfigurable__c=FALSE;
      product.SBQQ__HasConfigurationAttributes__c=TRUE;
      product.SBQQ__Hidden__c=FALSE;
      product.SBQQ__HidePriceInSearchResults__c=FALSE;
      product.SBQQ__IncludeInMaintenance__c=TRUE;
      product.SBQQ__NewQuoteGroup__c=FALSE;
      product.SBQQ__NonDiscountable__c=FALSE;
      product.SBQQ__NonPartnerDiscountable__c=FALSE;
      product.SBQQ__Optional__c=FALSE;
      product.SBQQ__OptionSelectionMethod__c='Click';
      product.SBQQ__PriceEditable__c=FALSE;
      product.SBQQ__PricingMethod__c='List';
      product.SBQQ__PricingMethodEditable__c=FALSE;
      product.SBQQ__QuantityEditable__c=TRUE;
      product.SBQQ__QuantityScale__c=0;
      product.SBQQ__ReconfigurationDisabled__c=FALSE;
      product.SBQQ__SubscriptionBase__c='List';
      product.SBQQ__SubscriptionPercent__c = percent;
      product.SBQQ__Taxable__c=FALSE;
     // product.Server_Site_Setup__c='Qlik Sense Enterprise';
      product.Team_Discount__c=FALSE;
      product.Use_Price_Level__c=FALSE;
      product.WaitListCount__c=0;
      if(doInsert){
        insert product;  
      }
      return product;
    }

    public static Product2 createMockProduct(String name) {
      return createMockProduct(name, 'Licenses', 0, true );      
    }
    
    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr, 
                                            Country_Name__c = countryName, 
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
}