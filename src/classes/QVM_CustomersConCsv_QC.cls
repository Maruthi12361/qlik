/*
@author: Malay Desai, Slalom LLC
@date: 01/24/2019
@description: Controller for the QVM customers list page for exporting to excel in Qlik Commerce Community
*/
public without sharing class QVM_CustomersConCsv_QC {
	
    public List<QVM_Customer_Products__c> CusProducts {
		get {
			String SoqlQuery = System.currentPagereference().getParameters().get('s');
			System.debug('SoqlQuery=' + SoqlQuery);
			if (null == CusProducts && null <> SoqlQuery) {
				CusProducts = Database.query(EncodingUtil.urlDecode(SoqlQuery, 'UTF-8'));
			}
			return CusProducts; 
		}
		set;
	}
	//export to excel - returns a page reference to the AccountDataExcel page
	public PageReference exportCustomersExcel() {
	   return Page.QVM_CustomersCSV_QC;
	}	
}