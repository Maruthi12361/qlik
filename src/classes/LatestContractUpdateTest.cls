/**     * File Name:LatestContractUpdateTest
        * Description : Test class for LatestContractUpdate trigger for Sales Channel
        * @author : SAN
*/
@isTest
private class LatestContractUpdateTest {
    
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    @isTest static void test_method_emptyContractRelatedList() {
        Test.startTest();
            QuoteTestHelper.createCustomSettings();
            Profile p = [select id from profile where name='System Administrator']; 
            User u = QTTestUtils.createMockSystemAdministrator();
            Account testAcc = QTTestUtils.createMockAccount('TestCompany', u);       
        
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAcc;
    
            Sales_Channel__c ca = new Sales_Channel__c();
            ca.License_Customer__c = testAcc.Id;
            insert ca;
            Contract contract = new Contract();
            contract.Sales_Channel__c = ca.Id;
            contract.AccountId = testAcc.Id;
            insert contract;
            
            ca.Revenue_Type__c = 'Direct';
            ca.Latest_Contract__c = contract.Id;
            update ca;
            
            System.assertEquals(contract.Id, ca.Latest_Contract__c); 
            
            contract.Sales_Channel__c = null;
            update contract;
           
            ca.Support_Level__c = 'Basic';
            update ca;
                
            ca = [select Id, Latest_Contract__c  from Sales_Channel__c where Id =: ca.Id];
            
           
            
            System.assertEquals(null, ca.Latest_Contract__c); 

        Test.stopTest();
    }
    
    @isTest static void test_method_withContractRelatedList() {
        Test.startTest();
            QuoteTestHelper.createCustomSettings();
            Profile p = [select id from profile where name='System Administrator']; 
            User u = QTTestUtils.createMockSystemAdministrator();
            Account testAcc = QTTestUtils.createMockAccount('TestCompany', u);       
        
            testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAcc;
    
            Sales_Channel__c ca = new Sales_Channel__c();
            ca.License_Customer__c = testAcc.Id;
            insert ca;
            Contract contract = new Contract();
            contract.Sales_Channel__c = ca.Id;
            contract.AccountId = testAcc.Id;
            insert contract;
            
            ca.Revenue_Type__c = 'Direct';
            ca.Latest_Contract__c = contract.Id;
            
            update ca;
            
            
            ca = [select Id, Latest_Contract__c  from Sales_Channel__c where Id =: ca.Id];
            
            System.assertEquals(contract.Id, ca.Latest_Contract__c); 
        Test.stopTest();
    }

}