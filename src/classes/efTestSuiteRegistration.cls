//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//          
// Change History
// --------------
// Author: YKA
// Change date: 21/06/2013
// Intent: Splitting the method interactionUnitTest into two methods interactionUnitTest1 and interactionUnitTest2. This is to deal with the errors we faced while deploying some CRs to production.
// 
//Author: MTM
// Change date: 18/02/2014
// Intent: This is to deal with the errors we faced while deploying some CRs to production.
//
// TJG  07/04/2014      CR 11469 https://eu1.salesforce.com/a0CD000000gAG2M
//                      Restructure APEX test class so does not only look for code coverage. 
//
//                      Added a number of assert statements, made sure all test methods call
//                      startTest and stopTest
// IRN 2015-10-22       Added webservice mock since the winter 16 release
//	06.02.2017  RVA :   changing CreateAcounts methods
//  29.03.2017 Rodion Vakulvoskyi fix test failures commenting QuoteTestHelper.QTTestUtils.GlobalSetUp();                   
// AIN 2017-03-30       Increased code coverage
//*********************************************************/
@isTest
private class efTestSuiteRegistration{
// YKA Part 1 of split of the original method interactionUnitTest to deal with Deployment errors of SOQL limits     
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    static testMethod void interactionUnitTest1() {
        Boolean b;
        PageReference p;
        String s;
        DateTime d;

        //QTTestUtils.GlobalSetUp();
        
        efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
        
        System.runAs(efTestSuiteHelper.registrantUser){
            efRegistrationController ctl = new efRegistrationController();
            ctl.isTest = true;
            Test.startTest();               
            ctl.initialize();   
                    
            p = ctl.checkStatusForTransition();
            p = ctl.checkInitialState();
            p = ctl.checkState();
            p = ctl.checkStatus();
            p = ctl.goTraining();      
            p = ctl.goAttendeeInfo();
            p = ctl.goSummary();
            p = ctl.goNext();
            p = ctl.goBack();  
            p = ctl.cancelConfirm();
            p = ctl.saveConfirm();
            p = ctl.goSaveForLater();
            p = ctl.goFinal();
            b = ctl.getValidatedOnTrainingPage();
            System.assertEquals(false, b);
            
            b = ctl.getValidatedOnRegisterPage();
            System.assertEquals(true, b);
             
            b = ctl.getValidatedOnConfirmPage();
            System.assertEquals(false, b);
            
            ctl.resetValidation();
            s = ctl.getMessage();   
            b = ctl.getSelectedTrainingInvalidated();           
            
            efRegistrationValidator efRegVal = ctl.getRegValidator();  
            efRegVal.getIsValidRegistrationData();
            
            b = ctl.isValidTrainingData();
            System.assertEquals(true, b);
                  
            b = ctl.isValidRegisterData();                      
            p = ctl.validateFormData(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE);
            p = ctl.validateFormData(efAppNavigator.REGISTRATION_SUMMARY_PAGE);
            p = ctl.validateFormData(efAppNavigator.REGISTRATION_TRAINING_PAGE);
            
            p = ctl.validateConfirm();
            b = ctl.getCanConfirmRegistration();
            System.assertEquals(true, b);
            
            b = ctl.getInValidPaymentUrl();
            System.assertEquals(false, b);
            
            s = ctl.getEventBannerUrl();    
            s = ctl.getEventName();
            s = ctl.getRegistrationId();
                        
            efRegistrationWrapper efRegWrap = ctl.getAttendeeRegistration();                        
            efContactWrapper efContactWrap = ctl.getAttendeeContact();    
            efEventWrapper efEventWrap = ctl.getEvent();
            s = ctl.getAttendeeType();
            s = ctl.getRegStatus();
            b = ctl.isAttendeeType('Attendee');
            System.assertEquals(false, b);
                //
           /* efEventWrap.getRegistrationLabelSummaryInvoice();  
            efEventWrap.getRegistrationLabelSummaryInvoiceConf();
            efEventWrap.getRegistrationLabelSummaryGroupDiscounts();
            efEventWrap.getRegistrationLabelSummaryGroup5Plus();
            efEventWrap.getRegistrationLabelSummaryGroup10Plus();*/
                //
            b = ctl.getIsEmployee();
            System.assertEquals(false, b);
                        
            ctl.setCompanySize('10-100');
            s = ctl.getCompanySize();
            ctl.setIndustry('Financials');
            s = ctl.getIndustry();
            ctl.setBusinessName('A-Z Landscapes');
            s = ctl.getBusinessName();          
            
            efClassWrapper[] cw;
            ctl.setClassesByGroup(cw);
                            
            List<efProductWrapper> listProdWrap = ctl.getClasses();
            List<efClassWrapper> listClassWrap = ctl.getClassesByGroup();
              
            List<efProductWrapper> listSelectedProdWrap = ctl.getTrainingOfInterest();  
            b = ctl.getIsTrainingSelected();
            System.assertEquals(false, b);
            
            s = ctl.getSubTotalPaymentAmount();
            s = ctl.getTotalPaymentAmount();
            s = ctl.getSummaryPaymentAmount();
            b = ctl.getPaymentTermsAccepted();
            System.assertEquals(false, b);
            
            efBillingManager efBillMgr = ctl.getBillingManager();
            Double db = efBillMgr.getTotalRegistrationAmount(); 
            db = efBillMgr.getTotalClassesAmount(); 
            db = efBillMgr.getTotalDiscountAmount();    
            db = efBillMgr.getTotalAddOnAmount();   
            db = efBillMgr.getTotalPaymentAmount();
            db = efBillMgr.getSubTotalPaymentAmount();
            efBillMgr.generateBill();
            List<efBillingManager.BillItem> listPI = efBillMgr.getPaymentItems();
            
            ctl.regenerateBilling();  
            ctl.setPromoCode('XYZ');
            s = ctl.getPromoCode(); 
            b = ctl.getAnyPromoCodeAdded();
            s = ctl.getPromoMessage();
            List<efProductWrapper> listPromo = ctl.getPromoCodeList();
            List<efProductWrapper> listPromo2 = ctl.getPromoCodes();    
            p = ctl.updatePromoCodes();
            ctl.setPromoCodeExecuted(true);
            b = ctl.getPromoCodeExecuted();                 
            ctl.removePromoCode();   
            b = ctl.getShowTrainingForAttendee();
            System.assertEquals(true, b);           
            b = ctl.getShowPromoCode();
            System.assertEquals(true, b);           
            b = ctl.getShowPaymentPage();
            System.assertEquals(false, b);
                  
            s = ctl.getRegistrationProductName();
            System.assertNotEquals(null, s);
            Test.stopTest();    
        }
    } 
    

// YKA Part 2 of split of the original method interactionUnitTest to deal with Deployment errors of SOQL limits   
    static testMethod void interactionUnitTest2() {
        Boolean b;
        PageReference p;
        String s;
        DateTime d;
        
        //QTTestUtils.GlobalSetUp();
        efTestSuiteHelper.setup();
        Profile pUser = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pUser.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert u;
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
        User userForTest = u;
        //System.runAs(QTTestUtils.createMockUserForProfile('System Administrator')){
        System.runAs(userForTest){
         PriceBook2 pb = new PriceBook2();
        pb.Name = 'Unit Test Event Pricebook';
        pb.IsActive = true;
        pb.Description = 'Unit Test Event Pricebook';
        insert pb;    
        Events__c eNoReg = new Events__c();
         eNoReg.Name = 'Unit Test Event No Registration';
        eNoReg.isActive__c = true;      
        eNoReg.Event_Start_Date__c = Date.today().addDays(30);
        eNoReg.Event_End_Date__c = Date.today().addDays(35);
        eNoReg.Venue_Name__c = 'EIC';       
        eNoReg.Venue_Website__c = 'http://www.eic.com';
        eNoReg.Venue_City__c = 'Edinburgh';
        eNoReg.Website__c = 'http://www.eventforce.com';
        eNoReg.Contact_Email__c = 'event@force.com';
        eNoReg.Banner_Image_Document__c = 'testing';
        eNoReg.Thumbnail_Image_Document__c ='testing';
        eNoReg.Pricebook_Name__c = pb.Name;
        eNoReg.Opportunity_Owner__c = UserInfo.getUserId();      
        insert eNoReg;
        Account testResseller = TestQuoteUtil.createReseller();
        Contact testContact = TestQuoteUtil.createContact(testResseller .id);   
		//removed
		/*
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c companyQT = TestQuoteUtil.createQCompany(sub.id);
		*/
        efRegistrationController ctl = new efRegistrationController();
        Registration__c testReg = new Registration__c();
            testReg .Event__c = eNoReg .Id;
            testReg .Registrant__c = testContact .Id;
            insert testReg ;
			/*
        Account  testAcc = TestQuoteUtil.createAccount(companyQT.id);    
		*/
		Account  testAcc = QTTestUtils.createMockAccount('TestCompany', userForTest, true);
	        testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAcc;
            
            
            
            efRegistrationManager regManager = new efRegistrationManager(testContact.id, testReg .Id);
            ctl.isTest = true;
            Test.startTest();                           
            ctl.initialize();           
    
            //efOpportunityManager efOppMgr = ctl.getOpportunityManager();
            efOpportunityManager efOppMgr = new efOpportunityManager(regManager);
            
            efOppMgr.cancelOldOpportunityAndLineItems();            
            
           /* s = ctl.getRegistrationLabelTrainingHeader();
            s = ctl.getRegistrationLabelTrainingSubHeader();
            s = ctl.getRegistrationLabelSummaryFinancialSummary();
            s = ctl.getRegistrationLabelSummaryMessageTrainingInvalidated();    
            s = ctl.getRegistrationLabelSummaryTermsAndConditions();    
            s = ctl.getRegistrationLabelSummaryPaymentRedirect();
            s = ctl.getRegistrationLabelFinishError();
            s = ctl.getRegistrationLabelFinishGoEvent();
            s = ctl.getRegistrationLabelFinishPaymentConfirmed();
            s = ctl.getRegistrationLabelFinishGoAttendeePortal();*/
            efPickLists picklists = ctl.getPicklist();
            s = ctl.getFormInEdition();
            ctl.editForm();

            // Comment& : finish method.            
            ctl.setPaymentTermsAccepted(false);            
            p = ctl.goFinish();
            b = ctl.getChkTermsAccepted();
            System.assertEquals(true, b);
            
            ctl.setPaymentTermsAccepted(true);
      //      p = ctl.goFinish();
            b = ctl.getChkTermsAccepted();
         //   System.assertEquals(false, b);
               
          //  p = ctl.goAttendeePortal();  
           // System.assertNotEquals(null,p);
            Test.stopTest();                    
        }
    } 
    
    
    static testMethod void registrationAndContactWrapperUnitTest() {
        Boolean b;
        PageReference p;
        String s;
        DateTime d;

        //QTTestUtils.GlobalSetUp();
        
        efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
        
        System.runAs(efTestSuiteHelper.registrantUser){
            efRegistrationController ctl = new efRegistrationController();
            ctl.isTest = true;
            Test.startTest();                           
            ctl.initialize();           
            
            efRegistrationWrapper efRegWrap = ctl.getAttendeeRegistration();
            
            Contact c2 = efRegWrap.getContact();
            efRegWrap.setContact(c2);
            Registration__c r2 = efRegWrap.getRegistration();
            s = efRegWrap.getEmergencyInfo();
            s = efRegWrap.getAttendeeSubType();
            s = efRegWrap.getId();
            efRegWrap.setMobilePhone('23434543543');
            s = efRegWrap.getMobilePhone();         
            //efRegWrap.setTrackInterest('Development');
            s = efRegWrap.getTrackInterest();
            s = efRegWrap.getTrackInterestName();           
            efRegWrap.setWhyRegister('no interest');
            s = efRegWrap.getWhyRegister();         
            efRegWrap.setEmpSessionTopics('employee topics');
            s = efRegWrap.getEmpSessionTopics();            
            efRegWrap.setWantToLearn(new List<String>{'a','b'});
            List<String> listString = efRegWrap.getWantToLearn();                   
            s = efRegWrap.getDisplayableWantToLearn();
            
            efRegWrap.setSeparateNameOnBadge(true);
            b = efRegWrap.getSeparateNameOnBadge();
            System.assertEquals(true, b);
            
            efRegWrap.setEmergencyContact('Sophie Grigson');            
            s = efRegWrap.getEmergencyContact();                        
            efRegWrap.setEmergencyPhone('12134334');
            s = efRegWrap.getEmergencyPhone();          
            efRegWrap.setEmergencyRelationship('Colleague');
            s = efRegWrap.getEmergencyRelationship();
            
            efRegWrap.setBadgeFirstName('S');
            s = efRegWrap.getBadgeFirstName();
            efRegWrap.setBadgeLastName('G');
            s = efRegWrap.getBadgeLastName();
            
            s = efRegWrap.getBadgeNameToDisplay();
            s = efRegWrap.getEmergencyPhoneToDisplay();
            efRegWrap.setWizardPageSaved('1');
            s = efRegWrap.getWizardPageSaved();
            efRegWrap.setStatus('Saved for Later');
            s = efRegWrap.getStatus();
            
            efRegWrap.setSpecialRequirements('vegetarian');
            s = efRegWrap.getSpecialRequirements();         
            efRegWrap.setRequiresSpecialAssistance(true);
            b = efRegWrap.getRequiresSpecialAssistance();
            System.assertEquals(true, b);
            
            efRegWrap.setDepartment('R&D');
            s = efRegWrap.getDepartment();
            
            d = efRegWrap.getPaymentTimestamp();
            efRegWrap.setPaymentTimestamp(DateTime.now());
            efRegWrap.setPrimaryRole('Technology'); 
            s = efRegWrap.getPrimaryRole();
            
            efRegWrap.setCurrencyIsoCode('USD');
            s = efRegWrap.getCurrencyIsoCode();
            
            s = efRegWrap.getRegId();           
            s = efRegWrap.getRegType();
            efRegWrap.setRegType('Attendee');
            
            efRegWrap.setPromoCode('PCODE');
            s = efRegWrap.getPromoCode();
            s = efRegWrap.getName();
            
            efRegWrap.setExtendedStay(true);
            b = efRegWrap.getExtendedStay();
            System.assertEquals(true, b);
            
            efPicklists pl = new efPicklists();
            List<SelectOption> arrivals = pl.getEventExtendedStayArrivalList(efTestSuiteHelper.e.Extended_Stay_Start_Date__c, efTestSuiteHelper.e.Extended_Stay_End_Date__c);
            List<SelectOption> departures = pl.getEventExtendedStayDepartureList(efTestSuiteHelper.e.Extended_Stay_Start_Date__c, efTestSuiteHelper.e.Extended_Stay_End_Date__c);
                        
            efRegWrap.setExtendedStayArrival(arrivals.get(0).getValue());
            s = efRegWrap.getExtendedStayArrival();         
            efRegWrap.setExtendedStayDeparture(departures.get(departures.size()-1).getValue());
            s = efRegWrap.getExtendedStayDeparture();
            
            efRegWrap.setSpouseAddition(true);
            b = efRegWrap.getSpouseAddition();
            System.assertEquals(true, b);
            
            efRegWrap.setSpouseFirstName('Jo');
            s = efRegWrap.getSpouseFirstName();
            efRegWrap.setSpouseLastName('Smith');
            s = efRegWrap.getSpouseLastName();
                        
            efContactWrapper efContactWrap = ctl.getAttendeeContact();    
            c2 = efContactWrap.getContact();

            efContactWrap.setFirstName('S');
            s = efContactWrap.getFirstName();           

            efContactWrap.setTitle('Miss');
            s = efContactWrap.getTitle();       

            efContactWrap.setMailingStreet('street');           
            s = efContactWrap.getMailingStreet();
                        
            efContactWrap.setMailingCity('city');
            s = efContactWrap.getMailingCity();

            efContactWrap.setMailingCountry('UK');
            s = efContactWrap.getMailingCountry();
            
            efContactWrap.setMailingState('state');
            s = efContactWrap.getMailingState();
            
            efContactWrap.setMailingPostalCode('e99 9ee');
            s = efContactWrap.getMailingPostalCode();

            efContactWrap.setJobFunction('tech');
            s = efContactWrap.getJobFunction();
                        
            efContactWrap.setJobTitle('CTO');
            s = efContactWrap.getJobTitle();
            
            efContactWrap.setJobLevel('C');
            s = efContactWrap.getJobLevel();                       
            
            efContactWrap.setLastName('G');
            s = efContactWrap.getLastName();            
                        
            efContactWrap.setEmail('s@g.com');
            s = efContactWrap.getEmail();
            efContactWrap.setContactPhone('1212124444');
            s = efContactWrap.getContactPhone();            
            s = efContactWrap.getCardEmailHotel();          
            s = efContactWrap.getNameToDisplay();           
            s = efContactWrap.getCardEmail();
            
            Account a2 = efContactWrap.getAccount();                       
            s = efContactWrap.getAccountName();
                                    
            b = ctl.getRegValidator().getIsValidRegistrationData();
            System.assertEquals(true, b);
            
            Test.stopTest();                
        }
    }

    

    static testMethod void mailBuilderUnitTest(){
        Boolean b;
        PageReference p;
        String s;
        
        //QTTestUtils.GlobalSetUp();

        efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
        
        System.runAs(efTestSuiteHelper.registrantUser){
            Test.startTest();
            efRegistrationMailBuilder mb = new efRegistrationMailBuilder(); 
            
            mb.setRegistrant(efTestSuiteHelper.c);
            Contact c = mb.getRegistrant();

            mb.setRegistrationId(efTestSuiteHelper.r.Id);    
            s = mb.getRegistrationId();
            
            Registration__c r = mb.getRegistration();
    
            mb.setNAText('naText');    
            s = mb.getNAText();
            
            s = mb.getAttendeeFirstName();          
            s = mb.getAttendeeName();           
            s = mb.getAccountName();            
            s = mb.getAttendeeMail();           
            s = mb.getAttendeeMailingStreet();          
            s = mb.getAttendeeMailingCity();            
            s = mb.getAttendeeMailingState();           
            s = mb.getAttendeeMailingPostalCode();          
            s = mb.getAttendeeMailingCountry();         
            s = mb.getAttendeeType();                   
            s = mb.getEventName();          
            s = mb.getEventWebsite();           
            s = mb.getEventContactEmail();          
            s = mb.getEventFormattedStartDate();            
            s = mb.getVenueCity();          
            s = mb.getVenueCombined();          
            s = mb.getAccommodationString();            
            s = mb.getContactString();          
            s = mb.getTotalAmount();            
            s = mb.getFormattedDate();          
            s = mb.getPaymentString();      
            s = mb.getRecentPaymentString();        
            s = mb.getConfirmedTrainingItems();         
            s = mb.getRegistrationItems();
            System.assertNotEquals(null, s);
            Test.stopTest();
        }       
    }
    static testMethod void qtCoverelements()
    {
        efQTPaymentGateway.CapturePayment_element cpw = new efQTPaymentGateway.CapturePayment_element();
        efQTPaymentGateway.Payment_element pw = new efQTPaymentGateway.Payment_element();
        efQTPaymentGateway.GetPaymentStatus_element gpse = new efQTPaymentGateway.GetPaymentStatus_element();
        efQTPaymentGateway.PaymentResponse_element pre = new efQTPaymentGateway.PaymentResponse_element();
        efQTPaymentGateway.CaptureResult cr = new efQTPaymentGateway.CaptureResult();
        efQTPaymentGateway.CapturePaymentResponse_element cpre = new efQTPaymentGateway.CapturePaymentResponse_element();
        efQTPaymentGateway.GetPaymentURLResponse_element gpur = new efQTPaymentGateway.GetPaymentURLResponse_element();
        efQTPaymentGateway.GetPaymentStatusResponse_element gpsre = new efQTPaymentGateway.GetPaymentStatusResponse_element();
        efQTPaymentGateway.GetPaymentURL_element gpue = new efQTPaymentGateway.GetPaymentURL_element();
        efQTPaymentGateway.Order o = new efQTPaymentGateway.Order();
        
        
    }
    static testMethod void qtPaymentStubUnitTest() {
        
        QTTestUtils.GlobalSetUp();
        //Causing uncommited work exception
        //efTestSuiteHelper.setup();
        
        Boolean b;
        PageReference p;
        String s;
        
        test.startTest();
        CapturePayment();
        test.stopTest();
            
    }
    @future (callout=true)
    static void CapturePayment()
    {
        efQTPaymentGateway.ServiceSoap svc = new efQTPaymentGateway.ServiceSoap(); 
        efQTPaymentGateway.CaptureResult cr = svc.CapturePayment('OrderCode');
    }
    static testMethod void efOrderTester()
    {
        //QTTestUtils.GlobalSetUp();
        WebServiceMockDispatcher dispatcher = new WebServiceMockDispatcher();
        test.setMock(WebServiceMock.Class, dispatcher);
        efTestSuiteHelper.setup();
        Boolean b;
        PageReference p;
        String s;

        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);

        
        System.runAs(efTestSuiteHelper.registrantUser){
            efRegistrationController ctl = new efRegistrationController();
            ctl.isTest = true;  


            efQTPaymentGateway.Order o = new efQTPaymentGateway.Order();
            efQTPaymentGateway.PaymentResult pr = new efQTPaymentGateway.PaymentResult();
            efQTPaymentGateway.PaymentOrder po = new efQTPaymentGateway.PaymentOrder();

            efQTPaymentGateway.ServiceSoap svc = new efQTPaymentGateway.ServiceSoap(); 

            o.OrderCode = efTestSuiteHelper.r.Id;
            o.Description = efTestSuiteHelper.r.Name;              
            o.CurrencyCode = 'USD'; // Comment& : need to address this for multi-currency.        
            o.Vat = 0;
            o.TotalAmount = Double.valueOf(ctl.getTotalPaymentAmount());
            
            Contact registrant = efTestSuiteHelper.c;
            o.Email = registrant.Email;
            o.Company = ctl.getBusinessName();
            o.FirstName = registrant.FirstName;
            o.LastName = registrant.LastName;
            o.Street = registrant.mailingStreet;
            o.HouseNbr = registrant.mailingStreet;
            o.Zip = registrant.mailingPostalCode;
            o.City = registrant.mailingCity;
            o.Country = registrant.mailingCountry;
            o.Phone = registrant.Phone;        
            o.CaptureType = 'EventForce';
            o.QlikTechCompany = 'QlikTech UK Ltd';
                   
            //This test fails if efTestSuiteHelper.setup(); is not commented out and is dependant on it
            test.startTest();
            s = svc.GetPaymentURL(o);
            System.assertNotEquals(null, s);
            test.stopTest();
            
            
        }
        
    }

    static testMethod void efTestPaymentOrder()
    {
        QTTestUtils.GlobalSetUp();

        efQTPaymentGateway.PaymentResult pr = new efQTPaymentGateway.PaymentResult();
        efQTPaymentGateway.PaymentOrder po = new efQTPaymentGateway.PaymentOrder();
        efQTPaymentGateway.ServiceSoap svc = new efQTPaymentGateway.ServiceSoap(); 

        String s;

        test.startTest();
        pr = svc.Payment(po);
        System.assertNotEquals(null, pr);
             
        s = svc.GetPaymentStatus('OrderCode');
        System.assertNotEquals(null, s);
        test.stopTest();
    }

    
    static testMethod void productWrapperUnitTest() {
        Boolean b;
        PageReference p;
        String s;
        DateTime d;
        
        //QTTestUtils.GlobalSetUp();

        efTestSuiteHelper.setup();
        
        ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);
        
        System.runAs(efTestSuiteHelper.registrantUser){
            efRegistrationController ctl = new efRegistrationController();
            ctl.isTest = true;
            Test.startTest();
            
            efProductWrapper ew = new efProductWrapper(efTestSuiteHelper.tp);
            
            s = ew.getCounter();
            s = ew.getIndex();
            s = ew.getSerialNumber();       
            ew.setCounter(1);
            ID i = ew.getID();
            Product2 p2 = ew.getProduct();
            PricebookEntry[] peList = ew.getPricebookEntries();
            b = ew.getIsSelected();
            b = ew.getIsContestPromoCode();
            ew.setIsSelected(true);    
            b = ew.getIsClassFull();
            b = ew.getIsClassFullWarning();
            b = ew.getSameTimeAlert();
            ew.setSameTimeAlert(true);   
            s = ew.getCalculatedDiscountAmount();
            ew.setCalculatedDiscountAmount('100');
            s = ew.getFormatedDiscountAmount();
            d = ew.getSDate();
            d = ew.getEDate();    
            s = ew.getFullStartDate();    
            s = ew.getStartDate();   
            s = ew.getEndDate();    
            d = ew.getStartDateTime();
            d = ew.getEndDateTime();
            s = ew.getStartTime();
            s = ew.getEndTime();
            s = ew.getName();
            s = ew.getRoom();
            s = ew.getRole();
            s = ew.getDescription();
            s = ew.getProductCode();    
            s = ew.getClassDates();    
            s = ew.getClassTime();   
            s = ew.getClassTimeLower();    
            s = ew.getDateForClassGroup();
            s = ew.getFormatedDate();
            s = ew.getFormatedDateSingle();   
            s = ew.getFormatedDateNoText();    
            s = ew.getFormatedTime();    
            s = ew.getUnitPrice();    
            s = ew.getRelatedOppLnItemId();     
            ew.setRelatedOppLnItemId('oppLnItmId');
            s = ew.getRelatedOppLnItemStatus();
            ew.setRelatedOppLnItemStatus('oppLnItemStatus');    
            s = ew.getRelatedOppLnItemDesc();
            ew.setRelatedOppLnItemDesc('oppLnItemDesc');                
            s = ew.getProductFamily();
            System.assertNotEquals(null, s);
            
            Test.stopTest();
        }
    }
}