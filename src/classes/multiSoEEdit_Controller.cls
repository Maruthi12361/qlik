/******************************************************

	Class: multiSoEEdit_Controller
	
	Changelog:
		2013-04-19	CCE		Initial development. CR# 7922 - Rework of SoE to Multi-edit form
							https://eu1.salesforce.com/a0CD000000YFMVz. Controller class for
							multiSoEEdit.page
		
******************************************************/
public with sharing class multiSoEEdit_Controller {
    
    private ApexPages.StandardController Controller;
	private String opportunityid;
    List<Sequence_of_Event__c> l_SoEX = new List<Sequence_of_Event__c>();
    	
    public multiSoEEdit_Controller(ApexPages.StandardController stdController)
	{
		//Save the Opp Id so we can navigate back to the record when we do a Save or Cancel
		opportunityid = ApexPages.currentPage().getParameters().get('opportunityid');            
	}

	//Create a list of the current Sequence of Events records and add a new empty one to the list
	public List<Sequence_of_Event__c> getSequence_of_Events()
	{
		List<Sequence_of_Event__c> l_SoE = new List<Sequence_of_Event__c>();
    	l_SoE = [select Checkpoint__c, S_o_E_Date__c, Proposed_Event__c, Description__c, Cost__c, Responsibility__c, Event_Charge__c, Customer_Agreed__c from Sequence_of_Event__c where Opportunity_ID__c = :opportunityid ORDER BY S_o_E_Date__c];
        
        l_SoEX.clear();	//so we don't duplicate the list when we addAll()
        l_SoEX.addAll(l_SoE);
        Sequence_of_Event__c seq = new Sequence_of_Event__c(Opportunity_ID__c = opportunityid, Checkpoint__c = 'Not Applicable');
    	//Sequence_of_Event__c seq = new Sequence_of_Event__c(Opportunity_ID__c = opportunityid, S_o_E_Date__c = System.today(), Checkpoint__c = 'Not Applicable');
    	l_SoEX.add(seq);
	
        return l_SoEX;
	}
    
    public PageReference SaveSet() {
    	if (!DoTheSave()) return System.currentPageReference();	//if the Save failed we stay on the same page so we see the captured error messages
    	
    	PageReference pr = new PageReference('/');	
		if (opportunityid != null)
		{
			//TODO it would be nice to find out why this isn't working
			//string homeurl = '/' + opportunityid + '#' + opportunityid + '_00N20000001zLV7_target';
			//System.debug('multiSoEEdit: homeurl: ' + homeurl);
			//pr = new PageReference(homeurl);
			//System.debug('multiSoEEdit: pr: ' + pr);
			
			pr = new PageReference('/' + opportunityid);
		}   	
		pr.setRedirect(true);
		return pr;			
	}
	
	public PageReference CancelSet() {
    	PageReference pr = new PageReference('/');	
		if (opportunityid != null)
		{
			pr = new PageReference('/' + opportunityid);
		}
		pr.setRedirect(true);
		return pr;			
	}
	
	public PageReference QuickSaveSet() {    	
		DoTheSave();
		return System.currentPageReference();			
	}
	
	//Save any changes to the page. If no changes have been made to the record we added earlier then we remove it before we save.
	public boolean DoTheSave() {
		boolean errFlg = true;
    	try {
	    	System.debug('multiSoEEdit: l_SoEX[l_SoEX.size()-1] = ' + l_SoEX[l_SoEX.size()-1]);
			Sequence_of_Event__c seq = l_SoEX[l_SoEX.size()-1];
			if ((seq.Checkpoint__c == 'Not Applicable' || seq.Checkpoint__c == null) && seq.Proposed_Event__c == null && seq.Cost__c == null && seq.Description__c == null && seq.Responsibility__c == null && seq.S_o_E_Date__c == null)
			{
				Sequence_of_Event__c delSoEX = l_SoEX.remove(l_SoEX.size()-1);	//remove the unused record 
				System.debug('multiSoEEdit: delSoEX = ' + delSoEX);
			}		
			upsert l_SoEX;
		}
        catch(Exception e) {
            System.debug('ERROR with multiSoEEdit: ' + e);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR with multiSoEEdit: ' + e.getMessage());
        	ApexPages.addMessage(msg);
        	errFlg = false;
        }
        return errFlg;		
	}
	
}