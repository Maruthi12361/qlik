/*      * File        :  removeDuplicateAccount
        * Description :   This is One time batch, used to delete duplicate account license and product License
        * @author     :   Anurag Gupta
        * Date        :   24th Nov 2017
        * User story  :   QCW-4251    
        
    Change Log:
    2018-12-04  CTS     SCU-5 Removal of variables used in Inactive Triggers.
*/

global class removeDuplicateAccount implements schedulable{
    

    
    global void execute(SchedulableContext ctx) {
    Integer Lmt = 100;
  
  //public removeDuplicateAccount(integer Lmt){
  
    List<AggregateResult> ResultLic = new List<AggregateResult>();
    List<AggregateResult> Result = new List<AggregateResult>();

    List<Entitlement> lstCas = new List<Entitlement>();
    List<Entitlement> lstCas2 = new List<Entitlement>();
    List<Entitlement> RTypeUpdte = new List<Entitlement>();
    List<String> nnn = new List<String>();
    List<String> aaa = new List<String>();
    List<entitlement> eltCase = new List<entitlement>();

    //CTS SCU-5 Removal of Inactive Triggers
    //Semaphores.CaseUpdateSupportContact_HasRunAfterDelete = true;    // to avoid CaseUpdateSupportContact trigger to run
    //Semaphores.CaseUpdateSupportContact_HasRunAfterUpdate = true;

    List<Id> lstCas3 = new List<Id>();  
    
        system.debug(Lmt+'llllllllll');
        Result = [select name, count(id) from Entitlement group by name having count(id) = 2 order by count(id) desc limit: Lmt] ;
        for(AggregateResult arr:Result){ 
              system.debug('Aggregate Result....++++++++++++++++++++++++++++++++++++' + arr);              
              String nn=(String)arr.get('name'); 
              system.debug(nn+'oooooooooooo');
              nnn.add(nn);
         }
        system.debug(nnn+'checkkkkkkkk');
 
        lstCas = [select id,Name from Entitlement where Name =: nnn ];
        for(Entitlement EntL : lstCas){
             lstCas3.add(EntL.Id);    
         }
          
         system.debug(lstCas3+'3333333333'); 
        eltCase = [select id, name,account_license__r.id,Account_License__c,status,RecordType.Name,recordtypeid  , ( select id,Account_License__c,entitlement.id from cases),(select id, product_license__c,name from environments__r) from entitlement where id IN  : lstCas3 ORDER BY Name];
        
        String prevName ='';
        String EltPrevAccountLicense  = '';
        
        Id StdRecordType = Schema.SObjectType.Entitlement.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        Id ObsRecordType = Schema.SObjectType.Entitlement.getRecordTypeInfosByName().get('Obsolete').getRecordTypeId();
        List<Entitlement> StdRcd           = new List<Entitlement>();
        List<Entitlement> RTStndEnt  = new List<Entitlement>();
        List<Entitlement> EntAccUpdate     = new List<Entitlement>();
        List<Entitlement> ObsRcd           = new List<Entitlement>();
        List<Entitlement> DelProduct       = new List<Entitlement>();
        List<Environment__c> LstEnv        = new List<Environment__c>();
        List<Environment__c> EnvUpdate     = new List<Environment__c>();
        List<Entitlement> ListPrd           = new List<Entitlement>();
        List<Entitlement> FinalPrd           = new List<Entitlement>();
        Set<Entitlement> SetPrd            = new Set<Entitlement>();
        List<case> CaseUpdate              = new List<Case>();
        List<case> LstCase                 = new List<Case>();  
              
              String checkOne='', checkTwo='';
              String PrevAccntName = '';
              String prevEntitlement,UpdateListName,Application;
              List<Account_license__c> AccntList             = new List<Account_license__c>();
              List<Account_License__c> lstEnt                = new List<Account_License__c>();
              List<Account_license__c> AcntDel               = new List<Account_license__c>();
              List<Account_License__c> DelAccntLicense       = new List<Account_License__c>();
              List<Account_License__c> updateAccntLicense    = new List<Account_License__c>();
              List<Account_License__c> CheckAccnt            = new List<Account_License__c>();
              List<Account_License__c> AccntDel              = new List<Account_License__c>();  
              List<Account_License__c> AccntAgrList          = new List<Account_License__c>();
              List<Id> AccnList                              = new List<Id>();
              
              String Compound_Key, NetSuiteId ,License_Number_ExternalID; 
              String AccntName ,AccntId ,Final_Body, descrp ;
              String X3rd_Party_Software, Client_Version, Connector_Version , Customer_Patch_Version, Environment,Full_Version, Legacy_License_Key , Navision_Country, Navision_Customer_Number,O_S,Product, Product_Level, Product_Type;
              String  INT_NetSuite_Support_Level, Version,Product_Version, Publisher_Version, Server_Version;
              Boolean Dynamic_update , Invoiced, Lookup_License , PDF_Generation , Uncapped,Web_Parts, Workbench; 
              String Description, Area, issue, Navision_Account_Name,  Special_Edition;
              Date Subscription_End_Date , Subscription_Start_Date , Support_From , Support_To , Time_Limit , Upgrade_From , Upgrade_To ;  
              DateTime Last_Successful_LEF_update;
              String Navision_Sales_Person , Navision_User ;
              Id Account_c , Account_License_Group_c , Selling_Partner_c, Lookup_License_c , Opportunity_c , Support_Provided_By_c ;
              Double Support_Percentage, Document_CAL, Named_User_CAL, No_of_Distribution_Services , No_of_Documents, No_of_Seats , No_of_Servers_in_Cluster , Session_CAL , Status , Tokens , Usage_CAL ;
              

              Set<Entitlement> ProdLicense = new Set<entitlement> () ;   
              List<Entitlement> EntList =  new List<Entitlement>();
              Set<Entitlement> entUpdate   = new Set<entitlement> () ; 
              List<NS_Support_Contract_Item__c> nsItemUpdate = new List<NS_Support_Contract_Item__c>();
              List<NS_Support_Contract_Item__c> NsItemList = new List<NS_Support_Contract_Item__c>();
              List<NS_Support_Contract_Item__c> NsUpList = new List<NS_Support_Contract_Item__c>();
        
        for(entitlement elt1: eltCase){
            if(elt1.Name == prevName){
                for(Case cse : LstCase){
                    cse.EntitlementId = elt1.Id;
                    CaseUpdate.add(cse);
                    system.debug(CaseUpdate.size()+'aaaa'+CaseUpdate+'2222222222');
                }
                for(environment__c envr : LstEnv){
                    envr.product_license__c = elt1.Id;
                    EnvUpdate.add(envr);
                    system.debug(EnvUpdate.size()+'vvvv'+EnvUpdate+'44444444444');
                }
                LstEnv.clear();
                LstCase.clear();
                if(elt1.RecordType.Name == 'Obsolete'){
                    elt1.recordTypeId = StdRecordType;
                    StdRcd.add(elt1);                // make it standard
                }
                if((elt1.Account_License__c == Null)&&(EltPrevAccountLicense != Null)){
                    elt1.Account_License__c = EltPrevAccountLicense;
                    EntAccUpdate.add(elt1); 
                }
                else{
                    if((elt1.Account_License__c ==NULL)&&(elt1.Account_License__c ==NULL)){
                        elt1.RecordTypeId = StdRecordType;
                        ObsRcd.add(elt1); // make it standrd and obsolete again 
                    }
                }
            }
            else{
                for(Case c: elt1.Cases){
                    LstCase.add(c);
                }
                for(Environment__c env : elt1.environments__r){
                    LstEnv.add(env);   
                }
                   DelProduct.add(elt1);
            }
        
        prevName = elt1.name;
        EltPrevAccountLicense =  elt1.Account_license__c;
        }
        if(StdRcd !=null){
            database.update(StdRcd,false) ;    // make sure it is updated completly, record type updated
        }
        if(EntAccUpdate  !=null){
            database.update(EntAccUpdate  ,false);
        }
        if(ObsRcd !=null){
            database.update (ObsRcd,false);            
        }
        for(Entitlement ect: ObsRcd){    //change Record Type to Obsolete after cases reparented
            ect.RecordTypeId = ObsRecordType ;
            RTStndEnt.add(ect);
        }
         String errDetails = '';
         String FailedCaseId = '';
         String SuccCase = '';
         String ErrCase= '';
         List<Id> successCAse = new List<Id>();
         List<Id> FailedCase = new List<Id>();
        List<Database.SaveResult> lsr = new List<Database.SaveResult>();
         if(CaseUpdate != null){
             lsr =  database.update(CaseUpdate,false);
             for (Database.SaveResult sr : lsr ) {
                    if (sr.isSuccess()) {
                        SuccCase =SuccCase+ sr.getId();
                    }
                    else
                        for(Database.Error err : sr.getErrors()) {
                        ErrCase = ErrCase + sr.getId(); 
                     }
             }
             
         }
       
        if(EnvUpdate !=null){
            database.update(EnvUpdate,false);
        }
        if(RTStndEnt !=null){
            database.update(RTStndEnt,false);     //   Update it back to Obsolete
        }
        system.debug(DelProduct+'DelProducttttttttttt');
        List<String> NameList = new List<String>();
        List<entitlement> DelListName = new List<entitlement>();
        DelListName =[select id,name from entitlement where Id IN: DelProduct];
        
        for(entitlement een: DelProduct){
            NameList.add(een.Name);
        }
            system.debug(NameList+'NameListttttttttttt');
         
        // Mail list of 'to be Deleted' Product License    
        String details ='';
        String errDetailserr = '';
        Database.Delete(DelProduct,false); 

       
        // Code for Account License Deletion Start
        
        // get List of Account License as per Name of product license (not deleted)
        ResultLic = [select name, count(id) from account_license__c group by name having count(id) = 2  AND Name IN:NameList order by name];
           
           for(AggregateResult arr: ResultLic){ 
              system.debug('Aggregate Result-----------'+arr);              
              String nn=(String)arr.get('name'); 
              system.debug(nn+'hhhhhhh');
              aaa.add(nn);
              system.debug(aaa+'ffffffffffffffff');
         }
         system.debug(aaa+'nnnnnnnnnnnn');
 
        AccntAgrList = [select id,Name from Account_license__c where Name =: aaa order by name ];
        for(Account_License__c Acc : AccntAgrList){
             AccnList.add(Acc.Id);  
             system.debug(Acc.Name+'Licensceeeeeeeeee');  
         }
       
       System.debug(AccnList+'Account License to be worked uponnnnnnnnnnnnnnnnnnn');
       
       
         lstEnt= [select id,name, X3rd_Party_Software__c,Account__c,Account_License_Group__c,Application__c,Area__c,Client_Version__c,Compound_Key__c,Connector_Version__c,
                    Selling_Partner__c,Customer_Patch_Version__c,Description__c,Document_CAL__c,DSE__c,Dynamic_update__c,
                    Environment__c,Full_Version__c,INT_NetSuite_InternalID__c,Invoiced__c,Issue__c,Last_Successful_LEF_update__c,
                    Legacy_License_Key__c,License_Number_ExternalID__c,License_Responsible_Partner__c,Lookup_License__c,Named_User_CAL__c,Navision_Account_Name__c,
                    Navision_Country__c,Navision_Customer_Number__c,Navision_Sales_Person__c,Navision_User__c,No_of_Distribution_Services__c,No_of_Documents__c,
                    No_of_Seats__c,No_of_Servers_in_Cluster__c,O_S__c,Opportunity__c,PDF_Generation__c,Product__c,Product_Level__c,Product_Type__c,
                    Product_Version__c,Publisher_Version__c,Server_Version__c,Session_CAL__c,Special_Edition__c,Status__c,
                    Subscription_End_Date__c,Subscription_Start_Date__c,Support_From__c,INT_NetSuite_Support_Level__c,Support_Level__c,Support_Office__c,
                    Support_Percentage__c,Support_Provided_By__c,Support_To__c,Time_Limit__c,Tokens__c,Uncapped__c,Upgrade_From__c,
                    Upgrade_To__c,Usage_CAL__c,Version__c,Web_Parts__c,Workbench__c, 
                    ( select id,name,Account_License__c from entitlements__r),(select id,Contract_Item_Account_License__c,name from NS_Support_Contract_Items1__r) from account_license__c where Id IN  : AccnList ORDER BY NAME]; 
                     
         system.debug(lstEnt+'AAAAAAAAAAAAA'+lstEnt.size());
         for(Account_License__c acnt: lstEnt){
         system.debug(acnt.name+'RRRRRRRRR');
                    if(acnt.Name == PrevAccntName){
                       system.debug(acnt.name+'SSSSSSSSS'+PrevAccntName);
                                if(acnt.INT_NetSuite_InternalID__c==null){ acnt.INT_NetSuite_InternalID__c = NetSuiteId;}
                                if(acnt.Description__c ==null){ acnt.Description__c = descrp;}else {acnt.Description__c = acnt.Description__c+ '\r\n'+ descrp; if(acnt.Description__c.Length()>250){ acnt.Description__c = acnt.Description__c.substring(0,250);}}
                                if(acnt.area__c ==null){acnt.area__c =area;} else{acnt.area__c=acnt.Area__c+ '\r\n'+area; if(acnt.area__c.length()>250){acnt.area__c=acnt.area__c.subString(0,250);}}
                                if(acnt.Navision_Account_Name__c ==null){acnt.Navision_Account_Name__c = Navision_Account_Name;} else{acnt.Navision_Account_Name__c = acnt.Navision_Account_Name__c +'\r\n'+Navision_Account_Name; if(acnt.Navision_Account_Name__c.length()>125){acnt.Navision_Account_Name__c = acnt.Navision_Account_Name__c.subString(0,125);}}
                                if(acnt.Issue__c ==null){acnt.Issue__c = Issue;}else{acnt.Issue__c = acnt.Issue__c+'\r\n'+Issue; if(acnt.Issue__c.length()>250){acnt.Issue__c = acnt.Issue__c.subString(0,250);}}
                                if(acnt.Special_Edition__c==null){acnt.Special_Edition__c = Special_Edition;}else{acnt.Special_Edition__c=Special_Edition +'\r\n'+Special_Edition; if(acnt.Special_Edition__c.length()>250){acnt.Special_Edition__c.subString(0,250);}}
                                
                                
                                if(acnt.X3rd_Party_Software__c ==null){acnt.X3rd_Party_Software__c = X3rd_Party_Software;}
                                if(acnt.Application__c ==null){acnt.Application__c = Application ;}
                                if(acnt.Client_Version__c ==null){acnt.Client_Version__c= Client_Version;}
                                if(acnt.Connector_Version__c ==null){acnt.Connector_Version__c = Connector_Version ; }
                                if(acnt.Customer_Patch_Version__c  ==null){acnt.Customer_Patch_Version__c =Customer_Patch_Version; }
                                if(acnt.Environment__c ==null){acnt.Environment__c = Environment ;}
                                if(acnt.Full_Version__c ==null){acnt.Full_Version__c = Full_Version;}
                                if(acnt.Legacy_License_Key__c ==null){acnt.Legacy_License_Key__c  =Legacy_License_Key;}
                                if(acnt.Navision_Country__c ==null){acnt.Navision_Country__c = Navision_Country;}
                                if(acnt.Navision_Customer_Number__c == null){acnt.Navision_Customer_Number__c = Navision_Customer_Number ;}
                                if(acnt.O_S__c ==null){acnt.O_S__c = O_S; }
                                if(acnt.Product__c ==null){acnt.Product__c =Product;}
                                if(acnt.Product_Level__c ==null){acnt.Product_Level__c = Product_Level ;}
                                if(acnt.Product_Type__c  ==null){acnt.Product_Type__c  = Product_Type  ;}
                                if(acnt.Product_Version__c==null){acnt.Product_Version__c = Product_Version;}
                                if(acnt.Publisher_Version__c ==null){acnt.Publisher_Version__c = Publisher_Version;}
                                if(acnt.Server_Version__c ==null){acnt.Server_Version__c=Server_Version;}
                                if(acnt.Support_Percentage__c ==null){acnt.Support_Percentage__c = Support_Percentage;}
                                if(acnt.Version__c ==null){acnt.Version__c =Version;}
                                
                                
                                // for Boolean fields
                                
                                if(acnt.Dynamic_update__c == null){acnt.Dynamic_update__c = Dynamic_update ; }
                                if(acnt.Invoiced__c == null){acnt.Invoiced__c =Invoiced ; }
                                if(acnt.Lookup_License__c == null){acnt.Lookup_License__c =Lookup_License ; }
                                if(acnt.PDF_Generation__c == null){acnt.PDF_Generation__c =PDF_Generation ; }
                                if(acnt.Uncapped__c == null){acnt.Uncapped__c = Uncapped ;}
                                if(acnt.Web_Parts__c == null) {acnt.Web_Parts__c = Web_Parts ; }
                                if(acnt.Workbench__c == null) {acnt.Workbench__c = Workbench ; }
                                
                                // for Date fields
                                if(acnt.Dynamic_update__c == null){acnt.Dynamic_update__c =Dynamic_update ; }
                                if(acnt.Subscription_End_Date__c == null){acnt.Subscription_End_Date__c = Subscription_End_Date ; }
                                if(acnt.Subscription_Start_Date__c == null){acnt.Subscription_Start_Date__c  = Subscription_Start_Date ; }
                                if(acnt.Support_From__c == null) {acnt.Support_From__c =Support_From ; }
                                if(acnt.Support_To__c ==null){acnt.Support_To__c =Support_To ; }
                                if(acnt.Time_Limit__C == null){acnt.Time_Limit__c  = Time_Limit ; }
                                if(acnt.Upgrade_From__c == null){acnt.Upgrade_From__c  = Upgrade_From ; }
                                if(acnt.Upgrade_To__c == null) {acnt.Upgrade_To__c = Upgrade_To ; }
                                if(acnt.Navision_Sales_Person__c == null){acnt.Navision_Sales_Person__c = Navision_Sales_Person ; }
                                if(acnt.Navision_User__c == null){acnt.Navision_User__c = Navision_User ; } 
                                // for LoopUP Relationship
                                if(acnt.Last_Successful_LEF_update__c == null){acnt.Last_Successful_LEF_update__c = Last_Successful_LEF_update ; }
                                if(acnt.Account__c == null) {acnt.Account__c = Account_c ; }
                                if(acnt.Account_License_Group__c == null){acnt.Account_License_Group__c = Account_License_Group_c;}
                                if(acnt.Selling_Partner__c ==null){acnt.Selling_Partner__c = Selling_Partner_c ; }
                                if(acnt.Opportunity__c ==null ){acnt.Opportunity__c = Opportunity_c; }
                                if(acnt.Support_Provided_By__c ==null){acnt.Support_Provided_By__c =Support_Provided_By_c ;} 
                               
                                // for number(18,0) fields
                                if(acnt.Document_CAL__c ==null){acnt.Document_CAL__c = Document_CAL;}
                                if(acnt.Named_User_CAL__c == null){acnt.Named_User_CAL__c = Named_User_CAL;}
                                if(acnt.No_of_Distribution_Services__c == null){acnt.No_of_Distribution_Services__c = No_of_Distribution_Services;}
                                if(acnt.No_of_Documents__c == null){acnt.No_of_Documents__c = No_of_Documents;}        
                                if(acnt.No_of_Seats__c == null){acnt.No_of_Seats__c = No_of_Seats;}
                                if(acnt.No_of_Servers_in_Cluster__c == null){acnt.No_of_Servers_in_Cluster__c = No_of_Servers_in_Cluster;}
                                if(acnt.Session_CAL__c == null){acnt.Session_CAL__c = Session_CAL;}
                              //  if(acnt.Status__c == null){acnt.Status__c = Status;}
                                acnt.status__c = 0;
                                
                                if(acnt.Tokens__c == null){acnt.Tokens__c = Tokens;}
                                if(acnt.Usage_CAL__c == null){acnt.Usage_CAL__c = Usage_CAL;}
                                
                                // External Id
                                if(acnt.Compound_Key__c ==null){acnt.Compound_Key__c =Compound_Key ;}
                                if(acnt.License_Number_ExternalID__c==null){acnt.License_Number_ExternalID__c = License_Number_ExternalID;}
                                        
                                
                                        
                                 // Move Product License
                                 for(entitlement p : ProdLicense){
                                     p.Account_License__c  = acnt.Id;
                                     entUpdate.add(p);
                                     
                                 }
                                 // Move Support Contract Item
                                 
                                for(NS_Support_Contract_Item__c NsItem: NsItemList){ 
                                    NsItem.Contract_Item_Account_License__c = acnt.Id;
                                    nsItemUpdate.add(NsItem);
                                    
                                 }    
                                 checkTwo = acnt.name;
                                 AccntList.add(acnt);
                    }
                    else{
                         
                        if((CheckOne != CheckTwo)&&(CheckOne!=null)){
                            AcntDel.remove(AcntDel.size() - 1);
                        }
                        CheckOne = acnt.Name;
                        AcntDel.add(acnt);
                        system.debug(AcntDel+'AcntttttttttttttDel'+AcntDel.size());
                        
                    }
                    
                    //String Fields
                    PrevAccntName = acnt.name;
                    NetSuiteId =acnt.INT_NetSuite_InternalID__c;
                    Descrp = acnt.Description__c;
                    area = acnt.area__c;
                    issue = acnt.issue__c;
                    Navision_Account_Name = acnt.Navision_Account_Name__c;
                    Special_Edition = acnt.Special_Edition__c;
                    
                    X3rd_Party_Software = acnt.X3rd_Party_Software__c;
                    Application = acnt.Application__c;
                    Client_Version =acnt.Client_Version__c;
                    Connector_Version = acnt.Connector_Version__c;
                    Customer_Patch_Version = acnt.Customer_Patch_Version__c;
                    Environment = acnt.Environment__c;
                    Full_Version = acnt.Full_Version__c;
                    Legacy_License_Key =acnt.Legacy_License_Key__c;
                    Navision_Country=acnt.Navision_Country__c;
                    Navision_Customer_Number =acnt.Navision_Customer_Number__c;
                    O_S = acnt.O_S__c;
                    Product = acnt.Product__c;
                    Product_Level = acnt.Product_Level__c;  
                    Product_Type  = acnt.Product_Type__c;
                    Product_Version =acnt.Product_Version__c;
                    Publisher_Version =acnt.Publisher_Version__c;
                    Server_Version =acnt.Server_Version__c;
                    Support_Percentage =acnt.Support_Percentage__c;
                    Version =acnt.Version__c;
                    
                    //Boolean Fields
                    Dynamic_update = acnt.Dynamic_update__c;
                    Invoiced = acnt.Invoiced__c;
                    Lookup_License =acnt.Lookup_License__c;
                    PDF_Generation =acnt.PDF_Generation__c;
                    Uncapped = acnt.Uncapped__c;
                    Web_Parts = acnt.Web_Parts__c;
                    Workbench =acnt.Workbench__c;
                    
                    //Date Fields
                    Subscription_End_Date = acnt.Subscription_End_Date__c;
                    Subscription_Start_Date = acnt.Subscription_Start_Date__c;
                    Support_From = acnt.Support_From__c;
                    Support_To = acnt.Support_To__c;
                    Time_Limit = acnt.Time_Limit__c;
                    Upgrade_From =acnt.Upgrade_From__c;
                    Upgrade_To =acnt.Upgrade_To__c;
                                         
                    Last_Successful_LEF_update = acnt.Last_Successful_LEF_update__c; 
                    
                    Navision_User = acnt.Navision_User__c;                     
                    Navision_Sales_Person =acnt.Navision_Sales_Person__c; 
                    
                    //LookUP
                    Account_c = acnt.Account__c; 
                    Account_License_Group_c = acnt.Account_License_Group__c;
                    Selling_Partner_c  = acnt.Selling_Partner__c;
                    Opportunity_c = acnt.Opportunity__c;
                    Support_Provided_By_c =  acnt.Support_Provided_By__c;
                    
                    //Number(18,0)fields
                    Document_CAL = acnt.Document_CAL__c;
                    Named_User_CAL = acnt.Named_User_CAL__c;
                    No_of_Distribution_Services = acnt.No_of_Distribution_Services__c;
                    No_of_Documents = acnt.No_of_Documents__c;
                    No_of_Seats = acnt.No_of_Seats__c;
                    No_of_Servers_in_Cluster = acnt.No_of_Servers_in_Cluster__c;
                    Session_CAL = acnt.Session_CAL__c;
                    Status = acnt.Status__c;
                    Tokens = acnt.Tokens__c;
                    Usage_CAL = acnt.Usage_CAL__c;
                    
                    // External Field Except NetSuite Internal Id
                    Compound_Key = acnt.Compound_Key__c;
                    License_Number_ExternalID = acnt.License_Number_ExternalID__c;
                    
                    NsItemList.clear();
                    ProdLicense.clear();
                                         
                    for(entitlement e : acnt.entitlements__r){
                        ProdLicense.add(e);
                    }
                    for(NS_Support_Contract_Item__c NsItem: acnt.NS_Support_Contract_Items1__r){
                        NsItemList.add(NsItem);
                    }
                    system.debug(ProdLicense+'EEEEEEEE');
                    // copy all other fields data into variable
                    
                }

             system.debug(AccntList+'FFFFFFFFFFFFFFF');
             system.debug(entUpdate+'DDDDDDDDDDDDDDD');
             if(AccntList != null){
                 database.update(AccntList,false);
             }
             for(Entitlement en : entUpdate){
                 EntList.add(en);
             }
             if(EntList != null){
                 database.update(EntList,false) ;
             }
             system.debug(nsItemUpdate +'eeeeeeeeeeee');
             
             for(NS_Support_Contract_Item__c ns : nsItemUpdate){
                NsUpList.add(ns);
            }

            system.debug(NsUpList+'EEEEEEEEEEEEEEEEE');
            
            if(NsUpList !=null){
                database.update(NsUpList,false);
            }
             system.debug(AcntDel+'AAAAAAAAAAa');
             for(Account_License__c  acl: AcntDel){
                 details = details + acl.Id+'&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        
                 details = details + acl.INT_NetSuite_InternalID__c+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                 details = details + acl.Name +'<br/>';
             }
             
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> sendTo = new List<String>();
            SendTo.add('Anurag.Gupta@qlik.com');
       //     SendTo.add('Vaniambadi.Venkatesan@qlik.com');
       //     SendTo.add('Vishal.Doijode@qlik.com');    
            mail.setToAddresses(sendTo);
            mail.setSubject('Account License to be Deleted');
            String body = 'Hi All,'+ '<br/>' + 'Below is the list of Account license that should be deleted'+ '<br/>';
            body =body+ 'Account License ID '+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + 'NetSuite ID' +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + 'Account License Name'+'<br/>'+'<br/>';
            body= body+ details+'Success'+SuccCase+'Error'+ErrCase  ;
            mail.setHtmlBody(body);
            mails.add(mail);
            Messaging.sendEmail(mails);
            system.debug(AcntDel+'1111111111');
            
            if(AcntDel != null){
                database.delete(AcntDel,false);
            }
    
        }
  }