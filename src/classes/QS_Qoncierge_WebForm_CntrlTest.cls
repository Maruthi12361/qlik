/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@IsTest
private class QS_Qoncierge_WebForm_CntrlTest {

    static testMethod void unitTestQonciergeForm() {
        String masterRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
                .get('QlikTech Master Support Record Type').getRecordTypeId();
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Temp Qoncierge', qtc);
        insert testAccount;
        //Create Account
        Account testAccountLund = TestDataFactory.createAccount('Temp - Support Office Lund', qtc);
        insert testAccountLund;
        //Create Account
        Account testAccountSydney = TestDataFactory.createAccount('Temp - Support Office Sydney', qtc);
        insert testAccountSydney;

        Account testAccountRaleigh = TestDataFactory.createAccount('Temp - Support Office Raleigh', qtc);
        insert testAccountRaleigh;

        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_Thomas', 'test_Jones', 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;

        //EmailTemplate e = new EmailTemplate (developerName = 'Qoncierge_Webform_Received', TemplateType= 'Text', Name = 'Qoncierge_Webform_Received'); // plus any other fields that you want to set
        //insert e;

        Qoncierge_Support_Email__c supportEmail = new Qoncierge_Support_Email__c(Name = 'Valid', Email__c = 'testqoncierge@testqlik.com');
        insert supportEmail;

        // Create Different Categories for 'QS_CaseWizard' Page 
        List<Category_Lookup__c> categoryLookupList = new List<Category_Lookup__c>();
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level', 'Third Category Level Type', true, 'Please email urgently', 'question1', 'question2', 'question3', 'question4', 'question5', '000123221', null, true, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Second Category Level', 'Fourth Category Level Type', true, 'Please call us immediately', 'Steps to reproduce:', 'Number of users affected:', null, null, null, '000123221', '1', true, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'First Category Level', 'Fifth Category Level', null, false, 'channel', 'question1', 'question2', 'question3', 'question4', 'question5', '000123221', null, true, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'Sixth Category Level', 'Nineth Category Level', null, true, 'channel', 'question1', 'question2', 'question3', 'question4', null, '000123221', null, true, 'QlikTech Master Support Record Type', 'Systems Queue'));
        categoryLookupList.add(TestDataFactory.createCategoryLookups('attachmentUpload', 'Seventh Category Level', 'Tenth Category Level', null, false, 'channel', 'question1', 'question2', null, null, null, '000123221', null, false, 'QlikTech Master Support Record Type', 'Systems Queue'));

        List<QS_Qoncierge_Country_License_Support__c> countrySupportList = new List<QS_Qoncierge_Country_License_Support__c>();
        countrySupportList.add(TestDataFactory.createCountryLicenseSupport('Afghanistan', testAccountLund.Id));
        countrySupportList.add(TestDataFactory.createCountryLicenseSupport('Argentina', testAccountRaleigh.Id));
        countrySupportList.add(TestDataFactory.createCountryLicenseSupport('Australia', testAccountSydney.Id));
        countrySupportList.add(TestDataFactory.createCountryLicenseSupport('United Kingdom', testAccountLund.Id));
        countrySupportList.add(TestDataFactory.createCountryLicenseSupport('Chile', testAccountRaleigh.Id));
        countrySupportList.add(TestDataFactory.createCountryLicenseSupport('Cambodia', testAccountSydney.Id));
        insert countrySupportList;

        if (categoryLookupList != null && !categoryLookupList.isEmpty()) {
            insert categoryLookupList;
        }

        // Running as the current user
        QS_CaseWizard_Controller caseWizard = new QS_CaseWizard_Controller();
        caseWizard.selectedFilter1 = 'First Category Level';
        caseWizard.filterCategoryLookupList();
        caseWizard.selectedFilter2 = 'Second Category Level';
        caseWizard.filterCategoryLookup2List();

        caseWizard.selectedFilter3 = 'Third Category Level Type';
        caseWizard.filterCategoryLookup3List();
        caseWizard.nextCaseDetail();
        System.assertNotEquals(caseWizard.caseObj.Severity__c, null);
        caseWizard.caseObj.Subject = 'Test Subject';
        caseWizard.caseObj.Description = 'Test Description';
        caseWizard.nextCaseDetail();

        // test to retrieve the created contact and create a case on this and send notifications
        QS_Qoncierge_WebForm_Cntrl webformController = new QS_Qoncierge_WebForm_Cntrl();
        webformController.contactTempObj = testContact;
        webformController.caseWizardRef = caseWizard;
        webformController.contactCompany = 'Test Company';
        webformController.contactAccountUsername = 'Test Company Account';
        webformController.tempCaseObj.License_No__c = 'Test_LI_Test';
        webformController.submitDetails();
        webformController.populateContactFields();
        System.assertNotEquals(webformController.newContactChat, null);
        System.assertNotEquals(webformController.caseWizardRef.isQonciergeFailure, false);
        System.assertNotEquals(webformController.caseWizardRef.showException, false);
        System.assertEquals(webformController.caseWizardRef.caseObj.Id, webformController.tempCaseObj.Id);


        Test.startTest();
        //test to create a new contact when no contact with the given email address is found and then create a case on this and send notifications
        webformController = new QS_Qoncierge_WebForm_Cntrl();
        webformController.caseWizardRef = caseWizard;

        webformController.contactTempObj.FirstName = 'firstName';
        webformController.contactTempObj.LastName = 'lastName';
        webformController.contactCompany = 'Test Company';
        webformController.contactTempObj.Contact_Country__c = 'Afghanistan';
        webformController.populateCountrySupport();
        System.assert(webformController.qonciergeCountrySupport != null
                && webformController.qonciergeCountrySupport.Name == 'Afghanistan'
                && webformController.qonciergeCountrySupport.Temp_Account__c == testAccountLund.Id);

        webformController.contactTempObj.Email = 'testfirstNamelastName@testemail.com';
        webformController.contactTempObj.Phone = '+44 09808089090';
        webformController.contactAccountUsername = 'Test Company Account';
        webformController.tempCaseObj.License_No__c = 'Test_LI_Test';

        // testing for few exceptions
        webformController.submitDetails();
        System.assertNotEquals(webformController.caseWizardRef.isQonciergeFailure, false);
        System.assertNotEquals(webformController.caseWizardRef.showException, false);
        System.assertEquals(webformController.caseWizardRef.caseObj.Id, webformController.tempCaseObj.Id);

        webformController = new QS_Qoncierge_WebForm_Cntrl();

        webformController.contactTempObj.FirstName = null;
        webformController.contactTempObj.LastName = null;
        webformController.caseWizardRef = caseWizard;
        webformController.contactCompany = 'Test Company';
        webformController.contactTempObj.Contact_Country__c = 'USA';
        webformController.populateCountrySupport();
        System.assert(webformController.qonciergeCountrySupport == null);

        webformController.contactTempObj.Email = 'testfirstNameNew@testemailnew.com';
        webformController.contactTempObj.Phone = '+44 09808089090';
        webformController.contactAccountUsername = 'Test Company Account';
        webformController.tempCaseObj.License_No__c = 'Test_LI_Test';

        webformController.submitDetails();
        System.assertNotEquals(webformController.caseWizardRef.isQonciergeFailure, false);
        System.assertNotEquals(webformController.caseWizardRef.showException, false);
        System.assertEquals(webformController.caseWizardRef.caseObj.Id, webformController.tempCaseObj.Id);

        caseWizard.caseObj.RecordTypeId = masterRecordTypeId;
        webformController.contactCompany = 'test';
        webformController.contactTempObj.Contact_Country__c = 'Afghanistan';
        webformController.contactTempObj.FirstName = 'test';
        webformController.contactTempObj.LastName = 'test';
        webformController.submitDetails();

        webformController.contactCompany = '';
        webformController.submitDetails();
        System.assert(ApexPages.hasMessages());

        webformController.contactTempObj = null;
        webformController.populateCountrySupport();
        System.assertEquals(webformController.qonciergeCountrySupport, null);

        Test.stopTest();

    }

}