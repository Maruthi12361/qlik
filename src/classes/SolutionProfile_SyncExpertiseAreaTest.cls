/*************************************************************************
*
* Test class: SolutionProfile_SyncExpertiseAreaTest
*
* Tests the SolutionProfile_SyncExpertiseArea trigger
* 
* 2014-11-13 Fluido: Test class created (CR# 17632)
* 2014-11-19 Fluido: Added new test method testSyncLockedOpp() (CR# 17632)
* 2016-01-25 TJG fix the error of FIELD_CUSTOM_VALIDATION_EXCEPTION, Account can be owned only by Master Reseller Partners.
*************************************************************************/

@isTest
private class SolutionProfile_SyncExpertiseAreaTest {
/*
    static testMethod void testSyncInsert() {
        Test.startTest();
        User tstUser = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(tstUser) {
            // Prepare test data
            RecordType oppRt = [
                SELECT Id, Name FROM RecordType WHERE Name LIKE 'Qlikbuy % Standard'
            ];

            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
            Semaphores.Opportunity_ManageForecastProductsIsInsert=true;
            QTTestUtils.GlobalSetUp();

            Opportunity tstOpp = QTTestUtils.createMockOpportunity(
                'sShortDescription', 'sName', 'Existing Customer', // sType
                'Direct', // sRevenueType
                'Goal Identified', // sStage
                ''+oppRt.Id, 'GBP', // sCurrencyIsoCode
                tstUser, true // doInsert
            );
            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];

            // Start testing
            System.assertEquals(null, tstOpp.Expertise_Area__c,
                'Opportunity\'s Expertise Area should be null on a new Opportunity!'
            );

            // 01. Solution Profile insert
            String eaStr = 'Healthcare';
            String eaStrFull = eaStr;
            Solution_Profiles__c tstSp01 = new Solution_Profiles__c(
                Name='spTest01', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            insert tstSp01;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile insert did not set expertise area on Opp correctly!'
            );

            // 02. Solution Profile insert (another expertise area)
            eaStr = 'Communications';
            eaStrFull = 'Communications,Healthcare';
            Solution_Profiles__c tstSp02 = new Solution_Profiles__c(
                Name='spTest02', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            insert tstSp02;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile insert did not update expertise area on Opp correctly!'
            );

            // 03. Solution Profile double insert
            List<Solution_Profiles__c> spList = new List<Solution_Profiles__c>();
            eaStr = 'Public Sector';
            eaStrFull = 'Communications,Healthcare,Public Sector';
            Solution_Profiles__c tstSp03 = new Solution_Profiles__c(
                Name='spTest03', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp03);
            eaStr = 'Life Sciences';
            eaStrFull = 'Communications,Healthcare,Life Sciences,Public Sector';
            Solution_Profiles__c tstSp04 = new Solution_Profiles__c(
                Name='spTest04', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp04);
            insert spList;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile multi insert did not update expertise area on Opp correctly!'
            );

            // 04. Solution Profile insert (blank expertise area)
            eaStr = null;
            eaStrFull = 'Communications,Healthcare,Life Sciences,Public Sector';
            Solution_Profiles__c tstSp05 = new Solution_Profiles__c(
                Name='spTest05', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            insert tstSp05;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile insert (blank) should not modify expertise area on Opp!'
            );

            // 05. Solution Profile insert (expertise area duplicates)
            eaStr = 'Healthcare'; // 'Healthcare' also on tstSp01
            eaStrFull = 'Communications,Healthcare,Life Sciences,Public Sector';
            Solution_Profiles__c tstSp06 = new Solution_Profiles__c(
                Name='spTest06', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            insert tstSp06;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile insert (duplicate) should not modify expertise area on Opp!'
            );
        }
        Test.stopTest();
    }

    static testMethod void testSyncUpdate() {
        Test.startTest();
        User tstUser = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(tstUser) {
            // Prepare test data
            RecordType oppRt = [
                SELECT Id, Name FROM RecordType WHERE Name LIKE 'Qlikbuy % Standard'
            ];


            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
            Semaphores.Opportunity_ManageForecastProductsIsInsert=true;
            QTTestUtils.GlobalSetUp();

            Opportunity tstOpp = QTTestUtils.createMockOpportunity(
                'sShortDescription', 'sName', 'Existing Customer', // sType
                'Direct', // sRevenueType
                'Goal Identified', // sStage
                ''+oppRt.Id, 'GBP', // sCurrencyIsoCode
                tstUser, true // doInsert
            );
            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];

            // Start testing
            System.assertEquals(null, tstOpp.Expertise_Area__c,
                'Opportunity\'s Expertise Area should be null on a new Opportunity!'
            );

            // 01. Solution Profile triple insert
            List<Solution_Profiles__c> spList = new List<Solution_Profiles__c>();
            String eaStr = 'Public Sector';
            String eaStrFull = 'Public Sector';
            Solution_Profiles__c tstSp01 = new Solution_Profiles__c(
                Name='spTest01', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp01);
            eaStr = 'Healthcare';
            eaStrFull = 'Healthcare,Public Sector';
            Solution_Profiles__c tstSp02 = new Solution_Profiles__c(
                Name='spTest02', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp02);
            eaStr = 'Communications';
            eaStrFull = 'Communications,Healthcare,Public Sector';
            Solution_Profiles__c tstSp03 = new Solution_Profiles__c(
                Name='spTest03', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp03);
            insert spList;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile multi insert did not update expertise area on Opp correctly!'
            );

            // 02. Solution Profile update
            //     Healthcare -> Life Sciences
            eaStr = 'Life Sciences';
            eaStrFull = 'Communications,Life Sciences,Public Sector';
            tstSp02.Expertise_Area__c = eaStr;
            update tstSp02;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile update did not set expertise area on Opp correctly!'
            );

            // 03. Solution Profile update (expertise area duplicates)
            //     Public Sector -> Communications
            eaStr = 'Communications'; // 'Communications' also on tstSp03
            eaStrFull = 'Communications,Life Sciences';
            tstSp01.Expertise_Area__c = eaStr;
            update tstSp01;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile update (duplicate) did not set expertise area on Opp correctly!'
            );

            // 04. Solution Profile insert (expertise area duplicates)
            eaStr = 'Communications'; // 'Communications' also on tstSp01 & tstSp03
            eaStrFull = 'Communications,Life Sciences';
            Solution_Profiles__c tstSp04 = new Solution_Profiles__c(
                Name='spTest04', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            insert tstSp04;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile insert (duplicate) should not modify expertise area on Opp!'
            );

            // 05. Solution Profile update (blank duplicate expertise area)
            //     Communications -> null
            eaStr = null; // 'Communications' also on tstSp01 & tstSp04
            eaStrFull = 'Communications,Life Sciences';
            tstSp03.Expertise_Area__c = eaStr;
            update tstSp03;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile update (blank duplicate) should not modify expertise area on Opp!'
            );
        }
        Test.stopTest();
    }

    static testMethod void testSyncDelete() {
        Test.startTest();
        User tstUser = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(tstUser) {
            // Prepare test data
            RecordType oppRt = [
                SELECT Id, Name FROM RecordType WHERE Name LIKE 'Qlikbuy % Standard'
            ];


            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
            Semaphores.Opportunity_ManageForecastProductsIsInsert=true;
            QTTestUtils.GlobalSetUp();

            Opportunity tstOpp = QTTestUtils.createMockOpportunity(
                'sShortDescription', 'sName', 'Existing Customer', // sType
                'Direct', // sRevenueType
                'Goal Identified', // sStage
                ''+oppRt.Id, 'GBP', // sCurrencyIsoCode
                tstUser, true // doInsert
            );
            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];

            // Start testing
            System.assertEquals(null, tstOpp.Expertise_Area__c,
                'Opportunity\'s Expertise Area should be null on a new Opportunity!'
            );

            // 01. Solution Profile quadruple insert
            List<Solution_Profiles__c> spList = new List<Solution_Profiles__c>();
            String eaStr = 'Public Sector';
            String eaStrFull = 'Public Sector';
            Solution_Profiles__c tstSp01 = new Solution_Profiles__c(
                Name='spTest01', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp01);
            eaStr = 'Healthcare';
            eaStrFull = 'Healthcare,Public Sector';
            Solution_Profiles__c tstSp02 = new Solution_Profiles__c(
                Name='spTest02', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp02);
            eaStr = 'Communications';
            eaStrFull = 'Communications,Healthcare,Public Sector';
            Solution_Profiles__c tstSp03 = new Solution_Profiles__c(
                Name='spTest03', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp03);
            eaStr = 'Healthcare'; // 'Healthcare' also on tstSp02
            eaStrFull = 'Communications,Healthcare,Public Sector';
            Solution_Profiles__c tstSp04 = new Solution_Profiles__c(
                Name='spTest04', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            spList.add(tstSp04);
            insert spList;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile multi insert did not update expertise area on Opp correctly!'
            );

            // 02. Solution Profile delete
            eaStrFull = 'Communications,Healthcare';
            delete tstSp01; // 'Public Sector'

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile delete did not update expertise area on Opp correctly!'
            );

            // 03. Solution Profile delete (duplicate)
            eaStrFull = 'Communications,Healthcare';
            delete tstSp04; // 'Healthcare' also on tstSp02

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile delete (duplicate) should not modify expertise area on Opp!'
            );
        }
        Test.stopTest();
    }

    static testMethod void testSyncLockedOpp() {
        Test.startTest();
        // Prepare a non system admin user
        User tstUser = QTTestUtils.createMockUserForProfile(
            'PRM - Independent Territory + QlikBuy'
        );
        System.RunAs(tstUser) {
            // Prepare test data
            RecordType oppRt = [
                SELECT Id, Name FROM RecordType WHERE Name LIKE 'Qlikbuy % Standard'
            ];


            Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
            Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
            Semaphores.Opportunity_ManageForecastProductsIsInsert=true;
            QTTestUtils.GlobalSetUp();

            
            Opportunity tstOpp = QTTestUtils.createMockOpportunity(
                'sShortDescription', 'sName', 'Existing Customer', // sType
                'Direct', // sRevenueType
                'Goal Identified', // sStage
                ''+oppRt.Id, 'GBP', // sCurrencyIsoCode
                tstUser, true // doInsert
            );

            tstOpp = [
                SELECT Id, Expertise_Area__c, Finance_Approved__c
                FROM Opportunity WHERE Id = :tstOpp.Id
            ];
            // Lock the Opportunity
            tstOpp.Finance_Approved__c = true;
            update tstOpp;
            
            // Start testing
            System.assertEquals(null, tstOpp.Expertise_Area__c,
                'Opportunity\'s Expertise Area should be null on a new Opportunity!'
            );
            System.assertEquals(true, tstOpp.Finance_Approved__c,
                'Opportunity\'s Finance Approved should be TRUE on updated Opp!'
            );

            // Solution Profile insert to a 'locked' Opp
            String eaStr = 'Healthcare';
            String eaStrFull = eaStr;
            Solution_Profiles__c tstSp01 = new Solution_Profiles__c(
                Name='spTest01', Opportunity_Name__c=tstOpp.Id, Expertise_Area__c=eaStr
            );
            insert tstSp01;

            tstOpp = [SELECT Id, Expertise_Area__c FROM Opportunity WHERE Id = :tstOpp.Id];
            System.assertEquals(eaStrFull, tstOpp.Expertise_Area__c,
                'Solution Profile insert did not set expertise area on Opp correctly!'
            );
        }
        Test.stopTest();
    }*/
}