/******************************************************

    Changelog:
        2017-05-26  CCE     CHG0031108 - Updates to Operations Request - Initial development of test class for NotesTrigger
                        
******************************************************/
@isTest
private class NotesTriggerTest {
    
    @isTest static void test_AddNoteToOperationsRequest() {
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(mockSysAdmin) {
            Marketing_Request__c mr = new Marketing_Request__c();
            mr.Requestor__c = mockSysAdmin.Id;
            mr.Project_Name__c = 'My OR test';
            mr.Project_Description__c = 'Blah blah blah';
            mr.Campaign_type__c = 'Internal project';
            mr.Expected_Campaign_Launch_Date__c = Date.today().addDays(1);
            mr.Target_Completion_Date__c = Date.today().addDays(10);
            mr.Region__c = 'Americas';
            mr.RecordTypeId = '012D0000000KJMD'; //Marketo Support Requests record type
            insert mr;

            Test.startTest();
            Note myNote = new Note(ParentId = mr.Id, Title = 'New Note', Body = 'My test note');
            insert myNote;

            Marketing_Request__c mrRet = [select Send_Note_Email__c from Marketing_Request__c where Id = :mr.Id];
            //This is where we need to add an assert to test Send_Note_Email__c is being set. Unfortunately as soon as the field is 
            //set a workflow will see it, send an email and clear it before we get a chance to read it again. This means that we can 
            //only provide coverage for the trigger. At this time of initial deveopment coverage is 100%
            //System.assert(mrRet.Send_Note_Email__c == true);
            Test.stopTest();
        }
    }   
}