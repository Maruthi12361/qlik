/**
	This class contains utility methods for creating test data

	Changelog:
		2012-08-14	TJG		Appended @test.com.qttest to userNameTest so that user record can be inserted.	
*/
@isTest
public with sharing class TestDataUtils {
	
	public static User createUser(String username, String nick) {
		Profile profile = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
    	User user = new User(Username = username, LastName = 'Test Lastname', Email = username, 
    						 Alias = 'tula', CommunityNickname = nick, TimeZoneSidKey = 'Europe/Paris', 
    						 LocaleSidKey = 'es_ES', EmailEncodingKey = 'ISO-8859-1', ProfileId = profile.Id, LanguageLocaleKey = 'en_US'); 
    	insert user;
    	return user;
	}
		
	
	static testMethod void test() {
		createUser('userNameTest@test.com.qttest','nickTest');
	}
}