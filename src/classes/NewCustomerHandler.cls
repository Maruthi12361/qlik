/*
    BSL-1261 Created by shubham Gupta 9/1/2019
    This Handler is placed on Opportunity trigger which on closure of opportunity to any closed won stage will set Accountstatus
    field on Account to Customer for the same as well as any prospect account having same DUNS number as account of this opportunity
    it will also set new customer status to false for all opportunites related to those accounts apart for the one which is closed here
    */
    public class NewCustomerHandler {

        public static void processCustomerFlag(List<Opportunity> newopp, Map<id,Opportunity> oldoppmap){
            Set<id> oppid = new Set<id>();
            Set<id> accid = new Set<id>();
            List<Account> acctoupdate = new List<Account>();
            List<Opportunity> opptoupdate = new List<Opportunity>();
            List<Account> withoutDuns = new List<Account>(); 
            Set<String> duns = new Set<String>();
            system.debug('new list******' + newopp);
            system.debug('oldmap***'+oldoppmap);
            for(Opportunity opp:newopp){
                system.debug('newopp stagename'+opp.stageName);
                system.debug('oldop stagename'+oldoppmap.get(opp.id).stageName);
                if(opp.stageName != oldoppmap.get(opp.id).stageName && opp.stageName.containsIgnoreCase('closed') && !opp.stageName.containsIgnoreCase('Lost')){
                    accid.add(opp.AccountID);
                    oppid.add(opp.id);
                }
            }

            if(!accid.isEmpty()){
            List<Account> relatedAcc = [select id,Domestic_Ultimate_DUNS__c,AccountStatus__c,(select id,New_Customer__c,Stagename,Bypass_Rules__c from Opportunities where id NOT IN :oppid AND (NOT Stagename LIKE '%closed%') AND New_Customer__c = true) from Account where id IN:accid AND AccountStatus__c != 'Customer'];
            system.debug('accountcame'+relatedAcc);
            if(!relatedAcc.isEmpty()){
                for(Account acc:relatedAcc){
                    system.debug('dunscame'+acc.Domestic_Ultimate_DUNS__c);
                    if(acc.Domestic_Ultimate_DUNS__c != '000000000' && !String.isBlank(acc.Domestic_Ultimate_DUNS__c)){
                    duns.add(acc.Domestic_Ultimate_DUNS__c);
                    }
                    else{
                        withoutDuns.add(acc);
                    }
                }
                if(!withoutDuns.isEmpty() && withoutDuns != null){
                    for(Account acctw:withoutDuns){
                        acctw.AccountStatus__c = 'Customer';
                        acctoupdate.add(acctw);
                        for(Opportunity oppw:acctw.Opportunities){
                            oppw.New_Customer__c = false;
                            oppw.Bypass_Rules__c = true;
                            opptoupdate.add(oppw);
                        }
                    }
                }
                system.debug('dunsset'+duns);
                if(duns!=null && !duns.isEmpty()){
                    List<Account> acctssameduns = [select id,Domestic_Ultimate_DUNS__c,AccountStatus__c,(select id,New_Customer__c,Stagename,Bypass_Rules__c from Opportunities where id NOT IN :oppid AND (NOT Stagename LIKE '%closed%') AND New_Customer__c = true) from Account where Domestic_Ultimate_DUNS__c IN :duns AND AccountStatus__c = 'Prospect'];

                    if(!acctssameduns.isEmpty()){
                        For(Account acct:acctssameduns){
                            acct.AccountStatus__c = 'Customer';
                            acctoupdate.add(acct);
                            For(Opportunity oppt:acct.Opportunities){
                                oppt.New_Customer__c = false;
                                oppt.Bypass_Rules__c = true;
                                opptoupdate.add(oppt);
                            }
                        }
                        
                        }
                    }

                        if(!acctoupdate.isEmpty()){
                        Semaphores.CustomAccountTriggerAfterUpdate = true;
                        Update acctoupdate;
                        }
                        if(!opptoupdate.isEmpty()){
                        Update opptoupdate;  
                        }
                    }
                }
            }
    }