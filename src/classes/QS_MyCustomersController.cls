/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_MyCustomersController {

public string UserId{get{return (UserInfo.getUserId());}}
public boolean selectedAccountNameHasChanged {get; set;}
public boolean ListFlag{get; set;}
public List<Case> resultsOpen { get; set; }
public boolean resultsLicensesNeedsUpdate { get; set; }
public boolean resultsCaseNeedsUpdate { get; set; }
public List<SelectOption> accountOptionListFiltered1 {get; set;}
public String AccountFilterText {get; set;}
public String Contact_partner { get; set; }
public boolean Flag ;

private string v_selectedAccountName;
    public String selectedAccountName {
        get {
            if(v_selectedAccountName == null)
                v_selectedAccountName = '';
            return v_selectedAccountName;
        }
        set {
            if(v_selectedAccountName != value) {
                v_selectedAccountName = value;
                selectedAccountNameHasChanged = true;
                resultsCaseNeedsUpdate = true;
                resultsLicensesNeedsUpdate = true;
            }
        }
    }

private transient User v_objUser;
    public User objUser {
        get {
            if(v_objUser == null)
                v_objUser = [Select id, name, accountid, Contact.Account.ispartner From User  where Id = :userinfo.getUserId()  LIMIT 1];
            return v_objUser;
        }
        set {
            v_objUser = value;
        }
        
    }
    private transient boolean v_isPartner;
    public boolean IsPartner {
        get {
            if(v_isPartner == null)
                v_isPartner = (objUser.Contact.Account.ispartner);
            return v_isPartner;
        }
        set {
            v_isPartner = value;
        }
    }



private transient List<Account> v_accountList;
    public List<Account> accountList {
        get {
            if(v_accountList == null) {
              
                if(!IsPartner) {
                    Id accountId = objUser.AccountId;
                    v_accountList = [Select Id, Name, RecordtypeId, RecordType.Name  From Account 
                                    Where Id = :accountId LIMIT 1];
                } else {
                    v_accountList = [Select Id, Name, RecordtypeId, RecordType.Name
                                    From Account Order By Name, RecordType.Name limit 1000];
                }
            }
            return v_accountList;
        }
        set {
            v_accountList = value;
        }
    }


    private transient List<SelectOption> v_accountOptionList;
    public List<SelectOption> accountOptionList {   
        get {
            if(v_accountOptionList == null){
                       
                v_accountOptionList = new list<SelectOption>();
                for(Account acc : accountList) {
                    v_accountOptionList.add(new SelectOption(acc.Id, acc.Name));
                }
            }
            
            return v_accountOptionList;
        }
        set {
            v_accountOptionList = value;
        }
    }
    public integer accountListSize {
        get {
            return accountList.Size();
        }
        set {
        }
    }

private transient List<SelectOption> v_CustList;
   public List<SelectOption> CustList {   
        get {
            if(v_CustList== null){
                       
                v_CustList = new list<SelectOption>();
                for(Account acc:  [SELECT Id, name From Account where isPartner= False order by Name asc]) {
                    v_CustList.add(new SelectOption(acc.Id, acc.Name));
                }
            }
            
            return v_CustList;
        }
        set {
            v_CustList = value;
        }
    }
    
    public integer CustListSize {
        get {
            return CustList.Size();
        }
        set {
        }
    }

 // all License(s) belongs to the user
    @TestVisible
    private List<QS_EntitlementWrapper> v_resultsLicenses;
    public List<QS_EntitlementWrapper> resultsLicenses {
        get {
            if(resultsOriginalLicenses != null) {
                if(resultsLicensesNeedsUpdate) {
                    v_resultsLicenses = new List<QS_EntitlementWrapper>();
                    for (Entitlement lic : resultsOriginalLicenses) {
                        if(v_resultsLicenses.size() < 1000) {
                            v_resultsLicenses.add(new QS_EntitlementWrapper(lic));
                        }
                    }
                    resultsLicensesNeedsUpdate = false;
                }
            } else if (v_resultsLicenses == null)
                v_resultsLicenses = new List<QS_EntitlementWrapper>();
            system.debug('v_resultsLicenses.Size(): ' + v_resultsLicenses.Size());
            return v_resultsLicenses;
        }
    }
    @TestVisible
    private List<Case> v_resultsCase;
    public List<Case> resultsCase {   
        get {
            if(resultsOriginalLicenses != null) {
                if(resultsCaseNeedsUpdate) {
                    v_resultsCase = new List<case>();
                    if(listFlag) {
                    for (Case lic : resultsOpen ) {
                        if( v_resultsCase.size() < 1000) {
                            v_resultsCase.add(lic);
                        }
                    }
                    }
                    resultsCaseNeedsUpdate = false;
                }
            }
            else if (v_resultsCase == null)
              //  v_resultsCase = new List<QS_EntitlementWrapper>();
            system.debug('v_resultsCase.Size(): ' + v_resultsCase.Size());
            return v_resultsCase;
        }
    }
    
    

    @TestVisible
    // all License(s) belongs to the user
    private List<Entitlement> v_resultsOriginalLicenses;
    public List<Entitlement> resultsOriginalLicenses {
        get {
            system.debug('licenseOptionList get start');
            system.debug('selectedAccountName: ' + selectedAccountName);
            system.debug('selectedAccountNameHasChanged: ' + selectedAccountNameHasChanged);
            system.debug('v_resultsOriginalLicenses==null: ' + (v_resultsOriginalLicenses==null));


            if(selectedAccountNameHasChanged || v_resultsOriginalLicenses == null) {
                if (selectedAccountName != null && selectedAccountName != ''){
                    //Entitlement_Check__c = TRUE and 
                    List<Entitlement> licenses = [select Id, Name, Status, Description__c, Account.Name, StartDate, EndDate, SlaProcess.Description, SlaProcess.Name, Entitlement_Check__c , Legacy_License_Key__c 
                                                                                    from Entitlement 
                                                                                    where Account.Id = :selectedAccountName and 
                                                                                        (EndDate >= TODAY or Entitlement_Check__c = true) and
                                                                                        RecordType.Name != 'Obsolete' 
                                                                                        Order by Name, Account.Name, Status];
                    
                                                                                        
                    if (licenses != null && licenses.size() > 0){
                        v_resultsOriginalLicenses = licenses;
                    } else
                        v_resultsOriginalLicenses = new List<Entitlement>();
                }
                else
                    v_resultsOriginalLicenses = new List<Entitlement>();
                selectedAccountNameHasChanged = false;
            }
            system.debug('licenseOptionList get end');
            system.debug('v_resultsOriginalLicenses.Size(): ' + v_resultsOriginalLicenses.Size());
            return v_resultsOriginalLicenses;
        }
    }



public QS_MyCustomersController() {
      // system.debug('QS_MyCustomersController : UserID'+ Userid);      
        Flag = false;
        ListFlag = false;
       Contact_partner = '';
       AccountFilterText = '';
       selectedAccountName = '';
        
         FilterCustomers();
       
         
    }

 public void FilterCustomers() {
        system.debug('FilterCustomers start');
        
        Id contactId, accId = null, Acc;
        string Accounttype ='';
        User usr = [Select contactid from User where id =: Userinfo.getUserid()];
       
                
        List<SelectOption> newFilteredList = new List<SelectOption>();
        string filterText = AccountFilterText.toLowerCase();

        if(accountOptionList != null)
            for(SelectOption so : CustList)
                if(so.getLabel().toLowerCase().contains(filterText))
                    newFilteredList.Add(so);

        accountOptionListFiltered1 = new List<SelectOption>();
        
        
        system.debug('newFilteredList.Size(): ' + newFilteredList.Size());
        if(newFilteredList.size() > 100) {
            accountOptionListFiltered1.add(new SelectOption('','Too many accounts, please use the filter above ('+newFilteredList.Size()+')'));        
        }
        else if(newFilteredList.size() > 0) {
            if(newFilteredList.size() > 1)
                accountOptionListFiltered1.add(new SelectOption('','Please select a value ('+newFilteredList.Size()+')'));
            accountOptionListFiltered1.addAll(newFilteredList);
        }
        else
            accountOptionListFiltered1.add(new SelectOption('','No accounts found, please use the filter to find an account'));        

        if(accountOptionListFiltered1.size() > 0)
        {
            selectedAccountName = accountOptionListFiltered1[0].getValue();
          //  GetEnvironments();
            }
     
        
        system.debug('FilterCustomers end');
    }

    public void getCases()
    {
        system.debug('getCases start');
       // List<Case> results;
        
        system.debug('selectedAccountName : ' + selectedAccountName);
       
        
        String QueryCase = '';      
        String conId= '';         
        List<Case> results = new List<Case>(); 
        
        QueryCase = 'select Id, CaseNumber, Account.name , AccountId, License_No__c, Subject, Severity__c, ClosedDate, Account_Origin__c, Status, CreatedDate, Environment__c, EnvironmentOfCase__r.name from Case ';
      
            
          if(Flag)
       {        
          if (selectedAccountName != null && selectedAccountName != ''){ 
                       QueryCase += ' where Account_Origin__c = :selectedAccountName ';                     
                } 
       }
      
         Flag = false;
                     
                     QueryCase += ' order by CreatedDate desc LIMIT 1000';
                     
                     results = (List<Case>) Database.query(QueryCase );
                         
                     system.debug('results : ' + results ); 
        

       
        resultsOpen = new List<Case>();
        
        if(results != null && (!results.isEmpty())) {
            for (Case caseObj : results) {
                system.debug('Case.CaseNumber: ' + caseObj.CaseNumber);
                if (caseObj.Status != null)
                {
                            resultsOpen.add(caseObj);                    
                   
                }
            }
        }
       
        system.debug('Amount open: ' + resultsOpen.size());
        

       
    }
    
    public PageReference filterPartnerList() {
        
        if(selectedAccountName != null && selectedAccountName != '') {
            List<Account> categoryLookupList = [SELECT Id, name ,Phone From Account where Id =: selectedAccountName];
           
            
            for(Account categoryLookup : categoryLookupList) {
                    Contact_partner = categoryLookup.Phone;
                }
              
        }
    
        return null;
    }
    
    public void  refreshLicenseList(){
        system.debug('refreshLicenseList start');        
        ListFlag = true;
        Flag =true;         
        Contact_partner ='';
        resultsOpen = new List<Case>();
        if (selectedAccountName != null && selectedAccountName != ''){ 
        getCases();        
        filterPartnerList();
        }
        system.debug('refreshLicenseList end');

    }


}