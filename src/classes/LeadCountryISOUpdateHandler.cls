/******************************************************

    Class: LeadCountryISOUpdateHandler
    
    Initiator: 	Tina Jensen 
    
    Changelog:
        2009-10-05  MBH     Created file
        2014-03-14  KMH     CR# 9851 - QMS Mailing Country Workflow
                            https://eu1.salesforce.com/a0CD000000bifua
        2017-10-25 AYS BMW-402 : Migrated from LeadCountryISOUpdate.trigger.

******************************************************/

public class LeadCountryISOUpdateHandler {
	public LeadCountryISOUpdateHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld, Boolean isUpdate) {
		System.Debug('LeadCountryISOUpdate starting');
    
	    List<string> CountryNames = new List<string>();
	    Map<string, string> CountryNameToISO = new Map<string, string>();
	    ID leadType = '01220000000DOzwAAG';// Standard Lead record type
	    ID PartnerleadType = '012D0000000JsWAIA0'; // Partner Opp Reg record type- CR9851-KMH

	    List<QlikTech_Company__c> QlikTechCompanys = [select Id, Name, Country_Name__c from QlikTech_Company__c];   
	    for (QlikTech_Company__c QTc : QlikTechCompanys) {
	        CountryNameToISO.put(QTc.Country_Name__c.toLowerCase(), QTc.Id);    
	    }
	    
	    for (integer i = 0; i < triggerNew.size(); i++) {
	        
	        if ((triggerNew[i].RecordTypeId != leadType) && (triggerNew[i].RecordTypeId != PartnerleadType )) continue;
	        
	        if (triggerNew[i].Country == null) continue;
	        
	        if (isUpdate && triggerNew[i].Country == triggerOld[i].Country) continue;
	        
	        if (CountryNameToISO.containsKey(triggerNew[i].Country.toLowerCase())) {
	            triggerNew[i].Country_Code__c = CountryNameToISO.get(triggerNew[i].Country.toLowerCase());
	        }
	    }
	    System.Debug('LeadCountryISOUpdate finnish');
	}
}