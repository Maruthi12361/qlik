/********************************************************
* Test class: CoveoCaseAttachedResultTriggerHndlrTest
* Description: Test class for CoveoCaseAttachedResultTriggerHandler
*
* Change Log:
* -------
* AUTHOR      DATE        DETAIL
*
* AIN         2019-07-09  Initial Development for IT-1959
* AIN         2019-10-01  Changed for IT-2105
*
**********************************************************/
@isTest
public class CoveoCaseAttachedResultTriggerHndlrTest {

    private static Id recTypeId;

    @isTest
    public static void Test1() {

        //Insert test data
        List<Bugs__c> bugs = new List<Bugs__c>();
        Bugs__c bug1 = new Bugs__c();
        bug1.Name = 'Bug 1';

        Bugs__c bug2 = new Bugs__c();
        bug2.Name = 'Bug 2';

        bugs.add(bug1);
        bugs.add(bug2);
        insert bugs;

        Account acc = new Account();
        acc.Name = 'Test account 1';
        insert acc;

        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Testersson';
        con.AccountId = acc.Id;
        insert con;


        Case caseObj;


        caseObj = TestDataFactory.createCase('Test subject 1', 'Test description 1', UserInfo.getUserId(), '3',
                    con.Id, acc.Id, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Email',
                    false, null, null,
                    null, null, null, null);
        caseObj.Area__c = 'I have an installation issue';
        caseObj.Issue__c = 'I have an issue with a new installation of a Qlik Client';
        
        insert caseObj;

        //Inserting this attached result should add bug1 as a lookup on case
        CoveoV2__CoveoCaseAttachedResult__c attachedResult1 = new CoveoV2__CoveoCaseAttachedResult__c();
        attachedResult1.BugId__c = bug1.Id;
        attachedResult1.CoveoV2__case__c = caseObj.Id;
        attachedResult1.CoveoV2__ResultUrl__c = 'Bug Test';
        attachedResult1.CoveoV2__Source__c = 'Bug Test';
        attachedResult1.CoveoV2__Title__c = 'Bug Test';
        attachedResult1.CoveoV2__UriHash__c = 'Bug Test';
        insert attachedResult1;

        caseObj = [select id, Bug__c from Case where id = :caseObj.Id];
        system.assert(caseObj.Bug__c ==  bug1.Id);

        //Inserting this attached result should add bug2 as a lookup on case and delete attachedResult1
        CoveoV2__CoveoCaseAttachedResult__c attachedResult2 = new CoveoV2__CoveoCaseAttachedResult__c();
        attachedResult2.BugId__c = bug2.Id;
        attachedResult2.CoveoV2__case__c = caseObj.Id;
        attachedResult2.CoveoV2__ResultUrl__c = 'Bug Test';
        attachedResult2.CoveoV2__Source__c = 'Bug Test';
        attachedResult2.CoveoV2__Title__c = 'Bug Test';
        attachedResult2.CoveoV2__UriHash__c = 'Bug Test';
        insert attachedResult2;

        caseObj = [select id, Bug__c from Case where id = :caseObj.Id];
        system.assert(caseObj.Bug__c ==  bug2.Id);

        List<CoveoV2__CoveoCaseAttachedResult__c> attachedResults = [select id from CoveoV2__CoveoCaseAttachedResult__c];
        system.assert(attachedResults.size() == 1);

        
        caseObj = [select id, Bug__c from Case where id = :caseObj.Id];
        system.assert(caseObj.Bug__c ==  bug2.Id);

        //Inserting this attached result should remove bug 2 from the lookup on case
        delete attachedResult2;

        caseObj = [select id, Bug__c from Case where id = :caseObj.Id];
        system.assert(caseObj.Bug__c ==  null);
    }
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}