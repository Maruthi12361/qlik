/****************************************************************
*
*  TEST_CustomQuoteTriggerHandler
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*  17.03.2017 : Rodion Vakulvsokyi
*  23.03.2017 : Rodion Vakulvsokyi fixed test coverage
*  27.03.2017 : Rodion Vakulvsokyi fixed test coverage
*  07.08.2017 : Oleksandr@4Front adding tests for the SetQuotePaymentTerms
*  09.08.2017 : Aslam Kamal QCW-2934 fixed test failure
*  12-09-2017 : Linus Löfberg Q2CW-2953 setting quote recipient to conform with added validation conditions.
*  18-04-2018 : AIN BSL-10 fix
*****************************************************************/
@isTest
public class TEST_CustomQuoteTriggerHandler{
    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    private static testMethod void testMethod1() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
               RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
									where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

		 /******* changes for qcw-2934 start *********/
        insert QuoteTestHelper.createPCS(testPartnerAccount);
       /******* changes for qcw-2934 end*********/
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           quoteForTest.SBQQ__Primary__c = true;
            quoteForTest.Sector__c='Healthcare';
            quoteForTest.Industry__c='Healthcare';
            quoteForTest.Function__c='Healthcare';
            quoteForTest.Solution_Area__c='Healthcare - Emergency Medicine';
           Test.startTest();
           insert quoteForTest;

            update quoteForTest;

        Test.stopTest();

        Opportunity createdOpp = [Select id From Opportunity];

        System.assertEquals([select id, SBQQ__Opportunity2__c From SBQQ__Quote__c Where id =:quoteForTest.id].SBQQ__Opportunity2__c, createdOpp.id);

        delete quoteForTest;
        }
    }

     private static testMethod void testMethod2() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id), createQlikTech('Iran (Islamic Republic of)', 'IR', testSubs.id)};
        insert listOfcreate;

        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
 /******* changes for qcw-2934 start *********/
            insert QuoteTestHelper.createPCS(testPartnerAccount);
  /******* changes for qcw-2934 end*********/
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
        quoteForTest.SBQQ__BillingCountry__c = 'France';
            quoteForTest.Bypass_Rules__c = true;
			Test.startTest();
        insert quoteForTest;


                //update quoteForTest;

            Opportunity createdOpp = [Select id From Opportunity];

            System.assertEquals([select id, SBQQ__Opportunity2__c From SBQQ__Quote__c Where id =:quoteForTest.id].SBQQ__Opportunity2__c, createdOpp.id);

            quoteForTest.SBQQ__BillingCountry__c = 'Iran (Islamic Republic of)';
            quoteForTest.SBQQ__Primary__c = true;
            quoteForTest.Legal_Approval_Triggered__c = false;

            update quoteForTest;
            List<SBQQ__Quote__c> listOfQuotes = new List<SBQQ__Quote__c>();
            listOfQuotes.add(quoteForTest);

            Account testAccount1 = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
          update testAccount;
                delete quoteForTest;
            Test.stopTest();
        }
    }

    private static testMethod void testMethod3() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {

            Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
       List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
       insert listOfcreate;

        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

		 /******* changes for qcw-2934 start *********/
       insert QuoteTestHelper.createPCS(testPartnerAccount);
      /******* changes for qcw-2934 end*********/

        //Contact testContact2 = QuoteTestHelper.createContact(testAccount.id);
        //    insert testContact2;
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];


        Test.startTest();
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
            insert quoteForTest;

            //update quoteForTest;
            quoteForTest.SBQQ__Primary__c = true;
            //quoteForTest.Quote_Recipient__c = testContact2.id;
            quoteForTest.Quote_Recipient__c = testContact.id;
            update quoteForTest;

            Opportunity createdOpp = [Select id, RecordTypeId From Opportunity];

            Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where id =:createdOpp.RecordTypeId]);
            oppCreatedForUpdate.Revenue_Type__c = 'Direct';
            insert oppCreatedForUpdate;
            quoteForTest.SBQQ__Opportunity2__c = oppCreatedForUpdate.id;
            update quoteForTest;
            oppCreatedForUpdate.CloseDate = Date.today().addDays(10);
            update oppCreatedForUpdate;
            Test.stopTest();
        }
    }

    private static testMethod void testMethod4() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
		 /******* changes for qcw-2934 start *********/
        insert QuoteTestHelper.createPCS(testPartnerAccount);
         /******* changes for qcw-2934 end*********/
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            pbEntryTest.CurrencyIsoCode = 'USD';
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];

                Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
                oppCreatedForUpdate.CurrencyIsoCode = 'USD';
                oppCreatedForUpdate.Revenue_Type__c = 'Direct';
                insert oppCreatedForUpdate;
                Test.startTest();
                SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
                quoteForTest.SBQQ__Primary__c = true;
                quoteForTest.SBQQ__Opportunity2__c = oppCreatedForUpdate.id;

                insert quoteForTest;
                OpportunityLineItem oppLineItem = QuoteTestHelper.createLineItem(oppCreatedForUpdate.id, pbEntryTest);
                    //oppLineItem.CurrencyIsoCode = 'USD';
                insert oppLineItem;


        Test.stopTest();


        //System.assertEquals([select id, SBQQ__Opportunity2__c From SBQQ__Quote__c Where id =:quoteForTest.id].SBQQ__Opportunity2__c, createdOpp.id);

        }
    }


    private static testMethod void testMethod5() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
		/******* changes for qcw-2934 start *********/
		insert QuoteTestHelper.createPCS(testPartnerAccount);
         /******* changes for qcw-2934 end*********/
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            pbEntryTest.CurrencyIsoCode = 'USD';
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];

                Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount, '', [Select id From RecordType where DeveloperName ='Sales_QCCS']);
                    oppCreatedForUpdate.CurrencyIsoCode = 'USD';
                insert oppCreatedForUpdate;
                Test.startTest();
                OpportunityLineItem oppLineItem = QuoteTestHelper.createLineItem(oppCreatedForUpdate.id, pbEntryTest);
                    //oppLineItem.CurrencyIsoCode = 'USD';
                insert oppLineItem;
                Map<Id, OpportunityLineItem> oppLineItemMap = new Map<Id, OpportunityLineItem>();
                oppLineItemMap.put(oppLineItem.id, oppLineItem);
                OppProductForecastAmountCalcHandler.onAfterInsert(new List<OpportunityLineItem>{oppLineItem}, oppLineItemMap);
                OppProductForecastAmountCalcHandler.onAfterDelete(new List<OpportunityLineItem>{oppLineItem}, oppLineItemMap);

        Test.stopTest();


        //System.assertEquals([select id, SBQQ__Opportunity2__c From SBQQ__Quote__c Where id =:quoteForTest.id].SBQQ__Opportunity2__c, createdOpp.id);

        }
    }



	private static testmethod void testMethod7() {
	    QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
		  Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testPartnerAccount.id);
            insert testContact;
        Contact testContact2 = QuoteTestHelper.createContact(testAccount.Id);
            insert testContact2;
		/******* changes for qcw-2934 start *********/
		  insert QuoteTestHelper.createPCS(testPartnerAccount);
        /******* changes for qcw-2934 end*********/

		Test.startTest();
		RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Reseller', 'Open', 'Quote', false, '');
			quoteForTest.Partner_Contact__c = testContact.id;
            quoteForTest.Quote_Recipient__c = testContact2.Id;
			quoteForTest.Sell_Through_Partner__c = testPartnerAccount.id;
		insert quoteForTest;
		update quoteForTest;
		Test.stopTest();
	}

	private static testmethod void testMethod8() {
	    QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
		  Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;

			Account testAccount2 = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount2.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount2;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testPartnerContact = QuoteTestHelper.createContact(testPartnerAccount.id);
            insert testPartnerContact;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Contact testContact2 = QuoteTestHelper.createContact(testAccount2.id);
            insert testContact2;
		/******* changes for qcw-2934 start *********/
        insert QuoteTestHelper.createPCS(testPartnerAccount);
      /******* changes for qcw-2934 end*********/
		Opportunity oppCreatedForUpdate = QuoteTestHelper.createOpportunity(testAccount2, testPartnerAccount.Id, [Select id From RecordType where DeveloperName ='Sales_QCCS']);
        oppCreatedForUpdate.CurrencyIsoCode = 'USD';
        oppCreatedForUpdate.Revenue_Type__c = 'Not For Resale';
        oppCreatedForUpdate.Partner_Contact__c = testPartnerContact.Id;
        insert oppCreatedForUpdate;
		Test.startTest();
		RecordType rType = [Select id From RecordType Where developerName = 'Internal_NFR_Quote'];
		SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, testPartnerAccount.id, 'Not For Resale', 'Open', 'Quote', false, '');
			quoteForTest.Partner_Contact__c = testPartnerContact.id;
			quoteForTest.Sell_Through_Partner__c = testPartnerAccount.id;
            quoteForTest.Quote_Recipient__c = testContact.id;
		insert quoteForTest;
		update quoteForTest;

		quoteForTest.SBQQ__Account__c = testAccount2.id;
        quoteForTest.Quote_Recipient__c = testContact2.id;
		quoteForTest.SBQQ__Opportunity2__c = oppCreatedForUpdate.id;
        quoteForTest.CurrencyIsoCode = 'USD';
		update quoteForTest;
		Test.stopTest();
	}

    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr,
                                            Country_Name__c = countryName,
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }

    static testMethod void test_SetQuotePaymentTerms_positive(){
        QuoteTestHelper.createCustomSettings();
        Test.startTest();

        RecordType recTypeAccEndUser = QuoteTestHelper.getRecordTypebyDevName('End_User_Account');
        RecordType recTypeQuote = QuoteTestHelper.getRecordTypebyDevName('Quote');

        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);

        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '60 days';
        insert accEndUser;

        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;

        SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote, contEndUser.Id, accEndUser, '', 'Direct', 'Open', 'Quote', false,'');
        quote.Bypass_Rules__c = true;
        insert quote;

        SBQQ__Quote__c assertQuote =    [SELECT Id, SBQQ__PaymentTerms__c, Sell_Through_Partner__c, SBQQ__Account__c
                                        FROM SBQQ__Quote__c
                                        WHERE Id = :quote.Id];

        System.assertEquals('60 days', assertQuote.SBQQ__PaymentTerms__c, 'Payment terms on SBQQ__Quote__c record should be ' + accEndUser.Payment_Terms__c + ' and it is : ' + assertQuote.SBQQ__PaymentTerms__c);
        Test.stopTest();
    }

    static testMethod void test_SetQuotePaymentTerms_negative(){
        QuoteTestHelper.createCustomSettings();
        Test.startTest();

        RecordType recTypeAccEndUser = QuoteTestHelper.getRecordTypebyDevName('End_User_Account');
        RecordType recTypeQuote = QuoteTestHelper.getRecordTypebyDevName('Quote');

        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);

        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        //accEndUser.Payment_Terms__c = '';
        insert accEndUser;

        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;

        SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote, contEndUser.Id, accEndUser, '', 'Direct', 'Open', 'Quote', false,'');
        quote.Bypass_Rules__c = true;
        insert quote;

        SBQQ__Quote__c assertQuote =    [SELECT Id, SBQQ__PaymentTerms__c, Sell_Through_Partner__c, SBQQ__Account__c
                                        FROM SBQQ__Quote__c
                                        WHERE Id = :quote.Id];

        System.assertEquals('30 days', assertQuote.SBQQ__PaymentTerms__c, 'Payment terms on SBQQ__Quote__c record should be ' + accEndUser.Payment_Terms__c + ' and it is : ' + assertQuote.SBQQ__PaymentTerms__c);
        Test.stopTest();
    }

    static testMethod void test_SetQuotePaymentTerms_reseller(){

        QuoteTestHelper.createCustomSettings();

        RecordType recTypeAccEndUser = [Select id, DeveloperName From Recordtype Where DeveloperName = 'End_User_Account' and SobjectType = 'Account'];
        RecordType recTypeAccPartner = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        RecordType recTypeQuote = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Quote' and SobjectType = 'SBQQ__Quote__c'];

        Test.startTest();

        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);

        Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
        accEndUser.Payment_Terms__c = '45 days';
        insert accEndUser;

        Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
        insert contEndUser;

		Account accPartner = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'PartnerAcc');
		accPartner.Navision_Status__c = 'Partner';
        accPartner.Payment_Terms__c = '75 days';
		insert accPartner;
		Contact conPartner = QuoteTestHelper.createContact(accPartner.id);
		insert conPartner;

		/******* changes for qcw-2934 start *********/
        insert QuoteTestHelper.createPCS(accPartner);
        /******* changes for qcw-2934 end *********/
        SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote, conPartner.Id, accEndUser, accPartner.Id, 'Reseller', 'Open', 'Quote', false,'');
        quote.Quote_Recipient__c = contEndUser.Id;
        quote.Bypass_Rules__c = true;
        insert quote;

        SBQQ__Quote__c assertQuote =    [SELECT Id, SBQQ__PaymentTerms__c
                                        FROM SBQQ__Quote__c
                                        WHERE Id = :quote.Id];

        System.assertEquals('75 days', assertQuote.SBQQ__PaymentTerms__c, 'Payment terms on SBQQ__Quote__c record should be ' + accPartner.Payment_Terms__c + ' and it is : ' + assertQuote.SBQQ__PaymentTerms__c);
        Test.stopTest();
    }
}