/********************************************************
* EntitlementSyncBatch24h
* Description:
*
* Change Log:
* 2012-07-01   Fluido (Alberto) Initial Development
* 2012-07-20   RDZ              CR# 5568 https://eu1.salesforce.com/a0CD000000NI3h1
*                               Add check on start/end date to Entitlement update classes
* 2013-03-21   TJG              CR# 7125 https://eu1.salesforce.com/a0CD000000U8rLn
* 2013-04-15   EXT_YKA:         CR# 7833 https://eu1.salesforce.com/a0CD000000XPrg9
*                               Standard Support should be default when creating Entitlements
* 2013-04-18   Fluido (Alberto) CR# 6986
* 2013-05-02   ATC              CR# 6986 replacing 'Obsolate' with 'Deleted', row 201
* 2016-04-13   NAD              Replaced Account License Support_Level__c references with INT_NetSuite_Support_Level__c per CR 33068.
* 2017-03-25   TJG              Fix testing error - System.NullPointerException: Attempt to de-reference a null object
* 2017-03-06   UIN              Changing execution logic from Account license to Entitlments. The logic is based around the formula field different exists
* 2017-10-09   ext_bad\ext_vos  CR# CHG0032086 - The Entitlments.BusinessHoursId won't be updated for 'Global Support Account'.
* 2017-10-09   ext_bad          CR# CHG0032086 - New Business Hours for Enterprise should be used.
* 2019-07-05   ext_bad          IT-1995   Add Subscription__c sync
* 2019-12-26    extbad          IT-2363   Added Signature Support Entitlement Process
*
* Apex to run once
EntitlementSyncBatch24h s = new EntitlementSyncBatch24h();
s.execute(null);
* Cron job to schedule the class every 10 minutes
* 
EntitlementSyncBatch24h a = new EntitlementSyncBatch24h();
String sch = '0 10 * * * ?';
String jobID = system.schedule('EntitlementSyncBatch24h Job 1', sch, a);
sch = '0 20 * * * ?';
jobID = system.schedule('EntitlementSyncBatch24h Job 2', sch, a);
sch = '0 30 * * * ?';
jobID = system.schedule('EntitlementSyncBatch24h Job 3', sch, a);
sch = '0 40 * * * ?';
jobID = system.schedule('EntitlementSyncBatch24h Job 4', sch, a);
sch = '0 50 * * * ?';
jobID = system.schedule('EntitlementSyncBatch24h Job 5', sch, a);
sch = '0 0 * * * ?';
jobID = system.schedule('EntitlementSyncBatch24h Job 6', sch, a);
**********************************************************/
global class EntitlementSyncBatch24h implements Database.Batchable<SObject>, Schedulable, Database.Stateful {
    global List<String> exception_List;

    public EntitlementSyncBatch24h(){
    }

    global void execute(SchedulableContext sc) {
        if (exception_List == null) {
            exception_List = new List<String>();
        }
        Database.executeBatch(new EntitlementSyncBatch24h()); //& default scope of 200.
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([
                SELECT Id, BusinessHoursId, Difference_Exists_Support_Office__c, Subscription__c, Account_License__c,Account_License__r.Support_From__c,
                        Account_License__r.Support_To__c,Account_License__r.Name, Account_License__r.INT_NetSuite_Support_Level__c, Account_License__r.Subscription__c,
                        Account_License__r.Account__c, Account_License__r.Account__r.Global_Support_Account__c, Account_License__r.Account__r.Premier_Support__c,
                        Account_License__r.Signature_End_Date__c
                FROM Entitlement WHERE Difference_Exists__c = TRUE LIMIT 500]);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope){
        List<Entitlement> diffEntitlements = (List<Entitlement>)scope;
        List<Entitlement> entitlementForUpd = new List<Entitlement>();
        Set<Id> accountIds = new Set<Id>();
        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
        SlaProcess standardEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name =:es.Standard_Entitlement_Process_Name__c LIMIT 1];
        SlaProcess premiumEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name =:es.Premium_Entitlement_Process_Name__c LIMIT 1];
        SlaProcess signatureEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name =:es.Signature_Entitlement_Process_Name__c LIMIT 1];
        String premiumSupportLevel = es.Premium_Support_Level__c;
        String standardSupportLevel = es.Standard_Support_Level__c;
        if (exception_List == null) {
            exception_List = new List<String>();
        }
        for (Entitlement e:diffEntitlements){
            if (e.Account_License__c != null && e.Account_License__r.Support_From__c <= e.Account_License__r.Support_To__c && e.Account_License__r.Account__c != null) {
                // Added if clause to check enddate is greater than start date
                accountIds.add(e.Account_License__r.Account__c);
                e.Name = e.Account_License__r.Name;
                e.StartDate = e.Account_License__r.Support_From__c;
                e.EndDate = e.Account_License__r.Support_To__c;
                e.Subscription__c = e.Account_License__r.Subscription__c;

                SlaProcess process;
                if ((e.Account_License__r.Signature_End_Date__c != null && e.Account_License__r.Signature_End_Date__c >= Date.today())
                        || e.Account_License__r.Account__r.Premier_Support__c) {
                    process = signatureEntitlementProcess;
                } else {
                    process = standardOrPremiumProcessId(e.Account_License__r.INT_NetSuite_Support_Level__c, premiumEntitlementProcess, standardEntitlementProcess,
                            premiumSupportLevel, standardSupportLevel);
                }
                if (process != null) {
                    e.SlaProcessId = process.Id;
                    e.SLA_Process_Name__c = process.Name;
                }
                entitlementForUpd.add(e);
            }
        }

        if (!entitlementForUpd.isEmpty()) {
            Map<Id, Support_Office__c> businessHoursToSupportOffice = new Map<Id, Support_Office__c>();
            Map<Id, Support_Office__c> accountToSupportOffice = new Map<Id, Support_Office__c>();
            Id defaultBusinessHoursId = [SELECT Id FROM BusinessHours WHERE IsDefault = TRUE LIMIT 1].Id;

            for (Account a : [SELECT a.Id, a.Support_Office__r.Business_Hours__c, a.Support_Office__r.Enterprise_Business_Hours__c FROM Account a
            WHERE Id IN :accountIds]) {
                accountToSupportOffice.put(a.Id, a.Support_Office__r);
            }

            for (Support_Office__c office : [SELECT Business_Hours__c, Enterprise_Business_Hours__c FROM Support_Office__c
            WHERE Business_Hours__c != NULL AND Enterprise_Business_Hours__c != NULL]) {
                businessHoursToSupportOffice.put(office.Business_Hours__c, office);
                businessHoursToSupportOffice.put(office.Enterprise_Business_Hours__c, office);
            }

            Support_Office__c supportOffice;
            for (Entitlement e: entitlementForUpd) {
                if (e.Account_License__r.Account__r.Global_Support_Account__c && null != e.BusinessHoursId) {
                    supportOffice = businessHoursToSupportOffice.get(e.BusinessHoursId);
                    e.BusinessHoursId = (e.Account_License__r.INT_NetSuite_Support_Level__c == premiumSupportLevel
                            && e.SlaProcessId != signatureEntitlementProcess.Id)
                            ? null != supportOffice ? supportOffice.Enterprise_Business_Hours__c : defaultBusinessHoursId
                            : null != supportOffice ? supportOffice.Business_Hours__c : defaultBusinessHoursId;

                } else {
                    supportOffice = accountToSupportOffice.get(e.Account_License__r.Account__c);
                    if (supportOffice != null) {
                        e.BusinessHoursId = (e.Account_License__r.INT_NetSuite_Support_Level__c == premiumSupportLevel
                                && e.SlaProcessId != signatureEntitlementProcess.Id)
                                ? supportOffice.Enterprise_Business_Hours__c != null ? supportOffice.Enterprise_Business_Hours__c : defaultBusinessHoursId
                                : supportOffice.Business_Hours__c != null ? supportOffice.Business_Hours__c : defaultBusinessHoursId;
                    } else {
                        e.BusinessHoursId = defaultBusinessHoursId;
                    }
                }
            }

            if (System.Test.isRunningTest()) {
                for (Integer i=0;i<202;i++) {
                    Entitlement e = new Entitlement();
                    e.Name = 'last';
                    entitlementForUpd.add(e);
                }
            }
            List<Database.SaveResult> sResults = Database.update(entitlementForUpd,false);
            for (Integer i =0; i<sResults.size();i++) {
                String msg='';
                if (!sResults[i].isSuccess()) {
                    msg += 'Product Entitlement Record with Id:'+ entitlementForUpd.get(i).Id + ' and Name: '+ entitlementForUpd.get(i).Name + 'failed due to the Error: "';
                    for(Database.Error err: sResults[i].getErrors()){
                        System.debug('There was an error when saving the record: ' + err.getMessage()) ;
                        msg += err.getMessage()+'<br/>';
                    }
                }
                if (msg!='' && msg.contains('Error:')) {
                    exception_List.add(msg);
                }
            }

            if (exception_List.size() > 500) {
                sendErrorMails(exception_List);
            }
        }
    }

    private void sendErrorMails(List<String> exceptions){
        String errMsg = 'The batch Apex job errors for Records processed till now are' +'<br/>';
        String temp = '';
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'ullalramakrishna.kini@qlik.com','ain@qlik.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Lead Email Update Batch Job Status In-Progress');
        if (!exceptions.isEmpty()) {
            for (String str:exceptions) {
                temp = temp + str;
            }
            mail.setHtmlBody(errMsg + temp);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            exception_List = new List<String>();
        }
    }
    // Returning standardEntitlementProcess as default (2013-04-15 EXT_YKA: CR#7833)
    private static SlaProcess standardOrPremiumProcessId(String supportLevel, SlaProcess premiumEntitlementProcess, SlaProcess standardEntitlementProcess, String premiumSupportLevel, String standardSupportLevel) {
        return supportLevel == premiumSupportLevel ? premiumEntitlementProcess : standardEntitlementProcess;
    }

    global void finish(Database.BatchableContext BC){
        if (exception_List == null) { // Fix for null pointer emails
            exception_List = new List<String>();
        }
        if (exception_List.size() > 0) {
            sendErrorMails(exception_List);
        }
    }

}