/*****************************************************************************************
* 2015-09-28 AIN Webservice Mock for class sharinghandlerQlikviewCom, using WebServiceMockDispatcher is prefered, 
* see that class for details.
****************************************************************************************/
@isTest
public class sharinghandlerMock implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

    system.debug('sharinghandlerMock Start');
    if(request instanceof sharinghandlerQlikviewCom.OpportunitySellThroughPartner_element)
    {
      system.debug('instanceof sharinghandlerQlikviewCom.OpportunitySellThroughPartner');
      response.put('response_x', new sharinghandlerQlikviewCom.OpportunitySellThroughPartnerResponse_element());
    } else if(request instanceof sharinghandlerQlikviewCom.PartnerLeadShareWithCreaterAfterSubmission_element){
      system.debug('instanceof sharinghandlerQlikviewCom.PartnerLeadShareWithCreaterAfterSubmission');
       response.put('response_x', new sharinghandlerQlikviewCom.PartnerLeadShareWithCreaterAfterSubmissionResponse_element());
    }
    else if(request instanceof sharinghandlerQlikviewCom.CaseSharingOnAccountOrigin_element){
      system.debug('instanceof sharinghandlerQlikviewCom.CaseSharingOnAccountOrigin');
       response.put('response_x', new sharinghandlerQlikviewCom.CaseSharingOnAccountOriginResponse_element());
    }
    system.debug('sharinghandlerMock End');
  }
}