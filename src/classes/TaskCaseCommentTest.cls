@isTest
private class TaskCaseCommentTest {
    
    static testMethod void testTaskCasecommentTrigger() {
        List<Recordtype> rtypes = [SELECT Id FROM RecordType WHERE Name = 'Support Tasks'];
        if (rtypes.size() <= 0) return;
        
        Case c = new Case();
        insert c;
        
        String description = 'Some descrption.';
        Task t = new Task(RecordTypeId = rtypes[0].Id, WhatId = c.Id, Description = description, Subject = 'Internal', Add_as_Case_Comment__c = true);
        insert t;
        t = [SELECT Add_as_Case_Comment__c FROM Task WHERE Id = :t.Id];     
        
        //check that a casecomment was created with the same description
        List<CaseComment> comments = [SELECT CommentBody FROM CaseComment WHERE ParentId = :c.Id];
        System.assertEquals(description, comments[0].CommentBody);
    }
}