/***************************************************************************************************************************************

    Changelog:
        2012-02-16  CCE     Added a test Account to set QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
        2013-04-25  CCE     CR# 7922 - Rework of SoE to Multi-edit form
                            https://eu1.salesforce.com/a0CD000000YFMVz.
                            Changes made to include 2 new fields in the query select: Event_Charge__c and Customer_Agreed__c
        2015-04-20  SLH     Added function and solution area for OP006 validation rule   
                                                
****************************************************************************************************************************************/

public with sharing class SoEUpdate_Controller {
    
    public Opportunity opportunity { get; set; }
    public Sequence_of_Event__c NewSoE { get; set; }
    public string ErrorMessage { get; set; }
    private ApexPages.StandardController Controller;

    public SoEUpdate_Controller(ApexPages.StandardController stdController)
    {
        Controller = stdController;
        opportunity = (Opportunity) Controller.getRecord();
        NewSoE = new Sequence_of_Event__c();            
        NewSoE.Opportunity_ID__c = opportunity.Id;
        ErrorMessage = '';
    }

    public List<Sequence_of_Event__c> getSoERelatedList()
    {
        return [select Id, Name, CheckPoint__c, Cost__c, Description__c, Opportunity_ID__c, Proposed_Event__c, Responsibility__c, S_o_E_Date__c, Type__c, CurrencyIsoCode, Event_Charge__c, Customer_Agreed__c from Sequence_of_Event__c where Opportunity_ID__c = :opportunity.Id ORDER BY LastModifiedDate DESC LIMIT 10];    
    }

    public PageReference CreateNew()
    {
        ErrorMessage = '';
        
        try
        {
            insert NewSoE;
            NewSoE = new Sequence_of_Event__c();
            NewSoE.Opportunity_ID__c = opportunity.Id;

        }
        catch (System.Exception Ex)
        {
            ErrorMessage = 'Could not save Sequence of Events: ' + Ex.getMessage();
        }   

        return System.currentPageReference();
    }   
    
    public PageReference DeleteSoE()
    {
        ErrorMessage = '';

        if (System.currentPagereference().getParameters().get('SoEid') == '')
        {
            return System.currentPageReference();   
        }

        try
        {       
            ID SoEid = System.currentPagereference().getParameters().get('SoEid');
            delete [select Id from Sequence_of_Event__c where Id = :SoEid];
        }
        catch (System.Exception Ex)
        {
            ErrorMessage = 'Could not delete Sequence of Events: ' + Ex.getMessage();
        }       
        return System.currentPageReference();
    }   
    
    public PageReference EditSoE()
    {
        ErrorMessage = '';
        
        if (System.currentPagereference().getParameters().get('SoEid') == '')
        {
            return System.currentPageReference();   
        }
        

        try
        {
            ID SoEid = System.currentPagereference().getParameters().get('SoEid');
            Sequence_of_Event__c SoE = [select Id from Sequence_of_Event__c where Id = :SoEid LIMIT 1];
            ApexPages.StandardController SoEController = new ApexPages.StandardController(SoE);
            
            PageReference EditRef = SoEController.edit();
            EditRef.getParameters().put('retURL', '/'+opportunity.Id);
            return EditRef;
        }
        catch (System.Exception Ex)
        {
            System.debug('EditSoE cought exception: ' + Ex.getMessage());
        }
        
        return System.currentPageReference();
    }
    
}