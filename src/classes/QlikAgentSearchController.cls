public with sharing class QlikAgentSearchController {
    private id CaseId;
    private String CaseNumber;
    private Map<String, String> contextInfo = new Map<String, String>();

    public id getCaseId(){ return CaseId; }
    public String getCaseNumber(){ return CaseNumber; }
    public String getContextInfo(){ return JSON.serialize(contextInfo); }

    public QlikAgentSearchController() {
        if(ApexPages.currentPage().getParameters().get('caseId') != null) {
          CaseId = ApexPages.currentPage().getParameters().get('caseId');
          list<Case> caseList = [SELECT CaseNumber, Subject, Status, LastModifiedBy.name, LastModifiedDate, SuppliedName, CreatedBy.name, CreatedDate FROM Case WHERE Id = :CaseId ];
          Case c = (caseList != null && caseList.size()>1) ? caseList[0] : null;
          if (c != null) {
              CaseNumber = c.CaseNumber;

              contextInfo.put('Status', c.Status);
              contextInfo.put('Last Modified By', c.LastModifiedBy.name + ', ' + c.LastModifiedDate);
              contextInfo.put('Created By', c.CreatedBy.name + ', ' + c.CreatedDate);
              contextInfo.put('Subject', c.Subject);
          }
        }
    }
}