/*
@author: Anthony Victorio, Model Metrics
@date: 02/21/2012
@description: Controller for the QVM "view all cases" page
*/
public with sharing class QVM_Cases {
    
    private QVM_Settings__c settings {get; set;}
    public QVM_Table cases { get; private set; }
    
    public list<Case> caseList { get { return cases.rows; }}
    
    public QVM_Cases() {
        
        cases = new QVM_Table(new List<QVM_Table.Column> {
            new QVM_Table.Column(Case.Subject.getDescribe().getLabel(), 'Subject', true),
            new QVM_Table.Column(Case.Status.getDescribe().getLabel(), 'Status', true),
            new QVM_Table.Column(Case.Priority.getDescribe().getLabel(), 'Priority', true),
            new QVM_Table.Column(Case.Type.getDescribe().getLabel(), 'Type', true),
            new QVM_Table.Column(System.Label.QVM_Created_By, 'CreatedBy.Name', true),
            new QVM_Table.Column(Case.CreatedDate.getDescribe().getLabel(), 'CreatedDate', true)
        },'CreatedDate');
        
        settings = QVM_Settings__c.getOrgDefaults();
        cases.customRowsToDisplay = settings.Pagination_After_Row__c.intValue();
        this.getTableData();
        
    }
    
    private void getTableData() {
        String queryString = 'select Id, Subject, Status, Priority, Type, CreatedBy.Name, ' +
        'CreatedDate from Case where ContactId = \'' + QVM.getUserContactId() + '\' and ' +
        'RecordType.Name = \'' + settings.Default_Case_Record_Type__c + '\'';
        
        cases.populate(queryString);
    }
    
    public void sort() {
        cases.sort();
        this.getTableData();
    }
    
    public PageReference goToQVMControlPanel() {
        PageReference p;
        p = Page.QVM_ControlPanel;
        return p;
    }
    
    public PageReference createNewCase() {
        PageReference p = Page.QVM_CreateCase;
        return p;
    }

}