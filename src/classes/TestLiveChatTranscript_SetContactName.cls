/*****************************************************************************************************************
 Change Log:
 20130402  SLH CR 7005 - Capture name and email address in Chat transcripts 

 3-7-2016 TTG CR - Updated Email__c fields to new Customer_Email__c field to get full coverage

******************************************************************************************************************/
@isTest
private class TestLiveChatTranscript_SetContactName {

    
   static testMethod void test_LiveChatTranscript_SetContactName()
  { 
    LiveChatVisitor lcv1 = new LiveChatVisitor();
    
    Contact contact1 = new Contact(  LastName='LastTest1', 
              FirstName='FirstTest1',
              Email='test1@test1.com'); 
    insert lcv1;          
    lcv1 = [select Id, Name from LiveChatVisitor where SessionKey = :lcv1.SessionKey];
    LiveChatTranscript lct1 = new LiveChatTranscript(  Status='Completed', 
              Customer_Email__c='test1@test1.com',
              LiveChatVisitorId = lcv1.Id);
  
    
    insert contact1;
    insert lct1;
    

    contact1 = [select Id, email from Contact where Id = :contact1.Id];
    lct1 = [select Id, Customer_Email__c from LiveChatTranscript where Id = :lct1.Id];
        
    test.startTest();

    System.assertEquals(contact1.email, lct1.Customer_Email__c);
    
    test.stopTest();    
    }
}