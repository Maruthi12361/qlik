/**
* DisableChatterNotifications
*
* Description:	Disable all Chatter notifications for External users.
*
* Added: 		04-01-2019 - ext_vos, extbad - CHG0030387
*
* Change log:
*
*/

global class DisableChatterNotifications implements Database.Batchable<sObject>, Database.Stateful {

    global final static String CUSTOMER_PROFILES_STARTS = 'Customer Portal';
    global final static String EVENTFORCE_PROFILES_STARTS = 'Eventforce Portal';
    global final static String EVENTFORCE_PROFILES_NON_CONTAINS = 'Employee';
    global final static String PARTNER_PROFILE_STARTS = 'PRM';
    global final static String PARTNER_PROFILE_STARTS_PSE = 'PSE - PRM';
    global final static String PARTNER_PROFILE_NON_CONTAINS_OLD = 'OLD';
    global final static String PARTNER_PROFILE_NON_CONTAINS_READ = 'READ ONLY';
    global String query;

    global DisableChatterNotifications() {
        Boolean isSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        query = 'select Id, UserPreferencesDisableAllFeedsEmail from User '
                + ' where isActive = true '
                + ( !isSandbox ? ' and UserPreferencesDisableAllFeedsEmail = false ' : '')
                + ' and ContactId != null'
                + ' and (Profile.Name like \'' + CUSTOMER_PROFILES_STARTS + '%\' '
                + ' or (Profile.Name like \'' + EVENTFORCE_PROFILES_STARTS + '%\' and (NOT Profile.Name like \'%' + EVENTFORCE_PROFILES_NON_CONTAINS + '%\')) '
                + ' or ((Profile.Name like \'' + PARTNER_PROFILE_STARTS + '%\' or Profile.Name like \'' + PARTNER_PROFILE_STARTS_PSE + '%\')'
                + ' and (NOT Profile.Name like \'%' + PARTNER_PROFILE_NON_CONTAINS_OLD + '%\') and (NOT Profile.Name like \'%' + PARTNER_PROFILE_NON_CONTAINS_READ + '%\'))'
                + ')';
        if (Test.isRunningTest()) {
            query = 'select Id, UserPreferencesDisableAllFeedsEmail from User where FirstName = \'userDisableChatter\'';
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<User> records) {
        List<String> userIds = new List<String>();
        List<User> usersToUpdate = new List<User>();
        for (User us : records) {
            if (!us.UserPreferencesDisableAllFeedsEmail) {
                us.UserPreferencesDisableAllFeedsEmail = true;
                usersToUpdate.add(us);
            }
            userIds.add(us.Id);
        }
        update usersToUpdate;

        List<NetworkMember> networkMembers = [
                SELECT Id, PreferencesDisableAllFeedsEmail
                FROM NetworkMember
                WHERE MemberId IN :userIds
        ];

        List<NetworkMember> nmToUpdate = new List<NetworkMember>();
        for (NetworkMember nm : networkMembers) {
            if (!nm.PreferencesDisableAllFeedsEmail) {
                nm.PreferencesDisableAllFeedsEmail = true;
                nmToUpdate.add(nm);
            }
        }
        update nmToUpdate;
    }

    global void finish(Database.BatchableContext BC) {
    }
}