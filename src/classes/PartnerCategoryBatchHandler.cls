/**     * File Name: PartnerCategoryBatch
        * Description : This apex batch handler class is for updating partner category staus fields(2 lookup fields)
        * @author : Rodion Vakulovskyi
        * Jira Id : QCW-3903
        * Modification Log :
        
        Ver     Date         Author            
        1      18/09/2017  Rodion Vakulovskyi 
        2      04/10/2017  Rodion Vakulovskyi 
     
*/
public class PartnerCategoryBatchHandler {
    public static void processBatch(List<SBQQ__Quote__c> inputList) {
        String errorText = 'The Partner Category & Status options on the Sell Through Partner Account do not match this deal type. Please contact Sales Operations to review this Partner\'s classifications before proceeding';
        String errorText2 = 'The Partner Category & Status options on the Second  Partner Account do not match this deal type. Please contact Sales Operations to review this Partner\'s classifications before proceeding';
        String errorTextOEM = 'The Partner Category & Status options on the OEM Account do not match this deal type. Please contact Sales Operations to review this Partner\'s classifications before proceeding';
        List<id> stpPartner = new List<Id>();        
        List<System_Message__c> listOfSystemMessages = new List<System_Message__c>();
        Map<Id,List<Partner_Category_Status__c>> stpPartnerCategoryStatusMap = new Map<Id,List<Partner_Category_Status__c>>();
        Map<Id,List<Partner_Category_Status__c>> secondPartnerCategoryStatusMap = new Map<Id,List<Partner_Category_Status__c>>();
        Map<Id,List<Partner_Category_Status__c>> accountPartnerCategoryStatusMap = new Map<Id,List<Partner_Category_Status__c>>();
        Map<Id,String> mapAccountIdVsRecordType = new Map<Id,String>();
        for(SBQQ__Quote__c thisQuote:inputList){   
            //thisQuote.Bypass_Rules__c = true;     
            stpPartner.add(thisQuote.Sell_Through_Partner__c);
            stpPartner.add(thisQuote.Second_Partner__c);
            stpPartner.add(thisQuote.SBQQ__Account__c);
            if(stpPartnerCategoryStatusMap.get(thisQuote.Sell_Through_Partner__c)==null){
                stpPartnerCategoryStatusMap.put(thisQuote.Sell_Through_Partner__c,new List<Partner_Category_Status__c>());
            }
            if(secondPartnerCategoryStatusMap.get(thisQuote.Second_Partner__c)==null){
                secondPartnerCategoryStatusMap.put(thisQuote.Second_Partner__c,new List<Partner_Category_Status__c>());
            }
            if(accountPartnerCategoryStatusMap.get(thisQuote.SBQQ__Account__c)==null){
                accountPartnerCategoryStatusMap.put(thisQuote.SBQQ__Account__c,new List<Partner_Category_Status__c>());
            }
            mapAccountIdVsRecordType.put(thisQuote.SBQQ__Account__c,thisQuote.SBQQ__Account__r.RecordType.Name);
        }
        List<Partner_Category_Status__c> stpPartnerCategoryStatusList = new List<Partner_Category_Status__c>([SELECT id,Program_Version__c,Partner_Category__c,Partner_Account__c FROM Partner_Category_Status__c WHERE Partner_Account__c IN :stpPartner AND Partner_Level_Status__c = 'Active']);                                       
        for(Partner_Category_Status__c pcs:stpPartnerCategoryStatusList){
            if(stpPartnerCategoryStatusMap.get(pcs.Partner_Account__c)!=null){
                stpPartnerCategoryStatusMap.get(pcs.Partner_Account__c).add(pcs);
            }
            if(secondPartnerCategoryStatusMap.get(pcs.Partner_Account__c)!=null){
                secondPartnerCategoryStatusMap.get(pcs.Partner_Account__c).add(pcs);
            }
            if(accountPartnerCategoryStatusMap.get(pcs.Partner_Account__c)!=null){
                accountPartnerCategoryStatusMap.get(pcs.Partner_Account__c).add(pcs);
            }
        }
        for(SBQQ__Quote__c thisQuote:inputList){            
            List<Partner_Category_Status__c> pcsListTemp = stpPartnerCategoryStatusMap.get(thisQuote.Sell_Through_Partner__c);       
            List<Partner_Category_Status__c> secondPartnerListTemp = secondPartnerCategoryStatusMap.get(thisQuote.Second_Partner__c);
            List<Partner_Category_Status__c> accountPartnerListTemp = accountPartnerCategoryStatusMap.get(thisQuote.SBQQ__Account__c);
            Partner_Category_Status__c selectedSecondPCS = new Partner_Category_Status__c();
            Partner_Category_Status__c selectedPCS = new Partner_Category_Status__c();
            Partner_Category_Status__c selectedAccntPCS = new Partner_Category_Status__c();
            Partner_Category_Status__c selectedAccntPCSOEM = new Partner_Category_Status__c();
            Integer counter = 0;
            if(thisQuote.Revenue_Type__c == 'Distributor'){                
                // First Partner //
                if(thisQuote.Sell_Through_Partner__c!=null){
                    for(Partner_Category_Status__c pcsTemp:pcsListTemp){
                        if(pcsTemp.Program_Version__c == 'Distributor'){
                            
                            selectedPCS = pcsTemp;
                            counter++;
                        }
                    }
                    if(counter==1){
                        thisQuote.PartnerCategoryStatusSTP__c = selectedPCS.Id;
                    }
                    else{
                        //throw error - none or multiple matches found
                        listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText));
                    }
                }
                // First Partner //
                // Second Partner //
                if(thisQuote.Second_Partner__c!=null){
                    counter = 0;
                    for(Partner_Category_Status__c secondPcsTemp:secondPartnerListTemp){
                        if(secondPcsTemp.Program_Version__c == 'QPP' && secondPcsTemp.Partner_Category__c == 'Resell'){
                            selectedSecondPCS = secondPcsTemp;
                            counter++;
                        }
                    }
                    if(counter==1){
                        thisQuote.PartnerCategoryStatusSecond__c = selectedSecondPCS.Id;
                    }
                    else if(counter>1){
                        //throw error - multiple records meeting criteria
                        listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText2));
                    }
                    else{
                        // counter should be 0 here
                        counter = 0;
                        for(Partner_Category_Status__c secondPcsTemp:secondPartnerListTemp){ 
                            if(secondPcsTemp.Program_Version__c == 'Partner Program 4.1'){
                                selectedSecondPCS = secondPcsTemp;
                                counter++;
                            }
                        }
                        if(counter==1){
                            thisQuote.PartnerCategoryStatusSecond__c = selectedSecondPCS.Id;
                        }
                        else if(counter>1){
                            //throw error - multiple records meeting criteria
                            listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText2));
                        }
                        else{
                            //no records met criteria
                            counter = 0;
                            for(Partner_Category_Status__c secondPcsTemp:secondPartnerListTemp){
                                if(secondPcsTemp.Program_Version__c == 'Master Reseller'){
                                    selectedSecondPCS = secondPcsTemp;
                                    counter++;
                                }
                            }
                            if(counter==1){
                                thisQuote.PartnerCategoryStatusSecond__c = selectedSecondPCS.Id;
                            }
                            else{
                                //throw error - none or multiple match criteria
                                listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText2));
                            }
                        }
                    }
                }    
                // Second Partner //
            }// End of Distributor
            else if(thisQuote.Revenue_Type__c == 'MSP'){
                if(accountPartnerListTemp.size()==0){
                    //QCW-3755
                    if( mapAccountIdVsRecordType != NULL && 
                        mapAccountIdVsRecordType.containsKey(thisQuote.SBQQ__Account__c) && 
                        mapAccountIdVsRecordType.get(thisQuote.SBQQ__Account__c) != 'Partner Account'){
                            createSystemMessageForError(thisQuote.Id, 'Please select only Partner Account for MSP Quote');
                    }else{
                        //throw error
                        listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText));
                    }
                }
                else{
                    counter = 0;
                    for(Partner_Category_Status__c accntPcsTemp:accountPartnerListTemp){
                        if(accntPcsTemp.Program_Version__c == 'QPP' && accntPcsTemp.Partner_Category__c == 'MSP'){
                            selectedAccntPCS = accntPcsTemp;
                            counter++;
                        }
                    }
                    if(counter == 1){
                        thisQuote.PartnerCategoryStatusSTP__c = selectedAccntPCS.Id;
                    }
                    else{
                        //throw error
                        listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText));
                    }
                }           
            } // end of MSP
            else if( thisQuote.Revenue_Type__c == 'OEM' || thisQuote.Revenue_Type__c == 'OEM Recruitment'){
                  if( thisQuote.SBQQ__Account__c!=null){
                    counter=0;
                     for( Partner_Category_Status__c pcsOEM:accountPartnerListTemp){
                          if(pcsOEM.Program_Version__c == 'OEM'){
                            selectedAccntPCSOEM =pcsOEM;
                            counter++; 
                       }
                     } 
                     if( counter==1){ //single match found- set PCS in Quote
                        thisQuote.PartnerCategoryStatusSTP__c = selectedAccntPCSOEM.Id;
                     }
                     else{ //no record or multiple records meet criteria- throw error
                         listOfSystemMessages.add(createSystemMessageForError(thisQuote.ID, errorTextOEM));
                     }
                }  
            }  // end of OEM           
            else{
                // First Partner //
                if(thisQuote.Sell_Through_Partner__c!=null){
                    counter = 0;
                    for(Partner_Category_Status__c pcsTemp:pcsListTemp){
                        if(pcsTemp.Program_Version__c == 'QPP' && pcsTemp.Partner_Category__c == 'Resell'){
                            selectedPCS = pcsTemp;
                            counter++;
                        }
                    }
                    if(counter==1){
                        thisQuote.PartnerCategoryStatusSTP__c = selectedPCS.Id;
                    }
                    else if(counter>1){
                        //throw error
                        listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText));
                    }
                    else{
                        counter = 0;
                        for(Partner_Category_Status__c pcsTemp:pcsListTemp){
                            if(pcsTemp.Program_Version__c == 'Partner Program 4.1'){
                                selectedPCS = pcsTemp;
                                counter++;
                            }
                        }
                        if(counter==1){
                            thisQuote.PartnerCategoryStatusSTP__c = selectedPCS.Id;
                        }
                        else if(counter>1){
                            //throw error
                            listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText));
                        }
                        else{                   
                            counter = 0;
                            for(Partner_Category_Status__c pcsTemp:pcsListTemp){
                                if(pcsTemp.Program_Version__c == 'Master Reseller'){
                                    selectedPCS = pcsTemp;
                                    counter++;
                                }
                            }
                            if(counter==1){
                                thisQuote.PartnerCategoryStatusSTP__c = selectedPCS.Id;
                            }
                            else{
                                //throw error
                                listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText));
                            }
                        }
                    }
                }
                // First Partner else ends//
                if(thisQuote.Second_Partner__c!=null){
                // Second Partner //
                    counter = 0;
                    for(Partner_Category_Status__c secondPcsTemp:secondPartnerListTemp){
                        if(secondPcsTemp.Program_Version__c == 'QPP' && secondPcsTemp.Partner_Category__c == 'Resell'){
                            selectedSecondPCS = secondPcsTemp;
                            counter++;
                        }
                    }
                    if(counter==1){
                        thisQuote.PartnerCategoryStatusSecond__c = selectedSecondPCS.Id;
                    }
                    else if(counter>1){
                        //throw error
                        listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText2));
                    }
                    else{
                        counter = 0;
                        for(Partner_Category_Status__c secondPcsTemp:secondPartnerListTemp){

                            if(secondPcsTemp.Program_Version__c == 'Partner Program 4.1'){
                                selectedSecondPCS = secondPcsTemp;
                                counter++;
                            }
                        }
                        if(counter==1){
                            thisQuote.PartnerCategoryStatusSecond__c = selectedSecondPCS.Id;
                        }
                        else if(counter>1){
                            //throw error
                            listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText2));
                        }
                        else{                   
                            counter = 0;
                            for(Partner_Category_Status__c secondPcsTemp:secondPartnerListTemp){
                                if(secondPcsTemp.Program_Version__c == 'Master Reseller'){
                                    selectedSecondPCS = secondPcsTemp;
                                    counter++;
                                }
                            }
                            if(counter==1){
                                thisQuote.PartnerCategoryStatusSecond__c = selectedSecondPCS.Id;
                            }
                            else{
                                //throw error
                                listOfSystemMessages.add(createSystemMessageForError(thisQuote.Id, errorText2));
                            }
                        }
                    }
                }
            }
        }
        Database.SaveResult[] saveResultList = Database.update(inputList, false);
        for(Database.SaveResult itemSaveResult : saveResultList) {
           if(!itemSaveResult.isSuccess()){
               listOfSystemMessages.add(createSystemMessageForError(itemSaveResult.getId(), itemSaveResult.getErrors()));
           } else {
               listOfSystemMessages.add(createSystemMessageForError(itemSaveResult.getId(), 'Partner Category update : Success'));
           }
        }
        upsert listOfSystemMessages;
    }

    private static System_Message__c createSystemMessageForError(Id quoteId, String errorText) {
        System_Message__c sysMessageToReturn = new System_Message__c(
                                                Quote__c = quoteId,
                                                Error_Message__c = errorText
                                                );
        return sysMessageToReturn;
    }

    private static System_Message__c createSystemMessageForError(Id quoteId, Database.Error[] errorList) {
        System_Message__c sysMessageToReturn = new System_Message__c(Quote__c = quoteId);
        for(Database.Error errorItem : errorList) {                
            sysMessageToReturn.Error_Message__c+= errorItem.getStatusCode() + ': ' + errorItem.getMessage() + ' \n';
        }
        return sysMessageToReturn;
    }
}