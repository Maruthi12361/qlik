/******************************************************

	        ProductMultiSelectHandler
	    
	        Changelog:
	            07.02.2017   RVA :   changing methods	                
	    ******************************************************/
	public with sharing class ProductMultiSelectHandler {
		public static void onAfterUpdate(List<Product2> inputList, Map<Id, Product2> oldMap) {

		}

		public static void onBeforeUpdate(List<Product2> inputList, Map<Id, Product2> oldMap) {
			system.debug('ProductMultiSelectHandler onBeforeUpdate start');
			List<Product2> listForMethod = new List<Product2>();
			for(Product2 item : inputList) {
				if((item.Visibility_Included_Countries__c != oldMap.get(item.id).Visibility_Included_Countries__c) || 
					(item.Visibility_Excluded_Countries__c != oldMap.get(item.id).Visibility_Excluded_Countries__c) ||
					(item.Visibility_Countries_Recalculate__c != oldMap.get(item.id).Visibility_Countries_Recalculate__c)) {
					listForMethod.add(item);
				}
			}
	        System.debug(listForMethod);
			if(listForMethod.size() > 0) {
				processProductPicklistValues(listForMethod);
			}
			system.debug('ProductMultiSelectHandler onBeforeUpdate end');
		}

		public static void onBeforeInsert(List<Product2> inputList) {
			processProductPicklistValues(inputList);
		}

		public static void onAfterInsert(List<Product2> inputList) {
			
		}

		private static List<String>  retrievePicklistValues(Schema.DescribeFieldResult inputField) {
			List<String> listForValues = new List<String>();
			for(Schema.Picklistentry picklistEntry : inputField.getPicklistValues()) {
				listForValues.add(pickListEntry.getValue());
			}
			return listForValues;
		}

		private static void processProductPicklistValues(List<Product2> inputList) {
			system.debug('ProductMultiSelectHandler processProductPicklistValues start');
			Map<String, String> mapOfCountriesAbbr = retrieveQlikTechCountryMapping();
			for(Product2 itemProduct : inputList) {
				itemProduct.Visibility_Countries_Recalculate__c = false;
	            itemProduct.Visibility_Countries_A_I__c = null;
	            itemProduct.Visibility_Countries_J_R__c = null;
	            itemProduct.Visibility_Countries_S_Z__c = null;
				itemProduct.Visibility_Countries_V_Z__c = null;
				String countryTextAF = '';
				String countryTextGM = '';
				String countryTextNS = '';
				String countryTextTZ = '';
				if(!String.isEmpty(itemProduct.Visibility_Included_Countries__c) && String.isEmpty(itemProduct.Visibility_Excluded_Countries__c)) {
	                System.debug('empty');
					for(String itemPicklist1 : itemProduct.Visibility_Included_Countries__c.split(';')) {
						if(mapOfCountriesAbbr.containsKey(itemPicklist1)) {
								if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist1)) && locatePlace(mapOfCountriesAbbr.get(itemPicklist1), 'A', 'F')){
									countryTextAF+=mapOfCountriesAbbr.get(itemPicklist1)+',';
								}
								if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist1)) && locatePlace(mapOfCountriesAbbr.get(itemPicklist1), 'G', 'M')){
									countryTextGM+=mapOfCountriesAbbr.get(itemPicklist1)+',';
								}
								if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist1)) && locatePlace(mapOfCountriesAbbr.get(itemPicklist1), 'N', 'S')){
									countryTextNS+=mapOfCountriesAbbr.get(itemPicklist1)+',';
								}
								if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist1)) && locatePlace(mapOfCountriesAbbr.get(itemPicklist1), 'T', 'Z')){
									countryTextTZ+=mapOfCountriesAbbr.get(itemPicklist1)+',';
								}
						}
					}
				} if(!String.isEmpty(itemProduct.Visibility_Excluded_Countries__c) && String.isEmpty(itemProduct.Visibility_Included_Countries__c)) {
					List<String> allPicklIstValues = retrievePicklistValues(Product2.Visibility_Excluded_Countries__c.getDescribe());
	                Set<String> setOfExcludedValues = new Set<String>();
					for(String itemStringGeneral : itemProduct.Visibility_Excluded_Countries__c.split(';')) {
						setOfExcludedValues.add(itemStringGeneral);
					}
	                for(Integer i = 0; i < allPicklIstValues.size(); i ++) {
	                    if(!setOfExcludedValues.contains(allPicklIstValues[i])) {
	                    	if(mapOfCountriesAbbr.containsKey(allPicklIstValues[i])) {
	                    		if(!String.isEmpty(mapOfCountriesAbbr.get(allPicklIstValues[i])) && locatePlace(mapOfCountriesAbbr.get(allPicklIstValues[i]), 'A', 'F')){
									countryTextAF+=mapOfCountriesAbbr.get(allPicklIstValues[i])+',';
								}
								if(!String.isEmpty(mapOfCountriesAbbr.get(allPicklIstValues[i])) && locatePlace(mapOfCountriesAbbr.get(allPicklIstValues[i]), 'G', 'M')){
									countryTextGM+=mapOfCountriesAbbr.get(allPicklIstValues[i])+',';
								}
								if(!String.isEmpty(mapOfCountriesAbbr.get(allPicklIstValues[i])) && locatePlace(mapOfCountriesAbbr.get(allPicklIstValues[i]), 'N', 'S')){
									countryTextNS+=mapOfCountriesAbbr.get(allPicklIstValues[i])+',';
								}
								if(!String.isEmpty(mapOfCountriesAbbr.get(allPicklIstValues[i])) && locatePlace(mapOfCountriesAbbr.get(allPicklIstValues[i]), 'T', 'Z')){
									countryTextTZ+=mapOfCountriesAbbr.get(allPicklIstValues[i])+',';
								}
	                    	}
	                    }
	                }
				} 
				if(String.isEmpty(itemProduct.Visibility_Excluded_Countries__c) && String.isEmpty(itemProduct.Visibility_Included_Countries__c)){
					List<String> allPicklIstValues = retrievePicklistValues(Product2.Visibility_Included_Countries__c.getDescribe());
					system.debug(allPicklIstValues);
					for(String itemPicklist : allPicklIstValues) {
						if(mapOfCountriesAbbr.containsKey(itemPicklist)) {
							if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist))&& locatePlace(mapOfCountriesAbbr.get(itemPicklist), 'A', 'F')){
								countryTextAF+=mapOfCountriesAbbr.get(itemPicklist)+',';
							}
							if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist)) && locatePlace(mapOfCountriesAbbr.get(itemPicklist), 'G', 'M')){
								countryTextGM+=mapOfCountriesAbbr.get(itemPicklist)+',';
							}
							if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist)) && locatePlace(mapOfCountriesAbbr.get(itemPicklist), 'N', 'S')){
								countryTextNS+=mapOfCountriesAbbr.get(itemPicklist)+',';
							}
							if(!String.isEmpty(mapOfCountriesAbbr.get(itemPicklist)) && locatePlace(mapOfCountriesAbbr.get(itemPicklist), 'T', 'Z')){
								countryTextTZ+=mapOfCountriesAbbr.get(itemPicklist)+',';
							}
						}
					}
				}
	            if(!String.isEmpty(countryTextAF)) {
	                System.debug('not empty');
					itemProduct.Visibility_Countries_A_I__c = countryTextAF;
	            }
	            if(!String.isEmpty(countryTextGM)) {
					itemProduct.Visibility_Countries_J_R__c = countryTextGM;
	            }
	            if(!String.isEmpty(countryTextNS)) {
					itemProduct.Visibility_Countries_S_Z__c = countryTextNS;
	            }
				if(!String.isEmpty(countryTextTZ)) {
					itemProduct.Visibility_Countries_V_Z__c = countryTextTZ;
	            }
			}
			system.debug('ProductMultiSelectHandler processProductPicklistValues end');
		}
		
		private static Map<String, String> retrieveQlikTechCountryMapping() {
			Map<String, String> mapOfCountry = new Map<String, String>();
			for(QlikTech_Company__c item : [Select id , Country_Name__c, NS_Country_Name__c, Country_Code_Two_Letter__c From QlikTech_Company__c]) {
				mapOfCountry.put(item.NS_Country_Name__c, item.Country_Code_Two_Letter__c);
			}
	        if(mapOfCountry.containsKey('USA')) {
	        	mapOfCountry.put('United States', 'US');
	        }
		return mapOfCountry;
		}
		
		private static Boolean locatePlace(String inputString, String beginInterval, String endInterval) {
		Boolean flag = false;
			if(inputString.getChars()[0] >= beginInterval.getChars()[0] && inputString.getChars()[0] <= endInterval.getChars()[0]){
				flag = true;
			}
		return flag;
		}


	}
	//'I', 'Q'
	//'A', 'H'
	//'R', 'Z'