@isTest
public class WebActivityRecordDownload_Test {
    @testSetup static void setup() {

        Semaphores.SetAllSemaphoresToTrue();
        List<QlikTech_Company__c> QlikCompanies = new List<QlikTech_Company__c>();
        SIC_Code__c code = new SIC_Code__c(Name ='aaa', Industry__c='bbbb');
        insert code;
        
        QlikTech_Company__c qc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
            qc.Country_Name__c = 'Sweden';
            qc.QlikTech_Region__c = 'NORDIC';
            qc.QlikTech_Sub_Region__c = 'NORDIC';
            qc.QlikTech_Operating_Region__c = 'N EUROPE';
            qc.Language__c = 'Swedish';
        QlikCompanies.add(qc);
        upsert QlikCompanies;

        HardcodedValuesQ2CW__c setting = new HardcodedValuesQ2CW__c();
        setting.Name = 'Default';
        setting.CM_SourceURL_field_Access__c = '005D0000005tXuN,005D0000005tXuM';
        insert setting;
        
        List<Account> Accounts = new List<Account>();
        Account a = new Account();
        a.Name = 'The Test Customer';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Test Partner';
        a.Navision_Status__c = 'Partner';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Test Customer 2';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        a = new Account();
        a.Name = 'The Test Customer Dup 1';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'Sweden';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        insert Accounts;
        Id AccId = Accounts[0].id;

        List<Contact> contacts = QTTESTUtils.createMockContacts(AccId,4);
        
        List<Lead> Leads = new List<Lead>();
        Lead l = new Lead();
        l.FirstName = 'First';
        l.LastName = 'Last';
        l.Email = 'first.last@lead.com';
        l.Country_Code__c = QlikCompanies[0].Id;
        l.Country_Name__c = QlikCompanies[0].Id;
        l.Country = 'Sweden';
        l.Company = 'The Lead';
        Leads.add(l);

        Lead l1 = new Lead();
        l1.FirstName = 'FirstTest';
        l1.LastName = 'LastTest';
        l1.Email = 'first.last2@lead.com';
        l1.Country_Code__c = QlikCompanies[0].Id;
        l1.Country_Name__c = QlikCompanies[0].Id;
        l1.Country = 'Sweden';
        l1.Company = 'The Lead';
        Leads.add(l1);
        insert Leads;
        
        
        List<Campaign> Campaigns = new List<Campaign>();
        Campaign cam = new Campaign();
        cam.Name = 'My Campaign';
        Campaigns.add(cam);
        insert Campaigns;
        
        List<ULC_Details__c> ULCDetails = new List<ULC_Details__c>();
        ULC_Details__c ulc = new ULC_Details__c();
        ulc.ULCName__c = 'thecontactuser';
        ulc.ULCStatus__c = 'Active';
        List<Contact> Contacts1 = [SELECT Id FROM Contact];
        ulc.ContactId__c = Contacts1[0].Id;
        ULCDetails.add(ulc);
        insert ULCDetails;
        
        List<ULC_Details__c> ULCDetailsContactwithCM = new List<ULC_Details__c>();
        ULC_Details__c ulcc = new ULC_Details__c();
        ulcc.ULCName__c = 'thecontactcmuser';
        ulcc.ULCStatus__c = 'Active';
        List<Contact> Contacts11 = [SELECT Id FROM Contact Where Email = '2_RDZTest@test.com.sandbox'];
        ulcc.ContactId__c = Contacts11[0].Id;
        ULCDetailsContactwithCM.add(ulcc);
        insert ULCDetailsContactwithCM;

        List<ULC_Details__c> ULCDetailsForLead = new List<ULC_Details__c>();
        ULC_Details__c ulcForLead = new ULC_Details__c();
        ulcForLead.ULCName__c = 'theleaduser';
        ulcForLead.ULCStatus__c = 'Active';
        //get Lead object
        List<Lead> Leads1 = [SELECT Id FROM Lead WHERE Email = 'first.last2@lead.com'];
        ulcForLead.LeadId__c = Leads1[0].Id;
        ULCDetailsForLead.add(ulcForLead);
        insert ULCDetailsForLead;
        
        List<ULC_Details__c> ULCDetailsForLeadwithCM = new List<ULC_Details__c>();
        ULC_Details__c ulcForLead1 = new ULC_Details__c();
        ulcForLead1.ULCName__c = 'theleadcmuser';
        ulcForLead1.ULCStatus__c = 'Active';
        //get Lead object
        List<Lead> Leads3 = [SELECT Id FROM Lead WHERE Email = 'first.last@lead.com'];
        ulcForLead1.LeadId__c = Leads3[0].Id;
        ULCDetailsForLeadwithCM.add(ulcForLead1);
        insert ULCDetailsForLeadwithCM;
        
        List<CampaignMember> CM = new List<CampaignMember>();
        CampaignMember cmforlead = new CampaignMember();
        cmforlead.Status = 'Form Fill Out';
        cmforlead.Number_Of_Form_Submissions__c = 1;
        List<Lead> Leads4 = [SELECT Id FROM Lead WHERE Email = 'first.last@lead.com'];
        cmforlead.LeadId = Leads4[0].id;
        cmforlead.CampaignId = Campaigns[0].id;
        CM.add(cmforlead);
        insert CM;
        
        List<CampaignMember> CM1 = new List<CampaignMember>();
        CampaignMember cmforcontact = new CampaignMember();
        cmforcontact.Status = 'Form Fill Out';
        cmforcontact.Number_Of_Form_Submissions__c = 1;
        List<Contact> contacts2 = [SELECT Id FROM Contact Where Email = '2_RDZTest@test.com.sandbox'];
        cmforcontact.ContactId = contacts2[0].id;
        cmforcontact.CampaignId = Campaigns[0].id;
        CM1.add(cmforcontact);
        insert CM1;

        Semaphores.SetAllSemaphoresToFalse();
    }
    
    private static WebActivityUtils.AdditionalParams AdditionalParamsSetUp_QlikID()
    {
        Campaign cam = [SELECT Id FROM Campaign WHERE Name = 'My Campaign'];
        Id sicId = [select id from SIC_Code__c].Id;
        Id cmId = [select id from Campaign].Id;
        WebActivityUtils.AdditionalParams ap = new WebActivityUtils.AdditionalParams();
        ap.SupplimentalData = 'CAMPAIGNID~'+cmId+'~KEYWORDS~aaaa~SUB_INDUSTRY~'+ sicId + '~LEADSOURCEDETAILMIRROR~lsrmirror~EMPLOYEE_RANGE~25-49~INDUSTRY~abc~DB_COMPANY_NAME~fffff~REGISTRY_STATE~~REGISTRY_COUNTRY~Sweden~JOB_FUNCTION~gggg~ref~QSDW~TCACCEPTED~true~WEB_ACTIVITY_SOURCE~VA~SOURCEURL~google.com~SourceULC~source-ulc~SourceEmpl~source-employee~SourcePartner~source-partner~SourceID1~source-id-1~SourceID2~source-id-2~Incentive~'+ cam.Id +'~FreeText~ThisIsMyFreeText~_Mkto_Trk~MarketoTracking~webleadinterest~Potential Customer~leadsourcedetail~leadsource-detail~partnersource~partner-source';
        ap.ZiftData = 'ZIFTPARTNERID~pziftpartnerid~ZIFTPARTNERNAME~pziftpartnername';
        ap = WebActivityUtils.AdditionalParamsSplit(ap);
        return ap;
    }
    
     private static WebActivityUtils.AdditionalParams AdditionalParamsSetUp_withoutCampaignId()
    {
        Campaign cam = [SELECT Id FROM Campaign WHERE Name = 'My Campaign'];
        Id sicId = [select id from SIC_Code__c].Id;
        //Id cmId = [select id from Campaign].Id;
        Id cmId = '7010D00000028Fy';
        WebActivityUtils.AdditionalParams ap = new WebActivityUtils.AdditionalParams();
        ap.SupplimentalData = 'CAMPAIGNID~'+cmId+'~KEYWORDS~aaaa~SUB_INDUSTRY~'+ sicId + '~LEADSOURCEDETAILMIRROR~lsrmirror~EMPLOYEE_RANGE~25-49~INDUSTRY~abc~DB_COMPANY_NAME~fffff~REGISTRY_STATE~~REGISTRY_COUNTRY~Sweden~JOB_FUNCTION~gggg~ref~QSDW~TCACCEPTED~true~WEB_ACTIVITY_SOURCE~VA~SOURCEURL~google.com~SourceULC~source-ulc~SourceEmpl~source-employee~SourcePartner~source-partner~SourceID1~source-id-1~SourceID2~source-id-2~Incentive~'+ cam.Id +'~FreeText~ThisIsMyFreeText~_Mkto_Trk~MarketoTracking~webleadinterest~Potential Customer~leadsourcedetail~leadsource-detail~partnersource~partner-source';
        ap.ZiftData = 'ZIFTPARTNERID~pziftpartnerid~ZIFTPARTNERNAME~pziftpartnername';
        ap = WebActivityUtils.AdditionalParamsSplit(ap);
        return ap;
    }
    
    private static List<WebActivityUtils.WebActivityContent> createWebActivityContent()
    {
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        return lstWAContent;
    }
    //Lead w/o campaign member and task
    @isTest static void Scenario1(){
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        ulcuser.UserName = 'theleaduser';
        List<ULC_Details__c> ulcid = [Select Id From ULC_Details__c where ULCName__c = 'theleaduser'];
        ulcuser.ULCDetailsID = ulcid[0].id;
        waContent.OUser = ulcuser;
        waContent.AdditParams = AdditionalParamsSetUp_QlikID();
        lstWAContent.add(waContent);
        
        Test.startTest();
        lstWAContent = WebActivityRecordDownload.RecordDownload(lstWAContent);
        Test.stopTest();
        
        if (lstWAContent.size() > 0)
        system.assertEquals('Completed', lstWAContent[0].Status);
            
    }
    //Lead and contact w/o campaign member and task
    @isTest static void Scenario2(){
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        ulcuser.UserName = 'theleaduser';
        List<ULC_Details__c> ulcid = [Select Id From ULC_Details__c where ULCName__c = 'theleaduser'];
        ulcuser.ULCDetailsID = ulcid[0].id;
        waContent.OUser = ulcuser;
        waContent.AdditParams = AdditionalParamsSetUp_QlikID();
        lstWAContent.add(waContent);
        
        WebActivityUtils.WebActivityContent waContent1 = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser ulcuser1 = new WebActivityUtils.ULCUser();
        ulcuser1.UserName = 'thecontactuser';
        List<ULC_Details__c> ulcid1 = [Select Id From ULC_Details__c where ULCName__c = 'thecontactuser'];
        ulcuser1.ULCDetailsID = ulcid1[0].id;
        waContent1.OUser = ulcuser1;
        waContent1.AdditParams = AdditionalParamsSetUp_QlikID();
        lstWAContent.add(waContent1);
        
        Test.startTest();
        lstWAContent = WebActivityRecordDownload.RecordDownload(lstWAContent);
        Test.stopTest();
        
        if (lstWAContent.size() > 0)
        system.assertEquals('Completed', lstWAContent[0].Status);
        system.assertEquals('Completed', lstWAContent[1].Status);
    }
    //Lead with campaign member and no task, Contact w/o campaign member and no task
    @isTest static void Scenario3(){
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        ulcuser.UserName = 'theleaduser';
        List<ULC_Details__c> ulcid = [Select Id From ULC_Details__c where ULCName__c = 'theleaduser'];
        ulcuser.ULCDetailsID = ulcid[0].id;
        waContent.OUser = ulcuser;
        waContent.AdditParams = AdditionalParamsSetUp_QlikID();
        lstWAContent.add(waContent);
        
        WebActivityUtils.WebActivityContent waContent1 = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser ulcuser1 = new WebActivityUtils.ULCUser();
        ulcuser1.UserName = 'theleadcmuser';
        List<ULC_Details__c> ulcid1 = [Select Id From ULC_Details__c where ULCName__c = 'theleadcmuser'];
        ulcuser.ULCDetailsID = ulcid1[0].id;
        waContent1.OUser = ulcuser1;
        waContent1.AdditParams = AdditionalParamsSetUp_QlikID();
        lstWAContent.add(waContent1);
        
        WebActivityUtils.WebActivityContent waContent2 = new WebActivityUtils.WebActivityContent();
        WebActivityUtils.ULCUser ulcuser2 = new WebActivityUtils.ULCUser();
        ulcuser2.UserName = 'thecontactuser';
        List<ULC_Details__c> ulcid2 = [Select Id From ULC_Details__c where ULCName__c = 'thecontactuser'];
        ulcuser2.ULCDetailsID = ulcid2[0].id;
        waContent2.OUser = ulcuser2;
        waContent2.AdditParams = AdditionalParamsSetUp_QlikID();
        lstWAContent.add(waContent2);
        
        Test.startTest();
        lstWAContent = WebActivityRecordDownload.RecordDownload(lstWAContent);
        Test.stopTest();
        
        if (lstWAContent.size() > 0)
        system.assertEquals('Completed', lstWAContent[0].Status);
        system.assertEquals('Completed', lstWAContent[1].Status);
        system.assertEquals('Completed', lstWAContent[2].Status);
    }
    //Contact with campaign member and no task
    @isTest static void Scenario4(){
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        ulcuser.UserName = 'thecontactcmuser';
        List<ULC_Details__c> ulcid = [Select Id From ULC_Details__c where ULCName__c = 'thecontactcmuser'];
        ulcuser.ULCDetailsID = ulcid[0].id;
        waContent.OUser = ulcuser;
        waContent.AdditParams = AdditionalParamsSetUp_QlikID();
        lstWAContent.add(waContent);
        
        Test.startTest();
        lstWAContent = WebActivityRecordDownload.RecordDownload(lstWAContent);
        Test.stopTest();
        
        if (lstWAContent.size() > 0)
        system.assertEquals('Completed', lstWAContent[0].Status);
            
        }
    //Contact w/o campaign member , no task and incorrect campaign ID-error scenario
    @isTest static void Scenario5(){
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        ulcuser.UserName = 'thecontactuser';
        List<ULC_Details__c> ulcid = [Select Id From ULC_Details__c where ULCName__c = 'thecontactuser'];
        ulcuser.ULCDetailsID = ulcid[0].id;
        waContent.OUser = ulcuser;
        waContent.AdditParams = AdditionalParamsSetUp_withoutCampaignId();
        lstWAContent.add(waContent);
        
        Test.startTest();
        lstWAContent = WebActivityRecordDownload.RecordDownload(lstWAContent);
        Test.stopTest();
        
        if (lstWAContent.size() > 0)
        system.assertNotEquals('Completed', lstWAContent[0].Status);
        }
    //Lead w/o campaign member , no task and incorrect campaign ID-error scenario
    @isTest static void Scenario6(){
        WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();
        List<WebActivityUtils.WebActivityContent> lstWAContent = new List<WebActivityUtils.WebActivityContent>();
        WebActivityUtils.ULCUser ulcuser = new WebActivityUtils.ULCUser();
        ulcuser.UserName = 'theleaduser';
        List<ULC_Details__c> ulcid = [Select Id From ULC_Details__c where ULCName__c = 'theleaduser'];
        ulcuser.ULCDetailsID = ulcid[0].id;
        waContent.OUser = ulcuser;
        waContent.AdditParams = AdditionalParamsSetUp_withoutCampaignId();
        lstWAContent.add(waContent);
        
        Test.startTest();
        lstWAContent = WebActivityRecordDownload.RecordDownload(lstWAContent);
        Test.stopTest();
        
        if (lstWAContent.size() > 0)
        system.assertNotEquals('Completed', lstWAContent[0].Status);
        }

}