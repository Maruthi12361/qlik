/**
    Change log
    11.27.2018 : AIN - IMP-1035 - Created
    2019-02-14 - AIN - Fixed errors related to new duplicate rule
**/
@isTest
public class Zuora_CreateOrderFormControllerTest {

    //private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';


    //List of accounts to be processed in test methods
    public Static List<Account> accounts = new List<Account>();


   @testSetup
    private static void testSetup() {
        QuoteTestHelper.createCustomSettings();
    }




    /** This method is used to test the functionality of Direct Revenue Type**/
    private static testMethod void testCreateFormDirect() {

        Subsidiary__c sub = new Subsidiary__c(name='Umpa Lumpa',Legal_Entity_Name__c='Umpa Lumpa',CurrencyIsoCode='USD');
        insert sub;

       Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'No Matches';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        endUserAccount.Subsidiary__c = sub.Id;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'No Matches';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';
        partner.Subsidiary__c = sub.Id;
        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'No Matches';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        //Test start here
          Test.startTest();
        //Constructer initilization
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        zquote.Quote_Status__c = 'Approved';
        //zquote.Subsidiary__c  = 'Umpa lumpa';
        insert zquote;
        Test.stopTest();
        zqu__Quote__c zquote1 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, Doc_Launcher_Config__c, zqu__Number__c, 
            zqu__Opportunity__r.OppNumber__c, zqu__Account__r.AccNumber__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        ApexPages.StandardController sc = new ApexPages.StandardController(zquote1);
        zuora_CreateOrderFormController createOrderForm = new zuora_CreateOrderFormController(sc);

        zquote1.Order_Form_Status__c = 'Out For Signature';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'External Review';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Quote Approvals Needed';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Sales Review';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Legal Review';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Orders Review';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Orders Approval';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Fully Executed';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'None';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Canceled';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Rejected';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Failed';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = 'Expired';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Order_Form_Status__c = null;
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Quote_Status__c = 'Contracting';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.Quote_Status__c = 'Umpa lumpa';
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        zquote1.zqu__ValidUntil__c = Date.Today()+1;
        sc = new ApexPages.StandardController(zquote1);
        createOrderForm = new zuora_CreateOrderFormController(sc);
        createOrderForm.validateMethod();

        //Assert for results
        Boolean result = createOrderForm.ECustomsCleared;
        System.assertEquals(true, result);

        //Calling controller method
        createOrderForm.validateMethod();

        createOrderForm.getQTCustomSettingsHier();

        zuora_CreateOrderFormController.checkVCStatus(null, zquote1.Revenue_Type__c,zquote1.zqu__Account__r.ECUSTOMS__RPS_Status__c,
            zquote1.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zquote1.zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zquote1.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c);

        //Calling controller method
        Pagereference pg = createOrderForm.returnToQuote();
        System.assert(pg != null);

        zuora_PlaceOrderController placeOrderForm = new zuora_PlaceOrderController(sc);
        boolean status = placeOrderForm.status;
        boolean VCstatus = placeOrderForm.VCstatus;
        placeOrderForm.sellThroughPartner = partner;
        boolean poRequired = placeOrderForm.poRequired;
    }
    private static testMethod void testNoSub() {

       Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'No Matches';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'No Matches';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';

        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'No Matches';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        //Test start here
          Test.startTest();
        //Constructer initilization
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        //zquote.Subsidiary__c  = 'Umpa lumpa';
        insert zquote;
        Test.stopTest();
        zqu__Quote__c zquote1 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        ApexPages.StandardController sc = new ApexPages.StandardController(zquote1);
        zuora_CreateOrderFormController createOrderForm = new zuora_CreateOrderFormController(sc);

        //Assert for results
        Boolean result = createOrderForm.ECustomsCleared;
        System.assertEquals(true, result);

        //Calling controller method
        createOrderForm.validateMethod();

        createOrderForm.getQTCustomSettingsHier();

        //Calling controller method
        Pagereference pg = createOrderForm.returnToQuote();
        System.assert(pg != null);
    }
    private static testMethod void testNotEcustomesCleared() {

        Subsidiary__c sub = new Subsidiary__c(name='Umpa Lumpa',Legal_Entity_Name__c='Umpa Lumpa',CurrencyIsoCode='USD');
        insert sub;

       Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'Alert';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        endUserAccount.Subsidiary__c = sub.id;
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'Alert';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';
        partner.Subsidiary__c = sub.id;
        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'Alert';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        //Test start here
          Test.startTest();
        //Constructer initilization
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        zquote.Quote_Status__c = 'Approved';
        insert zquote;
        Test.stopTest();
        zqu__Quote__c zquote1 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        ApexPages.StandardController sc = new ApexPages.StandardController(zquote1);
        zuora_CreateOrderFormController createOrderForm = new zuora_CreateOrderFormController(sc);

        //Assert for results
        Boolean result = createOrderForm.ECustomsCleared;
        System.assertEquals(false, result);

        //Calling controller method
        createOrderForm.validateMethod();

        createOrderForm.getQTCustomSettingsHier();

        //Calling controller method
        Pagereference pg = createOrderForm.returnToQuote();
        System.assert(pg != null);
    }

    private static testMethod void testNonePrimary() {

        Subsidiary__c sub = new Subsidiary__c(name='Umpa Lumpa',Legal_Entity_Name__c='Umpa Lumpa',CurrencyIsoCode='USD');
        insert sub;

       Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.ECUSTOMS__RPS_Status__c= 'Alert';
        endUserAccount.ECUSTOMS__RPS_Date__c= Date.Today();
        endUserAccount.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        endUserAccount.BillingStreet='hhhh';
        endUserAccount.BillingCity='iiiii';
        endUserAccount.Subsidiary__c = sub.id;
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;
 
        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        partner.ECUSTOMS__RPS_Status__c= 'Alert';
        partner.ECUSTOMS__RPS_Date__c= Date.Today();
        partner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        partner.BillingStreet='hhhh';
        partner.BillingCity='iiiii';
        partner.Subsidiary__c = sub.id;
        insert partner;
 
        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        secondPartner.ECUSTOMS__RPS_Status__c= 'Alert';
        secondPartner.ECUSTOMS__RPS_Date__c= Date.Today();
        secondPartner.ECUSTOMS__RPS_RiskCountry_Status__c='N/A';
        secondPartner.BillingStreet='hhhgh';
        secondPartner.BillingCity='iiiigi';
        insert secondPartner;


        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'Jasper';    
        partnerContact.LastName = 'Johnsson';    
        partnerContact.Email = 'jonsontest@partners.com';    
        partnerContact.Phone = '156-717-1123';         
        insert partnerContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Reseller';
        insert testOpp;

        //Test start here
          Test.startTest();
        //Constructer initilization
        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        zquote.Quote_Status__c = 'Approved';
        insert zquote;

        zqu__Quote__c zquote2 = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote2.zqu__BillToContact__c = endUserContact.id;
        zquote2.zqu__SoldToContact__c = endUserContact.id;
        zquote2.Quote_Status__c = 'Approved';
        zquote2.zqu__Primary__c = true;
        insert zquote2;
        zquote2.zqu__Primary__c = true;
        update zquote2;
        Test.stopTest();
        zqu__Quote__c zquote1 = [select id,Revenue_Type__c,zqu__Account__r.ECUSTOMS__RPS_Status__c,Subsidiary__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c,Quote_Status__c, zqu__Primary__c,  Is_Quote_Expired__c from zqu__Quote__c where id = :zquote.Id];
        ApexPages.StandardController sc = new ApexPages.StandardController(zquote1);
        zuora_CreateOrderFormController createOrderForm = new zuora_CreateOrderFormController(sc);

        //Assert for results
        Boolean result = createOrderForm.ECustomsCleared;
        System.assertEquals(false, result);

        //Calling controller method
        createOrderForm.validateMethod();

        createOrderForm.getQTCustomSettingsHier();

        //Calling controller method
        Pagereference pg = createOrderForm.returnToQuote();
        System.assert(pg != null);
    }

    
}