/****************************************************************
*
*  OpportunityPreviousOwnerHandler
*
*  2016-05-19 TJG : Migrated from the following trigger
					OpportunityPreviousOwner
*  2017-06-22 MTM  QCW-2711 remove sharing option
*****************************************************************/
public class OpportunityPreviousOwnerHandler {
	public static void handle(List<Opportunity> triggerOld, List<Opportunity> triggerNew)
	{
		for(Integer i = 0; i < triggerNew.size(); i++) {
			Opportunity oldOpp = triggerOld[i];
			
			System.debug('===>OpportunityPreviousOwner');
			
			if(oldOpp.is_services_opportunity__c)
			{
				Opportunity newOpp = triggerNew[i];
				
				Id prevOwnerId = oldOpp.OwnerId;
				
				//don't modify if previous owner is null or previous owner hasn't changed or owner hasn't changed
				if (prevOwnerId != null && newOpp.Previous_Owner__c != prevOwnerId && newOpp.OwnerId != prevOwnerId) {			
					newOpp.Previous_Owner__c = prevOwnerId;
					System.debug('===>OpportunityNewOwner:' + newOpp.OwnerId);
					System.debug('===>OpportunityPreviousOwner:' + prevOwnerId);
				}
			}
		}
	}
}