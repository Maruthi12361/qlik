/* Purpose: Partner Expertise Market your Expertise Controller
* Created Date: 13 Nov 2015
* Initial development MTM 10 May 2015
* Change log:
*/
public with Sharing class EP_MarketExpertiseController {
   
    public EP_MarketExpertiseController(){
        AccountId = ApexPages.currentPage().getParameters().get('varAccountID');
        System.debug('Account Id = ' + AccountId);
    }
    private string accId;
    public String AccountId
    {       
        get{ return accId;}
        set{        
            if(value != null && value != '')
            {
                accId = value;
            }            
        }
    }
    
    public List <SelectOption> QlikMarkets
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            for ( QVM_Product_Data__c c : ListQlikMarketApp ) 
            {
            	System.debug('QVMIdAndName__c = ' + c.QVMIdAndName__c);
                options.add(new SelectOption(c.Id, c.QVMIdAndName__c));            
        	} 
            return options;
        }
    }
    
    public List <SelectOption> ExpertiseAreas
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            for ( Partner_Expertise_Area__c c : ListExpertiseArea) 
            {
            	System.debug('Expertise_Area__c = ' + c.Expertise_Area__c);
                options.add(new SelectOption(c.Id, c.Expertise_Area__c));            
        	} 
            return options;
        }
    }
    public Id ExpertiseArea {get; set;}
    public Id QlikMarketApp { get;set;}
    
    public List<QVM_Product_Data__c> ListQlikMarketApp{
        get{
            List<QVM_Product_Data__c> listofQlikMarkets = [SELECT QVMIdAndName__c, 
                                                           Id, 
                                                           Name, 
                                                           Partner_Expertise_Area_ID__c 
                                                           FROM QVM_Product_Data__c 
                                                           WHERE QVM_Partner__r.Partner_Account__c =:AccountId 
                                                           and Type__c ='Application'
                                                           and Status__c = 'Active'
                                                           and Id not in (Select Qlik_Market_Application__c from Partner_Expertise_Area__c where Expertise_Designation_Status__c = 'Under Review' or Expertise_Designation_Status__c = 'Approved')];
            return listofQlikMarkets;
        }
    }
    public map<Id,Partner_Expertise_Area__c> ExpertiseAreaMap
    {
        get{
            Map<Id,Partner_Expertise_Area__c>  expMap = new Map<Id,Partner_Expertise_Area__c>();
            for ( Partner_Expertise_Area__c c : ListExpertiseArea) 
            {
            	expMap.put(c.Id, c); 
        	}
            return expMap;
        }       
    }
    
    public map<Id,QVM_Product_Data__c> QlikMarketAppMap
    {
        get{
            Map<Id,QVM_Product_Data__c>  qlikAppMap = new Map<Id,QVM_Product_Data__c>();
            for ( QVM_Product_Data__c c : ListQlikMarketApp) 
            {
            	qlikAppMap.put(c.Id, c); 
        	}
            return qlikAppMap;
        }       
    }
    public List<Partner_Expertise_Area__c> ListExpertiseArea{
        get{
            List<Partner_Expertise_Area__c> listofExp = [Select
                                                         Id, 
                                                         Expertise_Area__c,
                                                         Qlik_Market_Application__c,
                                                         Qlik_Market_Application__r.Name
                                                         from Partner_Expertise_Area__c where Partner_Account_Name__c=: AccountId
                                                         and (Expertise_Designation_Status__c != 'Under Review' and Expertise_Designation_Status__c !='Approved')
                                                         and  Expertise_Application_Eligibility__c=: 'Accepted' ];
            return listofExp;
        }     
    }
    public List<Partner_Expertise_Area__c> ListQMAssigned{
        get{
            List<Partner_Expertise_Area__c> listofExp = [Select
                                                         Id, 
                                                         Expertise_Area__c,                                                         
                                                         Qlik_Market_Application__c,
                                                         Qlik_Market_Application__r.QVMIdAndName__c,
                                                         Qlik_Market_Application__r.Product_Name__c
                                                         from Partner_Expertise_Area__c where Partner_Account_Name__c=: AccountId                  
                                                         AND Qlik_Market_Application__c !=  null];
            return listofExp;
        }     
    }
    
    public PageReference Save()
    {
        if(ExpertiseArea != null && QlikMarketApp != null)
        {
            Partner_Expertise_Area__c expertArea;
            QVM_Product_Data__c qlikMarket;
            for ( Partner_Expertise_Area__c c : ListExpertiseArea) 
            {
                if(c.Qlik_Market_Application__c == QlikMarketApp)
                {
                    c.Qlik_Market_Application__c = null;
                    c.Qlik_Market_App_Status__c = 'Not Assigned';
                }
                update c;
            }
            if(ExpertiseAreaMap.containsKey(ExpertiseArea))
            {
                expertArea = ExpertiseAreaMap.get(ExpertiseArea);
                expertArea.Qlik_Market_Application__c = QlikMarketApp;
                expertArea.Qlik_Market_App_Status__c = 'Completed';
            }
            if(QlikMarketAppMap.containsKey(QlikMarketApp))
            {
                qlikMarket = QlikMarketAppMap.get(QlikMarketApp);
                qlikMarket.Partner_Expertise_Area_ID__c = ExpertiseArea;
            }
            try{
                update expertArea;
                update qlikMarket;
            }
            catch(Exception ex)
            {
                System.Debug('Exception on updating Qlik Market Application');
                ApexPages.Message errMessage = new ApexPages.Message(ApexPages.Severity.Warning, 'Exception on updating Qlik Market Application');
                ApexPages.addMessage(errMessage);                
            }
        }
        return null;
    }
}