/*
	Created: 2019-01-02
	Created By: Linus Löfberg

	Change log:
		2019--01-02 - Class created for BSL-1293
*/
public with sharing class ZuoraSubscrptionsRelatedListCTRLEXT {

	private final Account myAccount;
	public List<Zuora__Subscription__c> subscriptionRecords { public get; private set; }

	public ZuoraSubscrptionsRelatedListCTRLEXT(ApexPages.StandardController stdController) {
		this.myAccount = (Account)stdController.getRecord();

		User partnerUser = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId()];

		List<Zuora__Subscription__c> accountSubscriptions =
		[SELECT Id, Name, Zuora__SubscriptionNumber__c, Zuora__QuoteNumber__c, Invoice_Owner__c, Zuora__SubscriptionStartDate__c, Zuora__SubscriptionEndDate__c, Zuora__AutoRenew__c, CurrencyIsoCode FROM Zuora__Subscription__c WHERE Zuora__Account__c = :myAccount.Id];

		Set<String> quoteNumberSet = new Set<String>();
		Map<String, Zuora__Subscription__c> quoteNumberSubscriptionMap = new Map<String, Zuora__Subscription__c>();
		for (Zuora__Subscription__c sub : accountSubscriptions) {
			quoteNumberSet.add(sub.Zuora__QuoteNumber__c);
			quoteNumberSubscriptionMap.put(sub.Zuora__QuoteNumber__c, sub);
		}

		List<zqu__Quote__c> quotesList = [SELECT Id, Name, zqu__Number__c, Sell_Through_Partner_Id__c, Sell_Through_Partner__c FROM zqu__Quote__c WHERE zqu__Number__c IN :quoteNumberSet];
		List<Zuora__Subscription__c> finalRecords = new List<Zuora__Subscription__c>();
		for (zqu__Quote__c quote : quotesList) {
			if (quote.Sell_Through_Partner_Id__c != null && quote.Sell_Through_Partner_Id__c.equals(partnerUser.Contact.AccountId)) {
				finalRecords.add(quoteNumberSubscriptionMap.get(quote.zqu__Number__c));
			}
		}
		subscriptionRecords = finalRecords;
	}
}
