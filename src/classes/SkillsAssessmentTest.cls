/**
* Test for SkillsAssessmentHandler and SkillsAssessmentTrigger
* Change log:
*
*   2019-08-28 ext_bad  CHG0036677: Initial implementation.
*/
@isTest
private class SkillsAssessmentTest {

    static testMethod void testPopulateFromLead() {
        Long dt = system.now().getTime();
        Lead lead = new Lead();
        lead.Email = 'test' + dt + '@test.com';
        lead.FirstName = 'testfirst' + dt;
        lead.LastName = 'testlast' + dt;
        Lead.Country = 'Sweden';
        Lead.Company = 'test comp';
        insert lead;

        ULC_Details__c det = new ULC_Details__c();
        det.ULCName__c = UserInfo.getUserId().toLowerCase();
        det.LeadId__c = lead.Id;
        insert det;

        Test.startTest();
        Skills_Assessment__c sa = new Skills_Assessment__c();
        sa.Lead__c = lead.Id;
        insert sa;
        Test.stopTest();

        sa = [SELECT Email__c, First_Name__c, Last_Name__c, ULC__c FROM Skills_Assessment__c WHERE ID = :sa.Id];
        System.assertEquals(lead.Email, sa.Email__c);
        System.assertEquals(lead.FirstName, sa.First_Name__c);
        System.assertEquals(lead.LastName, sa.Last_Name__c);
        System.assertEquals(det.ULCName__c, sa.ULC__c);
    }

    static testMethod void testPopulateFromContact() {
        Long dt = system.now().getTime();
        Contact cont = new Contact();
        cont.Email = 'test' + dt + '@test.com';
        cont.FirstName = 'testfirst' + dt;
        cont.LastName = 'testlast' + dt;
        insert cont;

        ULC_Details__c det = new ULC_Details__c();
        det.ULCName__c = UserInfo.getUserId().toLowerCase();
        det.ContactId__c = cont.Id;
        insert det;

        Test.startTest();
        Skills_Assessment__c sa = new Skills_Assessment__c();
        sa.Contact__c = cont.Id;
        insert sa;
        Test.stopTest();

        sa = [SELECT Email__c, First_Name__c, Last_Name__c, ULC__c FROM Skills_Assessment__c WHERE ID = :sa.Id];
        System.assertEquals(cont.Email, sa.Email__c);
        System.assertEquals(cont.FirstName, sa.First_Name__c);
        System.assertEquals(cont.LastName, sa.Last_Name__c);
        System.assertEquals(det.ULCName__c, sa.ULC__c);
    }
}