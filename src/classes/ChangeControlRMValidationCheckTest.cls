/*******************************************************************
* Class ChangeControlRMValidationCheckTest
*
* Test class for trigger ChangeControlRMValidationCheck
* 
* Change Log:
* 2014-02-26   AIN (Andreas Nilsson)	 	Initial Development
*											CR# 10110 #Add Release Management to CR Process
**********************************************************************/

@isTest
private class ChangeControlRMValidationCheckTest 
{
	//Currently included record types
	private static final string RECORDTYPE_IT_SYSTEMS = '01220000000IMuV';
	private static final string RECORDTYPE_PROJECT = '01220000000I5iz';

	//Currently excluded record types
	private static final string RECORDTYPE_ADMIN = '01220000000HZ2I';
	private static final string RECORDTYPE_CHANGE_REQUEST = '01220000000ID9W';
	private static final string RECORDTYPE_DEMO_AND_BEST_PRACTICE = '01220000000IMuU';
	private static final string RECORDTYPE_KNOWLEDGE_MANAGEMENT = '01220000000IMuW';
	private static final string RECORDTYPE_QLIKVIEW_APPLICATION_CHANGE = '012D0000000KDYl';
	private static final string RECORDTYPE_PRODUCT = '01220000000IMuX';
	private static final string RECORDTYPE_TFS = '01220000000IMuY';
	private static final string RECORDTYPE_USER = '01220000000HZ2J';

	//Currently excluded types
	private static final string TYPE_INSTALLATION = 'Installation';
	private static final string TYPE_INFRASTRUCTURE = 'Infrastructure';
	private static final string TYPE_BUSINESS_DISCOVERY = 'Business Discovery';
	private static final string TYPE_PUB_QLIKTECH_COM = 'pub.qliktech.com';

	//Not excluded types
	private static final string TYPE_ADMINISTRATION = 'Administration';
	private static final string TYPE_DEVELOPMENT = 'Development';
	private static final string TYPE_BUSINESS_DISCOVERY_APPLICATION_DATA = 'Business Discovery Application & Data';
	private static final string TYPE_BUSINESS_DISCOVERY_INFRASTRUCTURE = 'Business Discovery Infrastructure';
	private static final string TYPE_INTEGRATION = 'Integration';
	private static final string TYPE_SYSTEMS_REVENUE = 'Systems - Revenue';
	private static final string TYPE_SYSTEMS_CORPORATE_SERVICES = 'Systems - Corporate Services';
	
	public static testmethod void InsertCheckedUpdateUncheckedTest() 
	{
		InitializeCustomSettings();


		list<SLX__Change_Control__c> changes = new list<SLX__Change_Control__c>();
		List<Id> ids = new List<Id> ();

		changes.add(CreateChangeControl(RECORDTYPE_IT_SYSTEMS, TYPE_ADMINISTRATION));
		changes.add(CreateChangeControl(RECORDTYPE_PROJECT, TYPE_DEVELOPMENT));
		changes.add(CreateChangeControl(RECORDTYPE_IT_SYSTEMS, TYPE_BUSINESS_DISCOVERY_APPLICATION_DATA));
		changes.add(CreateChangeControl(RECORDTYPE_PROJECT, TYPE_INTEGRATION));
		changes.add(CreateChangeControl(RECORDTYPE_IT_SYSTEMS, TYPE_SYSTEMS_CORPORATE_SERVICES));
		
		insert changes;

		for (SLX__Change_Control__c change : changes)
			ids.add(change.Id);

		Validate(ids, true);

		UpdateChangeControl(changes[0], RECORDTYPE_ADMIN, TYPE_ADMINISTRATION);
		UpdateChangeControl(changes[1], RECORDTYPE_CHANGE_REQUEST, TYPE_DEVELOPMENT);
		UpdateChangeControl(changes[2], RECORDTYPE_IT_SYSTEMS, TYPE_INSTALLATION);
		UpdateChangeControl(changes[3], RECORDTYPE_PROJECT, TYPE_INFRASTRUCTURE);
		UpdateChangeControl(changes[4], RECORDTYPE_ADMIN, TYPE_INSTALLATION);

		update changes;

		Validate(ids, false);
	}
	public static testmethod void InsertUncheckedUpdateCheckedTest() 
	{
		InitializeCustomSettings();
		list<SLX__Change_Control__c> changes = new list<SLX__Change_Control__c>();
		List<Id> ids = new List<Id> ();

		changes.add(CreateChangeControl(RECORDTYPE_ADMIN, TYPE_ADMINISTRATION));
		changes.add(CreateChangeControl(RECORDTYPE_CHANGE_REQUEST, TYPE_DEVELOPMENT));
		changes.add(CreateChangeControl(RECORDTYPE_IT_SYSTEMS, TYPE_INSTALLATION));
		changes.add(CreateChangeControl(RECORDTYPE_PROJECT, TYPE_INFRASTRUCTURE));
		changes.add(CreateChangeControl(RECORDTYPE_ADMIN, TYPE_INSTALLATION));
		
		insert changes;

		for (SLX__Change_Control__c change : changes)
			ids.add(change.Id);

		Validate(ids, false);

		UpdateChangeControl(changes[0], RECORDTYPE_IT_SYSTEMS, TYPE_ADMINISTRATION);
		UpdateChangeControl(changes[1], RECORDTYPE_PROJECT, TYPE_DEVELOPMENT);
		UpdateChangeControl(changes[2], RECORDTYPE_IT_SYSTEMS, TYPE_BUSINESS_DISCOVERY_APPLICATION_DATA);
		UpdateChangeControl(changes[3], RECORDTYPE_PROJECT, TYPE_INTEGRATION);
		UpdateChangeControl(changes[4], RECORDTYPE_IT_SYSTEMS, TYPE_SYSTEMS_CORPORATE_SERVICES);
		
		update changes;

		Validate(ids, true);
	}
	
	private static void Validate(List<Id> ids, boolean expected)
	{
		List <SLX__Change_Control__c> changes = [select ID, Name, RecordTypeId, Type__c, RM_Validation_Check__c from SLX__Change_Control__c where ID in :ids];

		for (SLX__Change_Control__c change : changes)
		{
			system.debug('Value of RecordTypeId: ' + change.RecordTypeId);
			system.debug('Value of Type__c: ' + change.Type__c);
			system.debug('Value of RM_Validation_Check__c: ' + change.RM_Validation_Check__c + ', expected: ' + expected);

			system.assertEquals(change.RM_Validation_Check__c, expected);
		}
	}
	private static SLX__Change_Control__c CreateChangeControl(string recordTypeId, string type)
	{
		SLX__Change_Control__c change = new SLX__Change_Control__c();
		change.RecordTypeId = recordTypeId;
		change.Type__c = type;
		return change;
	}
	private static SLX__Change_Control__c UpdateChangeControl(SLX__Change_Control__c change, string recordTypeId, string type)
	{
		change.RecordTypeId = recordTypeId;
		change.Type__c = type;
		return change;
	}
	private static void InitializeCustomSettings()
	{
		
		QTRMCustomSet__c customSetting = QTRMCustomSet__c.getOrgDefaults();
		if (customSetting == null) 
			customSetting = new QTRMCustomSet__c();

		customSetting.CRExcludeTypes__c = 'Installation, Infrastructure, Business Discovery, pub.qliktech.com,';
		customSetting.CRRecordTypes__c = 'IT/Systems:01220000000IMuV; Project:01220000000I5iz';
		
		upsert customSetting;
	}
}