/*
 File Name: CaseDetectLanguageHandler
 Moved from trigger CaseDetectLanguage on Case (after insert, after update)

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.

*/

public with sharing class CaseDetectLanguageHandler {


    public static void handleCaseDetectLanguage(List<Case> newCases) {
        //Create map of <String,String> to send to @future annotated method, as it doesn't accept SObject
        Map<String, String> caseIdsDescriptions = new Map<String, String>();
        List<Case> cases = new List<Case>();
        set<string> areasIssues = new set<string>();
        Map<String, List<String>> areasIssuesQuestions = new Map<String, List<String>>();

        //filter out the cases who already have a language and those whose description length is too short
        for (Case c : newCases) {
            if (String.isBlank(c.Language__c)) {
                if (String.isNotBlank(c.Description) && (c.Description.length() > 5)) {
                    if (c.Area__c != null && c.Issue__c != null) {
                        areasIssues.add(c.Area__c + '-' + c.Issue__c);
                        cases.add(c);
                    } else {
                        caseIdsDescriptions.put(c.Id, c.Description.trim());
                    }
                }
            }
        }
        //Name is area, Category_Level__c is issue
        if (areasIssues.size() > 0) {
            List<Category_Lookup__c> categoryLookups = [
                    SELECT Id, Name, Area_Issue__c, Category_Level__c,
                            Question_Prompt_1__c, Question_Prompt_2__c, Question_Prompt_3__c,
                            Question_Prompt_4__c, Question_Prompt_5__c
                    FROM category_lookup__c
                    WHERE Area_Issue__c in :areasIssues
            ];
            for (Category_Lookup__c cl : categoryLookups) {
                areasIssuesQuestions.put(cl.Area_Issue__c, New List<String>{
                        cl.Question_Prompt_1__c, cl.Question_Prompt_2__c, cl.Question_Prompt_3__c,
                        cl.Question_Prompt_4__c, cl.Question_Prompt_5__c
                });
            }
        }
        for (Case c : cases) {
            if (areasIssuesQuestions.containsKey(c.Area__c + '-' + c.Issue__c)) {
                system.debug(LoggingLevel.DEBUG, '[CaseDetectLanguageHandler] Description before replace: ' + c.description);
                List<string> questions = areasIssuesQuestions.get(c.Area__c + '-' + c.Issue__c);
                string description = (c.Description != null ? c.Description : '');
                for (string question : questions) {
                    if (question != null) {
                        description = description.replace(question, '');
                    }
                }
                system.debug(LoggingLevel.DEBUG, '[CaseDetectLanguageHandler] Description after replace: ' + description);
                caseIdsDescriptions.put(c.Id, description.trim());
            } else {
                caseIdsDescriptions.put(c.Id, c.Description.trim());
            }
        }

        if (!caseIdsDescriptions.isEmpty() && !Test.isRunningTest()) {
            GoogleTranslateUtils.detectLanguages(caseIdsDescriptions);
        }
    }
}