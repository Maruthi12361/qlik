/*****************************************************************************************
*
* Change Log:
* 2014-06-04    AIN     Initial Development
*                       CR# 7283 https://eu1.salesforce.com/a0CD000000U9yv8
* 2014-07-11    AIN      QTWebserviceUtil now uses custom settings to change the endpoint
*                       One or more records must be created for each web service in the custom setting
*                       QTWebserviceUtil__c, which has the fields Webservicename, sandbox and endpoint
*                       Webservice is supplied to the function from the helper functions.
*                       If there is a record in QTWebserviceUtil__c which has the same webservicename and Sandbox
*                       the endpoint is set to the records endpoint.
*                       Otherwise iff there is a record in QTWebserviceUtil__c which has the same webservicename and 
*                       a generic sandbox record, Denoted as *, the endpoint is set to the records endpoint
* 2014-09-01    AIN     Added exception handling when calling SetWebServiceEndpoint without a valid custom setting available
*
*****************************************************************************************/

public class QTWebserviceUtil {

    //Helper functions
    
    //sfUtils
    //Sets the web service end point to an Org specific value
    public static void SetWebServiceEndpoint(sfUtilsQlikviewCom.ServiceSoap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'sfUtils', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);
    }
    

    //SFDCSharingHandler
    //Sets the web service end point to an Org specific value
    public static void SetWebServiceEndpoint(sharinghandlerQlikviewCom.ServiceSoap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'SFDCSharingHandler', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);
    }

    //efQTPaymentGateway
    //Sets the web service end point to an Org specific value
    public static void SetWebServiceEndpoint(efQTPaymentGateway.ServiceSoap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'efQTPaymentGateway', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);    
    }

    //ULC
    //Sets the web service end point to an Org specific value
    public static void SetWebServiceEndpoint(ulcv3QlikviewCom.ServiceSoap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'ULCv3', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);
    }

    //QTVoucher
    //Sets the web service end point to an Org specific value  
    public static void SetWebServiceEndpoint(qtvoucherQliktechCom.ServiceSoap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'QTVoucher', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);
    }
	
	/* UIN commented as part of BSL-449
    //QTeCustoms
    public static void SetWebServiceEndpoint(qtecustomsQliktechCom.Service1Soap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'QTeCustoms', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);
    }
	*/
	//QtStrikeIron
    public static void SetWebServiceEndpoint(QtStrikeironCom.AddressValidationServiceSoap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'QtStrikeIron', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);
    }

	//Q2CWeCustoms
	/* UIN commented as part of BSL-449
    public static void SetWebServiceEndpoint(qtecustomsq2cw.eCustomsServiceSoap service)
    {
        system.debug('Endpoint prior to update: ' + service.endpoint_x);
        service.endpoint_x = GetEndpoint(service.endpoint_x, 'Q2CWeCustoms', null);
        system.debug('Endpoint after update: ' + service.endpoint_x);
    }
	*/
    //Main function

    //Only change this value for test purposes!
    //Makes the test class believe it's not in live
    public static boolean SkipEndpointOrganizationValidation = false;
    //Changes the endpoint to the value of the webservice for the sandbox
    //http://webservice.qliktech.com:7890/QTeCustoms/service1.asmx ->
    //http://testwebservice.qliktech.com:7890/QTeCustomsrefreshest/service1.asmx
    //If we are in the sandbox refreshest
    //Testclass: WebServiceGetEndpointT,est
    //Uses the custom setting QTWebserviceUtil__c which contains webservicename, sandbox and endpoint.
    //If webservicename is the same as in constructor and sandbox is the same as the current sandbox, endpoint is set to endpoint property
    //If webservicename is the same as in constructor and there is no record with the current sandbox, we look for a generic setting, denoted by sandbox = * and set the endpoint to the endpoint propery
    //If no custom settings are found for the current webservice / sandbox combination, the current endpoint is returned.
    public static string GetEndpoint(string endpoint, string webserviceName, string sandbox)
    {
        if(webservicename == null || webServiceName == '') 
            return endpoint;
        else 
            webserviceName = webserviceName.toLowerCase();

        if(sandbox == null)
            sandbox = UserInfo.getUserName().substringAfterLast('.').toLowerCase();
        else
            sandbox = sandbox.toLowerCase();

        //If production, don't change the URL
        if(UserInfo.getOrganizationId().startsWith('00D20000000IGPX') && !QTWebserviceUtil.SkipEndpointOrganizationValidation)
            return endpoint;

        boolean settingsFound = false;
        string newEndpoint = '';

        if(sandbox == null || sandbox == '')
        {
            system.debug('Couldn\'t not resolve current sandbox from Username!');
            return endpoint;
        }

        List<QTWebserviceUtil__c> qtWebserviceUtils = QTWebserviceUtil__c.getall().values();

        for(QTWebserviceUtil__c setting : qtWebserviceUtils)
        {
            if(setting.WebserviceName__c.toLowerCase() == webServiceName)
            {
                system.debug('Settings for webservice ' + webserviceName + ' found');
                if(setting.Sandbox__c == '*' && !settingsFound)
                {
                    system.debug('General settings found');
                    settingsFound = true;
                    newEndpoint = setting.Endpoint__c;
                }
                else if(setting.Sandbox__c.toLowerCase() == sandbox)
                {
                    system.debug('Sandbox specific settings found');
                    settingsFound = true;
                    newEndpoint = setting.Endpoint__c;
                    break;
                }
            }
        }
        
        if(settingsFound)
            return newEndpoint.replace('{sandbox}', sandbox);
        else
        {
            return endpoint;
        }
    }
}