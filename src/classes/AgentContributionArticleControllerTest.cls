/*
* File AgentContributionArticleControllerTest
    * @description : Unit test for AgentContributionArticleController
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       27.12.2017   Pramod Kumar V         Created Class

*/
@isTest(seeAlldata=false)
private class AgentContributionArticleControllerTest{
    
   
    static testMethod void test_AgentContributionArticleController() {
     Case testCase = new Case(Status = 'New', Priority = 'Medium', Origin = 'Email');
     insert testCase;
     ApexPages.KnowledgeArticleVersionStandardController sc = new ApexPages.KnowledgeArticleVersionStandardController(testCase);
     AgentContributionArticleController testAccPlan = new AgentContributionArticleController (sc);
    }
  }