/************************************************************************
*
*   OpportunityWhenEISCreateOppsHandler
*   
*   This trigger will make sure Opportunity with empty Value will
*   be updated with the Average Deal Size. Setting set in Custom 
*   Setting.    
*       
*   Changelog:
*       2016-05-19  Roman@4front  Migrated from Opportunity_WhenEISCreateOpps.trigger
*		2017-06-22 MTM  QCW-2711 remove sharing option
*************************************************************************/

public class OpportunityWhenEISCreateOppsHandler {
	public OpportunityWhenEISCreateOppsHandler() {
		
	}

	public static void handle(List<Opportunity> triggerNew) {
		System.Debug('Starting Opportunity_WhenEISCreateOpps');
    
	    //Set<string> CreatedByIDs = new Set<string>();
	    //for (integer i = 0; i < trigger.new.size(); i++)
	    //{
	    //    System.debug('Opportunity_WhenEISCreateOpps: trigger.new[i].CreatedById='+trigger.new[i].CreatedById);
	    //    if (!CreatedByIDs.Contains(trigger.new[i].CreatedById))
	    //    {
	    //        CreatedByIDs.Add(trigger.new[i].CreatedById);           
	    //    }       
	    //}
	    //System.debug('Opportunity_WhenEISCreateOpps: CreatedByIDs.size()='+CreatedByIDs.size());
	        
	    //Map<string, string> UsrProf = new Map<string, string>();
	    //Map<string, string> UsrRole = new Map<string, string>();
	    //for (User u : [select Id, UserRole.Name, Profile.Name from User where Id in :CreatedByIDs])
	    //{
	    //    System.debug('Opportunity_WhenEISCreateOpps: UsrProf Id='+u.Id+' Profile.Name='+u.Profile.Name);
	    //    UsrProf.Put(u.Id, u.Profile.Name);
	        
	    //    System.debug('Opportunity_WhenEISCreateOpps: UsrRole Id='+u.Id+' UserRole.Name='+u.UserRole.Name);
	    //    UsrRole.Put(u.Id, u.UserRole.Name);        
	    //}
	    
	        
	    //ID ID_DR_Std = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Direct / Reseller - Std Sales Process').getRecordTypeId();
	    //System.debug('ID_DR_Std is: ' + ID_DR_Std);
	    //ID ID_DR_Ext = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Direct / Reseller - Extension').getRecordTypeId();
	    //System.debug('ID_DR_Ext is: ' + ID_DR_Ext);
	    //ID ID_CCS = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('CCS - Customer Centric Selling').getRecordTypeId();
	    //System.debug('ID_CCS is: ' + ID_CCS);
	    
	    
	    //QTWorkflow_Rules__c qtwr = QTWorkflow_Rules__c.getInstance();
	    //string strPro1 = qtwr.Profiles_for_Stage_GI_1__c;
	    //string strPro2 = qtwr.Profiles_for_Stage_GI_2__c;
	    //string strPro3 = qtwr.Profiles_for_Stage_GI_3__c;
	    //string strPro4 = qtwr.Profiles_for_Stage_GI_4__c;
	    //string strPro5 = qtwr.Profiles_for_Stage_GI_5__c;
	    //string strPro6 = qtwr.Profiles_for_Stage_GI_6__c;
	    //string strPro7 = qtwr.Profiles_for_Stage_GI_7__c;
	    //string strPro8 = qtwr.Profiles_for_Stage_GI_8__c;
	    //string strProAll = strPro1+','+strPro2+','+strPro3+','+strPro4+','+strPro5+','+strPro6+','+strPro7+','+strPro8;
	    //System.Debug('Opportunity_WhenEISCreateOpps: strProAll=' + strProAll);
	    
	    //List<Opportunity> ToUpdate = new List<Opportunity>();
	    List<OpportunityTeamMember> oppTeamMembers = new List<OpportunityTeamMember>();
	    for (Opportunity o : triggerNew)
	    {
	        //Opportunity Opp = new Opportunity(Id = o.Id);
	        //if ((o.RecordTypeId == ID_DR_Std) || (o.RecordTypeId == ID_DR_Ext) || (o.RecordTypeId == ID_CCS)) 
	        //{
	        //    System.Debug('SAN Opportunity_WhenEISCreateOpps: User Profile=' + UsrProf.Get(o.CreatedById));
	        //    System.Debug('SAN Opportunity_WhenEISCreateOpps: User Role=' + UsrRole.Get(o.CreatedById));
	        //    System.Debug('SAN Opportunity_WhenEISCreateOpps: created by =' + o.CreatedById);
	            
	        //    //----- DEBUG
	        //    System.Debug('User role: ' + UsrRole.Get(o.CreatedById));
	        //    System.Debug('Is service: ' + o.is_Services_Opportunity__c);
	        //    System.Debug('Is pse service: ' + o.pse__is_Services_Opportunity__c);

	            
	        //    //CR# 5892 - Goal Identified when BDR/Marketing and EIS create ops from contacts
	        //    if (strProAll.contains(UsrProf.Get(o.CreatedById)))
	        //    {
	        //       Opp.StageName = 'Goal Identified';
	        //       //Opp.Comments_Referral__c = 'Hello World - just testing';
	        //       ToUpdate.Add(Opp);
	        //    }
	        //    //CR# 5674 - Goal Identified when EIS create ops
	        //    else if (UsrRole.Get(o.CreatedById).contains('- EIS'))
	        //    {
	        //       Opp.StageName = 'Goal Identified';
	        //       //Opp.Comments_Referral__c = 'Hello World - just testing - EIS';
	        //       ToUpdate.Add(Opp);   
	        //    }
	        //    //This bit is to cover what the existing workflow rule "Override the Marketing Qualified Stage" does.
	        //    //CR# 7336 - The Consulting services need to be able to save opportunities in Goal Identified stage
	        //    else if (!UsrRole.Get(o.CreatedById).contains('Marketing') && o.is_Services_Opportunity__c == False)
	        //    {
	        //       Opp.StageName = 'Goal Confirmed';
	        //       //Opp.Comments_Referral__c = 'Hello World - just testing - Goal Confirmed';
	        //       ToUpdate.Add(Opp);   
	        //    }       
	        //}
	        if (o.Deal_Split_Parent_Opportunity__c != null) 
	        {
	            OpportunityTeamMember teamMember = new OpportunityTeamMember();
	            teamMember.OpportunityId = o.Id;
	            teamMember.UserId = o.CreatedById;          
	            teamMember.TeamMemberRole = 'Sales Rep';
	            oppTeamMembers.Add(teamMember);         
	        }
	    }    

	    //System.debug('Opportunity_WhenEISCreateOpps: ToUpdate.size()='+ToUpdate.size());
	    
	    //if (ToUpdate.size() > 0)
	    //{
	    //    update(ToUpdate);
	    //}
	    
	    System.debug('Opportunity_WhenEISCreateOpps: oppTeamMembers.size()='+oppTeamMembers.size());
	    if (oppTeamMembers.size() > 0)
	    {
	        insert(oppTeamMembers);
	    }
	    
	    System.Debug('Finishing Opportunity_WhenEISCreateOpps');

	}
}