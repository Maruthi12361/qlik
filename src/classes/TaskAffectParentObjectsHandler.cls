/**************************************************
*
* Change Log:
*
* 2017-11-27	ext_bad	CR#CHG0030519	Created.
*										Includes 'after insert' logic from NewTaskTouchParentObject trigger.
**************************************************/

public class TaskAffectParentObjectsHandler {

    public static void handle(List<Task> newTasks) {
        //Put these in maps to ensure that each object is only in there once
        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
        Map<Id, Lead> leadsToUpdate = new Map<Id, Lead>();
        Map<Id, Opportunity> oppsToUpdate = new Map<Id, Opportunity>();
        Map<Id, Campaign> campsToUpdate = new Map<Id, Campaign>();
        Map<Id, Contact> contsToUpdate = new Map<Id, Contact>();
        Map<Id, Contract> contrsToUpdate = new Map<Id, Contract>();

        for (Task tsk : newTasks) {
            //Only CTI would set the CallType field
            if (tsk.CallType != null && tsk.CallType != '') {
                if (tsk.WhoId != null) {
                    String whoId = tsk.WhoId;
                    String whoIdPrefix = whoId.substring(0, 3);

                    if (whoIdPrefix.equalsIgnoreCase('00Q')) {
                        Lead lead = new Lead(Id = whoId, Last_Activity_Datetime__c = System.now());
                        leadsToUpdate.put(whoId, lead);
                    } else if (whoIdPrefix.equalsIgnoreCase('003')) {
                        Contact contact = new Contact(Id = whoId, Last_Activity_Datetime__c = System.now());
                        contsToUpdate.put(whoId, contact);
                    }
                }
                if (tsk.WhatId != null) {
                    String whatId = tsk.WhatId;
                    String whatIdPrefix = whatId.substring(0, 3);

                    //Only CTI would fill the CallType field
                    if (whatIdPrefix.equalsIgnoreCase('001')) {
                        Account acc = new Account(Id = whatId, Last_Activity_Datetime__c = System.now());
                        accountsToUpdate.put(whatId, acc);
                    } else if (whatIdPrefix.equalsIgnoreCase('006')) {
                        Opportunity opp = new Opportunity(Id = whatId, Last_Activity_Datetime__c = System.now());
                        oppsToUpdate.put(whatId, opp);
                    } else if (whatIdPrefix.equalsIgnoreCase('701')) {
                        Campaign campaign = new Campaign(Id = whatId, Last_Activity_Datetime__c = System.now());
                        campsToUpdate.put(whatId, campaign);
                    } else if (whatIdPrefix.equalsIgnoreCase('800')) {
                        Contract contract = new Contract(Id = whatId, Last_Activity_Datetime__c = System.now());
                        contrsToUpdate.put(whatId, contract);
                    }
                }
            }
        }

        try {
            if (accountsToUpdate.size() > 0) {
                update accountsToUpdate.values();
            }
            if (leadsToUpdate.size() > 0) {
                update leadsToUpdate.values();
            }
            if (oppsToUpdate.size() > 0) {
                update oppsToUpdate.values();
            }
            if (contsToUpdate.size() > 0) {
                update contsToUpdate.values();
            }
            if (contrsToUpdate.size() > 0) {
                update contrsToUpdate.values();
            }
            if (campsToUpdate.size() > 0) {
                update campsToUpdate.values();
            }
        } catch (System.DmlException e) {
            System.debug(e.getMessage());
        }
    }
}