/*******************************************************************
* Class CampaignMarketingAssetLinkController
* 
* Author: Linus Löfberg @ 4front
*
* Changelog:
* 2015-08-03 CR# 45769 Marketing Assets object update
*
**********************************************************************/

public with sharing class CampaignMarketingAssetLinkController {
	private final Link_Marketing_Asset__c myLinkMarketingAsset;
    public Marketing_Asset_NEW__c MarketingAsset { get; set; }
    private String campaignId { get; set; }
    private String marketingAssetId { get; set; }

    public CampaignMarketingAssetLinkController(ApexPages.StandardController stdController) {
        this.myLinkMarketingAsset = (Link_Marketing_Asset__c)stdController.getRecord();
        MarketingAsset = new Marketing_Asset_NEW__c();

        if (System.currentPageReference().getParameters().containsKey('MarketingAssetId')) {
            marketingAssetId = System.currentPageReference().getParameters().get('MarketingAssetId');
        } else if (System.currentPageReference().getParameters().containsKey('CampaignId')) {
            campaignId = System.currentPagereference().getParameters().get('CampaignId');
        }
    }

    public String getOrigin() {
        if (System.currentPageReference().getParameters().containsKey('Origin')) {
            return System.currentPageReference().getParameters().get('Origin');
        } else {
            return '';
        }
    }

    public PageReference goBack() {
        PageReference goBackRef = null;
        if (getOrigin() == 'Campaign') {
            goBackRef = new PageReference('/' + campaignId);
            goBackRef.setRedirect(true);
        } else if (getOrigin() == 'MarketingAsset') {
            goBackRef = new PageReference('/' + marketingAssetId);
            goBackRef.setRedirect(true);
        }
        return goBackRef;
    }

    public PageReference customSaveLink() {
        PageReference redirect = null;
        if (getOrigin() == 'Campaign' && myLinkMarketingAsset.Marketing_Asset_NEW__c != null) {
            myLinkMarketingAsset.Campaign__c = campaignId;
            redirect = new PageReference('/' + campaignId);
            redirect.setRedirect(true);
            insert myLinkMarketingAsset;
        } else if (getOrigin() == 'MarketingAsset' && myLinkMarketingAsset.Campaign__c != null) {
            myLinkMarketingAsset.Marketing_Asset_NEW__c = marketingAssetId;
            redirect = new PageReference('/' + marketingAssetId);
            redirect.setRedirect(true);
            insert myLinkMarketingAsset;
        }
        return redirect;
    }

    public PageReference saveNewMarketingAsset() {
        PageReference redirect = null;
        if (MarketingAsset != null && campaignId != null) {
            MarketingAsset.Campaign__c = campaignId;
            insert MarketingAsset;

            myLinkMarketingAsset.Campaign__c = campaignId;
            myLinkMarketingAsset.Marketing_Asset_NEW__c = MarketingAsset.Id;
            insert myLinkMarketingAsset;

            redirect = new PageReference('/' + campaignId);
            redirect.setRedirect(true);
            return redirect;
        }
        return redirect;
    }
}