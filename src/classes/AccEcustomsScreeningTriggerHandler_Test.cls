/**     * File Name:AccEcustomsScreeningTriggerHandler_Test
        * Description : This is test class for AccountEcustomsScreeningTriggerHandler
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Jan 26th 2019 RamakrishnaKini     Added new  logic.
*/
@isTest
private class AccEcustomsScreeningTriggerHandler_Test {
    
    @testSetup static void testSetup() {
        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('USD');
        //QuoteTestHelper.createCustomSettings();
        Semaphores.SetAllSemaphoresToFalse();
    }

    @isTest static void Testmethod2() {
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com',
        INT_NetSuite_InternalID__c = '5307');

        System.runAs(u) {
            Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, false);
            testAccount.ECUSTOMS__Screening_Trigger__c = false;
            insert testAccount;

            Account Testacc1 = [select id, ECUSTOMS__Screening_Trigger__c from Account Where Id = :testAccount.Id];
            Testacc1 .ECUSTOMS__Screening_Trigger__c = true;
            update Testacc1 ;

            AccountEcustomsScreeningTriggerHandler accecustomshandler = new AccountEcustomsScreeningTriggerHandler();
        }
    }
}