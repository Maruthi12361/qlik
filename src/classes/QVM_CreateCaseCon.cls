/*
@author: Anthony Victorio, Model Metrics
@date: 03/14/2012
@description: Controller for the QVM new case page
*/
public with sharing class QVM_CreateCaseCon {
	
	public Case newCase {get;set;}
	
	public QVM_CreateCaseCon() {
		
		newCase = new Case();
		QVM_Settings__c settings = QVM_Settings__c.getOrgDefaults();
		
		newCase.RecordTypeId = QVM.getRecordType('Case', settings.Default_Case_Record_Type__c);
		newCase.AccountId = QVM.getUserAccountId();
		newCase.ContactId = QVM.getUserContactId();
		newCase.Status = settings.Default_Case_Status__c;
		newCase.Origin = settings.Default_Case_Origin__c;
		newCase.Priority = settings.Default_Case_Priority__c;
		
	}
	
	public PageReference saveCase() {
			
        try {
        	Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            newCase.setOptions(dmo);
            insert newCase;
        	PageReference next = Page.QVM_ControlPanelMain;
            return next;
        } catch (Exception e) { ApexPages.addMessages(e); return null; }
		
	}
	
	public PageReference cancel() {
		PageReference next = Page.QVM_ControlPanelMain;
		return next;
	}

}