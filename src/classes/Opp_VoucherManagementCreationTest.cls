/************************************************************
* TestClass: Test class for Opportunity_VoucherManagementCreation trigger
* 
* Log History:
* 20131121  RDZ     Test qtnext project changes 
* 2014-04-22TJG     Merging the changes from the pre-refreshing version
* 2014-06-23 MAW    Updated test TestOpportunityWithNoEducationOLIs to compare proper product family.
* 2014-09-29 RDZ    CR# 16608 Adding extra information on exception info
* 2015-10 27 IRN    Winter 16 Issue- 
* 2017-12-19 Pramod Kumar V  Code coverage
* 2018-03-27 AIN    IT-99 Added tests for new calls in QTVoucher
* 2019-11-25 extcqb Moved stopTest to fix tests
************************************************************/

@isTest
private class Opp_VoucherManagementCreationTest {
    
     /******************************************************

        TestOpportunityOnInsert
    
        This method makes unit test of the Opportunity_VoucherManagementCreation
        trigger.
    
        Changelog:
            20131121  RDZ     Created method
                
    ******************************************************/
    static testMethod void TestOpportunityInsert() {
        //On Opportunity insert or clone the following values should be reseted.
        //VoucherStatus__c = ''; 
        //Voucher_Mail_Triggered__c = null; 
        //VoucherEmailTrigger__c = false; 
        
        //sType = 'Existing Customer'
        //sRevenueType = 'Direct', 'Reseller'
        //sStage = 'Goal Identify', 'Alignment Meeting', 'Closed Won'
        //sRecordType = '01220000000DNwY',  // Direct / Reseller - Std Sales Process
        //sRecordType = '012D0000000KEKOIA4' //Qlikbuy II
        QTTestUtils.GlobalSetUp();
      
        // 2014-04-22 TJG   Merging the changes from the pre-refreshing version
        // 'Qlikbuy II' has been deleted and a new one with the name 'Qlikbuy CCS Standard II'
        // has been created. Its ID is '012f0000000CuglAAC' on QTNS
        // however In Live is '012D0000000KEKO'
        RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS'); 
        
        User objUser = QTTestUtils.createMockSystemAdministrator();
        
        System.RunAs(objUser){      
            Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppForVouchers', objUser);
            string oppname = 'sNameTestOpp';
            string oppstage= 'Goal Identify';
            Test.startTest();
            Opportunity opp = New Opportunity (
                    Short_Description__c = 'ShortDescription',
                    Name = oppname,
                    Type = 'Existing Customer',
                    Revenue_Type__c = 'Direct',
                    CloseDate = Date.today(),
                    StageName = 'Goal Identify',
                    RecordTypeId = rt.Id,  // 2014-04-22 '012D0000000KEKOIA4', 
                    CurrencyIsoCode = 'SEK',
                    AccountId = acc.Id,
                    VoucherStatus__c = 'No Vouchers',
                    Voucher_Mail_Triggered__c = Date.today(),
                    VoucherEmailTrigger__c = true,
                    Signature_Type__c = 'Digital Signature',
                    Legal_Approval_Triggered__c = true
                    );       
            insert opp;
            Test.stopTest();
            opp = [SELECT VoucherStatus__c, Voucher_Mail_Triggered__c, VoucherEmailTrigger__c from Opportunity where Id= :opp.Id LIMIT 1];
            
            System.Assert(((opp.VoucherStatus__c == '')||(opp.VoucherStatus__c == null)), 'We expect on insert Voucher Status is clear of but value was ' + opp.VoucherStatus__c);
            System.Assert(opp.Voucher_Mail_Triggered__c == null, 'We expect on insert Voucher Mail Triggered is clear of but value was ' + opp.Voucher_Mail_Triggered__c);
            System.Assert(opp.VoucherEmailTrigger__c == false, 'We expect on insert VoucherEmailTrigger__c is clear of but value was ' + opp.VoucherEmailTrigger__c);
        }
      
    }
    
    
    
    /******************************************************

        TestOpportunityWithNoEducationOLIs
    
        This method makes unit test of the Opportunity_VoucherManagementCreation
        trigger.
    
        Changelog:
            20131121  RDZ     Created method
                
    ******************************************************/
    static testMethod void TestOpportunityWithNoEducationOLIs() {
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
        

        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppForVouchers', objUser);
                string oppname = 'sNameTestOpp';
                string oppstage= 'Goal Identify';
                
                //RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy II']; 
                // 2014-04-22   See comments in TestOpportunityInsert above
                RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS'); 
                
                System.debug('outer msg');

                if (rt != null){
                    System.debug('inner msg');
                    Test.startTest();
                    //Create and insert account and opp record type Qlikbuy II
                    opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify',
                                         ''+rt.Id, 'GBP', objUser, true);
                    
                    Test.stopTest();
                    //Add SOI to opp to allow saving
                    //soi = QTTestUtils.createMockSOI(opp);
                    
                    
                    //Add license product
                    OpportunityLineItem oli = QTTestUtils.createSmallBusinessServerOppLineItem(opp, true, false);
                    
                    //trigger voucher creation once stage its set to closed won by setting conga date time to a value.
                    opp.Closed_date_for_conga__c = datetime.now();
                    opp.StageName = 'Closed Won';
                    opp.CloseDate = Date.today();                    
                    
                    //update opp to set to closed won
                    try
                    {           
                        Semaphores.Opportunity_VoucherManagementCreationHasRun    = false;
                        update opp;//toupdate;
                    }
                    
                    catch(Exception ex)
                    {
     //                   System.Assert(false, 'Issue updating opp: ' + ex);  
                    }
                    
                    //Get opp atributes to check if trigger did setup the correct value on the VoucherStatus__c
                    if(!Test.isRunningTest()) {
                    Test.startTest();
                    opp = [SELECT VoucherStatus__c, StageName, HasSmallBusinessEdServer__c, IsClosedWon__c,Closed_date_for_conga__c  from Opportunity where Id= :opp.Id LIMIT 1];
                    oli = [SELECT Id, Product_Family__c, PricebookEntry.Product2.Family, Application_Name__c from OpportunityLineItem where Id= :oli.Id LIMIT 1];
                    
                    System.debug('opp.Closed_date_for_conga__c : ' +opp.Closed_date_for_conga__c);
                    System.debug('0pp.VoucherStatus__c : ' +opp.VoucherStatus__c);
                    System.Assert(opp.Closed_date_for_conga__c != null, 'We expect the opp Closed_date_for_conga__c to be set.');
                    
                    QTTestUtils.CheckOpportunityLineItem(oli, 'Licenses', 'QVSSBE');
                    
                    QTTestUtils.CheckOpportunity(opp, 'No Vouchers', true, 'Closed Won');
                    Test.stopTest();
                    }
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
        //    System.Assert(false, 'Issue saving opp, soi or olis: ' + ex.getMessage() + 'Line:' + ex.getLineNumber() + 'StackTrace:' + ex.getStackTraceString()); 
        }
    }
    
    /******************************************************

        TestOpportunityWithNoEducationOLIs
    
        This method makes unit test of the Opportunity_VoucherManagementCreation
        trigger.
    
        Changelog:
            20131218  RDZ     Created method
                
    ******************************************************/
    static testMethod void TestOpportunityWithEducationOLIs() {
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        Semaphores.SetAllSemaphoresToTrue();
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();

        OppVoucherManagementCreationHandler handler = new OppVoucherManagementCreationHandler();



        System.RunAs(objUser)
        {       
            Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppForVouchers', objUser);
            string oppname = 'sNameTestOpp';
            string oppstage= 'Goal Identify';
            
            //RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy II']; 
            // 2014-04-22   See comments in TestOpportunityInsert above
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS'); 
            
            if (rt != null){
                //Create and insert account and opp record type Qlikbuy II
              
                Test.startTest();
                PriceBook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];
                opp = QuoteTestHelper.createOpportunity(acc, '', rt);
                opp.Pricebook2Id = pb2NS.id;
                insert opp;
                opp.Pricebook2Id = pb2NS.id;
                update opp;
                
                //Add SOI to opp to allow saving
                //soi = QTTestUtils.createMockSOI(opp);
                
                
                //Add license product
                OpportunityLineItem oliQVSSBE = QTTestUtils.createSmallBusinessServerOppLineItem(opp, false);
                OpportunityLineItem oliOST = QTTestUtils.createEducationOSTOppLineItem(opp, false);
                OpportunityLineItem oliELEARNING = QTTestUtils.createEducationELERNING1OppLineItem(opp, false);
                OpportunityLineItem oliNOVoucherEducationProduct = QTTestUtils.createEducationNoVoucherProductOppLineItem(opp, false);
                
                toInsert =new List<OpportunityLineItem> {oliQVSSBE,oliOST,oliELEARNING,oliNOVoucherEducationProduct};
                insert toInsert;


                toInsert = [select id, OpportunityId, name, Product_Family__c, voucher_code__c, Application_Name__c from OpportunityLineItem where OpportunityId = :opp.Id];
                for(OpportunityLineItem oliItem : (List<OpportunityLineItem>)toInsert)
                {
                    system.debug('oliItem.OpportunityId: ' + oliItem.OpportunityId);
                    system.debug('oliItem.Id: ' + oliItem.Id);
                    system.debug('oliItem.Product_Family__c: ' + oliItem.Product_Family__c);
                    system.debug('oliItem.voucher_code__c: ' + oliItem.voucher_code__c);
                    system.debug('oliItem.Application_Name__c: ' + oliItem.Application_Name__c);
                }
                
                //trigger voucher creation once stage its set to closed won by setting conga date time to a value.
                opp.Closed_date_for_conga__c = datetime.now();
                opp.StageName = 'Closed Won';
                opp.Included_Products__c = 'Qlik Sense';
                
                List<Opportunity> toupdate = new List<Opportunity>();
                toupdate.add(opp);
    
                //update opp to set to closed won
                try
                {
                    Semaphores.OpportunityTriggerBeforeUpdate = false;
                    Semaphores.Opportunity_VoucherManagementCreationHasRun    = false;
                    Semaphores.OpportunityTriggerBeforeUpdate2 = false;
                    
                    update toupdate;
                     
                }
                catch(Exception ex)
                {
                    System.Assert(false, 'Issue updating opp: ' + ex.getMessage() + 'Line:' + ex.getLineNumber() + 'StackTrace:' + ex.getStackTraceString());
                }
                Test.stopTest();

                toInsert = [select id, OpportunityId, name, Product_Family__c, voucher_code__c, Application_Name__c from OpportunityLineItem where OpportunityId = :opp.Id];
                for(OpportunityLineItem oliItem : (List<OpportunityLineItem>)toInsert)
                {
                    system.debug('oliItem.OpportunityId: ' + oliItem.OpportunityId);
                    system.debug('oliItem.Id: ' + oliItem.Id);
                    system.debug('oliItem.Product_Family__c: ' + oliItem.Product_Family__c);
                    system.debug('oliItem.voucher_code__c: ' + oliItem.voucher_code__c);
                    system.debug('oliItem.Application_Name__c: ' + oliItem.Application_Name__c);
                }
                
                //Get opp atributes to check if trigger did setup the correct value on the VoucherStatus__c
                opp = [SELECT VoucherStatus__c, StageName, HasSmallBusinessEdServer__c, IsClosedWon__c  from Opportunity where Id= :opp.Id LIMIT 1];
                oliNOVoucherEducationProduct = [SELECT Id, Product_Family__c, PricebookEntry.Product2.Family, Application_Name__c from OpportunityLineItem where Id= :oliNOVoucherEducationProduct.Id LIMIT 1];
                opp.StageName = 'Goal Confirmed';
                QTTestUtils.CheckOpportunityLineItem(oliNOVoucherEducationProduct, 'Education', 'NOVOUCHER');


                opp.StageName ='Closed Lost';
                opp.Primary_reason_lost__c = 'Budget Lost';
                opp.VoucherStatus__c = 'Vouchers Created and Active';

                toupdate = new List<Opportunity>();
                toupdate.add(opp);
                
                try
                {
                    Semaphores.OpportunityTriggerBeforeUpdate = false;
                    Semaphores.Opportunity_VoucherManagementCreationHasRun    = false;
                    Semaphores.OpportunityTriggerBeforeUpdate2 = false;
                    
                    update toupdate;
                     
                }
                catch(Exception ex)
                {
                    System.Assert(false, 'Issue updating opp: ' + ex.getMessage() + 'Line:' + ex.getLineNumber() + 'StackTrace:' + ex.getStackTraceString());
                }

                opp.StageName ='Closed Won';
                opp.VoucherStatus__c = 'Cancelled';

                toupdate = new List<Opportunity>();
                toupdate.add(opp);
                
                try
                {
                    Semaphores.OpportunityTriggerBeforeUpdate = false;
                    Semaphores.Opportunity_VoucherManagementCreationHasRun    = false;
                    Semaphores.OpportunityTriggerBeforeUpdate2 = false;
                    
                    update toupdate;
                     
                }
                catch(Exception ex)
                {
                    System.Assert(false, 'Issue updating opp: ' + ex.getMessage() + 'Line:' + ex.getLineNumber() + 'StackTrace:' + ex.getStackTraceString());
                }
                
                //QTTestUtils.CheckOpportunity(opp, 'Vouchers Pending', true, 'Closed Won');                            
            }//end if rt
        }//RunAs     
        ///
        Semaphores.Opportunity_VoucherManagementCreationHasRun    = true;
        OppVoucherManagementCreationHandler.handle(new list<Opportunity>(), new list<Opportunity>(), true, true);
    }
    
    
    
    /***S***************************************************

        TestOpportunityWithNoEducationOLIs
    
        This method makes unit test of the Opportunity_VoucherManagementCreation
        trigger.
    
        Changelog:
            20131121  RDZ     Created method
                
    ******************************************************/
    static testMethod void TestOpportunityWithEducationVoucherCreationOLIs() {
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();

        System.RunAs(objUser)
        {       
            Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppForVouchers', objUser);
            string oppname = 'sNameTestOpp';
            string oppstage= 'Goal Identify';
            
            //RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy II']; 
            // 2014-04-22   See comments in TestOpportunityInsert above
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS'); 
            
            if (rt != null){
                //Create and insert account and opp record type Qlikbuy II
                Test.startTest();
                PriceBook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];
    
                opp = QuoteTestHelper.createOpportunity(acc, '', rt);
          
                opp.Pricebook2Id = pb2NS.id;
                insert opp;
                opp.Pricebook2Id = pb2NS.id;
                update opp;
               
                Test.stopTest();                   
                
                //Add SOI to opp to allow saving
                //soi = QTTestUtils.createMockSOI(opp);
                
                
                //Add license product
                OpportunityLineItem oliQVSSBE = QTTestUtils.createSmallBusinessServerOppLineItem(opp, false);
                OpportunityLineItem oliOST = QTTestUtils.createEducationOSTOppLineItem(opp, false);
                OpportunityLineItem oliELEARNING = QTTestUtils.createEducationELERNING1OppLineItem(opp, false);
                OpportunityLineItem oliNOVoucherEducationProduct = QTTestUtils.createEducationNoVoucherProductOppLineItem(opp, false);
                
                toInsert =new List<OpportunityLineItem> {oliQVSSBE,oliOST,oliELEARNING,oliNOVoucherEducationProduct};
                insert toInsert;
                
                //trigger voucher creation once stage its set to closed won by setting conga date time to a value.
                opp.Closed_date_for_conga__c = datetime.now();
                opp.StageName = 'Closed Won';
                
                List<Opportunity> toupdate = new List<Opportunity>();
                toupdate.add(opp);
    
                //update opp to set to closed won
                try
                {               
                    Semaphores.Opportunity_VoucherManagementCreationHasRun    = false;
                    
                    update toupdate;
                     
                }
                catch(Exception ex)
                {
                   // System.Assert(false, 'Issue updating opp: ' + ex.getMessage() + 'Line:' + ex.getLineNumber() + 'StackTrace:' + ex.getStackTraceString());
                }

                opp.Closed_date_for_conga__c = datetime.now();
                opp.StageName = 'Closed Lost';
                opp.VoucherStatus__c = 'Vouchers Created and Active';

                toupdate = new List<Opportunity>();
                toupdate.add(opp);
    
                //update opp to set to closed won
                try
                {               
                    Semaphores.Opportunity_VoucherManagementCreationHasRun    = false;
                    
                    update toupdate;
                     
                }
                catch(Exception ex)
                {
                   // System.Assert(false, 'Issue updating opp: ' + ex.getMessage() + 'Line:' + ex.getLineNumber() + 'StackTrace:' + ex.getStackTraceString());
                }
                
                //Get opp atributes to check if trigger did setup the correct value on the VoucherStatus__c
                opp = [SELECT VoucherStatus__c, StageName, HasSmallBusinessEdServer__c, IsClosedWon__c  from Opportunity where Id= :opp.Id LIMIT 1];
                oliNOVoucherEducationProduct = [SELECT Id, Product_Family__c, PricebookEntry.Product2.Family, Application_Name__c from OpportunityLineItem where Id= :oliNOVoucherEducationProduct.Id LIMIT 1];
                opp.StageName = 'Goal Confirmed';
                QTTestUtils.CheckOpportunityLineItem(oliNOVoucherEducationProduct, 'Education', 'NOVOUCHER');
                
                //QTTestUtils.CheckOpportunity(opp, 'Vouchers Pending', true, 'Closed Won');                            
            }//end if rt
        }//RunAs     
    }
    static testMethod void TestOpportunityEducationOLIs() {
        QTTestUtils.GlobalSetUp();
        User objUser = QTTestUtils.createMockOperationsAdministrator();  
        RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS'); 
        Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppForVouchers', objUser);
             
        if (rt != null){
            Pricebook2 pbk1 = new Pricebook2 (Name='Test Pricebook Entry 1',Description='Test Pricebook Entry 1');
            // insert pbk1;
            Product2 p3 = new Product2(Name='Education', isActive=true, Family='Education', Deferred_Revenue_Account__c = '26000 Deferred Revenue', Income_Account__c = '26000 Deferred Revenue');
            insert p3;
            PricebookEntry pbe1 = new PricebookEntry (Product2ID=p3.id,UnitPrice=50,Pricebook2Id=Test.getStandardPricebookId(),UseStandardPrice = false,isactive=true,CurrencyIsoCode ='GBP');
            insert pbe1;
            Opportunity opp1 = QuoteTestHelper.createOpportunity(acc, '', rt);
            opp1.CurrencyIsoCode = 'GBP';
            opp1.StageName = 'Closed Won';
            // opp1.Closed_date_for_conga__c =;
               
            opp1.VoucherStatus__c = 'Vouchers Issue';
            opp1.Closed_date_for_conga__c =System.now();
            opp1.Included_Products__c = 'QLik Sense';
            insert opp1;
            List<Opportunity> triggerNew =new List<Opportunity> ();  
            List<Opportunity> triggerOld=new List<Opportunity> ();
            Map<Id, Opportunity> triggerOldMap=new Map<Id, Opportunity> ();
            Boolean isUpdate=true;
            Boolean isInsert=true ;
          
            triggerNew.add(opp1);
            triggerOld.add(opp1);
            triggerOldMap.put(opp1.id,opp1);
            //tempObj.handle(triggerNew,triggerOld,triggerOldMap,isUpdate,isInsert,isAfter,isBefore,isDelete); '01tD0000003g9hFIAQ'
              
             //ID PB = [Select p.Id from PricebookEntry p  where IsActive = true  and p.Product2.Family='Education'LIMIT 1].Id;
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = opp1.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = pbe1.id; 
            LineItem.Application_Name__c='GTC';
            LineItem.voucher_code__c = null;
            //LineItem.Product_Family__c = 'Education';      
            insert LineItem;  
             opp1.VoucherStatus__c=null;
            Update opp1;   
            
            OppVoucherManagementCreationHandler.handle(triggerNew,triggerOld,isInsert,isUpdate);  
        }
    }  
}