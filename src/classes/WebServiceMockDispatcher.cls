/*****************************************************************************************
* 2015-09-28 AIN Mock dispatcher for webservice callouts
* This class will catch all callouts and redirect the callout to the relevant mock for that WS
* These mocks are located in their respective files and can be used independently, but I recommend using this 
* dispatcher.
*
* To use the Mock dispatcher in your code, use the following 2 lines.
*
* WebServiceMockDispatcher mock = new WebServiceMockDispatcher();
* test.setMock(WebServiceMock.class, mock); 
*
* As SFdC has trouble handling multiple mocks for different web service responses, this class is needed.
* Our triggers can easily fire off multiple callouts in one transaction
* Using this class we also put all webservice mock callout maintenance in one place

****************************************************************************************/

@isTest
public class WebServiceMockDispatcher implements WebServiceMock {

  
  public WebServiceMockDispatcher() 
  {
  }
  public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

    //When a callout is made, the stub is used to figure out which webservice that made the callout
    //We redirect the callout to that specific web service mock class.

    system.debug('WebServiceMockDispatcher start');

    if(stub instanceof sfUtilsQlikviewCom.ServiceSoap)
    {
      system.debug('Creating mock of type sfUtilsQlikviewCom');
      new sfUtilsMock().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
    }
    else if(stub instanceof sharinghandlerQlikviewCom.ServiceSoap)
    {
      system.debug('Creating mock of type sharinghandlerQlikviewCom');
      new sharinghandlerMock().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
    }
    else if(stub instanceof ulcv3QlikviewCom.ServiceSoap)
    {
      system.debug('Creating mock of type ulcv3QlikviewCom');
      new ULCMock().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
    }else if(stub instanceof efQTPaymentGateway.ServiceSoap)
    {
      system.debug('Creating mock of type efQTPaymentGateway');
      new efQTPaymentGatewayMock().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
    }
    else if(stub instanceof MetadataService.MetadataPort)
    {
      system.debug('Creating mock of type MetadataService');
      new MetadataServiceMock().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType); 
    }
    else if(stub instanceof S3.AmazonS3)
    {
      system.debug('Creating mock of type S3');
      new S3Mock().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType); 
    } 
    ///////////////mock
    else if (stub instanceof qtvoucherQliktechCom.ServiceSoap) 
    {
      system.debug('Creating mock of type qtvoucherQliktechCom');
      new QTVoucherMock().doInvoke(stub, request, response, endpoint, soapAction, requestName, responseNS, responseName, responseType);
    }
    else
      system.debug('no specific mock found');
    return;
  }
}