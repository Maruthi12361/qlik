/**************************************************
*
* Change Log:
* 
* 2017-03-06   Created by external vendor as part of the NVM Replacement project - adds recording buttons to the B+S Dialer widget
*
**************************************************/
global with sharing class RecordingControlsController {
    
    /////////////// PUBLIC SECTION /////////////////////////

    public class RecordingControlsException extends Exception {}

    @RemoteAction
    global static String retrieveControlButtonsLabels() {
        Map<String, String> labels = new Map<String, String>();

        labels = getControlButtonsLabels();

        return JSON.serialize(labels);
    }

    @RemoteAction
    global static String retrieveUserInfo()
    {
        User user = [SELECT
        cnx__Agent_PasswordEncrypted__c,
        cnx__Agent_Id__c,
        cnx__Agent_Phone_Extension__c,
        Name, FirstName, LastName, Id, Alias, CommunityNickname
        FROM User
        WHERE Id = :UserInfo.getUserId()
        LIMIT 1];

        //instantiate the generator
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeObjectField('user', user);
        gen.writeEndObject();

        return gen.getAsString();
    }

    @RemoteAction
    global static void writeRecordingLink(String taskId, String fieldName, String link)
    {
        Task task;
        String sanitizedFieldName = String.escapeSingleQuotes(fieldName);

        try {
            task = Database.query('SELECT ' + sanitizedFieldName + ' FROM Task WHERE Id = :taskId LIMIT 1');
        } catch(Exception ex) {
            throw new RecordingControlsException('Task with id ' + taskId + ' not found. Exception: ' + ex.getMessage());
        }

        Map<String, Schema.SObjectField> fieldTypeMap = Schema.SObjectType.Task.fields.getMap();
        Schema.SObjectField field = fieldTypeMap.get(fieldName);
        Schema.DisplayType fieldType = field.getDescribe().getType();

        if (fieldType != Schema.DisplayType.String && fieldType != Schema.DisplayType.URL) {
            throw new RecordingControlsException('Field ' + fieldName + ' is not of type String or URL');
        }

        //Check if the recording link is short enough for the field
        Integer fieldLength = field.getDescribe().getLength();
        if (fieldLength < link.length()) {
            throw new RecordingControlsException('Field ' + fieldName + ' is not long enough (must be at least ' + link.length() + ' to save that link)');
        }

        //Add the recording link to task
        task.put(fieldName, link);
        update task;
    }
    
    /////////////// PRIVATE SECTION /////////////////////////

    /* RETRIEVE CUSTOM LABELS FOR CONTROL BUTTON COMPONENT */
    private static Map<String, String> getControlButtonsLabels() 
    {
        Map<String, String> labels = new Map<String, String>();
        
        labels.put('RecordingRecordTitle', System.Label.I18n_RecordingControls_RecordingRecordTitle);
        labels.put('RecordingRecordText', System.Label.I18n_RecordingControls_RecordingRecordText);
        labels.put('RecordingPauseTitle', System.Label.I18n_RecordingControls_RecordingPauseTitle);
        labels.put('RecordingPauseText', System.Label.I18n_RecordingControls_RecordingPauseText);
        labels.put('RecordingResumeTitle', System.Label.I18n_RecordingControls_RecordingResumeTitle);
        labels.put('RecordingResumeText', System.Label.I18n_RecordingControls_RecordingResumeText);
        labels.put('RecordingRestartTitle', System.Label.I18n_RecordingControls_RecordingRestartTitle);
        labels.put('RecordingRestartText', System.Label.I18n_RecordingControls_RecordingRestartText);
        labels.put('RecordingDeleteTitle', System.Label.I18n_RecordingControls_RecordingDeleteTitle);
        labels.put('RecordingDeleteText', System.Label.I18n_RecordingControls_RecordingDeleteText);
        labels.put('RecordingMetadataTitle', System.Label.I18n_RecordingControls_RecordingMetadataTitle);
        labels.put('RecordingMetadataText', System.Label.I18n_RecordingControls_RecordingMetadataText);
        labels.put('RecordingStartTitle', System.Label.I18n_RecordingControls_RecordingStartTitle);
        labels.put('RecordingStartText', System.Label.I18n_RecordingControls_RecordingStartText);
        labels.put('RecordingStopTitle', System.Label.I18n_RecordingControls_RecordingStopTitle);
        labels.put('RecordingStopText', System.Label.I18n_RecordingControls_RecordingStopText);
        labels.put('RecordingDisabledText', System.Label.I18n_RecordingControls_DisabledText);
        
        // --- OTHER ---
        labels.put('RecordingControls', System.Label.I18n_RecordingControls_RecordingControls);
        
        return labels;
    }
}