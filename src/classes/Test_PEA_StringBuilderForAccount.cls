/*
	Test class: Test_PEA_StringBuilderForAccount
	
	2013-02-19	CCE	Initial development	for CR# 6833 https://eu1.salesforce.com/a0CD000000U7aL2
					The trigger is used to help populate a field on the Account which will hold
					a ; separated list of partner Experise Areas.	
    07.02.2017   RVA :   changing methods				
 */
@isTest
private class Test_PEA_StringBuilderForAccount {

    static testMethod void TestBuildingString() {
        System.debug('Test_PEA_StringBuilderForAccount: Starting');
		/*
		Subsidiary__c testSub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSub.Id						
		);
		insert QTComp;*/
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'United States', 'United States');
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
								
		Contact TestDEContact = new Contact(
			FirstName = 'Mr Partner',
			LastName = 'PP',
			AccountId = TestPartnerAccount.Id
		);
		insert TestDEContact;
		
		Test.startTest();		    	
		//Build the Partner Expertise Areas
		list<Partner_Expertise_Area__c> peaList = new list<Partner_Expertise_Area__c>();
		Partner_Expertise_Area__c pea1 = new Partner_Expertise_Area__c(
			Partner_Account_Name__c = TestPartnerAccount.Id,
			Dedicated_Expertise_Contact__c = TestDEContact.Id,
			Expertise_Area__c = 'Healthcare'
		);				
		peaList.add(pea1);
		Partner_Expertise_Area__c pea2 = new Partner_Expertise_Area__c(
			Partner_Account_Name__c = TestPartnerAccount.Id,
			Dedicated_Expertise_Contact__c = TestDEContact.Id,
			Expertise_Area__c = 'Communications'
		);
		peaList.add(pea2);
		Partner_Expertise_Area__c pea3 = new Partner_Expertise_Area__c(
			Partner_Account_Name__c = TestPartnerAccount.Id,
			Dedicated_Expertise_Contact__c = TestDEContact.Id,
			Expertise_Area__c = 'QlikView Mobile'
		);
		peaList.add(pea3);				
		insert peaList;
		Test.stopTest();
		
		//Test we have added the Partner Expertise Area - Expertise Area values to the Partner Expertise List   
		Account TestAcc = [Select Id, Partner_Expertise_List__c from Account where Id = :TestPartnerAccount.Id];
        System.assert(TestAcc.Partner_Expertise_List__c.contains('Healthcare'));
	    System.assert(TestAcc.Partner_Expertise_List__c.contains('Communications'));
	    System.assert(TestAcc.Partner_Expertise_List__c.contains('QlikView Mobile'));
       
		//test removing Partner Expertise Areas       
        delete peaList;
        TestAcc = [Select Id, Partner_Expertise_List__c from Account where Id = :TestPartnerAccount.Id];
        System.assertEquals(null, TestAcc.Partner_Expertise_List__c);
	    
        System.debug('Test_PEA_StringBuilderForAccount: Finishing');
    }
}