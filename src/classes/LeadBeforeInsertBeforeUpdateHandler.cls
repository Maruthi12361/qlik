/*****************************************************************************************************************
 Class: LeadBeforeInsertBeforeUpdateHandler
 
 Changelog: 
 
 201302113  TJG Created for Data Dispatch project phase I

 Populate segmentation fields on the Lead – trigger on the Lead

    Once the Probable Account (Probable_Account__c) on the Lead is selected, key segmentation fields on the Lead will be populated:
        No of Employee, text type (No._of _Employees__c, custom field not the standard one named the same)
        SIC Code Name, lookup type (SIC_Code_Name__c)
        Yearly Revenue, text type (Annual_Turnover__c)
        Named Account, checkbox (Named_Account__c from the Account to Named_Account_Lead__c on the Lead)
        Segment, picklist type (Segment_New__c from the Account to Segment__c on the Lead)
        Company, text type ( Name from the Account to Company on the Lead)
    On Update any existing data in these fields will be overwritten.
    These fields will be automatically emptied if the user deletes the probable account 
    (for use case when Probable Account had a value but it has been deleted fields should be set to null).

    Once Lead validation is done and filed Data_Validated__c (checkbox type) is set to True 
    field Validated_By__c should be populated with the user who has validated the lead.
    Data_Validated__c field gets updated (checked) by the workflow rule

 	20130321    TJG add three more fields as requested
                    Employee Range, picklist type (Site_Employee_Range__c )
                    Owner of the Linked Account, lookup type (Owner from the Account to Owner_of_the_Linked_Account__c on the Lead)
                    Link to Account ID, text type (Id from the Account to Link_to_Account_ID__c on the Lead
	20130703    TJG Case 00186262 https://eu1.salesforce.com/500D000000QnpS3
                    Catch the situation of probable account has been deleted
	20150126    CCE CR# 16699 https://eu1.salesforce.com/a0CD000000ksoid Add Key Account field
	20150422    CCE CR# 22167 https://eu1.salesforce.com/a0CD000000pcfqd Add Navision Type for Mkto field
 	2017-10-25 AYS BMW-402 : Migrated from LeadBeforeInsertBeforeUpdateTrigger.trigger.
 	2018-08-01 ext_bad IT-852: only Academic Program Group can make changes to Lead.Primary_Job_Function__c field
******************************************************************************************************************/


public class LeadBeforeInsertBeforeUpdateHandler {
	static final String DEBUGPREFIX = '-DDDDDDD-';

	public LeadBeforeInsertBeforeUpdateHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld, Boolean isInsert) {
	    System.Debug(DEBUGPREFIX + 'Starting LeadBeforeInsertBeforeUpdateTrigger');
	    Set<Id> accountIds = new Set<Id>();
	    if (isInsert) {
	        for(Lead lead : triggerNew) {
	            // if validated, save who did it
	            if (lead.Data_Validated__c == true)
	            {
	                lead.Validated_By__c = UserInfo.getUserId();
	            }           
	            // populate the six fields from probabl account
	            if (lead.Probable_Account__c != null) {
	                accountIds.Add(lead.Probable_Account__c);
	            }
	        }
	    }
	    else {
	        // this is update trigger
			Group grp = [SELECT Id FROM Group WHERE DeveloperName = 'Academic_Program_Team' LIMIT 1];
			List<GroupMember> members = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = : grp.Id];
			List<String> userIds = new List<String>();
			for (GroupMember member : members) {
				userIds.add(member.UserOrGroupId);
			}
	            
	        for (Integer i = 0; i < triggerNew.size(); i++) {
	            // if validated, save who did it
	            if (triggerOld[i].Data_Validated__c != true && triggerNew[i].Data_Validated__c == true){
	                triggerNew[i].Validated_By__c = UserInfo.getUserId();
	            }
	            else if (triggerNew[i].Data_Validated__c != true){
	                if (triggerNew[i].Validated_By__c != null){
	                    triggerNew[i].Validated_By__c = null;
	                }
	            }
	            
	            ID oldId = triggerOld[i].Probable_Account__c;
	            ID newId = triggerNew[i].Probable_Account__c;
	            
	            System.Debug(DEBUGPREFIX + ' oldId = ' + oldId);
	            //System.Debug(DEBUGPREFIX + ' newId = ' + newId);

	            // newly assigned or updated probable account
	            if (newId != null){
	                System.Debug(DEBUGPREFIX + ' newId = ' + newId);
	                accountIds.Add(newId);
	            }
	            // probable account deleted, clear all six fields
	            if (newId == null && oldId != null){
	                triggerNew[i].Segment__c = null;
	                triggerNew[i].SIC_Code_Name__c = null;
	                triggerNew[i].No_of_Employees__c = null;
	                triggerNew[i].Named_Account_Lead__c = false;
	                //triggerNew[i].Company = null;
	                triggerNew[i].Annual_Turnover__c = null;
	                triggerNew[i].Site_Employee_Range__c = null;
	                triggerNew[i].Owner_of_the_Linked_Account__c = null;
	                triggerNew[i].Link_to_Account_ID__c = null;
	                triggerNew[i].Key_Account__c = false; //cce
	                triggerNew[i].Navision_Type_for_Mkto__c = null; //cce
	            }

				if (triggerNew[i].RecordTypeName__c == 'Academic Program Lead'
						&& triggerNew[i].Primary_Job_Function__c != triggerOld[i].Primary_Job_Function__c
						&& !userIds.contains(UserInfo.getUserId())) {
					triggerNew[i].addError('Only the Academic Program team can modify the "Primary Job Function" field.');
				}
	        }  
	    }
	    if (!accountIds.IsEmpty()) {
	        System.Debug(DEBUGPREFIX + ' accountsIds = ' + accountIds);
	        Map<ID, Account> IdToProbAcc = new Map<ID, Account>([Select Id, Segment_New__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account__c, Name, Annual_Turnover__c, Site_Employee_Range__c, OwnerId, Key_Account__c, Navision_Status__c From Account where Id in : accountIds]);
	        System.Debug(DEBUGPREFIX + ' IdToProbAcc = ' + IdToProbAcc);
	        for(Lead lead : triggerNew) {
	            Id probAccId = lead.Probable_Account__c;
	            System.Debug(DEBUGPREFIX + ' probAccId = ' + probAccId);
	            if (probAccId != null) {
	                Account probAcc = IdToProbAcc.Get(probAccId);
	                System.Debug(DEBUGPREFIX + ' probAcc = ' + probAcc);        
	                if (probAcc != null) {
	                    lead.Segment__c = probAcc.Segment_New__c;
	                    lead.SIC_Code_Name__c = probAcc.SIC_Code_Name__c;
	                    lead.No_of_Employees__c = probAcc.No_of_Employees__c;
	                    lead.Named_Account_Lead__c = probAcc.Named_Account__c;
	                    lead.Company = probAcc.Name;
	                    lead.Annual_Turnover__c = probAcc.Annual_Turnover__c;
	                    lead.Site_Employee_Range__c = probAcc.Site_Employee_Range__c;
	                    lead.Owner_of_the_Linked_Account__c = probAcc.OwnerId;
	                    lead.Link_to_Account_ID__c = probAcc.Id;
	                    lead.Key_Account__c = probAcc.Key_Account__c;    //cce
	                    lead.Navision_Type_for_Mkto__c = probAcc.Navision_Status__c;    //cce
	                }
	            }
	        }
	    }     
	    System.Debug(DEBUGPREFIX + 'Ending LeadBeforeInsertBeforeUpdateTrigger');
	}
}