/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_PartnerLibrarySearch extends QS_Search  {
    public QS_PartnerLibrarySearch(string searchTerms, integer numberItems) {

        system.debug('QS_PartnerLibrarySearch start');
        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();

        if (numberItems == 0) {
            pageSize = (integer)searchSettings[0].Page_Size__c;
        }
        else {
            pageSize = numberItems;
        } 
 
               
        startIndex = 0;
        this.searchTerms = searchTerms;
       
        totalSize = getCount();
        doSearch();
        
        system.debug('QS_PartnerLibrarySearch end');
    }

     public override void doSearch() {
   
        system.debug('doSearch start');
        if (searchTerms != null && searchTerms.length() > 1) {

            searchTerms = string.escapesinglequotes(searchTerms);
            List<String> arguments = new String[] {searchTerms, pageSize.format(), startIndex.format()};
            string queryString = string.format('FIND  \'\'{0}\'\'  RETURNING ContentVersion (ContentDocumentId, Title, Description, id, ContentModifiedDate where PublishStatus= \'\'P\'\'  and IsLatest = true and Framework_Category__c in (\'\'Development\'\',\'\'Deployment\'\', \'\'Scalability\'\',\'\'Administration\'\') order by ContentModifiedDate desc limit {1} offset {2}  )', arguments);
            if(test.isrunningtest()){
                queryString = string.format('FIND  \'\'{0}\'\'  RETURNING ContentVersion (ContentDocumentId, Title, Description, id, ContentModifiedDate order by ContentModifiedDate desc limit {1} offset {2}  )', arguments);
            }

            List<List<SObject>> searchList = Search.query(queryString);
                                

            ContentVersion[] contentList = ((List<ContentVersion>)searchList[0]);

            system.debug('contentList.Size(): ' + contentList.Size());
            
            List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
            //string url = 'https://' +  searchSettings[0].Instance_Name__c + '.salesforce.com/sfc/#version?selectedDocumentId=';
            string url = searchSettings[0].Qlik_com_Login_URL__c + 'https://' +  searchSettings[0].Instance_Name__c + '.salesforce.com/sfc/%23version?selectedDocumentId=';

            searchResults = new List<QS_SearchWrapper>();

            for (ContentVersion c : contentList) {
                QS_SearchWrapper w = new QS_SearchWrapper();
                w.title = c.Title;
                w.description = c.Description;
                w.url = url + c.ContentDocumentId;
                w.dateAdded = c.ContentModifiedDate.format('dd MMMM yyyy');
                //AIN 2015-05-27
                w.dateAddedDT = c.ContentModifiedDate;
                searchResults.add(w); 

            } 
        }
        system.debug('doSearch end');
               
    }



     private integer getCount() {

         if (searchTerms != null && searchTerms.Length() > 1) {
            //Special characters, must be preceeded by \ 
            //& | ! { } [ ] ( ) ^ ~ * : \ " ' + -
            
            searchTerms = searchTerms.replace('\'','\\\\\\\'');
            searchTerms = searchTerms.replace('&','\\\\&');
            searchTerms = searchTerms.replace('|','\\\\|');
            searchTerms = searchTerms.replace('!','\\\\!');
            searchTerms = searchTerms.replace('{','\\\\{');
            searchTerms = searchTerms.replace('}','\\\\}');
            searchTerms = searchTerms.replace('[','\\\\[');
            searchTerms = searchTerms.replace(']','\\\\]');
            searchTerms = searchTerms.replace('(','\\\\(');
            searchTerms = searchTerms.replace(')','\\\\)');
            searchTerms = searchTerms.replace('^','\\\\^');
            searchTerms = searchTerms.replace('~','\\\\~');
            searchTerms = searchTerms.replace('*','\\\\*');
            searchTerms = searchTerms.replace(':','\\\\:');
            searchTerms = searchTerms.replace('"','\\\\"');
            searchTerms = searchTerms.replace('+','\\\\+');
            searchTerms = searchTerms.replace('-','\\\\-');
            

            List<String> arguments = new String[] {searchTerms};
            string queryString = string.format('FIND  \'\'{0}\'\'  RETURNING ContentVersion (id where PublishStatus= \'\'P\'\' and IsLatest = true and Framework_Category__c in (\'\'Development\'\',\'\'Deployment\'\', \'\'Scalability\'\',\'\'Administration\'\')  order by ContentModifiedDate desc limit 2000 )', arguments);
            List<List<SObject>> searchList = Search.query(queryString);
            ContentVersion[] contentList = ((List<ContentVersion>)searchList[0]);
            return contentList.size();
         }
         else {
            return 0;
         }
    }


}