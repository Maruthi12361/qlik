/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Linus Löfberg
* @version        1.0
* @created        2019-10-17
* @modified       2019-10-17
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
*
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
private class Z_SubscriptionRatePlanTriggerHandlerTest {

    @testSetup
    static void testSetup() {

    }

    @isTest
    static void test_connect_subscription_pos() {
        Account_License__c testAccountLicense = new Account_License__c();
        testAccountLicense.Name = '1234567890';
        insert testAccountLicense;
        System.assertNotEquals(null, testAccountLicense.Id);

        Zuora__Subscription__c testSubsctiption = new Zuora__Subscription__c();
        testSubsctiption.Zuora__External_Id__c = 'IDK123456789KI';
        insert testSubsctiption;
        System.assertNotEquals(null, testSubsctiption.Id);

        Test.startTest();
        Zuora__SubscriptionRatePlan__c testRatePlan = new Zuora__SubscriptionRatePlan__c();
        testRatePlan.Zuora__Subscription__c = testSubsctiption.Id;
        testRatePlan.LicenseKey__c = '1234567890';
        insert testRatePlan;
        System.assertNotEquals(null, testSubsctiption.Id);

        testAccountLicense = [SELECT Id, Name, Subscription__c FROM Account_License__c WHERE Id = :testAccountLicense.Id];
        System.assertEquals(testAccountLicense.Subscription__c, testSubsctiption.Id);
        Test.stopTest();
    }

    @isTest
    static void test_connect_subscription_neg() {
        Account_License__c testAccountLicense = new Account_License__c();
        testAccountLicense.Name = '1234567899';
        insert testAccountLicense;
        System.assertNotEquals(null, testAccountLicense.Id);

        Zuora__Subscription__c testSubsctiption = new Zuora__Subscription__c();
        testSubsctiption.Zuora__External_Id__c = 'IDK123456789KI';
        insert testSubsctiption;
        System.assertNotEquals(null, testSubsctiption.Id);

        Test.startTest();
        Zuora__SubscriptionRatePlan__c testRatePlan = new Zuora__SubscriptionRatePlan__c();
        testRatePlan.Zuora__Subscription__c = testSubsctiption.Id;
        testRatePlan.LicenseKey__c = '1234567890';
        insert testRatePlan;
        System.assertNotEquals(null, testSubsctiption.Id);

        testAccountLicense = [SELECT Id, Name, Subscription__c FROM Account_License__c WHERE Id = :testAccountLicense.Id];
        System.assertNotEquals(testAccountLicense.Subscription__c, testSubsctiption.Id);
        Test.stopTest();
    }
}