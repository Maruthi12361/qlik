/*
* File UpdateCustomSettingsTest
    * @description :
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       09.01.2018   Pramod Kumar V      Created Class
    2       28.04.2018   Pramod Kumar v      ITRM-60 Added assert methods
    3       06.03.2019   ext_bjd             ITRM-320 Added method getUrlForInstance to determine instance url and update
    *       custom settings Support_Portal_Login_Url__c and Support_Portal_Url__c

*/
@isTest(seeAlldata=false)
public class UpdateCustomSettingsTest {

    static testMethod void testmethodTestingCustomSettingsforQA() {

        QTCustomSettings__c Qtsetting = QTTestUtils.createQTCustomSettingsTestData();
        Steelbrick_Settings__c SBsetting = QTTestUtils.createSteelBrickSettingsTestData();
        QVM_Settings__c qvmSetting = QTTestUtils.createQVMSettingsTestData();
        QS_Partner_Portal_Urls__c qsSetting = QTTestUtils.createQS_PartnerPortalUrlsTestData();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();
        system.debug('qvmSetting2-->' + qvmSetting);
        Test.StartTest();

        UpdateCustomSettings cs = new UpdateCustomSettings();
        RefreshSandboxData__mdt RefeshData = cs.getRefreshSandboxDataBySandboxName('QA');
        system.debug('RefreshSandboxData__mdt-->' + RefeshData);
        cs.UpdateCustomSettingsfromMdt(RefeshData);

        QTCustomSettings__c updatedQT = [select BoomiBaseURL__c,ULC_Base_URL__c from QTCustomSettings__c where Id = :Qtsetting.id];
        Steelbrick_Settings__c updatedSB = [select BoomiBaseURL__c,SpringAccountPrefix__c,CMInstanceUrl__c from Steelbrick_Settings__c where Id = :SBsetting.id];
        QVM_Settings__c updatedQVM = [select QlikMarket_Website__c from QVM_Settings__c where Id = :qvmSetting.id];
        QS_Partner_Portal_Urls__c updatedQS = [select Support_Portal_CSS_Base__c,Support_Portal_index_allow_options__c,Support_Portal_Live_Agent_API_Endpoint__c,Support_Portal_Login_Page_Url__c,Support_Portal_Login_Url__c,Support_Portal_Url__c,Support_Portal_Url_Base__c,Support_Portal_Logout_Page_Url__c from QS_Partner_Portal_Urls__c where Id = :qsSetting.id];

        Test.stopTest();

        //Debug statements
        system.debug('RefreshSandboxData__mdt-->' + RefeshData);
        system.debug('qvmSetting-->' + qvmSetting);

        system.debug('BoomiQS-->' + updatedQT.BoomiBaseURL__c);
        system.debug('BoomiIT-->' + RefeshData.BoomiBaseURL__c);

        system.assertEquals(updatedQT.ULC_Base_URL__c, RefeshData.ULC_Base_URL__c);
        system.assertEquals(updatedQT.BoomiBaseURL__c, RefeshData.BoomiBaseURL__c);

        system.assertEquals(updatedSB.BoomiBaseURL__c, RefeshData.BoomiBaseURL__c);
        system.assertEquals(updatedSB.CMInstanceUrl__c, RefeshData.CMInstanceUrl__c);
        system.assertEquals(updatedSB.SpringAccountPrefix__c, RefeshData.SpringAccountPrefix__c);

        system.assertEquals(updatedQVM.QlikMarket_Website__c, RefeshData.QlikMarket_Website__c);

        system.assertEquals(updatedQS.Support_Portal_Login_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : RefeshData.Support_Portal_Login_Url__c);
        system.assertEquals(updatedQS.Support_Portal_CSS_Base__c, RefeshData.Support_Portal_CSS_Base__c);
        system.assertEquals(updatedQS.Support_Portal_index_allow_options__c, RefeshData.Support_Portal_index_allow_options__c);
        system.assertEquals(updatedQS.Support_Portal_Live_Agent_API_Endpoint__c, RefeshData.Support_Portal_Live_Agent_API_Endpoint__c);
        system.assertEquals(updatedQS.Support_Portal_Login_Page_Url__c, RefeshData.Support_Portal_Login_Page_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : RefeshData.Support_Portal_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Logout_Page_Url__c, RefeshData.Support_Portal_Logout_Page_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Url_Base__c, RefeshData.Support_Portal_Url_Base__c);

    }
    static testMethod void testmethodTestingCustomSettingsforDefault() {

        QTCustomSettings__c QtsettingDef = QTTestUtils.createQTCustomSettingsTestData();
        Steelbrick_Settings__c SBsettingDef = QTTestUtils.createSteelBrickSettingsTestData();
        QVM_Settings__c qvmSettingDef = QTTestUtils.createQVMSettingsTestData();
        QS_Partner_Portal_Urls__c qsSettingDef = QTTestUtils.createQS_PartnerPortalUrlsTestData();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        system.debug('qvmSetting2-->' + qvmSettingDef);

        Test.StartTest();

        UpdateCustomSettings cs = new UpdateCustomSettings();
        RefreshSandboxData__mdt RefeshData = cs.getRefreshSandboxDataBySandboxName('test');
        system.debug('RefreshSandboxData__mdt-->' + RefeshData);
        cs.UpdateCustomSettingsfromMdt(RefeshData);

        QTCustomSettings__c updatedQT = [select BoomiBaseURL__c,ULC_Base_URL__c from QTCustomSettings__c where Id = :QtsettingDef.id];
        Steelbrick_Settings__c updatedSB = [select BoomiBaseURL__c,SpringAccountPrefix__c,CMInstanceUrl__c from Steelbrick_Settings__c where Id = :SBsettingDef.id];
        QVM_Settings__c updatedQVM = [select QlikMarket_Website__c from QVM_Settings__c where Id = :qvmSettingDef.id];
        QS_Partner_Portal_Urls__c updatedQS = [select Support_Portal_CSS_Base__c,Support_Portal_index_allow_options__c,Support_Portal_Live_Agent_API_Endpoint__c,Support_Portal_Login_Page_Url__c,Support_Portal_Login_Url__c,Support_Portal_Url__c,Support_Portal_Url_Base__c,Support_Portal_Logout_Page_Url__c from QS_Partner_Portal_Urls__c where Id = :qsSettingDef.id];

        Test.stopTest();

        //Debug statements
        system.debug('RefreshSandboxData__mdt-->' + RefeshData);
        system.debug('qvmSetting-->' + qvmSettingDef);

        system.debug('BoomiQS-->' + updatedQT.BoomiBaseURL__c);
        system.debug('BoomiIT-->' + RefeshData.BoomiBaseURL__c);

        system.assertEquals(updatedQT.ULC_Base_URL__c, RefeshData.ULC_Base_URL__c);
        system.assertEquals(updatedQT.BoomiBaseURL__c, RefeshData.BoomiBaseURL__c);

        system.assertEquals(updatedSB.BoomiBaseURL__c, RefeshData.BoomiBaseURL__c);
        system.assertEquals(updatedSB.CMInstanceUrl__c, RefeshData.CMInstanceUrl__c);
        system.assertEquals(updatedSB.SpringAccountPrefix__c, RefeshData.SpringAccountPrefix__c);

        system.assertEquals(updatedQVM.QlikMarket_Website__c, RefeshData.QlikMarket_Website__c);

        system.assertEquals(updatedQS.Support_Portal_Login_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : RefeshData.Support_Portal_Login_Url__c);
        system.assertEquals(updatedQS.Support_Portal_CSS_Base__c, RefeshData.Support_Portal_CSS_Base__c);
        system.assertEquals(updatedQS.Support_Portal_index_allow_options__c, RefeshData.Support_Portal_index_allow_options__c);
        system.assertEquals(updatedQS.Support_Portal_Live_Agent_API_Endpoint__c, RefeshData.Support_Portal_Live_Agent_API_Endpoint__c);
        system.assertEquals(updatedQS.Support_Portal_Login_Page_Url__c, RefeshData.Support_Portal_Login_Page_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : RefeshData.Support_Portal_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Logout_Page_Url__c, RefeshData.Support_Portal_Logout_Page_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Url_Base__c, RefeshData.Support_Portal_Url_Base__c);

    }
    static testMethod void testmethodTestingCustomSettingsforNull() {

        QTCustomSettings__c QtsettingDef = QTTestUtils.createQTCustomSettingsTestData();
        Steelbrick_Settings__c SBsettingDef = QTTestUtils.createSteelBrickSettingsTestData();
        QVM_Settings__c qvmSettingDef = QTTestUtils.createQVMSettingsTestData();
        QS_Partner_Portal_Urls__c qsSettingDef = QTTestUtils.createQS_PartnerPortalUrlsTestData();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        system.debug('qvmSetting2-->' + qvmSettingDef);

        Test.StartTest();

        UpdateCustomSettings cs = new UpdateCustomSettings();
        RefreshSandboxData__mdt RefeshData = cs.getRefreshSandboxDataBySandboxName('');
        system.debug('RefreshSandboxData__mdt-->' + RefeshData);
        cs.UpdateCustomSettingsfromMdt(RefeshData);
        QTCustomSettings__c updatedQT = [select BoomiBaseURL__c,ULC_Base_URL__c from QTCustomSettings__c where Id = :QtsettingDef.id];
        Steelbrick_Settings__c updatedSB = [select BoomiBaseURL__c,SpringAccountPrefix__c,CMInstanceUrl__c from Steelbrick_Settings__c where Id = :SBsettingDef.id];
        QVM_Settings__c updatedQVM = [select QlikMarket_Website__c from QVM_Settings__c where Id = :qvmSettingDef.id];
        QS_Partner_Portal_Urls__c updatedQS = [select Support_Portal_CSS_Base__c,Support_Portal_index_allow_options__c,Support_Portal_Live_Agent_API_Endpoint__c,Support_Portal_Login_Page_Url__c,Support_Portal_Login_Url__c,Support_Portal_Url__c,Support_Portal_Url_Base__c,Support_Portal_Logout_Page_Url__c from QS_Partner_Portal_Urls__c where Id = :qsSettingDef.id];

        Test.stopTest();

        //Debug statements
        system.debug('RefreshSandboxData__mdt-->' + RefeshData);
        system.debug('qvmSetting-->' + qvmSettingDef);

        system.debug('BoomiQS-->' + updatedQT.BoomiBaseURL__c);
        system.debug('BoomiIT-->' + RefeshData.BoomiBaseURL__c);

        system.assertEquals(updatedQT.ULC_Base_URL__c, RefeshData.ULC_Base_URL__c);
        system.assertEquals(updatedQT.BoomiBaseURL__c, RefeshData.BoomiBaseURL__c);

        system.assertEquals(updatedSB.BoomiBaseURL__c, RefeshData.BoomiBaseURL__c);
        system.assertEquals(updatedSB.CMInstanceUrl__c, RefeshData.CMInstanceUrl__c);
        system.assertEquals(updatedSB.SpringAccountPrefix__c, RefeshData.SpringAccountPrefix__c);

        system.assertEquals(updatedQVM.QlikMarket_Website__c, RefeshData.QlikMarket_Website__c);

        system.assertEquals(updatedQS.Support_Portal_Login_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : RefeshData.Support_Portal_Login_Url__c);
        system.assertEquals(updatedQS.Support_Portal_CSS_Base__c, RefeshData.Support_Portal_CSS_Base__c);
        system.assertEquals(updatedQS.Support_Portal_index_allow_options__c, RefeshData.Support_Portal_index_allow_options__c);
        system.assertEquals(updatedQS.Support_Portal_Live_Agent_API_Endpoint__c, RefeshData.Support_Portal_Live_Agent_API_Endpoint__c);
        system.assertEquals(updatedQS.Support_Portal_Login_Page_Url__c, RefeshData.Support_Portal_Login_Page_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Url__c, (urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : RefeshData.Support_Portal_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Logout_Page_Url__c, RefeshData.Support_Portal_Logout_Page_Url__c);
        system.assertEquals(updatedQS.Support_Portal_Url_Base__c, RefeshData.Support_Portal_Url_Base__c);

    }

    static testMethod void testmethodTestingCustomSettingsforProd() {

        QTCustomSettings__c qtSetting = QTTestUtils.createQTCustomSettingsTestDataForLive();
        Steelbrick_Settings__c sbSetting = QTTestUtils.createSteelBrickSettingsTestDataForLive();
        QVM_Settings__c qvmSetting = QTTestUtils.createQVMSettingsTestDataForLive();
        QS_Partner_Portal_Urls__c qsSetting = QTTestUtils.createQS_PartnerPortalUrlsTestDataForLive();
        List<String> urlsInstance = UpdateCustomSettings.getUrlForInstance();

        Test.startTest();

        RefreshSandboxData__mdt rs = UpdateCustomSettings.getProdCustomSettingSandboxData();

        Test.stopTest();

        //Debug statements
        Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        if (!IsSandbox) {
            System.debug('qvmSetting-->' + qvmSetting);
            System.debug('BoomiQS-->' + rs.BoomiBaseURL__c);

            System.assertEquals(qtSetting.ULC_Base_URL__c, rs.ULC_Base_URL__c);
            System.assertEquals(qtSetting.BoomiBaseURL__c, rs.BoomiBaseURL__c);

            System.assertEquals(sbSetting.BoomiBaseURL__c, rs.BoomiBaseURL__c);
            System.assertEquals(sbSetting.CMInstanceUrl__c, rs.CMInstanceUrl__c);
            System.assertEquals(sbSetting.SpringAccountPrefix__c, rs.SpringAccountPrefix__c);

            System.assertEquals(qvmSetting.QlikMarket_Website__c, rs.QlikMarket_Website__c);

            //System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : qsSetting.Support_Portal_Login_Url__c, rs.Support_Portal_Login_Url__c);
            System.assertEquals(qsSetting.Support_Portal_CSS_Base__c, rs.Support_Portal_CSS_Base__c);
            System.assertEquals(qsSetting.Support_Portal_Live_Agent_API_Endpoint__c, rs.Support_Portal_Live_Agent_API_Endpoint__c);
            System.assertEquals(qsSetting.Support_Portal_Login_Page_Url__c, rs.Support_Portal_Login_Page_Url__c);
            //System.assertEquals((urlsInstance.size() > 1) ? UpdateCustomSettings.URL_PART_HTTPS + UpdateCustomSettings.URL_PART_QLIKID + '-' + urlsInstance[0] + '.' + UpdateCustomSettings.URL_PART_QLIK_COM + UpdateCustomSettings.URL_PART_HTTPS + urlsInstance[0] + '-' + UpdateCustomSettings.URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + UpdateCustomSettings.URL_PART_FORCE_COM : qsSetting.Support_Portal_Url__c, rs.Support_Portal_Url__c);
            System.assertEquals(qsSetting.Support_Portal_Logout_Page_Url__c, rs.Support_Portal_Logout_Page_Url__c);
            System.assertEquals(qsSetting.Support_Portal_Url_Base__c, rs.Support_Portal_Url_Base__c);
        }
    }
}
