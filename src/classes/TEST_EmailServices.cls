/******************************************************

	Class: TEST_EmailServices
	
	This class is used to gather all the email realted
	tests. 
	
	Changelog:
		2009-10-06	MBH		Created file
				
******************************************************/
@isTest
private class TEST_EmailServices {

	
	/******************************************************

		testUnsubscribe
	
		This method tests the Unsubscribe functionallity.
		Positive tests.
	
		Source:
		http://wiki.developerforce.com/index.php/Force.com_Email_Services_Unsubscribe
	
		Changelog:
			2009-10-06	MHG		Created method
			2011-10-11	MHG		New Validation Rules on Lead
				
	******************************************************/	
	static testMethod void testUnsubscribe() {

		// Create a new email and envelope object
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;
		Messaging.InboundEnvelope env 	= new Messaging.InboundEnvelope();
	
		// Create a new test Lead and insert it in the Test Method        
		Lead l = new Lead(firstName='Martin', 
	   						lastName='Haagen',
	   						Company='QlikTech International AB', 
	   						Country = 'Sweden',
	   						Email='mhg@qlikview.com', 
	   						HasOptedOutOfEmail=false);
		insert l;
	
		// Create a new test Contact and insert it in the Test Method  
		Contact c = new Contact(firstName='Martin', 
	   								lastName='Haagen', 
	   								Email='mhg@qlikview.com', 
	   								HasOptedOutOfEmail=false);
		insert c;
	   
	   	test.startTest();
	   
	  	// test with subject that matches the unsubscribe statement
	   	email.subject = 'test unsubscribe test';
	   	env.fromAddress = 'mhg@qlikview.com';
	   
	   	// call the class and test it with the data in the testMethod
	   	Unsubscribe unsubscribeObj = new Unsubscribe();
	   	unsubscribeObj.handleInboundEmail(email, env );
	   	
	   	Lead l2 = [select Id, HasOptedOutOfEmail from Lead where firstName='Martin' and lastName='Haagen' and Company='QlikTech International AB' and Email='mhg@qlikview.com' LIMIT 1];
	   	Contact c2 = [select Id, HasOptedOutOfEmail from Contact where firstName='Martin' and lastName='Haagen' and Email='mhg@qlikview.com' LIMIT 1];
	   	
	   	System.assertEquals(true, l2.HasOptedOutOfEmail);
	   	System.assertEquals(true, c2.HasOptedOutOfEmail);
	   	
	   	test.stopTest();
	   	
	}

	/******************************************************

		testUnsubscribe2
	
		This method tests the Unsubscribe functionallity.
		Negative tests.
	
		Source:
		http://wiki.developerforce.com/index.php/Force.com_Email_Services_Unsubscribe
	
		Changelog:
			2009-10-06	MHG		Created method
			2011-10-14	MHG		New Validation Rules on Lead
				
	******************************************************/		
	static testMethod void testUnsubscribe2() {
	
		// Create a new email and envelope object
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;
		Messaging.InboundEnvelope env 	= new Messaging.InboundEnvelope();
	
		// Create a new test Lead and insert it in the Test Method        
		Lead l = new Lead(firstName='Martin', 
	   						lastName='Haagen',
	   						Company='QlikTech International AB',
	   						Country = 'Sweden', 
	   						Email='mhg@qlikview.com', 
	   						HasOptedOutOfEmail=false);
		insert l;
	
		// Create a new test Contact and insert it in the Test Method  
		Contact c = new Contact(firstName='Martin', 
	   								lastName='Haagen', 
	   								Email='mhg@qlikview.com', 
	   								HasOptedOutOfEmail=false);
		insert c;
	   
	   	test.startTest();
	   
	  	// test with subject that matches the unsubscribe statement
	   	email.subject = 'test';
	   	env.fromAddress = 'mhg@qlikview.com';
	   
	   	// call the class and test it with the data in the testMethod
	   	Unsubscribe unsubscribeObj = new Unsubscribe();
	   	unsubscribeObj.handleInboundEmail(email, env );
	   	
	   	Lead l2 = [select Id, HasOptedOutOfEmail from Lead where firstName='Martin' and lastName='Haagen' and Company='QlikTech International AB' and Email='mhg@qlikview.com' LIMIT 1];
	   	Contact c2 = [select Id, HasOptedOutOfEmail from Contact where firstName='Martin' and lastName='Haagen' and Email='mhg@qlikview.com' LIMIT 1];
	   	
	   	System.assertEquals(false, l2.HasOptedOutOfEmail);
	   	System.assertEquals(false, c2.HasOptedOutOfEmail);
	   	
	   	test.stopTest();						
   }    
   
}