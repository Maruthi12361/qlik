/**
* Change log:
* 
* 2019-03-04    ext_vos CHG0035184: update test method with attached objects from Coveo.
* 2019-05-05    AIN IT-1597 Updated for Support Portal Redesign
*/
@isTest
private class QS_CaseArticleListControllerTest {
        
    public static testmethod void test1() {
        Case caseObj = [select id froM Case where Subject = 'Test CaseArticleList subject' limit 1];
        QS_CaseArticleListController controller = new QS_CaseArticleListController();
        controller.contentDocumentParentId = caseObj.Id;
        controller.showException = false;
        controller.errorMessageText = 'Test';

        System.assertEquals(5, controller.getAttachResults().size());   
    }

    @testSetup
    static void InsertTestData() {
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'leadSource';
        insert testContact;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        
        Case caseObj = TestDataFactory.createCase('Test CaseArticleList subject', 'Test description', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        insert caseObj;

        Id knowledgeArticleId = createArticle();

        CaseArticle caseArticle = new CaseArticle();
        caseArticle.KnowledgeArticleId = knowledgeArticleId;
        caseArticle.CaseId = caseObj.Id;
        insert caseArticle;

        Id knowledgeArticleId2 = createArticle();
        List<CoveoV2__CoveoCaseAttachedResult__c> attachResults = new List<CoveoV2__CoveoCaseAttachedResult__c> ();
        attachResults.add(new CoveoV2__CoveoCaseAttachedResult__c(CoveoV2__ResultUrl__c = 'https://community.qlik.com', 
                            CoveoV2__UriHash__c = System.now().millisecond() + '_' + System.now().millisecond(),
                            CoveoV2__Source__c = 'Lithium Community', CoveoV2__Title__c = 'Test link for community', CoveoV2__case__c = caseObj.Id));
        attachResults.add(new CoveoV2__CoveoCaseAttachedResult__c(CoveoV2__ResultUrl__c = 'https://www.youtube.com', 
                            CoveoV2__UriHash__c = System.now().millisecond() + '_' + System.now().millisecond(),
                            CoveoV2__Source__c = 'Qlik Support Youtube', CoveoV2__Title__c = 'Youtube', CoveoV2__case__c = caseObj.Id));
        attachResults.add(new CoveoV2__CoveoCaseAttachedResult__c(CoveoV2__ResultUrl__c = 'sandboxURL/' + knowledgeArticleId2, 
                            CoveoV2__UriHash__c = System.now().millisecond() + '_' + System.now().millisecond(),
                            CoveoV2__Source__c = 'Salesforce Knowledge', CoveoV2__Title__c = 'sandbox test knowledge', CoveoV2__case__c = caseObj.Id));
        attachResults.add(new CoveoV2__CoveoCaseAttachedResult__c(CoveoV2__ResultUrl__c = 'https://help.qlik.com', 
                            CoveoV2__UriHash__c = System.now().millisecond() + '_' + System.now().millisecond(),
                            CoveoV2__Source__c = 'Qlik Help', CoveoV2__Title__c = 'sandbox test Qlik Help', CoveoV2__case__c = caseObj.Id));
        // duplicate artile - will not be in result list
        attachResults.add(new CoveoV2__CoveoCaseAttachedResult__c(CoveoV2__ResultUrl__c = 'sandboxURL/' + knowledgeArticleId, 
                            CoveoV2__UriHash__c = System.now().millisecond() + '_' + System.now().millisecond(),
                            CoveoV2__Source__c = 'Salesforce Knowledge', CoveoV2__Title__c = 'Title', CoveoV2__case__c = caseObj.Id));
        insert attachResults; 
    }
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }

    private static ID createArticle() {
        String objType;
        List<String> kavNames = new List<String>(getKavNames());
        if (kavNames.isEmpty()){
            return null;
        }
        objType = kavNames.get(0);

        SObject kavObj = Schema.getGlobalDescribe().get(objType).newSObject();
        kavObj.put('Title','Foo Foo Foo!!!' + System.now().millisecond());
        kavObj.put('UrlName', 'foo-foo-foo-' + System.now().millisecond());
        kavObj.put('Summary', 'This is a summary!!! Foo. Foo. Foo.');
        kavObj.put('Language', 'en_US');
        insert kavObj;

        // requery the kavObj to get the KnowledgeArticleId on it that is created automatically
        String q = 'select id, KnowledgeArticleId from ' +objType+ ' where Id = \'' +kavObj.get('Id')+  '\' and PublishStatus = \'Draft\' and Language = \'en_US\'';
        List<SObject> kavs = Database.query(q);
        if (kavs.isempty()) {
            return null;
        }
        kavObj = kavs[0];
        KbManagement.PublishingService.publishArticle((ID)(kavObj.get('KnowledgeArticleId')),true);
        return (ID)(kavObj.get('KnowledgeArticleId'));
    }
    
     private static Set<String> getKavNames() {
        Set<String> kavNames;
        kavNames = new Set<String>();
        Map<String,Schema.SOBjectType> gd = Schema.getGlobalDescribe();

        for (String s : gd.keySet()) {
            if (s.contains('__kav')) {
                kavNames.add(s);
            }
        }
        return kavNames;
    }
}