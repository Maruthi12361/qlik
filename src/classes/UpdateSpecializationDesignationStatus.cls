/*    * File : UpdateSpecializationDesignationStatus 
        * Description : Batch to update the Designation Status of Specialization records if the Expiration Date is passed.
        * @author : ext_axw
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 August-8-2019 Anjuna Baby   BSL-888
        
*/

global class UpdateSpecializationDesignationStatus implements Database.Batchable<sObject>, schedulable {

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,Name, Designation_Status__c, Expiration_Date__c FROM Specialization__c WHERE Expiration_Date__c < TODAY AND Designation_Status__c !=\'Expired\'';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Specialization__c > scope){
        // process each batch of records
        List<Specialization__c> listToUpdate = new List<Specialization__c>();
        for(Specialization__c spec: scope){
            spec.Designation_Status__c = 'Expired';
            listToUpdate.add(spec);
        }  
        
        if(listToUpdate.size() > 0) {
            Try{
                Database.Update(listToUpdate,false);
                } Catch (Exception e) {
                    System.debug('Exception'+e);
                }
        }
    } 
    
       
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    } 
    
    global void execute(SchedulableContext SC) {
        database.executebatch(new UpdateSpecializationDesignationStatus());
    
    }   
}