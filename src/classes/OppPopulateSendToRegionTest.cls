/*
 File Name: OppPopulateSendToRegionHandler

 2018-07-11 ext_bad   CHG0034322
*/
@isTest
public class OppPopulateSendToRegionTest {

    public static testMethod void testSendToPopulate() {
        QTTestUtils.GlobalSetUp();

        User tUser = [SELECT id FROM User WHERE id =: UserInfo.getUserId()];
        String subId = QTTestUtils.getSubsidiary('test');
        QlikTech_Company__c compSPI = new QlikTech_Company__c(Name = 'test', QlikTech_Region__c = 'SPI',
                QlikTech_Sub_Region__c = 'Italy', Subsidiary__c = subId);
        insert compSPI;
        QlikTech_Company__c compTest = new QlikTech_Company__c(Name = 'ttest', QlikTech_Region__c = 'test',
                QlikTech_Sub_Region__c = 'test', Subsidiary__c = subId);
        insert compTest;

        List<Account> accs = new List<Account>();
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', tUser, true);
        testAccount.Billing_Country_Code__c = compTest.Id;
        Account testAccountSPI = QTTestUtils.createMockAccount('TestCompany2', tUser, true);
        testAccountSPI.Billing_Country_Code__c = compSPI.Id;
        accs.add(testAccount);
        accs.add(testAccountSPI);
        update accs;

        RecordType rType = [SELECT id FROM RecordType WHERE developerName = 'Sales_QCCS'];
        List<Opportunity> opps = new List<Opportunity>();
        Opportunity opp = QuoteTestHelper.createOpportunity(testAccount, '', rType);
        opp.Send_to_DL_QT_Region__c = null;
        opps.add(opp);
        Opportunity oppSPI = QuoteTestHelper.createOpportunity(testAccountSPI, '', rType);
        oppSPI.Send_to_DL_QT_Region__c = null;
        opps.add(oppSPI);

        Test.startTest();
        insert opps;
        Test.stopTest();

        opp = [SELECT Send_to_DL_QT_Region__c FROM Opportunity WHERE Id = :opp.Id];
        oppSPI = [SELECT Send_to_DL_QT_Region__c FROM Opportunity WHERE Id = :oppSPI.Id];
        Organization org = [SELECT Name, IsSandbox FROM Organization LIMIT 1];
        if (DL_Training_Notification_Emails__c.getInstance() == null
                || DL_Training_Notification_Emails__c.getInstance().No_Region__c == null) {
            System.assertEquals(null, opp.Send_to_DL_QT_Region__c);
        } else if (org.IsSandbox) {
            String sandbox = UserInfo.getUserName().substringAfterLast('.com.');
            System.assertEquals((sandbox + '_No_QT_Region_In_Qliktech_Company@qlikview.com').toLowerCase(), opp.Send_to_DL_QT_Region__c);
            System.assertEquals(('DL-' + sandbox + '-TrainingNotificationITALY@qlikview.com').toLowerCase(), oppSPI.Send_to_DL_QT_Region__c);
        } else {
            System.assertEquals('No_QT_Region_In_Qliktech_Company@qlikview.com'.toLowerCase(), opp.Send_to_DL_QT_Region__c);
            System.assertEquals('DL-TrainingNotificationITALY@qlikview.com'.toLowerCase(), oppSPI.Send_to_DL_QT_Region__c);
        }
    }
}