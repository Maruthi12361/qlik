/*********************************************
Class:UserPortalUserHandlerTest

Description: 
			Test class for UserPortalUserHandler and trigger User_PortalUser

Log History:
2017-01-11    BAD    Created - CR# 97619
2018-10-09	ext_bjd	 ITRM-226 Fix test related with portal account owner error.
**********************************************/
@isTest
private class UserPortalUserHandlerTest {
	@isTest
	static void test_UserPortalUserTrigger() {
		User mockPortalAccountUsr = QTTestUtils.createMockPortalUser();
		UserPortalUserHandler tempObj = new UserPortalUserHandler();

		System.runAs(mockPortalAccountUsr) {
			User mockPRMBase = QTTestUtils.createMockUserForProfile('PRM - Base');

			Test.startTest();

			Contact con = [SELECT Id, Portal_User_Active__c FROM Contact WHERE Id = :mockPRMBase.ContactId];
			System.assertEquals(false, con.Portal_User_Active__c);

			mockPRMBase.IsActive = true;
			update mockPRMBase;

			Test.stopTest();

			Contact con2 = [SELECT Id, Portal_User_Active__c FROM Contact WHERE Id = :mockPRMBase.ContactId];
			System.assertEquals(true, con2.Portal_User_Active__c);
		}
	}
}