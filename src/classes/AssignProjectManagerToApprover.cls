//    Trigger helper class for Expense and Timecards.
// 	  We are not enforcing "with sharing" as this is meant to be executed by Triggers
//
//    Version 0.1    Metacube(Abhinav & Ashok)    31-Dec-2009    Original

/**
 	Updates the Approved field on Timecard_Header__c or Expense_Report__c by the Project's --> Project Manager
 	So simple formula for timecard would be 
 	Timecard-->Approver = Timecard-->Project-->ProjectManager-->Salesforce User
 	
 	NOTE: Code for Timecard/Expense Report Approver updation is pretty similar. But for sake of similicity, we have kept it like that.
 	An advanced practice to reduce the code foot size and copy paste will be to use Apex Sobject class and call methods like Sobject.get() and Sobject.put()
 	One can go for that approach in case of complex triggers. This trigger is relatively simple in nature so this approach is fine. 
*/ 
public class AssignProjectManagerToApprover {
	
	/**
		Updates the Approver field on Expense Report with SFDC_USER(Project Manager) of the Expense Report's Project.
		Formula for would be 
 		Expense Report-->Approver = Expense Report-->Project-->ProjectManager-->Salesforce User
	*/
	public static void assignApproversForExpenseReports(List<pse__Expense_Report__c> newExpenseReports){
		
		if (newExpenseReports != null && !newExpenseReports.isEmpty()) {
			Set<Id> projIds = new Set<Id>();
			for (pse__Expense_Report__c expRpt : newExpenseReports) {
				// Project should be there in any case 
				// for ex: Filling from Standard UI : Project is marked mandantory
				if (expRpt.pse__Project__c != null) {
					projIds.add(expRpt.pse__Project__c);
				}
			}
			
			// Do nothing, if projects are somehow missing from timecards.
			if (!projIds.isEmpty()) {
				// Load the required project information like SFDC User etc 
				Map<Id, pse__Proj__c> projectMap = loadProjects(projIds);
				
				// Update timecards with the loaded project information
				for (pse__Expense_Report__c expRpt : newExpenseReports) {
					if (expRpt.pse__Project__c != null) {
						pse__Proj__c expRptProject = projectMap.get(expRpt.pse__Project__c);
						if (expRptProject.pse__Project_Manager__c != null && expRptProject.pse__Project_Manager__r.pse__Salesforce_User__c != null) {
							expRpt.pse__Approver__c = expRptProject.pse__Project_Manager__r.pse__Salesforce_User__c;	
						}
					}
				}
			}
		}	
	}	
	
	
	
	/**
		Updates the Approver field on Timecard Header with SFDC_USER(Project Manager) of the Timecard's Project.
		Formula for would be 
 		Timecard-->Approver = Timecard-->Project-->ProjectManager-->Salesforce User
 		
 		Modification (9/21/2010, JasonF Appirio):
 		If PM is submitting the timecard, set the Approver to the PM's Manager.
	*/
	public static void assignApproversForTimecards(List<pse__Timecard_Header__c> newTimecards){
		
		if (newTimecards != null && !newTimecards.isEmpty()) {
			Set<Id> projIds = new Set<Id>();
			Set<Id> resourceIds = new Set<Id>();
			for (pse__Timecard_Header__c tc : newTimecards) {
				// Project will be there in any case, 
				// Case1: Filling from Standard Timecard UI : Project is marked mandantory
				// Case2: Filling from Custom Timecard UI (Packaged with pse) : Assignment's project is assigned to project field. 
				// Still to be safe, we are adding the checks
				if (tc.pse__Project__c != null) {
					projIds.add(tc.pse__Project__c);
				}
				if (tc.pse__Resource__c != null) {
					resourceIds.add(tc.pse__Resource__c);
				}
			}
			
			// Do nothing, if projects are somehow missing from timecards.
			if (!projIds.isEmpty()) {
				// Load the required project information like SFDC User etc
				Map<Id, pse__Proj__c> projectMap = loadProjects(projIds);
				// Load the required resource information like SFDC User Manager
				Map<Id, Contact> resourceMap = loadResources(resourceIds);
				
				// Update timecards with the loaded project information
				for (pse__Timecard_Header__c tc : newTimecards) {
					if (tc.pse__Project__c != null) {
						pse__Proj__c tcProject = projectMap.get(tc.pse__Project__c);
						if (tcProject.pse__Project_Manager__c != null && tcProject.pse__Project_Manager__r.pse__Salesforce_User__c != null) {
							//added by JasonF on 9/21/2010
							if (tcProject.pse__Project_Manager__c != tc.pse__Resource__c) {
								tc.pse__Approver__c = tcProject.pse__Project_Manager__r.pse__Salesforce_User__c;
							} else if (tc.pse__Resource__c != null) {
								Contact resource = resourceMap.get(tc.pse__Resource__c);
								if(resource.pse__Salesforce_User__c != null && resource.pse__Salesforce_User__r.ManagerId != null) {
									tc.pse__Approver__c = resource.pse__Salesforce_User__r.ManagerId;
								}
							}	
						}
					}
				}
			}
		}	
	}
	
	/**
		Load Projects with details like Project Manager's User etc for the given project ids.
		@param projIds : Project Ids for whom Projects should be loaded
		@return Map<Id, Proj__c> : Key(Id) = Will be Project Id , Value(Proj__c) = will be the Proj__c instance  
	*/	
	private static 	Map<Id, pse__Proj__c> loadProjects(Set<Id> projIds) {
		return new Map<Id, pse__Proj__c>([SELECT Id, p.pse__Project_Manager__c, p.pse__Project_Manager__r.pse__Salesforce_User__c 
					FROM pse__Proj__c p WHERE Id IN :projIds]);
	}
	
	/**
		Load Resources details
	*/
	private static Map<Id, Contact> loadResources(Set<Id> resourceIds) {
		return new Map<Id, Contact>([SELECT id, pse__Salesforce_User__c, pse__Salesforce_User__r.ManagerId 
				FROM Contact WHERE id IN :resourceIds]);    
	}
}