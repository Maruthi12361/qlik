/****************************************************************************************************
   
   CaseAttachmentPurge
   IT-79 : Class to purge case attachments which are closed and older than 90 days
   IT-467 :Class Updated to work in partial success scenarios and also send emails with error Id's only when there are any- CRW 12/04/2018
   IT-606 : Update list of Case Recored Type - ext_vos - 10/07/2018
   IT-2347: Added Closure Code 'License Delivery' to exceptions - extbad - 2020-01-29
****************************************************************************************************/
global class CaseAttachmentPurge implements Database.Batchable<SObject>, Database.Stateful {
    global String query;
    global Integer successIdsCount;
    global List<Id> errorIds = new List<Id>();

    global static Id TEST_ERROR_ID;
    global static Boolean TEST_EMAIL_SENDING = false;

    global CaseAttachmentPurge() {
    //Query to get the attachment records for those cases closed and 90 days older
        if ([SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox || Test.isRunningTest()) {
            query = 'SELECT Id FROM Attachment WHERE ParentId IN ' 
                    + '(SELECT Id FROM Case ' 
                        + 'WHERE ' 
                            + ' (RecordType.Name in (\'QlikTech Master Support Record Type\',\'QT Problem Case Record Type\',\'QT Support Customer Portal Record Type\','
                                + '\'QT Support Partner Portal Record Type\') '
                            + ' AND Status = \'Closed\' AND Closure_Code__c != \'License Delivery\')'
                        + ' OR '
                            + '(RecordType.Name = \'Live Chat Support Record Type\' AND Status = \'Chat Resolved\')' 
                    + ')';
        } else {
            query = 'SELECT Id FROM Attachment ' 
                    + ' WHERE ParentId IN ' 
                        + '(SELECT Id FROM Case ' 
                            + 'WHERE LastModifiedDate < LAST_N_DAYS:90 ' 
                            + ' AND (' 
                                    + ' (RecordType.Name in (\'QlikTech Master Support Record Type\',\'QT Problem Case Record Type\','
                                        + '\'QT Support Customer Portal Record Type\',\'QT Support Partner Portal Record Type\') '
                                    + ' AND Status = \'Closed\' AND Closure_Code__c != \'License Delivery\')'
                                + ' OR '
                                    + '(RecordType.Name = \'Live Chat Support Record Type\' AND Status = \'Chat Resolved\')'
                            + ')' 
                        + ')';
        }
        System.debug('Myquery ' + query); 
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        Database.QueryLocator ql = Database.getQueryLocator(query);
        System.debug('Myquery2 ' + ql.getQuery()); 
        return ql;
    }

    global void execute(Database.BatchableContext BC, List<Attachment> scope){
        System.debug('Scope: ' + scope.size());
        successIdsCount = 0;
        Database.DeleteResult[] drList = Database.delete(scope,false);
        for (Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                successIdsCount++;
            } else {
                errorIds.add(dr.getId());
            }
        }
    }

    global void finish(Database.BatchableContext BC){
        System.debug('ErrorId list: ' + errorIds);
        if (Test.isRunningTest() && TEST_ERROR_ID != null) {
            errorIds.add(TEST_ERROR_ID);
        }
        //modified by CRW for IT-467
        if (errorIds != null && errorIds.size() > 0) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            QTCustomSettings__c defSettings = QTCustomSettings__c.getValues('Default');
            String qlikNoreply = '';
            List<String> listTo = new List<String>();
            if (defSettings != null) {
                qlikNoreply = defSettings.QlikNoReplyEmailAddress__c;                
                if (!String.isEmpty(defSettings.CaseAttachmentPurgeEmail__c)) {
                    listTo = defSettings.CaseAttachmentPurgeEmail__c.split(',');
                }                
            }
            OrgWideEmailAddress[] orgwideaddress = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :qlikNoreply LIMIT 1];
            if (!orgwideaddress.isEmpty()) {
                mail.setOrgWideEmailAddressId(orgwideaddress[0].Id);
            }
            mail.setToAddresses(listTo);
            mail.setSubject('Attachments Failed to Delete');
            String plainTextBody = 'Batch Process has completed. \r\n\n'
                                        + 'Number of Attachments are successfully deleted: ' + successIdsCount + '\r\n\n'
                                        + 'Number Of Attachments failed to delete: ' + errorIds.size() + '\r\n\n';
            for (Attachment att: [SELECT Id, Name, ParentId FROM Attachment WHERE Id IN :errorIds]) {
                plainTextBody += '\r\n\nFailed to delete \'' + att.Name + '\' ' +
                        ' for Case '
                        + Url.getSalesforceBaseUrl().toExternalForm() + '/' + att.ParentId;
            }
            mail.setPlainTextBody(plainTextBody);    
            if (Test.isRunningTest()) {
                TEST_EMAIL_SENDING = true;
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}