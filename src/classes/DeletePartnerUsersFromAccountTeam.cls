/*****************************************************************************************************************
 Change Log:

 20130509	TJG CR 7707 https://eu1.salesforce.com/a0CD000000XOYhM
 			Unfortunately there is no way that we can create triggers on Account Team object, or a custom 
 			VisualForce page to replace the Add account team member button and dialog.  After much discussions
 			we decided to create an scheduled APEX task that removes partner users from account team. 
 			We can run this every 24 hours. 

******************************************************************************************************************/
global class DeletePartnerUsersFromAccountTeam implements Schedulable {
	static final String DEBUGPRIF = '--ATMARMATM-- ';
	static final string CLSNAME = 'DeletePartnerUsersFromAccountTeam';
	global void execute(SchedulableContext sc) {
		
		// retrieve all partner user account team members
		List<AccountTeamMember> partnerATMs = [Select UserId,Id From AccountTeamMember where UserId in (select Id from User where Profile__c like 'PRM%')];
		if (partnerATMs.size() > 0) {
			System.debug(DEBUGPRIF + CLSNAME + ' Deleting ' + partnerATMs.size() + ' partner user account team member(s).');
			Delete (partnerATMs);
		}
		else {
			System.debug(DEBUGPRIF + CLSNAME + ' No partner user account team members found.');
		}
	}
}