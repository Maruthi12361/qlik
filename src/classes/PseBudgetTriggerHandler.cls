/**********************************************************
* Class: 		PseBudgetTriggerHandler
* Description: 	Implements logic for PseBudgetTrigger trigger.
* Autor:		ext_vos
* Date:			2017-11-16
* Changes Log:
*	Date        author			CR#			Description
*
*	2010-09-22	Jason Favors				A trigger to mark a Project billable upon approval of a Budget.
*				(Appirio)					(Moved from PseMarkProjectBillableAfterBudgetApproval.trigger)	
*	2015-08-18	RDZ							Re-saving file to make it isValid in production
*	2017-11-09  ext_vos     	CHG0031444	Add new logic: populates the Project PO number when budget was approved.
*	2017-12-14	ext_vos			CHG0031444	Update PO number logic: set Project PO number when Budget.pse__Include_In_Financials__c change from false to true.
*	2019-03-25	ext_vos			CHG0034979	Update Billable logic: check pse__Status__c field during on the Budget creation.
*************************************************************/

public class PseBudgetTriggerHandler {

	private static final String APPROVED_STATUS = 'Approved';

	public static void handleAfterInsert(Map<Id, pse__Budget__c> newMapBudgets) {
		updateProjects(null, newMapBudgets);
	}

	public static void handleAfterUpdate(Map<Id, pse__Budget__c> oldMapBudgets, Map<Id, pse__Budget__c> newMapBudgets) {
		updateProjects(oldMapBudgets, newMapBudgets);
	}

	private static void updateProjects(Map<Id, pse__Budget__c> oldMapBudgets, Map<Id, pse__Budget__c> newMapBudgets) {
		// assemble a map of budgets with Project info
	    Set<Id> budgetIds = newMapBudgets.keySet();

	    List<pse__Budget__c> budgetsWithProject = [SELECT Id, PO_Number__c, pse__Approved__c, pse__Project__r.pse__Is_Billable__c, 
	    													pse__Project__r.PO_Number__c, pse__Include_In_Financials__c, pse__Status__c
										            FROM pse__Budget__c
										            WHERE Id in :budgetIds];

	    // assemble list of Projects to update
	    List<pse__Proj__c> updateProjects = new List<pse__Proj__c>();	    
		pse__Budget__c newBudget;
        pse__Budget__c oldBudget;
        Boolean beUpdated;
		for (pse__Budget__c budget : budgetsWithProject) {
	        beUpdated = false;
			newBudget = newMapBudgets.get(budget.Id);
			if (oldMapBudgets != null) {
				oldBudget = oldMapBudgets.get(budget.Id);
			}
			// check pse__Status__c field during on the Budget creation;
			// check pse__Approved__c flag during on creation/editing
			if (((newBudget.pse__Approved__c == true && (oldBudget == null || (oldBudget != null && newBudget.pse__Approved__c != oldBudget.pse__Approved__c))) 
					|| APPROVED_STATUS.equals(newBudget.pse__Status__c))
				&& !newBudget.pse__Project__r.pse__Is_Billable__c) {
	            
	            budget.pse__Project__r.pse__Is_Billable__c = true;
	            beUpdated = true;   	
			}
			if (((newBudget.pse__Include_In_Financials__c == true 
						&& (oldBudget == null || (oldBudget != null && newBudget.pse__Include_In_Financials__c != oldBudget.pse__Include_In_Financials__c)))
				 || (newBudget.pse__Approved__c == true 
				 		&& (oldBudget == null || (oldBudget != null && newBudget.pse__Approved__c != oldBudget.pse__Approved__c))) 
				 || (APPROVED_STATUS.equals(newBudget.pse__Status__c)
				 		&& (oldBudget == null || (oldBudget != null && newBudget.pse__Status__c != oldBudget.pse__Status__c))))) {
                budget.pse__Project__r.PO_Number__c = newBudget.PO_Number__c;
                beUpdated = true;   	
			}
			if (beUpdated) {
				updateProjects.add(budget.pse__Project__r);	 
			}
			
		}
		// Update modified related projects, if any
	    if (updateProjects.size() > 0) {
	        update updateProjects;
	    }	    
	}

}