/****************************************************************
*
*  QuoteApproverContoller_Test 
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*  15.11.2019  DKF: : BSL-2898 - Updated future/currentAppSize asserts
*****************************************************************/
@isTest
public  class QuoteApproverContoller_Test
{
    private static final String PARTNERACC = '01220000000DOFzAAO';

    private static testMethod void testMethod_1() {
       
            
          Profile p = [select id from profile where name='System Administrator']; 
          User u = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');


         System.runAs(u) {
            
            Boolean checkError=false;
        Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
        QuoteTestHelper.createCustomSettings();
        /*
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c companyQT = TestQuoteUtil.createQCompany(sub.id);
        */
        Account testResseller = TestQuoteUtil.createReseller();
        Contact testContact = TestQuoteUtil.createContact(testResseller .id);
        /*
        Account  testAcc = TestQuoteUtil.createAccount(companyQT.id); 
        */
        Account  testAcc = QTTestUtils.createMockAccount('TestCompany', u, true);
        testAcc.RecordtypeId = PARTNERACC;
        update testAcc;
        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Evaluation_Quote'];
          

        Test.startTest();
      SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
        quoteForTest.ApprovalStatus__c='Pending';
        update quoteForTest;
            Test.stopTest();
        
        Group testGroup = [Select id,Name from Group Where Name='Finance Orders Quote Approvers' Limit 1 ];
        GroupMember gm= new GroupMember(); 
        gm.GroupId=testGroup.id;
        gm.UserOrGroupId = u.id;
        insert gm;
              System.assertNotEquals(null, gm.id);
             
           List<sbaa__Approval__c> approvaList = new List<sbaa__Approval__c>();
              for(Integer i=0;i<10;i++){
                approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Requested',
                                                       sbaa__ApprovalStep__c=i,
                                                       sbaa__RecordField__c='Test'+i,
                                                       sbaa__AssignedTo__c=UserInfo.getUserId()
                                                       ));
                  approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Assigned',
                                                       sbaa__ApprovalStep__c=i+10,
                                                       sbaa__RecordField__c='Test'+i+10,
                                                       sbaa__AssignedTo__c=UserInfo.getUserId()
                                                       ));
                  approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Assigned',
                                                       sbaa__ApprovalStep__c=i+20,
                                                       sbaa__RecordField__c='Test'+i+20,
                                                       sbaa__AssignedGroupId__c =testGroup.id,
                                                       sbaa__AssignedTo__c=null
                                                       ));
                                    approvaList.add( new sbaa__Approval__c(
                                                     Quote__c=quoteForTest.id ,
                                                       sbaa__Status__c='Requested',
                                                       sbaa__ApprovalStep__c=i+30,
                                                       sbaa__RecordField__c='Test'+i+30,
                                                       sbaa__AssignedGroupId__c =testGroup.id,
                                                       sbaa__AssignedTo__c=null
                                                       ));
                }
             
           try{
quoteForTest.Quote_Approval_Reason__c = 'asdasdd';
update quoteForTest;
              insert approvaList;
            
        }
           catch(DmlException e) {
               checkError = true;   
          }
           System.assertEquals(false, checkError);
              QuoteApproverController cnt =new QuoteApproverController();
              cnt.getAssignedToApprovals();
              Integer futureAppSize = cnt.getFutureApprovalTitle().size();
              Integer currentAppSize = cnt.getCurrentPendingTitle().size();
              System.assertEquals(40, cnt.approvallis.size());
             System.assertEquals(40, cnt.approvallis.size());
              System.assertEquals(9, futureAppSize);
              System.assertEquals(9, currentAppSize);
            
          
         }
          
         




    
  }
}