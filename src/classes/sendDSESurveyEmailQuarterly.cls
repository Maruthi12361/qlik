/*************************************************************************

    sendDSESurveyEmailQuarterly
    
     Changelog:
        2015-04-14  AIN     Added description (this part)
                            CR #XXXXX - Test failure in production
                            Added try/catch to Messaging.sendEmail(mails);
                            
************************************************************************/
global class sendDSESurveyEmailQuarterly implements Schedulable{
    global void execute(SchedulableContext sc) {
        // Check if trigger has been run already this quarter
        Set<String> history = new Set<String>();
        List<QuarterlyEmail__c> qEmail = QuarterlyEmail__c.getAll().values();

        for (QuarterlyEmail__c historyElement : qEmail) {
            history.add(historyElement.Name);
        }

        // Jan = 1 , so we have to substract 1 to get the quarter correctly
        String currentYearQuarter = '' + date.today().year() + 'Q' + ((date.today().month()-1)/3 + 1);
        System.debug('sendDSESurveyEmailQuarterly:date:' + currentYearQuarter);

        if (history.contains(currentYearQuarter)) { // the test itself
            System.debug('sendDSESurveyEmailQuarterly:the trigger has been invoked already in ' + currentYearQuarter + '. Skipping.');
            return;
        }

        // Get the amount of all Contacts needed to receive survay
        List<Contact> dseContacts = [SELECT Id, Email FROM Contact WHERE DSE_Access__c = true and EmailBouncedDate = null];
        EmailTemplate et = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE Name='DSE Survey Email Template' LIMIT 1]; 

        if (dseContacts.size() == 0 || et == null) {
            return;
        }
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        try { Messaging.reserveSingleEmailCapacity(dseContacts.size()); } // check if we're not exceeding daily limits
        catch (Exception e) {
            System.debug('sendDSESurveyEmailQuarterly: EXCEPT|ON COUGHT: COULD NOT RESERVE ' + dseContacts.size() + ' SINGLE EMAIL CAPACITY');
        }
        
        OrgWideEmailAddress owa = [SELECT id, Address FROM OrgWideEmailAddress WHERE Address='supportfeedback@qlik.com'];

        for (Contact dseC : dseContacts) {
            Messaging.SingleEmailMessage mailVar = new Messaging.SingleEmailMessage();
            mailVar.setToAddresses(new List<String>{dseC.Email});
            if (owa == null) {
                mailVar.setSenderDisplayName('QlikView Support');
                mailVar.setReplyTo('supportfeedback@qlik.com');
            } else {
                mailVar.setOrgWideEmailAddressId(owa.Id);
            }
            mailVar.setSaveAsActivity(false);
            mailVar.setTemplateId(et.Id);
            mailVar.setTargetObjectId(dseC.Id);
            mailVar.setUseSignature(false);
            mails.add(mailVar);
        }

        try
        {
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(mails);
            for (Messaging.SendEmailResult res : results) {
                if (!res.Success) {
                    System.debug('sendDSESurveyEmailQuarterly: ONE OF EMAILS HAS FAILED');
                }
            }

            // save to custom setting table QuarterlyEmail current quarter
            QuarterlyEmail__c newQuarter = QuarterlyEmail__c.getInstance();
            newQuarter.Name = currentYearQuarter;
            newQuarter.SetupOwnerId = UserInfo.getOrganizationId();
            insert newQuarter;
        }
        catch(EmailException ex)
        {
            System.debug('There was an error while sending the emails: ' + ex.getMessage());
        }
    }
}