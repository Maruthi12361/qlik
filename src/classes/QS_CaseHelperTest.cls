/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_CaseHelperTest 
{
	private static Id networkId;
	private static Id recTypeId;
	static testMethod void test1() 
	{
		// creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

		//Create Contacts
		Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
		testContact.Persona__c = 'Decision Maker';
		testContact.LeadSource = 'leadSource';
		insert testContact;
		Contact testContact2 = TestDataFactory.createContact('test_FName', 'test_LName2', 'testSandbox2@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact2.Persona__c = 'Decision Maker';
        insert testContact2;
        Contact testContact3 = TestDataFactory.createContact('test_FName', 'test_LName3', 'testSandbox3@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact3.Persona__c = 'Decision Maker';
        insert testContact3;

        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

		// Create Community Users
		User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        User communityUser2 = TestDataFactory.createUser(profileId, 'testSandbox2@qlikTech.com', 'tSbo2', String.valueOf(testContact2.Id));
        insert communityUser2;
        User communityUser3 = TestDataFactory.createUser(profileId, 'testSandbox3@qlikTech.com', 'tSbo3', String.valueOf(testContact3.Id));
        insert communityUser3;

        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        
        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        
        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, environment.Id,
                                    productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
            
        insert caseObj;

        EntitySubscription entitySubscription = TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser.Id, caseObj.Id);

		List<EntitySubscription> entitySubscriptionList = new List<EntitySubscription>();
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser2.Id, caseObj.Id));
        entitySubscriptionList.add(TestDataFactory.createEntitySubscription(getNetworkIdCustom(), communityUser3.Id, caseObj.Id));

        List<String> stakeHolderIds = new List<String>();

        
        stakeHolderIds.add(communityUser2.Id);
        stakeHolderIds.add(communityUser3.Id);
        

        test.startTest();
        QS_CaseHelper.addStakeholder(entitySubscription);
        QS_CaseHelper.addStakeholders(entitySubscriptionList);

        system.assert(QS_CaseHelper.deleteStakeholder(caseObj.Id, communityUser.Id));
        system.assert(QS_CaseHelper.deleteStakeholders(caseObj.Id, stakeHolderIds));
        QS_CaseHelper.setEnvironmentToCase(caseObj.Id, environment.Id);

		test.stopTest();
		
	}

	private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
				User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' and NetWork.name = 'support' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live' and NetWork.name = 'support') LIMIT 1];
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}