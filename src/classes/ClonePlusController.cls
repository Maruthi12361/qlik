/* 

	Class: ClonePlusController
	
	Changelog:
        2013-04-25	Based upon work by: http://christopheralunlewis.blogspot.co.uk/2012/04/clone-plus-clone-salesforce-objects.html  
        2013-04-30  CCE     Modified for CR# 7927 - Rework of Account Plan feature and functionality
                            https://eu1.salesforce.com/a0CD000000YFNC8. This has been changed so that we can pass in various paramters
                            via the URL to allow us to modify several fields on the head object. In additon to the original child object
                            types we can also pass in parentfieldstoblank, fieldforrename and settocurrentdate. Each of these can take
                            a comma seperated list of fields to modify eg. parentfieldstoblank=Reviewer__c,Approved_by_Sales_Productivity__c
                            parentfieldstoblank - this is a list of fields on the parent to blank by setting them to either null of false.
                            fieldforrename - this is a list of fields to reanme. The rename is done by appending " Copy". Currently no checking
                            is done to limit the length of the field.
                            settocurrentdate - this is a list of Date fields that will be reset to the current Date.
                            The generic nature of the ClonePlus page and Contoller class means that their use is not limited to
                            any particular object. However this version has been modified and the "genericness" lost. To create a generic version
                            remove or comment out the "If creating a generic version then remove this line" lines.

*/
public class ClonePlusController {

  public List<relatedObjects> objectChildren  { get; set; }
  public String               objectTypeName  { get; set; }
  public String               objectName      { get; set; }
   
  private SObject headSObject, headClone;
  //A list of fields in the head object that need to be cleared
  private List<String> headObjectFieldsToNull = new List<String>{};
  //A list of date fields in the head object that need setting to the current date
  private List<String> headObjectSetToCurrentDate = new List<String>{};
  //A list of fields in the head object that are required and that we need to append the word ' Copy' to
  private List<String> headObjectFieldRequired = new List<String>{};
      
  // Initialisation method called when the clone plus page is loaded.
  // Use the id page parameter to find out what kind of 
  // object we are trying to clone.
  // Then load the object from the database.
  // Finally call the populateObjectChildren method to      
  public pagereference initialiseObjectsForCloning()
  {

    // Here we generate a keyprefixmap using the global describe 
    // Then compare that to our object to determine type.  
    Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
       
    Map<String,String> keyPrefixMap = new Map<String,String>{};
          
    for(String sObj : gd.keySet()){
      Schema.DescribeSObjectResult r = gd.get(sObj).getDescribe();
      keyPrefixMap.put(r.getKeyPrefix(), r.getName());
    }
      
    String objectID = ApexPages.currentPage().getParameters().get('id');
                        
    String objectTypeKey = objectId.subString(0,3);
      
    objectTypeName = keyPrefixMap.get(objectTypeKey);
      
    String primaryObjectQueryString = 'SELECT Id, Name FROM '
                                    + objectTypeName
                                    + ' WHERE Id = \''
                                    + objectId
                                    + '\'';
    
    headSObject = Database.query(primaryObjectQueryString);
    objectName = '' + headSObject.get('Name');
    populateObjectChildren();
    
    String sUrl = ApexPages.currentPage().getUrl().toLowerCase();
    system.debug('ClonePlusController>sUrl = ' + sUrl);
    // read the fields we need to clear on the copied head object from the page parameter (URL).    
    if (sUrl.contains('parentfieldstoblank')) {   
    	headObjectFieldsToNull.addAll(ApexPages.currentPage().getParameters().get('parentfieldstoblank').split(','));
    	//for(integer i=0; i<headObjectFieldsToNull.size(); i++) {
    	//	system.debug('ClonePlusController>initialiseObjectsForCloning>headObjectFieldsToNull[' + i +'] = ' + headObjectFieldsToNull[i]);    	
       	//}
    }
    // read the fields we need to set to the current date on the copied head object from the page parameter (URL).
    if (sUrl.contains('settocurrentdate')) {   
    	headObjectSetToCurrentDate.addAll(ApexPages.currentPage().getParameters().get('settocurrentdate').split(','));
    	//for(integer i=0; i<headObjectSetToCurrentDate.size(); i++) {
    	//	system.debug('ClonePlusController>initialiseObjectsForCloning>headObjectSetToCurrentDate[' + i +'] = ' + headObjectSetToCurrentDate[i]);    	
       	//}
    }
    // read the fields we need to append ' Copy' to on the copied head object from the page parameter (URL).
    if (sUrl.contains('fieldforrename')) {   
    	headObjectFieldRequired.addAll(ApexPages.currentPage().getParameters().get('fieldforrename').split(','));
    	//for(integer i=0; i<headObjectFieldRequired.size(); i++) {
    	//	system.debug('ClonePlusController>initialiseObjectsForCloning>headObjectFieldRequired[' + i +'] = ' + headObjectFieldRequired[i]);
    	//}
    }
    return doClone();	//If creating a generic version then remove this line    
  }

  // Get all of the children of the current object that have a 
  // object type contained in the child object types page parameter.
  // Not restricting the child objects to particular types 
  // results in unclonable system objects being added to the options, 
  // which we need to avoid (You will not want to clone these!)
  // Making these object type choices also allows us 
  // focus our efforts on the specific kinds of objects 
  // we want to allow users to clone.  
  public void populateObjectChildren()
  {
       
    objectChildren = new List<relatedObjects>{};
        
    Set<String> childObjectTypes = new Set<String>{};
    
    // read the object types from the page parameter.    
    childObjectTypes.addAll(ApexPages.currentPage().getParameters().get('childobjecttypes').split(','));
    
    // Use the sobjecttype describe method to retrieve all child relationships for the object to be cloned.    
    Schema.DescribeSObjectResult headDescribe = headsObject.getSObjectType().getDescribe();
    
    List<Schema.ChildRelationship> childRelationships = headDescribe.getChildRelationships(); 
    
    // Iterate through each relationship, and retrieve the related objects.       
    for (Schema.ChildRelationship childRelationship : childRelationships)
    {
      Schema.SObjectType childObjectType = childRelationship.getChildSObject();
      
      // Only retrieve the objects if their type is included in the page argument.          
      if (childObjectTypes.contains(childObjectType.getDescribe().getName()))
      {
        List<relatedObjectRow> relatedObjects = new List<relatedObjectRow>{};
                
        Schema.SObjectField childObjectField = childRelationship.getField();
                
        String relatedChildSObjectsquery = 
               'SELECT ID, Name FROM ' 
             + childObjectType.getDescribe().getName()
             + ' WHERE '
             + childObjectField.getDescribe().getName()
             + ' = \'' 
             + headsObject.Id
             + '\''; 
                                                        
        for (SObject childObject : Database.query(relatedChildSObjectsquery))
        {
          relatedObjects.add(new relatedObjectRow(childObject));
        }
            
        if (!relatedObjects.isEmpty())
        {
          objectChildren.add(new relatedObjects(relatedObjects, 
                childObjectType.getDescribe().getLabelPlural(), 
                childObjectField.getDescribe().getName()));
        }  
      }
    }
  }
  
  // Perform the cloning process.
  // First clone the parent, then all of the child objects. 
  // Then redirect the user to the new object page.
  public PageReference doClone()
  {
    headClone = cloneObjects(new List<sObject>{headSObject}, headObjectFieldsToNull, headObjectSetToCurrentDate, headObjectFieldRequired).get(0);
    
    insert headClone;
    
    cloneSelectedObjects();
    
    return new PageReference('/' + headClone.Id + '/e?retURL=/' + headClone.Id);
  }
  
  // Clone the selected child objects.
  // Associate the cloned objects with the new cloned parent object.
  public void cloneSelectedObjects()
  {
        
    List<sObject> clonedObjects = new List<sObject>{};
    List<sObject> selectedRelatedObjects;
     
    for (relatedObjects relatedObject : objectChildren)
    {
      selectedRelatedObjects = new List<sObject>{};  
      clonedObjects = new List<sObject>{};  
      
      for (relatedObjectRow row : relatedObject.objectRows) 
      {
        if (row.selected)
        {
          selectedRelatedObjects.add(row.obj);
        }
      }
      
      if (!selectedRelatedObjects.isEmpty())
      {
        clonedObjects = cloneObjects(selectedRelatedObjects, null, null, null);
        
        for (sObject clone : clonedObjects)
        {
          clone.put(relatedObject.relatedFieldName, headClone.Id);  
        }
        
        insert clonedObjects;
      }
    }
  }

  // Clone a list of objects to a particular object type
  // Parameters 
  // - List<sObject> sObjects - the list of objects to be cloned 
  // The sObjects you pass in must include the ID field, 
  // and the object must exist already in the database, 
  // otherwise the method will not work.
  public static List<sObject> cloneObjects(List<sObject> sObjects, List<String> fieldsToBlank, List<String> datesToSet, List<String> fieldForRename){
                                                
    Schema.SObjectType objectType = sObjects.get(0).getSObjectType();
    
    // A list of IDs representing the objects to clone
    List<Id> sObjectIds = new List<Id>{};
    // A list of fields for the sObject being cloned
    List<String> sObjectFields = new List<String>{};
    // A list of new cloned sObjects
    List<sObject> clonedSObjects = new List<sObject>{};
    
    // Get all the fields from the selected object type using the get describe method on the object type.
    if(objectType != null){
      sObjectFields.addAll(objectType.getDescribe().fields.getMap().keySet());
    }
    
    // If there are no objects sent into the method, then return an empty list
    if (sObjects != null || 
        sObjects.isEmpty() || 
        sObjectFields.isEmpty()){
    
      // Strip down the objects to just a list of Ids.
      for (sObject objectInstance: sObjects){
        sObjectIds.add(objectInstance.Id);
      }

      /* Using the list of sObject IDs and the object type, 
         we can construct a string based SOQL query 
         to retrieve the field values of all the objects.*/
    
      String allSObjectFieldsQuery = 'SELECT ' + sObjectFields.get(0); 
    
      for (Integer i=1 ; i < sObjectFields.size() ; i++){
        allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
      }
    
      allSObjectFieldsQuery += ' FROM ' + 
                               objectType.getDescribe().getName() + 
                               ' WHERE ID IN (\'' + sObjectIds.get(0) + 
                               '\'';

      for (Integer i=1 ; i < sObjectIds.size() ; i++){
        allSObjectFieldsQuery += ', \'' + sObjectIds.get(i) + '\'';
      }
    
      allSObjectFieldsQuery += ')';
	  
      try{
      
        // Execute the query. For every result returned, 
        // use the clone method on the generic sObject 
        // and add to the collection of cloned objects
        for (SObject sObjectFromDatabase : Database.query(allSObjectFieldsQuery)){
			SObject sTmp = sObjectFromDatabase.clone(false,true);	//deep clone
			
			//Blank the requested fields
			//Create a map to use later for when we want to get the field types (we need this to decide if we need to set them to null or false)
    		Map<String, SObjectField> allFields = sTmp.getSObjectType().getDescribe().fields.getMap();    		
    		if ((fieldsToBlank != null) && (fieldsToBlank.size() > 0)) {
				for(integer i=0; i<fieldsToBlank.size(); i++) {
    				for(integer j=0; j<sObjectFields.size(); j++) {
    					if (sObjectFields.get(j) == fieldsToBlank.get(i)) {
    						
    						Schema.DescribeFieldResult f = allFields.get(sObjectFields.get(j)).getDescribe();
							
							// Get the describe result from the token
							f = f.getSObjectField().getDescribe();
    						Schema.DisplayType fielddataType = f.getSObjectField().getDescribe().getType();
    						
    						if (fielddataType == Schema.DisplayType.Boolean) {
    							sTmp.put(sObjectFields.get(j), false);
    						}
    						else {
    							sTmp.put(sObjectFields.get(j), null);
    						}
    					}
    				}
				}
			}
			
			//Set the requested Date fields to the current Date
			if ((datesToSet != null) && (datesToSet.size() > 0)) {
				for(integer i=0; i<datesToSet.size(); i++) {
    				for(integer j=0; j<sObjectFields.size(); j++) {
    					if (sObjectFields.get(j) == datesToSet.get(i)) {
    						sTmp.put(sObjectFields.get(j), Date.today());
    					}
    				}
				}
			}
			        		
			//Append ' Copy' to the requested fields
			if ((fieldForRename != null) && (fieldForRename.size() > 0)) {
				for(integer i=0; i<fieldForRename.size(); i++) {
    				for(integer j=0; j<sObjectFields.size(); j++) {
    					if (sObjectFields.get(j) == fieldForRename.get(i)) {
    						String sNewName = sTmp.get(sObjectFields.get(j)) + ' Copy';
    						sTmp.put(sObjectFields.get(j), sNewName);
    					}
    				}
				}
			}        		
			clonedSObjects.add(sTmp);        		
			//clonedSObjects.add(sObjectFromDatabase.clone(false,true));  headObjectSetToCurrentDate
        }
    
      } catch (exception e){
      }
      
    }
   
    return clonedSObjects;
    
  }
  
  // Related objects data construct - 
  // used to store a collection of child objects connected to 
  // the head object through the same relationship field.
  public class relatedObjects
  {
    public List<relatedObjectRow> objectRows { get; set; }
    public String                 pluralLabel      { get; set; }
    public String                 relatedFieldName { get; set; }
    
    public relatedObjects(List<relatedObjectRow> objectRows, 
                          String pluralLabel, 
                          String relatedFieldName) 
    {
      this.objectRows       = objectRows;
      this.pluralLabel      = pluralLabel;
      this.relatedFieldName = relatedFieldName;
    }   
  }     

  // An indidual child object row. 
  // Each instance simply contains the object definition, 
  // and a checkbox to select the row for cloning 
  // on the clone plus page.
  public class relatedObjectRow
  {
    public sObject obj      { get; set; }
    public Boolean selected { get; set; }
    
    public relatedObjectRow(Sobject obj)
    {
      this.obj      = obj;
      // All object rows are selected by default.
      this.selected     = true;
    }
    
    public String getName(){
      try{
        return '' + obj.get('Name');
      } catch (Exception e){
        return '';
      }    
    }   
  }
}