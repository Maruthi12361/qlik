// Test case to test the triggers: SOIUpdateContact
// Changelog:
// 2013-04-03  Madhav Kakani - Fluido Oy Initial development for CR# 7356
// 2018-10-18  CCE CHG0034877 BMW-1074 Update Contact Status field on Contact from SOI 
//             (Also updated existing test as it wasn't testing anything and provided no coverage and removed See All Data dependency)
//
@isTest //(SeeAllData=true)
private class SOIUpdateContactTriggerTest {
//static testMethod void runTest() {
//    User[] ulist = [select id from User where alias='mkak'];
//    if (ulist.size() > 0) {
//        System.RunAs(ulist[0]) {  //Run as the first user
        
//        QlikTech_Company__c qtc = new QlikTech_Company__c();
//        qtc.name = 'SWE';
//        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
//        qtc.Country_Name__c = 'Sweden';
//        qtc.CurrencyIsoCode = 'SEK';
//        insert qtc;
    
//        Account act = new Account(name='Test Account');
//        act.QlikTech_Company__c = 'QlikTech Nordic AB';
//        act.Billing_Country_Code__c = qtc.Id;
//        insert act;
       
//        Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
//        insert ct;

//        // Query an existing Opp in the organization. This is because inserting a test opp is failing the test
//        // due to the insert operation triggering web service callouts.
//        // web service callouts are not supported in test methods.
//        Opportunity opp = [SELECT Id, Name, StageName FROM Opportunity WHERE Name='Test Opp' LIMIT 1];
//        system.assert(opp != null);

//        Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
//        soi.Opportunity__c = opp.Id;
//        soi.Contact__c = ct.Id;
//        soi.Role__c = 'Beneficiary';    
//        insert soi;
    
//        Contact ctRet = [SELECT Is_Closed_Won_SOI__c FROM Contact WHERE Id = :ct.Id];
//        system.assert(ctRet.Is_Closed_Won_SOI__c == 'Yes');
   
//        delete soi;
    
//        ctRet = [SELECT Is_Closed_Won_SOI__c FROM Contact WHERE Id = :ct.Id];
//        system.assert(ctRet.Is_Closed_Won_SOI__c == 'No');    
//        }
//    }
//  }

    @isTest static void test_WithClosedWonOpp() {
        
        QTTestUtils.GlobalSetUp();
        Opportunity opp;
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        System.RunAs(objUser) {
            string oppname = 'sNameTestOpp';
            string oppstage= 'Closed Won';
            
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
            if (rt != null){
                opp = QTTestUtils.createMockOpportunity('sShortDescription', oppname, 'Existing Customer', 'Direct', oppstage,
                                     ''+rt.Id, 'GBP', objUser, true);                
            }
            system.assert(opp != null);

            Contact ct = QTTestUtils.createMockContact();

            Test.starttest();

            //Create soi 
            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = opp.Id;
            soi.Contact__c = ct.Id;
            soi.Role__c = 'Beneficiary';    
            insert soi;
            Test.stoptest();

            //Test that the Contact Is_Closed_Won_SOI__c field is updated correctly    
            Contact ctRet = [SELECT Is_Closed_Won_SOI__c FROM Contact WHERE Id = :ct.Id];
            system.assert(ctRet.Is_Closed_Won_SOI__c == 'Yes');
       
            delete soi;
        
            ctRet = [SELECT Is_Closed_Won_SOI__c FROM Contact WHERE Id = :ct.Id];
            system.assert(ctRet.Is_Closed_Won_SOI__c == 'No');
        }
    }

    @isTest static void test_simulateSOICreationFromLeadConvert() {
        
        QTTestUtils.GlobalSetUp();
        Opportunity opp;
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        System.RunAs(objUser) {
            string oppname = 'sNameTestOpp';
            string oppstage= 'Goal Identify';
            
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
            if (rt != null){
                opp = QTTestUtils.createMockOpportunity('sShortDescription', oppname, 'Existing Customer', 'Direct', oppstage,
                                     ''+rt.Id, 'GBP', objUser, true);                
            }
            system.assert(opp != null);

            Contact ct = QTTestUtils.createMockContact();

            Test.starttest();

            //Create soi as though it had been created from a Lead (i.e. from a Lead Convert)
            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = opp.Id;
            soi.Contact__c = ct.Id;
            soi.From_Lead__c = true;
            soi.Role__c = 'Champion';    
            insert soi;
            Test.stoptest();

            //Test that the Contact status field has been updated with the Opportunity StageName value
            Contact ctRet = [SELECT id, Contact_Status__c FROM Contact WHERE Id = :ct.Id];
            System.assertEquals(ctRet.Contact_Status__c, oppstage);
        }
    }
}