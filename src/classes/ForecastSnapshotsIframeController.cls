/**
* Change log:
* 2018-10-08	ext_vos	CHG0033956: use custom settings for URL.
*
*/
public with sharing class ForecastSnapshotsIframeController {
	public String historyUrl {get; private set;}

	public ForecastSnapshotsIframeController() {
		QTCustomSettings__c settings = QTCustomSettings__c.getInstance('Default');
		if (settings != null && settings.ForecastHistoryURL__c != null) {
            historyUrl = settings.ForecastHistoryURL__c;
        }
        if (String.isBlank(historyUrl)) {
        	ApexPages.Message errMessage = new ApexPages.Message(ApexPages.Severity.Info, 'History URL isn\'t setup.');
            ApexPages.addMessage(errMessage);  
        }
	}
}