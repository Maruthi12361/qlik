/*Change Log:
*
*       MTM 2014-11-28 CR# 17952
*
******/
global class BoomiUtil {
        public static void updateBoomiStatus(String id) { 
            Boomi_ExeCtrl__c exStatus = getBoomiStatus(id);
            if(exStatus != null){
                delete exStatus;
            } 
            List<Boomi_ExeCtrl__c> updateRecord= new List<Boomi_ExeCtrl__c>();
            Boomi_ExeCtrl__c boomiStatus = new Boomi_ExeCtrl__c();               
            boomiStatus.Status__c = 'Pending';
            boomiStatus.Message__c = null;
            boomiStatus.ObjectId__c = id;
            
            updateRecord.add(boomiStatus);
            upsert updateRecord;
        }
        
        public static Boomi_ExeCtrl__c getBoomiStatus(String id) {
            for(Boomi_ExeCtrl__c status : [SELECT Id, Status__c, Message__c, ObjectId__c FROM Boomi_ExeCtrl__c WHERE ObjectId__c =: id LIMIT 1]){
                return status;
            }
             
            return null;             
        }
        
        public static QTCustomSettings__c getCustomSettings() {
            for(QTCustomSettings__c setting : [SELECT BoomiBaseURL__c, BoomiToken__c From QTCustomSettings__c where name = 'default']){
                if (setting.BoomiBaseURL__c != null && setting.BoomiBaseURL__c != '' && setting.BoomiToken__c != null && setting.BoomiToken__c != '')//Ensure Boomi settings are there
                    return setting; //Boomi URL and Token
            }
            return null; 
        }
        
        @future (callout=true)
        public static void callBoomi(String objectId, String boomiMethodToCall, String message) {
            /*QTCustomSettings__c custSettings = getCustomSettings();

            if (custSettings == null || custSettings.BoomiBaseURL__c == null) {
                system.debug('Cannot call boomi, settings are missing');
                return;
            }
    
            //Update the BOOMI Status table..it will be changed by Boomi once the operation is finished              
            //updateBoomiStatus(objectId);

            String urlToCall = custSettings.BoomiBaseURL__c + boomiMethodToCall;

            system.Debug('Calling BOOMI  ' + urlToCall + ' at '+ Datetime.now() +' with Message :: ' + message);
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(urlToCall);
            // HTTP headers, Content-Type and Digest Authorization.
            req.setHeader('Content-Type','text/xml');
            req.setHeader('Authorization',custSettings.BoomiToken__c); 
            //Set the message as HTTPRequest body   
            req.setBody(message);
            
            Http http = new Http();    
            try {
                //Execute web service call here     
                HTTPResponse res = http.send(req);  
 
                //Helpful debug messages
                System.debug(res.toString());
                System.debug('STATUS:' + res.getStatus());                  
                System.debug('STATUS_CODE:' + res.getStatusCode());
                
            } catch(System.CalloutException e) {
                    //Exception handling goes here....
                    System.debug('Error Calling BOOMI : ' + e);
            }*/
            boomiWSCall(objectId, boomiMethodToCall, message);
        }

        public static String callBoomiSync(String objectId, String boomiMethodToCall, String message) {

            return boomiWSCall(objectId, boomiMethodToCall, message);
        }

        private static String boomiWSCall(String objectId, String boomiMethodToCall, String message){
            QTCustomSettings__c custSettings = getCustomSettings();

            if (custSettings == null || custSettings.BoomiBaseURL__c == null) {
                system.debug('Cannot call boomi, settings are missing');
                return 'Cannot call boomi, settings are missing';
            }
    
            //Update the BOOMI Status table..it will be changed by Boomi once the operation is finished              
            //updateBoomiStatus(objectId);

            String urlToCall = custSettings.BoomiBaseURL__c + boomiMethodToCall;

            system.Debug('Calling BOOMI  ' + urlToCall + ' at '+ Datetime.now() +' with Message :: ' + message);
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(urlToCall);
            // HTTP headers, Content-Type and Digest Authorization.
            req.setHeader('Content-Type','text/xml');
            req.setHeader('Authorization',custSettings.BoomiToken__c); 
            //Set the message as HTTPRequest body   
            req.setBody(message);
            
            Http http = new Http();    
            try {
                //Execute web service call here     
                HTTPResponse res = http.send(req);  
 
                //Helpful debug messages
                System.debug(res.toString());
                System.debug('STATUS:' + res.getStatus());                  
                System.debug('STATUS_CODE:' + res.getStatusCode());
                return (res.toString());
                
            } catch(System.CalloutException e) {
                    //Exception handling goes here....
                    System.debug('Error Calling BOOMI : ' + e);
                    return e.getMessage();
            }

        }
}