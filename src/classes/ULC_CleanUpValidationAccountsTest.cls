/********************************************************

ULC_CleanUpValidationAccountsTest
Test class for ULC_CleanUpValidationAccounts

ChangeLog:
2015-02-10	AIN 	Initial implementation
***********************************************************/

@isTest
public with sharing class ULC_CleanUpValidationAccountsTest {

	//Account should be deleted
	public static testmethod void test1() 
	{
		string accountName = 'Clean up validation Accounts Test';
		CreateTestAccount(accountName, true);

		ULC_CleanUpValidationAccounts.CleanUpValidationAccounts();

		List<Account> testAccounts = [select id, name from Account where Name = :accountName];

		system.assert(testAccounts.size() == 0);
	}
	//Account should still be there
	public static testmethod void test2() 
	{
		string accountName = 'Clean up validation Accounts Test2';
		CreateTestAccount(accountName, false);

		ULC_CleanUpValidationAccounts.CleanUpValidationAccounts();

		List<Account> testAccounts = [select id, name from Account where Name = :accountName];

		system.assert(testAccounts.size() == 1);
	}
	public static testmethod void test3()
	{
		Test.StartTest();
		ULC_CleanUpValidationAccounts sh1 = new ULC_CleanUpValidationAccounts();
		String sch = '0 0 23 * * ?'; 
		system.schedule('Test ULC_CleanUpValidationAccounts Check', sch, sh1); 
		Test.stopTest();
	}
	private static Id CreateTestAccount(string accountName, boolean isValidationAccount)
	{
		CreateTestQTCompany();

        QlikTech_Company__c company = [Select id, QlikTech_Company_Name__c from QlikTech_Company__c where QlikTech_Company_Name__c = 'QlikTech Nordic AB' limit 1];
        system.debug('QlikTech_Company_Name__c: ' + company.QlikTech_Company_Name__c);
        
        Account TestAccount = new Account(
                OwnerId = UserInfo.getUserId(), Name = accountName, Navision_Status__c = 'Customer',
                QlikTech_Company__c = 'Sweden', Billing_Country_Code__c = company.id, 
                BillingStreet = 'Street 123', BillingCity = 'City 123', BillingState = 'S 123',
        		BillingCountry = 'Sweden', ShippingStreet = 'Street 123', ShippingCity = 'City 123',
        		ShippingState = 'S 123', ShippingCountry = 'Sweden',Account_Creation_Reason__c = 'Test',
        		IsValidationAccount__c = isValidationAccount);
        insert TestAccount;
        return TestAccount.Id;
	}
	private static void CreateTestQTCompany()
	{
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c qtc = new QlikTech_Company__c();
        qtc.name = 'SWE';
        qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
        qtc.Country_Name__c = 'Sweden';
        qtc.CurrencyIsoCode = 'SEK';
		qtc.Subsidiary__c = testSubs1.id;
        insert qtc;
	}
}