/********************************************************
* QS_PageTemplateController
* Description:
*
* Change Log:
* 2016-04-13   NAD              Removed Responsible_Partner__r reference and replaced Contact.Account.Responsible_Partner__c reference with Responsible_Partner_count__c per CR 33068.
* 2017-12-12    ext_bad         LCE-8 Change "Contact Support" button to go to the Knowledge before case (supportSidebar variable)
* 2019-05-05    AIN             IT-1597 Updated for Support Portal Redesign
**********************************************************/
public with sharing class QS_PageTemplateController {
    //all
    public string UserId { get {return (UserInfo.getUserId());} }
    //Page template
    public string FirstName { 
        get {
                string firstname = UserInfo.getFirstName();
                if(firstname == null)
                    firstname = UserInfo.getLastName();
                return firstname;
            } 
        }
    public string FirstNameTrunc { 
        get {
            string firstname = UserInfo.getFirstName();
            if(firstname == null)
                firstname = UserInfo.getLastName();
            String firstnameAbb = firstname.abbreviate(30);
            return firstnameAbb;
        } 
    }

    //Page template
    public List<Case> caselist;
    public List<Case> caselist2;
    public Boolean showBugPopUp { get; set; }

    public Boolean enabledChat { get; set; }
    public List<Case> bugCaseList { get; set; }

    private Id contactId;

    //Page template
    public List<Case> caseSurveyList { get; set; }
    //Page template & home page
    public Boolean unauthenticated { get; set; }
    public Boolean isPartnerAccount { get; set; }

    //Page template
    public static final String STATUS_PENDING_CONTACT_RESPONSE = 'Pending Contact Response';
    public static final String STATUS_CONTACT_RESPONSE_RECEIVED = 'Contact Response Received';
    public static final String STATUS_SOLUTION_SUGGESTED = 'Solution Suggested';
    public static final String STATUS_CLOSED = 'Closed';
    public static final String STATUS_COMPLETED = 'Completed';
    public static final String STATUS_RESOLVED = 'Resolved';

    // Id of selected survey
    //Page template    
    public String surveyIdChosen { get; set; }

    //Page template
    public string LoginPageURL { get; set; }
    public string CSSBaseURL { get; set; }

    //Used to create a link to the livechat
    //Page template
    public string LiveAgentAPIEndpoint { get; set; }
    public string OrganizationId { get; set; }
    public string LiveChatDeploymentId { get; set; }


    public string UserName { get {return (UserInfo.getName());} }
    //public Boolean noEmail4me { get; set; }
    public String profileImageUrl { get; set; }
    //private Contact myContact;
    //all
    //public String[] SelectedPersona { get; set; }
    //String[] Persona = new String[]{
    //};
    public boolean IsSandbox { get; set; }
    public boolean isDirectCustomerToQlik { get; set; }
    public Id AccountId { get; set; }
    public boolean supportSidebar { get; set; }

    public Map<string, string> bodyIdMap { get; set; }

    public string cookieHideAfterClickValue { get; set; }
    public string cookieLastAnnouncementNameValue { get; set; }
    public string lastAnnouncementName { get; set; }

    public integer AnnouncementCookieExpireTime { get; set; }
    public integer WelcomeScreenCookieExpireTime { get; set; }
    

    //all

    public string caseListSize { 
        get{
            if(caseList.Size() > 0)
                return ', you have ' + caseList.Size() + ' cases awaiting action.';
            else
                return '';
        }
    }
    public string caseListSize2 { 
        get{
            return String.valueOf(caseList.Size());
        }
    }
    public integer numberOfAnnouncements { get; set; }
    public QS_PageTemplateController() {

        system.debug('AIN 1');
        Map<string, Cookie> cookies =  ApexPages.currentPage().getCookies();
        for (Cookie cookie : cookies.values()){
            if(cookie.getName() == 'hide-after-click'){
                cookieHideAfterClickValue = cookie.getValue();
            }
            else if (cookie.getName() == 'last-announcement-name'){
                cookieLastAnnouncementNameValue = cookie.getValue();
            }
        }
        system.debug('AIN 2');

        PageReference currenPage = ApexPages.currentPage();
        caseList  = getCaseList();
        supportSidebar = currenPage.getUrl().toLowerCase().contains(Page.QS_CoveoSearch.getUrl());
        isDirectCustomerToQlik = false;
        system.debug('QS_PageTemplateController: UserID' + Userid);

        isPartnerAccount = false;
        // enabledChat = false;
        User currentUser;
        // retrieving the current user info
        if (userinfo.getUserId() != null) {
            currentUser = [
                    Select Id, Name, ContactId, Contact.AccountId, Contact.Persona__c, Contact.Name, Contact.Account.Name, Contact.Account.IsPartner, Contact.Account.IsCustomerPortal,
                            Contact.Account.QT_Designated_Support_Contact__c, Contact.Account.Partner_Support_Contact__c, Contact.Account.Responsible_Partner_count__c, Contact.Account.Navision_Status__c, AccountId
                    From User
                    where Id = :userinfo.getUserId()
                    LIMIT 1
            ];
        }

        // Specifying whether the current user is a customer or partner
        AccountId = (currentUser != null && currentUser.ContactId != null && currentUser.Contact.AccountId != null ? currentUser.Contact.AccountId : null);
        isPartnerAccount = (currentUser != null && currentUser.ContactId != null && currentUser.Contact.AccountId != null && currentUser.Contact.Account.IsPartner != null) ? currentUser.Contact.Account.IsPartner : false;

        showBugPopUp = false;

        unauthenticated = (currentUser == null || currentUser.ContactId == null || currentUser.AccountId == null) ? true : false;


        initBug();
        initSurvey();

        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        LoginPageURL = partnerPortalURLs.Support_Portal_Login_Page_Url__c;
        CSSBaseURL = partnerPortalURLs.Support_Portal_CSS_Base__c;
        LiveAgentAPIEndpoint = partnerPortalURLs.Support_Portal_Live_Agent_API_Endpoint__c;
        OrganizationId = GetOrganizationId();
        LiveChatDeploymentId = GetLiveChatDeploymentId();
        
        system.debug('OrganizationId: ' + OrganizationId);
        system.debug('LiveChatDeploymentId: ' + LiveChatDeploymentId);
        IsSandbox = [select Id, IsSandbox from Organization limit 1].IsSandbox;
        isDirectCustomerToQlik = CheckIfQlikCustomer();
        System.debug('isDirectCustomerToQlik: ' + isDirectCustomerToQlik);

        QS_SearchSettings__c qsSearchSettings = QS_SearchSettings__c.getInstance('SearchSettings');
        AnnouncementCookieExpireTime = (integer)qsSearchSettings.Announcement_Cookie_Expire_Time__c;
        WelcomeScreenCookieExpireTime = (integer)qsSearchSettings.Welcome_Screen_Cookie_Expire_Time__c;


        bodyIdMap = new Map<string, string>();
        bodyIdMap.Put('qs_authenticateduser', 'home');
        bodyIdMap.Put('qs_contactform', 'contact-us');
        bodyIdMap.Put('qs_contactus', 'contact-us');
        bodyIdMap.Put('qs_home_page', 'home');
        bodyIdMap.Put('qs_environmentlistpage', 'enviroments');
        bodyIdMap.Put('qs_supporttechspertthursdays', 'webinar-landingpage');
        bodyIdMap.Put('qs_caselistpage', 'manage-cases');
        bodyIdMap.Put('qs_populartopics', 'popular-topics');
        bodyIdMap.Put('qs_products', 'products');
        bodyIdMap.Put('qs_qliksense', 'qlik-sense');
        bodyIdMap.Put('qs_qlik-view', 'qlik-view');
        bodyIdMap.Put('qs_casedetails', 'casedetails');

        /*if (null == noEmail4me) {
          getMyContact();
          noEmail4me  = null == myContact ? false : (null == myContact.QS_Opt_out_case_emails__c ? false : myContact.QS_Opt_out_case_emails__c);
        }
        if(myContact != null)
        {
            SelectedPersona = myContact.Persona__c == null ? new string[]{} : myContact.Persona__c.split(';');
            for(Integer j = 0;j<SelectedPersona.size();j++)
                SelectedPersona[j] = SelectedPersona[j].Trim();
        }*/

        //getPersonasList();

        numberOfAnnouncements = database.countQuery('SELECT count() FROM Support_Announcement__c WHERE (Start_Date__c <= TODAY AND End_Date__c >= TODAY AND Active__c = true) OR (Active__c = true AND End_Date__c = null)');
        List<Support_Announcement__c> lastNameAnnouncements = [SELECT Name FROM Support_Announcement__c WHERE (Start_Date__c <= TODAY AND End_Date__c >= TODAY AND Active__c = true) OR (Active__c = true AND End_Date__c = null) order by name desc limit 1];
        if(lastNameAnnouncements.size() > 0){
            lastAnnouncementName = (string)lastNameAnnouncements[0].Id;
            
            system.debug('cookieLastAnnouncementNameValue: ' + cookieLastAnnouncementNameValue);
            system.debug('lastAnnouncementName: ' + lastAnnouncementName);
            if(cookieLastAnnouncementNameValue == null || (lastAnnouncementName != cookieLastAnnouncementNameValue)){
                Cookie c1 = new cookie('last-announcement-name', lastAnnouncementName, null, 604800, false);
                Cookie c2 = new cookie('hide-after-click', 'no', null, 604800, false);
                ApexPages.currentPage().setCookies(new Cookie[]{c1, c2});
                cookieHideAfterClickValue = 'no';
            }
        }
    }

    public QS_PageTemplateController gettheController() {

        return this;

    }

    public list<SelectOption> getPersonasList()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult fieldResult = contact.Persona__c.getDescribe();

        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : plvalues ) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }

    public string BodyTags {
        get{
            string currentPage = ApexPages.currentPage().getUrl().toLowerCase();

            system.debug('Last index of /: ' + currentPage.lastIndexOf('/'));
            system.debug('Last index of ?: ' + currentPage.lastIndexOf('?'));
            if(currentPage.lastIndexOf('/') == -1)
                return '';
            if(currentPage.lastIndexOf('?') == -1)
                currentPage = currentPage.mid(currentPage.lastIndexOf('/') + 1, currentPage.Length()-currentPage.lastIndexOf('/') - 1);
            else
                currentPage = currentPage.mid(currentPage.lastIndexOf('/') + 1, currentPage.lastIndexOf('?') - currentPage.lastIndexOf('/') - 1);
            system.debug('currentPage: ' + currentPage);
            if(bodyIdMap.containsKey(currentPage))
                return bodyIdMap.get(currentPage);
            else
                return '';
        }
    }
    public string ClassTags {
        get{
            system.debug('cookieHideAfterClickValue: ' + cookieHideAfterClickValue);
            system.debug('numberOfAnnouncements: ' + numberOfAnnouncements);
            string classTags = '';
            if(UserInfo.getUserId() != '005D0000004wTRsIAM')
                 classTags += 'myaccount';
            if(numberOfAnnouncements > 0 && (cookieHideAfterClickValue == null || cookieHideAfterClickValue == 'no'))
            {
                if(classTags != '')
                    classTags += ' ';
                classTags += 'myalerts';
            }
            return classTags;
            return '';
        }
    }
    //Page template
    private void initBug() {
        try {
            List<Bugs__History> bugHistoryList;
            //Added as tests don't have access to bughistory                                                
            if (test.isRunningTest()) {
                bugHistoryList = new List<Bugs__History>();
                system.debug('Test 1');
                List<Case> caseList = [Select Bug__c From Case where Status != :STATUS_CLOSED AND Status != :STATUS_RESOLVED AND Status != :STATUS_COMPLETED AND Status != :System.Label.QS_CloseCase];
                if (caseList.Size() > 0) {
                    Id BugId = caseList[0].Bug__c;
                    system.debug('BugId: ' + BugId);
                    Bugs__History bh = new Bugs__History(ParentId = BugId, Field = 'Status__c');
                    bugHistoryList = new List<Bugs__History>();
                    bugHistoryList.add(bh);
                }
            } else {
                //Will never be covered by tests
                bugHistoryList = [
                        Select ParentId, Parent.Status__c, OldValue, NewValue, Id, Field, CreatedDate, CreatedById
                        From Bugs__History
                        Where Field = 'Status__c'
                        AND ParentId IN (
                                Select Bug__c
                                From Case
                                where Status != :STATUS_CLOSED AND Status != :STATUS_RESOLVED AND Status != :STATUS_COMPLETED
                                AND Status != :System.Label.QS_CloseCase
                        )
                ];
            }
            if (bugHistoryList != null && !bugHistoryList.isEmpty()) {
                showBugPopUp = true;
                Set<Id> bugHistorySet = new Set<Id>();
                for (Bugs__History bug : bugHistoryList) {
                    bugHistorySet.add(bug.ParentId);
                }
                if (bugHistorySet != null && !bugHistorySet.isEmpty()) {
                    bugCaseList = [
                            Select Id, CaseNumber, Status, CreatedDate, Bug__c, Bug__r.Name, Bug__r.Status__c, Bug__r.Bug_Id__c
                            From Case
                            where Bug__c IN :bugHistorySet
                            AND Bug__c != null AND Status != :STATUS_CLOSED AND Status != :STATUS_RESOLVED AND Status != :STATUS_COMPLETED AND Status != :System.Label.QS_CloseCase
                            order by CreatedDate ASC
                            LIMIT 100
                    ];
                    if (bugCaseList != null && !bugCaseList.isEmpty())
                        showBugPopUp = true; else
                            showBugPopUp = false;
                }
            } else {
                showBugPopUp = false;
            }
        } catch (Exception ex) {
            showBugPopUp = false;
        }
    }
    private void initSurvey() {

        //Uncomment for p2
        /*caseSurveyList = [Select Id, CaseNumber, Subject, Status, CreatedDate 
            From Case 
            Where Id NOT IN (Select Case_ID__c From Survey__c where Case_ID__c != null) 
            AND (Status = :STATUS_CLOSED OR Status = :STATUS_RESOLVED OR Status = :System.Label.QS_CloseCase OR Status = :STATUS_COMPLETED)
            AND (Survey_Status_Response__c = null OR Survey_Status_Response__c = '' OR (Survey_Status_Response__c = 'Later' AND (LastModifiedDate < :System.today() OR LastModifiedDate >= :System.today()+1)))
            Order By CreatedDate DESC, Survey_Status_Response__c ASC NULLS FIRST LIMIT 100]; // Might need to change the limit later on*/
    }
    public string GetOrganizationId() {
        String orgId = UserInfo.getOrganizationId().Mid(0, 15);
        return orgId;
    }

    public string GetLiveChatDeploymentId() {
        List<LiveChatDeployment> lcds = [select Id, MasterLabel from LiveChatDeployment where MasterLabel = 'Support Deployment' limit 1];

        if (lcds.size() > 0) {
            LiveChatDeployment lcd = lcds[0];
            return String.valueOf(lcd.Id).Mid(0, 15);
        } else
                return '';
    }
    public List<Case> getCaseList() {
        getContactId();

        system.debug ('~~~~~~~~ getCaseList.contactId-' + contactId);

        // caselist= [select Id, CaseNumber, AccountId, Subject, Priority, Status, CreatedDate, ContactId from Case where Status = 'Open' and ContactId :contactId ];
        if (caselist == null)
            caselist = [
                    SELECT Id, CaseNumber, Subject, Status, CreatedDate, ContactId
                    FROM Case
                    WHERE ContactId = :contactId AND (Status = :STATUS_PENDING_CONTACT_RESPONSE OR Status = :STATUS_SOLUTION_SUGGESTED)
                    ORDER BY CreatedDate DESC
                    LIMIT 100
            ];
        system.debug ('~~~~~~~~ getCaseList.caselist.size-' + caseList.size());
        return caselist;
    }
    private void getContactId() {
        //getUserId();
        if (null == contactId)
            contactId = [select contactId from User where Id = :Userid limit 1].ContactId;
    }
    public PageReference deleteSurveyStatus() {
        try {
            if (surveyIdChosen != null) {
                Case todel = [
                        select Id, Survey_Status_Response__c,Performance_Comments__c, Performance_Communication__c,Performance_Support_Entitlement__c,
                                Performance_Content_Utilization__c,Performance_Case_Quality__c,Performance_Troubleshooting__c
                        from Case
                        where Id = :surveyIdChosen
                        limit 1
                ];
                todel.Survey_Status_Response__c = 'Remove';
                if (todel.Performance_Comments__c == null || todel.Performance_Comments__c == '')
                    todel.Performance_Comments__c = 'Survey Removed';
                if (todel.Performance_Communication__c == null || todel.Performance_Communication__c == '')
                    todel.Performance_Communication__c = 'N/A';
                if (todel.Performance_Support_Entitlement__c == null || todel.Performance_Support_Entitlement__c == '')
                    todel.Performance_Support_Entitlement__c = 'N/A';
                if (todel.Performance_Content_Utilization__c == null || todel.Performance_Content_Utilization__c == '')
                    todel.Performance_Content_Utilization__c = 'N/A';
                if (todel.Performance_Case_Quality__c == null || todel.Performance_Case_Quality__c == '')
                    todel.Performance_Case_Quality__c = 'N/A';
                if (todel.Performance_Troubleshooting__c == null || todel.Performance_Troubleshooting__c == '')
                    todel.Performance_Troubleshooting__c = 'N/A';

                update todel;
            }
        } catch (Exception ex) {
            //errorMessage = ex.getMessage();
            System.debug(ex.getMessage());
        }

        return Page.QS_CaseListPage;
    }
    public PageReference setMyEmailOption() {
        return null;
    }

    public PageReference Enabledchat() {
        enabledChat = true;
        return Page.QS_QlikQonciergeService;
    }
    public boolean CheckIfQlikCustomer() {
        if (!isPartnerAccount && AccountId != null) {
            List<Entitlement> licenses = [select id from Entitlement where AccountId = :AccountId and Support_Provided_By_Text__c = 'Qlik'];
            //List<Entitlement> licenses = new List<Entitlement>();
            if (licenses.Size() > 0)
                return true; else
                    return false;
        } else
                return false;
    }

    public PageReference SaveBut() { return null;}

    public Attachment attachment {
        get {
            if(attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
    public String getUserprofilePic()
    {         
        User  lstuser = [select FullPhotoUrl from User where Id =: UserInfo.getUserId()];          
        String luser= lstuser.FullPhotoUrl;
        String[] parts = luser.split('/');
        Boolean isUploadedPhoto = parts[parts.size() - 2].length() == 15;
        
        if(isUploadedPhoto) {
            profileImageUrl=lstuser.FullPhotoUrl;
        }
        return  profileImageUrl;
    }
}