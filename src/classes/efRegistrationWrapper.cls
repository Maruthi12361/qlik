//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efRegistrationWrapper{
    private Registration__c r;
    private Contact c;
    private Payment_Details__c p;
    private String promoCode = '';
    
    public efRegistrationWrapper(Registration__c r, Contact c){
        this.r = r;
        this.c = c;
    }    
    public efRegistrationWrapper(Registration__c r){
        this.r = r;
    }    
    public efRegistrationWrapper(){
        r = new Registration__c();
    }    
    public Contact getContact(){
        return c;
    }    
    public void setContact(Contact c){
        this.c = c;
    }     
    public Registration__c getRegistration(){
        return r;
    }  
    public void setPaymentDetails(Payment_Details__c p){
        this.p = p;
    }     
    public Payment_Details__c getPaymentDetails(){
        return p;
    }     
    public String getEmergencyInfo(){
        if(efUtility.isNull(r.Emergency_Relationship__c))
                return efUtility.giveValue(r.Emergency_Contact__c); 
        else {
        	String ec = efUtility.giveValue(r.Emergency_Contact__c) + '(' + efUtility.giveValue(r.Emergency_Relationship__c) + ')';
        	return efUtility.getWrapString(ec,30);                
		}           
    }        
    public String getAttendeeSubType(){
        return efUtility.giveValue(r.Attendee_Subtype__c);
    }    
    public String getId(){
        return efUtility.giveValue(r.Id);
    }    
    public String getMobilePhone(){
        return efUtility.giveValue(r.Mobile_Phone__c);
    }    
    public void setMobilePhone(String mp){
        r.Mobile_Phone__c = mp;
    }    
    
    public String getInvoiceAddress(){
    	if(efUtility.isNull(r.Invoice_Address__c ))
    	{
    		if(efUtility.isNull(c.Account.BillingStreet))
    		{
    			return efUtility.giveValue(r.Invoice_Address__c);
    		}
    		else
    		{
    			String address = efUtility.giveValue(c.Account.BillingStreet) + '\r\n';
    			address += efUtility.giveValue(c.Account.BillingState) + '\r\n';
    			address += efUtility.giveValue(c.Account.BillingCity) + '\r\n';
    			address += efUtility.giveValue(c.Account.BillingCountry) + '\r\n';
    			address += efUtility.giveValue(c.Account.BillingPostalCode);
    			
    			return address;
    		}
    	}
    	else
    	{
        	return efUtility.giveValue(r.Invoice_Address__c);
    	}
    }    
    public void setInvoiceAddress(String ia){
        r.Invoice_Address__c = ia;
    }   
    public String getVatNo(){
        return efUtility.giveValue(r.Vat_No__c);
    }    
    public void setVatNo(String vn){
        r.Vat_No__c = vn;
    }   
            
    public String getTrackInterest(){
        return efUtility.giveValue(r.Track_Of_Interest_Lkup__c);
    }    
    public String getTrackInterestName(){
        if(!efUtility.isNull(r.Track_Of_Interest_Lkup__c)){
            Track__c track = [select Name from Track__c where Id =: r.Track_Of_Interest_Lkup__c];
            return efUtility.giveValue(track.Name);
        }
        return null;
    }    
    public void setTrackInterest(String ti){
        r.Track_Of_Interest_Lkup__c = ti;
    }    
    public String getWhyRegister(){
        return efUtility.giveValue(r.Why_register__c);
    }    
    public void setWhyRegister(String wr){
        r.Why_register__c = wr;
    }
    public String getEmpSessionTopics(){
        return efUtility.giveValue(r.Session_Topics__c);
    }    
    public void setEmpSessionTopics(String wr){
        r.Session_Topics__c = wr;
    }    
    public List<String> getWantToLearn(){
        if (efUtility.isNull(r.Want_to_learn__c))
            return new List<String>();
        else
            return r.Want_to_learn__c.split(';',0);
    }    
    public void setWantToLearn(List<String> wtl){
        String concatString = '';
        
        if (wtl != null && wtl.size() > 0){
            concatString = wtl[0];
            
            for (Integer i = 1; i < wtl.size(); i ++){
                concatString += ';' + wtl[i];
            }
        }
        
        r.Want_to_learn__c = concatString;
    }
    public String getDisplayableWantToLearn(){
        if (efUtility.isNull(r.Want_to_learn__c))
            return '';
        else
            return r.Want_to_learn__c.replaceAll('\\s*;\\s*',', ');
    }    
    public boolean getSeparateNameOnBadge(){
        return r.Separate_Name_On_Badge__c;
    }    
    public void setSeparateNameOnBadge(boolean separateNameOnBadge){
        r.Separate_Name_On_Badge__c = separateNameOnBadge;
    }    
    public String getEmergencyContact(){
        return efUtility.giveValue(r.Emergency_Contact__c);
    }    
    public void setEmergencyContact(String emergencyContact){
        r.Emergency_Contact__c = emergencyContact;
    }       
    public String getEmergencyPhone(){
        return efUtility.giveValue(r.Emergency_Phone__c);
    }    
    public void setEmergencyPhone(String emergencyPhone){
        r.Emergency_Phone__c = emergencyPhone;
    }    
    public String getEmergencyRelationship(){
        return efUtility.giveValue(r.Emergency_Relationship__c);
    }    
    public void setEmergencyRelationship(String emergencyRelationship){
        r.Emergency_Relationship__c = emergencyRelationship;
    }    
    public String getBadgeFirstName(){
        if(efUtility.isNull(r.Badge_First_Name__c))
            return efUtility.giveValue(c.FirstName);
        return efUtility.giveValue(r.Badge_First_Name__c);
    }    
    public void setBadgeFirstName(String badgeFirstName){
        r.Badge_First_Name__c = badgeFirstName;
    }    
    public String getBadgeLastName(){
        if(efUtility.isNull(r.Badge_Last_Name__c))
            return efUtility.giveValue(c.LastName);
        return efUtility.giveValue(r.Badge_Last_Name__c);
    }    
    public void setBadgeLastName(String badgeLastName){
        r.Badge_Last_Name__c = badgeLastName;
    }    
    public String getBadgeNameToDisplay(){
        return efUtility.getWrapString(getBadgeFirstName() + ' ' + getBadgeLastName(),30);
    }    
    public String getEmergencyPhoneToDisplay(){
        return efUtility.getWrapString(getEmergencyPhone(),30);
    }    
    public void setWizardPageSaved(String page){
        r.Wizard_Page_Saved__c = page;
    }    
    public String getWizardPageSaved(){
        return r.Wizard_Page_Saved__c;
    }    
    public String getStatus(){
        return r.Status__c;
    }    
    public void setStatus(String status){
        r.Status__c = status;
    }
    public String getSpecialRequirements(){
        if (r.Special_Requirement__c != null && r.Special_Requirement__c.length() > 255)
            return r.Special_Requirement__c.substring(0,255);
        else
            return r.Special_Requirement__c;
    }    
    public void setSpecialRequirements(String specialRequirements){
        r.Special_Requirement__c = specialRequirements;
    }    
    public Boolean getRequiresSpecialAssistance(){
        return r.Require_special_assistance__c;
    }    
    public void setRequiresSpecialAssistance(Boolean requiresSpecialAssistance){
        r.Require_special_assistance__c = requiresSpecialAssistance;
        if(requiresSpecialAssistance==false){
            setSpecialRequirements('');
        }
    }
    public String getDepartment(){
        return efUtility.giveValue(r.Employee_Department__c);
    }
    public void setDepartment(String Department){
            r.Employee_Department__c = Department;
    }    
    public DateTime getPaymentTimestamp(){
        return r.Payment_Timestamp__c;
    }    
    public void setPaymentTimestamp(DateTime pt){
        r.Payment_Timestamp__c = pt;
    }    
    public String getPrimaryRole(){
        return efUtility.giveValue(r.Primary_Role__c);
    }    
    public void setPrimaryRole(String PrimaryRole){
        r.Primary_Role__c = PrimaryRole;
    }    
    public String getCurrencyIsoCode(){
        return efUtility.giveValue(r.CurrencyIsoCode);
    }    
    public void setCurrencyIsoCode(String CurrencyIsoCodeVal)   {
        r.CurrencyIsoCode = CurrencyIsoCodeVal;
    }        
    public String getRegId(){       
        return r.Reg_Number__c;
    }    
    public void setRegType(String regType){    
        r.Type__c = regType;    
    }    
    public String getRegType(){
        String mapAttendeeType='';
            
        if(r.Type__c=='Attendee'){
            mapAttendeeType='Standard';
        }
        if(r.Type__c=='Executive Summit'){
            mapAttendeeType='Standard';
        }
        if(r.Type__c=='PR/IR/AR'){
            mapAttendeeType='Press';
        }
        if(r.Type__c=='Employee'){
            mapAttendeeType='Employee';
        }
        if(r.Type__c=='Sponsor'){
            mapAttendeeType='Standard';
        }
        return mapAttendeeType;
    }       
    public void setPromoCode(String promo){
        r.PromoCode__c = promo;
    }    
    public String getPromoCode(){
        return r.PromoCode__c;
    }
    public String getName(){
        return r.Name;
    }
    public Boolean getExtendedStay(){
        return r.Has_Extended_Stay_Addon__c;
    }    
    public void setExtendedStay(Boolean extendedStay){
        r.Has_Extended_Stay_Addon__c = extendedStay;
    }    
    public String getExtendedStayArrival(){    	
        return efUtility.giveValue(r.Extended_Stay_Start_Date__c);
    }    
    public void setExtendedStayArrival(String arrival){
        r.Extended_Stay_Start_Date__c = arrival;
    }
    public String getExtendedStayDeparture(){
        return efUtility.giveValue(r.Extended_Stay_End_Date__c);
    }    
    public void setExtendedStayDeparture(String departure){
        r.Extended_Stay_End_Date__c = departure;
    }
    public boolean getSpouseAddition()
    {
        return r.Spouse_Added__c;
    }    
    public void setSpouseAddition(boolean spouseAddition)
    {
        r.Spouse_Added__c = spouseAddition;
    }
    public String getSpouseFirstName()   {
        return efUtility.giveValue(r.Spouse_First_Name__c);
    }   
    public void setSpouseFirstName(String spouseFirstName)    {
        r.Spouse_First_Name__c = spouseFirstName;
    }    
    public String getSpouseLastName()   {
        return efUtility.giveValue(r.Spouse_Last_Name__c);
    }    
    public void setSpouseLastName(String spouseLastName)    {
        r.Spouse_Last_Name__c = spouseLastName;
    }    
    
    public Boolean getShareContactInfo()
    {
    	return r.Agree_to_share_info__c;
    }
    
    public void setShareContactInfo(Boolean ShareInfo)
    {
    	r.Agree_to_share_info__c = ShareInfo;    
    }
}