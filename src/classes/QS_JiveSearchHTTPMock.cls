/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
public class QS_JiveSearchHTTPMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public QS_JiveSearchHTTPMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }
    public static QS_JiveSearchHTTPMock CreateHTTPMock(integer responseCode)
    {
        system.debug('CreateHTTPMock start');
        StaticResource sr = [select Body from StaticResource where Name = 'QS_JiveResults'];
        string body = sr.Body.toString();
        Map<String, String> responseHeaders = new Map<String, String>();
        QS_JiveSearchHTTPMock mock = new QS_JiveSearchHTTPMock(responseCode, 'Alles gut', body,responseHeaders);
        system.debug('Create HTTP Mock end');
        return mock;
    }

}