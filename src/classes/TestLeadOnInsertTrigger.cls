/*****************************************************************************************************************
 Change Log:
 20121026   TJG Quick CR 6484 - Currently when new Partner Opp Reg Leads are created, 
            no value is being recorded in the Lead Source field. 
            This value should be 'PART - Partner' for every new Lead created
            
 20120905   TJG Ensure Web Activity is Qonnect Markting Services and Record type is 'Parter Opp Reg'
            and Lead source is 'Parter - Partner'

 20120823   TJG PO179 https://projects.qliktech.com/sites/projects/MarketingasaServiceforPartners/default.aspx
            Test LeadOnInsertTrigger2 that copies partner OwnerId from QMS_Original_Partner_OwnerId__c to OwnerId

 20140701   MHG Changed QMS_Original_Partner_OwnerId__c from Andy May to David Reeve due to inactive user

 20180323   CRW CHG0033482: added test method test_LeadCallMarketoCoverage()
 
******************************************************************************************************************/
@isTest
private class TestLeadOnInsertTrigger {
    static final String WA_SOURCE = 'Qonnect Marketing Services';
    static final String LEADSOURCE = 'PART - Partner';
    static final String RT_PARTEROPPREG = '012D0000000JsWAIA0';

    static testMethod void test_LeadOnInsertTrigger()
    {               
        Lead lead0 = new Lead(  LastName='LastTest', 
                            FirstName='FirstTest', 
                            Country='Sweden',
                            Email='asd@asd.com',
                            Web_Activity_Source__c = WA_SOURCE,
                            LeadSource = LEADSOURCE,
                            RecordTypeId = RT_PARTEROPPREG,
                            QMS_Original_Partner_OwnerId__c = '00520000000zCfx', 
                            Company ='Testing');
                            
        Lead lead1 = new Lead(  LastName='LastTest1', 
                            FirstName='FirstTest1', 
                            Country='Sweden',
                            Email='asd1@asd1.com',
                            LeadSource = LEADSOURCE,
                            RecordTypeId = RT_PARTEROPPREG,
                            QMS_Original_Partner_OwnerId__c = '00520000000zCfx', 
                            Company ='Testing1');
  
        Lead lead2 = new Lead(  LastName='LastTest2', 
                            FirstName='FirstTest2', 
                            Country='Sweden',
                            Email='asd2@asd2.com',
                            RecordTypeId = RT_PARTEROPPREG,
                            Company ='Testing2');
        insert lead0;
        Semaphores.LeadTriggerHandlerBeforeInsert = false;
        insert lead1;
        Semaphores.LeadTriggerHandlerBeforeInsert = false;
        insert lead2;
        
        lead0 = [select Id, OwnerId, QMS_Original_Partner_OwnerId__c from Lead where Id = :lead0.Id];
        lead1 = [select Id, OwnerId, QMS_Original_Partner_OwnerId__c from Lead where Id = :lead1.Id];
        lead2 = [select Id, LeadSource from Lead where Id = :lead2.Id];
                
        test.startTest();

        System.assertEquals(lead0.OwnerId, lead0.QMS_Original_Partner_OwnerId__c);
        System.assertNotEquals(lead1.OwnerId, lead1.QMS_Original_Partner_OwnerId__c);
        System.assertEquals(lead2.LeadSource, LEADSOURCE);
        
        test.stopTest();        
    }
    static testMethod void test_LeadCallMarketoCoverage()
    {
        Lead lead0 = new Lead(  LastName='LastTest', 
                            FirstName='FirstTest', 
                            Country='Sweden',
                            Email='asd@asd.com',
                            Web_Activity_Source__c = WA_SOURCE,
                            LeadSource = LEADSOURCE,
                            RecordTypeId = RT_PARTEROPPREG,
                            QMS_Original_Partner_OwnerId__c = '00520000000zCfx', 
                            Company ='Testing');
        insert lead0;
        
        lead0.Trigger_Webservice_Shadow__c = True;
        lead0.Trigger_Webservice__c = True;
        Semaphores.LeadTriggerHandlerAfterupdate = false;
        Semaphores.LeadTriggerHandlerbeforeupdate = false;
        update lead0;
    }
}