/******************************************************************** 
* Class:OppVoucherManagementCreationHandler
* 
* This is handler class for trigger Opportunity_VoucherManagementCreation
* Description: trigger that will fire on opportunity, will clear values after a clone on Voucher Status on Opp. 
* Also will check if the opp has any Opportunity Line Items with Product Family Education, 
* they are not deleted and voucher code has not been set, then will fire a method on QTVoucher.cls 
* do a future call to QTVoucher web service 
* 
* Log History: 
* 2016-05-20 - Roman@4front - Migrated from Opportunity_VoucherManagementCreation.trigger.
* 2017-06-22 MTM  QCW-2711 remove sharing option
* 2018-03-27 AIN IT-99 Added calls to deactivate and reactivate vouchers
****************************************************************************/

public class OppVoucherManagementCreationHandler {
    public OppVoucherManagementCreationHandler() {

    }

    public static void handle(List<Opportunity> triggerNew, List<Opportunity> triggerOld, Boolean isInsert, Boolean isUpdate) {

        System.Debug('-----Start Opportunity_VoucherManagementCreation----');

        //Check if Opportunity_VoucherManagementCreation trigger has already runned
	     /*if (Semaphores.Opportunity_VoucherManagementCreationHasRun) 
	        { 
	            System.Debug('-----Semaphore has run----'); 
	            return; 
	        } 
	        else 
	        { 
	            System.Debug('-----Set Semaphore to true to stop double run----'); 
	            Semaphores.Opportunity_VoucherManagementCreationHasRun = true; 
	        }*/

        //Set of opp id's tha contain Opportunity Line Items with Product Family Education and Voucher_Code__c not set.
        Set<Id> OppIDsWithEducationOLI = new Set<Id>();
        Set<Id> OppIDsForCancellation = new Set<Id>();
        Set<Id> OppIDsForReactivation = new Set<Id>();
        Set<Id> OppIDsForActivation = new Set<Id>();
        List<Voucher_Management__c> InactiveVouchersList = new List<Voucher_Management__c>();
        Opportunity NewOpp, OldOpp;
        List<String> OppIds = new List<String>();

        //If is insert, make sure that all Voucher fields on opp are blank out, this is in case an opp is cloned
        if (isInsert) {
            System.Debug('-----Insert----');

            for (Integer i = 0; i < triggerNew.size(); i++) {
                NewOpp = triggerNew[i];

                NewOpp.VoucherStatus__c = '';
                NewOpp.Voucher_Mail_Triggered__c = null;
                NewOpp.VoucherEmailTrigger__c = false;
            }

        }

        // IF it is update, check if there are vouchers that needs to be created and activated
        if (isUpdate) {
            System.Debug('-----Update----');
            //Check if Opportunity_VoucherManagementCreation trigger has already executed.
            if (Semaphores.Opportunity_VoucherManagementCreationHasRun) {
                System.Debug('-----Semaphore has run----');
                return;
            } else {
                System.Debug('-----Set Semaphore to true to stop double run----');
                Semaphores.Opportunity_VoucherManagementCreationHasRun = true; // 20140507 SAN
            }

            Map<Id, List<OpportunityLineItem>> OppIdEducationOLIMap = new Map<Id, List<OpportunityLineItem>>();
            List<OpportunityLineItem> OLIList = new List<OpportunityLineItem>();
            OLIList = [
                    select Id, Account_License__c, Account_License_RO__c, Application_Name__c,
                            AppStore_Registration__c, CreatedById, CreatedDate, CurrencyIsoCode,
                            Description, Discount__c, Discounted_Price__c, Eval_Product__c,
                            HasQuantitySchedule, HasRevenueSchedule, HasSchedule, IsDeleted,
                            LastModifiedById, LastModifiedDate, List_after_Partner_Margin__c,
                            ListPrice, Name__c, Net_Before_Partner_Margin__c, Net_Price__c,
                            NonProfit_Registration__c, NS_Item_Price__c, OpportunityId,
                            Oppty_Product__c, Part_Number__c, Partner_Margin__c, Partner_Margin_RO__c,
                            PricebookEntryId, Product_Family__c, pse__Added_To_Project__c,
                            pse__IsServicesProductLine__c, Quantity,
                            Registration__c, ServiceDate, SortOrder, Status__c, SysAdmin_Field__c,
                            SystemModstamp, Total_List_Value__c, Total_Net_before_Partner_Margin__c,
                            TotalPrice, UnitPrice, Voucher_Code__c
                    from OpportunityLineItem
                    Where isDeleted = false
                    and Product_Family__c = 'Education'
                    and voucher_code__c = null
                    and Application_Name__c != null
                    and OpportunityId in:triggerNew
            ];

            QTCustomSettings__c settings = QTCustomSettings__c.getValues('Default');

            //For each OLI we set it in the Opportunity Id Map with the list of Opp line items belonging to the same opp.
            List<OpportunityLineItem> listaux;
            for (OpportunityLineItem oli : OLIList) {
                //If Application Name in OppLineItem is not amongst the permissible applications then skip this OLI
                system.debug('OppVoucherManagementCreationHandler.oli.Application_Name__c: ' + oli.Application_Name__c);
                system.debug('OppVoucherManagementCreationHandler.settings.OLIAppNamesToCreateVouchers__c: ' + settings.OLIAppNamesToCreateVouchers__c);
                if (oli.Application_Name__c != null && oli.Application_Name__c != '' && settings.OLIAppNamesToCreateVouchers__c != null && !settings.OLIAppNamesToCreateVouchers__c.contains(oli.Application_Name__c))
                    continue;

                if (OppIdEducationOLIMap.containsKey(oli.OpportunityId)) {
                    listaux = OppIdEducationOLIMap.get(oli.OpportunityId);
                    listaux.add(oli);
                    OppIdEducationOLIMap.put(oli.OpportunityId, listaux);
                } else {
                    OppIdEducationOLIMap.put(oli.OpportunityId, new List<OpportunityLineItem>());
                    listaux = OppIdEducationOLIMap.get(oli.OpportunityId);
                    listaux.add(oli);
                }
            }


            //Foreach opp with education OLIs, not deleted and with voucher not attached to them check if the opp is closed won to create vouchers
            for (Integer i = 0; i < triggerNew.size(); i++) {
                NewOpp = triggerNew[i];
                OldOpp = triggerOld[i];

                System.Debug('New op.id(' + NewOpp.Id + ')');
                System.Debug('Old op.id(' + OldOpp.Id + ')');

                //Calculate isClosedWon using formula
                //Boolean NewOppIsClosedWon = newOpp.Closed_date_for_conga__c != null;
                //Boolean OldOppIsClosedWon = oldOpp.Closed_date_for_conga__c != null;

                Boolean NewOppIsClosedWon = newOpp.StageName.toLowerCase().contains('closed won');
                Boolean OldOppIsClosedWon = oldOpp.StageName.toLowerCase().contains('closed won');

                Boolean NewOppIsClosedLost = newOpp.StageName.toLowerCase().contains('closed lost');
                Boolean OldOppIsClosedLost = oldOpp.StageName.toLowerCase().contains('closed lost');

                //The trigger should fired only if opp is Closed Won and it contains Opportunity Line Items type education

                System.Debug('New Stage(' + NewOpp.StageName + ')');
                System.Debug('Old Stage(' + OldOpp.StageName + ')');
                System.Debug('newOpp.Closed_date_for_conga__c=' + newOpp.Closed_date_for_conga__c);
                System.Debug('OldOpp.VoucherStatus__c (' + OldOpp.VoucherStatus__c + ')');
                System.Debug('NewOpp.VoucherStatus__c (' + NewOpp.VoucherStatus__c + ')');
                System.Debug('NewOppIsClosedWon =' + (NewOppIsClosedWon));
                System.Debug('OldOppIsClosedWon =' + (OldOppIsClosedWon));
                System.Debug('Create vouchers???' + ((NewOppIsClosedWon && !OldOppIsClosedWon) &&
                        (OldOpp.VoucherStatus__c == '' || OldOpp.VoucherStatus__c == null)));
                System.Debug('Retrigger voucher creation if there was an issue and administrator reset voucher status to empty');
                System.Debug('Retrigger? ' + (NewOppIsClosedWon
                        && ((NewOpp.VoucherStatus__c == '' || NewOpp.VoucherStatus__c == null)
                        //&& (OldOpp.VoucherStatus__c == 'Vouchers Issue')
                )));
                //If Opp Stage contains Closed won and Voucher Status is empty
                //Create vouchers for OLI with Product Family education and no voucher associated
                //Or if there was an issue and the stage is closed won and voucher status is reset to blank, retrigger voucher creation

                //IT-99, Opp is lost, we deactivate vouchers
                if(NewOppIsClosedLost && OldOppIsClosedWon && NewOpp.VoucherStatus__c == 'Vouchers Created and Active'){
                    System.Debug('Opp ID (' + NewOpp.Id + ') will be cancelled');
                    //Add opp to Set of Opp so the web service can cancel them
                    OppIDsForCancellation.add(NewOpp.Id);
                    NewOpp.VoucherStatus__c = 'Vouchers Pending';
                }
                //IT-99 if opp goes from closed lost to closed won again, reactivate vouchers
                else if(NewOppIsClosedWon && OldOppIsClosedLost && NewOpp.VoucherStatus__c == 'Cancelled'){
                    System.Debug('Opp ID (' + NewOpp.Id + ') will be reactivated');
                    //Add opp to Set of Opp so the web service can reactivate them
                    OppIDsForReactivation.add(NewOpp.Id);
                    NewOpp.VoucherStatus__c = 'Vouchers Pending';
                }
                else if(NewOppIsClosedWon && (NewOpp.VoucherStatus__c == '' || NewOpp.VoucherStatus__c == null)) {
                
                //unnecessary redundancy, oldopp voucher status is irrelavant, replaced by above code AIN
                /* 
                if (
                        (NewOppIsClosedWon
                                && (NewOpp.VoucherStatus__c == '' || NewOpp.VoucherStatus__c == null)
                        ) ||
                                (
                                        NewOppIsClosedWon
                                                && ((NewOpp.VoucherStatus__c == '' || NewOpp.VoucherStatus__c == null)
                                                && (OldOpp.VoucherStatus__c == 'Vouchers Issue')))
                        ) {*/
                    //check if they have Education OLIs
                    System.Debug('Opp ID (' + NewOpp.Id + ') Checking if contains OLIs');
                    if (OppIdEducationOLIMap.containsKey(NewOpp.Id)) {
                        System.Debug('OppIdEducationOLIMap OLI list size (' + OppIdEducationOLIMap.get(NewOpp.Id).size() + ')');
                        //Add opp to Set of Opp so the web service can update them and create the vouchers
                        OppIDsWithEducationOLI.add(NewOpp.Id);
                        NewOpp.VoucherStatus__c = 'Vouchers Pending';
                    } else {
                        System.Debug('Opp (' + NewOpp.Id + ') Does not contain Education Opp Line Items associated with vouchers');
                        NewOpp.VoucherStatus__c = 'No Vouchers';
                    }
                }
	            
	/*           System.Debug('Issue, retry to activate vouchers....'); 
	             if(NewOpp.IsClosedWon__c > 0 && NewOpp.VoucherStatus__c == 'Vouchers Created' && OldOpp.VoucherStatus__c == 'Vouchers Issue') 
	             { 
	             //Add to list of opp to try activation 
	             OppIDsForActivation.add(NewOpp.Id); 
	             } 
	*/
            }

            //for qlikbuy II create and activate vouchers straight away
            if (OppIDsWithEducationOLI.size() > 0) {
                for (Id oppId : OppIDsWithEducationOLI) {
                    OppIds.add('' + oppId);
                }
                System.Debug('invoke CallCreateAndActivateVouchersFromOpportunities....');
                QTVoucher.CallCreateAndActivateVouchersFromOpportunities(OppIds);
                Semaphores.Opportunity_VoucherManagementCreationHasRun = true;
            }
            if (OppIDsForCancellation.size() > 0) {
                for (Id oppId : OppIDsForCancellation) {
                    OppIds.add('' + oppId);
                }
                System.Debug('invoke CallDeactivateVouchersOnOpportunities....');
                sendMails('Cancellation', OppIDsForCancellation);
                QTVoucher.CallDeactivateVouchersOnOpportunities(OppIds);
            }
            if (OppIDsForReactivation.size() > 0) {
                for (Id oppId : OppIDsForReactivation) {
                    OppIds.add('' + oppId);
                }
                System.Debug('invoke CallReactivateVouchersOnOpportunities....');
                sendMails('Reactivation', OppIDsForReactivation);
                QTVoucher.CallReactivateVouchersOnOpportunities(OppIds);
            }
	/*      
	         //Foreach opp with Education Olis call web method to create vouchers 
	         if (OppIDsWithEducationOLI.size() > 0) 
	         { 
	             for (Id oppId : OppIDsWithEducationOLI) 
	             { 
	                OppIds.add(''+oppId); 
	             } 
	             System.Debug('CallCreateVouchersFromOpportunities....');            
	             QTVoucher.CallCreateVouchersFromOpportunities(OppIds); 
	            
	         } 
	        
	         //Foreach opp with Opp with Education Oli ready for activation Call WEBSERVICE Activation Call 
	         if (OppIDsForActivation.size() > 0) 
	         { 
	             for (Id oppId : OppIDsForActivation) 
	             { 
	                 OppIds.add(''+oppId); 
	             } 
	             System.Debug('CallActivateVouchersFromOpportunities....');              
	             QTVoucher.CallActivateVouchersFromOpportunities(OppIds); 
	            
	         } 
	*/
            //Clear Map of Opp with Education OLIs
            OppIdEducationOLIMap.clear();
        }
    }
    //Sends an email when vouchers are deactivated or activated.
    @testVisible
    private static void sendMails(string type, Set<Id> oppIds) {
        //Find out who to send the email to (there is a list of addresses in a QTCustomSetting)
        //initailise a default email address based on Live or sandbox Org
        String soEmailDefault = (UserInfo.getOrganizationId().startsWith('00D20000000IGPX') ? 'ain@qlik.com' : 'ain@qlik.com.sandbox');
        QTCustomSettings__c qtcs = QTCustomSettings__c.getValues('Default');
        String nso = qtcs != null ? qtcs.VoucherDeactivateReactivateEmail__c : soEmailDefault;
        // Strings to hold the email addresses to which you are sending the email.
        if (nso != null) {  //Can't send an email if we have no addresses to send to
            list<string> toAddresses = nso.split(',');
            System.Debug(LoggingLevel.DEBUG, '[OppVoucherManagementCreationHandler] toAddresses = ' + toAddresses);

            boolean orgAllowedToSendEmails = true;
            if ( !Test.isRunningTest() ) {
                try {
                    Messaging.reserveSingleEmailCapacity(toAddresses.size()); //This changes depending on the number of recipients in our Custom Setting
                } catch (System.NoAccessException ex) {
                    system.debug(LoggingLevel.ERROR, '[OppVoucherManagementCreationHandler] Org is not allowed to send email.');
                    orgAllowedToSendEmails = false;
                }
            } else {
                orgAllowedToSendEmails = false;
            }
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            

            //set the FROM address by setting the org-wide address to use
            string Qlik_noreply_email_address = 'noreply@qlikview.com'; //set a default value to use if the custom settings fail  
            if(QTCustomSettings__c.getInstance('Default') != null) {
                Qlik_noreply_email_address = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
                System.Debug('SendEmail: Qlik_noreply_email_address = ' + Qlik_noreply_email_address);                
            }
            mail.setReplyTo('noreply@qlik.com');
            OrgwideEmailAddress[] orgwideaddress = [select id from orgwideEmailAddress where displayname = :Qlik_noreply_email_address Limit 1]; 
            //OrgwideEmailAddress[] orgwideaddress = [select id from orgwideEmailAddress where displayname = :System.Label.Qlik_noreply_email_address]; 
            if(orgwideaddress.Size() > 0){
                mail.setOrgWideEmailAddressId(orgwideaddress[0].id); 
            }

            if(type == 'Reactivation') {
                mail.setSubject('Opportunities being reactivated');
            }
            else {
                mail.setSubject('Opportunities being cancelled');
            }
            
            mail.setBccSender(false);
            mail.setUseSignature(false);
            string oppIdsString = '';
            // Specify the text content of the email.
            String soErrors = '';
            for (Id oppId : oppIds) {
                oppIdsString += oppId + ',' ;
            }

            if (oppIdsString != '') {
                oppIdsString = oppIdsString.removeEnd(',');
                if(type == 'Reactivation')
                    mail.setPlainTextBody('The following opportunities are being reactivated, this might take a few minutes, Voucher Status will change to "Vouchers Created and Active" when complete\n\r\n\r' + oppIdsString);
                else
                    mail.setPlainTextBody('The following opportunities are being cancelled, this might take a few minutes, Voucher Status will change to "Cancelled" when complete\n\r\n\r' + oppIdsString);
                if (orgAllowedToSendEmails) {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                            mail
                    });
                }
            }
        }
    }
}