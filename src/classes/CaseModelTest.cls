/********************************************************
* 2015-10-28	RDZ		Adding unit test for CaseModel, 
						other test might be on TestCaseServices.
*********************************************************/


@isTest
private class CaseModelTest {
	
	@isTest static void getAccountRelatedToCaseTest() {
		Case c = createMockCaseWithAccount();
		CaseModel cm = new CaseModel(c);
		Account a = cm.getAccountRelatedToCase();
		System.assertEquals(a.Id, c.AccountId);
	}
	
	@isTest static void getAccountLicensesRelatedToAccountTest() {
		Case c = createMockCaseWithAccount();
		CaseModel cm = new CaseModel(c);
		List<Account_License__c> a = cm.getAccountLicensesRelatedToAccount();
		//TODO System.assertEquals(a.Id, c.AccountId);
	}

	@isTest static void recordNullTest() {
		Case c = null;
		CaseModel cm = new CaseModel(c);
		//check that even case is null, case model creates a new record but does not insert it.
		System.assert(cm.record != null);
		System.assert(cm.record.Id == null);
	}

	@isTest static void hasValidIdTest()
	{
		Case c = null;
		CaseModel cm = new CaseModel(c);
		System.assert(cm.hasValidId == false, 'If case is null CaseModel hasValidId should return false');
	}

	@isTest static Case createMockCaseWithAccount()
	{
		Account a = QTTestUtils.createMockAccount('TestAccount', QTTestUtils.createMockSystemAdministrator(), true);
		Case c = new Case(AccountId= a.Id);
		insert c;
		return [Select Id, AccountId From Case where Id =: c.Id];
	}

	@isTest static void userHasPermissionSystemAdministratorShouldHaveCasePermissionTest()
	{
		User u = QTTestUtils.createMockSystemAdministrator();
		Case c = null;
		CaseModel cm = null;
		u.Access_Create_Problem_Case__c=true;
		update u;
		System.runAs(u)
		{
			c = createMockCaseWithAccount();			
			cm = new CaseModel(c);		
			System.assert(cm.userHasPermission);
		}
	}

	@isTest static void userHasPermissionProfileWithNoCasePermissionTest()
	{
		User archivedUser = QTTestUtils.createMockUserForProfile('Archive User');
		User adminUser = QTTestUtils.createMockSystemAdministrator();
		Case c = null;
		CaseModel cm = null;
		System.runAs(adminUser)
		{
			c = createMockCaseWithAccount();	
			cm = new CaseModel(c);		
		}

		System.runAs(archivedUser)
		{
			System.assert(!cm.userHasPermission);
		}
	}
	
}