global class Z_SelectBillingAccountComponent implements zqu.SelectBillingAccountComponentOptions.ICustomizeBillingAccountPlugin { 
	global zqu.JsRemoteController.BillingAccountObjects getAvailableBillingAccounts(zqu.JsRemoteController.BillingAccountObjects accountObjs){
		return accountObjs;
	}
	global zqu.JSRemoteController.QuoteTypeObjects getAvailableQuoteTypes(zqu.JSRemoteController.QuoteTypeObjects quoteTypeObjs){
		//remove cancel quote option
		quoteTypeObjs.quoteTypes.remove(3);

		return quoteTypeObjs; 
	}
	global zqu.JSRemoteController.SubscriptionObjects getAvailableSubscriptions(zqu.JSRemoteController.SubscriptionObjects subscriptionObjs){
		
		String invoiceOwnerAccount;
		Set<String> billingAccountIdSet = new Set<String>();
		Set<String> sub_id_with_sell_through_partner_invoice_owner_set = new Set<String>();

		Opportunity opp = [SELECT Id, Account.Id, Revenue_Type__c, Sell_Through_Partner__c FROM Opportunity WHERE Id =: subscriptionObjs.opportunityId];

		if(opp.Revenue_Type__c.equals('Reseller') || opp.Revenue_Type__c.equals('Fulfillment') || opp.Revenue_Type__c.equals('Distributor')){
			invoiceOwnerAccount = opp.Sell_Through_Partner__c;
		}else if(opp.Revenue_Type__c.equals('Direct')){
			invoiceOwnerAccount = opp.Account.Id;
		}

		List<Zuora__CustomerAccount__c> billingAccountList = [SELECT Id FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c =: invoiceOwnerAccount];
		for(Zuora__CustomerAccount__c b_acc : billingAccountList){
			billingAccountIdSet.add(b_acc.Id);
		}

		List<Zuora__Subscription__c> subscriptionList = [SELECT Id, Zuora__External_Id__c FROM Zuora__Subscription__c WHERE Zuora__InvoiceOwner__c =: billingAccountList];

		for(Zuora__Subscription__c sub : subscriptionList){
			sub_id_with_sell_through_partner_invoice_owner_set.add(sub.Zuora__External_Id__c);
		}

		for(Integer i = 0; i < subscriptionObjs.subscriptions.dataObjects.size(); i++){
			
			Map<String, Object> subMap = subscriptionObjs.subscriptions.dataObjects.get(i);
			String subId = (String)subMap.get('Id');
			
			if( !sub_id_with_sell_through_partner_invoice_owner_set.contains(subId)){
				subscriptionObjs.subscriptions.dataObjects.remove(i);
				i--;
			}
			
		}
		

		return subscriptionObjs;
	}
}