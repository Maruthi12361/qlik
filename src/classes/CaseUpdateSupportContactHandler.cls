/*
 File Name: CaseUpdateSupportContactHandler
 Moved from trigger CaseUpdateSupportContact on Case(after update, after delete)

 Updates a contact's Support Contact field when a Case of type Incident is opened
 Updates a contact's Qoncierge Contact field when a Case of type Service Request is opened
 2018-01-01 Viktor@4Front QCW-4612, QCW-4761, QCW-5050 Trigger consolidation.

*/

public with sharing class CaseUpdateSupportContactHandler {

    public static void handleCaseUpdateSupportContact(List<Case> newCases, List<Case> oldCases) {
        Map<Id, String> ctIDs1 = new Map<Id, String>(); // Incident type
        Map<Id, String> ctIDs2 = new Map<Id, String>(); // Service Request type
        List<Case> lstTmp = (oldCases != null) ? oldCases : newCases;
        List<RecordType> lstRec = [
                SELECT Id
                FROM RecordType
                WHERE SobjectType = 'Case'
                AND IsActive = true
                AND Name IN('QlikTech Master Support Record Type',
                        'QT Support Partner Portal Record Type',
                        'QT Support Customer Portal Record Type')
        ];

        for (Case c : lstTmp) {
// Select all cases which are opened by this contact
            List<Case> lstC = [
                    SELECT Id, Type
                    FROM Case
                    WHERE
                    ContactId = :c.ContactId AND RecordTypeId IN :lstRec
            ];

            Boolean bYes1 = false; // Check if any one of them is of type Incident
            for (Case x : lstC) {
                if (x.Type == 'Incident') {
                    bYes1 = true;
                    break;
                }
            }
            // Atleast one of the cases is of type Incident
            if (bYes1 == true) {
                ctIDs1.put(c.ContactId, 'Yes');
            } else {
                ctIDs1.put(c.ContactId, 'No');
            }

            Boolean bYes2 = false; // Do a similar check if any one of them is of type Service Request
            for (Case x : lstC) {
                if (x.Type == 'Service Request') {
                    bYes2 = true;
                    break;
                }
            }
            // Atleast one of the cases is of type Service Request
            if (bYes2 == true) {
                ctIDs2.put(c.ContactId, 'Yes');
            } else {
                ctIDs2.put(c.ContactId, 'No');
            }
        }

        if (ctIDs1.size() > 0) {
            List<Contact> lstCt1 = [
                    SELECT Id, Support_Contact__c
                    FROM Contact
                    WHERE Id In :ctIDs1.keySet()
            ];
            for (Integer i = 0; i < lstCt1.size(); i++) {
                lstCt1[i].Support_Contact__c = ctIDs1.get(lstCt1[i].Id);
            }

            if (lstCt1.size() > 0) {
                try{
                update lstCt1;
                }
                catch(Exception e){
                system.debug(e);
                }
            }
        }

        if (ctIDs2.size() > 0) {
            List<Contact> lstCt2 = [
                    SELECT Id, Qoncierge_Contact__c
                    FROM Contact
                    WHERE Id in :ctIDs2.keySet()
            ];
            for (Integer i = 0; i < lstCt2.size(); i++) {
                lstCt2[i].Qoncierge_Contact__c = ctIDs2.get(lstCt2[i].Id);
            }

            if (lstCt2.size() > 0) {
                try{
                update lstCt2;
                }
                catch(Exception e){
                system.debug(e);
                }
            }
        }
    }

}