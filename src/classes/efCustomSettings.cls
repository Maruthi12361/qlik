//*********************************************************/
// Author: Mark Cane&
// Creation date: 18/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efCustomSettings{
	public static final String CONFIGURATION = 'Configuration';
	public static final String SESSION_MAP = 'Session';
	public static final String MAIL = 'Mail';
	public static final String EMPLOYEE = 'Employee';
	
	public static Map<String, Map<String,String>> returnMap = new Map<String, Map<String,String>>(); 
	
	public static Map<String,String> getValues(String customType){
		if(returnMap.get(customType)!=null){
			return returnMap.get(customType);
		} else{
			Map<String,String> retValue = new Map<String,String>();
					
			for(Custom_Settings__c cs : [select Name,Name__c,Value__c from Custom_Settings__c where Name=:customType]){
				retValue.put(cs.Name__c,cs.Value__c);
			}		
			returnMap.put(customType,retValue);
			return retValue;		
		}
	}	
}