/*

    HerokuULCLevelBatchable 

    These are batchable class that run to update active license for contacts everyday for the ULC Heroku Application

    SAN     2016-01-06

 */
global class HerokuULCLevelBatchable implements Database.Batchable<sObject>{
    
    public HerokuULCLevelBatchable (){
        //& Retrieve configuration settings at the process level.

    }

    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([ SELECT Account__c FROM Account_License__c WHERE Time_Limit__c >= TODAY ]);        
    }
        
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        List<Account_License__c> changedLicenses = (List<Account_License__c>)scope;

        //use set so only keep unique accountids
        Set<Id> accountIds = new Set<Id>();                 
        for (Account_License__c al : changedLicenses) accountIds.add(al.Account__c);

        Set<Id> licenseToContactsId = new Set<Id>(); // only update unique contacts
        List<Contact> licenseToContacts = new List<Contact>();        
        //& contact that has valid licenses
        for (Contact e : [SELECT Id, ULC_ActiveLicense__c FROM Contact 
                              WHERE AccountId IN :accountIds]){
            licenseToContactsId.add(e.Id);
        }

        for (Contact f : [SELECT Id, ULC_ActiveLicense__c FROM Contact 
                              WHERE Id IN :licenseToContactsId]){
            f.ULC_ActiveLicense__c = true;
            licenseToContacts.add(f);
        }
        if (licenseToContacts.size() > 0) update licenseToContacts;


    }
    
    global void finish(Database.BatchableContext BC){}
}