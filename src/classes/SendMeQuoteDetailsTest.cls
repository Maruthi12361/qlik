@IsTest
public class SendMeQuoteDetailsTest {
	
    public static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
    public static Schema.DescribeSObjectResult accDescribe= gd.get('Account').getDescribe();
    public static Schema.DescribeSObjectResult oppDescribe= gd.get('Opportunity').getDescribe();
    
    public static String END_USER_ACCOUNT_RT = accDescribe.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
    public static String PARTNER_ACCOUNT_RT =  accDescribe.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
    public static String SALES_QCCS_OPP_RT = oppDescribe.getRecordTypeInfosByName().get('Qlikbuy CCS Standard').getRecordTypeId();
    
    @IsTest
    static void createTestData() {
        
        /* Set up partner user - START*/
        Id p = [SELECT Id FROM Profile WHERE Name='PRM - Sales Dependent Territory + QlikBuy'].Id;
        Id apId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
        
        Account acc = new Account(name ='Test Partner Account', RecordTypeId=PARTNER_ACCOUNT_RT) ;
        /* Test End User Account - START */
        
        insert acc;
            
            /* Test End User Account - END */
            
        Contact con = new Contact(LastName ='Test Partner Contact',AccountId = acc.Id,
                                     SFDCAccessGranted__c='LeadsOppsQuotes');
        
        insert con;  
        
        User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com', 
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
      localesidkey='en_US', profileid = apId, 
      timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');
        
        insert mockSysAdmin; //new List<User>{partner,accountManager};
		QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
                 qtc.Country_Name__c = 'Sweden';
                 qtc.CurrencyIsoCode = 'SEK';
            update qtc;
        
        Account act = new Account(name='Test Account');
            act.OwnerId = mockSysAdmin.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';
            insert act;
        
        Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
        
        Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Confirmed' ,
                RecordTypeId = '01220000000DNwY',   // Direct / Reseller - Std Sales Process
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = ct.AccountId,
                OEM_Partner_Account__c=null,
            	Partner_Contact__c = con.Id
                //GI_Acceptance_Status__c='GI Rejected'

            ); 
        //System.runAs(Package.version)
        Test.startTest();
        Boolean bool = SendMeQuoteDetails.isPartnerContactNull(testNewOpp.Id);
        String emailResult = SendMeQuoteDetails.sendEmail(testNewOpp.Id);
        Test.stopTest();

        
    }
    
    /*@IsTest
    public static void testSendMeQuotes(){
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name='TestOpp - delete me'];
        Test.startTest();
        Boolean bool = SendMeQuoteDetails.isPartnerContactNull(opp.Id);
        String emailResult = SendMeQuoteDetails.sendEmail(opp.Id);
        Test.stopTest();
    }*/
}