public with sharing class CustomInfluencerLookupController {
 
  public Contact contact {get;set;} 
  public List<Contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
     public String accplanid {get;set;}
     public String accid {get;set;}
  public CustomInfluencerLookupController() {
    contact = new Contact();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    accplanid = System.currentPageReference().getParameters().get('accplanid');
    
    accid = [SELECT Account__c FROM Account_Plan__c WHERE id = :accplanid].Account__c;
    runSearch();  
  }
 
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command Private
  public void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. Private 
  public List<Contact> performSearch(string searchString) {
 
    String soql = 'SELECT Account.id, name from Contact where Account.id = \'' + accid+ '\'';
    
  //  String soql = 'SELECT Account.id,name from Contact';
    if(searchString != '' && searchString != null)
      soql = soql +  ' AND name LIKE \'%' + searchString +'%\'';
    soql = soql + ' limit 30';
    System.debug(soql);
    return database.query(soql); 
 
  }
 

  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}