/**
* Change log:
*
*   04-01-2019  ext_vos, extbad CHG0030387: Add disableChatterEmailsForExternalUsersTest() and disableChatterEmailsForInternalUsersTest().
*/
@isTest
public class Test_CustomUserTriggerHdlr {

	@TestSetup
	static void setup() {
		QuoteTestHelper.createCustomSettings();

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
		insert qtc;

		Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
		insert testAccount;
		Contact testContactExt = TestDataFactory.createContact('testContactExt', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
		insert testContactExt;
	}

	public static testmethod void processUserTest() {
		Profile p = [select id from profile where name='System Administrator'];
		User u = new User(alias = 'standt', email='standarduser@testorg.com',
				emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
				localesidkey='en_US', profileid = p.Id,use_Salesforce_CPQ__c = false,Country = 'France',
				timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');
		QlikTech_Company__c Q = QTTestUtils.createMockQTCompany('United Kingdoms', 'GBR', 'United States');
		Q.Country_Name__c = 'France';
		update Q;
		User testUser = [Select id, Country, use_Salesforce_CPQ__c From User where id =: UserInfo.getUserId()];
		System.runAs(testUser){
			Test.startTest();
			insert u;


			u.use_Salesforce_CPQ__c = true;
			u.Quote_Approval_Role__c = 'CEO';
			update u;
			Test.stopTest();
		}
	}

	public static testMethod void disableChatterEmailsForInternalUsersTest() {
		// should not update UserPreferencesDisableAllFeedsEmail for internal users

		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		User u = new User(Alias = 'admtus', Email = 'user' + System.now().millisecond() + '@chemtest.com',
				EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
				LocalesIdKey = 'en_US', ProfileId = p.Id, Country = 'Sweden',
				TimeZonesIdKey = 'America/Los_Angeles', Username = 'admtus' + System.now().millisecond() + '@chemtest.com',
				UserPreferencesDisableAllFeedsEmail = false);

		Test.startTest();
		insert u;
		Test.stopTest();

		User testUser = [SELECT UserPreferencesDisableAllFeedsEmail FROM User WHERE Id =: u.Id];
		System.assert(!testUser.UserPreferencesDisableAllFeedsEmail);

		List<NetworkMember> nms = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :u.Id];
		for (NetworkMember nm : nms) {
			System.assert(!nm.PreferencesDisableAllFeedsEmail);
			nm.PreferencesDisableAllFeedsEmail = true;
		}
		update nms;

		testUser.UserPreferencesDisableAllFeedsEmail = true;
		update testUser;
		testUser = [SELECT UserPreferencesDisableAllFeedsEmail FROM User WHERE Id =: u.Id];
		System.assert(testUser.UserPreferencesDisableAllFeedsEmail);
		nms = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :u.Id];
		for (NetworkMember nm : nms) {
			System.assert(nm.PreferencesDisableAllFeedsEmail);
		}
	}

	public static testMethod void disableChatterEmailsForExternalUsersTest() {
		Profile exProfile = [SELECT id FROM profile WHERE Name = 'Customer Portal Base Access + QlikBuy' LIMIT 1];
		Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'testContactExt'];
		User exUs = new User (Alias = 'tuext', Email = 'user' + System.now().millisecond() + '@chemtest.com',
				EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
				LocalesIdKey = 'en_US', ProfileId = exProfile.Id, Country = 'Sweden', ContactId = contact.Id,
				TimeZonesIdKey = 'America/Los_Angeles', Username = 'tuext' + System.now().millisecond() + '@chemtest.com',
				UserPreferencesDisableAllFeedsEmail = false);

		Test.startTest();
		insert exUs;
		Test.stopTest();

		User testUser = [SELECT UserPreferencesDisableAllFeedsEmail FROM User WHERE Id =: exUs.Id];
		System.assert(testUser.UserPreferencesDisableAllFeedsEmail);

		List<NetworkMember> nms = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :exUs.Id];
		for (NetworkMember nm : nms) {
			System.assert(nm.PreferencesDisableAllFeedsEmail);
			nm.PreferencesDisableAllFeedsEmail = false;
		}
		update nms;

		testUser.UserPreferencesDisableAllFeedsEmail = false;
		update testUser;
		testUser = [SELECT UserPreferencesDisableAllFeedsEmail FROM User WHERE Id =: exUs.Id];
		System.assert(testUser.UserPreferencesDisableAllFeedsEmail);

		nms = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :exUs.Id];
		for (NetworkMember nm : nms) {
			System.assert(nm.PreferencesDisableAllFeedsEmail);
		}
	}
}