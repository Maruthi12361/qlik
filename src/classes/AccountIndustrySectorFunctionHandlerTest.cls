/**     * File Name:AccountIndustrySectorFunctionHandlerTest
        * Description : Test class for AccountIndustrySectorFunctionHandler
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 March 22nd 2017 RamakrishnaKini     Initial version.
        29.03.2017 Rodion Vakulvoskyi fix test failures commenting QuoteTestHelper.createCustomSettings();
        13.12.2017 Pramod Kumar V Test class coverage
        
*/
@isTest
private class AccountIndustrySectorFunctionHandlerTest {
    
    @testSetup static void test_method_one() {

        Semaphores.SetAllSemaphoresToTrue();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('USD');
        //QuoteTestHelper.createCustomSettings();
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, 
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com', 
        INT_NetSuite_InternalID__c = '5307');

        System.runAs(u) {
            SIC_Code__c code = new SIC_Code__c(Name ='8732:Commercial Economic, Sociological, and Educational Research', Industry__c='Services', QlikTech_Sector__c='Retail & Services');
            insert code;

            Id accrecTypeId = [select id from recordtype where developername = 'End_User_Account' limit 1].Id;

            Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, true);
            testAccount.RecordtypeId = accrecTypeId;
            testAccount.Name = 'Test Val Rule 1';
            testAccount.Territory_Country__c= 'France';
            update testAccount;
            
            List<QlikTech_Company__c> companyQT = [Select Id, QlikTech_Company_Name__c, Country_Name__c From QlikTech_Company__c Where Country_Name__c = 'France'];
            System.assertEquals('France', companyQT[0].Country_Name__c);

            Contact testContact1 = new Contact(); // typical account contact;
            testContact1.FirstName = 'Roman';
            testContact1.Lastname = 'Dovbush';
            testContact1.Email = 'test@test.com';
            testContact1.HasOptedOutOfEmail = false;
            testContact1.AccountId = testAccount.Id;
            insert testContact1;
            List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(testAccount.Id, testContact1.Id, 'Billing'),
                QuoteTestHelper.createAddress(testAccount.Id, testContact1.Id, 'Shipping')                
            };
            insert addrs;
            RecordType opprecTYpe= [select id from Recordtype where DeveloperName='Sales_QCCS' limit 1]; 
            Account testacc2 = [select id from account where name ='Test Val Rule 1' limit 1];
            Opportunity opp = new Opportunity(CurrencyIsoCode= 'USD', name = 'asasa', Short_Description__c ='frff',
                                        StageName = 'Goal Identified', Revenue_Type__c= 'Direct', CloseDate = Date.Today() +10,
                                        AccountId = testacc2.ID, RecordtypeId= opprecTYpe.Id);
            insert opp;
            RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote'];
            SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact1.id, testAccount, null, 'Direct', 'Open', 'Quote', true, opp.Id);
            insert quoteForTest;
            System.assertNotEquals(null, quoteForTest.Id);
        }
        Semaphores.SetAllSemaphoresToFalse();
    }
    
    @isTest static void test_method_two() {
        // Implement test code
        Id sicCodeId = [select id from SIC_Code__c where Name = '8732:Commercial Economic, Sociological, and Educational Research' limit 1].Id;
        System.assertNotEquals(null, sicCodeId);
        Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
        Test.startTest();
            AccountIndustrySectorFunctionHandler accind = new AccountIndustrySectorFunctionHandler();
            AccountIndustrySectorFunctionHandler.onBeforeInsert(new List<Account>());
            testAccount.SIC_Code_Name__c = sicCodeId;
            update testAccount;
        Test.stopTest();
        Account testAccount1 = [Select Id, Name,SIC_Code_Name__c,sector__c From Account Where Name = 'Test Val Rule 1' limit 1];
        System.assertNotEquals(null, testAccount1.SIC_Code_Name__c);
        System.assertEquals('Retail & Services', testAccount1.sector__c);
        
    }
    
}