@isTest
private class Z_SelectBillingAccountComponent_Test {


	private static Account account;
	private static Opportunity opp;
	private static Contact contact;
    private static Zuora__CustomerAccount__c customerAccount;
    private static Profile p;
    Private static User u2;

     private static void set_up_data() {
     	QuoteTestHelper.createCustomSettings();
     	QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
     	account = Z_TestFactory.buildAccount();
     	account.OwnerId = UserInfo.getUserId();
        account.Name = 'Sell Qlik Test Ltd.';
        account.Navision_Status__c = 'Partner';
        account.QlikTech_Company__c = 'QlikTech Inc';
        account.Billing_Country_Code__c = QTComp.Id;
     	insert account;

     	contact = Z_TestFactory.makeContact(account);
        insert contact;

     	opp = Z_TestFactory.buildOpportunity2(account);
     	opp.Revenue_Type__c = 'Direct';
        insert opp;

        customerAccount = Z_TestFactory.createBillingAccount(account, 'id0123456789KK');
        insert customerAccount;

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
      	User u2 = new User(Alias = 'nUser1', Email='newuser1234@testorg.com', 
	       EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	       LocaleSidKey='en_US', ProfileId = p.Id, 
	       TimeZoneSidKey='America/Los_Angeles', UserName='newuser1234@testorg.com');

     }
	
	@isTest static void test_class() {

		set_up_data();

			zqu.JSRemoteController.BillingAccountObjects accountObjs = new zqu.JSRemoteController.BillingAccountObjects();
			accountObjs.defaultBillingAccountType = 'test';
			accountObjs.billingAccounts = new zqu.JSRemoteController.ListData();
			accountObjs.billingAccounts.dataObjects = new List<Map<String,Object>>();
			accountObjs.billingAccounts.dataObjects.add(new Map<String,Object>());
			accountObjs.billingAccountTypes = new List<String>();
			accountObjs.billingAccountTypes.add('test');

			zqu.JSRemoteController.QuoteTypeObjects quoteTypeObjs = new zqu.JSRemoteController.QuoteTypeObjects();
			quoteTypeObjs.quoteTypes = new List<String>{'new', 'amend','renew','cancel'};

			zqu.JSRemoteController.SubscriptionObjects subscriptionObjs = new zqu.JSRemoteController.SubscriptionObjects();
			subscriptionObjs.subscriptions = new zqu.JSRemoteController.ListData();
			subscriptionObjs.subscriptions.dataObjects =  new List<Map<String,Object>>();
			subscriptionObjs.subscriptions.dataObjects.add(new Map<String,Object>());
			subscriptionObjs.opportunityId = opp.id;

			Test.startTest();
			Z_SelectBillingAccountComponent z_SelectBillingAccountPlugin = new Z_SelectBillingAccountComponent();
			z_SelectBillingAccountPlugin.getAvailableBillingAccounts(accountObjs);
			z_SelectBillingAccountPlugin.getAvailableQuoteTypes(quoteTypeObjs);
			z_SelectBillingAccountPlugin.getAvailableSubscriptions(subscriptionObjs);
			Test.stopTest();
		
	}


	
}