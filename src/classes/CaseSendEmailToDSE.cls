/****************************************************************************************************

    CaseSendEmailToDSE
    
    Class to send an email to the DSE.
    I did not use assignment rules as Customer Users were unable to trigger the assignment email through code.
        
    Changelog:

        2016-04-06  AIN Initial implementation
        2017-11-13 Ravindra Babu IT-128 Added condition to filter InActive Users
        Added code to send DSE email from From Email address "noreply@qlikview.com"
        2018-04-12  ext_vos CHG0030589 - Change "From" address from 'noreply@qlikview.com' to 'no-reply@qlik.com'.
        2018-04-18  ext_bad CHG0033720   Change org wide email, select it from custom settings.
****************************************************************************************************/
public class CaseSendEmailToDSE {
    //Used for test
    public static boolean EmailWasSent = false;
    public static void sendEmailsToDse(List<Case> cases) {
        Messaging.SingleEmailMessage[] dseEmailList = new List<Messaging.SingleEmailMessage>();
        system.debug(LoggingLevel.DEBUG, '[CaseSendEmailToDSE] ');

        String noReplyAddress = '';

        if(QTCustomSettings__c.getInstance('Default') != null) {
            noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
        }
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = :noReplyAddress];

        for(Case c : cases) {
            if(c.Designated_Support_Engineer__c != null) {
        List<Id> userIds = new List<Id>();
        //Group is Designated Support Engineer
        //Added condition to filter the InActive Users IT-128
        for(GroupMember gm : [Select UserOrGroupId From GroupMember where GroupId = '00GD00000027hII'  and UserOrGroupId in(select Id from User where IsActive = true)])
            userIds.Add(gm.UserOrGroupId);

        List<string> userEmails = new List<string>();
        for(User u : [select Email from User where id in :userIds])
            userEmails.Add(u.Email);

                Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
                if (userEmails.Size() > 0) {
                    emailMessage.setToaddresses(userEmails);
                    system.debug(LoggingLevel.DEBUG, '[CaseSendEmailToDSE] emails: ' + userEmails);
                } else {
                    system.debug(LoggingLevel.DEBUG, '[CaseSendEmailToDSE] No user emails found in queue');
                    break;
                }

                if ( owea.size() > 0 ) {
                    emailMessage.setOrgWideEmailAddressId(owea.get(0).Id);
                }

                List<EmailTemplate> ets = [Select Id from EmailTemplate where Name = 'DSE Case Created'];
                EmailTemplate et;

                if (ets.size() > 0)
                    et = ets[0];
                else {
                    system.debug(LoggingLevel.DEBUG, '[CaseSendEmailToDSE] No email template with the name DSE Case Created found');
                    break;
                }
                emailMessage.settargetObjectId(c.ContactId);
                emailMessage.setTreatTargetObjectAsRecipient(false);
                emailMessage.setWhatId(c.Id);
                emailMessage.setTemplateId(et.ID);
                dseEmailList.add(emailMessage);
            }
        }

        if (dseEmailList != null && !dseEmailList.isEmpty()) {
            try {
                List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(dseEmailList);
                for (Messaging.SendEmailResult res : sendres) {
                    system.debug(LoggingLevel.DEBUG, '[CaseSendEmailToDSE] Result: ' + res);
                    EmailWasSent = true;
                }
            } 
            catch (System.EmailException ex) {
                system.debug(LoggingLevel.DEBUG, '[CaseSendEmailToDSE] Error when sending mail: ' + ex.GetMessage());
            }
        }
    }
}