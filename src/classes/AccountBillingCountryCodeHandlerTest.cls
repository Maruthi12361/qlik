/* File AccountBillingCountryCodeHandlerTest
    * @description : Unit test for AccountBillingCountryCodeHandler
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       20.12.2017   Pramod Kumar V         Created Class

*/
@isTest(seeAlldata=false)
private class AccountBillingCountryCodeHandlerTest{
    
   
    static testMethod void test_AccountBillingCountryCodeHandler() {
    
       Id rtId4 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
       Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
       QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
      // Account testAccount3 =new Account(name='Test AccountName2', recordtypeid=rtId4,QlikTech_Company__c ='QTcomp',Territory_Country__c='France',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222',Billing_Country_Code__c=QTcomp.id );
       //Account testAccount3 =new QuoteTestHelper.createAccount(QlikTech_Company__c qtComp, RecordType rType, String description)
        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];

       Account testAccount3 = QuoteTestHelper.createAccount(QtComp, recTypeAccEndUser , 'test');
       testAccount3.Territory_Country__c='France';
       insert testAccount3;
       
       
       Subsidiary__c subs1 = new Subsidiary__c (name='test',Legal_Entity_Name__c='test',CurrencyIsoCode='USD');
       insert subs1 ;
       QlikTech_Company__c QTcomp1 = new QlikTech_Company__c(Name='Test',CurrencyIsoCode='USD',Subsidiary__c=subs1.id,NS_Country_Name__c='India');
       insert QTcomp1;
       
       Account testAccount =new Account(name='Test AccountName2', recordtypeid=rtId4,QlikTech_Company__c ='QTcomp',Territory_Country__c='France',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222',Billing_Country_Code__c=QTcomp1.id );
       insert testAccount ;
        testAccount.Territory_Country__c='India';
        testAccount.Billing_Country_Code__c=QTcomp.id;
        update testAccount;
       //Contact conPartner1 = QuoteTestHelper.createContact(testAccount3.id);
       //insert conPartner1;
       List<Account> inputList=new List<Account> ();
       inputList.add(testAccount3);
       //AccountBillingCountryCodeHandler Accbilling=new AccountBillingCountryCodeHandler ();
       
         
       
       AccountBillingCountryCodeHandler.onBeforeInsert(inputList);
       Map<id, Account> inputMap =new Map<id, Account>();
       Map<id, Account> oInputMap =new Map<id, Account>();
       inputMap.put(testAccount3.id,testAccount3);
       testAccount3.Territory_Country__c='India';
       
       update testAccount3;
       inputList.add(testAccount3);
       oInputMap.put(testAccount3.id,testAccount3);
       
       inputList.add(testAccount);
       oInputMap.put(testAccount.id,testAccount);
       AccountBillingCountryCodeHandler.onBeforeInsert(inputList);
       AccountBillingCountryCodeHandler.onBeforeUpdate(inputList, inputMap,oInputMap);
       //AccountBillingCountryCodeHandler.updBillingCountryCode(inputList);
       
    
    }
    
 }