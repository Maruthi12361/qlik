/****************************************************************
*
*  TEST_QuoteBusinessLogic
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*  17.03.2017 : Rodion Vakulvsokyi
*  29.03.2017 : Roman Dovbush  : duplicated createCustomSettings() deleted. We call it from QTTestUtils.GlobalSetUp();
* 02.09.2017 : Srinivasan PR- fix for query error
*****************************************************************/
@isTest
public with sharing class TEST_QuoteBusinessLogic {
    static final String OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').Id; //'012D0000000KEKOIA4';
	static final String AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id; //'01220000000DOFu';
    public static testmethod void testQuoteAndOppCreation(){
        prepareData();
    }

    private static void prepareData() {
         QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
		/*
            Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;

        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount;
		*/
		Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAccount;
		Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
		List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
										where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
        //quoteForTest.SBQQ__Primary__c = true;
           insert quoteForTest;
        Test.startTest();

            update  quoteForTest;

            SBQQ__Quote__c quoteforQuery = [select id, SBQQ__Opportunity2__c From SBQQ__Quote__c Where id =:quoteForTest.id];

            SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
            insert testQuoteLine;


            Opportunity createdOpp = [Select id From Opportunity where id =: quoteforQuery.SBQQ__Opportunity2__c];
        Test.stopTest();

        System.assertEquals([select id, SBQQ__Opportunity2__c From SBQQ__Quote__c Where id =:quoteForTest.id].SBQQ__Opportunity2__c, createdOpp.id);
        List<OpportunityLineItem> oppLines = new List<OpportunityLineItem>([select id From opportunityLineItem where OpportunityId =: createdOpp.id]);
         System.assertEquals(0, oppLines.size());
        }
    }

    public static testmethod void testAddressValidationAndCreation() {
        QuoteTestHelper.createCustomSettings();
		/*
        Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            testAccount.BillingCountry = 'France';
            insert  testAccount;
		*/
		User testUser = [Select id From User where id =: UserInfo.getUserId()];
		Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
			testAccount.BillingCountry = 'France';
	        update testAccount;
		Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
		List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

        Test.setCurrentPageReference(new PageReference('Page.RedirectorAndValidator'));
        System.currentPageReference().getParameters().put('accId', testAccount.id);
        System.currentPageReference().getParameters().put('type', 'Quote');

        Test.setMock(WebServiceMock.class, new AdressMockService());
        Test.startTest();
        Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
        billAddress.Country__c = 'United Kingdom';
        insert billAddress;

        Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            insert shippAddress;
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           insert quoteForTest;

            update quoteForTest;

            update shippAddress;

            delete shippAddress;
        Set<id> setOfIds = new Set<Id>();
        setOfIds.add(billAddress.id);
        AddressTriggerHandler handler = new AddressTriggerHandler(true, 1);

        System.assertEquals(true, handler.IsTriggerContext);
        System.assertEquals(false, handler.IsVisualforcePageContext);
        System.assertEquals(false,handler.IsWebServiceContext);
        System.assertEquals(false,handler.IsExecuteAnonymousContext);


        Test.stopTest();

        AddressTriggerHelper.VerifyAddress(setOfIds);
        AddressTriggerHelper.CheckRestrictedCompany(setOfIds);
        System.assertEquals(false,AddressTriggerHelper.IsApiUser());
    }

    public static testmethod void testQuoteAddingLineItems() {
         QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
		/*
            Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount;
		*/
		Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAccount;
		Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
		List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
		                          where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PW');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
		 Test.startTest();
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
            quoteForTest.SBQQ__Primary__c = true;
        insert quoteForTest;


            update quoteForTest;
            SBQQ__Quote__c quoteforQuery = [Select id, SBQQ__Opportunity2__c From SBQQ__Quote__c where id =: quoteForTest.id];
            Opportunity oppForTesting = [Select id, CloseDate From Opportunity where id =: quoteforQuery.SBQQ__Opportunity2__c];
            oppForTesting.CloseDate = Date.today();
            update oppForTesting;
         SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        //insert testQuoteLine;
        Test.stopTest();

     //   System.assertEquals(true, [Select id From OpportunityLineItem].size() > 0);
       // System.assertEquals(true, [Select id From OpportunityLineItemSchedule].size() > 0);
        }
    }

    public static testmethod void quotePendingStatus() {

        QuoteTestHelper.createCustomSettings();
        User sysAmin = [select id From User where id =: UserInfo.getUserId()];
        System.runAs(sysAmin) {
            Test.startTest();
                SBQQ__Quote__c quoteForTest = createPrepareDataForRevenueType();
                quoteForTest.SBQQ__Status__c = 'Pending';
            //not sure.check it from ui
                update quoteForTest;
            Test.stopTest();
        }

    }

    public static testmethod void quoteApprovedStatus() {
        QuoteTestHelper.createCustomSettings();
        User sysAmin = [select id From User where id =: UserInfo.getUserId()];
        System.runAs(sysAmin) {
        Test.startTest();
             SBQQ__Quote__c quoteForTest = createPrepareDataForRevenueType();
             String recOld = quoteForTest.RecordTypeId;
            quoteForTest.SBQQ__Status__c = 'Approved';
            // record type change check
            update quoteForTest;
            SBQQ__Quote__c quoteNewRecordType = [Select id, RecordTypeId From SBQQ__Quote__c where id =: quoteForTest.id];
            System.assertEquals(quoteNewRecordType.RecordTypeId, quoteForTest.RecordTypeId);
        Test.StopTest();
        }
    }

    public static testmethod void quoteRejectedStatus() {
        QuoteTestHelper.createCustomSettings();
        User sysAmin = [select id From User where id =: UserInfo.getUserId()];
        System.runAs(sysAmin) {
        Test.startTest();
            SBQQ__Quote__c quoteForTest = createPrepareDataForRevenueType();
            String recOld = quoteForTest.RecordTypeId;
            quoteForTest.SBQQ__Status__c = 'Rejected';
            // record type change check
            update quoteForTest;
            System.assertEquals(recOld, quoteForTest.RecordTypeId);
        Test.stopTest();
        }
    }

    public static testmethod void quotePlaceOrderStatus() {
        QuoteTestHelper.createCustomSettings();
        User sysAmin = [select id From User where id =: UserInfo.getUserId()];
        System.runAs(sysAmin) {
        Test.startTest();
            SBQQ__Quote__c quoteForTest = createPrepareDataForRevenueType();
            quoteForTest.SBQQ__Status__c = 'Place order';
            //to check if its creating assets for primary quote
            update quoteForTest;
            System.assertEquals(0, [Select id From Asset].size());
        Test.stopTest();
        }
    }

    public static testmethod void quoteSendCustomerStatus() {
        QuoteTestHelper.createCustomSettings();
        User sysAmin = [select id From User where id =: UserInfo.getUserId()];
        System.runAs(sysAmin) {
        Test.startTest();
            SBQQ__Quote__c quoteForTest = createPrepareDataForRevenueType();
            quoteForTest.SBQQ__Status__c = 'Send to customer';
            //UI
            update quoteForTest;
        Test.stopTest();
        }
    }


    public static testmethod void quoteAcceptCustomerStatus() {
        QuoteTestHelper.createCustomSettings();
        User sysAmin = [select id From User where id =: UserInfo.getUserId()];
        System.runAs(sysAmin) {
        Test.startTest();
            SBQQ__Quote__c quoteForTest = createPrepareDataForRevenueType();
            quoteForTest.SBQQ__Status__c = 'Accepted by customer';
            //license and assets check
            update quoteForTest;
        Test.stopTest();
        }
    }

    public static testmethod void testServiceDefferred() {
        QuoteTestHelper.createCustomSettings();
            User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        Test.startTest();
            SBQQ__Quote__c  quoteForTest2 = createPrepareDataForRevenueType();
            quoteForTest2.SBQQ__Status__c = 'Services - Deferred';
            update quoteForTest2;
        Test.stopTest();
        }
    }

    public static SBQQ__Quote__c createPrepareDataForRevenueType() {
		/*
            Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount;
			*/
			User testUser = [Select id From User where id =: UserInfo.getUserId()];
			Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAccount;
		Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
		List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
									where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
         //  quoteForTest.SBQQ__Primary__c = true;
        insert quoteForTest;
        update quoteForTest;
    return quoteForTest;
    }

	private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
            								Country_Code_Two_Letter__c = countryAbbr,
            								Country_Name__c = countryName,
            								Subsidiary__c = subsId);
    return qlikTechIns;
    }

}