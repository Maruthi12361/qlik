/*Changelog: 
* 2015-10-28 IRN Winter 16 Issue - removed seealldata and added call to globalsetup 
* 2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring  
*/
@isTest
private class Test_Opportunity_UpdateOppOwnerIntId {

    static testMethod void Test_FieldTOBeUpdated() {
    	QTTestUtils.GlobalSetUp();

        //Create the test Account
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		
		Account TestAccount = new Account(
			Name = 'PUFA Test Ltd.',
			Navision_Status__c = 'Customer',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id						
		);
		insert TestAccount;
		
		//Add an Opportunity	
		Opportunity Opp = new Opportunity();
		Opp.AccountId = TestAccount.Id;
		Opp.Short_Description__c = 'My Test Description';
		Opp.Name = '.';
		Opp.Amount = 0;
		Opp.CurrencyIsoCode = 'USD';
		Opp.CloseDate = Date.today().addDays(30);
		Opp.StageName = 'Goal Identified';
		insert Opp;	
		
		User user1 = [select Id, INT_NetSuite_InternalID__c from User where Id = :UserInfo.getUserId()];
		Opp = [select Id, INT_NetSuite_OppOwner_InternalD__c from Opportunity where Id = :Opp.Id];
		
		System.assertEquals(user1.INT_NetSuite_InternalID__c, Opp.INT_NetSuite_OppOwner_InternalD__c);
		User user2 = [select Id, INT_NetSuite_InternalID__c from User where IsActive = true and Profile__c = 'Custom: QlikBuy Sales Std User' LIMIT 1];
		Semaphores.OpportunityTriggerBeforeUpdate = false;
       	Semaphores.OpportunityTriggerAfterUpdate = false;
       	Semaphores.OpportunityTriggerBeforeUpdate2 = false;
       	Semaphores.OpportunityTriggerAfterUpdate2 = false;
		Opp.OwnerId = user2.Id;
		update Opp;
		Opp = [select Id, INT_NetSuite_OppOwner_InternalD__c from Opportunity where Id = :Opp.Id];
		
		System.assertEquals(user2.INT_NetSuite_InternalID__c, Opp.INT_NetSuite_OppOwner_InternalD__c);
    }
}