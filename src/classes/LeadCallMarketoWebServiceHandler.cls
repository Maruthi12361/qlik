/******************************************************

	Class: LeadCallMarketoWebServiceHandler  
  
	This functionality looks for "Trigger_Webservice" to be
	checked. When checked it will be reset and a call
	to the webservice at dev.qlikview.com will be
	performed.

	Changelog:    
		2010-12-23  MHG   	Created trigger
		2011-03-01	MHG		Added Shaddow field
		2017-10-25 AYS BMW-402 : Migrated from CallMarketoWebService.trigger.
		2018-03-22 CRW CHG0033482:Added !System.isBatch()
******************************************************/ 

public class LeadCallMarketoWebServiceHandler {

	public static void handle(List<Lead> triggerNew, Boolean isBefore, Boolean isAfter) {
		// A new Lead will not contain an Id until it's saved. A shadow field will
		// be used to singnal the after trigger to call the webservice
		if (isBefore)
		{
			for (Lead l : triggerNew)
			{
				// Start of my clearing the shadow field (might be set if function has been used before)
				if (l.Trigger_Webservice_Shadow__c)
				{
					l.Trigger_Webservice_Shadow__c = false;
				}
				// If we should trigger. Clear the checkbox and select the shadow field. 
				if (l.Trigger_Webservice__c)
				{
					l.Trigger_Webservice_Shadow__c = true;
					l.Trigger_Webservice__c = false;
				}	
			}						
		}
		// In the after trigger we will always have an Id. We should only look at
		// the shadow field in the after trigger.
		if (isAfter)
		{
			Map<string, string> LeadToCall = new Map<string, string>();
			
			for (Lead l : triggerNew)
			{
				if (l.Trigger_Webservice_Shadow__c)
				{
					string Id = l.Id;
					LeadToCall.put(Id, l.Email);
				}			
			}
			// If there is any Leads in the Call queue, call the external webservice
			if (LeadToCall.size() > 0)
			{
				System.debug('CallMarketoWebService: Calling array: ' + LeadToCall);
                if(!System.isBatch()){
                	System.debug('CallInitializeApexTrigger future method invoked for>>' + LeadToCall);
					devQlikViewCom.CallInitializeApexTrigger(LeadToCall, false);
                }
			}
		}
	}
}