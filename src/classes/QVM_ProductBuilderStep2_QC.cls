/*
@author: Malay Desai, Slalom LLC
@date: 01/20/2019
@description: Controller for the QVM "product builder step 2" page
#JIRA ID: BMWPC-68
*/
public without sharing class QVM_ProductBuilderStep2_QC {
    
    public QVM_Product_Data__c product {get; set;}
    public Id pId {get; set;}
    
    private List<SelectOption> QEAOptionsList;  //CCE 2013-02-01 CR# 6833
    
    //private String MAGENTO_SESSION_ID {get;set;}
    
    public list<SelectOption> MagentoProductTypes {get;set;}
    public String ProductTypeId {get;set;}
    public String ExpertiseArea {get;set;}  //CCE 2013-02-12 CR# 6833
    
    public list<SelectOption> MagentoProductIndustries {get;set;}
    public String[] IndustryIds {get;set;}
    
    public list<SelectOption> MagentoJobFunctions {get;set;}
    public String[] FunctionIds {get;set;}
    
    public list<SelectOption> MagentoQVVersion {get;set;}
    public String[] VersionIds {get;set;}
    
    public map<String,String> MagentoCategoryMap {get;set;}
    
    public QVM_ProductBuilderStep2_QC() {
        pId = ApexPages.currentPage().getParameters().get('id');
        product = [select Id, Type__c, Product_Type_Id__c, Industry__c, Industry_Id__c, Job_Function__c, Job_Function_Id__c, 
            Version__c, QlikView_Version__c, QlikView_Version_Id__c, Expertise_Area_Formula__c, Partner_Expertise_Area_ID__c
            from QVM_Product_Data__c where Id = :pId];
        //call Magento to obtain session id
        //MAGENTO_SESSION_ID = QVM.getMagentoSessionId();
        
        //this.setupMagentoLists();
        this.setupMagentoCategoryMap();
        
        ProductTypeId = (product.Product_Type_Id__c == null) ? '' : product.Product_Type_Id__c;
        IndustryIds = (product.Industry_Id__c == null) ? new String[]{} : QVM.getArrayFromString(product.Industry_Id__c, ';');
        FunctionIds = (product.Job_Function_Id__c == null) ? new String[]{} : QVM.getArrayFromString(product.Job_Function_Id__c, ';');
        VersionIds = (product.QlikView_Version_Id__c == null) ? new String[]{} : QVM.getArrayFromString(product.QlikView_Version_Id__c, ';');
        
        System.debug('QVM_ProductBuilderStep2: product.Expertise_Area_Formula__c = ' + product.Expertise_Area_Formula__c);        
        ExpertiseArea = (product.Expertise_Area_Formula__c == null) ? '' : product.Expertise_Area_Formula__c;       //CCE 2013-02-12 CR# 6833
    }
    
    public void checkForExceptions() {
        if(ProductTypeId == null || ProductTypeId == '') {
            throw new QVM.newException(Label.QVM_PB_Step_2_Type_Exception);
        }
        if(IndustryIds == null || IndustryIds.Size() == 0) {
            throw new QVM.newException(Label.QVM_PB_Step_2_Industry_Exception);
        }
        if(FunctionIds == null || FunctionIds.Size() == 0) {
            throw new QVM.newException(Label.QVM_PB_Step_2_Job_Function_Exception);
        }
        if(VersionIds == null || VersionIds.Size() == 0) {
            throw new QVM.newException(Label.QVM_PB_Step_2_QlikView_Version_Exception);
        }
    }
    
    //public void setupMagentoLists() {
        //call Magento and in a single call retrieve all catalog tree info
        //Dom.Document doc = QVM.getMagentoCallDocument(MAGENTO_SESSION_ID, 'catalog_category.tree', null);
        
        ////parse catalog tree document into select options
        //MagentoProductTypes = QVM.getMagentoCategoryList(doc, 'Type');
        //MagentoProductIndustries = QVM.getMagentoCategoryList(doc, 'Industry');
        //MagentoJobFunctions = QVM.getMagentoCategoryList(doc, 'Function');
        //MagentoQVVersion = QVM.getMagentoCategoryList(doc, 'Qlikview Version');
    //}
    
    public void setupMagentoCategoryMap() {
        MagentoCategoryMap = new map<String,String>();
        //map<String,String> MagentoCategoryMapNew = new map<String,String>();
        
        //list<SelectOption> alloptions = new list<SelectOption>();
        
        //alloptions.addAll(MagentoProductTypes);
        //alloptions.addAll(MagentoProductIndustries);
        //alloptions.addAll(MagentoJobFunctions);
        //alloptions.addAll(MagentoQVVersion);
        
        //for(SelectOption o : alloptions) {
        //    MagentoCategoryMap.put(o.getValue(),o.getLabel());
        //}

    
        MagentoProductTypes = new list<SelectOption>();
        MagentoProductIndustries = new list<SelectOption>();
        MagentoJobFunctions = new list<SelectOption>();
        MagentoQVVersion = new list<SelectOption>();

        QVM_Settings__c Settings = QVM_Settings__c.getOrgDefaults();
        List<string> magCap = Settings.MagentoCategoryMap_Type__c != null ? Settings.MagentoCategoryMap_Type__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < magCap.size(); i+=2) {   
            MagentoCategoryMap.put(magCap[i],magCap[i+1]);
            MagentoProductTypes.add(new SelectOption(magCap[i],magCap[i+1]));
        }
        magCap = Settings.MagentoCategoryMap_Industry__c != null ? Settings.MagentoCategoryMap_Industry__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < magCap.size(); i+=2) {   
            MagentoCategoryMap.put(magCap[i],magCap[i+1]);
            MagentoProductIndustries.add(new SelectOption(magCap[i],magCap[i+1]));
        }
        magCap = Settings.MagentoCategoryMap_Function_1__c != null ? Settings.MagentoCategoryMap_Function_1__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < magCap.size(); i+=2) {   
            MagentoCategoryMap.put(magCap[i],magCap[i+1]);
            MagentoJobFunctions.add(new SelectOption(magCap[i],magCap[i+1]));
        }
        magCap = Settings.MagentoCategoryMap_Function_2__c != null ? Settings.MagentoCategoryMap_Function_2__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < magCap.size(); i+=2) {   
            MagentoCategoryMap.put(magCap[i],magCap[i+1]);
            MagentoJobFunctions.add(new SelectOption(magCap[i],magCap[i+1]));
        }
        magCap = Settings.MagentoCategoryMap_QVVersion__c != null ? Settings.MagentoCategoryMap_QVVersion__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < magCap.size(); i+=2) {   
            MagentoCategoryMap.put(magCap[i],magCap[i+1]);
            MagentoQVVersion.add(new SelectOption(magCap[i],magCap[i+1]));
        }
        magCap = Settings.MagentoCategoryMap_QVVersion_2__c != null ? Settings.MagentoCategoryMap_QVVersion_2__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < magCap.size(); i+=2) {   
            MagentoCategoryMap.put(magCap[i],magCap[i+1]);
            MagentoQVVersion.add(new SelectOption(magCap[i],magCap[i+1]));
        }
        magCap = Settings.MagentoCategoryMap_QVVersion_3__c != null ? Settings.MagentoCategoryMap_QVVersion_3__c.split(',', -1) : new List<string>();
        for (Integer i = 0; i < magCap.size(); i+=2) {   
            MagentoCategoryMap.put(magCap[i],magCap[i+1]);
            MagentoQVVersion.add(new SelectOption(magCap[i],magCap[i+1]));
        }
    }
    
    public String getMagentoCategoryString(String[] items) {
        String result = '';
        for(String s : items) {
            result += ';' + MagentoCategoryMap.get(s);
        }
        return result;
    }
    
    public void setupMagentoCategoryValues() {
        product.Product_Type_Id__c = ProductTypeId;
        product.Industry_Id__c = QVM.getFlatStringFromArray(IndustryIds, ';');
        product.Job_Function_Id__c = QVM.getFlatStringFromArray(FunctionIds, ';');
        product.QlikView_Version_Id__c = QVM.getFlatStringFromArray(VersionIds, ';');

        product.Type__c = MagentoCategoryMap.get(ProductTypeId);
        product.Industry__c = getMagentoCategoryString(IndustryIds);
        product.Job_Function__c = getMagentoCategoryString(FunctionIds);
        product.QlikView_Version__c = getMagentoCategoryString(VersionIds);
        
        //product.Expertise_Area__c = ExpertiseArea;        //CCE 2013-02-12 CR# 6833
        //product.Expertise_Area_Formula__c = ExpertiseArea;        //CCE 2013-02-14 CR# 6833
        product.Partner_Expertise_Area_ID__c = QVM.getPartnerExpertiseAreaID(ExpertiseArea).Id; //CCE 2013-02-13 CR# 6833
    }
    
    public PageReference saveProduct() {
        
        try{
            this.checkForExceptions();
            this.setupMagentoCategoryValues();
            update product;
            PageReference next;

            next = Page.QVM_ProductBuilderStep2_QC;
            return next;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
        
    }
    
    public PageReference previewPage() {
        try{
            this.checkForExceptions();
            this.setupMagentoCategoryValues();
            update product;
            PageReference preview;
            preview = Page.QVM_ProductPreview_QC;
            preview.getParameters().put('Id', product.Id);
            return preview;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }

    public PageReference backPage() {
        try{
            update product;
            PageReference back;
            back = Page.QVM_ProductBuilderStep1_QC;
            back.getParameters().put('Id', product.Id);
            return back;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }
    
    public PageReference nextPage() {
        try{
            this.checkForExceptions();
            this.setupMagentoCategoryValues();
            update product;
            PageReference next; 
            next = Page.QVM_ProductBuilderStep3_QC;
            next.getParameters().put('Id', product.Id);
            return next;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
        
    }
    
    //Get the list of Expertise Areas for the currently logged in portal user CCE 2013-02-05 CR# 6833
    public List<SelectOption> getQEAOptionsList() {
        this.QEAOptionsList = new List<SelectOption>();
        this.QEAOptionsList.add(new selectOption('', '- None -')); //add the first option of '- None -' in case the user doesn't want to select a value or there is nothing returned from getExpertiseAreaList().
        List<string> QEAList = new List<string>(QVM.getExpertiseAreaList());
        for(Integer n=0; n<QEAList.size(); n++){
            this.QEAOptionsList.add(new SelectOption(QEAList.get(n),QEAList.get(n)));   
        }
        return QEAOptionsList;
    }     

}