/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_PartnerLibrarySearchTest {
	
	private static Id[] fixedSearchResults;
	@isTest static void testWithResults() {

		InsertTestDate();

		CreateSearchResults();	

		system.debug('fixedSearchResults.size(): ' + fixedSearchResults.size());
		for(Id i : fixedSearchResults)
			system.debug('Id: ' + i);
		Test.setFixedSearchResults(fixedSearchResults);

		QS_PartnerLibrarySearch pls = new QS_PartnerLibrarySearch('Qlik', 0);
	}
	@isTest static void testWithNoResults() {

		InsertTestDate();
		QS_PartnerLibrarySearch pls = new QS_PartnerLibrarySearch('Qlik', 1);
	}
	@isTest static void testWithNoSearchString() {

		InsertTestDate();
		QS_PartnerLibrarySearch pls = new QS_PartnerLibrarySearch('', 1);
	}
	private static void InsertTestDate()
	{

		List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
		if(searchSettings.Size() == 0)
		{
			QS_SearchSettings__c ss = new QS_SearchSettings__c();
			ss.Name = 'SearchSettings';
			ss.Page_Size__c = 20;
			ss.Jive_DefaultPlaces__c ='/places/217522';
			ss.Jive_EndPoint__c ='https://community.qlik.com/api/core/v3/search/contents?startIndex=index&callback=?&count=pageSize&filter=search(searchTerms)';
			ss.Instance_Name__c = 'cs14';
			ss.Jive_EndPoint_Personas__c = 'https://community.qlik.com/api/core/v3/contents/trending?&callback=?&count=pageSize&startIndex=index&filter=place(placeTerms)';
			insert ss;
		}

	}
	private static void CreateSearchResults()
	{
		
        //In order to query through knoledgeArticles we need to add at least one on dev environments.
        ContentVersion version = setupContent();

		//List<ContentVersion> results =  [select ContentDocumentId, Title, Description, id, ContentModifiedDate from ContentVersion where PublishStatus= 'P'  and IsLatest = true and Framework_Category__c in ('Development','Deployment', 'Scalability','Administration') order by ContentModifiedDate desc limit 2];
		List<ContentVersion> results =  [select ContentDocumentId, Title, Description, id, ContentModifiedDate from ContentVersion where IsLatest = true order by ContentModifiedDate desc limit 2];

		system.debug('results.size(): ' + results.size());

		system.assert(results.size() == 2);

		fixedSearchResults= new Id[2];

		fixedSearchResults[0] = results[0].Id;
		fixedSearchResults[1] = results[1].Id;
	}
	public static ContentVersion setupContent() // inserts content and download history
    { 
    	List<ContentVersion> contentVersions = new List<ContentVersion>();
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.VersionData = Blob.valueOf('testString');      
        contentVersion.PathOnClient = 'test.txt';
        contentVersions.Add(contentVersion);
        contentVersion = new ContentVersion();
        contentVersion.VersionData = Blob.valueOf('testString');      
        contentVersion.PathOnClient = 'test.txt';
        contentVersions.Add(contentVersion);
        insert contentVersions; 
        List<ContentVersionHistory> hisList = new List<ContentVersionHistory>();     
        ContentVersionHistory cvh = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');   
        ContentVersionHistory cvh1 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        ContentVersionHistory cvh2 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        ContentVersionHistory cvh3 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        hisList.add(cvh);
        hisList.add(cvh1);
        hisList.add(cvh2);
        hisList.add(cvh3);
        insert hisList; 
        System.debug('hislist---'+hisList);      
        return contentVersion;  
    }   
}