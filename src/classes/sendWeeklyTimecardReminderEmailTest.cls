/*****************************************************************************************************************
 Change Log:

 20130130	TJG Unit test for CR 6560 https://eu1.salesforce.com/a0CD000000NL6PY
 			Send a reminder email message on Monday if consultant has not complete timecard for the week just ended
******************************************************************************************************************/
@isTest
private class sendWeeklyTimecardReminderEmailTest {

    static testMethod void myUnitTest() {
		test.startTest();
		sendWeeklyTimecardReminderEmail wtcMail = new sendWeeklyTimecardReminderEmail();
		String schedule = '0 30 * * * ?';
		system.schedule('wtcMail', schedule, wtcMail);
		test.stopTest();
    }
}