/****************************************************************
*
*  TestAddressTrigger
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*****************************************************************/
@isTest
public class TestAddressTrigger {
	static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    public static testmethod void testAddressProcessing() {
        QuoteTestHelper.createCustomSettings();
		User testUser = [Select id From User where id =: UserInfo.getUserId()];
		/*
        Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            testAccount.BillingCountry = 'France';
            insert  testAccount;
		*/
		Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
			testAccount.BillingCountry = 'France';
	        update testAccount;
		Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
		List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

        Test.setCurrentPageReference(new PageReference('Page.RedirectorAndValidator'));
        System.currentPageReference().getParameters().put('accId', testAccount.id);
        System.currentPageReference().getParameters().put('type', 'Quote');

        Test.setMock(WebServiceMock.class, new AdressMockService());
        Test.startTest();
        Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
		billAddress.Country__c = 'United Kingdom';
		insert billAddress;

        Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            insert shippAddress;
        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           insert quoteForTest;

            update quoteForTest;

            update shippAddress;

            delete shippAddress;
        Set<id> setOfIds = new Set<Id>();
        setOfIds.add(billAddress.id);
        AddressTriggerHandler handler = new AddressTriggerHandler(true, 1);

        System.assertEquals(true, handler.IsTriggerContext);
        System.assertEquals(false, handler.IsVisualforcePageContext);
        System.assertEquals(false,handler.IsWebServiceContext);
        System.assertEquals(false,handler.IsExecuteAnonymousContext);


        Test.stopTest();

        AddressTriggerHelper.VerifyAddress(setOfIds);
        AddressTriggerHelper.CheckRestrictedCompany(setOfIds);
        System.assertEquals(false,AddressTriggerHelper.IsApiUser());
    }

	private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
            								Country_Code_Two_Letter__c = countryAbbr,
            								Country_Name__c = countryName,
            								Subsidiary__c = subsId);
    return qlikTechIns;
    }
}