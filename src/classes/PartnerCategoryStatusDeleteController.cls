/**     * File PartnerCategoryStatusDeleteController
        * @author : Reshma Ravi
        * @description : QCW-3792
        * Modification Log ===============================================================
        Ver     Date         Author              Modification 
        1       19.09.2017   Reshma Ravi          Created Class
**/
public class PartnerCategoryStatusDeleteController{

    public Partner_Category_Status__c ParCat;
    public string parAccount = '';
    
    public PartnerCategoryStatusDeleteController(ApexPages.StandardController stdController) {
        this.ParCat = (Partner_Category_Status__c)stdController.getRecord();
    }
    
    public pagereference init(){
        ParCat = [SELECT Id, Partner_Account__c FROM Partner_Category_Status__c WHERE Id =: ParCat.Id];
        parAccount = ParCat.Partner_Account__c;
        return new pagereference('/'+parAccount);  
    }
}