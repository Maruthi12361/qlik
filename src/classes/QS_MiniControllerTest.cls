/*******************************************
*
*       2019-04-04 AIN Test class for minimalistic controller for pages and component that needs less information
*
*******************************************/
@isTest
public with sharing class QS_MiniControllerTest {
    static testMethod void testMiniController() {
        QTTESTUtils.GlobalSetUp();

        QS_MiniController mc = new QS_MiniController();
        mc.GetOrganizationId();
        mc.GetLiveChatDeploymentId();

        string username = mc.UserName;
        string userid = mc.UserId;
        string firstName = mc.FirstName;
        mc.supportSidebar =true;
    }

}