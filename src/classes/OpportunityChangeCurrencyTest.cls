/*Change Log:
*   2014-07-10 Updated a condition of logging
    MTM 2014-11-28 CR# 17952
    MTM 20141203 CR# 19145
 *       2016-06-08    Andrew Lokotosh Commented Forecast_Amount fields line  149
******/
@isTest
public with sharing class OpportunityChangeCurrencyTest{

    static void setupCustomSettings() {
        QTCustomSettings__c setting = new QTCustomSettings__c();
        setting.Name = 'Default';
        setting.BoomiBaseURL__c = 'https://www.google.com';
        setting.BoomiToken__c = 'BoomiToken';
        insert setting;
    }

    static testMethod void testOpportunityChangeCurrency_NullParameters() {

        String bId = 'boomiId__test';       
        String message = OpportunityChangeCurrency.ChangeCurrency(null, null);
        
        //System.assert(message == '');
    }

    static testMethod void testOpportunityChangeCurrency_MockOpportunity(){
        //setupCustomSettings();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');

        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
        
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeCurr', objUser);
                
                RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];
                
                if (rt != null){                    
                    //Create and insert account and opp record type Qlikbuy II
					Test.startTest();
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                              
                    //Add license product
                    OpportunityLineItem oli = QTTestUtils.createSmallBusinessServerOppLineItem(opp, true, false);
                    
                    //Call ChangeCurrency to change the currency to EUR
                    //OpportunityChangeCurrency.ChangeCurrency(opp.Id, 'USD');
                     Test.stopTest();     
                    //Get opp atributes to check if method set the correct values, removed all OLIs and Quotes.
                    opp = [SELECT CurrencyIsoCode from Opportunity where Id= :opp.Id LIMIT 1];
                    List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where Id= :oli.Id LIMIT 1];
                    
                    System.debug('opp.CurrencyIsoCode : ' +opp.CurrencyIsoCode);
                    System.debug('0pp.olis count : ' + olis.size());
					
                   // System.Assert(opp.CurrencyIsoCode == 'EUR');
                    //System.Assert(olis.size() == 0);
                    
                    
                }
                else
                {
                    system.debug('Record type missing!');
                }
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);
        }
        
    }
    
    static testMethod void testOpportunityChangeCurrency_NoLineItems(){
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        //setupCustomSettings();
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
        
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeCurr', objUser);
                
                RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];
                
                if (rt != null){                    
                    //Create and insert account and opp record type Qlikbuy II
					Test.startTest(); 
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                    Test.stopTest(); 
                    opp = [SELECT License_Forecast_Amount__c from Opportunity where Id= :opp.Id LIMIT 1];                  
                    
                    //Call ChangeCurrency to change the currency to INR
                    OpportunityChangeCurrency.ChangeCurrency(opp.Id, 'INR');
                    
                    //Get opp atributes to check if method set the correct values, removed all OLIs and Quotes.
                    opp = [SELECT CurrencyIsoCode from Opportunity where Id= :opp.Id LIMIT 1];
                    List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where OpportunityId = :opp.Id LIMIT 1];
                    
                    System.debug('opp.CurrencyIsoCode : ' +opp.CurrencyIsoCode);
                    System.debug('0pp.olis count : ' + olis.size());

                    //System.Assert(opp.CurrencyIsoCode == 'INR');
                    //System.Assert(olis.size() == 0);
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);
        }  
        
    }
    
    static testMethod void testOpportunityChangeCurrency_NoQuote(){
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        //setupCustomSettings();
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
                           
                    
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeCurr', objUser);
                
                RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];
                
                if (rt != null){                    
                    //Create and insert account and opp record type Qlikbuy II
					Test.startTest();
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                    Test.stopTest();  
                    opp = [SELECT License_Forecast_Amount__c from Opportunity where Id= :opp.Id LIMIT 1];
                    opp.License_Forecast_Amount__c = 10000;
                    update opp;                   
                    
                    //Call ChangeCurrency to change the currency to INR
                    OpportunityChangeCurrency.ChangeCurrency(opp.Id, 'INR');
                    
                    //Get opp atributes to check if method set the correct values, removed all OLIs and Quotes.
                    opp = [SELECT CurrencyIsoCode from Opportunity where Id= :opp.Id LIMIT 1];
                    List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where OpportunityId = :opp.Id LIMIT 1];
                    
                    System.debug('opp.CurrencyIsoCode : ' +opp.CurrencyIsoCode);
                    System.debug('0pp.olis count : ' + olis.size());

                    //System.Assert(opp.CurrencyIsoCode == 'INR');
                   // System.Assert(olis.size() == 0);
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);
        } 
        
                           
    }
    

    static testMethod void testSetOppCurrencyByBoomi_MockOpportunity(){
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        //setupCustomSettings();
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
        
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeCurr', objUser);
                
                RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];
                
                if (rt != null){
                    //Create and insert account and opp record type Qlikbuy II
					Test.startTest();
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                    Test.stopTest();
					opp.INT_NetSuite_InternalID__c = '11';
                    update opp;
                                        
                    //Add license product
                    OpportunityLineItem oli = QTTestUtils.createSmallBusinessServerOppLineItem(opp, true, false);
                    
                    //Call SetOppCurrencyByBoomi to change the currency to EUR
                    
                    String message = OpportunityChangeCurrency.SetOppCurrencyByBoomi(opp.Id, 'EUR');
                    Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());
                    //System.Assert(message != '');                    
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);
        }
        
    }
    
    static testMethod void testSetOppCurrencyByBoomi_MockOpportunity_MissingNetsuiteIntID(){
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
        //setupCustomSettings();
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
        
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeCurr', objUser);
                
                RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];
                
                if (rt != null){
                    //Create and insert account and opp record type Qlikbuy II
                    Test.startTest();
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                    Test.stopTest();                   
                    //Add license product
                    OpportunityLineItem oli = QTTestUtils.createSmallBusinessServerOppLineItem(opp, true, false);
                    
                    //Call SetOppCurrencyByBoomi to change the currency to EUR
                    String message = OpportunityChangeCurrency.SetOppCurrencyByBoomi(opp.Id, 'EUR');
                    
                  //  System.Assert(message == '');                    
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);
        }
        
    }

}