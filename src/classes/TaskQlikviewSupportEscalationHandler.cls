/*
Name:		TaskQlikviewSupportEscalationHandler
Date: 		2017-11-28
Purpose: 	Moved logic from TaskUpdateQlikviewSupportEscalation.trigger
			Update Qlikview_Support_Escalation field "Last Customer Communication" to the current date and time 
				when an email is sent from the Escalation record.
History
------- 
VERSION AUTHOR      DATE        CR
1.0     MTM  2013-07-02  		CR#8287   https:https://eu1.salesforce.com/a0CD000000Z8JHe
*/
public class TaskQlikviewSupportEscalationHandler {
	
	public static void handle(List<Task> newTasks) {
		System.Debug('TaskUpdateQlikviewSupportEscalation: Starting');
        
	    List<Qlikview_Support_Escalation__c> escalationUpdateList = new List<Qlikview_Support_Escalation__c>();
	    Map<Id, DateTime> esacalationIdTimeMap = new Map<Id, DateTime>();
	    
	    //Get a map of EscalationId and LastModifiedDate 
	    for (Task t : newTasks) {        
	        esacalationIdTimeMap.put(t.WhatId, t.LastModifiedDate);
	    }
	    
	    //Get the corrsponding escalation record and update the last customer communication field.
	    for (Qlikview_Support_Escalation__c escalation: [select Id, Last_customer_communication__c  
	    												 from Qlikview_Support_Escalation__c e 
	    												 where e.Id IN: esacalationIdTimeMap.KeySet()]) {
	    	escalation.Last_customer_communication__c = esacalationIdTimeMap.get(escalation.Id);
	    	escalationUpdateList.Add(escalation);
	    }
	    
	    if (escalationUpdateList.Size() > 0) {
	    	update escalationUpdateList;
	    }
	    System.Debug('TaskUpdateQlikviewSupportEscalation: End');
	}

}