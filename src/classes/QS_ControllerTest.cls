/*
* Change log:
*
* 2018-01-18    ext_vos     CHG0032673: add test method for isCustomer/isIndirectCustomer logic.
* 2018-03-28    ext_vos     CHG0032295: add QS_SearchSettings__c definition for sttWebinarSearch.
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*/

@isTest
private class QS_ControllerTest {

    @testSetup static void testsetup() {
        QuoteTestHelper.createCustomSettings();
    }

    private static Id networkId;
    private static Id recTypeId;

    private static Case surveyToDelete;
    private static Attachment attachment;

    @isTest static void testProperties() {
        PageReference pageRef = Page.QS_Home_Page;
        Test.setCurrentPage(pageRef);

        system.debug('Current URL according to test: ' +ApexPages.currentPage().getURL());

        QS_Controller controller = new QS_Controller();
        string UserName = controller.UserName;
        string UserId = controller.UserId;

        Boolean unauthenticated = controller.unauthenticated;
        Boolean isPartnerAccount = controller.isPartnerAccount;

        String firstName = controller.FirstName;
    }

    @isTest
    private static void testSupportBlogRSSFeed()
    {

        User testUser = IsnsertTestData('');

        PageReference pageRef = Page.QS_CaseListPage;
        Test.setCurrentPage(pageRef);

        QS_ControllerHTTPMock mock = QS_ControllerHTTPMock.CreateHTTPMock(15);



        test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        QS_Controller controller = new QS_Controller();
        controller.loadSupportBlogFeed();
        test.stopTest();
    }
    /*@isTest
    private static void testInsertKB() {
        PageReference pageRef = Page.QS_Home_Page;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('q', 'searching...');
        QS_Controller controller = new QS_Controller();

        controller.insertKB();
    }*/

    @isTest
    private static void testIsCustomer() {
        Test.startTest();
        List<ULC_Level__c> levels = new List<ULC_Level__c>();

        ULC_Level__c level1 = new ULC_Level__c();
        level1.Name = 'CPLOG';
        level1.Status__c = 'Active';
        levels.add(level1);

        ULC_Level__c level2 = new ULC_Level__c();
        level2.Name = 'CPVIEW';
        level2.Status__c = 'Active';
        levels.add(level2);
        insert levels;

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if (searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Page_Size__c = 20;
            ss.Jive_DefaultPlaces__c ='/places/217522';
            ss.Jive_EndPoint__c ='https://community.qlik.com/api/core/v3/search/contents?startIndex=index&callback=?&count=pageSize&filter=search(searchTerms)';
            ss.Instance_Name__c = 'cs86';
            ss.Jive_EndPoint_Personas__c = 'https://community.qlik.com/api/core/v3/contents/trending?&callback=?&count=pageSize&startIndex=index&filter=place(placeTerms)';
            insert ss;
        }

        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        insert testContact;

        String profileId = [select id from Profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1].Id;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        List<Assigned_ULC_Level__c> assignedLevels = new List<Assigned_ULC_Level__c>();
        Assigned_ULC_Level__c assignedULCLevel1 = new Assigned_ULC_Level__c();
        assignedULCLevel1.ContactId__c = testContact.Id;
        assignedULCLevel1.Status__c = 'Approved';
        assignedULCLevel1.ULCLevelId__c = level1.Id;
        assignedLevels.add(assignedULCLevel1);

        Assigned_ULC_Level__c assignedULCLevel2 = new Assigned_ULC_Level__c();
        assignedULCLevel2.ContactId__c = testContact.Id;
        assignedULCLevel2.Status__c = 'Rejected';
        assignedULCLevel2.ULCLevelId__c = level2.Id;
        assignedLevels.add(assignedULCLevel2);
        insert assignedLevels;
        Test.stopTest();

        System.runAs(communityUser) {
            PageReference pageRef = Page.QS_Home_Page;
            Test.setCurrentPage(pageRef);
            QS_Controller controller = new QS_Controller();

            System.assert(!controller.isCustomer);
            System.assert(!controller.isIndirectCustomer);
        }

        testAccount.Navision_Status__c = 'Customer';
        update testAccount;

        System.runAs(communityUser) {
            PageReference pageRef = Page.QS_Home_Page;
            Test.setCurrentPage(pageRef);
            QS_Controller controller = new QS_Controller();

            System.assert(controller.isCustomer);
            System.assert(!controller.isIndirectCustomer);
        }
    }

    private static User IsnsertTestData(string persona)
    {
        Semaphores.SetAllSemaphoresToTrue();

        List<QS_SearchSettings__c> searchSettings = QS_SearchSettings__c.getall().values();
        if (searchSettings.Size() == 0) {
            QS_SearchSettings__c ss = new QS_SearchSettings__c();
            ss.Name = 'SearchSettings';
            ss.Page_Size__c = 20;
            ss.Jive_DefaultPlaces__c ='/places/217522';
            ss.Jive_EndPoint__c ='https://community.qlik.com/api/core/v3/search/contents?startIndex=index&callback=?&count=pageSize&filter=search(searchTerms)';
            ss.Instance_Name__c = 'cs86';
            ss.Jive_EndPoint_Personas__c = 'https://community.qlik.com/api/core/v3/contents/trending?&callback=?&count=pageSize&startIndex=index&filter=place(placeTerms)';
            insert ss;
        }

        QS_Search_Setting__c ss1 = new QS_Search_Setting__c();
        ss1.Name = 'Jive_Search';
        ss1.Active__c = true;
        insert ss1;

        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        //Create Contacts
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = persona;
        testContact.LeadSource = 'leadSource';
        insert testContact;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

        // Create Community Users
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        //Create Entitlement
        Entitlement productLicense = TestDataFactory.createLicense(testAccount.Id, 'licenseReference', '5302402020420');
        insert productLicense;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        //Create Environment
        Environment__c environment = TestDataFactory.createEnvironment(testAccount.Id, 'environmentName', 'operatingSystem', productLicense.Id, 'type', 'version');
        insert environment;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        Bugs__c bug = new Bugs__c();
        bug.Status__c ='Status 2';
        Insert bug;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        bug = [select Status__c from Bugs__c where id = :bug.id];
        System.debug('Bug.Status: ' + bug.Status__c);
        bug.Status__c = 'Status 2';
        update bug;

        bug = [select Status__c from Bugs__c where id = :bug.id];
        System.debug('Bug.Status: ' + bug.Status__c);

        system.debug('AIN ContactId: ' + testContact.Id);
        system.debug('AIN Contact.Account.Id: ' + testContact.Account.Id);
        system.debug('AIN AccountId: ' + testAccount.Id);

        Case caseObj = TestDataFactory.createCase('Test subject', 'Test description', communityUser.Id, '3',
                testContact.Id, testAccount.Id, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                false, null, environment.Id,
                productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);
        caseObj.Bug__c = bug.Id;
        //caseObj.Status = QS_Controller.STATUS_SOLUTION_SUGGESTED;

        insert caseObj;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        surveyToDelete = TestDataFactory.createCase('Test Subject Survey','Test description survey', communityUser.Id, '3',
                testContact.Id, testAccount.Id, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                false, null, environment.Id,
                productLicense.Id, productLicense.name, environment.Architecture__c, environment.Operating_System__c);

        surveyToDelete.Survey_Status_Response__c = 'Later';
        surveyToDelete.Performance_Comments__c = '';
        surveyToDelete.Performance_Communication__c  = '';
        surveyToDelete.Performance_Support_Entitlement__c  = '';
        surveyToDelete.Performance_Content_Utilization__c  = '';
        surveyToDelete.Performance_Case_Quality__c  = '';
        surveyToDelete.Performance_Troubleshooting__c  = '';

        insert surveyToDelete;
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());

        Blob b = Blob.valueOf('Test Data');
        attachment = new Attachment();
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = b;

        List<Bugs__History> bugHistoryList = [Select ParentId, Parent.Status__c, OldValue, NewValue, Id, Field, CreatedDate, CreatedById
        From Bugs__History
        Where Field = 'Status__c' ];
        //AND ParentId IN (Select Bug__c From Case
        //  where Status != :QS_Controller.STATUS_CLOSED AND Status != :QS_Controller.STATUS_RESOLVED AND
        //  Status != :QS_Controller.STATUS_COMPLETED AND Status != :System.Label.QS_CloseCase)];



        system.debug('bugHistoryList.size(): ' + bugHistoryList.size());

        for(Bugs__History bh : bugHistoryList)
        {
            system.debug('bh.OldValue: ' + bh.OldValue);
            system.debug('bh.NewValue: ' + bh.NewValue);
        }
        System.debug('Limits: ' + Limits.getQueries() + '/' + Limits.getLimitQueries());
        Semaphores.SetAllSemaphoresToFalse();
        return communityUser;
    }

    private static id getNetworkIdCustom() {
        if(networkId != null) {
            return networkId;
        } else {
            if(Network.getNetworkId() != null) {
                networkId = Network.getNetworkId();
            } else {
                User communityUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where UserType != 'Standard' AND ProfileId != null AND IsPortalEnabled = true AND IsActive = true AND ContactId != null AND AccountId != null AND Id IN (Select MemberId From NetworkMember where Network.Status = 'Live') LIMIT 1];
                networkId = communityUser.NetworkMemberUsers[0].NetworkId;
            }
            return networkId;
        }
    }
    // Get the Record Type for the case
    private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {

            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }


}