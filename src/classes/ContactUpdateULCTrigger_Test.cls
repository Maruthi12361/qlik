@isTest
public class ContactUpdateULCTrigger_Test {

    /******************************************************

        TEST_ContactUpdateULCTrigger
    
        This method makes unit test of the ContactUpdateULCTrigger
    
        Changelog:
            2015-11-26  SAN     Created method
            2016-05-25  CCE     Added Semaphore clearing as part of CR# 80053
            2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
			2018-03-06  CRW     Modified ulc username to avoid new validation error.BMW-508, line - 34,41,59
    ******************************************************/     

    static testMethod void TEST_ContactUpdateULCTrigger() {
                
        List<Contact> contacts = new List<Contact>();            
        Contact contact = new Contact(   FirstName = 'Test LeadCountryISOUpdate',
                                Email = 'asd@asd.com',
                                LastName = 'Lastname', 
                                QCloudID__c = 'Enterprise',
                                PartnerSourceNo__c = '12345'
                                );
       
        contacts.add(contact);
        
        
        // Do insert tests
        insert(contacts); 
        Semaphores.TriggerHasRun('ContactUpdateULCTrigger', 1);
        
        List<ULC_Details__c> ULCDetails = new List<ULC_Details__c>();
        ULC_Details__c ulc = new ULC_Details__c();
        ulc.ULCName__c = 'thecontactuser';
        ulc.ULCStatus__c = 'Active';
        ulc.ContactId__c = contacts[0].Id;
        ULCDetails.add(ulc);

        insert (ULCDetails);
        
        ulc = [select Id, ULC_Email__c, ULC_QCloudID__c, ULC_PartnerSourceNo__c, Toggle_dirty__c from ULC_Details__c where ULCName__c = 'thecontactuser'];
        System.assertEquals('Enterprise', ulc.ULC_QCloudID__c);
        System.assertEquals('asd@asd.com', ulc.ULC_Email__c);
        System.assertEquals('12345', ulc.ULC_PartnerSourceNo__c);
        System.assertEquals(false, ulc.Toggle_dirty__c);

        test.startTest();        
        Boolean toggle = ulc.Toggle_dirty__c;
        contact = [select Id, FirstName, LastName, QCloudID__c, Email, PartnerSourceNo__c from Contact where FirstName = 'Test LeadCountryISOUpdate'];
        
        contact.QCloudID__c = 'not enterprise';
        contact.Email = 'notasd@asd.com';
        contact.PartnerSourceNo__c = '54321';
        
        // Do update tests 
        Semaphores.ContactTriggerHandlerAfterUpdate = false;
        update(contact);

        ulc = [select Id, ULC_Email__c, ULC_QCloudID__c, ULC_PartnerSourceNo__c, Toggle_dirty__c  from ULC_Details__c where ULCName__c = 'thecontactuser'];
        System.assertEquals('not enterprise', ulc.ULC_QCloudID__c);
        System.assertEquals('notasd@asd.com', ulc.ULC_Email__c);
        System.assertEquals('54321', ulc.ULC_PartnerSourceNo__c);
        System.assertEquals(!toggle, ulc.Toggle_dirty__c);


        test.stopTest();
    }
}