/**
 * Ain  2017-03-10 Initial implementation
 * AIN  2017-11-15 Adding functionality to create new users even if no assigned ULC level is found
 * ext_bad 2018-04-18  CHG0033720   Change org wide email, select it from custom settings.
 * AIN Added support for RelayState to redirect
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 * 2019-06-15 AIN IT-1957 Added support for + in username
 */
public without sharing class QS_LandingPageController {
    public string CSSBaseURL {get;set;}
    public string SupportPortalURLBase {get;set;}
    public string InformationString {get;set;}
    public boolean hasCPLOG {get;set;}
    public boolean hasCPVIEW {get;set;}
    public string username {get;set;}
    public string status {get;set;}
    public Id contactId {get;set;}
    public boolean ShowLoading {get;set;}
    public string RelayState {get;set;}
    private boolean isDebug = false;

    private string ErrorMessageUnfixable;

    public QS_LandingPageController() {

        ShowLoading = isdebug;
        if(isDebug)
            InformationString = 'Test AIN sldkfj lsfdj lö sjfkl jfldösjflads löresjlöesjrklöjseglö röes lökresjglö sregl jrlestg jlösrej löesrj löjres ljres ljreslö jslre lsre lrse lj';
        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        CSSBaseURL = partnerPortalURLs.Support_Portal_CSS_Base__c;
        SupportPortalURLBase = partnerPortalURLs.Support_Portal_Url_Base__c  + '?sfdcLogin=true';

        hasCPLOG = false;
        hasCPVIEW = false; 
    }

    public PageReference redirect(PageReference pageRef) {
        pageRef.setRedirect(true);
        return pageRef;
    }
    public PageReference redirect(string url) {
        PageReference pageRef = null;
        if(url != null && url != '')
            pageRef = new PageReference(url);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public PageReference redirectToQlikId() {
        QS_Partner_Portal_Urls__c urls = QS_Partner_Portal_Urls__c.getInstance();
        string url = urls.Support_Portal_Url_Base__c;
        if(url == null || url == '')
            url = 'http://qlikid.qlik.com/portal/support';
        url += '?sfdcLogin=true';
        if(relaystate != null && relaystate != '')
            url += '&relaystate=' + RelayState;
        return redirect(url);
    }
    public PageReference redirectToSupportPortal() {
        return redirect(Page.QS_Home_Page);
    }
    public PageReference redirectToSupportPortalWithRedirect() {
        try{
            system.debug('Start redirectToSupportPortalWithRedirect');
            system.debug('RelayState: ' + relaystate);
            if(RelayState != null && RelayState != '')
            {
                if(!RelayState.startsWith('http')) {
                    QS_Partner_Portal_Urls__c urls = QS_Partner_Portal_Urls__c.getInstance();
                    string baseUrl = urls.Support_Portal_Login_Url__c;
                    return redirect(baseUrl + '/' + RelayState);
                }
                else {
                    return redirect(RelayState);
                }
            }
            else
                return redirect(Page.QS_Home_Page);
        }
        catch (Exception ex){
            return redirect(Page.QS_Home_Page);   
        }
    }
    public PageReference CheckULC() {
        RelayState = '';
        if(isDebug)
            return null;
            
        username = ApexPages.currentPage().getParameters().get('username');
        RelayState = ApexPages.currentPage().getParameters().get('relaystate');

        if(username != null && username != '')
            return CheckULCStatus(username);
        else {
            InformationString = 'No username specified, redirecting to home page';
            return redirectToSupportPortal();
        }
    }

    public PageReference CheckULCStatus(string ulcName) {

        string CaseWizardURL = 'QS_CaseWizard.page';

        ErrorMessageUnfixable = 'We could not verify your access at this time.  Please submit a case to Customer Support together with your username';


        //isSupportedByQlik

        system.debug('CheckULCStatus start');
        system.debug('Username: ' + ulcName);
        ulcname = ulcname.replace(' ', '+');

        //We now check everyone
        /*if(UserInfo.getUserId() != '005D0000004wTRsIAM'){
            InformationString = 'User is logged in correctly, redirect to home page';
            return redirect(Page.QS_Home_Page);
        }*/

        List<ULC_Details__c> details = [select id, ContactId__r.TriggerPortalControl__c, ContactId__r.Left_Company__c, ULCName__c, ContactId__c, ContactId__r.Account.Navision_Status__c, ContactId__r.AccountId, ContactId__r.Id from ULC_Details__c where ULCName__c = :ulcName];
        if(details.size() == 0){
            //ULC Detail not found
            InformationString = 'ULC Detail not found';
            return redirectToSupportPortal();
        }
        ULC_Details__c detail = details[0];


        if(detail.ContactId__c == null) {
            //ULC is a lead, redirect to login info
            return redirect(Page.QS_LoginInfo);
        }
        contactId = detail.ContactId__r.Id;

        boolean isPartner = detail.ContactId__r.Account.Navision_Status__c == 'Partner';
        boolean isCustomer = detail.ContactId__r.Account.Navision_Status__c == 'Customer';

        if(UserInfo.getUserId() != '005D0000004wTRsIAM' && isPartner){
            //User is a partner is already logged in, redirect to home page
            InformationString = 'User is a partner and is already logged in, redirect to home page';
            return redirectToSupportPortalWithRedirect();
        }

        if(detail.ContactId__r.Left_Company__c == true) {
            //Contact has left company, redirect to home page
            return redirectToSupportPortal();
        }
        if(detail.ContactId__r.AccountId == '0012000000I7QJhAAN') {
            //User is on the QlikTech Single Signon Hold Account, redirect to home page
            InformationString = 'User is on the QlikTech Single Signon Hold Account, redirecting to home page';
            return redirectToSupportPortal();
        }

        if(!isPartner && !isCustomer){
            //User is not partner or customer, redirect to home page
            InformationString = 'Account ERP Status is not Customer or Partner, redirecting to homepage';
            return redirectToSupportPortal();
        }
        boolean ShouldSwitch = false;
        boolean isSupportedByQlik = isSupportedByQlik(detail.ContactId__r.AccountId);

        system.debug('Looking up ulc levels for contact: ' + detail.ContactId__c);
        List<Assigned_ULC_Level__c> levels = [select id, ULCLevelId__r.Name, Status__c from Assigned_ULC_Level__c where ContactId__c = :detail.ContactId__c];
        List<Assigned_ULC_Level__c> assignedCPLogLevels = new List<Assigned_ULC_Level__c>();
        List<Assigned_ULC_Level__c> assignedCPViewLevels = new List<Assigned_ULC_Level__c>();
        system.debug('Assigned ULC Levels.Size: ' + levels.Size());
        for(Assigned_ULC_Level__c level : levels) {
            system.debug('Name: ' + level.ULCLevelId__r.Name);
            if(level.ULCLevelId__r.Name == 'CPLOG') {
                if(level.Status__c == 'Approved')
                    hasCPLOG = true;
                assignedCPLogLevels.add(level);
            }
            else if(level.ULCLevelId__r.Name == 'CPVIEW') {
                if(level.Status__c == 'Approved')
                    hasCPVIEW = true;
                assignedCPViewLevels.add(level);   
            }
        }

        if(UserInfo.getUserId() != '005D0000004wTRsIAM')
        {
            if(isSupportedByQlik && hasCPLOG){
                //User is supported by Qlik, already has CPLOG and his user is active, we don't need to modify this user
                InformationString = 'User is supported by Qlik, already has CPLOG and his user has Customer Portal Case Logging Access Profile, we don\'t need to modify this user';
             
                return redirectToSupportPortalWithRedirect();
            }
            else if(!isSupportedByQlik && hasCPVIEW){
                //User is not supported by Qlik, already has CPVIEW and his user is active, we don't need to modify this user
                InformationString = 'User is not supported by Qlik, already has CPVIEW and his user has Customer Portal Case Viewing Access Profile, we don\'t need to modify this user';
                return redirectToSupportPortalWithRedirect();
            }
            else
                ShouldSwitch = true;
        }

        

        List<User> users = [select id, isActive, FederationIdentifier, CommunityNickname, ContactId, ProfileId from user where ContactId = :detail.ContactId__c or FederationIdentifier = :ulcName or CommunityNickname  = :ulcName];

        //Found no user or user needs to switch assigned ULC Level, user can be safely created
        if(users.size() == 0 || ShouldSwitch) {
            system.debug('No user found');

            //system.debug('checking if the user had ')
            //if(!hasCPLOG && !hasCPVIEW) { 
                //Returning to home page as contact doesn't CPLOG or CPVIEW already as per CR CHG0031290 https://qliktech.service-now.com/nav_to.do?uri=%2Fchange_request.do%3Fsys_id%3D6f1d672edb1afac02b66f1951d9619c4%26sysparm_stack%3D%26sysparm_view%3Dit%26sysparm_view_forced%3Dtrue
                //We might later reuse the code below
                //return redirect(Page.QS_Home_Page);
            //}

            if(detail.ContactId__r.TriggerPortalControl__c == true) {
                Contact c = [select TriggerPortalControl__c from Contact where Id =: detail.ContactId__c];
                c.TriggerPortalControl__c = false;
                update c;
            }
            
            List<Assigned_ULC_Level__c> levelsToDelete = new List<Assigned_ULC_Level__c>();
            List<Assigned_ULC_Level__c> levelsToCreate = new List<Assigned_ULC_Level__c>();

            levelsToDelete.addall(assignedCPViewLevels);
            levelsToDelete.addall(assignedCPLogLevels);
            if(isSupportedByQlik || detail.ContactId__r.Account.Navision_Status__c == 'Partner'){
                system.debug('User is either Partner or supported by us, applying CPLOG');      
                //Add CPLOG
                Assigned_ULC_Level__c level = AssignULCLevelToContact(detail.ContactId__c, 'CPLOG');
                if(level != null)
                    levelsToCreate.add(level);
            }
            else{
                //Add CPVIEW
                Assigned_ULC_Level__c level = AssignULCLevelToContact(detail.ContactId__c, 'CPVIEW');
                if(level != null)
                    levelsToCreate.add(level);
            }
            if(levelsToDelete.size() > 0)
                delete levelsToDelete;
            if(levelsToCreate.size() > 0) {
                insert levelsToCreate;
                status = 'CreatingPortalUser';
                ShowLoading = true;
                InformationString = 'The access rights for your account are being updated. This one time setup can take a few minutes, we appreciate your patience.';
            }
        }
        else {
            system.debug('At least one user found');
            boolean foundActiveUser = false;
            boolean federationIdentifierExists = false;
            boolean communityNicknameExists = false;
            boolean contactOnActiveUser = false;
            boolean foundThisUser = false;
            User activeUser = null;
            User inactiveUser = null;

            for(User u : users) {
                if(u.CommunityNickname != null && u.CommunityNickname  == ulcName)
                    communityNicknameExists = true;
                if(u.FederationIdentifier != null && u.FederationIdentifier == ulcName)
                    federationIdentifierExists  = true;
                if(u.isActive) {
                    foundActiveUser = true;
                    activeUser = u;
                    if(u.ContactId != null && u.ContactId == detail.ContactId__c)
                        contactOnActiveUser = true;
                }
                else{
                    if(u.FederationIdentifier != null && u.FederationIdentifier == ulcName && u.CommunityNickname != null && 
                        u.CommunityNickname  == ulcName && u.ContactId != null && u.ContactId == detail.ContactId__c)
                        inactiveUser = u;
                }
            }
            
            if(foundActiveUser){
                system.debug('Active user found');
                if(contactOnActiveUser){

                    system.debug('The user we found is the user trying to login, redirect back to QlikId to login the user.');
                    //The user we found is the user trying to login, redirect back to QlikId to login the user.
                    return redirectToQlikId();
                }
                else {
                    system.debug('We found a user that either has the correct federationId, the correct nickname or the correct contact');
                    //We found a user that either has the correct federationId, the correct nickname or the correct contact
                    //InformationString = 'Qlikid is already in use, this can\'t be resolved automatically, please submit a case and provide your username for manual reactivation.';
                    InformationString = ErrorMessageUnfixable;
                    Status = 'Unresolvable';
                    return null;
                }
            }
            //Inactive user found
            else if(inactiveUser != null){
                system.debug('Inactive user found');
                //Inactive user with the same federation id, nickname and contactid found
                if(contactOnActiveUser) {
                    system.debug('Active user with the same federation id, nickname or contactid found');
                    //InformationString = 'Qlikid is already in use, this can\'t be resolved automatically, please submit a case and provide your username for manual reactivation.';
                    InformationString = ErrorMessageUnfixable;
                    Status = 'Unresolvable';
                    return null;
                }

                if(!CheckLicenses('Gold Partner')){
                    //No Gold Licenses left, can't reactivate
                    return redirectToSupportPortal();
                }

                InformationString = 'User is deactivated, reactivating user';
                inactiveUser.isActive = true;
                system.debug('User is deactivated, reactivating user');

                try{
                    update inactiveUser;
                    return redirectToQlikId();
                }
                catch(DmlException ex){
                    InformationString = 'Failed to activate user.';
                    SendEmailToDev('Support Portal User Reactivation Error','Failed to activate user: ' + ex.getMessage());
                    Status = 'Unresolvable';
                }
            }
            else{
                //We found a user that either has the correct federationId, the correct nickname or the correct contact
                system.debug('Qlikid is already in use, this can\'t be resolved automatically, please submit a case and provide your username for manual reactivation.');
                //InformationString = 'Qlikid is already in use, this can\'t be resolved automatically, please submit a case and provide your username for manual reactivation.';
                InformationString = ErrorMessageUnfixable;
                Status = 'Unresolvable';
                return null;   
            }
        }
        system.debug('CheckULCStatus end');
        return null;
    }

    public void checkStatusController() {
        system.debug('checkStatusController start');
        if(status == 'CreatingPortalUser') {
            system.debug('Checking if Portal User is created');
            List<User> users = [select id from user where ContactId = :contactId and IsActive = true];
            if(users.size() == 0) {
                system.debug('No user found! Will check again in 15 seconds');
            }
            else {
                system.debug('User found! We can now redirect to QlikId');
                status = 'UserCreated';
            }
        }
        system.debug('checkStatusController ebd');
    }
    public void stopCheckingStatus() {
        system.debug('stopCheckingStatus start');
        if(status == 'CreatingPortalUser') {
            //InformationString = 'User access was given but user wasn\'t created after 5 minutes. Please refresh the page to try again or use the toolbar above to enter as guest.';
            //string link = '<a href="'+Page.QS_CaseDetails.getUrl()+'">' + 'Here</a>';
            InformationString= 'Access rights could not be granted for this account. Please refresh this page or contact our Customer Support team';
            status = 'FailedToCreateUser';
            ShowLoading = false;
        }
        system.debug('stopCheckingStatus end');
    }
    private Assigned_ULC_Level__c AssignULCLevelToContact(Id contactId, string ulcLevel) {
        List<ULC_Level__c> levels = [select id from ULC_Level__c where Name = :ulcLevel];
        if(levels.size() == 0){
            //No ULC Level found, abort
            return null;
        }
        Assigned_ULC_Level__c assignedULCLevel = new Assigned_ULC_Level__c();
        assignedULCLevel.ULCLevelId__c = levels[0].Id;
        assignedULCLevel.Status__c = 'Approved';
        assignedULCLevel.ContactId__c = contactId;
        return assignedULCLevel;
    }
    public void SendEmailToDev(string subject, string body){
        SendEmail('ain@qlikview.com', subject, body);
    }
    private void SendEmail(string email, string subject, string body){
        String noReplyAddress = '';
        if(QTCustomSettings__c.getInstance('Default') != null) {
            noReplyAddress = QTCustomSettings__c.getInstance('Default').QlikNoReplyEmailAddress__c;
        }
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = :noReplyAddress];
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        if ( owea.size() > 0 )
           message.setOrgWideEmailAddressId(owea.get(0).Id);
        // Set recipients to two contact IDs.
        // Replace IDs with valid record IDs in your org.
        message.toAddresses = new String[] { 'ain@qlik.com'};
        message.subject = subject;
        message.plainTextBody = body;
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }

    }
    public boolean CheckLicenses(string licenseName) {

        return true;
        List<UserLicense> licenses = [select id, Name, UsedLicenses, Totallicenses from UserLicense where Name = :licenseName];
        if(licenses.size() == 0)
            return false;
        
        Integer UsedLicenses = licenses[0].UsedLicenses;
        Integer Totallicenses = licenses[0].Totallicenses;
        if(licenses[0].UsedLicenses >= licenses[0].Totallicenses){
            InformationString = 'No licenses left!';
            SendEmailToDev('No licenses left for ' +licenseName, 'No licenses left to assign in Support Portal.');
            return false;
            
        }
        return true;
    }
    private boolean isSupportedByQlik(string accountId) {

        Integer qlikSupport = 0;

        /*for (AggregateResult aggRes : 
            [select count(id) countAcc from Contract 
            where 
            (Status_Formula__c = 'Active' or 
            Status_Formula__c = 'Renewal Processed') and 
            Support_Provided_By__c != :accountId and  
            Support_Provided_By__r.Name != 'Qlik' and 
            Support_Provided_By__r.Name != '' and 
            AccountId = :accountId])*/

        for (AggregateResult aggRes : [select count(id) countAcc from Contract 
            where 
            (Status_Formula__c = 'Active' or 
            Status_Formula__c = 'Renewal Processed' or
            Status_Formula__c = 'Renewal Generated') and 
            Support_Provided_By__r.Name = 'Qlik' and 
            AccountId = :accountId])
            
            qlikSupport = (Integer) aggRes.get('countAcc');

        return true;
 
        return (qlikSupport > 0);
    }
}