/***************************************
Class: SetDoNotFollowUpFlagsOnContact
Description: Called from Flow (Manually Add ULC) to update the Do not follow fields.
             (Using code so that users without FLS for these fields can still use the Add ULC button) 

Changes Log:
2018-05-24	CCE		Initial code: BMW-848 CHG0033967
					
****************************************/
public class SetDoNotFollowUpFlagsOnContact {
	@InvocableMethod(label='Set Do Not Follow Up Flags' description='Sets the Do not Follow Up flags for the specified Contact IDs.')
	public static void setDoNotFollowUpFlags(List<ID> ids) {
		List<Contact> contacts = [SELECT DoNotCall, Marketing_Suspended__c, Marketing_Suspended_Date__c, Marketing_Suspended_Reason__c FROM Contact WHERE Id in :ids];
		for (Contact con : contacts) {
			con.Marketing_Suspended__c = true;
			con.Marketing_Suspended_Date__c = Date.today();
			con.Marketing_Suspended_Reason__c = 'User did not accept T/Cs yet (Add ULC button)';
			con.DoNotCall = true;
		}
		if (contacts.size() > 0) {
			update contacts;
		}
	}
}