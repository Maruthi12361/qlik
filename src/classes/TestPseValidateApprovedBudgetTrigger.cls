@isTest
private class TestPseValidateApprovedBudgetTrigger {

    // 2017-11-09  ext_vos Methods were moved to PseProjectTriggerTest
    // to be deleted
    
    /*static testMethod void testInsertInvalidProjects() {
        List<pse__Proj__c> testProjects = new List<pse__Proj__c>();
        for (Integer i = 0; i < 20; i++) {
            pse__Proj__c invalidProject = new pse__Proj__c(
                Name = 'Test Project ' + i, 
                pse__Is_Billable__c = true, 
                pse__Is_Active__c = true,
                pse__Stage__c = 'In Progress',
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                Invoicing_Type__c = 'Deferred');
            testProjects.add(invalidProject);
        }
        try {
            insert testProjects;
        } catch (DmlException e) {
            // Assert error message
            System.assert(e.getMessage().contains('Cannot mark a project as \'Billable\''), e.getMessage());
            System.assertEquals(e.getDmlStatusCode(0), 'FIELD_CUSTOM_VALIDATION_EXCEPTION');
            System.debug(e);
        }
    }

    static testMethod void testUpdateToInvalidProjects() {
        List<pse__Proj__c> testProjects = new List<pse__Proj__c>();
        for (Integer i = 0; i < 20; i++) {
            pse__Proj__c invalidProject = new pse__Proj__c(
                Name = 'Test Project ' + i, 
                pse__Is_Billable__c = false, pse__Is_Active__c = true, 
                pse__Allow_Timecards_Without_Assignment__c = true,
                pse__Stage__c = 'In Progress',
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                Invoicing_Type__c = 'Deferred');
            testProjects.add(invalidProject);
        }
        insert testProjects;

        // Update projects incorrectly
        for (pse__Proj__c invalidProject : testProjects) {
            invalidProject.pse__Is_Billable__c = true;
        }
        try {
            update testProjects;
        } catch (DmlException e) {
            // Assert error message
            System.assert(e.getMessage().contains('Cannot mark a project as \'Billable\''));
            System.assertEquals(e.getDmlStatusCode(0), 'FIELD_CUSTOM_VALIDATION_EXCEPTION');
            System.debug(e);
        }
    }*/
}