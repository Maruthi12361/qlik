// CR# 10681
// controller class to show products from the parent opportunity in a deal split child opportunity
// Change log:
// January 14, 2014 - Initial Implementation - Madhav Kakani - Fluido Oy
// March 22, 2017   - Roman Dovbush (4front) - Query changed to select other fields.

public with sharing class OpportunityParentProductsCtrl {
    private Opportunity opp;
    public List<OpportunityLineItem> lItems {get;set;}
    public Boolean showItems {get;set;}
    
    public OpportunityParentProductsCtrl(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();

        if(opp.Deal_Split_Parent_Opportunity__c != null) {
            // old soql as of March 21, 2017
            //lItems = [SELECT Name__c, Product_Family__c, Quantity, NS_List_Price__c, Discount__c,
            //        Total_Net_before_Partner_Margin__c, Partner_Margin_RO__c, UnitPrice, TotalPrice,
            //         Deal_Split_Price__c, description, net_before_partner_margin_NS__c
            //        FROM OpportunityLineItem WHERE OpportunityId = :opp.Deal_Split_Parent_Opportunity__c order by name__c asc];
            
            // new soql starting from March 22, 2017
            lItems = [SELECT Name__c, Product_Family__c, Product_Group_From_Quoteline__c, 
                    SBQQ__QuoteLine__r.Product_Name__c, SBQQ__QuoteLine__r.Customer_Unit_Price_RO__c, SBQQ__QuoteLine__r.Extended_CUP_RO__c, 
                    SBQQ__QuoteLine__r.Extended_CUP_AD_RO__c, Currency_Text__c, List_Unit_Price_RO__c, Additional_Discount_QuoteLine__c, Partner_Margin_RO__c, Net_Price__c, TotalPrice,
                    SBQQ__QuoteLine__r.Additional_Discount_Perc__c, Distributor_Discount__c, Deal_Split_Price__c
                    FROM OpportunityLineItem WHERE OpportunityId = :opp.Deal_Split_Parent_Opportunity__c];

            if(!lItems.isEmpty()) showItems = true;
            for(Integer i = 0; i < lItems.size(); i++) {
                lItems[i].Deal_Split_Price__c = lItems[i].TotalPrice * opp.Deal_Split_Percentage__c / 100.0;
            }
        }
    }

}