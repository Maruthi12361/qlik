/**
*	27.03.2017 Rodion Vakulovskyi   add logic for Test.isRunningTest();
**/
global class QuoteSharingRecalculation implements Database.Batchable<sObject>{
  public static final String emailAddress = 'mtm@qlik.com';
  public static final String OEMENDUSER = 'OEM_End_User_Account';

  global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT id, Short_Description__c, Name, SBQQ__Account__c, SBQQ__Account__r.Name, SBQQ__Account__r.RecordTypeId, Sell_Through_Partner__c, OwnerId FROM SBQQ__Quote__c]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
	System.debug('executeSharingBatch');
      Map<ID, SBQQ__Quote__c> quoteMap = new Map<ID, SBQQ__Quote__c>((List<SBQQ__Quote__c>)scope);
      List<SBQQ__Quote__Share> newQuoteShare = new List<SBQQ__Quote__Share>();
      Map<String, String> mapOfOwnersAcc = new Map<String, String>();
      List<Id> listOfOwnersAcc = new List<Id>();
      Map<Id, Id> quoteOemEndUserAccount = new Map<Id, Id>();
      Map<Id, Id> quoteToSellThroughPartner = new Map<Id, Id>();
      Map<Id, Id> quoteToOwner = new Map<Id, Id>();
      Set<Id> uniqueAccIds = new Set<Id>();
      Map<Id, Id> accountToRole = new Map<Id,Id>();
        Set<Id> uniquePortalRoles = new Set<Id>();
        List<Id> portalRoleIds = new List<Id>();
    System.debug('Number of quotes = ' + scope.size());

        RecordType rType = [Select id, DeveloperName From RecordType where DeveloperName =: OEMENDUSER];
        //Doing loop for filling collections to get UserRole and Role in groups for sharing
      for (SBQQ__Quote__c itemQuote : quoteMap.values()) {
            listOfOwnersAcc.add(itemQuote.OwnerId);
			System.debug(itemQuote);
            if (!String.isBlank(itemQuote.Sell_Through_Partner__c)) {
                if (!uniqueAccIds.contains(itemQuote.Sell_Through_Partner__c)) {
                    uniqueAccIds.add(itemQuote.Sell_Through_Partner__c);
          System.debug('Sell_Through_Partner__c added = Quote Id =' + itemQuote.Id + 'Partner ' + itemQuote.Sell_Through_Partner__c);
                }

                if (!quoteToSellThroughPartner.containsKey(itemQuote.Id)) {
                    quoteToSellThroughPartner.put(itemQuote.Id, itemQuote.Sell_Through_Partner__c);
                }
            }
            //quote owner account 
            if (itemQuote.OwnerId != null && mapOfOwnersAcc.containsKey(itemQuote.OwnerId) && !String.isEmpty(mapOfOwnersAcc.get(itemQuote.OwnerId))) {
                if (!uniqueAccIds.contains(mapOfOwnersAcc.get(itemQuote.OwnerId))) {
                    uniqueAccIds.add(mapOfOwnersAcc.get(itemQuote.OwnerId));
                }
                if (!quoteToOwner.containsKey(itemQuote.id)) {
                    quoteToOwner.put(itemQuote.id, mapOfOwnersAcc.get(itemQuote.OwnerId));
          System.debug('quoteToOwner = Quote Id =' + itemQuote.Id + 'owner acct Id ' + mapOfOwnersAcc.get(itemQuote.OwnerId));
                }
            }
            //OEM end user Account
            if (itemQuote.SBQQ__Account__r.RecordTypeId == rType.id && !String.isBlank(itemQuote.SBQQ__Account__c)) {
                if (!uniqueAccIds.contains(itemQuote.SBQQ__Account__c)) {
                    uniqueAccIds.add(itemQuote.SBQQ__Account__c);
                }
                if (!quoteOemEndUserAccount.containsKey(itemQuote.Id)) {
                    quoteOemEndUserAccount.put(itemQuote.Id, itemQuote.SBQQ__Account__c);
                }
            }
        }
        //end of loop
        //quering owner account ids of quotes
        for(User userItem: [select id, AccountId From User where id =: listOfOwnersAcc]) {
          mapOfOwnersAcc.put(userItem.id, userItem.AccountId);
        }

        // retrieving User Roles
        List<UserRole> userRoles = ApexSharingRules.GetListOfUserRolesByPortalAccountIds(uniqueAccIds, 'Partner', 'Executive');
        System.debug('Number of userRoles = ' + userRoles.size());
        for (UserRole usrRole :userRoles) {
            if (!uniquePortalRoles.contains(usrRole.Id)) {
                uniquePortalRoles.add(usrRole.Id);
                portalRoleIds.add(usrRole.Id);
            }
            accountToRole.put(usrRole.PortalAccountId, usrRole.Id);
        }
    
    System.debug('Number of accountToRole = ' + accountToRole.size());
    //retrieving role in groups
        Map<Id,Id> roleToGroup = new Map<Id,Id>();

        List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(portalRoleIds, 'RoleAndSubordinates');
        for (Group grp : groups) {
            roleToGroup.put(grp.RelatedId, grp.Id);
        }
      
    System.debug('Number of roleToGroup = ' + roleToGroup.size());
       
        if(Test.isRunningTest()) {
			//throw new stException();
		}
        //quering old sharing records for quotes
      List<SBQQ__Quote__Share> oldQuoteShrs = [SELECT Id FROM SBQQ__Quote__Share WHERE ParentId IN :quoteMap.keySet() AND (RowCause = :Schema.SBQQ__Quote__Share.rowCause.Sell_Through_Partner__c)];

    

       for(SBQQ__Quote__c quoteItem : quoteMap.values()){
         //QuoteSellThroughPartner Recalculating
         if(!String.isBlank(quoteItem.Sell_Through_Partner__c) && quoteToSellThroughPartner.containsKey(quoteItem.Id)){
           Id stPartnerRole = accountToRole.get(quoteItem.Sell_Through_Partner__c);
           if (roleToGroup.containsKey(stPartnerRole)) {
                newQuoteShare.add(createQuoteSharingRecord(roleToGroup.get(stPartnerRole), quoteItem.Id));
              }
          }
            //QuoteAccOwnerforOEM Recalculating
            if(quoteItem.SBQQ__Account__r.RecordTypeId == rType.id && !String.isBlank(quoteItem.SBQQ__Account__c) && quoteOemEndUserAccount.containsKey(quoteItem.Id)) {
              Id stPartnerRole = accountToRole.get(quoteOemEndUserAccount.get(quoteItem.Id));
              if (roleToGroup.containsKey(stPartnerRole)) {
                newQuoteShare.add(createQuoteSharingRecord(roleToGroup.get(stPartnerRole), quoteItem.Id));
              }
          }
            //Owner accountId Recalculating
            if (quoteItem.OwnerId != null && mapOfOwnersAcc.containsKey(quoteItem.OwnerId) && !String.isEmpty(mapOfOwnersAcc.get(quoteItem.OwnerId)) && quoteToOwner.containsKey(quoteItem.Id)) {
              Id stPartnerRole = accountToRole.get(quoteToOwner.get(quoteItem.Id));
              if (roleToGroup.containsKey(stPartnerRole)) {
                newQuoteShare.add(createQuoteSharingRecord(roleToGroup.get(stPartnerRole), quoteItem.Id));
              }
            }
        }
        //process delete of old sharing rules section 
		
			try {
			
				Delete oldQuoteShrs;
				if(Test.isRunningTest()) {
				newQuoteShare.add(createQuoteSharingRecord(UserInfo.getUserId(), quoteMap.values()[0].id));
				newQuoteShare[0].AccessLevel = null;
					if(quoteMap.values()[0].SBQQ__Account__r.Name == 'Exception') {
						throw new DmlException();
					}	
				}
				Database.SaveResult[] lsr = Database.insert(newQuoteShare,false);
				
				for(Database.SaveResult sr : lsr){
				
					if(!sr.isSuccess()){
						Database.Error err = sr.getErrors()[0];
						if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                     &&  err.getMessage().contains('AccessLevel'))){
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							String[] toAddresses = new String[] {emailAddress}; 
							mail.setToAddresses(toAddresses); 
							mail.setSubject('Apex Sharing Recalculation Exception');
							mail.setPlainTextBody(
							'The Apex sharing recalculation threw the following exception: ' + 
									err.getMessage());
							Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
						}
					}
				}   
			} catch(DmlException e) {
           // Send an email to the Apex job's submitter on failure.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {emailAddress}; 
            mail.setToAddresses(toAddresses); 
            mail.setSubject('Apex Sharing Recalculation Exception');
            mail.setPlainTextBody(
              'The Apex sharing recalculation threw the following exception: ' + 
                        e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
    }

    global void finish(Database.BatchableContext BC){
      //sending email to admin
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailAddress}; 
        mail.setToAddresses(toAddresses); 
        mail.setSubject('Apex Sharing Recalculation Completed.');
        mail.setPlainTextBody
                      ('The Apex sharing recalculation finished processing');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }

    global SBQQ__Quote__Share createQuoteSharingRecord(Id roleToGroup, Id quoteId) {
      SBQQ__Quote__Share quoteSharingRecord = new SBQQ__Quote__Share();
        quoteSharingRecord.UserOrGroupId = roleToGroup;
        quoteSharingRecord.AccessLevel = 'Edit';
        quoteSharingRecord.ParentId = quoteId;
        quoteSharingRecord.RowCause = Schema.SBQQ__Quote__Share.RowCause.Sell_Through_Partner__c;
    return quoteSharingRecord;
    }

	public class stException extends Exception{}

}