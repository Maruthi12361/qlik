public with sharing class CreateProblemCaseContExt
{
    private final Case c;
    public CreateProblemCaseContExt(ApexPages.StandardController sc)
    {
        this.c = (Case)sc.getRecord();
    }
    //Methods
	public PageReference initPage()
	{
    	PageReference pr = null;
        try
        {       
            CaseModel cm = new CaseModel(c);
            //Check if Case has valid Id
            if (!cm.userHasPermission) {
            	
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You do not have permission to Access this functionality, Please contact your Administrator if you believe you should.'));
                return null;
                
            } else if(cm.hasValidId) {
            	
            	Id newProblemCaseId = CaseServices.createProblemCase(c);
            	CaseServices.updateProblemCaseField(c, newProblemCaseId);
            	pr = new PageReference('/' + newProblemCaseId + '/e?retURL=/' + c.Id + '&saveURL=/' + newProblemCaseId);
            } else {
            	
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This is not a valid case record!'));
                return null;
            }
        }       
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getTypeName() + ': ' + e.getMessage()));
            return null;
        }
    	return pr;
	}
}