/********************************************************
* CLASS: WebActivityRetryTest
* DESCRIPTION: Test class for class WebActivityRetry
*
* CHANGELOG:    
*	2018-10-29 - CRW - creating the test class for WebActivityRetry batch process
*********************************************************/
@isTest
private class WebActivityRetryTest {
    
    @isTest
    public Static void invokeBatch(){
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(mockSysAdmin){
            List<Web_Activity__c> lstWA = new List<Web_Activity__c>();
            
            QlikTech_Company__c qc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
            qc.Country_Name__c = 'Sweden';
            qc.QlikTech_Region__c = 'NORDIC';
            qc.QlikTech_Sub_Region__c = 'NORDIC';
            qc.QlikTech_Operating_Region__c = 'N EUROPE';
            qc.Language__c = 'Swedish';
            update qc;
            
            Account acc = QTTestUtils.createMockAccount('Test Account', mockSysAdmin, false);
			acc.Navision_Status__c = 'Partner';
			insert acc;
			system.assert(acc.Id != null);
		

	        Lead lead = new Lead(
		        LastName = 'LName',
		        Company = 'Cmpany', 
		        IsUnreadByOwner = True,
		        Country='Sweden',
		        Email='Lead@test.com.sandbox',
		        Phone='5476767676'
	        	);
	        insert lead;
	        


	        Campaign camp1 = new Campaign(Name = 'Test Campaign', IsActive = True);
	        insert camp1;
			QTCustomSettings__c setting = QTTestUtils.createQTCustomSettingsTestDataForLive();
            setting.ULC_QT_SSO_Account__c = acc.Id;
        	update setting;
	        WebActivitySettings__c csWebActivity = new WebActivitySettings__c();
	        csWebActivity.QSDTaskSubject__c = 'Initial Log In – Qlik Sense Desktop;';
	        csWebActivity.QSCTaskSubject__c = 'Initial Log In – Qlik Sense Cloud;';
	        csWebActivity.Number_of_Retries__c = 3;
	        csWebActivity.Retry_Batch_Limit__c = '100';
	        csWebActivity.Error_Exception_List__c  = 'unable to obtain exclusive access,operation performed with inactive user';
	        insert csWebActivity;

	        Web_Activity__c wa = new Web_Activity__c(
				Name = 'ULC-1212',
				Sender__c = 'QlikID',
				Type__c = 'LeadGeneration',
				Status__c = 'Failed',
				Content__c = '{"action":"SetUser","oUser":{"UserName":"rebeccao","FirstName":"Rebecca","LastName":"Sullivan","Phone":"+640278099526","Account":"bopdhb","EmailAddress":"Lead@test.com.sandbox","State":"","Country":"Sweden","CountryCode":"SWE","JobTitle":"OT","LeadID":"","ULCDetailsID":"","LeadSourceDetail":"","ClaimedQTRel":"","QCloudID":"","EmailOptOut":false,"Feature":"DIRECT_ULC","ref":"QSD"},"additionalParams":{"SupplimentalData":"SOURCEID2~Qlik Sense Desktop~REF~QSD~INCENTIVE~Free Download~LEADSOURCEDETAILMIRROR~WEB - Download QlikSense~_mkto_trk~id:497-BMK-910&token:_mch-qlik.com-1567550141850-68379~CAMPAIGNID~7018E000000lOYyQAM~","Asset":"","ZiftData":""}}',
                Audit_Trail__c = 'creation failed:operation performed with inactive user',
                No_of_retries__c = 0
	        	);
	        insert wa;
            wa.Audit_Trail__c = 'creation failed: Error:unable to obtain exclusive access to this record';
            wa.Content__c.replaceFirst('7018E000000lOYyQAM',camp1.id);
	        update wa;
            
            
	        Web_Activity__c wa2 = new Web_Activity__c(
				Name = 'ULC-1313',
				Sender__c = 'QlikID',
				Type__c = 'LeadGeneration',
				Status__c = 'Failed',
				Content__c = '{"action":"SetUser","oUser":{"UserName":"rafaole44","FirstName":"RAFAEL","LastName":"MELO","Phone":"+5531987481917","Account":"OlConsignado","EmailAddress":"rafael.melo@oleconsignado.com.br","State":"","Country":"Sweden","CountryCode":"SWE","JobTitle":"Consultor Pleno","LeadID":"","ULCDetailsID":"","LeadSourceDetail":"","ClaimedQTRel":"","QCloudID":"","EmailOptOut":true,"Feature":"DIRECT_ULC","ref":null},"additionalParams":{"SupplimentalData":"REF~null~_mkto_trk~id:497-BMK-910&token:_mch-qlik.com-1548777414574-90407~","Asset":"","ZiftData":""}}',
                No_of_retries__c = 0,
                Audit_Trail__c = 'creation failed'
	        	);
	        insert wa2;

            Test.startTest();	        
			ScheduleWebActivityRetry sh1 = new ScheduleWebActivityRetry('Recurring');
            String sch = '0 5 * * * ?';
            system.schedule('Test Schedule', sch, sh1);


            wa2.Status__c = 'Failed';
            wa2.Audit_Trail__c = 'creation failed:operation performed with inactive user';
            update wa2;

			//WebActivityRetry updloadBatch = new WebActivityRetry('twiceADay');
			//Database.executeBatch(updloadBatch, 100);
			ScheduleWebActivityRetry sh2 = new ScheduleWebActivityRetry('twiceADay');
            String sch1 = '0 5 * * * ?';
            system.schedule('Test Schedule2', sch1, sh2);


	        Test.stopTest();
            
        }
    }

}