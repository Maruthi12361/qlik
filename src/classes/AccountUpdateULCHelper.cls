/*
    2018-01-23	AIN		Initial implementation for CHG0032866
    2018-03-08  AIN     Changed from picklist to QCC Entitled Checkbox
*/


public with sharing class AccountUpdateULCHelper {
	public static void updateULC(List<Account> triggerNew, List<Account> triggerOld){
        System.debug('AccountUpdateULCTrigger: Starting');

        Set<Id> AccountsId = new Set<Id>();
        List<ULC_Details__c> ULCsToBeUpdated = new List<ULC_Details__c>();

        system.debug('triggerNew.size: ' +  triggerNew.size());
        system.debug('triggerOld.size: ' +  triggerOld.size());

        for(Integer i = 0; i<triggerNew.size();i++){
            if( 
                triggerNew[i].Navision_Status__c != triggerOld[i].Navision_Status__c ||
                triggerNew[i].Adopted_Navision_Status__c != triggerOld[i].Adopted_Navision_Status__c ||
                triggerNew[i].Partner_Type__c != triggerOld[i].Partner_Type__c ||
                triggerNew[i].ParentId != triggerOld[i].ParentId ||
                //If QBR_Partner has changed from Managed or to Managed
                triggerNew[i].QCC_Entitled__c  != triggerOld[i].QCC_Entitled__c )
            {
                    AccountsId.add(triggerNew[i].Id);
                    system.debug('Adding account: ' + triggerNew[i].Id + ' to dirty list');
            }
        }
        if(AccountsId.size() > 0) {
            system.debug('Running batch on ' + AccountsId.size() + ' accounts');
            Database.executeBatch(new AccountUpdateULCHelperBatch(AccountsId), 100);  
        }
        System.debug('AccountUpdateULCTrigger: ending');
    }
}