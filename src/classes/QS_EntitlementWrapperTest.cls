/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_EntitlementWrapperTest {
	public static testMethod void Test1() {
		
        QS_EntitlementWrapper cw = new QS_EntitlementWrapper();
        System.assertEquals(cw.checked,false);

        QS_EntitlementWrapper cw2 = new QS_EntitlementWrapper(new Entitlement(name='Test1'));
        System.assertEquals(cw2.cat.name,'Test1');
        System.assertEquals(cw2.checked,false);
	}
}