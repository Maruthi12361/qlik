/****************************************************************
*
*  OppPartnerContactHandler
*
*  2016-05-19 TJG : Migrated from the following trigger
					OppPartnerContactTrigger 
   2017-04-1-  MTM fix null reference errors
*****************************************************************/

public class OppPartnerContactHandler {
	// to be called on both events before insert and before update
	public static void handle(List<Opportunity> triggerOld, List<Opportunity> triggerNew)
	{
	    if (Semaphores.Opportunity_OppPartnerContactTriggerHasRun == false) {
	        ActivePSMUpdate.UpdatePSM(triggerNew, triggerOld);
	        ActivePSMUpdate.UpdateReferralPSM(triggerNew, triggerOld);
	        Semaphores.Opportunity_OppPartnerContactTriggerHasRun = true; 
	    }
	}
}