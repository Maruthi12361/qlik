/*********************************************
Change Log:
2016-02-19  NAD     Removed Support_Level__c and Support_Per__c references per CR# 33068 (Responsible Partner change)          
********************************************/

public with sharing class ContactModel 
{
	//Constuctor
	public ContactModel(Id contactId)
	{
		record = [SELECT Id, AccountId, Name FROM Contact WHERE Id =: contactId];
	}
	
	//Methods
	//Get information for the Account related to the Contact
	public Account getAccountRelatedToContact()
	{
		return [SELECT Id,
				Name,
				Account_Prioritisation__c,
				Credit_Status__c, 
				QlikTech_Company__c,
				By_Pass_st_Line_Support__c,
				Responsible_Partner_count__c,
				Red_Account__c,
				Maintenance_Type__c
				FROM Account 
				WHERE Id =: record.AccountId LIMIT 1];
	}
		
	//Get information for the Account Licenses related to the Account
	public List<Account_License__c> getAccountLicensesRelatedToAccount()
	{
		return [SELECT Id, 
					   Name, 
					   Application__c, 
					   Version__c, 
					   Product_Level__c, 
					   Support_From__c, 
					   Support_To__c,
					   O_S__c,
					   Environment__c,
					   Product__c,
					   Area__c,
					   Issue__c,
					   X3rd_Party_Software__c,
					   Client_Version__c,
					   Server_Version__c,
					   Publisher_Version__c,
					   Connector_Version__c,
					   Customer_Patch_Version__c,
					   Selling_Partner__r.Name
				FROM Account_License__c
				WHERE Account__c =: record.AccountId];
	}
	//Properties
	public Contact record
	{
		get
		{
			if(record==null)
			{
				record = new Contact();
			}
			return record;
		}		
		set;
	}	
	//Checks if the Case has a valid Id
    public Boolean hasValidId
    {
    	get
    	{
	        if(record.Id == null)
	            return false;
	        else
	            return true;
    	}
    }
    
    
}