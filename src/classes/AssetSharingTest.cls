/*
	AssetSharingTest
	
	Author:
		Tao Jiang, tjg@qlikview.com
		
	Changes:
		2017-01-20 TJG	Created.

	Description:
		Test class for AssetSharing.

 */
@isTest
public class AssetSharingTest {
	static final integer NBR_OF_ACCOUNTS = 2; 
    //    SELECT Id, Name FROM profile WHERE name = 'System Administrator' LIMIT 1
    static final Id AdminProfId = '00e20000000yyUzAAI';
    //    Select Id From UserRole Where PortalType = 'None' Limit 1
    static final Id PortalRoleId = '00E20000000vrJSEAY';
    //    SELECT Id, name FROM Profile WHERE Name LIKE 'PRM - Sales Dependent %y'
    static final Id ProfileId = '00e20000001OyLwAAK';
    // Partner Account Record Type ID
    static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';

    static testMethod void dummyTest()
    {
    	ApexSharingRules.TestingParnershare = true;
    	Account acc = new Account();
    	Asset asset = createAnAsset('DummyTest',acc,null, 'Active');
    	Insert asset;
    	List<Id> assetIds = new List<Id>();
    	AssetSharing.UpdateAssetSharing(assetIds);
    }

   static testMethod void SellThroughPartnerTest() 
    {
        ApexSharingRules.TestingParnershare = true;
        Test.startTest();
        QTCustomSettings__c setting = new QTCustomSettings__c(
        Name = 'Default', 
        eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO',
        RecordTypesForecastOmitted__c = '01220000000DNwZAAW',
        OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
        Insert setting;
        NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
        settings.UserRecordTypeIdsToExclude__c = UserInfo.getUserId(); //'005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
        insert settings;
        QuoteTestHelper.createCustomSettings();
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User ownerUser = createSysAdminUser(AdminProfId, PortalRoleId, me);

        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');

        List<Contact> ctList = new List<Contact>();
        List<Account> partnerAccs = createPartnerAccounts(ctList, ownerUser, qtComp);

        //PRM - Sales Dependent Territory + QlikBuy
        List<User> ppUsers = createUserFromProfileIdContact(ProfileId, ctList);
        
        // sell through partner account Ids
        Set<Id>  uniqueAccIds = new Set<Id>();

        List<Asset> testAsset = new List<Asset>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
            // create Asset with partnerAccs[1] as SellThrough partner
            Id stpId = partnerAccs[NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + i].Id;
            Asset anAsset = createAnAsset(
                'Asset Sharing Test ' + i,
                partnerAccs[NBR_OF_ACCOUNTS + i],
                stpId,
                'Active'
            );
            testAsset.add(anAsset);
            uniqueAccIds.add(stpId);
        }

        Insert testAsset;

        List<Id> assetIds = new List<Id>();
        Integer iLen = testAsset.size();
        for (Integer i = 0; i < iLen; i++)
        {
            assetIds.add(testAsset[i].Id);
            testAsset[i].Reseller__c = partnerAccs[1].Id;
        }

        Update testAsset;

        List<AssetShare> assetShares = [Select Id from AssetShare Where RowCause = 'Manual' AND AssetId in :assetIds];

        System.assertNotEquals(0, assetShares.size());

        for (Integer i = 0; i < iLen; i++)
        {
            testAsset[i].Reseller__c = null;
        }
        
        Update testAsset;

        Test.stopTest();        
    }    
    
    static testMethod void EndUserTest() 
    {
        ApexSharingRules.TestingParnershare = true;
        Test.startTest();
        QTCustomSettings__c setting = new QTCustomSettings__c(
        Name = 'Default', 
        eCustoms_QlikBuy_RecordTypes__c='01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO',
        RecordTypesForecastOmitted__c = '01220000000DNwZAAW',
        OEM_Record_Types__c='01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW');
        Insert setting;
        NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
        settings.Name = 'NSSettingsDetail';
        settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
        settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
        settings.UserRecordTypeIdsToExclude__c = UserInfo.getUserId(); //'005D0000002URF0';
        settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
        insert settings;
        QuoteTestHelper.createCustomSettings();
        /////RecordType rt = [SELECT Id FROM RecordType WHERE Name='Qlikbuy CCS Standard II' limit 1];
        //Profile prof = [Select Id from Profile where name = 'System Administrator' limit 1];

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User ownerUser = createSysAdminUser(AdminProfId, PortalRoleId, me);

        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');

        List<Contact> ctList = new List<Contact>();
        List<Account> partnerAccs = createPartnerAccounts(ctList, ownerUser, qtComp);


        //PRM - Sales Dependent Territory + QlikBuy
        /////Profile prof = [SELECT Id, name FROM Profile WHERE Name LIKE 'PRM - Sales Dependent %y']; 
        List<User> ppUsers = createUserFromProfileIdContact(ProfileId, ctList);

        List<Asset> testAssets = new List<Asset>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
            // create Asset with partnerAccs[1] as SellThrough partner
            Asset anAsset = createAnAsset(
                'Asset Sharing Test ' + i,
                partnerAccs[NBR_OF_ACCOUNTS + i],
                null,
                'Active'
            );
            testAssets.add(anAsset);
        }

        Insert testAssets;

        List<Id> assetIds = new List<Id>();
        //Integer iLen = testAssets.size();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++)
        {
            assetIds.add(testAssets[i].Id);
            testAssets[i].AccountId = partnerAccs[NBR_OF_ACCOUNTS + i - 1].Id;
        }

        List<AssetShare> assetShares = [Select Id from AssetShare Where RowCause = 'Manual' AND AssetId in :assetIds];

        //--System.assertNotEquals(0, assetShares.size());

        /*
        Set<Asset> oSet = new Set<Asset>();
        oSet.addAll(testAssets);

        AssetShare oppShare = new AssetShare(
            AssetId = testAssets[0].Id,
            AssetAccessLevel = 'Edit',
            UserOrGroupId = ppUsers[1].Id);
        Insert oppShare;

        ApexSharingRules.UpdateAssetSharing(oSet);
        // force to update again
        ApexSharingRules.UpdateAssetSharing(oSet);
        */

        List<Id> accountIds = new List<Id>();
        for (Account acc : partnerAccs)
        {
            accountIds.add(acc.Id);
        }

        //List <UserRole> uRoles = [Select Id, PortalAccountId, ParentRoleId, PortalType, PortalRole from UserRole where PortalAccountId in :accountIds];


        //System.debug('uRoles.size() = ' + uRoles.size() + ', uRoles=' + uRoles);

        //System.assertNotEquals(uRoles, null);
        //System.assertNotEquals(uRoles.size(), 0); 

        System.assertNotEquals(0, assetShares.size());

        Update testAssets;

        Test.stopTest();        
    }

    public static User createSysAdminUser(Id profId, Id roleId, User me ) 
    {
        User u 
            = new User(
               alias = 'adminUsr', email='adminUser@tjtest.com.test',
                Emailencodingkey='UTF-8', lastname='adminTest', 
                Languagelocalekey='en_US', localesidkey='en_US',
                Profileid = profId,
                Timezonesidkey='America/Los_Angeles', 
                //ContactId = cont.Id,
                UserRoleId = roleId,
                Username= 'usr' + System.now().millisecond() + '@jttest.com.test'
            );

        System.runAs ( me ) 
        {
            insert u;
        }

        return u;
    }   

    public static List<User> createUserFromProfileIdContact(Id profId, List<Contact> cont) {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        List<User> usrs = new List<User>();
        System.runAs ( thisUser ) {
            //UserRole ur = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            
            for (integer ndx = 0; ndx < cont.size(); ndx++)
            {
                User u = new User(alias = 'newUser', email='puser'+ ndx + '@tjtest.com.test',
                    Emailencodingkey='UTF-8', lastname='Testing', 
                    Languagelocalekey='en_US', localesidkey='en_US',
                    Profileid = profId,
                    Timezonesidkey='America/Los_Angeles',
                    ContactId = cont[ndx].Id,
                    //UserRoleId = ur.Id,
                    Username= 'opuser' + ndx + '@tjtest.com.test');
                usrs.add(u);
            }
            insert usrs;
            for (integer i = 0; i < usrs.size(); i++)
            {
                usrs[i].IsPortalEnabled = true;                        
            }
            update usrs;
        }
        return usrs;
    } 

    private static List<Account> createAccounts(User user)
    {
        List<Account> nuAccounts = new List<Account>();
        for (Integer i = 0; i < NBR_OF_ACCOUNTS; i++) {
            Account acc = QTTestUtils.createMockAccount('OppSharing Test Inc [' + (i+1) + ']', user, false);
            nuAccounts.add(acc);
        }
        Insert nuAccounts;
        return nuAccounts;
    }

    public static Account createPartnerAccount(String sName, User user, QlikTech_Company__c qtComp, Boolean doInsert)
    {
        Account acc = new Account(  Name=sName, 
            OwnerId = user.Id,
            Billing_Country_Code__c = qtComp.Id,
            QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
            Navision_Customer_Number__c = '12345', 
            Navision_Status__c = 'Partner',
            RecordTypeId = PartnerAccRecTypeId,
            Legal_Approval_Status__c = 'Legal Approval Granted', 
            BillingStreet = '31 Hight Street',
            BillingState ='Berks',
            BillingPostalCode = 'RG2 5ST',
            BillingCountry = 'United Kingdom',
            BillingCity = 'Reading',           
            Shipping_Country_Code__c = QTComp.Id,
            ShippingStreet = '31 Hight Street',
            ShippingState ='Berks',
            ShippingPostalCode = 'RG2 5ST',
            ShippingCountry = 'United Kingdom',
            ShippingCity = 'Reading',
            From_Lead_Conversion__c = false
        );      
        if (doInsert)
        {
            insert acc;
        }
        return acc;
    }

    public static List<Account> createPartnerAccounts(List<Contact> ctList, User user, QlikTech_Company__c qtComp)
    {
        List<Account> nuAccounts = new List<Account>();
        for (Integer i = 0; i < (NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS) ; i++)
        {
            Account acc = new Account(  
                Name = 'AssetTest' + i, 
                OwnerId = user.Id,
                Billing_Country_Code__c = qtComp.Id,
                QlikTech_Company__c = qtComp.QlikTech_Company_Name__c,
                Navision_Customer_Number__c =  'N' + (10000+i), 
                Navision_Status__c = 'Partner',
                RecordTypeId = PartnerAccRecTypeId,  // Partner Account
                Legal_Approval_Status__c = 'Legal Approval Granted', 
                BillingStreet = '3' + i + ' Hight Street',
                BillingState ='Berks',
                BillingPostalCode = 'RG2 5ST',
                BillingCountry = 'United Kingdom',
                BillingCity = 'Reading',           
                Shipping_Country_Code__c = QTComp.Id,
                ShippingStreet = '3' + i + ' Hight Street',
                ShippingState ='Berks',
                ShippingPostalCode = 'RG2 5ST',
                ShippingCountry = 'United Kingdom',
                ShippingCity = 'Reading',
                From_Lead_Conversion__c = false
            );
            nuAccounts.add(acc);
        }
        Insert nuAccounts;

        for(Integer i = 0; i < (NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS + NBR_OF_ACCOUNTS); i++)
        {
            Contact contact = new Contact( 
                FirstName = 'TestCon'+i,
                Lastname = 'testsson',
                AccountId = nuAccounts[i].Id,
                pse__Is_Resource__c = true,
                pse__Is_Resource_Active__c = true,
                Email = 'asset' + i + '@test.com');
            ctList.add(contact);  
        }
        insert ctList;
        return nuAccounts;
    }

    private static Asset createAnAsset(String sName, Account acc, Id stp, String status)
    {
        Asset asset = New Asset (
            Name = sName,
            AccountId = acc.Id,
            Reseller__c = stp,
            Status = status
            ); 
   
        return asset;
    }
}