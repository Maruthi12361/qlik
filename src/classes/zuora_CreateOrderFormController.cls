/**
**** 10/8/2018 New controller class for create order form button for zuora quotes.
**/
global class zuora_CreateOrderFormController {

    public zqu__Quote__c quote {get; set;}

    public Boolean ECustomsCleared{
        get{
            system.debug('aaaa'+quote.Revenue_Type__c+quote.zqu__Account__r.ECUSTOMS__RPS_Status__c+quote.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c);
            return ECustomsHelper.VCStatus(quote.Revenue_Type__c,quote.zqu__Account__r.ECUSTOMS__RPS_Status__c,
            quote.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,quote.zqu__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c,
            quote.zqu__Opportunity__r.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c);
        }
    }

    
    public ApexPages.StandardController controller;

    public zuora_CreateOrderFormController (ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()){
            stdController.addFields(new List<String>{'Is_Quote_Expired__c'});
        }
        quote = (zqu__Quote__c)stdController.getRecord();
        controller =stdController;
    }

    //to call from JavaScript button 'Create Order Form'
    webservice static boolean checkVCStatus(string quoteAccount, string quoteRevType, String EUserRPSStatus, String EUserRCStatus, String PUserRPSStatus, String PUserRCStatus){
        return ECustomsHelper.VCStatus(quoteRevType,EUserRPSStatus, PUserRPSStatus,EUserRCStatus, PUserRCStatus);
    }


    public pageReference validateMethod() {
        displayPopup = true;

        if (quote.Subsidiary__c == NULL) {
            string message = 'Account is missing Subsidiary record, please update and try again.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
            return NULL;
        } else {
            if (!quote.Is_quote_Expired__c) {
                if(quote.zqu__Primary__c) {
                    if (quote.Quote_Status__c == 'Approved') {
                        if (!ECustomsCleared) {
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This customer needs to be reviewed and approved by the legal department. Legal has been notified automatically. If the quote remains locked after 1 business day, please contact SFLegalApprovals@qlik.com.'));
                            return NULL;
                        } else {
                            if(quote.Order_Form_Status__c == 'Out For Signature'){
                                string message = 'There is an Order Form currently out for signature. If you need to recall or resend the request open the Order Form in the "In Process" folder. Then select Send, For Signature and make a choice.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'External Review'){
                                string message = 'There is an Order Form currently with the Quote Recipient / SOI Contact for review.  If you need to recall or resend the request open the Order Form in the "In Process" folder.  Then select Send, External Review and make a choice.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'Quote Approvals Needed'){
                                string message = 'Your Order Form is currently in process and being redlined by Legal.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'Sales Review'){
                                string message = 'There is an Order Form currently in process and waiting for your action. Please go to the "In Process" folder and make a choice to take the next step there.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'Legal Review'){
                                string message = 'There is an Order Form currently in process and with Legal for redlines.  If you need to recall or have questions about your Order Form, reach out to Legal.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'Orders Review'){
                                string message = 'There is an Order Form currently in process and with Orders for approval. If you need to recall or have questions about your Order Form, reach out to Orders.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'Orders Approval'){
                                string message = 'There is an Order Form currently in process and with Orders for approval. If you need to recall or have questions about your Order Form, reach out to Orders.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'Fully Executed'){
                                string message = 'This opportunity is "Closed Won" and an Order Form cannot be generated.';
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                                return NULL;
                            }else if(quote.Order_Form_Status__c == 'None' ||
                            quote.Order_Form_Status__c == NULL ||
                            quote.Order_Form_Status__c == 'Canceled' ||
                            quote.Order_Form_Status__c == 'Rejected' ||
                            quote.Order_Form_Status__c == 'Failed' ||
                            quote.Order_Form_Status__c == 'Expired') {

                                //Commented out to match new URL-format 2018-11-14
                                //string pageRef = 'https://' + getQTCustomSettingsHier().SpringCM_Environment__c + '.springcm.com/atlas/doclauncher/eos/' + quote.Doc_Launcher_Config__c + '?aid=' + getQTCustomSettingsHier().SpringCM_Account_Id__c + '&eos[0].Id=' + quote.Id + '&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name=' + quote.Name + '&eos[0].ScmPath=/Salesforce/Accounts/' + quote.zqu__Account__c + '/Opportunities/' + quote.zqu__Opportunity__c + '/Quotes';

                                string pageRef = 'https://' + getQTCustomSettingsHier().SpringCM_Environment__c + '.springcm.com/atlas/doclauncher/eos/' + quote.Doc_Launcher_Config__c + '?aid=' + getQTCustomSettingsHier().SpringCM_Account_Id__c + '&eos[0].Id=' + quote.Id + '&eos[0].System=Salesforce&eos[0].Type=zqu__Quote__c&eos[0].Name=' + quote.zqu__Number__c + '&eos[0].ScmPath=/Salesforce/Accounts/' + quote.zqu__Account__r.AccNumber__c + '/3 - Opportunities/' + quote.zqu__Opportunity__r.OppNumber__c + '/Quotes';

                                PageReference quoteDetail = new PageReference(pageRef);
                                quoteDetail.setRedirect(true);
                                return quoteDetail;
                            }/* UIN to be removed later if needed
                            else{
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Message not defined for this order form status.'));
                            return NULL;
                        }*/

                        }
                    } else if(quote.Quote_Status__c == 'Contracting') {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There is already an order form created for this quote, please cancel the outstanding order form to be able to create a new order form.'));
                        return NULL;
                    } else{
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Cannot create another order form. Quote is not approved yet.'));
                        return NULL;
                    }
                } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Cannot create another order form. Quote is non-primary or status is in contracting. Please cancel any outstanding order forms to be able to generate a new order form.'));
                    return NULL;
                }
            }
            return NULL;
        }
    }

    public QTCustomSettingsHier__c getQTCustomSettingsHier() {
        QTCustomSettingsHier__c qtCSHier = QTCustomSettingsHier__c.getInstance();
        return qtCSHier;
    }

    public boolean displayPopup {get; set;}

    public PageReference returnToQuote(){
        PageReference quoteDetail = new PageReference('/'+quote.Id);
        quoteDetail.setRedirect(true);
        return quoteDetail;
    }
}