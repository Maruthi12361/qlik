/**********************************************************
ChangeLog
    2013-08-07  CCE Initial creation for CR# 8441.

***********************************************************/
public with sharing class CaseCollaborationExtension {
    private Case c;
    public Case caze {
        get{
            return c;
        }
        set{
            c=value;
        }
    }
    
    public CaseCollaborationExtension(ApexPages.StandardController stdController) {
        this.c = (Case)stdController.getRecord();
        this.c = [select id, status, Collaboration_Team__c, Request_Collaboration_at__c from Case where id = :c.id];
    }

    //Set the default Collaboration Team value
    public String issueDescription {
        get; 
        set;
    }
    
    //Set the default Collaboration Team value
    public String DefaultCollaborationTeam {get; set;} {
        DefaultCollaborationTeam = 'Technical Product Support';
    }

    //Store a flag for wether we render the command button or not
    public boolean bRenderButton {get; set;}{
        //Disable button initially
        bRenderButton = false;
    }
    
    //get the Collaboration Team picklist values
    public list<SelectOption> getCollaborationTeams(){
        list<SelectOption> options = new list<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Case.Collaboration_Team__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {    
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    //Update the chatter feed with the previously stored user input 
    public PageReference requestCollaboration() {
        System.debug('requestCollaboration start');
        if(issueDescription != null && issueDescription != ''){
            System.debug('requestCollaboration inside if');
            //Adding a Text post 
            FeedItem post = new FeedItem(); 
            post.ParentId = c.Id;
            post.Body = '#Collaboration ' + issueDescription; 
            insert post; 

            issueDescription = '';
            c.Request_Collaboration_at__c = Datetime.Now() - 0.0001;
            upsert c;
        }
        System.debug('requestCollaboration end');
        return null;
    }
    public PageReference reloadPage() {
        this.c = [select id, status, Collaboration_Team__c, Request_Collaboration_at__c from Case where id = :c.id];
        return null;
    }
    public PageReference delayCollaboration() {
        if(issueDescription != null && issueDescription != ''){
            System.debug('requestCollaboration inside if');
            //Adding a Text post 
            FeedItem post = new FeedItem(); 
            post.ParentId = c.Id;
            post.Body = '#Collaboration ' + issueDescription; 
            insert post; 
            c.Request_Collaboration_at__c = null;
            upsert c;
            issueDescription = '';
        }
        
        return null;
    }
}