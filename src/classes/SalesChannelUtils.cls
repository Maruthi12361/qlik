/*
	SalesChannelUtils
	
	Author:
		Tao Jiang, tjg@qlikview.com
		
	Changes:
		2017-01-14 TJG	Created.
		2017-02-20 TJG 	To include revenue type in the Sales Channel matching
		2017-03-01 IRN 	added method createSalesChannel that will return the Sales channel
        2017-11-30 MTM QCW-4398 populate second partner

	Description:
		A class that creates and updates Sales Channel object formerly known as Contract Asset Link. 
		The API name for this object is Sales_Channel__c.

 */

public with sharing class SalesChannelUtils {
	public SalesChannelUtils() {
		
	}

	public static Id createSalesChannel(
		Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, Date contractEndDate)
	{
		Sales_Channel__c cal = new Sales_Channel__c(
			License_Customer__c = licenseCustomerId,
			Sell_Through_Partner__c = sellThroughPartnerId,
			Support_Provided_by__c = supportProvidedById,
			Contract_End_Date__c = contractEndDate
			);
		Insert cal;
		System.debug('createSalesChannel completed: ' + cal);
		return cal.Id;
	}

	public static Id createSalesChannel(
		Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, Date contractEndDate, Id latestContractId)
	{
		Sales_Channel__c cal = new Sales_Channel__c(
			License_Customer__c = licenseCustomerId,
			Sell_Through_Partner__c = sellThroughPartnerId,
			Support_Provided_by__c = supportProvidedById,
			Contract_End_Date__c = contractEndDate,
			Latest_Contract__c = latestContractId
			);
		Insert cal;
		System.debug('createSalesChannel completed for ' + cal.Id);
		return cal.Id;
	}

	public static Id createSalesChannel(
		Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, Date contractEndDate, Id latestContractId,
		String revenueType)
	{
		Sales_Channel__c cal = new Sales_Channel__c(
			License_Customer__c = licenseCustomerId,
			Sell_Through_Partner__c = sellThroughPartnerId,
			Support_Provided_by__c = supportProvidedById,
			Contract_End_Date__c = contractEndDate,
			Latest_Contract__c = latestContractId,
			Revenue_Type__c = revenueType
			);
		Insert cal;
		System.debug('createSalesChannel completed for ' + cal.Id);
		return cal.Id;
	}	

	public static Sales_Channel__c createSalesChannel(Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, String supportProvidedByType, String revenueType, String secondPartner)
	{
		System.debug('in createSalesChannel2');
		Sales_Channel__c cal = new Sales_Channel__c(
			License_Customer__c = licenseCustomerId,
			Sell_Through_Partner__c = sellThroughPartnerId,
			Support_Provided_by__c = supportProvidedById,
			Support_Provided_By_Type__c = supportProvidedByType,
			Revenue_Type__c = revenueType,
            Second_Partner__c = secondPartner
			);
		System.debug('createSalesChannel completed for ' + cal);
		return cal;
	}	

	public static Id createSalesChannel(
		Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, Date contractEndDate, Id latestContractId,
		String revenueType, String statusFormula)
	{
		Sales_Channel__c cal = new Sales_Channel__c(
			License_Customer__c = licenseCustomerId,
			Sell_Through_Partner__c = sellThroughPartnerId,
			Support_Provided_by__c = supportProvidedById,
			Contract_End_Date__c = contractEndDate,
			Latest_Contract__c = latestContractId,
			Revenue_Type__c = revenueType
			);
		Insert cal;
		System.debug('createSalesChannel completed for ' + cal.Id);
		return cal.Id;
	}

	public static Id createNewSalesChannel(
		Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, Date contractEndDate, Id latestContractId, 
		String revenueType, String statusFormula, String supportLevel, Decimal supportPercentage)
	{
		Sales_Channel__c cal = new Sales_Channel__c(
			License_Customer__c = licenseCustomerId,
			Sell_Through_Partner__c = sellThroughPartnerId,
			Support_Provided_by__c = supportProvidedById,
			Contract_End_Date__c = contractEndDate,
			Latest_Contract__c = latestContractId,
			Revenue_Type__c = revenueType,
			Support_Level__c = supportLevel,
			Support_Percentage__c = supportPercentage
			);
		Insert cal;
		System.debug('createNewSalesChannel completed for ' + cal.Id);
		return cal.Id;
	}

	public static Id updateExistingSalesChannel(
		Id salesChannelId, Id latestContractId, String revenueType, 
		String statusFormula, String supportLevel, Decimal supportPercentage)
	{
		Sales_Channel__c cal = new Sales_Channel__c
		(
			Id = salesChannelId,
			Latest_Contract__c = latestContractId,
			Revenue_Type__c = revenueType,
			Support_Level__c = supportLevel,
			Support_Percentage__c = supportPercentage
		);
		Update cal;
		System.debug('updateExistingSalesChannel completed for ' + cal.Id);
		return cal.Latest_Contract__c;
	}

	public static String updateContractAndSupportLevel(Id salesChannelId, Id latestContractId, String supportLevel, Decimal supportPercentage)
	{
		Sales_Channel__c cal = new Sales_Channel__c
		(
			Id = salesChannelId,
			Latest_Contract__c = latestContractId,
			Support_Level__c = supportLevel,
			Support_Percentage__c = supportPercentage
		);
		Update cal;
		System.debug('updateContractAndSupportLevel completed for ' + cal.Id);
		return cal.Support_Level__c;
	}

	public static Decimal updateSupportLevelPercentage(Id salesChannelId, String supportLevel, Decimal supportPercentage)
	{
		Sales_Channel__c cal = new Sales_Channel__c
		(
			Id = salesChannelId,
			Support_Level__c = supportLevel,
			Support_Percentage__c = supportPercentage
		);
		Update cal;
		System.debug('updateSupportLevelPercentage completed for ' + cal.Id);
		return cal.Support_Percentage__c;
	}

	public static Id updateLatestContract(Id salesChannelId, Id latestContractId)
	{
		Sales_Channel__c cal = new Sales_Channel__c
		(
			Id = salesChannelId,
			Latest_Contract__c = latestContractId
		);
		Update cal;
		System.debug('updateLatestContract completed for ' + cal.Id);
		return cal.Latest_Contract__c;
	}

	public static String updateRevenueType(Id salesChannelId, String revenueType)
	{
		Sales_Channel__c cal = new Sales_Channel__c
		(
			Id = salesChannelId,
			Revenue_Type__c = revenueType
		);
		Update cal;
		System.debug('updateRevenueType completed for ' + cal.Id);
		return cal.Revenue_Type__c;
	}

	public static Id upsertSalesChannel(
		Id salesChannelId, Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, 
		Date contractEndDate, Id latestContractId, String revenueType, String statusFormula, 
		String supportLevel, Decimal supportPercentage)
	{
		Sales_Channel__c cal = new Sales_Channel__c
		(
			Id = salesChannelId,
			License_Customer__c = licenseCustomerId,
			Sell_Through_Partner__c = sellThroughPartnerId,
			Support_Provided_by__c = supportProvidedById,
			Contract_End_Date__c = contractEndDate,
			Latest_Contract__c = latestContractId,
			Revenue_Type__c = revenueType,
			Support_Level__c = supportLevel,
			Support_Percentage__c = supportPercentage
		);
		Upsert cal;
		System.debug('Upserted SalesChannel: ' + cal.Id);
		return cal.Id;
	}

	/* for a given list of contracts, get all possible match sales channels */
	public static List<Sales_Channel__c> getPossibleSalesChannelsForContracts(List<Contract> contracts)
	{
		List<Id> customerIds = new List<Id>();
		for (Contract ct : contracts)
		{
			customerIds.add(ct.AccountId);
		}
		List<Sales_Channel__c> sChannels = [Select Id, License_Customer__c, Sell_Through_Partner__c, Support_Provided_by__c, Contract_End_Date__c, Latest_Contract__c, Revenue_Type__c, Latest_Contract_Status__c, Support_Level__c, Support_Percentage__c from Sales_Channel__c Where License_Customer__c in :customerIds];
		return sChannels;
	}

	/* for a given list of contracts, create their sales channels in memory */
	public static List<Sales_Channel__c> createSalesChannelsForContracts(List<Contract> contracts)
	{
		List<Sales_Channel__c> sCs = new List<Sales_Channel__c>();
		for (Contract ct : contracts)
		{
			Sales_Channel__c cal = new Sales_Channel__c(
					License_Customer__c = ct.AccountId,
					Sell_Through_Partner__c = ct.Sell_Through_Partner__c,
					Support_Provided_by__c = ct.Support_Provided_By__c,
					Contract_End_Date__c = ct.EndDate,
					//Latest_Contract__c = latestContractId,
					Revenue_Type__c = ct.Revenue_Type__c,
					Support_Level__c = ct.Support_Level__c,
					Support_Percentage__c = ct.Support_Percentage__c				
				);
			sCs.add(cal);
		}
		return sCs;
	}
	
    public static Boolean salesChannelMatchesContract(Sales_Channel__c sch, Contract contract)
    {
		if (contract.AccountId == sch.License_Customer__c && 
			contract.Sell_Through_Partner__c == sch.Sell_Through_Partner__c &&
			contract.Support_Provided_By__c == sch.Support_Provided_by__c &&
			contract.Revenue_Type__c == sch.Revenue_Type__c)
			return true;
		else
			return false;   	
    }

    /* 
       when a new contract is created as a result of Finance license management activities,
       there is no quote involed. Try to find matching Sales Channel. If not found create one
     */

    public static Id findOrCreateSalesChannel(Id licenseCustomerId, Id sellThroughPartnerId, Id supportProvidedById, Date contractEndDate, Id latestContractId, 
		String revenueType, String statusFormula, String supportLevel, Decimal supportPercentage)
    {
    	List<Sales_Channel__c> cals = [Select Id from Sales_Channel__c Where License_Customer__c = :licenseCustomerId 
    		and Sell_Through_Partner__c = :sellThroughPartnerId 
    		and Support_Provided_by__c = :supportProvidedById and Revenue_Type__c = :revenueType limit 1];
    	if (cals.size() > 0)
    	{
    		return cals[0].Id;
    	}
    	/* Can't find one, create new */
    	return createNewSalesChannel(licenseCustomerId, sellThroughPartnerId, supportProvidedById, contractEndDate, latestContractId, 
					revenueType, statusFormula, supportLevel, supportPercentage);
    }	
}