/*********************************************
Class:UserPortalUserHandler

Description to handle trigger: 
			Update field "Portal user active" on Contact object, when Portal user is activated/deactivated

Log History:
2017-01-11    BAD    Created - CR# 97619
**********************************************/
public with sharing class UserPortalUserHandler {
	public UserPortalUserHandler() {
	}

	@future (callout=true)
	public static void UpdateContacts(List<Id> ContactsID) {
		List<Contact> ContactsToUpdate = new List<Contact>();

	    for (Contact con : [SELECT Id, Portal_User_Active__c FROM Contact WHERE Id IN :ContactsID])
	    {       
	        con.Portal_User_Active__c = !con.Portal_User_Active__c; 
	        ContactsToUpdate.add(con);
	    }

	    update(ContactsToUpdate);

	}


}