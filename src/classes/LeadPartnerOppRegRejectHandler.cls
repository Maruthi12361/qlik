/******************************************************
Class: LeadPartnerOppRegRejectHandler

Changelog:    
	2017-10-25 AYS BMW-402 : Migrated from LeadPartnerOppRegReject.trigger.
	2018-28-25 AYS BMW-666 : Original_Partner_OwnerId__c blank validation added.
******************************************************/ 

public class LeadPartnerOppRegRejectHandler {
	public LeadPartnerOppRegRejectHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
		// correct on live, qt test, qt dev and partner dev
	    Id oppRegId = '012D0000000JsWAIA0';
	    Id oppRegIdRejected = '012D0000000JsWFIA0';
	    
	    //if the lead's record type is Partner Opp Reg and it has been rejected, change the record type
	    //to Partner Opp Reg Rejected, change the owner back to the partner and put the rejected date
	    for (Integer i = 0; i < triggerNew.size(); i++) {
	        Lead newLead = triggerNew[i];
	        Lead oldLead = triggerOld[i];
	        if (newLead.Opp_Reg_Rejected__c && !oldLead.Opp_Reg_Rejected__c && newLead.RecordTypeId == oppRegId && newLead.Original_Partner_OwnerId__c != null) {
	            newLead.OwnerId = newLead.Original_Partner_OwnerId__c;
	            newLead.RecordTypeId = oppRegIdRejected;
	            newLead.Opp_Reg_Rejected_Date__c = System.today();
	        }       
	    }
	}
}