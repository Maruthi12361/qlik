/**************************************************************************************************** 
   InactiveCustomerUsers_RunBatch
   IT-1013 : scheduled job for weekly launch
****************************************************************************************************/
global class InactiveCustomerUsers_RunBatch implements Schedulable {

    global void execute(SchedulableContext sc) {
        Integer limitQuery = 100; // default value in case custom settings are empty
        QTCustomSettings__c settings = QTCustomSettings__c.getInstance('Default');
        if (settings != null && settings.LimitForInactivationCustomerUsers__c != null) {
            limitQuery = settings.LimitForInactivationCustomerUsers__c.intValue();
        }
        Database.executeBatch(new UserDeactiveInactiveCustomerUsers(limitQuery));
    }

    // to run job daily
    global void execute() {
        // Tests for scheduled jobs can fail if the same job is already executing in Salesforce with the same name
        String jobName = Test.isRunningTest() ? 'UserDeactiveInactiveCustomerUsersTest' : 'UserDeactiveInactiveCustomerUsers';
        system.schedule(jobName, '0 0 1 ? * 7 *', new InactiveCustomerUsers_RunBatch());
    }
}