@isTest

/***********************************************************************************************

	Changelog:
		2016-05-24  Roman@4front	Method created for OpportunityLockFromOppProducts.trigger
		06.02.2017   RVA :   changing QT methods 
************************************************************************************************/

private class OpportunityLockFromOppProductsTest {
	
	@isTest static void test_InsertOppLineItem() {
		
		QTTestUtils.GlobalSetUp();
		test.startTest();
        // sample user
		//changed from standard user to system administrator
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.appdev');
        insert u;
        System.assertNotEquals(null, u.Id);

        Id s = Test.getStandardPricebookId();
		System.assertNotEquals(null, s);

        System.RunAs(QTTestUtils.createMockOperationsAdministrator()) {  
		
	        // create the product
	        Product2 p1 = new Product2(
	            name='Test Product 1',
	            IsActive=true,
	            Description='My Product',
	            CurrencyIsoCode = 'GBP',
	            ProductCode='12345',
	            Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
	            Income_Account__c = '26000 Deferred Revenue'
	        );
	        insert p1;       
	      
	        Product2 p2 = new Product2(
	            name='Test Product 2',
	            IsActive=true,
	            Description='My Product2',
	            CurrencyIsoCode = 'GBP',
	            ProductCode='12345',
	            Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
	            Income_Account__c = '26000 Deferred Revenue'
	        );
	        insert p2; 
	        // create the pricebookentry
	        PricebookEntry pbe1 = new PricebookEntry(
	            Pricebook2Id= s,
	            Product2Id= p1.id,
	            UnitPrice= 900.00,
	            IsActive=true,
	            UseStandardPrice=false,
	            CurrencyIsoCode = 'GBP'
	        ); 
	        insert pbe1;
		    System.assertNotEquals(null, pbe1.Id);

		    //CCE  Adding an account with BillingCountryCode and Qliktech Company
			/*
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
	            Name = 'GBR',
	            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
	        );
	        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
	        insert QTComp;
			*/
			QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
	        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
	            
	        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId='01220000000DOFzAAO', Pending_Validation__c = false);
	        insert acc;
	        
	        OpportunityLockFromOppProductsHandler tempObj = new OpportunityLockFromOppProductsHandler();
	        // create the opportunity
	        Opportunity opp1 = new Opportunity(
	            name='Test Opp 1',
	            recordtypeid='01220000000DNwY',
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'GBP',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = acc.Id,
	            Finance_Approved__c = false        
	        );
	        insert opp1;
	        System.assertNotEquals(null, opp1.Id);
	        
	        // add the line item
	        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();

	        OpportunityLineItem oli = new OpportunityLineItem();
	        oli.Quantity = 1;
	        oli.UnitPrice = 1000;
	        oli.PricebookEntryId = pbe1.id;
	        oli.OpportunityId = opp1.id; 
	        oli.Partner_Margin__c = 40;
	        oli.Discounted_Price__c = 900;
	        insert oli;
	        System.assertNotEquals(null, oli.Id);

	        delete oli;
		}

        test.stopTest();
	}

	@isTest static void test_NotValidUser() {
		
		QTTestUtils.GlobalSetUp();
		test.startTest();
        // sample user
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com.qtweb');
        insert u;
        System.assertNotEquals(null, u.Id);

        Id s = Test.getStandardPricebookId();
		System.assertNotEquals(null, s);

        System.RunAs(QTTestUtils.createMockOperationsAdministrator()) {  
		
	        // create the product
	        Product2 p1 = new Product2(
	            name='Test Product 1',
	            IsActive=true,
	            Description='My Product',
	            CurrencyIsoCode = 'GBP',
	            ProductCode='12345',
	            Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
	            Income_Account__c = '26000 Deferred Revenue'
	        );
	        insert p1;       
	      
	        Product2 p2 = new Product2(
	            name='Test Product 2',
	            IsActive=true,
	            Description='My Product2',
	            CurrencyIsoCode = 'GBP',
	            ProductCode='12345',
	            Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
	            Income_Account__c = '26000 Deferred Revenue'
	        );
	        insert p2; 
	        // create the pricebookentry
	        PricebookEntry pbe1 = new PricebookEntry(
	            Pricebook2Id= s,
	            Product2Id= p1.id,
	            UnitPrice= 900.00,
	            IsActive=true,
	            UseStandardPrice=false,
	            CurrencyIsoCode = 'GBP'
	        ); 
	        insert pbe1;
		    System.assertNotEquals(null, pbe1.Id);

		    //CCE  Adding an account with BillingCountryCode and Qliktech Company
			/*
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
	            Name = 'GBR',
	            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
	        );
	        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
	        insert QTComp;
			*/
			QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
	        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
	            
	        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId='01220000000DOFzAAO', Pending_Validation__c = false);
	        insert acc;
	            
	        // create the opportunity
	        Opportunity opp1 = new Opportunity(
	            name='Test Opp 1',
	            recordtypeid='01220000000DNwY',
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'GBP',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = acc.Id,
	            Finance_Approved__c = false        
	        );
	        insert opp1;
	        System.assertNotEquals(null, opp1.Id);

	        // add the line item
	        List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
	        
	        OpportunityLineItem oli = new OpportunityLineItem();
	        oli.Quantity = 1;
	        oli.UnitPrice = 1000;
	        oli.PricebookEntryId = pbe1.id;
	        oli.OpportunityId = opp1.id; 
	        oli.Partner_Margin__c = 40;
	        oli.Discounted_Price__c = 900;
	        insert oli;
	        System.assertNotEquals(null, oli.Id);
	        //oppList.add(oli);

	        delete oli;
		}

        test.stopTest();
	}

	@isTest static void test_LockErrorMessage() {
		
		QTTestUtils.GlobalSetUp();
		test.startTest();
        // sample user
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='standtest@qliktech.com.appdev',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='standtest@qliktech.com');
        insert u;
        System.assertNotEquals(null, u.Id);

        Id s = Test.getStandardPricebookId();
		System.assertNotEquals(null, s);

        System.RunAs(QTTestUtils.createMockOperationsAdministrator()) {  
		
	        // create the product
	        Product2 p1 = new Product2(
	            name='Test Product 1',
	            IsActive=true,
	            Description='My Product',
	            CurrencyIsoCode = 'GBP',
	            ProductCode='12345',
	             Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
	            Income_Account__c = '26000 Deferred Revenue'
	        );
	        insert p1;       
	      
	        Product2 p2 = new Product2(
	            name='Test Product 2',
	            IsActive=true,
	            Description='My Product2',
	            CurrencyIsoCode = 'GBP',
	            ProductCode='12345',
	             Deferred_Revenue_Account__c = '26000 Deferred Revenue', 
	            Income_Account__c = '26000 Deferred Revenue'
	        );
	        insert p2; 
	        // create the pricebookentry
	        PricebookEntry pbe1 = new PricebookEntry(
	            Pricebook2Id= s,
	            Product2Id= p1.id,
	            UnitPrice= 900.00,
	            IsActive=true,
	            UseStandardPrice=false,
	            CurrencyIsoCode = 'GBP'
	        ); 
	        insert pbe1;
		    System.assertNotEquals(null, pbe1.Id);

		    //CCE  Adding an account with BillingCountryCode and Qliktech Company
			/*
	        QlikTech_Company__c QTComp = new QlikTech_Company__c(
	            Name = 'GBR',
	            QlikTech_Company_Name__c = 'QlikTech UK Ltd'            
	        );
	        QTComp.Subsidiary__c = QTTestUtils.getSubsidiary(QTComp.Name);
	        insert QTComp;
			*/
			QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'GBR');
	        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
	        Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id, RecordTypeId='01220000000DOFzAAO', Pending_Validation__c = false);
	        insert acc;
	            
	        // create the opportunity
	        Opportunity opp1 = new Opportunity(
	            name='Test Opp 1',
	            recordtypeid='01220000000DNwY',
	            StageName = 'Marketing Qualified',
	            CurrencyIsoCode = 'GBP',
	            CloseDate = System.today(), 
	            Signature_Type__c = 'Digital Signature',       
	            AccountId = acc.Id,
	            Expertise_Area__c = 'test',
	            Finance_Approved__c = false        
	        );
	        insert opp1;
	        System.assertNotEquals(null, opp1.Id);
	        Opportunity tempOpp = [Select Id, Expertise_Area__c, Finance_Approved__c From Opportunity Where Id =:opp1.Id];
	        System.assertEquals(false, tempOpp.Finance_Approved__c);
	        tempOpp.Finance_Approved__c = true;
	        tempOpp.Expertise_Area__c = 'test next';
	        update tempOpp;

	        // add the line item
	        List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
	        
	        OpportunityLineItem oli = new OpportunityLineItem();
	        oli.Quantity = 1;
	        oli.UnitPrice = 1000;
	        oli.PricebookEntryId = pbe1.id;
	        oli.OpportunityId = tempOpp.id; 
	        oli.Partner_Margin__c = 40;
	        oli.Discounted_Price__c = 900;
	        try {
	        	insert oli;
	        }
	        catch(Exception e) {
	        	System.assertEquals(true, e.getMessage().contains('Opportunity is Locked'));
	        }
	        System.assertNotEquals(null, oli.Id);
		}

        test.stopTest();
	}
}