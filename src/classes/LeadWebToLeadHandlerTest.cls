/**
* Class created 11-04-2018 AYS BMW-721
*/
@isTest
private class LeadWebToLeadHandlerTest {
	
	@isTest static void testLeadWebToLeadHandler() {

		Id standardLeadRecordTypeId = [SELECT id FROM RecordType WHERE DeveloperName = 'Standard_lead'].id;

		insert new Lead_Settings__c(
        	User_Boomi_Integration__c = UserInfo.getUserId(), 
        	Source_Web_Activity__c = 'WEB - Web Activity',
			RT_Standard_lead__c = standardLeadRecordTypeId,
        	SetupOwnerId=UserInfo.getOrganizationId()
        );

        Lead lead0 = new Lead(  LastName='LastTest', 
                    FirstName='FirstTest', 
                    Country='Sweden',
                    Email='asd@asd.com',
                    LeadSource = 'WEB - Web Activity',
                    RecordTypeId = standardLeadRecordTypeId,
                    Company ='Testing');
        insert lead0;	

    }
}