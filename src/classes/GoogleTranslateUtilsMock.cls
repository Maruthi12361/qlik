/*****************************************************************************************
* 2016-12-15 AIN HTTP Callout Mock for class googleTranslateUtils
****************************************************************************************/
@isTest
global class GoogleTranslateUtilsMock implements HttpCalloutMock {
	global HttpResponse respond(HttpRequest req) {
        system.debug('GoogleTranslateUtilsMock start');
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        //res.setBody('{"status":"ok"}');
        string body = '{' +
        '    "data": {' +
        '        "detections": [' +
        '            [' +
        '                {' +
        '                    "language": "no",' +
        '                    "isReliable": true,' +
        '                    "confidence": 0.9' +
        '                }' +
        '            ]' +
        '        ]' +
        '    }'+
        '}';
        res.setBody(body);
        res.setStatusCode(200);
        system.debug('GoogleTranslateUtilsMock end');
        return res;
	}
}