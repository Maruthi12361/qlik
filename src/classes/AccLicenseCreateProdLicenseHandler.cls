/**     * File Name:AccLicenseCreateProdLicenseHandler
        * Description : This handler class is used to update create a product license(Entitlement) on insert of account license or 
        *               in case of update of account license if there is no existing product license
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Feb 3rd 2017 RamakrishnaKini     Added new trigger logic.
        1.1 Feb 23rd 2017 RamakrishnaKini     Added new trigger logic for account license delete scenarion to inactivate the product license.
        1.2 May 3rd 2017 RamakrishnaKini     Logic for null checks and end date greater than startdate.
        1.3 July 23rd 2017 QCW-3462 RamakrishnaKini  Modified logic to update entitlements when account on account license is changed
        1.4 Oct 25rd 2017 CHG0032086 ext_bad  Modified logic of populating Business Hours for Entitlement
        1.5 July 5rd 2019 IT-1995 ext_bad  Add Subscription__c sync
*/
public with sharing class AccLicenseCreateProdLicenseHandler {
    static ID standardEntitlementProcessId;
    static ID premiumEntitlementProcessId;
    static ID twentyFourSevenBusinessHoursId;
    static ID defaultBusinessHoursId;
    static String standardSupportLevel;
    static String premiumSupportLevel;
    static String holdAccountName;
    static SlaProcess standardEntitlementProcess;
    static SlaProcess premiumEntitlementProcess;

    public AccLicenseCreateProdLicenseHandler() {
        
        
    }

    /*  @Description :This common method is used to create the product license on account license after insert
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap Account Map
   
    */
    public static void onAfterInsert(List<Account_License__c> inputList, Map<Id, Account_License__c> inputMap) {
        system.debug('AccLicenseCreateProdLicenseHandler start');
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accLicenseIds = new Set<Id>();
        Set<String> accLicenseNames = new Set<String>();// UIN New
        Map<Id, Id> accountToBusinessHours = new Map<Id, Id>();
        Map<Id, Id> accountToEnterpriseBusinessHours = new Map<Id, Id>();
        Map<String, Entitlement> mEntNameEntitlement = new Map<String, Entitlement>();
        List<Entitlement> newEntitlements = new List<Entitlement>();
        List<Entitlement> updExstEntitlements = new List<Entitlement>();
        List<Account_License__c> accLicenses = new List<Account_License__c>();
        
        system.debug('AccLicenseCreateProdLicenseHandler before input list');
        for(Account_License__c acclicense: inputList){
            system.debug('AccLicenseCreateProdLicenseHandler inside input list acclicense.Account__c:' + acclicense.Account__c);
            if(String.isNotBlank(acclicense.Account__c)){
                system.debug('AccLicenseCreateProdLicenseHandler inside input list not blank');
                accountIds.add(acclicense.Account__c);
                accLicenseIds.add(acclicense.ID);
                accLicenseNames.add(acclicense.Name);// UIN New
            }
        }
        if(!accLicenseNames.isEmpty()){
            
            for(Entitlement e: [select id, name, recordtypeid, Account_License__c, Subscription__c from Entitlement where name in :accLicenseNames and Account_License__c = null]){
                if(!mEntNameEntitlement.containsKey(e.name))
                    mEntNameEntitlement.put(e.name, e);
            }
        }
        system.debug('AccLicenseCreateProdLicenseHandler after input list');
        if(!accLicenseIds.isEmpty()){
            if(!accountIds.isEmpty()){
                for (Account a : [SELECT a.Id, a.Support_Office__r.Business_Hours__c, a.Support_Office__r.Enterprise_Business_Hours__c 
                                        FROM Account a 
                                        WHERE Id IN :accountIds]) {           
                    accountToBusinessHours.put(a.Id, a.Support_Office__r.Business_Hours__c);
                    accountToEnterpriseBusinessHours.put(a.Id, a.Support_Office__r.Enterprise_Business_Hours__c);
                }
            }
            accLicenses = [select id,name, Account__c, Support_From__c, Support_To__c, INT_NetSuite_Support_Level__c, Subscription__c
                from Account_License__c where id in :accLicenseIds];
            
            if(!accLicenses.isEmpty()){
                List<RecordType> entStdRecType = [select id from recordtype where developername= 'Standard' and sobjecttype = 'Entitlement'];
                retrieveSettings();
                for(Account_License__c al: accLicenses){
                    if(al.Support_From__c <= al.Support_to__c){// Fix for end data greater than start date
                        if(!mEntNameEntitlement.containsKey(al.name))
                            newEntitlements.add(createEntitlement(al, accountToBusinessHours, accountToEnterpriseBusinessHours));
                        else{
                            if(mEntNameEntitlement.containsKey(al.name)){
                                mEntNameEntitlement.get(al.name).Account_License__c = al.id;
                                mEntNameEntitlement.get(al.name).recordtypeid = entStdRecType[0].Id;
                                mEntNameEntitlement.get(al.name).StartDate = al.Support_From__c;
                                mEntNameEntitlement.get(al.name).Enddate = al.Support_to__c;
                                mEntNameEntitlement.get(al.name).Subscription__c = al.Subscription__c;
                                updExstEntitlements.add(mEntNameEntitlement.get(al.name));
                            }
                        }
                    }  
                }

                if(!newEntitlements.isEmpty())
                    insert newEntitlements;
                if(!updExstEntitlements.isEmpty())
                    update updExstEntitlements;
            }
        }
    }

    /*  @Description :This common method is used to update the product license on account license after update
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap Account Map
   
    */
    public static void onAfterUpdate(List<Account_License__c> inputList, Map<Id, Account_License__c> inputMap, Map<Id, Account_License__c> oInputMap) {
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accLicenseIds = new Set<Id>();
        Set<Id> accChangedLicenseIds = new Set<Id>();
        List<Account_License__c> accLicenses = new List<Account_License__c>();
        List<Entitlement> newEntitlements = new List<Entitlement>();
        Map<Id, Id> accountToBusinessHours = new Map<Id, Id>();
        Map<Id, Id> accountToEnterpriseBusinessHours = new Map<Id, Id>();
        Set<String> accLicenseNames = new Set<String>();// UIN New
        Map<String, Entitlement> mEntNameEntitlement = new Map<String, Entitlement>();// UIN New
        List<Entitlement> updExstEntitlements = new List<Entitlement>();// UIN New
        for(Account_License__c acclicense: inputList){
            if(String.isNotBlank(acclicense.Account__c)){
                accountIds.add(acclicense.Account__c);
                accLicenseIds.add(acclicense.ID);
                accLicenseNames.add(acclicense.Name);// UIN New
            }
        }
        //Added by UIN for QCW-3462
        for(Account_License__c acclicense: inputList){
            if(String.isNotBlank(acclicense.Account__c) && acclicense.Account__c != oInputMap.get(acclicense.Id).Account__c){
                //accountIds.add(acclicense.Account__c);
                accChangedLicenseIds.add(acclicense.ID);
            }
        }

        if(!accChangedLicenseIds.isEmpty()){
            For(Entitlement ent: [select id, account_license__c, account_license__r.Account__c, AccountId from Entitlement where account_license__c in :accChangedLicenseIds]){
                if(ent.account_license__r.Account__c != ent.AccountId){
                    ent.AccountId = ent.account_license__r.Account__c;
                    newEntitlements.add(ent);
                }
            }

            if(!newEntitlements.isEmpty())
                update newEntitlements;
        }
        //Added by UIN for QCW-3462
        if(!accLicenseIds.isEmpty()){
            Set<Id> mAccLicWithEntitlements = new Set<Id>();
            Set<String> sAccLicNamesWithEntitlements = new Set<String>();
            For(Entitlement ent: [select id, account_license__c,name from Entitlement where account_license__c in :accLicenseIds]){
                if(!mAccLicWithEntitlements.contains(ent.account_license__c)){
                    mAccLicWithEntitlements.add(ent.account_license__c);
                    sAccLicNamesWithEntitlements.add(ent.name);
                }
            }
            /*Set<Id> accLicenseIdsNoEntitlements = new Set<Id>();
            for(Id accId: accLicenseIds){
                if(!mAccLicWithEntitlements.contains(accId))
                    accLicenseIdsNoEntitlements.add(accId);
            }*/

            
            accLicenseIds.removeAll(mAccLicWithEntitlements);
            accLicenseNames.removeAll(sAccLicNamesWithEntitlements);
            if(!accLicenseNames.isEmpty()){
            
                for(Entitlement e: [select id, name, recordtypeid, Account_License__c, Subscription__c from Entitlement where name in :accLicenseNames and Account_License__c = null]){
                    if(!mEntNameEntitlement.containsKey(e.name))
                        mEntNameEntitlement.put(e.name, e);
                }
            }
            if(!accLicenseIds.isEmpty()){
                if(!accountIds.isEmpty()){
                    for (Account a : [SELECT a.Id, a.Support_Office__r.Business_Hours__c, a.Support_Office__r.Enterprise_Business_Hours__c 
                                        FROM Account a 
                                        WHERE Id IN :accountIds]) {           
                        accountToBusinessHours.put(a.Id, a.Support_Office__r.Business_Hours__c);
                        accountToEnterpriseBusinessHours.put(a.Id, a.Support_Office__r.Enterprise_Business_Hours__c);
                    }
                }
                accLicenses = [select id,name, Account__c, Support_From__c, Support_To__c, INT_NetSuite_Support_Level__c, Subscription__c
                    from Account_License__c where id in :accLicenseIds];
                if(!accLicenses.isEmpty()){
                    List<RecordType> entStdRecType = [select id from recordtype where developername= 'Standard' and sobjecttype = 'Entitlement'];
                    retrieveSettings();
                    for(Account_License__c al: accLicenses){
                        if(al.Support_From__c <= al.Support_to__c){// Fix for end data greater than start date
                            if(!mEntNameEntitlement.containsKey(al.name))
                                newEntitlements.add(createEntitlement(al, accountToBusinessHours, accountToEnterpriseBusinessHours));
                            else{
                                if(mEntNameEntitlement.containsKey(al.name)){
                                    mEntNameEntitlement.get(al.name).Account_License__c = al.id;
                                    mEntNameEntitlement.get(al.name).recordtypeid = entStdRecType[0].Id;
                                    mEntNameEntitlement.get(al.name).StartDate = al.Support_From__c;
                                    mEntNameEntitlement.get(al.name).Enddate = al.Support_to__c;
                                    mEntNameEntitlement.get(al.name).Subscription__c = al.Subscription__c;
                                    updExstEntitlements.add(mEntNameEntitlement.get(al.name));
                                }
                            }
                        }    
                    }
                    if(!newEntitlements.isEmpty())
                        insert newEntitlements;
                    if(!updExstEntitlements.isEmpty())
                        update updExstEntitlements;
                }
            }
        }

    }

    /*  @Description :This common method is used to delete the product license on account license after delete
    
        @parameter inputList: Trigger.old Account list
        @parameter inputMap: Trigger.oldMap Account Map
   
    */
    public static void onBeforeDelete(List<Account_License__c> inputList, Map<Id, Account_License__c> inputMap) {
        Set<Id> accountIds = new Set<Id>();
        Set<Id> accLicenseIds = new Set<Id>();
        List<Entitlement> entitlementsForDel = new List<Entitlement>();
        List<Entitlement> entitlementsForUpd = new List<Entitlement>();
        for(Account_License__c acclicense: inputList){
            accLicenseIds.add(acclicense.Id);
            system.debug('acclicense.Id: ' + acclicense.Id);
        }

        List<RecordType> entObseleteRecType = [select id from recordtype where developername= 'Obsolete' and sobjecttype = 'Entitlement'];
            
        if(!accLicenseIds.isEmpty() && entObseleteRecType.Size() > 0){
            entitlementsForDel = [select id, Recordtypeid from Entitlement where Account_License__c in :accLicenseIds and RecordTypeId != :entObseleteRecType[0].Id];
            if(!entitlementsForDel.isEmpty()){
                for(Entitlement e: entitlementsForDel){
                    e.RecordtypeId = entObseleteRecType[0].Id;
                    entitlementsForUpd.add(e);
                }
                //Update record type for product licenses whose account licenses are deleted
                if(!entitlementsForUpd.isEmpty())
                    update entitlementsForUpd;
            }
        }
    }

    /**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/

    private static SlaProcess standardOrPremiumProcessId(String supportLevel) { 
        return supportLevel == premiumSupportLevel ? premiumEntitlementProcess : standardEntitlementProcess;
    }

    private static Entitlement createEntitlement(Account_License__c al, Map<Id, Id> accountToBusinessHours, Map<Id, Id> accountToEnterpriseBusinessHours) {
        Entitlement e = new Entitlement(Account_License__c = al.Id, AccountId = al.Account__c, Automatic__c = true); 
        e.Name = al.Name;
        e.StartDate = al.Support_From__c;
        e.EndDate = al.Support_To__c;
        e.Subscription__c = al.Subscription__c;
        e.SlaProcessId = standardOrPremiumProcessId(al.INT_NetSuite_Support_Level__c).Id;
        e.SLA_Process_Name__c = standardOrPremiumProcessId(al.INT_NetSuite_Support_Level__c).Name;
        
        Id businessHoursId = accountToBusinessHours.get(al.Account__c);
        Id enterpriseBusinessHoursId = accountToEnterpriseBusinessHours.get(al.Account__c);
        if (businessHoursId != null) {
            e.BusinessHoursId = al.INT_NetSuite_Support_Level__c == premiumSupportLevel 
            ? enterpriseBusinessHoursId != null ? enterpriseBusinessHoursId : defaultBusinessHoursId
            : businessHoursId;                  
        } else {
            e.BusinessHoursId = defaultBusinessHoursId;
        }
        return e;
    }

    private static void retrieveSettings(){
        Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();     
        premiumSupportLevel = es.Premium_Support_Level__c;
        standardSupportLevel = es.Standard_Support_Level__c;
        holdAccountName = es.Hold_Account_Name__c;
            
        standardEntitlementProcess = [select Id,name from SlaProcess where name =:es.Standard_Entitlement_Process_Name__c limit 1];
        premiumEntitlementProcess = [select Id,name from SlaProcess where name =:es.Premium_Entitlement_Process_Name__c limit 1];
        standardEntitlementProcessId = standardEntitlementProcess.Id;
        premiumEntitlementProcessId = premiumEntitlementProcess.Id;
        twentyFourSevenBusinessHoursId = [select Id from BusinessHours where name=:es.X24x7_Business_Hours_Name__c limit 1].Id;
        defaultBusinessHoursId = [select Id from BusinessHours where isDefault=true limit 1].Id; 
    }

}