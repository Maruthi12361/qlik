/********
* NAME :OpportunityHelperClassTest
* Description: Tests for Helper class to opportunityHelperClass
* 
*
*Change Log:
    IRN      2016-02-09  
******/
@isTest
private class OpportunityHelperClassTest {

    private static testMethod void TestOpportunityHelperClass(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        User aUser = QTTestUtils.createMockUserForProfile('System Administrator');
        Account account = QTTestUtils.createMockAccount('name', aUser, true);
        Boolean res = OpportunityHelperClass.doOpenOpportunitiesExist(account.Id);
        System.assert(!res);
        RecordType rt = [SELECT Id, Name from RecordType where Name like 'Qlikbuy % II'];

        Opportunity testOpp = new Opportunity(
        Name = 'CreatedBy Test Opp 1',
        StageName = 'Goal Confirmed',
        ForecastCategoryName = 'Omitted',
        CloseDate = Date.today(),
        AccountId = account.Id,
        CurrencyISOCode = 'GBP',   
        Original_Created_by_Function__c = 'IT',
        Function__c = 'Other',
        Solution_Area__c = 'Other',
        Signature_Type__c = 'Digital Signature',
        Amount = 100.0,
        Included_Products__c = 'Qlik Sense');
        insert testOpp;        
        Boolean res2 = OpportunityHelperClass.doOpenOpportunitiesExist(account.Id);
        System.assert(res2);
        test.stopTest();
    }
    
}