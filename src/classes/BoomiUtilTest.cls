@isTest
public with sharing class BoomiUtilTest {
        
    static void setupCustomSettings() {
        QTCustomSettings__c setting = new QTCustomSettings__c();
        setting.Name = 'Default';
        setting.BoomiBaseURL__c = 'https://www.google.com';
        setting.BoomiToken__c = 'BoomiToken';
        insert setting;
    }

    static testMethod void testgetBoomiStatus_InvalidId() {
        setupCustomSettings();
        User objUser = QTTestUtils.createMockOperationsAdministrator();     
        
        try
        {
            System.RunAs(objUser)
            {       
                System.assert(BoomiUtil.getBoomiStatus('') == null);
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Assert(false, 'Issue retreiving boomi status: ' + ex);   
        }   
    }

    static testMethod void testgetBoomiStatus_ValidId() {
        setupCustomSettings();
        String bId = 'boomiId__test';       
        Test.startTest();
        BoomiUtil.updateBoomiStatus(bId);
        Boomi_ExeCtrl__c bStatus = BoomiUtil.getBoomiStatus(bId);
        System.assert(bStatus != null);
        System.assert(bStatus.Status__c == 'Pending');
        System.assert(bStatus.Message__c == null);
        Test.stopTest();
    }
    
    static testMethod void testgetCustomSettings() {
        setupCustomSettings();
        Test.startTest();
        QTCustomSettings__c settings =  BoomiUtil.getCustomSettings();      
        System.assert(settings != null);        
        System.assertEquals('https://www.google.com', settings.BoomiBaseURL__c);
        System.assertEquals('BoomiToken', settings.BoomiToken__c);  
        Test.stopTest();
    }

    static testMethod void testgetCustomSettings_missingSettings() {        
        Test.startTest();
        QTCustomSettings__c settings =  BoomiUtil.getCustomSettings();
        System.assert(settings == null);        
        Test.stopTest();
    }
    
    static testMethod void testUpdateBoomiStatus(){
        setupCustomSettings();      
        
        Test.startTest();
        BoomiUtil.updateBoomiStatus('test1234');        
        System.assertEquals('Pending', BoomiUtil.getBoomiStatus('test1234').Status__c);
        Test.stopTest();
    }

    static testMethod void testCallBoomi() {
        setupCustomSettings();
        Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());

        Test.startTest();
        BoomiUtil.callBoomi('test1234', 'BoomiUpdate', 'Go get them!');
        Test.stopTest();        
    }

    static testMethod void testCallBoomi_missingCustSettings() {        
        Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());
        
        Test.startTest();
        BoomiUtil.callBoomi('test1234', 'BoomiUpdate', 'Go get them!');
        Test.stopTest();        
    }
    
}