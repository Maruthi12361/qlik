/************************************************************************************************************
        CaseContentDocumentLinkPurge
        Description: This is the schedulable class to call the 
                     class where purge case ContentDocumentLink Object attachments that are closed and older than 90 days
   ext_bad - IT-606 - 2018-05-16 - Run scheduled CaseContentDocumentLinkPurge_RunBatch

*************************************************************************************************************/
global class CaseContentDocumentLinkPurge_RunBatch implements Schedulable {
    global void execute(SchedulableContext sc){
    	Database.executeBatch(new CaseContentDocumentLinkPurge());
    }

    // to run job daily
    global void execute(){
        // Tests for scheduled jobs can fail if the same job is already executing in Salesforce with the same name
        String jobName = Test.isRunningTest() ? 'CaseContentDocumentLinkPurgeTest' : 'CaseContentDocumentLinkPurge';
        system.schedule(jobName, '0 0 23 * * ?', new CaseContentDocumentLinkPurge_RunBatch());
    }
}