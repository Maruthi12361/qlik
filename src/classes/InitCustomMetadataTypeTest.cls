/**
 * Created by ext_bjd on 10/9/18.
 * updated:
 * 24.01.2019  ext_bjd Updated test for asynchronous invoke
 */

@IsTest
private class InitCustomMetadataTypeTest {
    @IsTest
    static void testCreateCustomMetadataType() {

        List<Appirio_core_Configuration_Group__mdt> configurationGroupsMdt = new List<Appirio_core_Configuration_Group__mdt>([
                SELECT Id, Name__c
                FROM Appirio_core_Configuration_Group__mdt
        ]);

        List<Appirio_core_Configuration_Option__mdt> configurationOptionsMdt = new List<Appirio_core_Configuration_Option__mdt>([
                SELECT Id, Name__c
                FROM Appirio_core_Configuration_Option__mdt
        ]);

        List<Appirio_core_Currency__mdt> currenciesMdt = new List<Appirio_core_Currency__mdt>([
                SELECT Id, Name__c
                FROM Appirio_core_Currency__mdt
        ]);

        List<Appirio_core_Currency_Exchange_Rate__mdt> currencyExchangeRatesMdt = new List<Appirio_core_Currency_Exchange_Rate__mdt>([
                SELECT Id, Name__c
                FROM Appirio_core_Currency_Exchange_Rate__mdt
        ]);

        Test.startTest();
        //Run the logic of auto population
        InitCustomMetadataType.runInsertCustomMetadata();
        Test.stopTest();

        List<appirio_core__Config_Group__c> configGroups = new List<appirio_core__Config_Group__c>([
                SELECT Id, Name FROM appirio_core__Config_Group__c
        ]);

        List<appirio_core__Config_Option__c> configOptions = new List<appirio_core__Config_Option__c>([
                SELECT Id, Name FROM appirio_core__Config_Option__c
                ]);

        List<appirio_core__Config_Value__c> configValues = new List<appirio_core__Config_Value__c>([
                SELECT Id, Name FROM appirio_core__Config_Value__c
        ]);

        List<Appirio_core__Currency__c> currencies = new List<Appirio_core__Currency__c>([
                SELECT Id, Name FROM Appirio_core__Currency__c
        ]);

        List<appirio_core__Currency_Exchange_Rate__c> currencyExchangeRates = new List<appirio_core__Currency_Exchange_Rate__c>([
                SELECT Id, Name FROM appirio_core__Currency_Exchange_Rate__c
        ]);

        System.assertEquals(configurationGroupsMdt.size(),configGroups.size());
        System.assertEquals(configurationOptionsMdt.size(),configOptions.size());
        System.assertEquals(currenciesMdt.size(),currencies.size());
        System.assertEquals(currencyExchangeRatesMdt.size(),currencyExchangeRates.size());
    }
}
