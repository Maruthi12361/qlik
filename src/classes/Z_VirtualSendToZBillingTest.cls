/*    Copyright (c) 2018 Zuora, Inc.
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy of 
 *   this software and associated documentation files (the "Software"), to use copy, 
 *   modify, merge, publish the Software and to distribute, and sublicense copies of 
 *   the Software, provided no fee is charged for the Software.  In addition the
 *   rights specified above are conditioned upon the following:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   Zuora, Inc. or any other trademarks of Zuora, Inc.  may not be used to endorse
 *   or promote products derived from this Software without specific prior written
 *   permission from Zuora, Inc.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 *   ZUORA, INC. BE LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
 *
 *   IN THE EVENT YOU ARE AN EXISTING ZUORA CUSTOMER, USE OF THIS SOFTWARE IS GOVERNED
 *
 *   BY THIS AGREEMENT AND NOT YOUR MASTER SUBSCRIPTION AGREEMENT WITH ZUORA.
 */
@isTest(SeeAllData=true)
public with sharing class Z_VirtualSendToZBillingTest {


    private static Account account;
    private static Contact contact;
    private static Zuora__CustomerAccount__c customerAccount;
    private static Opportunity opp;
    private static zqu__Quote__c quote;
    private static Product2 product;
    private static zqu__ProductRatePlan__c productRatePlan;
    private static zqu__QuoteAmendment__c quoteAmendment;
    private static zqu__QuoteRatePlan__c quoteRatePlan;

     //Address information
    public static String ADDR_COUNTRY     = 'United States';
    public static String ADDR_STATE       = 'California';
    public static final String ADDR_CITY        = 'Foster City';
    public static final String ADDR_STREET  = '1051 E Hillsdale Blvd';
    public static String ADDR_ZIP         = '94404';
    

    private static void set_up_data() {
        //account = Z_TestFactory.buildAccount();
        //insert account;

        Subsidiary__c subid = TestQuoteUtil.createSubsidiary();
        //QlikTech_Company__c QTCompId=TestQuoteUtil.createQlikTech('France', 'FR', subid.id);
        //Account acc = TestQuoteUtil.createAccount(QTCompId.Id);
        Account acc =Z_TestFactory.makeAccount();
        insert acc;
         acc.Pending_Validation__c =FALSE;
        update acc;
        Account reseller = Z_TestFactory.buildAccount2();
        insert reseller;
        acc.Pending_Validation__c =FALSE;
        update reseller;
        Contact con = TestQuoteUtil.createContact(reseller.id);
        con.FirstName = 'DTNA';
        con.LastName = 'Support';
        con.Email = 'email@mail.me';
        con.MailingCountry = ADDR_COUNTRY;
        //testContact.MailingCountryCode = 'US';
        con.MailingState = ADDR_STATE;
        //testContact.MailingStateCode = 'CA';
        con.MailingPostalCode = ADDR_ZIP;
        con.MailingStreet = ADDR_STREET;
        con.MailingCity = ADDR_CITY;
        con.Phone = '404-444-4444';
        upsert con;

        Opportunity opp =TestQuoteUtil.createOpportunity(acc, reseller);

        //Contact con = createTestContact(acc, true);

        //Opportunity opp =createOpportunity();
        //Opportunity opp = createTestOpportunity(acc, true);
        //zqu__Quote__c quote = createTestQuote(acc, opp, con, con, true);



        //contact = Z_TestFactory.makeContact(account);
        //insert contact;

        customerAccount = Z_TestFactory.createBillingAccount(acc, 'id0123456789KK');
        insert customerAccount;

        /*Nov 5th: updated based on the code*/
        //opp = Z_TestFactory.buildOpportunity(account, contact);

        //opp = Z_TestFactory.makeOpportunity(account, contact);
        //insert opp;

        product = Z_TestFactory.makeProduct();
        product.Training_Product__c = 'Yes';
        insert product;

        productRatePlan = Z_TestFactory.makeProductRatePlan(product.Id, 'ratePlanId1');
        insert productRatePlan;

        quote = Z_TestFactory.makeQuote2(opp, acc);
        quote.zqu__BillToContact__c = con.id;
        quote.zqu__SoldToContact__c = con.id;
        insert quote;

        quoteAmendment = Z_TestFactory.makeQuoteAmendment(quote);
        insert quoteAmendment;

        quoteRatePlan = Z_TestFactory.makeQRP(quote, quoteAmendment, productRatePlan);
        insert quoteRatePlan;

    }


    static testmethod void success(){
        set_up_data();

        Z_SendToZBilling sender = new Z_SendToZBilling();
        Set<String> quoteList = new Set<String>{quote.id};
        Test.startTest();
        sender.run(quoteList);      
        Test.stopTest();
        quote = [Select Quote_Submit_Status__c FROM zqu__Quote__c WHERE id = :quote.id];
        System.assertEquals(quote.Quote_Submit_Status__c, 'Sent to Z-Billing');
    }

    static testmethod void success_test_instantiator(){
        set_up_data();

        Test.startTest();
        Z_SendToZBillingInstantiator.sendToZuora(quote.id);      
        Test.stopTest();
    }
}