/********************************************************
* CLASS: WebActivityHandler
* DESCRIPTION: Handler class for Web Activity
*
* CHANGELOG:    
*   2018-10-15 - BAD - Added Initial logic
*   2019-06-14 - CRW - Added logic for event type lead flow intake
*   2019-10-22 - BAD - BMW-1730 - enabled Zift Partner Preregistration
*********************************************************/
public class WebActivityHandler {
    
    public WebActivityHandler() {}

    public static void handleBeforeInsert(List<Web_Activity__c> triggerNew){
        Map<String, Id> mapUsernameToULC = new Map<String, Id>();
        Set<String> setUsernames = new Set<String>();
        List<WebActivityUtils.WebActivityContent> lstWATypeLeadGeneration = new List<WebActivityUtils.WebActivityContent>();
        List<WebActivityUtils.WebActivityContent> lstWATypeRecordDownload = new List<WebActivityUtils.WebActivityContent>();

        System.debug('WebActivity Handler start');

        for(Web_Activity__c wa: triggerNew){
            if(String.isNotBlank(wa.Content__c)){
                try{
                    //deserialize Content JSON string for additional params
                    WebActivityUtils.WebActivityContent WAcontent = new WebActivityUtils.WebActivityContent ();

                    //Added to remove escape chars coming in content due to Boomi
                    if (wa.Type__c == 'preregistration.trial.created'){
                        String[] parts = wa.Content__c.split('"context"');
                        String charcleanup = parts[1].replaceAll('\n','');
                        String txt = '"context"';
                        wa.Content__c = parts[0] + txt + charcleanup;
                    }
                    wa.Audit_Trail__c = '';
                    WAcontent = (WebActivityUtils.WebActivityContent) JSON.deserialize (wa.Content__c, WebActivityUtils.WebActivityContent.class);  
					WAcontent.MessageID =  wa.Name;
                    system.debug('WACONTENT before ' + WAcontent);

                    switch on wa.Type__c {
                        when 'LeadGeneration','com.qlik.snowflake.leadgeneration' {
                            System.debug('WebActivityHandler - Type is LeadGeneration');
							
							if (String.isNotBlank(wa.Sender__c) && wa.Sender__c == 'QlikId')
							{
		                        WebActivityUtils.ULCData ULCData = new WebActivityUtils.ULCData();
		                        ULCData = (WebActivityUtils.ULCData) JSON.deserialize (wa.Content__c, WebActivityUtils.ULCData.class);
		                        WAcontent.OUser = new WebActivityUtils.ULCUser(ULCData.OUser);
		                        WAcontent.AdditParams =  WebActivityUtils.AdditionalParamsSplit(ULCData.additionalParams);
							}
							else if (String.isNotBlank(wa.Sender__c) && wa.Sender__c == 'qlik.com')
							{
                                WebActivityUtils.W2LContent content = new WebActivityUtils.W2LContent();
                                content = (WebActivityUtils.W2LContent) JSON.deserialize (wa.Content__c, WebActivityUtils.W2LContent.class);
                                WAcontent.OUser = new WebActivityUtils.ULCUser(content);
                                WAcontent.AdditParams = new WebActivityUtils.AdditionalParams(content);
                                WAcontent.Sender = wa.Sender__c ;
							}
							else if (String.isNotBlank(wa.Sender__c) && wa.Sender__c == 'snowflake')
							{
                                WebActivityUtils.LeadData content = new WebActivityUtils.LeadData();
                                content = (WebActivityUtils.LeadData) JSON.deserialize (wa.Content__c, WebActivityUtils.LeadData.class);
                                WAcontent.OUser = new WebActivityUtils.ULCUser(content);
                                WAcontent.AdditParams = new WebActivityUtils.AdditionalParams(content);
                                WAcontent.Sender = wa.Sender__c ;
							}							
                            lstWATypeLeadGeneration.add(WAcontent);
                        }
                        when 'preregistration.trial.created','invited.user.detail' {
                            if(String.isNotBlank(wa.Sender__c) && (wa.Sender__c.toLowerCase() == 'product' || wa.Sender__c.toLowerCase() == 'qlik.com' || wa.Sender__c.toLowerCase() == 'partner')){
                                System.debug('WebActivityHandler - Type is DBJ');
                                WebActivityUtils.auth0 content = new WebActivityUtils.auth0();
                                content = (WebActivityUtils.auth0) JSON.deserialize (wa.Content__c, WebActivityUtils.auth0.class);
                                WAcontent.OUser = new WebActivityUtils.ULCUser(content);
                                WAcontent.AdditParams = new WebActivityUtils.AdditionalParams(content);
                                WAcontent.Sender = wa.Sender__c ;
                                WAcontent.WebActivityType = wa.Type__c ;
                                lstWATypeLeadGeneration.add(WAcontent);
                            }
                        }
                        when 'RecordDownload' {
							System.debug('WebActivityHandler - Type is RecordDownload');

                            WebActivityUtils.ULCData ULCData = new WebActivityUtils.ULCData();
                            ULCData = (WebActivityUtils.ULCData) JSON.deserialize (wa.Content__c, WebActivityUtils.ULCData.class);
                            
                            WAcontent.OUser = ULCData.OUser;
                            WAcontent.AdditParams =  WebActivityUtils.AdditionalParamsSplit(ULCData.additionalParams);

                            lstWATypeRecordDownload.add(WAcontent);
                        }                          
                        when 'QSC_FirstLogin', 'QSD_FirstLogin' {
                            wa.ULC_Username__c = WAcontent.ULCUsername;
                            wa.TimeStamp__c = WAcontent.TimeStamp;
                            setUsernames.add(WAcontent.ULCUsername);
                        }
                    }
                    System.debug('WebActivityHandler handleBeforeInsert, after switch, WAcontent = ' + WAcontent);
                } 
                catch (System.Exception ex){
                    wa.Status__c = 'Failed';
                    wa.Audit_Trail__c = 'Error - HandleBeforeInsert: Error Deserializing Content JSON string - ' + ex.getMessage(); 
                }
            }
            else{
                wa.Audit_Trail__c = 'Warning - HandleBeforeInsert: Content field is not set'; //TODO: add err message in Custom Setting
            }
        }


        //Start Lead Generation
        if(lstWATypeLeadGeneration.size() > 0){        
            lstWATypeLeadGeneration = WebActivityLeadGeneration.SetUser(lstWATypeLeadGeneration);
            Map<String, Integer> mapMessageIdToWAContentIndex = new  Map<String, Integer>();    
            Integer i = 0;
            
            //Build a map on messageID to Index on the list of web activity used by the Lead Generation            
            for(WebActivityUtils.WebActivityContent waContent : lstWATypeLeadGeneration)
            {
                mapMessageIdToWAContentIndex.put(waContent.MessageID, i);
                i++;
            }
			
            //loop through trigger and update fields, associate Web Activty to Lead/Contact 
            for(Web_Activity__c wa: triggerNew)
			{
                i = mapMessageIdToWAContentIndex.get(wa.Name);
                if (String.isNotBlank(lstWATypeLeadGeneration[i].OUser.LeadID)) {wa.LeadId__c = lstWATypeLeadGeneration[i].OUser.LeadID;}
                if (String.isNotBlank(lstWATypeLeadGeneration[i].OUser.ContactID)) {wa.ContactId__c = lstWATypeLeadGeneration[i].OUser.ContactID;}
                if (String.isNotBlank(lstWATypeLeadGeneration[i].OUser.ULCDetailsID)) {wa.ULC__c = lstWATypeLeadGeneration[i].OUser.ULCDetailsID;}
                if(lstWATypeLeadGeneration[i].Status != 'Failed')
                {
                    wa.Status__c = 'Completed';
                }
                else
                {
					wa.Status__c = 'Failed';
					wa.Audit_Trail__c = lstWATypeLeadGeneration[i].AuditTrail; 
				}
                System.debug('Final Audit trail: ' + lstWATypeLeadGeneration[i].AuditTrail);
                System.debug('Final OUSer ' + lstWATypeLeadGeneration[i].OUser);
			}
        }

        //Start Record Download
        if(lstWATypeRecordDownload.size() > 0){        
            lstWATypeRecordDownload = WebActivityRecordDownload.RecordDownload(lstWATypeRecordDownload);
            System.debug('RecodDownload Final ' + lstWATypeRecordDownload[0].OUser);
            Map<String, Integer> mapMessageIdToWAContentIndex = new  Map<String, Integer>();
            Integer i = 0;
            //Build a map on messageID to Index on the list of web activity used by the Lead Generation            
            for(WebActivityUtils.WebActivityContent waContent : lstWATypeRecordDownload)
            {
                mapMessageIdToWAContentIndex.put(waContent.MessageID, i);
                i++;
            }
            //loop through trigger and update fields, associate Web Activty to Lead/Contact 
            for(Web_Activity__c wa: triggerNew)
			{
                i = mapMessageIdToWAContentIndex.get(wa.Name);
                if (String.isNotBlank(lstWATypeRecordDownload[i].OUser.LeadID)) {wa.LeadId__c = lstWATypeRecordDownload[i].OUser.LeadID;}
                if (String.isNotBlank(lstWATypeRecordDownload[i].OUser.ContactID)) {wa.ContactId__c = lstWATypeRecordDownload[i].OUser.ContactID;}
                if (String.isNotBlank(lstWATypeRecordDownload[i].OUser.ULCDetailsID)) {wa.ULC__c = lstWATypeRecordDownload[i].OUser.ULCDetailsID;}
                if(lstWATypeRecordDownload[i].Status != 'Failed')
                {
                    wa.Status__c = 'Completed';
                }
                else
                {
					wa.Status__c = 'Failed';
					wa.Audit_Trail__c = lstWATypeRecordDownload[i].AuditTrail; 
				}
                System.debug('Final Audit trail: ' + lstWATypeRecordDownload[i].AuditTrail);
                System.debug('Final OUSer ' + lstWATypeRecordDownload[i].OUser);
			}
        }

        //First Login QSC/QSD
        if(setUsernames.size() > 0)
        {
            //Set up the map username to ULC
            for(ULC_Details__c ulc : [Select id, ULCName__c from ULC_Details__c where ULCName__c in :setUsernames]){
                mapUsernameToULC.put(ulc.ULCName__c, ulc.Id);
            }

            //Create the lookup between WebActivity and ULCDetails
            for(Web_Activity__c wa: triggerNew){
                wa.ULC__c = mapUsernameToULC.get(wa.ULC_Username__c);
            }
        }

    }

    public static void handleAfterInsert(List<Web_Activity__c> triggerNew){
        List<Web_Activity__c> lstWATypeQSCFirstLogin  = new List<Web_Activity__c>();
        List<Web_Activity__c> lstWATypeQSDFirstLogin  = new List<Web_Activity__c>();

        for (Integer i = 0; i < triggerNew.size(); i++){
            // Split into different lists depending on Web Activity Type
            switch on triggerNew[i].Type__c  {
                when 'QSC_FirstLogin' {
                    lstWATypeQSCFirstLogin.add(triggerNew[i]);
                }
                when 'QSD_FirstLogin' {
                    lstWATypeQSDFirstLogin.add(triggerNew[i]);
                }
            }
        }

        System.debug('FirstLoginSize ' + lstWATypeQSCFirstLogin.size() + ':' + lstWATypeQSDFirstLogin.size());

        //Process QSC FirstLogins - with batch
        if(lstWATypeQSCFirstLogin.size() > 0){
            //Check for max 5 batch size limit before starting the batch
            Boolean isExecuting = (([SELECT COUNT() FROM AsyncApexJob WHERE ApexClassId IN (
                                            SELECT Id FROM ApexClass WHERE Name = 'QSC_FirstLogin')
                                    ]) <= 5) ? false : true ;
            if(!isExecuting){
                Database.executeBatch(new CreateQSLoginDetails(lstWATypeQSCFirstLogin, 'QSC_FirstLogin'), 100);  
            }
        }
        //Process QSD FirstLogins - with batch
        if(lstWATypeQSDFirstLogin.size() > 0){
            //Check for max 5 batch size limit before starting the batch
            Boolean isExecuting = (([SELECT COUNT() FROM AsyncApexJob WHERE ApexClassId IN (
                                            SELECT Id FROM ApexClass WHERE Name = 'QSD_FirstLogin')
                                    ]) <= 5) ? false : true ;
            if(!isExecuting){
                Database.executeBatch(new CreateQSLoginDetails(lstWATypeQSDFirstLogin, 'QSD_FirstLogin'), 100);  
            }
        }   
    }
}