//*********************************************************/
// Author: Mark Cane&
// Creation date: 18/08/2010
// Intent:  
//          
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efPickLists{
   public List<SelectOption> getCountryList(){       
        Map<String,String> configMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);
        
        String countriesString = configMap.get('countryList');
        List<String> countries = efUtility.splitStringToList(countriesString,'~');
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--Select One--'));
        
        for(String c:countries)
        {
            List<String> itemValue = efUtility.splitStringToList(c,'/');
            options.add(new SelectOption(itemValue[0],itemValue[1]));   
        }
        
        return options;
    }
    
    public List<SelectOption> getEmployeeList(){        
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Account.Company_Size__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        options.add(new SelectOption('','-- Select one --'));
        
        for( Schema.PicklistEntry f : ple){
          options.add(new SelectOption(f.getValue(), f.getLabel()));
        }               
        return options;
    }
        
    public List<SelectOption> getIndustryList(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select One --'));
        
        Schema.DescribeSObjectResult accnt =  Account.SObjectType.getDescribe();
        Schema.DescribeFieldResult fieldResult = Account.Industry.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
          options.add(new SelectOption(f.getValue(), f.getLabel()));
        }               
        return options;     
    }
    
        public List<SelectOption> getPrimaryRoleList(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select One --'));
        
        Schema.DescribeSObjectResult reg =  Registration__c.SObjectType.getDescribe();
       Schema.DescribeFieldResult fieldResult = Registration__c.Primary_Role__c.getDescribe();
       
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
      for( Schema.PicklistEntry f : ple)
      {
        options.add(new SelectOption(f.getValue(), f.getLabel()));
      }              
       return options;      
    }

    public List<SelectOption> getWhyNoHotelList(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Select One --'));
        
        Schema.DescribeFieldResult fieldResult = Registration__c.Why_No_Hotel__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry f:ple){
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }            
        return options;
    }
    
    public List<SelectOption> getEmployeeDepartmentList(){
        Map<String,String> regMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);
        
        String rolesString = regMap.get('employeeDepartmentList');
        List<String> roles = efUtility.splitStringToList(rolesString,'~');
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','-- Select One --'));
        
        for(String r:roles){
            options.add(new SelectOption(r,r));
        }        
        return options;
    }   
            
    public List<SelectOption> getJobFunctionList(){
        Map<String,String> contactMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);
        
        String functionsString = contactMap.get('jobFunctionList');
        List<String> functions = efUtility.splitStringToList(functionsString,'~');
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','-- Select One --'));
        
        for(String f:functions){
            options.add(new SelectOption(f,f));
        }        
        return options;
    }
    
    public List<SelectOption> getJobLevelList(){
        //  Antonio : Code changed to incorporate Custom Settings
        Map<String,String> contactMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);
        
        String levelsString = contactMap.get('jobLevelList');
        List<String> levels = efUtility.splitStringToList(levelsString,'~');
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','-- Select One --'));
        
        for(String l:levels){
            options.add(new SelectOption(l,l));
        }        
        return options;
    }  
    
    public List<SelectOption> getTrackList(String eventId){
        List<SelectOption> options = new List<SelectOption>();
        
        List<Track__c> listtracks = [Select Id,Name From Track__c Where Event__c=:eventId Order By Name];
        options.add(new SelectOption('','-- Select One --'));
        
        if(listtracks.size() > 0){
            for(Track__c t:listtracks){
                options.add(new SelectOption(t.Id,t.Name));
            }
        }
        return options; 
    }
                
    public List<SelectOption> getEventDateList(Date startDate, Date endDate){       
        if(startDate==null || endDate==null) return null;
        Integer month = startDate.month();
        Integer day = startDate.day();
        Integer year = startDate.year();        
        DateTime dtStart = DateTime.newInstance(year, month, day, 0, 0, 0);
        
        month = endDate.month();
        day = endDate.day();
        year = endDate.year();  
        DateTime dtEnd = DateTime.newInstance(year, month, day, 0, 0, 0);
        
        List<SelectOption> options = new List<SelectOption>();      
        String currDate = '';
        
        do {            
            currDate = dtStart.format('MM/dd/yyyy');
            options.add(new SelectOption(currDate,currDate));
            dtStart = dtStart.addDays(1);
        } while (dtStart < dtEnd.addDays(1));
        return options;
    }
    
    public List<SelectOption> getEventExtendedStayArrivalList(Date startDate, Date endDate){        
        if(startDate==null || endDate==null) return null;
                
        Integer month = startDate.month();
        Integer day = startDate.day();
        Integer year = startDate.year();        
        DateTime dtStart = DateTime.newInstance(year, month, day, 0, 0, 0);
        
        month = endDate.month();
        day = endDate.day();
        year = endDate.year();  
        DateTime dtEnd = DateTime.newInstance(year, month, day, 0, 0, 0);       
        
        List<SelectOption> options = new List<SelectOption>();      
        String currDate = '';
        
        do {            
            currDate = dtStart.format('MM/dd/yyyy');
            options.add(new SelectOption(currDate,currDate));
            dtStart = dtStart.addDays(1);
        } while (dtStart < dtEnd);
        return options;
    }
    
    public List<SelectOption> getEventExtendedStayDepartureList(Date startDate, Date endDate){      
        if(startDate==null || endDate==null) return null;
        Integer month = startDate.month();
        Integer day = startDate.day();
        Integer year = startDate.year();        
        DateTime dtStart = DateTime.newInstance(year, month, day, 0, 0, 0);
        
        month = endDate.month();
        day = endDate.day();
        year = endDate.year();  
        DateTime dtEnd = DateTime.newInstance(year, month, day, 0, 0, 0);
        
        List<SelectOption> options = new List<SelectOption>();      
        String currDate = '';
        
        do {
            dtStart = dtStart.addDays(1);           
            currDate = dtStart.format('MM/dd/yyyy');
            options.add(new SelectOption(currDate,currDate));           
        } while (dtStart < dtEnd.addDays(1));
        return options;
    }       

    public List<SelectOption> getUsStateList(){     
        List<SelectOption> options = new List<SelectOption>();          
                options.add(new SelectOption('','-- Select One --'));
                options.add(new SelectOption('AL','Alabama'));
                options.add(new SelectOption('AK','Alaska'));
                options.add(new SelectOption('AZ','Arizona'));
                options.add(new SelectOption('AR','Arkansas'));
                options.add(new SelectOption('CA','California'));
                options.add(new SelectOption('CO','Colorado'));
                options.add(new SelectOption('CT','Connecticut'));
                options.add(new SelectOption('DE','Delaware'));
                options.add(new SelectOption('DC','District of Columbia'));
                options.add(new SelectOption('FL','Florida'));
                options.add(new SelectOption('GA','Georgia'));
                options.add(new SelectOption('HI','Hawaii'));
                options.add(new SelectOption('ID', 'Idaho'));
                options.add(new SelectOption('IL','Illinois'));
                options.add(new SelectOption('IN','Indiana'));
                options.add(new SelectOption('IA','Iowa'));
                options.add(new SelectOption('KS','Kansas'));
                options.add(new SelectOption('KY','Kentucky'));
                options.add(new SelectOption('LA','Louisiana'));
                options.add(new SelectOption('ME','Maine'));
                options.add(new SelectOption('MD','Maryland'));
                options.add(new SelectOption('MA','Massachusetts'));
                options.add(new SelectOption('MI','Michigan'));
                options.add(new SelectOption('MN','Minnesota'));
                options.add(new SelectOption('MS','Mississippi'));
                options.add(new SelectOption('MO','Missouri'));
                options.add(new SelectOption('MT','Montana'));
                options.add(new SelectOption('NE','Nebraska'));
                options.add(new SelectOption('NV','Nevada'));
                options.add(new SelectOption('NH','New Hampshire'));
                options.add(new SelectOption('NJ','New Jersey'));
                options.add(new SelectOption('NM','New Mexico'));
                options.add(new SelectOption('NY','New York'));
                options.add(new SelectOption('NC','North Carolina'));
                options.add(new SelectOption('ND','North Dakota'));
                options.add(new SelectOption('OH','Ohio'));
                options.add(new SelectOption('OK','Oklahoma'));
                options.add(new SelectOption('OR','Oregon'));
                options.add(new SelectOption('PA','Pennsylvania'));
                options.add(new SelectOption('RI','Rhode Island'));
                options.add(new SelectOption('SC','South Carolina'));
                options.add(new SelectOption('SD','South Dakota'));
                options.add(new SelectOption('TN','Tennessee'));
                options.add(new SelectOption('TX','Texas'));
                options.add(new SelectOption('UT','Utah'));
                options.add(new SelectOption('VT','Vermont'));
                options.add(new SelectOption('VA','Virginia'));
                options.add(new SelectOption('WA','Washington'));
                options.add(new SelectOption('WV','West Virginia'));
                options.add(new SelectOption('WI','Wisconsin'));
                options.add(new SelectOption('WY','Wyoming'));
      
        return options;
    }
    
    public List<SelectOption> getCaStateList() 
    {       
        List<SelectOption> options = new List<SelectOption>();                  
                options.add(new SelectOption('','-- Select One --'));
                options.add(new SelectOption('AB','Alberta'));
                options.add(new SelectOption( 'BC','British Columbia') );
                options.add(new SelectOption('MB','Manitoba'));
                options.add(new SelectOption( 'NB','New Brunswick'));
                options.add(new SelectOption('NL','Newfoundland'));
                options.add(new SelectOption('NT','Northwest Territories'));
                options.add(new SelectOption( 'NS','Nova Scotia'));
                options.add(new SelectOption('NU','Nunavut'));
                options.add(new SelectOption('ON','Ontario'));
                options.add(new SelectOption('PE','Prince Edward Island'));
                options.add(new SelectOption('QC','Quebec'));
                options.add(new SelectOption('SK','Saskatchewan'));
                options.add(new SelectOption('YT','Yukon'));
                return options;
    }
        
    public List<SelectOption> getTracklist(){
        Map<String,String> configMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','TRACK'));
                
        List<Track__c> tracklist = new List<Track__c>();
        tracklist = [SELECT Name FROM Track__c where Event__c=:configMap.get('eventId') ORDER BY Name ASC] ;
        
        if (tracklist.Size() >0){
            for(Track__c t:tracklist){
                if (t.Name != null)
                  options.add(new SelectOption(t.Name,t.Name));
            }
        }
         return options; 
    }
}