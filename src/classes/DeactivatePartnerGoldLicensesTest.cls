/*************************************************************************************************************
 Name: DeactivatePartnerGoldLicensesTest
 Author: CCE
 Purpose: This is test class for DeactivatePartnerGoldLicenses
  
 Log History:
 2019-08-12 BMW-1663 Initial Development
*************************************************************************************************************/
@isTest
private class DeactivatePartnerGoldLicensesTest {
	
	static testmethod void DeactivatePartnerGoldLicenses_Test1()
    {
        System.debug('DeactivatePartnerGoldLicensesTest: Starting');

        Test.startTest();
        DeactivatePartnerGoldLicenses.scheduleMeDaily();
        DeactivatePartnerGoldLicenses dpgl = new DeactivatePartnerGoldLicenses();

        dpgl.execute(null);  
        
        Test.stopTest();

        System.debug('DeactivatePartnerGoldLicensesTest: Finishing');        
    }
	
}