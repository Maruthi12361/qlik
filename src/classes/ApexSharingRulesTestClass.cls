/************************************************************************************************
	2016-01-12	TJG		CR# 42998 https://eu1.salesforce.com/a0CD000000uWSH5?srPos=0&srKp=a0C
						Partner Sharing project. Now call APEX sharing class
						* 02.09.2017 : Srinivasan PR- fix for query error
	2019-02-20  BAD - Qlik Commerce - removing sharing Account, Contact, Opp						
 ************************************************************************************************/
@isTest
private class ApexSharingRulesTestClass {

	/*
	 *  TEST SETUP
	 */
	static final integer NBR_OF_ACCOUNTS = 50;
	static final integer NBR_OF_LEADS = 10;

	static testMethod void test_CreateResponseStructuresToGainTestCoverage()
	{
		
		test.startTest();
		
		sharinghandlerQlikviewCom.AccountSharingRulesOnPartnerPortalAccountsResponse_element Element1 = new sharinghandlerQlikviewCom.AccountSharingRulesOnPartnerPortalAccountsResponse_element();
		sharinghandlerQlikviewCom.CaseSharingOnAccountOriginResponse_element Element2 = new sharinghandlerQlikviewCom.CaseSharingOnAccountOriginResponse_element();
		sharinghandlerQlikviewCom.OpportunitySellThroughPartnerResponse_element Element3 = new sharinghandlerQlikviewCom.OpportunitySellThroughPartnerResponse_element(); 		 
		sharinghandlerQlikviewCom.CaseSharingOnAccountOrigin_element caseSonAorigin = new sharinghandlerQlikviewCom.CaseSharingOnAccountOrigin_element();
		sharinghandlerQlikviewCom.OpportunitySellThroughPartner_element ostp = new sharinghandlerQlikviewCom.OpportunitySellThroughPartner_element();
		sharinghandlerQlikviewCom.PartnerLeadShareWithCreaterAfterSubmission_element plshare = new sharinghandlerQlikviewCom.PartnerLeadShareWithCreaterAfterSubmission_element();
		sharinghandlerQlikviewCom.PartnerLeadShareWithCreaterAfterSubmissionResponse_element plresp = new sharinghandlerQlikviewCom.PartnerLeadShareWithCreaterAfterSubmissionResponse_element();

		test.stopTest();
	
	}

	static testMethod void test_LeadSharingRules() {
		System.debug('ApexSharingRulesTestClass.test_LeadSharingRules: TEST START');

		List<Lead> testLeads = new List<Lead>();

		for (Integer i=0; i<NBR_OF_LEADS; i++) {
			Lead alead = new Lead(email = 'em' + i +'@ytestcom.net',
				LastName = 'last' + i,
        		FirstName = 'First' + i,
        		Company = 'Company' + i, 
        		Country='Sweden',
        		Phone='3333',
        		MobilePhone='3333',
        		Switchboard__c='3333');
			testLeads.add(alead);
		}
		Test.startTest();
		Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
		try {

			Insert testLeads;
			sharinghandlerQlikviewCom.ArrayOfString leadIds = new sharinghandlerQlikviewCom.ArrayOfString();
			leadIds.string_x = new String[]{};

			for (Lead tld : testLeads) {
				leadIds.string_x.add(tld.Id);
			}

			sharinghandlerQlikviewCom.ServiceSoap svcSoap = new sharinghandlerQlikviewCom.ServiceSoap();

			ApexSharingRules.PartnerLeadShareWithCreaterAfterSubmission(LeadIds.string_x);

			svcSoap.PartnerLeadShareWithCreaterAfterSubmission(LeadIds);

        } catch (Exception ex) {
        	System.debug('ApexSharingRulesTestClass.test_LeadSharingRules: Caught exception: ' + ex.getMessage());
        }

		Test.stopTest();
		System.debug('ApexSharingRulesTestClass.test_LeadSharingRules: TEST STOP`'); 
	}


    static testMethod void test_AddPRMAccountSharingRules() {
          
        System.debug('ApexSharingRulesTestClass.test_AddPRMAccountSharingRules: TEST START'); 
        Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
        //string ownerId = [select Id from User where alias = 'ssh22' limit 1].Id;        
        User owner = QTTestUtils.createMockUserForProfile('System Administrator');
                     
        List<Account> testAccounts = new List<Account>();
        for (Integer i=0; i< NBR_OF_ACCOUNTS; i++) {
        	Account acc = new Account(OwnerId = owner.Id, Name = 'MHG Test Inc [' + (i+1) + ']');
        	testAccounts.add(acc);
        }
        
        try {
        	test.startTest();
        	
        	System.debug('ApexSharingRulesTestClass.test_AddPRMAccountSharingRules: Attempting to insert accounts into database');
        	System.debug(testAccounts);
        	insert testAccounts;
        	System.debug('ApexSharingRulesTestClass.test_AddPRMAccountSharingRules: Accounts inserted successful');	

			/*
			
			Since this functionally is ported to the web service -- we can't see the actual change
			since there will not be any outbound calls done from TestCases.
			
			List<AccountShare> accountShares = [select ID, AccountId, UserOrGroupId, RowCause, OpportunityAccessLevel, ContactAccessLevel, CaseAccessLevel, AccountAccessLevel from AccountShare  where AccountId in :testAccounts and RowCause = 'Manual'];
			System.assertEquals(NBR_OF_ACCOUNTS, accountShares.size());
			for (AccountShare accShare:accountShares) {
				System.assertEquals(accShare.ContactAccessLevel, 'Edit');
				System.assertEquals(accShare.OpportunityAccessLevel, 'Edit');
				System.assertEquals(accShare.CaseAccessLevel, 'Edit');
				System.assertEquals(accShare.AccountAccessLevel, 'Edit');					
			}
			System.debug(accountShares);
			*/
        	
        } catch (System.Exception e) {
        	System.debug('ApexSharingRulesTestClass.test_AddPRMAccountSharingRules: Caught exception: ' + e.getDmlMessage(0));
        }
        test.stopTest();
        System.debug('ApexSharingRulesTestClass.test_AddPRMAccountSharingRules: TESTS DONE');
    }
    

    static testMethod void test_AddCPAccountSharingRules() {
                
        System.debug('ApexSharingRulesTestClass.test_AddCPAccountSharingRules: TEST START');        
                        
        try {
        	test.startTest();
        	Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
	        //Account account = [select Id from Account where Name = '3M Company' limit 1];
	        User aUser = QTTestUtils.createMockUserForProfile('System Administrator');
	        Account account = QTTestUtils.createMockAccount('3M Company', aUser, true);        
        	
        	System.debug('ApexSharingRulesTestClass.test_AddCPAccountSharingRules: Attempting to update account');
			update account;                        
			account = [select Id from Account where Id = :account.Id];
           	System.debug('ApexSharingRulesTestClass.test_AddCPAccountSharingRules: Account updated successful');	

			/*
			Since this functionally is ported to the web service -- we can't see the actual change
			since there will not be any outbound calls done from TestCases.

			List<AccountShare> accountShares = [select ID, AccountId, UserOrGroupId, RowCause, OpportunityAccessLevel, ContactAccessLevel, CaseAccessLevel, AccountAccessLevel from AccountShare  where AccountId = :account.Id and RowCause = 'Manual'];
			System.assert(accountShares.size() > 0);
			for (AccountShare accShare:accountShares) {
				if (accShare.ContactAccessLevel == 'None') {
					System.debug('ApexSharingRulesTestClass.test_AddCPAccountSharingRules: Testing sharing rule for CP portal');
					System.assertEquals(accShare.ContactAccessLevel, 'None');
					System.assertEquals(accShare.OpportunityAccessLevel, 'None');
					System.assertEquals(accShare.CaseAccessLevel, 'Edit');
					System.assertEquals(accShare.AccountAccessLevel, 'Read');
				} else {
					System.debug('ApexSharingRulesTestClass.test_AddCPAccountSharingRules: Testing sharing rule for PRM - Responsile partner');
					System.assertEquals(accShare.ContactAccessLevel, 'Edit');
					System.assertEquals(accShare.OpportunityAccessLevel, 'None');
					System.assertEquals(accShare.CaseAccessLevel, 'Edit');
					System.assertEquals(accShare.AccountAccessLevel, 'Edit');					
				}					
			}
			System.debug(accountShares);
        	*/
        	
        } catch (System.Exception e) {
        	System.debug('ApexSharingRulesTestClass.test_AddCPAccountSharingRules: Caught exception: ' + e.getDmlMessage(0));
        }
        test.stopTest();
        System.debug('ApexSharingRulesTestClass.test_AddCPAccountSharingRules: TESTS DONE');
    }
/*
//removed when Parter Community implemented
    static testMethod void test_AddOpportunitySharingRules() {
                
        System.debug('ApexSharingRulesTestClass.test_AddOpportunitySharingRules: TEST START');        
                        
        try {
        	test.startTest();
        	
	        Contact PartnerContact = [select Id, AccountId from Contact where Account.Name = 'QGate Software Ltd' limit 1];        
	        Contact contact = [select Id from Contact where Account.Name = 'Climber AB' and Name = 'Per Benteke' limit 1];
	        Opportunity testNewOpp = New Opportunity (
	        	Short_Description__c = 'TestOpp - delete me',
	        	Name = 'TestOpp - Delte me',
	        	Type = 'New Customer',
	        	Revenue_Type__c = 'Reseller',
	        	CloseDate = Date.today(),
	        	StageName = 'Alignment Meeting',
	        	RecordTypeId = '01220000000DNwY',
	        	CurrencyIsoCode = 'GBP',
	        	AccountId = PartnerContact.AccountId,
	        	Sell_Through_Partner__c = PartnerContact.AccountId,
	        	Partner_Contact__c = PartnerContact.Id,
	        	Referring_Contact__c = contact.id,
	        	Opportunity_Origin__c = 'Referral Opportunity'
	        );           	
        	
        	System.debug('ApexSharingRulesTestClass.test_AddOpportunitySharingRules: Attempting to insert opportunity');
			insert testNewOpp;                        
			System.debug('ApexSharingRulesTestClass.test_AddOpportunitySharingRules: Created Opportunity ' + testNewOpp.Id);
			
			
			//These had to be removed since we now call an external web service.
			
			//List<OpportunityShare> opportunityShares = [select ID, OpportunityId, UserOrGroupId, RowCause, OpportunityAccessLevel from OpportunityShare  where OpportunityId = :testNewOpp.Id and RowCause = 'Manual'];
			//System.assert(opportunityShares.size() > 0);
			//for (OpportunityShare oppShare:opportunityShares) {
					//System.debug('ApexSharingRulesTestClass.test_AddOpportunitySharingRules: Testing sharing rule for Opportunity Sell Through Partner');
					//System.assertEquals(oppShare.OpportunityAccessLevel, 'Edit');
					
			//}
			//System.debug(opportunityShares);
			 
			update testNewOpp;

			//sharinghandlerQlikviewCom.ServiceSoap svcSoap = new sharinghandlerQlikviewCom.ServiceSoap();
			//sharinghandlerQlikviewCom.ArrayOfString oppIds = new sharinghandlerQlikviewCom.ArrayOfString();
			//oppIds.string_x = new String[]{};
			//oppIds.string_x.add(testNewOpp.Id);
			//ApexSharingRules.AddPRMOpportunitySharingRulesOnSellThroughPartner(oppIds.string_x);
			//svcSoap.OpportunitySellThroughPartner(oppIds);
        	
			List<Id> oppsIds = new List<Id>();
			oppsIds.add(testNewOpp.Id);
			ApexSharingRules.UpdateOpportunitySharingForTest(oppsIds);
        } catch (System.Exception e) {
        	System.debug('ApexSharingRulesTestClass.test_AddOpportunitySharingRules: Caught exception: ' + e.getMessage());
        }
        test.stopTest();
        System.debug('ApexSharingRulesTestClass.test_AddOpportunitySharingRules: TESTS DONE');
    }
*/    
    
    static testMethod void test_AddCaseSharingOnAccountOrigin() {
                
        System.debug('ApexSharingRulesTestClass.test_AddCaseSharingOnAccountOrigin: TEST START');        
                        
        try {
        	test.startTest();
        	
        	List<Id> caseIds = new List<Id>();
	        Account Partner = [select Id from Account where Name = 'QGate Software Ltd' limit 1];        
	        Account Customer = [select Id from Account where Name = 'Dako Denmark A/S' limit 1];        
        	Case TheCase = new Case(Subject = 'Test Account', AccountId = Partner.Id, Account_Origin__c = Customer.Id);
        	
        	System.debug('ApexSharingRulesTestClass.test_AddCaseSharingOnAccountOrigin: Attempting to insert opportunity');
			insert TheCase;                        
			System.debug('ApexSharingRulesTestClass.test_AddCaseSharingOnAccountOrigin: Created Opportunity ' + TheCase.Id);
			update TheCase;

			caseIds.add(TheCase.Id);
			//sharinghandlerQlikviewCom.ServiceSoap svcSoap = new sharinghandlerQlikviewCom.ServiceSoap();
			//sharinghandlerQlikviewCom.ArrayOfString caseIds = new sharinghandlerQlikviewCom.ArrayOfString();
			//caseIds.string_x = new String[]{};
			//caseIds.string_x.add(TheCase.Id);
			//ApexSharingRules.AddCaseSharingOnAccountOrigin(caseIds.string_x);
			//svcSoap.CaseSharingOnAccountOrigin(caseIds);
			ApexSharingRules.UpdateCaseSharingForTest(caseIds);        	
        } catch (Exception ex) {
        	System.debug('ApexSharingRulesTestClass.test_AddCaseSharingOnAccountOrigin: Caught exception: ' + ex.getMessage());
        }
        test.stopTest();
        System.debug('ApexSharingRulesTestClass.test_AddCaseSharingOnAccountOrigin: TESTS DONE');
    }

/*
//removed when Parter Community implemented
    static testMethod void testUpdateAccountSharing(){
    	test.startTest();
    	List<Id> Accounts = new List<Id>(); 
    	User user = QTTestUtils.createMockUserForProfile('System Administrator'); 
    	Account account = QTTestUtils.createMockAccount('Anna andersson', user, true);
    	accounts.add(account.Id);
    	ApexSharingRules.updateAccountSharingForTest(accounts);
    	List<AccountShare> accountshares = [SELECT Id, AccountId FROM AccountShare WHERE RowCause = 'Manual' and AccountId = : account.Id];
    	System.assertEquals(0,accountshares.size());
    	test.stopTest();
    }
*/    

    static testMethod void testUpdateNSSupportContractSharing(){
    	test.startTest();
    	NS_Support_Contract__c contract = createNSSupportContract();
        List<Id> contracts = new List<Id>();
        contracts.add(contract.Id);
        List<Id> endUsers = new List<Id>();
        endUsers.add(contract.End_User__c);

       	ApexSharingRules.updateNSSupportContractSharingForTest(endUsers, contracts);
        
        List<NS_Support_Contract__Share> share = [Select Id, AccessLevel, RowCause, ParentId, UserOrGroupId from NS_Support_Contract__Share where parentId = :contract.id]; 
        System.assertEquals(2, share.size());
        test.stopTest();

    }

    private static NS_Support_Contract__c createNSSupportContract(){

    	Id SysAdminProfileId = '00e20000000yyUzAAI';
    	Id PortalUserRoleId  = '00E20000000vrJSEAY';
    	Id PrmProfileId = '00e20000001OyLwAAK';

   		QTTestUtils.GlobalSetUp();
        
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User myUser = NSSupportContractSharingTest.createSysAdminUser(SysAdminProfileId, PortalUserRoleId, me);

        List<Contact> ctList = new List<Contact>();
        List<Account> myAccounts = NSSupportContractSharingTest.createTestAccounts(myUser, ctList);
        
        List<User> pUsers = NSSupportContractSharingTest.createUserFromProfileId(PrmProfileId, ctList); 

        NS_Support_Contract__c contract = NSSupportContractSharingTest.createMockNSSupportContract(myAccounts);
        return contract;
    }

    static testMethod void testUpdateAccountLicensesDueToNSSupportContractSharing(){
    	test.startTest();
    	NS_Support_Contract__c contract = createNSSupportContract();
        List<Id> contracts = new List<Id>();
        contracts.add(contract.Id);
        List<Id> endUsers = new List<Id>();
        endUsers.add(contract.End_User__c);

       	ApexSharingRules.updateNSSupportContractSharingForTest(endUsers, contracts);
        
        List<NS_Support_Contract__Share> share = [Select Id, AccessLevel, RowCause, ParentId, UserOrGroupId from NS_Support_Contract__Share where parentId = :contract.id]; 
        System.assertEquals(2, share.size());

        delete contract;

    	ApexSharingRules.updateAccountLicensesDueToNSSupportContractSharingForTest(contracts);
    	 List<NS_Support_Contract__Share> shareAfterDelete = [Select Id, AccessLevel, RowCause, ParentId, UserOrGroupId from NS_Support_Contract__Share where parentId = :contract.id]; 
    	System.assertEquals(0, shareAfterDelete.size());
    	test.stopTest();
    }

    static testMethod void testGetAccessTypeValue()
    {
    	String s = 'read';
    	Integer res = ApexSharingRules.getAccessTypeValue(s);
    	System.assertEquals(1, res);
    }

/*
//removed when Parter Community implemented
	static testMethod void risingTestCoverage() {
		QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = '01220000000DOFu';
            update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType 
		                          where DeveloperName = 'Partner_Account' and sobjecttype='Account' ];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

		


		RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
            quoteForTest.SBQQ__Primary__c = true;
            quoteForTest.Sector__c='Healthcare';
            quoteForTest.Industry__c='Healthcare';
            quoteForTest.Function__c='Healthcare';
            quoteForTest.Solution_Area__c='Healthcare - Emergency Medicine';
        Test.startTest();
           insert quoteForTest;
            
            update quoteForTest;
		

		ApexSharingRules.UpdateContactSharing(new List<Id>{testContact.id});
		Campaign testCamp= new Campaign(
               Name='Test'
			   );
        insert testCamp; 
		Id oppId = [Select id From Opportunity].id;
		ApexSharingRules.UpdateCampaignSharing(new List<String>{testCamp.id});

		ApexSharingRules.AddQuoteSharingRulesOnSellThroughPartner(new List<String>{quoteForTest.id});

		ApexSharingRules.updateAccountSharing(new List<String>{testAccount.id});

		ApexSharingRules.UpdateOpportunitySharing(new List<Id>{oppId});

		ApexSharingRules.updateAccountOpportunitySharing(new List<Id>{testAccount.id}, new List<Id>{oppId});

		ApexSharingRules.GetListOfUserRolesByPortalAccountIds(new Set<Id>{testAccount.id}, new List<String>{''}, '');
		}
	}
*/	

	static testMethod void testUpdateCaseSharing() {
		Test.startTest();
			Case testCase = new Case(Status ='New', Priority = 'Medium', Origin = 'Email');
			insert testCase;
			ApexSharingRules.UpdateCaseSharing(new List<Id>{testCase.id});
		Test.stoptest();
	}
    static testMethod void coverageTest() {
        ApexSharingRules.UpdateSBQQSubscriptionSharing(new List<id>());
        ApexSharingRules.UpdateSBQQSubscriptionSharingNow(new List<id>());
        ApexSharingRules.UpdateAssetSharing(new List<id>());
        ApexSharingRules.UpdateAssetSharingNow(new List<id>());
        ApexSharingRules.UpdateCaseSharing(new List<id>());
        ApexSharingRules.UpdateCaseSharingForTest(new List<id>());
        ApexSharingRules.UpdateAssignmentSharing(new List<id>());
        ApexSharingRules.UpdateAssignmentSharingForTest(new List<id>());
        ApexSharingRules.updateNSSupportContractSharing(new List<id>(), new List<id>());
        ApexSharingRules.updateAccountLicensesDueToNSSupportContractSharing(new List<id>());
        List<string> portalTypes = new List<string>();
        portalTypes.add('Test');
        portalTypes.add('Test2');
        ApexSharingRules.GetListOfUserRolesByPortalAccountIds(new Set<id>(), portalTypes[0], 'Test');

        ApexSharingRules.GetListOfUserRolesByPortalAccountIds(new Set<id>(), portalTypes, 'Test');

        List<Id> ids = new List<Id>();
        Case c = new Case();
        ids.add(c.Id);
        ids.add(c.Id);

        ApexSharingRules.getRange(ids, 0, 1);

        ApexSharingRules.UpdateCampaignSharing(new List<id>());
        ApexSharingRules.UpdateCampaignSharingForTest(new List<id>());
        ApexSharingRules.AddQuoteSharingRulesOnSellThroughPartner(new List<String>());
    }
}