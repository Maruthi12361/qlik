/*****************************************************************************************************************
Change Log:
20150715    CCE Initial development - test class for UpdateFollowUpRequiredTypeForContact.trigger
2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
******************************************************************************************************************/
@isTest
private class UpdateFollowUpRequiredTypeForContactTest {
    
    static testMethod void test_updatingContactFoundinSoIonClosedWonOpp() {
        QTTestUtils.GlobalSetUp();
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('UpdateFollowUpRequiredTypeForContactTest: act = ' + act.Id);
            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            //Create a Contact
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ct = ' + ct.Id);
        
            //Create test Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today().addDays(2),
                StageName = 'Alignment Meeting',
                RecordTypeId = '01220000000DNwY',   // Direct / Reseller - Std Sales Process
                CurrencyIsoCode = 'GBP',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                Consultancy_Forecast_Amount__c = 1,
                Signature_Type__c = 'Digital',
                AccountId = ct.AccountId
            );
            insert testNewOpp;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: testNewOpp = ' + testNewOpp.Id);

            //Add our Contact to an SoI and add it to the Opportunity
            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = testNewOpp.Id;
            soi.Contact__c = ct.Id;
            soi.Role__c = 'Beneficiary';    
            insert soi;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: soi = ' + soi.Id);

            Test.StartTest();
            Semaphores.OppUpdateContactSOI_IsAfter = false;
            //And update the Opportunity to Closed Won
            testNewOpp.StageName = 'Closed Won';
            testNewOpp.Included_Products__c = 'Qlik Sense';
            update testNewOpp;
            
            //and now we can start the actual test
            
            //setting the Event Follow-Up field to true should cause the trigger we are testing to find
            // the Contact on the SoI on a Closed Won opportunity and therefore set the Contact Follow-Up Required Type field to Won.
            //Semaphores.TriggerHasRun('UpdateFollowUpRequiredTypeForContact', 1); //clear the Semaphore that was set when we inserted the Contact
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Event_Follow_Up_Required__c = true;
            update ct;

            Contact ctRet = [SELECT Overall_Follow_up_Required__c, Follow_Up_Required_Type__c FROM Contact WHERE Id = :ct.Id];
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ctRet.Overall_Follow_up_Required__c = ' + ctRet.Overall_Follow_up_Required__c);
            system.assertEquals('Won', ctRet.Follow_Up_Required_Type__c);    
    
            delete testNewOpp;
    
            Test.StopTest();
        }
    }

    static testMethod void test_ReviveContact() {
        
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('UpdateFollowUpRequiredTypeForContactTest: act = ' + act.Id);

            Test.StartTest();
            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            //Create a Contact
            Contact ct = new Contact(AccountId=act.Id, lastname='Contact1', firstname='Test', Contact_Status__c = 'Follow-Up Disqualified', Follow_Up_Disqualified_Reason__c = 'No Response');
            insert ct;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ct = ' + ct.Id);
                        
            //setting the Event Follow-Up field to true should cause the trigger we are testing to set the Contact Follow-Up Required Type 
            //field to Revived as the Event_Follow_Up_Required__c is true and the Contact Status field is Follow-Up Disqualified.           
            //Semaphores.TriggerHasRun('UpdateFollowUpRequiredTypeForContact', 1); //clear the Semaphore that was set when we inserted the Contact
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Event_Follow_Up_Required__c = true;
            update ct;
            
            Contact ctRet = [SELECT Overall_Follow_up_Required__c, Follow_Up_Required_Type__c FROM Contact WHERE Id = :ct.Id];
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ctRet.Overall_Follow_up_Required__c = ' + ctRet.Overall_Follow_up_Required__c);
            system.assertEquals('Revived', ctRet.Follow_Up_Required_Type__c);    
        
            Test.StopTest();
        }
    }

    static testMethod void test_updatingContactFoundinSoIonClosedLostOpp() {
        QTTestUtils.GlobalSetUp();
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('UpdateFollowUpRequiredTypeForContactTest: act = ' + act.Id);
            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            //Create a Contact
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ct = ' + ct.Id);
        
            //Create test Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today().addDays(2),
                StageName = 'Alignment Meeting',
                RecordTypeId = '01220000000DNwY',   // Direct / Reseller - Std Sales Process
                CurrencyIsoCode = 'GBP',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                Consultancy_Forecast_Amount__c = 1,
                Signature_Type__c = 'Digital',
                AccountId = ct.AccountId
            );
            insert testNewOpp;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: testNewOpp = ' + testNewOpp.Id);

            //Add our Contact to an SoI and add it to the Opportunity
            Sphere_of_Influence__c soi = new Sphere_of_Influence__c();
            soi.Opportunity__c = testNewOpp.Id;
            soi.Contact__c = ct.Id;
            soi.Role__c = 'Beneficiary';    
            insert soi;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: soi = ' + soi.Id);

            Test.StartTest();
            Semaphores.OppUpdateContactSOI_IsAfter = false;
            //And update the Opportunity to Closed Won
            testNewOpp.StageName = 'Closed Lost';
            testNewOpp.Primary_reason_lost__c = 'Budget Lost';
            update testNewOpp;
            
            //and now we can start the actual test
            
            //setting the Event Follow-Up field to true should cause the trigger we are testing to find
            // the Contact on the SoI on a Closed Lost opportunity and therefore set the Contact Follow-Up Required Type field to Lost.
            //Semaphores.TriggerHasRun('UpdateFollowUpRequiredTypeForContact', 1); //clear the Semaphore that was set when we inserted the Contact
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Event_Follow_Up_Required__c = true;
            update ct;

            Contact ctRet = [SELECT Overall_Follow_up_Required__c, Follow_Up_Required_Type__c FROM Contact WHERE Id = :ct.Id];
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ctRet.Overall_Follow_up_Required__c = ' + ctRet.Overall_Follow_up_Required__c);
            system.assertEquals('Lost', ctRet.Follow_Up_Required_Type__c);    
    
            delete testNewOpp;
    
            Test.StopTest();
        }
    }

    static testMethod void test_ClearFollowUpDisqualified() {
        
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('UpdateFollowUpRequiredTypeForContactTest: act = ' + act.Id);
            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            //Create a Contact
            Contact ct = new Contact(AccountId=act.Id, lastname='Contact1', firstname='Test', Contact_Status__c = 'Follow-Up Disqualified', Follow_Up_Disqualified_Reason__c = 'No Response');
            insert ct;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ct = ' + ct.Id);
                        
            Test.StartTest();
            //setting the Contact Status field to Goal Discovery should cause the trigger to clear the Follow_Up_Disqualified_Reason__c field.           
            //Semaphores.TriggerHasRun('UpdateFollowUpRequiredTypeForContact', 1); //clear the Semaphore that was set when we inserted the Contact
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Goal Discovery';
            update ct;
            
            Contact ctRet = [SELECT Follow_Up_Disqualified_Reason__c FROM Contact WHERE Id = :ct.Id];
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ctRet.Follow_Up_Disqualified_Reason__c = ' + ctRet.Follow_Up_Disqualified_Reason__c);
            system.assertEquals(null, ctRet.Follow_Up_Disqualified_Reason__c);    
        
            Test.StopTest();
        }
    }

    static testMethod void test_ClearFollowUpRejected() {
        
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('UpdateFollowUpRequiredTypeForContactTest: act = ' + act.Id);
            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            //Create a Contact
            Contact ct = new Contact(AccountId=act.Id, lastname='Contact1', firstname='Test', Contact_Status__c = 'Follow-Up Rejected', Follow_Up_Rejected_Reason__c = 'Already Working');
            insert ct;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ct = ' + ct.Id);
                        
            Test.StartTest();
            //setting the Contact Status field to Goal Discovery should cause the trigger to clear the Follow_Up_Rejected_Reason__c field.           
            //Semaphores.TriggerHasRun('UpdateFollowUpRequiredTypeForContact', 1); //clear the Semaphore that was set when we inserted the Contact
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Contact_Status__c = 'Goal Discovery';
            update ct;
            
            Contact ctRet = [SELECT Follow_Up_Rejected_Reason__c FROM Contact WHERE Id = :ct.Id];
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ctRet.Follow_Up_Rejected_Reason__c = ' + ctRet.Follow_Up_Rejected_Reason__c);
            system.assertEquals(null, ctRet.Follow_Up_Rejected_Reason__c);    
        
            Test.StopTest();
        }
    }

    static testMethod void test_ClearFollowUpNotFollowUpRequiredReason() {
        
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('UpdateFollowUpRequiredTypeForContactTest: act = ' + act.Id);
            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            //Create a Contact
            Contact ct = new Contact(AccountId=act.Id, lastname='Contact1', firstname='Test', Contact_Status__c = 'Follow-Up Rejected', Follow_Up_Rejected_Reason__c = 'Not Follow-Up Required', Not_Follow_Up_Required_Reason__c = 'Student');
            insert ct;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ct = ' + ct.Id);
                        
            Test.StartTest();
            //setting the Follow_Up_Rejected_Reason__c field to Already Working should cause the trigger to clear the Not_Follow_Up_Required_Reason__c field.           
            //Semaphores.TriggerHasRun('UpdateFollowUpRequiredTypeForContact', 1); //clear the Semaphore that was set when we inserted the Contact
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Follow_Up_Rejected_Reason__c = 'Already Working';
            update ct;
            
            Contact ctRet = [SELECT Follow_Up_Rejected_Reason__c, Not_Follow_Up_Required_Reason__c FROM Contact WHERE Id = :ct.Id];
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ctRet.Not_Follow_Up_Required_Reason__c = ' + ctRet.Not_Follow_Up_Required_Reason__c);
            system.assertEquals(null, ctRet.Not_Follow_Up_Required_Reason__c);    
        
            Test.StopTest();
        }
    }
    
    static testMethod void test_createNewContact() {
        
        User mockSysAdmin = QTTestUtils.createMockOperationsAdministrator();
        
        System.RunAs(mockSysAdmin) {
            //Create Account
            Account act = QTTestUtils.createMockAccount('My Test Account', mockSysAdmin);
            System.debug('UpdateFollowUpRequiredTypeForContactTest: act = ' + act.Id);
            Semaphores.TriggerHasRun('UpdateFollowUpRequiredTypeForContact', 1);

            Test.StartTest();

            Semaphores.ContactTriggerHandlerBeforeInsert = false;
            //Create a Contact
            Contact ct = new Contact(AccountId=act.Id, lastname='Contact1', firstname='Test', New_Follow_Up_Required__c = true);
            insert ct;
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ct = ' + ct.Id);
                        
            //setting the Event Follow-Up field to true should cause the trigger we are testing to set the
            // Contact Follow-Up Required Type field to New as the Event_Follow_Up_Required__c is true.         

            Contact ctRet = [SELECT Overall_Follow_up_Required__c, Follow_Up_Required_Type__c FROM Contact WHERE Id = :ct.Id];
            System.debug('UpdateFollowUpRequiredTypeForContactTest: ctRet = ' + ctRet);
            system.assertEquals('New', ctRet.Follow_Up_Required_Type__c);    
        
            Test.StopTest();
        }
    }
    
}