/**     * File Name:ECustomsHelper
    * Description : Helper class for ECustoms service
    * @author : MTM, Rodion
    * Modification Log ===============================================================
    Ver     Date         Author         Modification         
    2017-04-19  MTM      QCW-1982 
    2017-07-07 MTM  QCW-2606  Address validations
    2018-06-04 MTM BSL-490 New visual compliance implementations    
    2018-06-29 Anjuna BSL-612 New visual compliance implementations
    2018-09-20 UIN BSL-846 changes for OEM
    2018-11-06 UIN IMP-872 Changes for Subscription project
*/

public class ECustomsHelper {   
    
    private Set<String> yellow_Countries;
    private Set<String> red_Countries;
    private Set<String> excluded_Countries;
    private static Set<String> partner_RevenueTypes;
    
    //Toggle switch for new and old eCustoms
    public Static Boolean NewVisualComplianceEnabled
    {
        get
        {
            Steelbrick_Settings__c sbSetting = Steelbrick_Settings__c.getInstance(SettingName); 
            return  sbSetting.NewVisualComplianceEnabled__c;
        }
    }
    
    //New Visual complaince status checks.
    //Used in Create Orderform and Place Order
    public Static Boolean VCStatus(String quoteRevType,String EUserRPSStatus, String PUserRPSStatus, String EUserRCStatus, String PUserRCStatus)
    {
        if('No Matches'.equalsIgnoreCase(EUserRPSStatus) &&
             !('Alert'.equalsIgnoreCase(EUserRCStatus)) &&
             ECustomsHelper.VCPartnerAccountApproved(quoteRevType, PUserRPSStatus, PUserRCStatus))
            {
                return true;
            }
            else return false;
    }
    public Static Boolean VCPartnerAccountApproved(String quoteRevType, String PUserRPSStatus, String PUserRCStatus)
    {
        if(ECustomsHelper.PartnerRevenueTypes.contains(quoteRevType))
        {
            System.debug('quote.Revenue_Type__c = ' + quoteRevType);
             if('No Matches'.equalsIgnoreCase(PUserRPSStatus) &&
             !('Alert'.equalsIgnoreCase(PUserRCStatus)))
            {
                return true;
            }
            else return false;
            
        }   
        // This is not partner deal. Pass legal approval status
        System.debug('PlaceOrderController:PartnerAddressesApproved END');
        return true;

    }
    
    //Trigger new VisualCompliance on End user and Partner Accounts.
    //This is triggered  from quote SBQQQuoteTriggerHandler beforeupdate handler class.
    public boolean CheckVisualCompliance(List<SBQQ__Quote__c> quotes, Map<ID, SBQQ__Quote__c> OldSBQQQuoteMap)
    {
        Set<Id> accIds = new Set<Id>();
        Set<Id> endUserAccIds = new Set<Id>();
        Set<Id> oemQuoteIds = new Set<Id>();
        Set<Id> oemAccIds = new Set<Id>();
        Set<Account> acctToUpdate = new Set<Account>();
        List<Account> oemAccounts = new List<Account>();
        String country;
        Set<System_Message__c> sMessages = new Set<System_Message__c>();
        
        for(SBQQ__Quote__c quote : quotes)
        {
            //OEMs?
            // UIN commented for BSL-846
            //if(quote.SBQQ__Status__c == 'Approved' && OldSBQQQuoteMap.get(quote.Id).SBQQ__Status__c != 'Approved')
            if(quote.ApprovalStatus__c == 'Approved' && OldSBQQQuoteMap.get(quote.Id).ApprovalStatus__c != 'Approved')
            {
                if(String.isNotBlank(quote.SBQQ__Account__c) && !quote.Revenue_Type__c.contains('OEM')) accIds.add(quote.SBQQ__Account__c);
                if(String.isNotBlank(quote.SBQQ__Account__c)  && !quote.Revenue_Type__c.contains('OEM')) endUserAccIds.add(quote.SBQQ__Account__c);
                if(String.isNotBlank(quote.SBQQ__Account__c)  && quote.Revenue_Type__c.contains('OEM')) {
                    accIds.add(quote.SBQQ__Account__c);
                    oemQuoteIds.add(quote.Id);
                }
                if(ECustomsHelper.PartnerRevenueTypes.contains(quote.Revenue_Type__c))
                {
                    System.debug('Add partner too');
                    if(String.isNotBlank(quote.Sell_Through_Partner__c)) accIds.add(quote.Sell_Through_Partner__c);
                }
            }
        }
        if(!oemQuoteIds.isEmpty()){
            List<SBQQ__Quote__c> oemQuotes = new List<SBQQ__Quote__c>();

            oemQuotes = [select id,End_User_Account_ID__c,End_User_Account_Name__c from SBQQ__Quote__c where id in :oemQuoteIds];
            for(SBQQ__Quote__c quote: oemQuotes){
                system.debug('BSL846'+quote.End_User_Account_ID__c);
                system.debug('BSL846'+quote.End_User_Account_Name__c);
                if(quote.End_User_Account_ID__c != null)
                    oemAccIds.add(quote.End_User_Account_ID__c);
            }
            if(!oemAccIds.isEmpty()){
                oemAccounts = [select id,Recordtype.name from account where id in :oemAccIds];
                for(Account acc: oemAccounts){
                    if(acc.Recordtype.Name.contains('End User') || acc.Recordtype.Name.contains('Academic Program')){
                        endUserAccIds.add(acc.Id);
                        accIds.add(acc.Id);
                    }else
                        accIds.add(acc.Id);
                }
            }

        }

        if(accIds.IsEmpty()) return true;  //If no accounts to check return back
        for(Account acct: [SELECT Id,
                              Name,
                              BillingCountry,
                              ECUSTOMS__Screening_Trigger__c,
                              ECUSTOMS__RPS_Status__c,
                              ECUSTOMS__RPS_Date__c,
                              ECUSTOMS__IM_Status__c,
                              ECUSTOMS__RPS_RiskCountry_Status__c,
                              RecordType.Name
                      FROM Account
                      WHERE id in:accIds])
        {
        if(String.isNotBlank(acct.BillingCountry))
                    country = acct.BillingCountry.Remove(',');

                if(RedCountries.contains(country))   //Red countries are never checked
                {
                    System.debug('Red Country');
                    acct.ECUSTOMS__IM_Status__c = 'Red Country: Rejected';
                    acctToUpdate.add(acct);
                    continue;
                }
                if((YellowCountries.contains(country) && endUserAccIds.contains(acct.Id))|| //Yellow countries should always be checked for End User Account
                   ((String.isBlank(acct.ECUSTOMS__RPS_Status__c) || acct.ECUSTOMS__RPS_Status__c =='Pending') && acct.ECUSTOMS__RPS_Date__c == null) || //If account is never triggered
                  (acct.ECUSTOMS__RPS_Date__c != null && !acct.ECUSTOMS__RPS_Status__c.equalsIgnoreCase('No Matches') && acct.ECUSTOMS__RPS_RiskCountry_Status__c.equalsIgnoreCase('Alert') && (acct.Recordtype.name.contains('End User') || acct.Recordtype.name.contains('Academic Program')))) //RPS status = Matches and not cleared by legal
                 {
                     System.debug('VC triggered for account ' + acct.Name);
                     acct.ECUSTOMS__Screening_Trigger__c = true;
                     System_Message__c sMessage = new System_Message__c();
                     sMessage.Account__c = acct.Id;
                     sMessage.Error_Type__c = 'Setting Screening Trigger to True from EcustomsHelper Class';
                     sMessage.Error_Message__c = acct.ECUSTOMS__RPS_Status__c + ' ' + acct.ECUSTOMS__RPS_RiskCountry_Status__c + ' '+ acct.ECUSTOMS__RPS_Date__c;
                     acctToUpdate.add(acct);
                     sMessages.add(sMessage);
                 }
        }
        if(!acctToUpdate.IsEmpty())
        {
            //We don't want to trigger any custom triggers with this update
            Semaphores.CustomAccountTriggerBeforeUpdate = true;
            Semaphores.CustomAccountTriggerAfterUpdate = true;
            update new List<Account>(acctToUpdate);
        }
        if(!sMessages.isEmpty())
            insert new List<System_Message__c>(sMessages);
        return true;
    }
    
    private static final String SettingName = 'SteelbrickSettingsDetails';
        
    private Set<String> RedCountries
    {
        get{
                Steelbrick_Settings__c sbSetting = Steelbrick_Settings__c.getInstance(SettingName);     
                if(red_Countries == null)
                {
                    if(String.isEmpty(sbSetting.RedCountry__c)) return new Set<String>();
                    List<String> countyList = sbSetting.RedCountry__c.Trim().split(',');
                    red_Countries = new Set<String>(countyList);
                }
                System.debug('Red Countries count =' + red_Countries.size());
                return  red_Countries;
            }
    }
   
    
    public static Set<String> PartnerRevenueTypes
    {
        get{
                HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
                if(partner_RevenueTypes == null)
                {
                    if(String.isEmpty(settings.Partner_Revenue_Types__c)) return new Set<String>();
                    List<String> revenueTypes = settings.Partner_Revenue_Types__c.Trim().split(',');
                    partner_RevenueTypes = new Set<String>(revenueTypes);
                }
                System.debug('PartnerRecordTypes count =' + partner_RevenueTypes.size());
                return  partner_RevenueTypes;
            }
    } 
    
    private Set<String> YellowCountries
    {
        get{
                Steelbrick_Settings__c sbSetting = Steelbrick_Settings__c.getInstance(SettingName);
                if(yellow_Countries == null)
                {
                    if(String.isEmpty(sbSetting.YellowCountry__c)) return new Set<String>();
                    List<String> countyList = sbSetting.YellowCountry__c.Trim().split(',');
                    yellow_Countries = new Set<String>(countyList);
                }
                System.debug('Yellow Countries count =' + yellow_Countries.size());
                return  yellow_Countries;
            }
    }
    
    
    //Zuora Code changes done by UIN
    
    public boolean zuora_CheckVisualCompliance(List<zqu__Quote__c> quotes, Map<ID, zqu__Quote__c> OldSBQQQuoteMap)
    {
        Set<Id> accIds = new Set<Id>();
        Set<Id> endUserAccIds = new Set<Id>();
        Set<Id> oemQuoteIds = new Set<Id>();
        Set<Id> oemAccIds = new Set<Id>();
        Set<Account> acctToUpdate = new Set<Account>();
        List<Account> oemAccounts = new List<Account>();
        String country;
        Set<System_Message__c> sMessages = new Set<System_Message__c>();
        
        for(zqu__Quote__c quote : quotes)
        {
            //OEMs?
            // UIN commented for BSL-846
            //if(quote.SBQQ__Status__c == 'Approved' && OldSBQQQuoteMap.get(quote.Id).SBQQ__Status__c != 'Approved')
            system.debug('fff'+quote.ApprovalStatus__c+OldSBQQQuoteMap.get(quote.Id).ApprovalStatus__c+quote.Revenue_Type__c);
            if(quote.ApprovalStatus__c == 'Approved' && OldSBQQQuoteMap.get(quote.Id).ApprovalStatus__c != 'Approved')
            {
                if(String.isNotBlank(quote.zqu__Account__c) && !quote.Revenue_Type__c.contains('OEM')) accIds.add(quote.zqu__Account__c);
                if(String.isNotBlank(quote.zqu__Account__c)  && !quote.Revenue_Type__c.contains('OEM')) endUserAccIds.add(quote.zqu__Account__c);
                if(String.isNotBlank(quote.zqu__Account__c)  && quote.Revenue_Type__c.contains('OEM')) {
                    accIds.add(quote.zqu__Account__c);
                    oemQuoteIds.add(quote.Id);
                }
                if(ECustomsHelper.PartnerRevenueTypes.contains(quote.Revenue_Type__c))
                {
                    System.debug('Add partner too');
                    //IMP-1113
                    if(String.isNotBlank(quote.Sell_Through_Partner_Id__c)) accIds.add(quote.Sell_Through_Partner_Id__c);          
                }
            }
        }
        /** Commenting out logic for oem as oem quotes not valid for zuora
        if(!oemQuoteIds.isEmpty()){
            List<zqu__Quote__c> oemQuotes = new List<zqu__Quote__c>();

            oemQuotes = [select id from zqu__Quote__c  where id in :oemQuoteIds];
            for(zqu__Quote__c quote: oemQuotes){
                system.debug('BSL846'+quote.End_User_Account_ID__c);
                system.debug('BSL846'+quote.End_User_Account_Name__c);
                if(quote.End_User_Account_ID__c != null)
                    oemAccIds.add(quote.End_User_Account_ID__c);
            }
            if(!oemAccIds.isEmpty()){
                oemAccounts = [select id,Recordtype.name from account where id in :oemAccIds];
                for(Account acc: oemAccounts){
                    if(acc.Recordtype.Name.contains('End User') || acc.Recordtype.Name.contains('Academic Program')){
                        endUserAccIds.add(acc.Id);
                        accIds.add(acc.Id);
                    }else
                        accIds.add(acc.Id);
                }
            }

        }   */

        if(accIds.IsEmpty()) return true;  //If no accounts to check return back
        for(Account acct: [SELECT Id,
                              Name,
                              BillingCountry,
                              ECUSTOMS__Screening_Trigger__c,
                              ECUSTOMS__RPS_Status__c,
                              ECUSTOMS__RPS_Date__c,
                              ECUSTOMS__IM_Status__c,
                              ECUSTOMS__RPS_RiskCountry_Status__c,
                              RecordType.Name
                      FROM Account
                      WHERE id in:accIds])
        {
        if(String.isNotBlank(acct.BillingCountry))
                    country = acct.BillingCountry.Remove(',');

                if(RedCountries.contains(country))   //Red countries are never checked
                {
                    System.debug('Red Country');
                    acct.ECUSTOMS__IM_Status__c = 'Red Country: Rejected';
                    acctToUpdate.add(acct);
                    continue;
                }
                if((YellowCountries.contains(country) && endUserAccIds.contains(acct.Id))|| //Yellow countries should always be checked for End User Account
                   ((String.isBlank(acct.ECUSTOMS__RPS_Status__c) || acct.ECUSTOMS__RPS_Status__c =='Pending') && acct.ECUSTOMS__RPS_Date__c == null) || //If account is never triggered
                  (acct.ECUSTOMS__RPS_Date__c != null && !acct.ECUSTOMS__RPS_Status__c.equalsIgnoreCase('No Matches') && acct.ECUSTOMS__RPS_RiskCountry_Status__c.equalsIgnoreCase('Alert') && (acct.Recordtype.name.contains('End User') || acct.Recordtype.name.contains('Academic Program')))) //RPS status = Matches and not cleared by legal
                 {
                     System.debug('VC triggered for account ' + acct.Name);
                     acct.ECUSTOMS__Screening_Trigger__c = true;
                     System_Message__c sMessage = new System_Message__c();
                     sMessage.Account__c = acct.Id;
                     sMessage.Error_Type__c = 'Setting Screening Trigger to True from EcustomsHelper Class';
                     sMessage.Error_Message__c = acct.ECUSTOMS__RPS_Status__c + ' ' + acct.ECUSTOMS__RPS_RiskCountry_Status__c + ' '+ acct.ECUSTOMS__RPS_Date__c;
                     acctToUpdate.add(acct);
                     sMessages.add(sMessage);
                 }
        }
        if(!acctToUpdate.IsEmpty())
        {
            //We don't want to trigger any custom triggers with this update
            Semaphores.CustomAccountTriggerBeforeUpdate = true;
            Semaphores.CustomAccountTriggerAfterUpdate = true;
            update new List<Account>(acctToUpdate);
        }
        if(!sMessages.isEmpty())
            insert new List<System_Message__c>(sMessages);
        return true;
    }

}