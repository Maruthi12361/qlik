/**     * File Name:AccountIndustrySectorFunctionHandler
        * Description : This handler class is used to set the sector,industry,function on the quote on change in account
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Feb 22nd 2017 RamakrishnaKini     Added new trigger logic.
*/
public with sharing class AccountIndustrySectorFunctionHandler {
	public static List<Id> lAccIds = new List<Id>();
	public AccountIndustrySectorFunctionHandler() {
		
	}

	public static void onBeforeInsert(List<Account> inputList) {

	}

	/*  @Description :This common method is used to update billing country code fields on account on before update trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap 
        @parameter oInputList: Trigger.old Account list  
    */
    public static void onAfterUpdate(List<Account> inputList, Map<id, Account> inputMap, Map<id, Account> oInputMap) {
    	for(Account acc: inputList){
            if (acc.Industry_from_SIC_Code_Lookup__c != oInputMap.get(acc.ID).Industry_from_SIC_Code_Lookup__c || acc.Sector__c != oInputMap.get(acc.ID).Sector__c) { 
                lAccIds.add(acc.Id);
            }
        }
        system.debug('hhhhhhh'+lAccIds);
        if(!lAccIds.isEmpty())
            updQuoteFields(lAccIds, inputMap);
	}

	/**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/
    
    /*  @Description :This method is used to fetch qliktech company details for accounts which have billing country populated
            and update the billing country code lookup
        @parameter inputList: Accounts whose billing country is populated
    */
    private static void updQuoteFields(List<Id> lAccountIds, Map<id, Account> inputMap){
    	List<SBQQ__Quote__c> quotes = new List<SBQQ__Quote__c>();
    	List<SBQQ__Quote__c> quotesForUpd = new List<SBQQ__Quote__c>();
    	Map<id, List<SBQQ__Quote__c>> accIDQuotes = new Map<id, List<SBQQ__Quote__c>>();
    	quotes = [select id,Sector__c,Industry__c,SBQQ__Primary__c,SBQQ__Account__c from SBQQ__Quote__c where SBQQ__Primary__c = true and SBQQ__Status__c='Open' and SBQQ__Account__c in :lAccountIds ];
    	system.debug('hhhhhhh1'+quotes);
    	if(!quotes.isEmpty()){
    		for(SBQQ__Quote__c quote: quotes){
    			if(accIDQuotes.containsKey(quote.SBQQ__Account__c))
    				accIDQuotes.get(quote.SBQQ__Account__c).add(quote);
				else
					accIDQuotes.put(quote.SBQQ__Account__c, new List<SBQQ__Quote__c>{quote});
    		}
    		for(Id accId:lAccountIds){
    			if(accIDQuotes.containsKey(accId)){
    				for(SBQQ__Quote__c quote: accIDQuotes.get(accId)){
	    				quote.Sector__c = inputMap.get(accId).Sector__c;
	    				quote.Industry__c = inputMap.get(accId).Industry_from_SIC_Code_Lookup__c;
	    				quote.Function__c = '';
                        quote.Solution_Area__c = '';
	    				quotesForUpd.add(quote);
    				}
    			}
    		}
    		if(!quotesForUpd.isEmpty())
    			update quotesForUpd;
		}
	}
}