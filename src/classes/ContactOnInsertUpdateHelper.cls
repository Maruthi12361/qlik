/***************************************************
Trigger: ContactOnInsertUpdateTrigger
Object: Contact
Description: When a contact is created or updated copy the Account Owner Id
             to Account_Owner__c as required in CR# 6397
             This works together with ContactOnInsertUpdateTrigger
Change Log:
 20121031   TJG https://eu1.salesforce.com/a0CD000000NKf1w
 20121220   TJG Tidying up the code layout and make it bullet proof
 20160324   CCE CR# 80053 - Adding semaphore to reduce SOQL queries
 20170718   Nikhil Jain - QCW-2868
 2017-09-22 - AYS -  BMW-396/CHG0032034 - Added logic for Archived Record Type.
 2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
 2019-04-15 CCE CHG0035936 BMW-1424 Stop Contact Resource RT from being overwritten on Suspect status
 2019-06-12 extbad IT-1923 Disabled Contact Duplicate rule for chats
******************************************************/
public class ContactOnInsertUpdateHelper {

	public static void updateAccountOwner(List<Contact> triggernew){

		System.debug('ContactOnInsertUpdateTrigger: Running');
        //Get a Set of Account Id's from the Contacts
        Set<Id> acctIDs = new Set<Id>();
        for(Contact contact : triggernew)
        {
            if(contact.AccountId != null)
            {
                acctIDs.add(contact.AccountId);
            }
        }
        
        //and create a Map of Id's and Accounts Owner Name based on our Set of Account Id's
        Map<Id, Account> acctMap = new Map<Id, Account>([select Owner.Name, Name from Account where Id in :acctIDs]);
        for(Contact cntct : triggernew)
        {
            if (cntct.AccountId != null)
            {
                Account account = acctMap.get(cntct.AccountId);
                if (account != null)
                {
                    cntct.Account_Owner__c = account.Owner.Id;
                }
            }
        }
	}

	public static void validateSOIContact(List<Contact> triggernew, List<Contact> triggerold, Map<id,contact> triggeroldmap){

		//Start-- QCW-2868
		List<String> lstContactsReparented = new List<String>();
        Set<String> setUniqueContactOnQuote = new Set<String>();
        Map<String,Set<String>> mapContactVsQuote = new Map<String,Set<String>>();
        
        //Preparing list of contacts which are re-parented
        for(Contact cntct : triggernew){
            if(cntct.AccountId != triggeroldmap.get(cntct.Id).AccountId){
                lstContactsReparented.add(cntct.Id);
            }
        }
        
        for(SBQQ__Quote__c quoteRec : [SELECT Id,Partner_Contact__c,Quote_Recipient__c,Name from SBQQ__Quote__c 
                                        WHERE SBQQ__Primary__c = TRUE 
                                        AND SBQQ__Status__c != 'Accepted by Customer'
                                        AND SBQQ__Status__c != 'Order Placed'
                                        AND (Partner_Contact__c in :lstContactsReparented OR Quote_Recipient__c in :lstContactsReparented)]){
            setUniqueContactOnQuote.add(quoteRec.Partner_Contact__c);
            setUniqueContactOnQuote.add(quoteRec.Quote_Recipient__c);
            
            if(!mapContactVsQuote.containsKey(quoteRec.Partner_Contact__c)){
                mapContactVsQuote.put(quoteRec.Partner_Contact__c,new Set<String>());
            }
            if(!mapContactVsQuote.containsKey(quoteRec.Quote_Recipient__c)){
                mapContactVsQuote.put(quoteRec.Quote_Recipient__c,new Set<String>());
            }
            mapContactVsQuote.get(quoteRec.Partner_Contact__c).add(quoteRec.Name);
            mapContactVsQuote.get(quoteRec.Quote_Recipient__c).add(quoteRec.Name);
        }
        
        for(String contactId: lstContactsReparented){
            if(setUniqueContactOnQuote.contains(contactId)){
                string quoteMessage = '(';
                for(String quoteName: mapContactVsQuote.get(contactId)){
                    quoteMessage += quoteName + ',';
                }
                quoteMessage = quoteMessage.subString(0,quoteMessage.length()-1);
                quoteMessage += ')';
                Trigger.newMap.get(contactId).addError('The contact cannot be re-parented as it is referenced in these open primary Quotes:'+quoteMessage);
            }
        }
    }//End-- QCW-2868

  		public static void suspectRecTypeUpdate(List<Contact> triggernew, List<Contact> triggerold, Map<id,contact> triggeroldmap){
  		//Begin BMW-396
        //If Status is changed to "Suspect" the Record Type changes to the value in the "Archived: Original Record Type" field. 
        //If no value is present make the Record Type = Standard Lead.
        ZiftSettings__c csZift = ZiftSettings__c.getInstance();

        for(Contact contact : triggernew) {
            Contact oldContact = triggeroldmap.get(contact.id);
            if(contact.RecordTypeId != csZift.RT_Resource_Con__c && contact.Contact_Status__c == 'Suspect' && oldContact.Contact_Status__c != 'Suspect'){
                if(contact.Old_Record_Type_Name__c != null){
                    try{
                        contact.RecordTypeId =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contact.Old_Record_Type_Name__c).getRecordTypeId();
                    }catch(exception e){
                        contact.RecordTypeId = csZift.RT_StandardContact__c;
                        System.debug('debug_Old_Record_Type_Name_error: ' + e.getMessage());
                    }
                }else{
                    contact.RecordTypeId = csZift.RT_StandardContact__c;
                }  
            }
        }
	}//End BMW-396

    public static void cleanSkipDuplicateRule(List<Contact> triggernew){
        for (Contact ct : triggernew) {

            if (String.isNotBlank(ct.Skip_Duplicate_Email_Rule__c)) {
                ct.Skip_Duplicate_Email_Rule__c = '';
            }
        }
    }
}