/*----------------------------------------------------------------------------------------------------------
Purpose: Test class of PartnerCategoryStatusclass
Created Date: 30/06/17
Created by: Kumar Navneet
Change log:16/08/2017
Purpose : PAEX 227
2018-05-28 Linus Löfberg BSL-372
-------------------------------------------------------------------------------------------------------------- */

@isTest(seeAlldata=false)
private class PartnerCategoryStatusclassTest{

    static testMethod void test_PartnerCategoryStatus() {
        List<Partner_Category_Status__c > Pcstatus = new List<Partner_Category_Status__c>();
        List<Account> testAccounts = new List<Account>();

        Id rtId2= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
        Id rtId3 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('OEM Partner Account').getRecordTypeId();

        Id rtId = Schema.SObjectType.Partner_Category_Status__c.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
        Id rtId1 = Schema.SObjectType.Partner_Category_Status__c.getRecordTypeInfosByName().get('OEM Partner Account').getRecordTypeId();

        Test.startTest();

        Account testAccount = new Account(name='Test AccountName',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount);
        Account testAccount1 = new Account(name='Test AccountName1',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount1);
        Account testAccount2 = new Account(name='Test AccountName2',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount2);
        Account testAccount3 = new Account(name='Test AccountName3',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount3);
        Account testAccount4 = new Account(name='Test AccountName4',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount4);
        Account testAccount5= new Account(name='Test AccountName4',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount5);
        Account testAccount6 =new Account(name='Test AccountName6', recordtypeid=rtId3,QlikTech_Company__c='test',Territory_Country__c='India',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222');
        testAccounts.add(testAccount6);
        Account testAccount7= new Account(name='Test AccountName7',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount7);
        Account testAccount8= new Account(name='Test AccountName8',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount8);
        Account testAccount9= new Account(name='Test AccountName9',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount9);
        Account testAccount10= new Account(name='Test AccountName10',recordtypeid=rtId2,BillingCountry='India');
        testAccounts.add(testAccount10);
        insert testAccounts;

        Partner_Category_Status__c PartnerCatSta = new Partner_Category_Status__c(Partner_Account__c=testAccounts[0].Id , recordtypeid=rtId ,Partner_Level__c= 'Elite' , Partner_Category__c='Resell' , Program_Version__c = 'QPP');
        Pcstatus.add(PartnerCatSta);
        Partner_Category_Status__c PartnerCatSta1 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[1].Id ,recordtypeid=rtId , Partner_Level__c= 'Elite' , Partner_Category__c='Solution Provider' , Program_Version__c = 'Partner Program 4.1');
        Pcstatus.add(PartnerCatSta1);
        Partner_Category_Status__c PartnerCatSta2 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[2].Id ,recordtypeid=rtId , Partner_Level__c= 'Elite Master Reseller' , Partner_Category__c='Master Reseller' , Program_Version__c = 'Master Reseller');
        Pcstatus.add(PartnerCatSta2);
        Partner_Category_Status__c PartnerCatSta3 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[3].Id ,recordtypeid=rtId , Partner_Level__c= 'Authorized Master Reseller' , Partner_Category__c='Master Reseller' , Program_Version__c = 'Master Reseller',Partner_Training_Discount__c=null);
        Pcstatus.add(PartnerCatSta3);
        Partner_Category_Status__c PartnerCatSta4 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[4].Id , recordtypeid=rtId ,Partner_Level__c= 'Distributor' , Partner_Category__c='Distributor' , Program_Version__c = 'Distributor');
        Pcstatus.add(PartnerCatSta4);
        Partner_Category_Status__c PartnerCatSta5 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[5].Id ,recordtypeid=rtId , Partner_Level__c= 'Strategic' , Partner_Category__c='Implementation & Consulting' , Program_Version__c = 'Global SI');
        Pcstatus.add(PartnerCatSta5);
        Partner_Category_Status__c PartnerCatSta6 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[6].Id ,recordtypeid=rtId1, Partner_Level__c= 'OEM' , Partner_Category__c='OEM' , Program_Version__c = 'OEM',Partner_Training_Discount__c=null);
        Pcstatus.add(PartnerCatSta6);
        Partner_Category_Status__c PartnerCatSta7 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[7].Id , recordtypeid=rtId ,Partner_Level__c= 'Authorized Master Reseller' , Partner_Category__c='Master Reseller' , Program_Version__c = 'Master Reseller');
        Pcstatus.add(PartnerCatSta7);
        Partner_Category_Status__c PartnerCatSta8 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[8].Id ,recordtypeid=rtId , Partner_Level__c= 'Technology Partner' , Partner_Category__c='Technology Partner' , Program_Version__c = 'Technology Partner',Partner_Training_Discount__c=null);
        Pcstatus.add(PartnerCatSta8);
        Partner_Category_Status__c PartnerCatSta9 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[9].Id ,recordtypeid=rtId , Partner_Level__c= 'Authorized' , Partner_Category__c='Influence' , Program_Version__c = 'QPP',Partner_Training_Discount__c=null,Influence_Benefit_Type__c= null);
        Pcstatus.add(PartnerCatSta9);
        Partner_Category_Status__c PartnerCatSta10 = new Partner_Category_Status__c(Partner_Account__c=testAccounts[10].Id ,recordtypeid=rtId , Partner_Level__c= 'Elite' , Partner_Category__c='MSP' , Program_Version__c = 'QPP',Partner_Training_Discount__c=null);
        Pcstatus.add(PartnerCatSta10);
        upsert Pcstatus;

        PageReference pageRef = Page.PartnerCategoryStatus;
        Test.setCurrentPage(pageRef);
        apexpages.currentpage().getparameters().get('id');
        String idReturl = apexpages.currentpage().getparameters().get('retURL');
        String PartnerAccID = apexpages.currentpage().getparameters().get('PartnerAccountID');

        ApexPages.StandardController sc = new ApexPages.StandardController(Pcstatus[0]);
        PartnerCategoryStatusclass Pcs = new PartnerCategoryStatusclass(sc);
        Pcs.Validate();
        Pcs.edit();
        Pcs.save();
        Pcs.saveAndNew();

        ApexPages.StandardController sc1 = new ApexPages.StandardController(Pcstatus[1]);
        PartnerCategoryStatusclass Pcs1 = new PartnerCategoryStatusclass(sc1);
        Pcs1.Validate();
        Pcs1.edit();
        Pcs1.save();
        Pcs1.saveAndNew();

        ApexPages.StandardController sc2 = new ApexPages.StandardController(Pcstatus[2]);
        PartnerCategoryStatusclass Pcs2 = new PartnerCategoryStatusclass(sc2);
        Pcs2.Validate();
        Pcs2.edit();
        Pcs2.save();
        Pcs2.saveAndNew();

        ApexPages.StandardController sc3 = new ApexPages.StandardController(Pcstatus[3]);
        PartnerCategoryStatusclass Pcs3 = new PartnerCategoryStatusclass(sc3);
        Pcs3.Validate();
        Pcs3.edit();
        Pcs3.save();
        Pcs3.saveAndNew();

        ApexPages.StandardController sc4 = new ApexPages.StandardController(Pcstatus[4]);
        PartnerCategoryStatusclass Pcs4 = new PartnerCategoryStatusclass(sc4);
        Pcs4.Validate();
        Pcs4.edit();
        Pcs4.save();
        Pcs4.saveAndNew();
        ApexPages.StandardController sc5 = new ApexPages.StandardController(Pcstatus[5]);
        PartnerCategoryStatusclass Pcs5 = new PartnerCategoryStatusclass(sc5);
        Pcs5.Validate();
        Pcs5.edit();
        Pcs5.save();
        Pcs5.saveAndNew();


        ApexPages.StandardController sc6 = new ApexPages.StandardController(Pcstatus[6]);
        PartnerCategoryStatusclass Pcs6 = new PartnerCategoryStatusclass(sc6);

        update PartnerCatSta1;
        Pcs6.save();
        update PartnerCatSta1;
        Pcs6.saveAndNew();
        update PartnerCatSta1;

        ApexPages.StandardController sc7 = new ApexPages.StandardController(Pcstatus[7]);
        PartnerCategoryStatusclass Pcs7= new PartnerCategoryStatusclass(sc7);
        Pcs7.Validate();
        Pcs7.edit();
        Pcs7.save();
        Pcs7.saveAndNew();

        ApexPages.StandardController sc8 = new ApexPages.StandardController(Pcstatus[8]);
        PartnerCategoryStatusclass Pcs8 = new PartnerCategoryStatusclass(sc8);
        Pcs8.Validate();
        Pcs8.edit();
        Pcs8.save();
        Pcs8.saveAndNew();

        ApexPages.StandardController sc9 = new ApexPages.StandardController(Pcstatus[9]);
        PartnerCategoryStatusclass Pcs9 = new PartnerCategoryStatusclass(sc9);
        Pcs9.Validate();
        Pcs9.edit();
        Pcs9.save();
        Pcs9.saveAndNew();

        ApexPages.StandardController sc10 = new ApexPages.StandardController(Pcstatus[10]);
        PartnerCategoryStatusclass Pcs10 = new PartnerCategoryStatusclass(sc10);
        Pcs10.Validate();
        Pcs10.edit();
        Pcs10.save();
        Pcs10.saveAndNew();
        apexPages.Currentpage().getParameters().put('Id',PartnerCatSta1.id);

        Test.stopTest();
    }
}