/******************************************************

	Class: LeadConvUpdateQVMCustomerProductsHandler
	
	Initiator: Project P0128: QlikMarket
	
	Changelog:
		2017-10-25 AYS BMW-402 : Migrated from UpdateQVMCustomerProductsOnLeadConvert.trigger.
		
******************************************************/

public class LeadConvUpdateQVMCustomerProductsHandler {
	public LeadConvUpdateQVMCustomerProductsHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld, Boolean isAfter) {
		if (isAfter) {
			Set<Id> Leads = new Set<Id>();
			Map<Id, Id> LeadToContact = new Map<Id, Id>();
			List<QVM_Customer_Products__c> QVMCustomerProdToBeUpdated = new List<QVM_Customer_Products__c>();
			
			for (integer i = 0; i < triggerNew.size(); i++)
			{
				if (triggerNew[i].IsConverted == true && triggerOld[i].IsConverted == false && triggerNew[i].ConvertedContactId != null)
				{
					Leads.add(triggerNew[i].Id);
					LeadToContact.put(triggerNew[i].Id, triggerNew[i].ConvertedContactId);			
				}
			}
			
			for (QVM_Customer_Products__c qvmCustProd : [select Id, Contact__c, Lead__c from QVM_Customer_Products__c where Lead__c in :Leads])
			{
				qvmCustProd.Contact__c = LeadToContact.get(qvmCustProd.Lead__c);
				qvmCustProd.Lead__c = null;		
				QVMCustomerProdToBeUpdated.Add(qvmCustProd);
			}
			if (QVMCustomerProdToBeUpdated.size() > 0)
			{
				update QVMCustomerProdToBeUpdated;		
			}
		}		
	}
}