/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * SAN 18/12/2012 FIX the LEad soql that returns more than 1
 * SLH 19/11/2013 FIX the Account SQL that returns more than one record....
 * 29/5/2018 BSL-418 added semaphore logic account trigger consolidation   shubham gupta
 */
@isTest
private class UpdateGroupEmpTEST {

    static testMethod void myUnitTest() {
        
        system.debug('*** Starting');
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();   
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            Country_Name__c = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
			Subsidiary__c = testSubs1.id
			);
        insert QTComp;

        Lead L = new Lead();
        L.FirstName = 'Steve';
        L.LastName = 'Steve';
        L.Country = 'Sweden';
        l.Email = 'test24348734@b.com';
        l.Company = 'Test';
        l.Country_Code__c = QTComp.Id;
        insert L;

        //Lead L = [SELECT Id FROM Lead WHERE FirstName = 'Steve' AND LastName = 'STEVE' LIMIT 1];
        
        L.Email = 'asd@asd.com';
        L.Country = 'Sweden';
        
        system.debug('*** Got Lead ' + L.Id);
        L.NumberOfEmployees = null;
        update L;
        System.assertEquals(L.Group_Employee_Range__c, null);

        L.NumberOfEmployees = 1;
        update L;
        system.debug('*** Set Employees to 1');

        Account A = new Account();
        A.Name = 'QGate';
        A.NumberOfEmployees = null;
        insert A;

        //Account A = [SELECT Id FROM Account WHERE Name Like 'QGate%' LIMIT 1];
        
        //A.NumberOfEmployees = null;
        //update A;
        System.assertEquals(A.Employee_Range__c, null);

        A.NumberOfEmployees = 1;

        Semaphores.CustomAccountTriggerBeforeUpdate = false;

        update A;

        System.assertEquals(pickListTranslations.ReturnEmployee(4), '1-4');
        System.assertEquals(pickListTranslations.ReturnEmployee(7), '5-9');
        System.assertEquals(pickListTranslations.ReturnEmployee(15), '10-19');
        System.assertEquals(pickListTranslations.ReturnEmployee(25), '20-49');
        System.assertEquals(pickListTranslations.ReturnEmployee(75), '50-100');
        System.assertEquals(pickListTranslations.ReturnEmployee(150), '100-199');
        System.assertEquals(pickListTranslations.ReturnEmployee(250), '200-499');
        System.assertEquals(pickListTranslations.ReturnEmployee(750), '500-999');
        System.assertEquals(pickListTranslations.ReturnEmployee(1300), '1000-1499');
        System.assertEquals(pickListTranslations.ReturnEmployee(1750), '1500-1999');
        System.assertEquals(pickListTranslations.ReturnEmployee(2500), '2000-2999');
        System.assertEquals(pickListTranslations.ReturnEmployee(3000), '3000-3999');
        System.assertEquals(pickListTranslations.ReturnEmployee(4500), '4000-4999');
        System.assertEquals(pickListTranslations.ReturnEmployee(7000), '5000-9999');
        System.assertEquals(pickListTranslations.ReturnEmployee(11000), '10000+');
        
    }
}