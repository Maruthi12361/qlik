@isTest
private class TriggerPortalControlHelperTest {
	
	@isTest static void checkPortalControl() {
		User testUser = [Select id From User where id =: UserInfo.getUserId()];
		System.runAs(testUser) {
			List<Contact> contacts = new List<Contact>();            
        	Contact contact = new Contact(   FirstName = 'Test LeadCountryISOUpdate',
                                Email = 'asd@asd.com',
                                LastName = 'Lastname', 
                                QCloudID__c = 'Enterprise',
                                PartnerSourceNo__c = '12345',
                                TriggerPortalControl__c = true
                                );
       
        	contacts.add(contact);
        
        
        // Do insert tests
        	insert(contacts); 
        	Semaphores.TriggerHasRun('ContactUpdateULCTrigger', 1);
        	Semaphores.ContactTriggerHandlerBeforeInsert = false;
        
        	List<ULC_Details__c> ULCDetails = new List<ULC_Details__c>();
        	ULC_Details__c ulc = new ULC_Details__c();
        	ulc.ULCName__c = 'thecontactuser';//modified for BMW-508
        	ulc.ULCStatus__c = 'Active';
        	ulc.ContactId__c = contacts[0].Id;
        	ULCDetails.add(ulc);

        	insert (ULCDetails);

        	Semaphores.ContactTriggerHandlerBeforeUpdate = false;
        	contacts[0].Email = 'xyz@asd.com';
        	update contacts[0];
		}
	}
}