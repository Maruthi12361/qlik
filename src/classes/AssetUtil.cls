/********
* NAME :AssetUtil.cls
* Description: Create asset with data from the quote, quote line and quotelinesubline object
* 
*
*Change Log:
    IRN      2016-08-19   created this class for the Q2CW project
    IRN      2017-02-06   populate support date only when it is not an eval or NFR license
    IRN      2017-02-13   creating saleschannel if no one exist with the same sellthroughpartner and license customer
	MTM		 2017-09-09   QCW-3803 MSP changes
    MTM      2017-11-30 QCW-4398 populate second partner
******/

public class AssetUtil{

    private Map<String, Id> assetCaryingLicenseIdMap = new Map<String, Id>();
    private Set<String> licenseProductIdThatIsRequired = new Set<String>();
    private Map<Id, Product2> upgradeTargetMap = new Map<Id, Product2>();
    private Map<Id, Product2> upgradeAssetMap = new Map<Id, Product2>();
    private Map<Id, Opportunity> oemOpportunitiesMap = new Map<Id, Opportunity>();

    public Set<String> exceptionList = new set<String>{'id', 'createddate', 'createdbyid', 'lastmodifieddate', 'lastmodifiedbyid', 'systemmodstamp', 'isdeleted', 'lastvieweddate', 'lastreferenceddate', 'name'};

     public void createAsset(Set<Id> quotesToCreateAssetsFor, Map<ID, SBQQ__Quote__c> quotesMap){
        system.debug('CreateAsset ' + quotesToCreateAssetsFor);

        Set<Id> licenseCustomerIds = new Set<Id>();
        Set<Id> sellThrougPartnerIds = new Set<Id>();
        Map<Id, Id>oemOppQuoteId = new Map<Id, Id>();
        for(Id qId: quotesMap.keySet()){
            if(quotesMap.get(qId).Revenue_Type__c.contains('OEM') || quotesMap.get(qId).Revenue_Type__c == 'MSP'){
                oemOppQuoteId.put(quotesMap.get(qId).SBQQ__Opportunity2__c, qId);
            }
            if(quotesMap.get(qId).SBQQ__Account__c != null){
                licenseCustomerIds.add(quotesMap.get(qId).SBQQ__Account__c);
            }
            if(quotesMap.get(qId).Sell_Through_Partner__c != null){
                sellThrougPartnerIds.add(quotesMap.get(qId).Sell_Through_Partner__c);
            }
        }
        List<Opportunity> oemOpportunities = [Select Id, End_User_Account_del__c from Opportunity where Id in :oemOppQuoteId.keyset()];
        for(Opportunity o :oemOpportunities){
            oemOpportunitiesMap.put(oemOppQuoteId.get(o.Id), o);
            if(o.End_User_Account_del__c != null){
                licenseCustomerIds.add(o.End_User_Account_del__c);
            }
        }
        //If quote line has an upgrade asset then update asset with license detail else creaet assets with data from license details
      
        //Get all saleschannels from same account and sell through partners
        List<Sales_Channel__c> saleschannels = [Select Id, License_Customer__c, Sell_Through_Partner__c, Support_Provided_by__c, Revenue_Type__c from Sales_Channel__c where License_Customer__c in : licenseCustomerIds or Sell_Through_Partner__c in : sellThrougPartnerIds];
        Map<String, Id> salesChannelMap = new Map<String, Id>();
        Map<Id, String> quoteSalesChannelMap = new Map<Id, String>();
        for(Sales_Channel__c sc : saleschannels){
            String supportProvidedById = sc.Support_Provided_by__c;
            System.debug('supportProvidedById' + supportProvidedById);
            if(supportProvidedById != null && supportProvidedById.length()>15){
                supportProvidedById = String.valueOf(sc.Support_Provided_by__c ).substring(0, 15); //we need to truncate it to 15 due to the formula field on the quote have the Id in that format    
            }
            
            salesChannelMap.put(sc.License_Customer__c + '_' + sc.Sell_Through_Partner__c + '_' + supportProvidedById + '_' + sc.Revenue_Type__c, sc.Id);
        }

        List<String> apiNames = apiNamesBothOnAssetAndQL();
        String partOfQuery = '';
        for(String s : apiNames){
            partOfQuery += 'Quote_Line__r.'+ s + ', ';
        }

        String query = 'Select Id,  LEFDetailID__c, LicenseProductID__c, Quote_Id__C, Quote_Line__c, Asset_Status__c, License_Number__c, Total_Qty__c, Required_by_LicenseProductID__c,'+  partOfQuery +'  Quote_Line__r.SBQQ__SubscriptionPricing__c, Quote_Line__r.SBQQ__ProductFamily__c, Quote_Line__r.SBQQ__Product__c, Quote_Line__r.SBQQ__Quantity__c, Quote_Line__r.SBQQ__Product__r.Name, Quote_Line__r.SBQQ__Product__r.SBQQ__UpgradeTarget__c, Quote_Line__r.SBQQ__OriginalPrice__c  from Quote_Line_Sub_Line__c where Quote_Id__c in : quotesToCreateAssetsFor';
        List<sObject> oList = Database.query(query);
        List<Quote_Line_Sub_Line__c> assetDetails = (List<Quote_Line_Sub_Line__c>) oList;
        System.debug(' assetDetails ' + assetDetails);
        Map<String, Quote_Line_Sub_Line__c>licenseProductIdsMap = new Map<String, Quote_Line_Sub_Line__c>();
        Map<String, Quote_Line_Sub_Line__c> lefdetailIdsMap = new Map<String, Quote_Line_Sub_Line__c>();
        Set<Id> upgradeTarget = new Set<Id>();
        for(Quote_Line_Sub_Line__c assetDetail :assetDetails){
            if(assetDetail.LicenseProductID__c != null){
                licenseProductIdsMap.put(assetDetail.LicenseProductID__c, assetDetail); 
            } else if(assetDetail.LEFDetailID__c != null){
                lefdetailIdsMap.put(assetdetail.LEFDetailID__c, assetDetail);
            }
            if(assetdetail.Required_by_LicenseProductID__c != null){
                licenseProductIdThatIsRequired.add(assetdetail.Required_by_LicenseProductID__c);
            }
            if(assetdetail.Quote_Line__r.SBQQ__Product__r.SBQQ__UpgradeTarget__c != null){
                upgradeTarget.add(assetdetail.Quote_Line__r.SBQQ__Product__r.SBQQ__UpgradeTarget__c);
            }
        }

        List<Product2> products = [select Id, Name, (Select Id, Name, SBQQ__Feature__r.Id, SBQQ__Feature__r.Name, SBQQ__OptionalSKU__r.Name, SBQQ__OptionalSKU__c  from SBQQ__Options__r) from Product2 where ID in : upgradeTarget];
        for(Product2 p : products){
            upgradeTargetMap.put(p.Id, p);
        }

        List<Asset> existingAssets = [Select Id, End_User__c, LEFDetailID__c, LicenseProductID__c, Quantity, SBQQ__SubscriptionStartDate__c , SBQQ__SubscriptionEndDate__c from Asset where LEFDetailID__c in: lefdetailIdsMap.keySet() or LicenseProductID__c in: licenseProductIdsMap.keySet() or LicenseProductID__c in: licenseProductIdThatIsRequired];
        System.debug('existingAssets.size ' + existingAssets.size());
        Map<String, Asset> assetMapLicenseProduct = new Map<String, Asset>();
        Map<String, Asset> assetMapLefDetail = new Map<String, Asset>();
        for(Asset a : existingAssets){
             if(a.LicenseProductID__c != null){
                assetMapLicenseProduct.put(a.LicenseProductID__c, a); 
            } else if(a.LEFDetailID__c != null){
                assetMapLefDetail.put(a.LEFDetailID__c, a);
            }
            System.debug('before populating assetCaryingLicenseIdMap ' + a.LicenseProductID__c);
            assetCaryingLicenseIdMap.put(a.LicenseProductID__c, a.Id);
        }

        List<Asset> assetToUpdateOrCreate = new List<Asset>();
        Map<Id, List<Asset>> assetsThatNeedANewCAlink = new Map<Id, List<Asset>>();
        System.debug('licenseProductIdsMap.size ' + licenseProductIdsMap.size());
        System.debug('lefdetailIdsMap.size ' + lefdetailIdsMap.size());



        //we need to create assets for all licensProduct first due to we need them as require by asset on the lefdetails assets
        for(String licensProductId : licenseProductIdsMap.keySet()){
            if(assetMapLicenseProduct.containsKey(licensProductId)){
                Asset a = assetMapLicenseProduct.get(licensProductId);
                a.LicenseProductID__c = licensProductId;
                Quote_Line_Sub_Line__c qlsl = licenseProductIdsMap.get(licensProductId);
                a = populateAssetWithData(a, qlsl, quotesMap.get(qlsl.Quote_Id__C), apiNames);
                assetToUpdateOrCreate.add(a);
            }else{
                Quote_Line_Sub_Line__c qlsl = licenseProductIdsMap.get(licensProductId);
                SBQQ__Quote__c q = quotesMap.get(qlsl.Quote_Id__C);
                String salesChannelKey = getSalesChannelKey(q, oemOpportunitiesMap);
                System.debug('salesChannelMap ' + salesChannelMap);
                if(salesChannelMap.containsKey(salesChannelKey)){
                    Asset a  = new Asset();
                    Id ca_link = salesChannelMap.get(salesChannelKey);
                    a.Sales_Channel__c = ca_link;//Sales Channel should be filled in when an asset is created with id of the contract_asset object. 
                    a = populateAssetWithData(a, qlsl, quotesMap.get(qlsl.Quote_Id__C), apiNames);
                    assetToUpdateOrCreate.add(a);
                }else{
                    quoteSalesChannelMap.put(q.Id, salesChannelKey);
                    Asset a  = new Asset();
                    a = populateAssetWithData(a, qlsl, quotesMap.get(qlsl.Quote_Id__C), apiNames);
                    if(assetsThatNeedANewCAlink.containsKey(quotesMap.get(qlsl.Quote_Id__C).Id)){
                        List<Asset> quoteAssets = assetsThatNeedANewCAlink.get(quotesMap.get(qlsl.Quote_Id__C).Id);
                        quoteAssets.add(a);
                        assetsThatNeedANewCAlink.put(quotesMap.get(qlsl.Quote_Id__C).Id, quoteAssets);
                    }else{
                        List<Asset> quoteAssets = new List<Asset>();
                        quoteAssets.add(a);
                        assetsThatNeedANewCAlink.put(quotesMap.get(qlsl.Quote_Id__C).Id, quoteAssets);
                    }
                    
                }
            }
        }
        List<Sales_Channel__c> newSalesChannels = new List<Sales_Channel__c>();
        Map<Id, Id> contractAssetLinkMap = new Map<Id, Id>();
        System.debug('assetsThatNeedANewCAlink ' +assetsThatNeedANewCAlink);
        for(Id quoteId : assetsThatNeedANewCAlink.keySet()){
            String salesChannelKey = quoteSalesChannelMap.get(quoteId);
            Sales_channel__c sa = createSalesChannel(salesChannelKey, quoteId, quotesMap);
            newSalesChannels.add(sa);
        }
        if(newSalesChannels.size() > 0){
            insert newSalesChannels;
            Integer count = 0;
            for(String id : assetsThatNeedANewCAlink.keySet()){
                List<Asset> assets = assetsThatNeedANewCAlink.get(id);
                for(Asset a: assets){
                    a.Sales_Channel__c = newSalesChannels[count].Id;
                    assetToUpdateOrCreate.add(a);    
                }
                contractAssetLinkMap.put(id, newSalesChannels[count].Id);
                count++;   
            }
        }
        if(assetToUpdateOrCreate.size() > 0 ){

            for (Integer i = 0; i < assetToUpdateOrCreate.size(); i++) {
                if (assetToUpdateOrCreate[i].Product2Id.equals('01tD0000006UkdsIAC') && assetToUpdateOrCreate[i].Id != null) {
                    assetToUpdateOrCreate.remove(i);
                }
            }

            system.debug('nbr of assets to upsert ' + assetToUpdateOrCreate.size());
            upsert assetToUpdateOrCreate;
        }
        List<Asset>updateCombinedKey = new List<Asset>();
        for(Asset a :assetToUpdateOrCreate) {
            system.debug(' a  '+ a);
            assetCaryingLicenseIdMap.put(a.LicenseProductID__c, a.Id);
            if(a.Id != null  && a.SBQQ__CombineKey__c != a.Id){
                a.SBQQ__CombineKey__c = a.Id;
                updateCombinedKey.add(a);
            }
        }

        if(updateCombinedKey.size() >0){
            update updateCombinedKey;
        }

        assetToUpdateOrCreate.clear();
        assetsThatNeedANewCAlink.clear();
        newSalesChannels.clear();

        for(String lefDetaiId : lefdetailIdsMap.keySet()){
            if(assetMapLefDetail.containsKey(lefDetaiId)){
                Asset a = assetMapLefDetail.get(lefDetaiId);
                a.LEFDetailID__c = lefDetaiId;
                Quote_Line_Sub_Line__c qlsl = lefdetailIdsMap.get(lefDetaiId);
                a = populateAssetWithData(a,qlsl, quotesMap.get(qlsl.Quote_Id__C), apiNames);
                assetToUpdateOrCreate.add(a);
            }else{
                Quote_Line_Sub_Line__c qlsl = lefdetailIdsMap.get(lefDetaiId);
                SBQQ__Quote__c q = quotesMap.get(qlsl.Quote_Id__C);
                String salesChannelKey = getSalesChannelKey(q, oemOpportunitiesMap);
                if(salesChannelMap.containsKey(salesChannelKey)){
                    //contract asset exist, use existing ca number and update the quote with that value
                    Asset a  = new Asset();
                    Id ca_link = salesChannelMap.get(salesChannelKey);
                    a.Sales_Channel__c = ca_link;//Sales Channel should be filled in when an asset is created with id of the contract_asset object. 
                    a = populateAssetWithData(a,  qlsl, quotesMap.get(qlsl.Quote_Id__C), apiNames);
                    assetToUpdateOrCreate.add(a);
                }else{
                    if(contractAssetLinkMap.containsKey(quotesMap.get(qlsl.Quote_Id__C).Id)){
                        Asset a  = new Asset();
                        a = populateAssetWithData(a,  qlsl, quotesMap.get(qlsl.Quote_Id__C), apiNames);
                        a.Sales_Channel__c = contractAssetLinkMap.get(quotesMap.get(qlsl.Quote_Id__C).Id);
                        assetToUpdateOrCreate.add(a);
                    }else{
                        quoteSalesChannelMap.put(q.Id, salesChannelKey);
                        Asset a  = new Asset();
                        a = populateAssetWithData(a, qlsl, quotesMap.get(qlsl.Quote_Id__C), apiNames);
                        if(assetsThatNeedANewCAlink.containsKey(quotesMap.get(qlsl.Quote_Id__C).Id)){
                            List<Asset> quoteAssets = assetsThatNeedANewCAlink.get(quotesMap.get(qlsl.Quote_Id__C).Id);
                            quoteAssets.add(a);
                            assetsThatNeedANewCAlink.put(quotesMap.get(qlsl.Quote_Id__C).Id, quoteAssets);
                        }else{
                            List<Asset> quoteAssets = new List<Asset>();
                            quoteAssets.add(a);
                            assetsThatNeedANewCAlink.put(quotesMap.get(qlsl.Quote_Id__C).Id, quoteAssets);
                        }
                    }
                    
                }
                
            }
        }

        System.debug('assetsThatNeedANewCAlink ' +assetsThatNeedANewCAlink);
        for(Id quoteId : assetsThatNeedANewCAlink.keySet()){
            String salesChannelKey = quoteSalesChannelMap.get(quoteId);
            Sales_channel__c sa = createSalesChannel(salesChannelKey, quoteId, quotesMap);
            newSalesChannels.add(sa);
        }
        if(newSalesChannels.size() > 0){
            insert newSalesChannels;
            Integer count = 0;
            for(String id : assetsThatNeedANewCAlink.keySet()){
                List<Asset> assets = assetsThatNeedANewCAlink.get(id);
                for(Asset a: assets){
                    a.Sales_Channel__c = newSalesChannels[count].Id;
                    assetToUpdateOrCreate.add(a);    
                }
                contractAssetLinkMap.put(id, newSalesChannels[count].Id);
                count++;   
            }
        }
        if(assetToUpdateOrCreate.size() > 0 ){

            for (Integer i = 0; i < assetToUpdateOrCreate.size(); i++) {
                if (assetToUpdateOrCreate[i].Product2Id.equals('01tD0000006UkdsIAC') && assetToUpdateOrCreate[i].Id != null) {
                    assetToUpdateOrCreate.remove(i);
                }
            }
            
            system.debug('nbr of assets to upsert ' + assetToUpdateOrCreate.size());
            upsert assetToUpdateOrCreate;
        }

        updateCombinedKey.clear();
        for(Asset a : assetToUpdateOrCreate){
            if(a.Id != null  && a.SBQQ__CombineKey__c != a.Id){
                a.SBQQ__CombineKey__c = a.Id;
                updateCombinedKey.add(a);
            }
        }
         if(updateCombinedKey.size() >0){
            update updateCombinedKey;
        }

        system.debug('end CreateAsset');

    }


    public Asset populateAssetWithData(Asset a, Quote_Line_Sub_Line__c qlsl, SBQQ__Quote__c quote, List<String> apiNamesToPopulate){

        //if it is an OEM deal reseller and account should be different compared to all other deals
        if(quote.Revenue_Type__c.contains('OEM') || quote.Revenue_Type__c == 'MSP'){
            Opportunity opp = oemOpportunitiesMap.get(quote.Id);
            a.Reseller__c = quote.SBQQ__Account__c;
            if(opp.End_User_Account_del__c != null){
                a.End_User__c = opp.End_User_Account_del__c;
            }else{
                a.End_User__c = quote.SBQQ__Account__c;
            }
            
        }else{
            a.Reseller__c = quote.Sell_Through_Partner__c;
            a.End_User__c = quote.SBQQ__Account__c;
        }
        if(quote.Second_Partner__c != null)
        {
            a.Second_Partner__c = quote.Second_Partner__c;
        }
        a.AccountId = quote.SBQQ__Account__c;
        if(qlsl.Asset_Status__c == '' || qlsl.Asset_Status__c == null ){
            a.Status = 'Active';    
        }else{
            a.Status = qlsl.Asset_Status__c;
        }
        
        String startOfAssetName = '';
        if(qlsl.LicenseProductID__c != null){//asset name should be license nbr if it exist
            startOfAssetName = qlsl.License_Number__c + ' ';
        }
        if(qlsl.Quote_Line__r.SBQQ__Product__r.SBQQ__UpgradeTarget__c != null){
            system.debug('SBQQ__UpgradeTarget__c ' + qlsl.Quote_Line__r.SBQQ__Product__r.SBQQ__UpgradeTarget__c);
            system.debug('Id ' + upgradeTargetMap.get(qlsl.Quote_Line__r.SBQQ__Product__r.SBQQ__UpgradeTarget__c).Id);
            Product2 upgradeProduct = upgradeTargetMap.get(qlsl.Quote_Line__r.SBQQ__Product__r.SBQQ__UpgradeTarget__c);
            upgradeAssetMap.put(a.Id, upgradeProduct);
            a.product2Id = upgradeProduct.Id;
            a.Name = startOfAssetName + upgradeProduct.Name;
            //a.Product2Id innan upgrade är gjort
        }else{
            a.Product2Id = qlsl.Quote_Line__r.SBQQ__Product__c; 
            a.Name = startOfAssetName + qlsl.Quote_Line__r.SBQQ__Product__r.Name;
        }

        //populate start and end date on subscription records
        System.debug('SBQQ__SubscriptionPricing__c = ' + qlsl.Quote_Line__r.SBQQ__SubscriptionPricing__c +' ProductFamily__c '  +  qlsl.Quote_Line__r.SBQQ__ProductFamily__c );
        if(qlsl.Quote_Line__r.SBQQ__SubscriptionPricing__c == 'Fixed Price' && qlsl.Quote_Line__r.SBQQ__ProductFamily__c != 'Maintenace'){
            a.SBQQ__SubscriptionStartDate__c  = quote.Subscription_Start_Date__c;
            a.SBQQ__SubscriptionEndDate__c = quote.Subscription_End_Date__c;
        }        

        if(qlsl.LicenseProductID__c != null){
                a.License_No__c = qlsl.License_Number__c;
        }
        a.NC_TimeLimit__c =    quote.Evaluation_Expiry_Date__c;

        a.Support_From_Date__c = quote.SBQQ__StartDate__c;
        if(!quote.Record_Type_Dev_Name__c.contains('Evaluation') && !quote.Record_Type_Dev_Name__c.contains('NFR Quote')){
            a.Support_To_Date__c = quote.License_End_Date__c ;    
        }
        
        a.SBQQ__OriginalUnitCost__c = qlsl.Quote_Line__r.SBQQ__OriginalPrice__c;
        if(a.Id != null){
            a.SBQQ__CombineKey__c = a.Id;
        }
        if(a.Id == null){
            a.Quantity_before_uppgrade__c = qlsl.Total_Qty__c;
            a.Quote__c = qlsl.Quote_Id__C;
            //a.SBQQ__QuoteLine__c = qlsl.Quote_Line__c;
        }else{
            a.Quantity_before_uppgrade__c = a.Quantity;
        }
        if(qlsl.LicenseProductID__c != null){
            a.Quantity = 1;    
        }else{
            a.Quantity =  qlsl.Total_Qty__c;    
        }
        

        for(String api : apiNamesToPopulate){
            a.put(api, qlsl.getSObject('Quote_Line__r').get(api));
            System.debug('when populating ' +api + '  : ' + qlsl.getSObject('Quote_Line__r').get(api));
        }

        if(qlsl.Required_by_LicenseProductID__c != null){ //This means it is a child asset
            if(assetCaryingLicenseIdMap.containsKey(qlsl.Required_by_LicenseProductID__c)){
                Id requiredByAsset = assetCaryingLicenseIdMap.get(qlsl.Required_by_LicenseProductID__c);
                System.debug('set root asset' + requiredByAsset);
                
                a.SBQQ__RequiredByAsset__c = requiredByAsset;
                a.SBQQ__RootAsset__c = requiredByAsset;
                Product2 upgradeProduct = upgradeAssetMap.get(requiredByAsset);
                if(upgradeProduct != null){
                    List<SBQQ__ProductOption__c> options = upgradeProduct.SBQQ__Options__r;
                    if(options != null && options.size() > 0){
                        for(SBQQ__ProductOption__c o : options){
                            system.debug('product option SBQQ__OptionalSKU__c ' +o.SBQQ__OptionalSKU__c);
                            system.debug('asset product2Id ' +a.Product2Id);
                            if(o.SBQQ__OptionalSKU__c == a.Product2Id){
                                System.debug('options ' + o);
                                a.SBQQ__DynamicOptionId__c = o.SBQQ__Feature__r.Id;//this api name also exist on the quote line and therefore we need to update after updating all quote lines values
                                a.SBQQ__ProductOption__c = o.Id;//this api name also exist on the quote line and therefore we need to update after updating all quote lines values
                            }    
                        }
                    }  
                }

            }   
        }
        a.LicenseProductID__c = qlsl.LicenseProductID__c; //this api name also exist on the quote line and therefore we need to update after updating all quote lines values since we want to use the value from the quote line sub line
        a.LEFDetailID__c = qlsl.LEFDetailID__c; //this api name also exist on the quote line and therefore we need to update after updating all quote lines values since we want to use the value from the quote line sub line
        System.debug('asset created or updated ' + a);
        return a;

    }

    //If a field on the asset and quote line object have the same api name and have the same type the value is copied over from the quote line to the asset. 
    //Except for the fields in the exceptionList
    public List<String> apiNamesBothOnAssetAndQL(){
        List<String> apiNames = new List<String>();

        Map<String, Schema.SObjectField> assetFieldMap = Asset.sObjectType.getDescribe().fields.getMap();
        System.debug('assetFieldMap keys ' + assetFieldMap.keyset());

        Map<String, Schema.SObjectField> qlFieldMap = SBQQ__QuoteLine__c.sObjectType.getDescribe().fields.getMap();
        System.debug('qlFieldMap keys ' + qlFieldMap.keyset());

        for(String key : assetFieldMap.keySet()){
            if(qlFieldMap.containsKey(key) && !exceptionList.contains(key)){
                schema.describefieldresult assetField = assetFieldMap.get(key).getDescribe();
                schema.describefieldresult qlslField = qlFieldMap.get(key).getDescribe();
                if(assetField.getType() == qlslField.getType()){
                    apiNames.add(key);
                }
            }
        }
        System.debug('apiNames.size() ' + apiNames.size());
        return apiNames;
    }

    public String getSalesChannelKey(SBQQ__Quote__c q, Map<Id, Opportunity> oemOpportunitiesMap){
        String salesChannelKey = q.SBQQ__Account__c + '_' + q.Sell_Through_Partner__c + '_' + q.Support_Provided_by_Id__c + '_' + q.Revenue_Type__c;
           if(q.Revenue_Type__c.contains('OEM') || q.Revenue_Type__c.equals('MSP')){
                Opportunity opp = oemOpportunitiesMap.get(q.Id);
                if(opp.End_User_Account_del__c != null){
                    salesChannelKey = opp.End_User_Account_del__c + '_';    
                }else{
                    salesChannelKey = q.SBQQ__Account__c + '_';
                }
                String revenuetype = q.Revenue_Type__c;
                if(revenuetype.equals('OEM Recruitment')){
                    revenuetype = 'OEM';
                }
                salesChannelKey = salesChannelKey+ q.SBQQ__Account__c + '_' + q.Support_Provided_by_Id__c + '_' + revenuetype;
            }
        System.debug('salesChannelKey ' + salesChannelKey);
        return salesChannelKey;
    }

    public Sales_Channel__c createSalesChannel(String salesChannelKey, Id quoteId, Map<Id, SBQQ__Quote__c> quotesMap){
        System.debug('start createSalesChannel');
        List<String> scData = salesChannelKey.split('_');
        Id licenseCustomerId = NULL;
        if(scData[0] != 'null'){
            licenseCustomerId = Id.valueOf(scData[0]);
        }
        Id sellThroughPartnerId = NULL;
        if(scData[1] != 'null') 
        {
            sellThroughPartnerId = Id.valueOf(scData[1]);
        }
        SBQQ__Quote__c q = quotesMap.get(quoteId);
        System.debug('before creating saleschannel ' + scData);
        Sales_Channel__c sa = SalesChannelUtils.createSalesChannel(licenseCustomerId,sellThroughPartnerId, q.Support_Provided_by_Id__c, q.Support_provided_by_Qlik__c, scData[3], q.Second_Partner__c);
        return sa;
    }

}