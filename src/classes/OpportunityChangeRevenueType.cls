/********
* NAME : OpportunityChangeRevenueType
* Description: Helper class to change an opportunity revenue type, 
*              in addition to that it also removes all line items and quotes on the opp.
* 
*
*Change Log:
*   2014-08-29 MTM Initial development
*   MTM 2014-11-28 CR# 17952
*   MTM 2014-12-03 CR# 19145
*   TJG 2015-04-30 CR# 37640 As discussed with Azeem check if there are still quotes before make Boomi call
*   TJG 2015-07-02 CR# 37640 As pointed out by Natalie, need to delete Approval Records as well
    2016-06-08   Andrew Lokotosh Comented Forecast_Amount fields line 77-82 go forward they will have Roll-Up Summery type
******/

global class OpportunityChangeRevenueType {
 
    private static String message = '';
    webservice static String ChangeRevenueType(String oppId, String newRevenueType, Id newSellThroughPartner ,Id newPartnerContact,Id newActivePSM ,Id newSecondPartner)
    {
        System.debug('Going to change RevenueType to '+ newRevenueType);
        if(oppId != null && newRevenueType != null){
            try
            {
                Opportunity opp = GetOpp(oppId);
                
                if(opp != null && RefreshOpportunityInfo(opp, newRevenueType, newSellThroughPartner ,newPartnerContact,newActivePSM ,newSecondPartner)){   
                    SetOppRevenueTypeByBoomi(opp.Id, opp.INT_NetSuite_InternalID__c , newRevenueType);                   
                }
            }
            catch(DmlException e){
                system.Debug('Error in OpportunityChangeRevenueType : ' + e); 
                message += e;
            }
        }
        return message;
        
    }
    
    private static Opportunity GetOpp(String OppId){
        for(Opportunity opp :[SELECT Id, Revenue_Type__c, Sell_Through_Partner__c, Partner_Contact__c, Active_PSM__c, Second_Partner__c, INT_NetSuite_InternalID__c, Quote_Expiration_Date__c, Quote_Status__c
                              FROM Opportunity WHERE Id =: oppId]){
                return opp;
            }
        return null;
    }
    @testVisible
    private static Boolean RefreshOpportunityInfo(Opportunity opp, String newRevenueType, Id newSellThroughPartner ,Id newPartnerContact,Id newActivePSM ,Id newSecondPartner){             
        if(opp.Revenue_Type__c == newRevenueType){
            message = 'You selected the same RevenueType.';
            return false;
        }        

        /* 20141203 MTM CR# 19145
         * Order of execution/update are important here, Please do not change
         * If changing this, need to test three cases 
         * One with No quotes and Zero prices
         * SEcond with no quote and non zero License forecast amounts means there will be oppty Line items
         * Third with Quote 
         */
        
        List<NS_Quote__c> allQs = [SELECT Id FROM NS_Quote__c WHERE Opportunity__c = :opp.Id];
        if(allQs.size() > 0)
        {
            message += 'Deleted Quote(s).\n';
            delete allQs;
        }
        
        // 2015-07-02 if there are Approval Records, then delete them
        List<Approval__c> allARs = [SELECT Id FROM Approval__c WHERE Opportunity__c = :opp.Id];
        if (allARs.size() > 0)
        {
            message += 'Deleted Approval Record(s).\n';
            delete allARs;
        }
        //Delete amounts on the opp.
       
       //opp.License_Forecast_Amount__c = null; 
       // opp.Education_Forecast_Amount__c = null;
       // opp.Support_Forecast_Amount__c = null;
       // opp.Consultancy_Forecast_Amount__c = null;
       // opp.Misc_Forecast_Amount__c = null;
       // opp.Total_Maintenance_Amount__c = null;
        opp.Quote_Expiration_Date__c = null;
        opp.Quote_Status__c = null;        

        opp.Quote_Approval_Reason_Request__c = null;        
        //Change RevenueType to new one            
        opp.Revenue_Type__c = newRevenueType;
        opp.Sell_Through_Partner__c = newSellThroughPartner;
        opp.Partner_Contact__c = newPartnerContact;
        opp.Active_PSM__c = newActivePSM;
        opp.Second_Partner__c = newSecondPartner;
        opp.Allow_Revenue_Type_Change__c = true;
        update opp;
        opp.Allow_Revenue_Type_Change__C = false;
        update opp;

        List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where OpportunityId= :opp.Id];
        System.debug('olis.size() = ' + olis.size());
        if(olis.size() > 0)
                delete olis;
        
        //TJG 2015-04-30 CR# 37640 As discussed with Azeem check if there are still quotes
        List<NS_Quote__c> nsqs = [SELECT Id from NS_Quote__c where Opportunity__c = :opp.Id and IsDeleted<>true];
        if (nsqs.size() > 0)
        {
            message += 'However, there are still quotes for this opportunity. Do not call Boomi!';
            return false;
        }
                        
        BoomiUtil.updateBoomiStatus(opp.Id);
        
        return true;
    }
    webservice static String SetOppRevenueTypeByBoomi(String oppId, String newRevenueType)
    {
        Opportunity opp = GetOpp(oppId);
        if(opp != null){                    
            SetOppRevenueTypeByBoomi(opp.Id, opp.INT_NetSuite_InternalID__c, newRevenueType);                  
        }
        return message;
    }
    private static Boolean SetOppRevenueTypeByBoomi(String sfOppId, String nsOppId, String newRevenueType){
       
        //Only ask boomi to update if this opp exists in Netsuite
        System.debug('SetOppRevenueTypeByBoomi sfOppId:' +sfOppId+ ' nsOppId:' + nsOppId + ' newRevenueType:' + newRevenueType);

        if(nsOppId != null && nsOppId != '')    {
            String orgId = UserInfo.getOrganizationId();
            String boomiMessage = '<Record type="Opportunity" environment="' + orgId + '"><ID>' + nsOppId + '</ID><secondaryID>' + sfOppId + '</secondaryID></Record>';
            BoomiUtil.callBoomi(sfOppId, 'updateNSOppCurrency', boomiMessage);

            System.debug('Called boomi with :' + boomiMessage);
            
            //message += '\nResetting the Opportunity... \n It can take few minutes to reset all opportunity information.';
            return true;
        }
        return false;
    }
}