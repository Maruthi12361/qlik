/**************************************************
*
* Change Log:
*
* 2017-11-27	ext_bad	CR#CHG0030519	Created.
*										Includes 'after insert' logic from TaskAfterInsert trigger.
**************************************************/

public class TaskCompleteMilestoneHandler {

    public static void handle(List<Task> newTasks) {
        Map<ID, Task> whatIds = new Map<ID, Task>();

        ID supportTaskRTId = Task.SObjectType.getDescribe().getRecordTypeInfosByName().get('Support Tasks')
                .getRecordTypeId();

        //& Task.whatId is polymorphic.
        for (Task t : newTasks) {
            if (t.IsClosed && t.RecordTypeId == supportTaskRTId
                    && (CaseMilestoneUtils.isCommunicationTask(t)
                    || t.Subject == CaseMilestoneUtils.TASK_SUBJECT_FIRST_RESPONSE)) {
                whatIds.put(t.whatId, t);
            }
        }

        if (whatIds.size() != 0) {
            CaseMilestoneUtils.completeMilestonesFromTask(whatIds);
        }
    }
}