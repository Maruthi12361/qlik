/*
 File Name: OppPopulateSendToRegionHandler

 2018-07-11 ext_bad   CHG0034322    Created from rule Voucher Populate Send to DL QT Region
*/
public with sharing class OppPopulateSendToRegionHandler {

    private static final DL_Training_Notification_Emails__c DL_EMAILS = DL_Training_Notification_Emails__c.getInstance();
    private static final Map<String, String> DL_EMAILS_MAP = getEmailsMap();

    private static final String SANDBOX_NAME = UserInfo.getUserName().substringAfterLast('.com.');
    private static final String NO_REGION = 'No Region';
    private static final String SPI_REGION = 'SPI';
    private static final String ITALY_SUB_REGION = 'ITALY';
    private static final String TO_REPLACE = '{Sandbox}';

    public static void populateSendToRegion(List<Opportunity> opps) {

        List<String> accountIds = new List<String>();
        for (Opportunity opp : opps) {
            if (opp.Send_to_DL_QT_Region__c == null) {
                accountIds.add(opp.AccountId);
            }
        }

        Map<String, Account> accountsMap = new Map<String, Account>(
        [
                SELECT Id, Billing_Country_Code__r.QlikTech_Region__c,
                        Billing_Country_Code__r.QlikTech_Sub_Region__c
                FROM Account
                WHERE Id IN :accountIds
        ]);

        for (Opportunity opp : opps) {
            if (opp.Send_to_DL_QT_Region__c == null) {
                Account acc = accountsMap.get(opp.AccountId);
                if (acc != null) {
                    opp.Send_to_DL_QT_Region__c = getEmailByRegion(acc.Billing_Country_Code__r.QlikTech_Region__c,
                            acc.Billing_Country_Code__r.QlikTech_Sub_Region__c);
                }
            }
        }
    }

    private static String getEmailByRegion(String region, String subRegion) {
        String email;
        String delimiter = '-';

        if (SPI_REGION.equalsIgnoreCase(region) && ITALY_SUB_REGION.equalsIgnoreCase(subRegion)) {
            email = DL_EMAILS_MAP.get(ITALY_SUB_REGION);
        } else {
            email = DL_EMAILS_MAP.get(region);
        }
        if (email == null) {
            email = DL_EMAILS_MAP.get(NO_REGION) != null ? DL_EMAILS_MAP.get(NO_REGION) : '';
            delimiter = '_';
        }
        if (OrganizationInfo.getOrganizationInfo().isSandbox__c) {
            if (!DL_EMAILS.Prod_Functionality__c) {
                email = email.replace(TO_REPLACE, SANDBOX_NAME + delimiter);
            } else {
                email = email.replace(TO_REPLACE, '');
            }
        } else {
            email = email.replace(TO_REPLACE, '');
        }
        return email;
    }

    private static Map<String, String> getEmailsMap() {
        List<SObjectField> dlFields = DL_Training_Notification_Emails__c.sObjectType.getDescribe().fields.getMap().values();
        Map<String, String> regionEmailMap = new Map<String, String>();

        for (SObjectField field : dlFields) {
            if (DL_EMAILS.get(field.getDescribe().getName()) != null) {
                regionEmailMap.put(field.getDescribe().getLabel(), DL_EMAILS.get(field.getDescribe().getName()).toString());
            }
        }
        return regionEmailMap;
    }
}