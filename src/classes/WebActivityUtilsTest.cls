/********************************************************
* CLASS: WebActivityUtilsTest
* DESCRIPTION: Test class for class WebActivityUtils
*
* CHANGELOG:    
*   2018-10-29 - BAD - Added Initial logic
*   2019-03-05 - CRW/BAD - added w2l logic
*********************************************************/
@isTest
private class WebActivityUtilsTest
{
    @isTest
    static void test_Constructors()
    {
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        System.RunAs(mockSysAdmin)
        {
            WebActivityUtils waUtils = new WebActivityUtils();
            WebActivityUtils.WebActivityContent waContent = new WebActivityUtils.WebActivityContent();

            waContent.ULCUsername = 'qtestbad';
            waContent.TimeStamp = '2018-10-04T11:11:11.916Z';

            WebActivityUtils.WebActivityContent waContent2 = new WebActivityUtils.WebActivityContent(waContent);

            //ULCUser
            WebActivityUtils.ULCUser us = new WebActivityUtils.ULCUser();
            WebActivityUtils.ULCUser us2 = new WebActivityUtils.ULCUser(us);
            //WebActivityUtils.ULCUser us3 = new WebActivityUtils.ULCUser(new WebActivityUtils.W2LContent());

            //Additional Params
            WebActivityUtils.AdditionalParams ap = new WebActivityUtils.AdditionalParams();
            ap.SupplimentalData = 'CAMPAIGNID~theCampaign~KEYWORDS~aaaa~SUB_INDUSTRY~sicCOde~LEADSOURCEDETAILMIRROR~lsrmirror~EMPLOYEE_RANGE~25-49~INDUSTRY~abc~DB_COMPANY_NAME~fffff~REGISTRY_STATE~~REGISTRY_COUNTRY~Sweden~JOB_FUNCTION~gggg~ref~QSDW~TCACCEPTED~true~WEB_ACTIVITY_SOURCE~VA~SOURCEURL~google.com~SourceULC~source-ulc~SourceEmpl~source-employee~SourcePartner~source-partner~SourceID1~source-id-1~SourceID2~source-id-2~Incentive~camID~FreeText~ThisIsMyFreeText~_Mkto_Trk~MarketoTracking~webleadinterest~Potential Customer~leadsourcedetail~leadsource-detail~partnersource~partner-source';
            ap = WebActivityUtils.AdditionalParamsSplit(ap);

            //W2L classes
            string json2 = '{"head":{"timestamp":"2018-11-09T07:14:10.3619957Z"},';
            json2 += '"body":{';
            json2 += '"user":{';
            json2 += '"ids": [';
            json2 += '{"systemName": "Marketo","systemType": "marketing automation","idType": "tracking","id": "id:497-BMK-910&token:_mch-qlik.com-1542095853734-38546"}]},';
            json2 += '"formdata":{"status":"executing","guid":"687d7498-c86a-4994-8030-7d04ee0cb091","timestamp":"2018-11-09T07:14:10.2160133Z","url":"https://qlik.dev.qws.qlik.com/us/try-or-buy/buy-now?sourcepartner=srcPartner&kw=keyword1&utm_source=utmsssssource",  "urlReferrer":"https://qlik.dev.qws.qlik.com/us/try-or-buy",';
            json2 += '"acknowledgements": [{"typeAck": "acknowledgement","name": "privacyPolicy","isAcceptanceEnabled": false,';
            json2 += '"isPreaccepted": true,"isAccepted": true,"isRequired": false,"text": "Please note that by submitting your personal data, you agree to receive marketing messages and other communications from Qlik. You may opt-out of receiving further communications at any time. For further information please see our <a target=newWin href=https://www.qlik.com/us/legal/cookies-and-privacy-policy>Privacy Policy</a>.",';
            json2 += '"contentAck": [{"type": "content reference","contentGUID": "0f8f96c1-22bd-11e8-8dbc-0abb13a835aa","contentLanguage": null,"contentVersion": "1.0",';
            json2 += '"contentType": "terms","guid": "055afb96-04cc-4852-8741-f6fe3f14d934","language": "en"}],"sortOrder": 0,"key": "privacyPolicyAcceptanceNotRequired","guid": "670bcaa8-a386-4598-9c16-256aa02e41e3","language": "en"}],';
            json2 += '"fields":{"incentive":"Incentives","sourceID2":"soource5656","campaignID":"7010D00000028Fy","ULCUsername":"ulcusername22","comments":"test devq1","company":"qlik","country":"IND","emailAddress":"qtestbad4@test.com.sandbox","firstName":"","lastName":"qtestbad4","jobTitle":"manager","postalCode":"90002","telephoneNumber":"440156456464"}}}}';
            WebActivityUtils.W2LContent content = new WebActivityUtils.W2LContent();
            content = (WebActivityUtils.W2LContent) JSON.deserialize (json2, WebActivityUtils.W2LContent.class);
            WAcontent.OUser = new WebActivityUtils.ULCUser(content);
            WAcontent.AdditParams = new WebActivityUtils.AdditionalParams(content);

            //Parse URL
            string paramValue = WebActivityUtils.GetParameterByName('kw', 'https://test.qlik.com?source=google&kw=QSD');
            system.assertEquals('QSD',paramValue);

            webActivityUtils.auth0 content3 = new WebActivityUtils.auth0();
            String jsonForEventTyoe = '{"cloudEventsVersion":"0.1","source":"com.qlik.qlikidp/preregistration","contentType":"application/json","eventType":"com.qlik.qlikidp.preregistration.trial.created","eventID":"615ea377-a912-481c-b904-5ef93a3a0ae2","extensions":{"subject":"auth0|a0826000001CMOjAAO"},"data":{"id":"f1a0e5fb-bb8d-488f-a919-8b8cc54b70d9","email":"joie@qlik.com","productId":"qsb","source":"com.qlik.qlikidp/preregistration","callbackUrl":"https://register.qlikcloud.com/","firstName":"Joe6464","lastName":"Qlikie43434","company":"Qlik","jobTitle":"ChiefMoraleOfficer","telephone":"(613)319-8153","country":"Sweden","locale":"en-CA","context":{"campaignID":"7010D00000028Fy","googleId":"86675823.1542","marketo":"123"},"subject":"a0826000001CMOjAAO","userType":"new"}}';
            content3 = (WebActivityUtils.auth0) JSON.deserialize (jsonForEventTyoe, WebActivityUtils.auth0.class);
            WAcontent.OUser = new WebActivityUtils.ULCUser(content3);
            WAcontent.AdditParams = new WebActivityUtils.AdditionalParams(content3);

            WebActivityUtils.LeadData content4 = new WebActivityUtils.LeadData();
            String jsonGenericLead = '{"email":"leadie@gmail.com","firstName":"TestLead","lastName":"TestLast","company":"Test","countryCode":"SE","country":"USA","state":"ON","jobTitle":"Test","telephone":"6130000000","campaignID":"7010D00000028Fy","comments":"Testing","googleId":"592277469.1574151110","marketo":"id:497-BMK-910&token:_mch-qlik.com-1574151110227-14637","url":"https://www.qlik.com/us/trial/qlik-sense-business","userAgent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36","privacyPolicyAccepted":"false","marketingOptIn":"true","incentive":"Snowflake Product Registration","sourceID2":"Attunity Replicate TestDrive"}';
            content4 = (WebActivityUtils.LeadData) JSON.deserialize (jsonForEventTyoe, WebActivityUtils.LeadData.class);
            WAcontent.OUser = new WebActivityUtils.ULCUser(content4);
            WAcontent.AdditParams = new WebActivityUtils.AdditionalParams(content4);            

        }       
    }

}