/*

    HerokuULCLevelSchedulable 

    These are schedulable methods for creating ULC leve needed by ULC Heroku Application

    SAN     2016-01-06

 */
 global class HerokuULCLevelSchedulable implements Schedulable {
    public HerokuULCLevelSchedulable (){}
    
    global void execute(SchedulableContext sc){
        System.abortJob(sc.getTriggerId());
        
        Double rnd =  Math.random();
        //& if > 4 batch processes queued or processing then reschedule this class and abort instance.
        if ([select count() from AsyncApexJob where Status='Queued' or Status='Processing']>4){                
            String s = '05 '+ (System.Now().Minute()==59 ? '0' : (System.Now().Minute()+1).format()) + ' * * * ?';
            String j = System.schedule('ULC level Sync - Auto Schedule ' + rnd, s, new HerokuULCLevelSchedulable ());
            return;
        }           
        //& invoke Batch Apex class.
        HerokuULCLevelBatchable b = new HerokuULCLevelBatchable();
        Id pId = Database.executeBatch(b); //& default scope of 200.

        HerokuULCLevelParentBatchable b2 = new HerokuULCLevelParentBatchable();
        Id pId2 = Database.executeBatch(b2); //& default scope of 200.        
    }
}