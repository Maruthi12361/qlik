/********************************************************
* 2018-07-10	ext_bad		CHG0034165
*********************************************************/
@isTest
private class CaseEscalationTest {
    static testMethod void testFieldsPopulating() {
        User testUsr = [
                SELECT Id
                FROM User
                WHERE IsActive = TRUE AND Id != :UserInfo.getUserId()
                AND ProfileId = :UserInfo.getProfileId()
                LIMIT 1
        ];
        System.runAs(testUsr) {
            List<EmailTemplate> etlist = [
                    SELECT id, Body, Subject, HtmlValue
                    FROM EmailTemplate
                    WHERE DeveloperName = :'New_Case_Escalation'
            ];
            if (etlist.size() == 0) {
                EmailTemplate et = new EmailTemplate(
                        DeveloperName = 'New_Case_Escalation',
                        Name = 'New Case Escalation',
                        FolderId = UserInfo.getOrganizationId(),
                        TemplateType = 'Custom',
                        Body = 'test',
                        Subject = 'test',
                        HtmlValue = 'test'
                );
                insert et;
            }
        }

        List<Account> accounts = new List<Account>();
        Account acc = new Account(Name = 'Test Account');
        Account acc2 = new Account(Name = 'Test Account');
        accounts.add(acc);
        accounts.add(acc2);
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        Contact c = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title');
        Contact c2 = new Contact(firstName = 'RDZTestContact',
                lastName = 'RDZTestLastName',
                Email = 'RDZTest@test.com.sandbox',
                HasOptedOutOfEmail = false,
                Job_Title__c = 'Job title 2');
        contacts.add(c);
        contacts.add(c2);
        insert contacts;
        Case cs = new Case();
        cs.Account_Origin__c = acc.Id;
        cs.ContactId = cs.Id;
        cs.Status = 'New';
        cs.Subject = 'New';
        cs.Severity__c = '1';
        cs.OwnerId = UserInfo.getUserId();
        insert cs;
        List<Qlikview_Support_Escalation__c> escs = new List<Qlikview_Support_Escalation__c>();

        Qlikview_Support_Escalation__c esc = new Qlikview_Support_Escalation__c();
        esc.Case__c = cs.Id;
        esc.ContactName__c = c.Id;
        esc.Description__c = 'test';
        esc.Business_Case__c = 'test';
        esc.Distribution_List__c = 'test';
        esc.Qlik_Management_Approver__c = UserInfo.getUserId();

        Qlikview_Support_Escalation__c esc2 = new Qlikview_Support_Escalation__c();
        esc2.Case__c = cs.Id;
        esc2.ContactName__c = c.Id;
        esc2.Contact_Role__c = 'new Job Title';
        esc2.Account_Origin_Lookup__c = acc2.Id;
        esc2.Description__c = 'test';
        esc2.Business_Case__c = 'test';
        esc2.Distribution_List__c = 'test';
        esc2.Qlik_Management_Approver__c = UserInfo.getUserId();

        escs.add(esc);
        escs.add(esc2);

        Test.startTest();
        insert escs;
        Test.stopTest();

        esc = [
                SELECT Id, Status__c, Account_Origin_Lookup__c, Contact_Role__c, CaseOwner__c
                FROM Qlikview_Support_Escalation__c
                WHERE ID = :esc.Id
        ];

        esc2 = [
                SELECT Id, Status__c, Account_Origin_Lookup__c, Contact_Role__c, CaseOwner__c
                FROM Qlikview_Support_Escalation__c
                WHERE ID = :esc2.Id
        ];

        System.assertEquals(cs.Account_Origin__c, esc.Account_Origin_Lookup__c);
        System.assertEquals(c.Job_Title__c, esc.Contact_Role__c);
        System.assertEquals('New', esc.Status__c);
        System.assertEquals(cs.OwnerId, esc.CaseOwner__c);

        System.assertNotEquals(cs.Account_Origin__c, esc2.Account_Origin_Lookup__c);
        System.assertNotEquals(c.Job_Title__c, esc2.Contact_Role__c);
        System.assertEquals('New', esc2.Status__c);
        System.assertEquals(cs.OwnerId, esc2.CaseOwner__c);

        esc.ContactName__c = c2.Id;
        esc.Contact_Role__c = 'test';
        update esc;

        esc = [
                SELECT Id, Contact_Role__c
                FROM Qlikview_Support_Escalation__c
                WHERE ID = :esc.Id
        ];
        System.assertEquals(c2.Job_Title__c, esc.Contact_Role__c);
    }
}