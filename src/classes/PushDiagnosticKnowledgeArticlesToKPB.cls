/**
* PushDiagnosticKnowledgeArticlesToKPB
*
* Description:	Job to push Diagnostic__kav Knowledge Articles to the public knowledge base.
* 				The push criteria is in PushKnowledgeArticlesToKPBHandler.findActualArticleIds().
*
* Added: 		11-07-2018 - ext_vos - CHG0034282
*
* Change log:
* 09-11-2018 - ext_vos - CHG0034844 - update constructor for "publishing by flag" too.
*
*/
global without sharing class PushDiagnosticKnowledgeArticlesToKPB implements Database.Batchable<sObject>, Database.Stateful {
	
    global final static String ARTICLE_TYPE = 'Diagnostic__kav';
    global String query;
    global String actualIds;
    global List<String> toManualUpdate;
    global Set<String> errorArticleNumbers = new Set<String>();

    global PushDiagnosticKnowledgeArticlesToKPB(List<String> articleNumbers) {
        toManualUpdate = new List<String>();
        toManualUpdate.addAll(articleNumbers);
        PushDiagnosticKnowledgeArticles(false);
    }

    global PushDiagnosticKnowledgeArticlesToKPB() {
        PushDiagnosticKnowledgeArticles(true);
    }

    global void PushDiagnosticKnowledgeArticles(Boolean isRegularFlow) {
        // prepare list of objects from handler
        if (isRegularFlow) {
            // get the list of articles for scheduled process
            actualIds = PushKnowledgeArticlesToKPBHandler.findActualArticleIds(ARTICLE_TYPE);
        } else {
            // get the list of articles for one-time publishing
            actualIds = PushKnowledgeArticlesToKPBHandler.findArticleIdsByNumbers(ARTICLE_TYPE, toManualUpdate);
        }
        if (String.isEmpty(actualIds)) {
            query = 'SELECT Id FROM ' + ARTICLE_TYPE + ' WHERE Id = null';            
        } else {
            query = 'SELECT Id, ArticleNumber, KnowledgeArticleId FROM ' + ARTICLE_TYPE + ' WHERE Id in (' + actualIds + ')';
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Diagnostic__kav> records) {
        Set<String> errors = PushKnowledgeArticlesToKPBHandler.publish(records, ARTICLE_TYPE);
        if (!errors.isEmpty()) {
            errorArticleNumbers.addAll(errors);
        }
    }

    global void finish(Database.BatchableContext BC) {        
        PushKnowledgeArticlesToKPBHandler.generateReport(ARTICLE_TYPE, actualIds, errorArticleNumbers);
    }
}