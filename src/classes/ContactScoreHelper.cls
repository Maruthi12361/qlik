/***************************************************
Trigger: ContactScore_Trigger
Object: Contact
Description: CR# 20060 – Add Predictive Lead Score Grade and Overall Priority
Description: CR# 23836 – Lead/Contact Lifecycle-Clear FUR Fields
Description: CR# 30746 – New Predictive Lead Score Field and Revise Grade
Change Log:
 20150312   Madhav Kakani - Fluido Denmark
 20150427   CCE CR# 35752 - Add conditions to Clear FUR fields only if the new Contact Status is different from the old one
 20150513   CCE CR# 35713 - Add exception condition to Clear FUR fields for Follow-Up Rejected Reason
 20150713   CCE/Madhav Kakani - Fluido Denmark - Changed logic for CR# 30746
 20150901   CCE CR# 56778 - Update Predictive Lead Score Grade Thresholds
 20161020   CCE CR# 95899 - Update Predictive Lead Score Grade field based on 6sense Scores
 20170810   CCE CHG0030380 - clear manual re-assignment flag
 20171212   CCE CHG0032730 - remove manual re-assignment flag
 20180328   CCE CHG0033445 - Set Conversica options field to Stop (case sensitive)
 20180611   CCE CHG0034173 Remove "with sharing", add additional FU clearing statuses
 20181012   CCE CHG0034825 BMW-1054 Fix order of execution issue by bringing "Set FU Disqualified for Lattice Scoring" workflow into this code
 20181012   CCE CHG0034697 BMW-1015 change follow up disposition conditions
******************************************************/

public class ContactScoreHelper {
	
	public static void addLeadScoreGrade(List<Contact> triggernew, Map<id,Contact> triggeroldmap){
		system.debug('ContactScore_Trigger: starting'); // : Semaphores.ContactScoreTrigger_HasRunBefore = ' + Semaphores.ContactScoreTrigger_HasRunBefore);
    //if(Semaphores.ContactScoreTrigger_HasRunBefore) return; // ensure that the trigger runs only once during a context
    //else Semaphores.ContactScoreTrigger_HasRunBefore = true;

    for(Contact c : triggernew) {        
        //// Calculate Predictive Lead Score Grades
        //if(c.Predictive_Lead_Score__c >= 91) {
        //    c.Predictive_Lead_Score_Grade__c = 'A'; 
        //    c.Predictive_Lead_Score_Numeric_Grade__c = '5'; 
        //}
        //else if(c.Predictive_Lead_Score__c >= 71) {
        //    c.Predictive_Lead_Score_Grade__c = 'B';
        //    c.Predictive_Lead_Score_Numeric_Grade__c = '4'; 
        //}
        //else if(c.Predictive_Lead_Score__c >= 31) {
        //    c.Predictive_Lead_Score_Grade__c = 'C';
        //    c.Predictive_Lead_Score_Numeric_Grade__c = '3'; 
        //}
        //else if(c.Predictive_Lead_Score__c >= 1) {
        //    c.Predictive_Lead_Score_Grade__c = 'D';
        //    c.Predictive_Lead_Score_Numeric_Grade__c = '1'; 
        //}
        //else c.Predictive_Lead_Score_Grade__c = null;
        
        //if(c.Predictive_Lead_Score_Grade__c == '' || c.Predictive_Lead_Score_Grade__c == null) {
        //    c.Predictive_Lead_Score_Numeric_Grade__c = '2'; 
        //}
        // Overall Priority is calculated with a formula        


        if ((c.Contact_Status__c == 'Follow-Up Attempt 1' || c.Contact_Status__c == 'Follow-Up Attempt 2' || c.Contact_Status__c == 'Follow-Up Attempt 3' || c.Contact_Status__c == 'Follow-Up Attempt 4' || c.Contact_Status__c == 'Follow-Up Attempt 5') && 
            c.Number_of_Follow_Up_Voicemails__c >= 6 && c.Number_of_Follow_Up_Emails__c >= 1) {
            c.Contact_Status__c = 'Follow-Up Disqualified';
            c.Follow_Up_Disqualified_Reason__c = 'No Response';
        }

        // Clear FUR fields
        Contact oldCon = triggeroldmap.get(c.Id);  //CCE CR# 35752
        //System.debug('ContactScore_Trigger: c.Contact_Status__c = ' + c.Contact_Status__c);
        //System.debug('ContactScore_Trigger: oldCon.Contact_Status__c = ' + oldCon.Contact_Status__c);
        if (((c.Contact_Status__c == 'Follow-Up Rejected' && oldCon.Contact_Status__c != 'Follow-Up Rejected') && (!(c.Follow_Up_Rejected_Reason__c == 'Incorrect Routing' || c.Follow_Up_Rejected_Reason__c == 'Incomplete/Inaccurate Info'))) ||   //CCE CR# 35713
            (c.Contact_Status__c == 'Follow-Up Disqualified' && oldCon.Contact_Status__c != 'Follow-Up Disqualified') ||
            (c.Contact_Status__c == 'Goal Discovery' && oldCon.Contact_Status__c != 'Goal Discovery') ||
            (c.Contact_Status__c == 'Archived' && oldCon.Contact_Status__c != 'Archived') ||
            (c.Contact_Status__c == 'Junk' && oldCon.Contact_Status__c != 'Junk') ||
            (c.Contact_Status__c == 'Follow-Up Flagged in Error' && oldCon.Contact_Status__c != 'Follow-Up Flagged in Error') ||
            (c.Contact_Status__c == 'Follow-Up Failed' && oldCon.Contact_Status__c != 'Follow-Up Failed') ||
            (c.Contact_Status__c == 'Contacted-Additional Work Required' && oldCon.Contact_Status__c != 'Contacted-Additional Work Required'))
        {
            system.debug('ContactScore_Trigger: clearing Follow-Up flags');
            if (c.Event_Follow_Up_Required__c == true) {
                c.Event_Follow_Up_Required__c = false;
                c.Event_Follow_Up_Completed_Date__c = System.today();
            }
            if (c.Existing_Follow_Up_Required__c == true) {
                c.Existing_Follow_Up_Required__c = false;
                c.Existing_Follow_Up_Completed_Date__c = System.today();
            }
            if (c.New_Follow_Up_Required__c == true) {
                c.New_Follow_Up_Required__c = false;
                c.New_Follow_Up_Completed_Date__c = System.today();
            }
            //adding next two lines as the workflow to clear this (Overall Follow-Up Required Flow End) does not always work
            c.Overall_Follow_up_Required__c = false; 
            c.Overall_Follow_Up_Completed_Date__c = System.today();
            //CHG0033445
            if ((c.AVA__AVAAI_hot_lead__c == true) || (c.AVA__AVAAI_action_required__c == true)) {
                c.AVA__AVAAI_options__c = 'Stop';
            }
            //c.Manually_Re_assigned__c = false;
        }
    }
    system.debug('ContactScore_Trigger: finishing');  
	}
}