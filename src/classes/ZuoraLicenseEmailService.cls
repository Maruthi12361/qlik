global class ZuoraLicenseEmailService implements Messaging.InboundEmailHandler {

	public static final String TYPECONST = 'Other';
	public static final String REGEX = 'Q\\d{6}';

	global virtual class ZuoraLicenceEmailServiceException extends Exception {}

	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env) {

		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

		// Logging some of the email properties
		System.debug('htmlBody');
		System.debug(email.htmlBody);
		System.debug('htmlBodyIsTruncated');
		System.debug(email.htmlBodyIsTruncated);
		System.debug('plainTextBody');
		System.debug(email.plainTextBody);
		System.debug('plainTextBodyIsTruncated');
		System.debug(email.plainTextBodyIsTruncated);
		System.debug('subject');
		System.debug(email.subject);

		//Find the Zuora qoute in email
		Matcher pm = Pattern.compile(REGEX).matcher(email.subject);

		if (pm.find()) {
			System.debug('******* Matcher *******');
			System.debug(pm);
			String zuoraQuoteNumber = pm.group(0);

			zqu__Quote__c zuoraQuote = [SELECT Id, Name, zqu__Number__c FROM zqu__Quote__c WHERE zqu__Number__c = :zuoraQuoteNumber LIMIT 1];

			if (zuoraQuote != null) {
				//Create the task.
				Task newTask = new Task(
					Type = TYPECONST,
					//WhoId = email.targetObjectId,
					Subject = email.Subject,
					WhatId = zuoraQuote.Id,
					Status = 'Completed',
					Description = 'License email sent to: '+ string.join(email.toAddresses,',')
				);

				if (email.ccAddresses != null) {
					newTask.CC_Address__c = String.join(email.ccAddresses, ',');
				}
				insert newTask;

				//And insert the email as an attachment to the newly created Task record.
				Attachment attachment = new Attachment(
					Body = Blob.valueOf(email.htmlBody),
					Name = email.Subject + '.html',
					ParentId = newTask.id
				);
				insert attachment;

				//Set a successful result and we are done.
				result.success = true;
			}
		} else {
			System.debug('Error trying to find Zuora Quote number in license email');
			throw new ZuoraLicenceEmailServiceException('Error trying to find Zuora Quote number in license email');
		}
		return result;
	}
}