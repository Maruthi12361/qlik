/************************************************************************
*
*   ClearProductsAndPricebook_Test
*   
*   Changelog:
*       2013-10-04  CCE     Initial development. CR 7524 https://eu1.salesforce.com/a0CD000000XN6U6
*                           Test class for ClearProductsAndPricebookController
*                           Contains Org dependancies
*       2014-06-03  TJG     Fix test error
*                           System.DmlException: Insert failed. First exception on row 0; first error: INVALID_CROSS_REFERENCE_KEY, 
*                           Record Type ID: this ID value isn't valid for the user: 01220000000J1KRAA0: [RecordTypeId]
*
*       2015-04-20  SLH    Added function and solution area for OP006 validation rule
*       2016-09-21  MTM disabled fo Q2CW       
*************************************************************************/
@isTest(SeeAllData=true)
private class ClearProductsAndPricebook_Test {
/*
   static testMethod void Test_ClearProductsAndPricebookController() {
        Id OppRecordTypeId_QB2 = '012D0000000KEKO'; //Qlikbuy CCS Standard II (qtns) //Qlikbuy II (qtns) //012D0000000KEKO //'012c00000004T4F';  //Qlikbuy2 (QTDev) 
        // Id OppRecordTypeId = '01220000000J1KR'; //Qlikbuy CCS Standard (qtns)        commmented out 2014-06-03
        Id OppRecordTypeId = '012D0000000KEKO'; //QTNS '012f0000000CuglAAC'; //Qlikbuy CCS Standard II on QTNS
        Id NSPricebookId = '01sD0000000JOeZ';   //NS Pricebook ID (qtns) //'01sc00000000aNZ';  //NS Pricebook ID (qtdev)
        Id AccRecordTypeId = '01220000000DOFu';  //End User Account
    
        System.debug('Test_ClearProductsAndPricebookController: Starting');
        
        //Get a pricebookEntry Id that we can use later - but not from the NS Pricebook
        ID PB = [Select Id from PricebookEntry where IsActive = true and CurrencyIsoCode = 'GBP' and Pricebook2Id != :NSPricebookId  and Name like 'Licenses' LIMIT 1].Id;
        System.debug('Test_ClearProductsAndPricebookController: PB=' + PB);
   
        // Adding an account with BillingCountryCode and Qliktech Company
		Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs.id              
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        Account acc = new Account(
            Name = 'Test',
            RecordTypeId = AccRecordTypeId, //End User Account
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        Opportunity Opp = new Opportunity();
        Opp.RecordTypeId = OppRecordTypeId;
        Opp.Name = 'Test Opp 1';
        Opp.StageName = 'Goal Confirmed';
        Opp.CloseDate = Date.today();
        Opp.Type = 'New Customer';
        Opp.Revenue_Type__c = 'Direct';
        Opp.ForecastCategoryName = 'Omitted';
        Opp.AccountId = acc.Id; 
        Opp.CurrencyIsoCode = 'GBP';
        Opp.Function__c = 'Other';
        Opp.Solution_Area__c = 'Other';
        Opp.Signature_Type__c = 'Digital Signature';
        insert Opp;
        Opportunity OppR = [select Id, Amount, Pricebook2Id, ForecastCategoryName from Opportunity where Id = :Opp.Id];
        System.debug('Test_ClearProductsAndPricebookController: OppR=' + OppR);
    
        OpportunityLineItem oli = new OpportunityLineItem();            
        oli.OpportunityId = Opp.Id;
        //oli.UnitPrice = 20;
        oli.Quantity = 1;
        oli.PricebookEntryId = PB;        
        insert oli; 
        OpportunityLineItem oliNew = [select Id, UnitPrice from OpportunityLineItem where Id = :oli.Id];       
        System.debug('Test_ClearProductsAndPricebookController: oliNew=' + oliNew);
        
        Opportunity OppNew = [select Id, Amount, Pricebook2Id, RecordTypeId from Opportunity where Id = :Opp.Id];
        System.debug('Test_ClearProductsAndPricebookController: OppNew=' + OppNew);
        System.assertEquals(oliNew.UnitPrice, OppNew.Amount);
        System.assertEquals(OppRecordTypeId, OppNew.RecordTypeId);
        
        test.startTest();
            
        ApexPages.StandardController StandardController = new ApexPages.StandardController(Opp);
        ClearProductsAndPricebookController controller = new ClearProductsAndPricebookController(StandardController);
        controller.doTheClear();
        
        Opportunity OppNew2 = [select Id, Amount, Pricebook2Id, RecordTypeId from Opportunity where Id = :Opp.Id];
        System.debug('Test_ClearProductsAndPricebookController: OppNew2=' + OppNew2);
        System.assertEquals(1, OppNew2.Amount);
        System.assertEquals(OppRecordTypeId_QB2, OppNew2.RecordTypeId);
        
        test.stopTest();
    }
	*/
}