/*****************************************************************************************
* 2015-09-28 AIN Webservice Mock for class MetadataService, using WebServiceMockDispatcher is prefered, see
* that class for details.
****************************************************************************************/
@isTest
public class MetadataServiceMock implements WebServiceMock {

  public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    system.debug('MetadataServiceMock Start');
    if(request instanceof MetadataService.update_element)
    {
    	system.debug('instanceof MetadataService.UpdateMetadataResponse_element');
      	MetadataService.updateResponse_element responseElement = new MetadataService.updateResponse_element();
       	responseElement.result = new MetadataService.AsyncResult[1];
       	responseElement.result[0] = new MetadataService.AsyncResult();
      	response.put('response_x', responseElement);
    }
    else if(request instanceof MetadataService.checkstatus_element)
    {
    	system.debug('instanceof MetadataService.checkstatus_element');
      	MetadataService.checkstatusResponse_element responseElement = new MetadataService.checkstatusResponse_element();
       	responseElement.result = new MetadataService.AsyncResult[1];
       	responseElement.result[0] = new MetadataService.AsyncResult();
      	response.put('response_x', responseElement);
    }
    system.debug('MetadataServiceMock End');
       
  }
}