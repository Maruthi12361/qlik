/********************************************************
* CLASS: WebActivityPartnerUserCreation
* DESCRIPTION: Handles product User record creation when event type is invitation created
*
* CHANGELOG:    
*   2018-01-31 - CRW - Added Initial logic
*********************************************************/
global class WebActivityPartnerUserCreation {
    public static List<WebActivityUtils.WebActivityContent> PartnerUserCreation(List<WebActivityUtils.WebActivityContent> lstWAContent,List<String> ulcForProductTrial){
        system.debug('PartnerUserCreation');
        List<Id> lstLeadsContacts = new List<Id>();
        map<string,string> mapPUSaveResult = new map<string,string>();
        map<String,String> mapContactWithTriaId = new map<String,String>();
        map<String,String> mapLeadWithTriaId = new map<String,String>();
        List<Product_User__c> lstProductUsers = new List<Product_User__c>();
        List<String> leadContactIds = new List<String>();
        map<String,List <Product_User__c>> mapExistingPU = new map<String,List <Product_User__c>>();
        Boolean createPU = true;
        
        system.debug('wa' + lstWAContent);
        if (ulcForProductTrial.size() > 0){
            //check if Pu already exists
            for(WebActivityUtils.WebActivityContent wa : lstWAContent){
                if (String.isNotBlank(wa.oUser.LeadID)){leadContactIds.add(wa.oUser.LeadID);}
                else {leadContactIds.add(wa.oUser.ContactID);}
            }
            system.debug('ldid' + leadContactIds);
            if(leadContactIds.size() > 0){
                For (Product_User__c PURecords : [SELECT Id,Contact__c,Lead__c,Product_Trial__c FROM Product_User__c WHERE (Contact__c In :leadContactIds OR Lead__c In :leadContactIds) LIMIT 49000])
                {
                    if(String.isNotBlank(PURecords.Contact__c)){
                        if(mapExistingPU.containskey(PURecords.Contact__c)){
                            mapExistingPU.get(PURecords.Contact__c).add(PURecords);
                        }else{mapExistingPU.put(PURecords.Contact__c,new List<Product_User__c>{PURecords});}
                    }
                    else if(String.isNotBlank(PURecords.Lead__c)){
                        if(mapExistingPU.containsKey(PURecords.Lead__c)){
                            mapExistingPU.get(PURecords.Lead__c).add(PURecords);
                        }else {mapExistingPU.put(PURecords.Lead__c,new List<Product_User__c>{PURecords});}
                    }
                }
            }
            
            system.debug('mapExistingPU' + mapExistingPU);
            //Query ulc details and take out lead/contacts. Then use those lead/contact to query Product trial to be used in PU creation
        
            System.debug(ulcForProductTrial);
            for (ULC_Details__c ulc :[SELECT Id, ULCName__c, LeadId__c, ContactId__c, qlikid__c FROM ULC_Details__c WHERE qlikid__c in :ulcForProductTrial]){
                if(String.isNotBlank(ulc.ContactId__c)){
                    lstLeadsContacts.add(ulc.ContactId__c);
                    mapContactWithTriaId.put(ulc.ContactId__c,ulc.qlikid__c);
                }else if(String.isNotBlank(ulc.LeadId__c)){
                    lstLeadsContacts.add(ulc.LeadId__c);
                 	mapLeadWithTriaId.put(ulc.LeadId__c,ulc.qlikid__c);
                }
            }
            system.debug('lstLeadsContacts' + lstLeadsContacts);
            map<String,Product_Trial__c> mapLeadContactWithPT = new map<String,Product_Trial__c>();
            system.debug('mapContactWithTriaId' + mapContactWithTriaId);
            for (Product_Trial__c lstPT : [SELECT Id,Contact__c,Lead__c FROM Product_Trial__c WHERE (Contact__c In :lstLeadsContacts OR Lead__c In :lstLeadsContacts) order by createdDate DESC])
            {
                if(String.isNotBlank(lstPT.Contact__c)){
                    if (!mapLeadContactWithPT.containsKey(lstPT.Contact__c)){
                        mapLeadContactWithPT.put(lstPT.Contact__c,lstPT);
                    }
                }else if(String.isNotBlank(lstPT.Lead__c)){
                    if (!mapLeadContactWithPT.containsKey(lstPT.Lead__c)){
                        mapLeadContactWithPT.put(lstPT.Lead__c,lstPT);
                    }
                }

            }
            for (id i:mapLeadContactWithPT.keySet()){
                if (mapContactWithTriaId.containsKey(i)){
                    String qlikIdTemp = mapContactWithTriaId.get(i);
                    mapContactWithTriaId.remove(i);
                    mapContactWithTriaId.put(qlikIdTemp,mapLeadContactWithPT.get(i).id);
                }else if(mapLeadWithTriaId.containsKey(i)){
                    String qlikIdTemp = mapLeadWithTriaId.get(i);
                    mapLeadWithTriaId.remove(i);
                    mapLeadWithTriaId.put(qlikIdTemp,mapLeadContactWithPT.get(i).id);
                }
            }
        }
        system.debug('mapContactWithTriaId' + mapContactWithTriaId);
        for(WebActivityUtils.WebActivityContent wa : lstWAContent){
            if (String.isBlank(wa.Status) || wa.Status != 'Failed'){
                if (String.isNotBlank(wa.WebActivityType) && wa.WebActivityType.contains('invited')){
					if(mapExistingPU.containsKey(wa.oUser.LeadID)){
                        List <Product_User__c> existingPUList= mapExistingPU.get(wa.oUser.LeadID);
                        for(Product_User__c pul:existingPUList){
                            system.debug('idsssss' + pul.Product_Trial__c + ',' + mapLeadWithTriaId.get(wa.oUser.ulcqlikId));
                            if(pul.Product_Trial__c == mapLeadWithTriaId.get(wa.oUser.ulcqlikId) || pul.Product_Trial__c == mapContactWithTriaId.get(wa.oUser.ulcqlikId)){
                                createPU = false;
                            }
                        }
                    }else if(mapExistingPU.containsKey(wa.oUser.ContactID)){
                        List <Product_User__c> existingPUList= mapExistingPU.get(wa.oUser.ContactID);
                        for(Product_User__c pul:existingPUList){
                            if(pul.Product_Trial__c == mapContactWithTriaId.get(wa.oUser.ulcqlikId) || pul.Product_Trial__c == mapLeadWithTriaId.get(wa.oUser.ulcqlikId)){
                                createPU = false;
                            }
                        }
                    }system.debug('flag' + createPU);
                    if(createPU){
                        Product_User__c pU = new Product_User__c();
                        if(String.isNotBlank(wa.oUser.LeadID)){
                            pU.Lead__c = wa.oUser.LeadID;
                        }else if(String.isNotBlank(wa.oUser.ContactID)){
                            pU.Contact__c = wa.oUser.ContactID;
                        }
                        if (mapLeadWithTriaId.ContainsKey(wa.oUser.ulcqlikId)){
                            pU.Product_Trial__c = mapLeadWithTriaId.get(wa.oUser.ulcqlikId);
                        }else if (mapContactWithTriaId.ContainsKey(wa.oUser.ulcqlikId)){
                            pU.Product_Trial__c = mapContactWithTriaId.get(wa.oUser.ulcqlikId);
                        }
                        
                        pU.Status__c = 'Active';
                        pU.User_Role__c = 'Invited';
                        pU.Status_Change_Date__c = date.today();
                        if(string.isNotBlank(wa.OUser.Subscription)){
                            pU.Subscription__c = wa.OUser.Subscription;
                        }
                        if(string.isNotBlank(wa.OUser.SubscriptionName)){
                            pU.Zuora_Subscription_ID__c = wa.OUser.SubscriptionName;
                        }
                        lstProductUsers.add(pU);
                    }
                }
            }
            
        }
        system.debug('ids for creation ' + lstProductUsers);
        if (lstProductUsers.size() > 0){
            try {
            List<Database.SaveResult> saveResults = Database.insert(lstProductUsers, false);
            for(integer i = 0; i < saveResults.size(); i++){
                String msg = 'Error (Product users creation): ';
                if(saveResults[i].isSuccess()){
                    if (String.isNotBlank(lstProductUsers.get(i).Lead__c)){
                        mapPUSaveResult.put(lstProductUsers.get(i).Lead__c,saveResults[i].getId());
                    } else {
                        mapPUSaveResult.put(lstProductUsers.get(i).Contact__c,saveResults[i].getId());
                    }
                }
                else
                {
                    for(Database.Error err: saveResults[i].getErrors()){  
                        msg += err.getmessage() + '\n';
                    }
                    if (String.isNotBlank(lstProductUsers.get(i).Lead__c)){
                        mapPUSaveResult.put(lstProductUsers.get(i).Lead__c, msg);
                    }
                    else {
                        mapPUSaveResult.put(lstProductUsers.get(i).Contact__c, msg);
                    }
                }
            }
        }
        catch(System.Exception ex){
            system.debug('Product users failed to Create -');
        }
        
        system.debug('result map' + mapPUSaveResult);
            for (WebActivityUtils.WebActivityContent PUResult : lstWAContent){
                String LeadPUResults = mapPUSaveResult.get(PUResult.oUser.LeadId);
                if (String.isNotBlank(LeadPUResults) && LeadPUResults.startsWith('Error')){
                    PUResult.status = 'Failed';
                    PUResult.AuditTrail = PUResult.AuditTrail + LeadPUResults;
                }else if (String.isNotBlank(LeadPUResults) && !LeadPUResults.contains('Error')){
                    if (PUResult.status != 'Failed'){
                    	PUResult.status = 'Completed';
                    }
                }
            }
                
        for (WebActivityUtils.WebActivityContent PUResult : lstWAContent){
            String ContactPUResults = mapPUSaveResult.get(PUResult.oUser.ContactId);
            if (String.isNotBlank(ContactPUResults) && ContactPUResults.startsWith('Error')){
                PUResult.status = 'Failed';
                PUResult.AuditTrail = PUResult.AuditTrail + ContactPUResults;
            }   
            else if (String.isNotBlank(ContactPUResults) && !ContactPUResults.contains('Error')){
                if (PUResult.status != 'Failed'){
                    PUResult.status = 'Completed';
                }
            }
        }
        }
        return lstWAContent;
    }

}