/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Linus Löfberg
* @version        1.0
* @created        2019-06-04
* @modified       2019-06-04
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
*
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
private class Z_SubscriptionTriggerHandlerTest {

    @testSetup
    static void testSetup() {

    }

    @isTest
    static void test_connect_ProductUser_pos() {
        Test.startTest();
        Product_User__c testPU = new Product_User__c();
        testPU.Zuora_Subscription_ID__c = 'IDK123456789KI';
        testPU.User_Role__c = 'Admin';
        testPU.Status__c = 'Active';
        insert testPU;
        System.assertNotEquals(null, testPu.Id);

        Zuora__Subscription__c testSubsctiption = new Zuora__Subscription__c();
        testSubsctiption.Name = 'IDK123456789KI';
        insert testSubsctiption;
        System.assertNotEquals(null, testSubsctiption.Id);

        testPU = [SELECT Id, Name, Zuora_Subscription_ID__c, Subscription__c FROM Product_User__c WHERE Id = : testPU.Id];
        System.assertEquals(testSubsctiption.Id, testPU.Subscription__c);
        Test.stopTest();
    }

    @isTest
    static void test_connect_ProductUser_neg() {
        Test.startTest();
        Product_User__c testPU = new Product_User__c();
        testPU.Zuora_Subscription_ID__c = 'IDK123456789KI';
        testPU.User_Role__c = 'Admin';
        testPU.Status__c = 'Active';
        insert testPU;
        System.assertNotEquals(null, testPu.Id);

        Zuora__Subscription__c testSubsctiption = new Zuora__Subscription__c();
        testSubsctiption.Name = 'testsub';
        insert testSubsctiption;
        System.assertNotEquals(null, testSubsctiption.Id);
        testSubsctiption = [SELECT Id, Name FROM Zuora__Subscription__c WHERE Id = :testSubsctiption.Id];

        testPU = [SELECT Id, Name, Zuora_Subscription_ID__c, Subscription__c FROM Product_User__c WHERE Id = : testPU.Id];
        System.assertNotEquals(testSubsctiption.Id, testPU.Subscription__c);
        Test.stopTest();
    }
}