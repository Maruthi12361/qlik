/*
* Change log:
*
* 2018-01-18    ext_vos     CHG0032673: update logic for isCustomer/isIndirectCustomer user. 
* 2018-03-28    ext_vos     CHG0032295: add search logic for 'Trending STT Webinars' box.
* 2018-05-08    ain         IT-597 added filter for STT Webinars
* 2018-10-05    ain         IT-1000 added changes for Lithium Community
* 2018-11-13    ext_vos     CHG0034901: show STT webinars links for guest and authorized users; update 'Useful links' with custom settings.
* 2019-05-05    AIN         IT-1597 Updated for Support Portal Redesign
* 2019-11-21    AIN         IT-2320 Added filter expression for Coveo
*/
public with sharing class QS_Controller {

    // public Boolean CaseDetails { get { return (ApexPages.currentPage().getParameters().get('Id') == '099c00000000LXG'); }}
    // public Boolean ListCases { get { return (ApexPages.currentPage().getParameters().get('Id') == '099c00000000LWN'); }}
    // public Boolean ListEnvironment   { get { return (ApexPages.currentPage().getParameters().get('Id') == '099c00000000LU7'); } }
    // public Boolean CreateEnvironment   { get { return (ApexPages.currentPage().getParameters().get('Id') == '099c00000000LMh'); } }
    // public Boolean Qs_Home   { get { return (ApexPages.currentPage().getParameters().get('Id') == '099c00000000LKC'); } }
 
    public string UserName{get{return (UserInfo.getName());}}
    public string UserId{get{return (UserInfo.getUserId());}}
    public string FirstName{get{return (UserInfo.getFirstName());}}

    public Boolean noEmail4me { get; set; }
    public Boolean isHomePage {get; set;}
    private Contact myContact;
    private Id contactId;

    public List<Case> caselist;

    public Boolean showBugPopUp {get; set;}
    public List<Case> bugCaseList {get; set;}

    //public List<Case> caseSurveyList {get; set;}
    public Boolean unauthenticated {get; set;}
    public Boolean isPartnerAccount {get; set;}
    public boolean isIndirectCustomer {get; set;}
    public boolean isCustomer {get; set;}
    
    public static final String STATUS_PENDING_CONTACT_RESPONSE = 'Pending Contact Response';
    public static final String STATUS_CONTACT_RESPONSE_RECEIVED = 'Contact Response Received';
    public static final String STATUS_SOLUTION_SUGGESTED = 'Solution Suggested';
    public static final String STATUS_CLOSED = 'Closed';
    public static final String STATUS_COMPLETED = 'Completed';
    public static final String STATUS_RESOLVED = 'Resolved';

    public list<SupportBlogEntry> supportBlogList { get; set; }

    String[] Persona= new String[]{};
    public String[] SelectedPersona {get;set;}
    public boolean ShowPersonaPopUp {get;set;}
    public Integer PersonaPopUpShown  {get;set;}

    public string CurrentPage {get;set;}

    public string LoginPageURL {get;set;}
    public string CSSBaseURL {get;set;}

    //Used to create a link to the livechat
    public string OrganizationId { get;  set; }
    public string LiveChatDeploymentId { get;  set; }
    
    // show 'Trending STT Webinars' links
    //public QS_Search sttWebinarSearch {get; set;}

    public string CommunitySource {get; set;}
    public string BlogLink {get; set;}
    public string BlogLinkRSS {get; set;}
    public integer CommunityPostListNumberOfResults {get; set;}
    public string CoveoFilterExpression { get; set; }

    public QS_Controller() {
        CommunitySource = 'Lithium';
        BlogLinkRSS = 'https://stage.community.qlik.com/cyjdu72974/rss/board?board.id=qlik-support-updates-blog';
        BlogLink = 'https://stage.community.qlik.com/t5/Qlik-Support-Updates-Blog/bg-p/qlik-support-updates-blog';

        QS_SearchSettings__c searchSettings = QS_SearchSettings__c.getInstance('SearchSettings');
        if(searchSettings != null) {
            if(searchSettings.Community_Source__c != null)
                CommunitySource = searchSettings.Community_Source__c;
            if(searchSettings.Community_Post_List_Number_Results__c != null)
                CommunityPostListNumberOfResults = (integer)searchSettings.Community_Post_List_Number_Results__c;
            if(searchSettings.Blog_Link_RSS__c != null)
                BlogLinkRSS = searchSettings.Blog_Link_RSS__c;
            if(searchSettings.Blog_Link__c != null)
                BlogLink = searchSettings.Blog_Link__c;
        }
       //Userid = ApexPages.currentPage().getParameters().get('id');   

       //Userid = ApexPages.currentPage().getParameters().get('id');   
       system.debug('QS_Controller: UserID'+ Userid);

       isPartnerAccount = false;
       isIndirectCustomer = false;
       isCustomer = false;
       User currentUser;
       // retrieving the current user info
       if(userinfo.getUserId() != null)
        {
            currentUser = [Select Id, Name, ContactId, Contact.AccountId, Contact.Persona__c, Contact.Name, Contact.Account.Name, Contact.Account.IsPartner, Contact.Account.IsCustomerPortal, 
                Contact.Account.QT_Designated_Support_Contact__c, Contact.Account.Partner_Support_Contact__c, Contact.Account.Navision_Status__c
                From User where Id = :userinfo.getUserId() LIMIT 1];
        }     
        // Specifying whether the current user is a customer or partner
        isPartnerAccount = (currentUser != null && currentUser.ContactId != null && currentUser.Contact.AccountId != null && currentUser.Contact.Account.IsPartner != null) ? currentUser.Contact.Account.IsPartner : false;
        
        if (currentUser != null && currentUser.ContactId != null && currentUser.Contact.AccountId != null && currentUser.Contact.Account.Navision_Status__c != null) {
            Boolean isCPLOG = QS_Utility.HasULCLevel(currentUser.ContactId, 'CPLOG');
            Boolean isCPVIEW = QS_Utility.HasULCLevel(currentUser.ContactId, 'CPVIEW');

            isCustomer = currentUser.Contact.Account.Navision_Status__c == 'Customer' && isCPLOG;
            isIndirectCustomer = currentUser.Contact.Account.Navision_Status__c == 'Customer' && isCPVIEW;
        }

        system.debug('isPartnerAccount: '+ isPartnerAccount);
        system.debug('isCustomer: '+ isCustomer);
        system.debug('isIndirectCustomer: '+ isIndirectCustomer);

        system.debug('QS_Controller: UserID'+ Userid);
        if (null == noEmail4me) {
          getMyContact();
          noEmail4me  = null == myContact ? false : (null == myContact.QS_Opt_out_case_emails__c ? false : myContact.QS_Opt_out_case_emails__c);
        }
        showBugPopUp = false;
        User userRec = [Select Id, AccountId, ContactId From User where Id = :userinfo.getUserId() AND IsActive = true LIMIT 1];
        unauthenticated = (userRec == null || userRec.ContactId == null || userRec.AccountId == null) ? true : false;

        isHomePage = true;
        initBug();
        initSurvey();

        CurrentPage = ApexPages.currentPage().getURL();
        if(CurrentPage.contains('/')) {
            CurrentPage = CurrentPage.Substring(CurrentPage.lastindexof('/')+1);
            system.debug('CurrentPage: ' + CurrentPage);
        }
        if(myContact != null)
        {
            SelectedPersona = myContact.Persona__c == null ? new string[]{} : myContact.Persona__c.split(';');
            for(Integer j = 0;j<SelectedPersona.size();j++)
                SelectedPersona[j] = SelectedPersona[j].Trim();
        }
       
        //getPersonasList();
           
        // Show the persona popUp if no persona selected   
        if(SelectedPersona!=null)
        {
            if(SelectedPersona.size()>0 || PersonaPopUpShown >0)
            {
                ShowPersonaPopUp=false;
            }
            else
            {
                if(PersonaPopUpShown== null)
                    PersonaPopUpShown=0;
                PersonaPopUpShown =PersonaPopUpShown + 1;
                ShowPersonaPopUp = true;
            }
        }

        if (! Test.isRunningTest()) { 
            loadSupportBlogFeed();
        }

        QS_Partner_Portal_Urls__c partnerPortalURLs = QS_Partner_Portal_Urls__c.getInstance();
        LoginPageURL = partnerPortalURLs.Support_Portal_Login_Page_Url__c;
        CSSBaseURL = partnerPortalURLs.Support_Portal_CSS_Base__c;

        OrganizationId = GetOrganizationId();
        LiveChatDeploymentId = GetLiveChatDeploymentId();

        system.debug('OrganizationId: ' + OrganizationId);
        system.debug('LiveChatDeploymentId: ' + LiveChatDeploymentId);

        Map<string, string> categorySearch = new Map<string, string>();
        categorySearch.put('Product__c','STT_Webinar'); 
        //sttWebinarSearch = new QS_KnowledgeSearch('STT Webinar', categorySearch, 'LastPublishedDate', 2);
        
        CoveoFilterExpression  = partnerPortalURLs.Coveo_Filter_Expression__c;       
    }

    private void initBug() {
        try {
            List<Bugs__History> bugHistoryList;
            //Added as tests don't have access to bughistory                                                
            if(test.isRunningTest())
            {
                bugHistoryList = new List<Bugs__History>();
                system.debug('Test 1');
                List<Case> caseList = [Select Bug__c From Case where Status != :STATUS_CLOSED AND Status != :STATUS_RESOLVED AND Status != :STATUS_COMPLETED AND Status != :System.Label.QS_CloseCase];
                if(caseList.Size() > 0)
                {
                    Id BugId = caseList[0].Bug__c;
                    system.debug('BugId: ' + BugId);
                    Bugs__History bh = new Bugs__History(ParentId = BugId, Field = 'Status__c');
                    bugHistoryList = new List<Bugs__History>();
                    bugHistoryList.add(bh);
                }
            }
            else
            {
                //Will never be covered by tests
                bugHistoryList = [Select ParentId, Parent.Status__c, OldValue, NewValue, Id, Field, CreatedDate, CreatedById 
                    From Bugs__History 
                    Where Field = 'Status__c' 
                    AND ParentId IN (Select Bug__c From Case where Status != :STATUS_CLOSED AND Status != :STATUS_RESOLVED AND Status != :STATUS_COMPLETED 
                    AND Status != :System.Label.QS_CloseCase)];
            }
            if(bugHistoryList != null && !bugHistoryList.isEmpty()) {
                showBugPopUp = true;
                Set<Id> bugHistorySet = new Set<Id>();
                for(Bugs__History bug : bugHistoryList) {
                    bugHistorySet.add(bug.ParentId);
                }
                if(bugHistorySet != null && !bugHistorySet.isEmpty()) {
                    bugCaseList = [Select Id, CaseNumber, Status, CreatedDate, Bug__c, Bug__r.Name, Bug__r.Status__c, Bug__r.Bug_Id__c
                        From Case 
                        where Bug__c IN :bugHistorySet 
                        AND Bug__c != null AND Status != :STATUS_CLOSED AND Status != :STATUS_RESOLVED AND Status != :STATUS_COMPLETED AND Status != :System.Label.QS_CloseCase
                        order by CreatedDate ASC LIMIT 100];
                    if(bugCaseList != null && !bugCaseList.isEmpty())
                        showBugPopUp = true;
                    else
                        showBugPopUp = false;
                }
            } 
            else {
                showBugPopUp = false;
            }
        } 
        catch(Exception ex) {
            showBugPopUp = false;
        }
    }
    private void initSurvey() {

        //Uncomment for p2
        /*caseSurveyList = [Select Id, CaseNumber, Subject, Status, CreatedDate 
            From Case 
            Where Id NOT IN (Select Case_ID__c From Survey__c where Case_ID__c != null) 
            AND (Status = :STATUS_CLOSED OR Status = :STATUS_RESOLVED OR Status = :System.Label.QS_CloseCase OR Status = :STATUS_COMPLETED)
            AND (Survey_Status_Response__c = null OR Survey_Status_Response__c = '' OR (Survey_Status_Response__c = 'Later' AND (LastModifiedDate < :System.today() OR LastModifiedDate >= :System.today()+1)))
            Order By CreatedDate DESC, Survey_Status_Response__c ASC NULLS FIRST LIMIT 100]; // Might need to change the limit later on*/
    }
    
         
     //Is this being used or has it been moved to Jivesearch?
    /*public string callJiveWebService( string searchUrl ) {
            boolean responseError;
           // Retrieving the custom setting record to check if we need to invoke the web service call
           QS_Search_Setting__c jiveAPICallSettings = QS_Search_Setting__c.getInstance('Jive_Search');
           if(jiveAPICallSettings != null && jiveAPICallSettings.Active__c != null && jiveAPICallSettings.Active__c == true) {

                //make call to Jive
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(searchUrl);
                req.setMethod('GET');
                HttpResponse res = http.send(req);
                
                //system.debug ('~~~~ http response - ' + res.getBody() );
                
                // setting the return message if the web service call is not successful
                String returnMsg = '';
                responseError = false;
                
                if(res.getStatusCode() >= 300 && res.getStatusCode() != 306 && res.getStatusCode() != 308) {
                    responseError = true;
                    returnMsg = System.Label.QS_Jive_Search_Inactive_Message;
                    return returnMsg;
                }
                            
                return res.getBody();
           } else {
                // setting the return message if the web service is not invoked
                responseError = true;
                String returnMsg = System.Label.QS_Jive_Search_Inactive_Message;
                return returnMsg;
           }
    }  */       
         
         
         
         
    private void getContactId() {
        //getUserId();
        if (null == contactId)
            contactId = [select contactId from User where Id = :Userid limit 1].ContactId;
    }

    private void getMyContact() {
        getContactId();
        if (null == myContact && null != contactId)
            myContact = [Select Id,Persona__c, QS_Opt_out_case_emails__c from Contact where ID= :contactId limit 1];
    }
    
    /*public PageReference setMyEmailOption()
    {
        return null;
    }*/
    
    /*public PageReference saveMyEmailOption()
    {
         getContactId();
         Contact newContact = new Contact(Id=contactId, QS_Opt_out_case_emails__c=noEmail4me);
         System.debug('noEmail4me=' + noEmail4me);                      
         update newContact;                        
    }*/
      
     
     // Func for get/set all Personas  
    /*public String getUserPersona() {   
        system.debug('QS_Controller: UserID'+ Userid);  
        String Personat;    
        if (null != contactId) {
            getMyContact(); 
            if (null != myContact) {
                Persona.add(myContact.Persona__c);
                Personat= myContact.Persona__c;
            }
        }
        return Personat;
    }         
     
    public list<SelectOption> getPersonasList()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult fieldResult = contact.Persona__c.getDescribe();

        List<Schema.PicklistEntry> plvalues = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : plvalues )
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
     
    public String[] getPersona() {
        return Persona;
    }  
     
    public void setPersona(String[] Persona) {
        this.Persona= Persona;            
    } 
    
    public PageReference SaveBut() {
        // upload User photo to Chatter
        UploadUserProfilePic();
      
        //update Persona on Contact object 
        getMyContact();
        string selectedPersonaString = '';
        for(string persona : SelectedPersona)
        {
            if(selectedPersonaString != '')
            {
                selectedPersonaString += ';';
            }
            selectedPersonaString += persona.Trim(); 
        }
        myContact.Persona__c = selectedPersonaString;
        myContact.QS_Opt_out_case_emails__c=noEmail4me;
        update myContact;       

        return null;
    }*/
    
    
    /*public PageReference deleteSurveyStatus() {
        try 
        {
            if(surveyIdChosen != null) 
            {

                Case todel = [select Id, Survey_Status_Response__c,Performance_Comments__c , Performance_Communication__c,Performance_Support_Entitlement__c,
                Performance_Content_Utilization__c,Performance_Case_Quality__c,Performance_Troubleshooting__c  from Case where Id=:surveyIdChosen limit 1];
                todel.Survey_Status_Response__c = 'Remove';
                if(todel.Performance_Comments__c   == null || todel.Performance_Comments__c   == '')
                    todel.Performance_Comments__c   = 'Survey Removed';
                if(todel.Performance_Communication__c  == null || todel.Performance_Communication__c  == '')
                    todel.Performance_Communication__c  = 'N/A';
                if(todel.Performance_Support_Entitlement__c  == null || todel.Performance_Support_Entitlement__c  == '')
                    todel.Performance_Support_Entitlement__c  = 'N/A';
                if(todel.Performance_Content_Utilization__c  == null || todel.Performance_Content_Utilization__c  == '')
                    todel.Performance_Content_Utilization__c  = 'N/A';
                if(todel.Performance_Case_Quality__c  == null || todel.Performance_Case_Quality__c  == '')
                    todel.Performance_Case_Quality__c  = 'N/A';
                if(todel.Performance_Troubleshooting__c  == null || todel.Performance_Troubleshooting__c  == '')
                    todel.Performance_Troubleshooting__c  = 'N/A';

                update todel;
            }
        }
        catch (Exception ex) {
            //errorMessage = ex.getMessage();
            System.debug(ex.getMessage());
        }     

        return Page.QS_CaseListPage;
    }*/
     
    // Get list of cases awaiting response from the external user
    /*public List<Case> getCaseList() {
        getContactId();
       
        system.debug ('~~~~~~~~ getCaseList.contactId-' + contactId);
       
        // caselist= [select Id, CaseNumber, AccountId, Subject, Priority, Status, CreatedDate, ContactId from Case where Status = 'Open' and ContactId :contactId ];
        if(caselist == null || caselist.isEmpty())
            caselist = [SELECT Id, CaseNumber, Subject, Status, CreatedDate, ContactId 
                FROM Case 
                WHERE ContactId = :contactId AND (Status = :STATUS_PENDING_CONTACT_RESPONSE OR Status = :STATUS_SOLUTION_SUGGESTED)
                ORDER BY CreatedDate DESC LIMIT 100];
        system.debug ('~~~~~~~~ getCaseList.caselist.size-' + caseList.size() );
        return caselist;
    }*/
       
    /**public Integer getNumOfUserCases()
    {
        //getContactId();
        // caselist= [select Id, CaseNumber, AccountId, Subject, Priority, Status, CreatedDate, ContactId from Case where Status = 'Open' and ContactId :contactId ];
        if(caselist == null || caselist.isEmpty())
            getCaseList();
        Integer caseCount = (caselist != null && !caselist.isEmpty() && caselist.size() > 0) ? caselist.size() : 0;
        return caseCount;   
    }*/
    
    /**public List<Case> getTop20Cases()
    {
        if (caselist == null || caseList.isEmpty() || caselist.size() == 0)
        {
            getCaseList();
        }
        return caselist;
    }*/

    // User Profile Photo Upload
    /*public String getUserprofilePic ()
    {         
        User  lstuser = [select FullPhotoUrl from User where Id =: UserInfo.getUserId()];          
        String luser= lstuser.FullPhotoUrl;
        String[] parts = luser.split('/');
        Boolean isUploadedPhoto = parts[parts.size() - 2].length() == 15;
        
        if(isUploadedPhoto) {
            profileImageUrl=lstuser.FullPhotoUrl;
        }
        return  profileImageUrl;
    }
     
    public Attachment attachment {
        get {
            if(attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
      
    private List<string> ImageHexCodes =  new List<string>{'FFD8FFE0', '474946383761' , '474946383961', '49492A00', '4D4D002A', '89504E470D0A1A0A', '424D'};
    public  PageReference UploadUserProfilePic() {

        Blob b;
        system.debug('Attachment.Name: ' + attachment.Name);
        if (attachment.Name !=null)
        {
                  
            attachment.OwnerId = UserId;
            attachment.ParentId = contactId; // put it in running user's folder

       
            try{
                string description = 'Profile picture uploaded ' + datetime.now();
                attachment.description = description;
                insert attachment;
                b = attachment.body;
            } 
            catch (DMLException e) {
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
                return null;
            } 
            finally {
                //document.body = null; // clears the viewstate
                attachment = new attachment();
            }

            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully : '+b));
            String communityId = null;
            String userId= UserId;
           
            //ConnectApi.Photo photo = ConnectApi.ChatterUsers.setPhoto(communityId, userId,  new ConnectApi.BinaryInput(b,'image/jpg','userImage.jpg'));
            system.debug('Before setPhoto');
            try
            {
                system.debug('Before BinaryInput');
                string hex = BlobToHex(b);
                
                boolean isImage = false;
                if(hex.Length() > 20) {
                    hex = hex.substring(0,20);
                    system.debug('Hex: ' + hex);
                    for(string s : ImageHexCodes) {
                        if(hex.startswith(s))
                        {
                            isImage = true;
                            break;
                        }
                    }
                }
                if(isImage) {
                    ConnectApi.BinaryInput bi = new ConnectApi.BinaryInput(b,'image/jpg','userImage2.jpg');
                    system.debug('After BinaryInput');
                    ConnectApi.Photo photo = ConnectApi.ChatterUsers.setPhoto(communityId, userId, bi);
                }   
                else
                    system.debug('File is not an image, aborting');
            }
            catch (Exception ex) {
                system.debug('Exception in setPhoto');
            }
            system.debug('After setPhoto');
        }
        return null;
    }*/
     
    /**
    *  Loads support blogs from "https://community.qlik.com/blogs/supportupdates/feeds/posts"
    * 
    */
    public void loadSupportBlogFeed () {

        String feedURL = 'https://stage.community.qlik.com/cyjdu72974/rss/board?board.id=qlik-support-updates-blog';
        String sourceType = 'Lithium';
        boolean blogActive = true;
        integer numberOfEntries = 3;
        QS_SearchSettings__c searchSettings = QS_SearchSettings__c.getInstance('SearchSettings');
        if(searchSettings != null) {
            if(searchSettings.Blog_Active__c != null)
                blogActive = searchSettings.Blog_Active__c;
            if(searchSettings.Community_Source__c != null)
                sourceType = searchSettings.Community_Source__c;
            if(searchSettings.Blog_URL__c != null)
                feedURL = searchSettings.Blog_URL__c;
            if(searchSettings.Blog_Results__c != null)
                numberOfEntries = (integer)searchSettings.Blog_Results__c;
        }

        
        //String feedURL = 'https://community.qlik.com/blogs/supportupdates/feeds/posts';
        if(blogActive)
            supportBlogList = getRSSData ( feedURL, numberOfEntries, sourceType);
        else
            supportBlogList = new list<SupportBlogEntry>();
    }
     
    public list<SupportBlogEntry> getRSSData(string feedURL, Integer numberOfElementsToReturn, string dataType) {
        system.debug('getRSSData start');
        
        if (numberOfElementsToReturn == 0 ) 
            numberOfElementsToReturn = 3;
        
        list<SupportBlogEntry> listToReturn = new list<SupportBlogEntry>();
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(feedURL);
        if(dataType == 'Lithium'){
            Blob headerValue = Blob.valueOf('qlik:fYN~Rp6hY');
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
        }
        
        req.setMethod('GET');
         
        Dom.Document doc = new Dom.Document();
        Http h = new Http();
         
        try
        {
            HttpResponse res = h.send(req);
            doc = res.getBodyDocument();
            Dom.XMLNode rss = doc.getRootElement();
        
            Integer numberOfElements = 0;
            

            for(Dom.XmlNode entries : doc.getRootElement().getChildElements()) {
                string title = '';
                string link;
                date updated;

                for(Dom.XmlNode entry : entries.getChildElements()) {

                    system.debug ('Child element: ' + entry);

                    if(dataType != 'Lithium') {
                        if ('title' == entry.getName() ){
                            //system.debug ('~~~~~~~~entry.name-' + entry.getName()  + '~~text~~' + entry.getText() + '~~getAttibute~~' + entry.getAttributeValue('Name', null));
                            if ( entry.getText() != null ){
                                title = entry.getText();   
                            }
                        }
                        else if ('updated' == entry.getName() ){
                            updated = date.valueOf( entry.getText() );
                            //system.debug ('~~~~~~~~-updated-' + updated );
                        }
                        else if('link' == entry.getName()) {
                            link = entry.getAttribute('href', null);
                            //system.debug ('~~~~~~~~link-' + link + '-getText-' + entry.getText() );
                        }
                        
                    }
                    
                    if(dataType == 'Lithium' && entry.getName() == 'item') {
                        for(Dom.XmlNode lithiumEntry : entry.getChildElements()) {
                            system.debug ('Lithium Child element: ' + lithiumEntry);
                            system.debug ('Lithium Child element name: ' + lithiumEntry.getName());
                            if('link' == lithiumEntry.getName()) {
                                link = lithiumEntry.getText(); 
                                system.debug ('Lithium link: ' + link);
                            }
                            else if('date' == lithiumEntry.getName()){
                                updated = date.valueOf( lithiumEntry.getText() );
                                system.debug ('Lithium updated: ' + updated);
                            }
                            else if ('title' == lithiumEntry.getName() ){
                                //system.debug ('~~~~~~~~entry.name-' + entry.getName()  + '~~text~~' + entry.getText() + '~~getAttibute~~' + entry.getAttributeValue('Name', null));
                                if ( lithiumEntry.getText() != null ){
                                    title = lithiumEntry.getText();   
                                    system.debug ('Lithium title: ' + title);
                                }
                            }
                        }
                        if ( title != ''){
                            listToReturn.add( new SupportBlogEntry( title, link, updated ) );
                        }
                        if ( listToReturn.size() >= numberOfElementsToReturn ){ // get from custom settings
                            break;
                        }
                    }
                }
                if (title != '' && dataType != 'Lithium'){
                    listToReturn.add( new SupportBlogEntry( title, link, updated ) );
                }
                
                if ( listToReturn.size() >= numberOfElementsToReturn ){ // get from custom settings
                    break;
                }
            }
        }
        catch (CalloutException ex)
        {
            system.debug('CalloutException: ' + ex.getMessage());
        }
        
        system.debug ('~~~~~~~~~~~~listToReturn.-'+listToReturn);
         
        return listToReturn;
    }
             
    public class SupportBlogEntry{
        public string title       { get; set; }
        public string link        { get; set; }
        public date   updateddate { get; set; }
         
        public SupportBlogEntry( string title, string link, date   updateddate ){
            this.title          =   title;
            this.link           =   link;
            this.updateddate    =   updateddate;
        }
    }
              
    /*private static string BlobToHex(Blob input){
        Map<string, Integer> base64 = new map<string, Integer>{'A'=>0,'B'=>1,'C'=>2,'D'=>3,'E'=>4,'F'=>5,'G'=>6,'H'=>7,'I'=>8,'J'=>9,'K'=>10,'L'=>11,'M'=>12,'N'=>13,'O'=>14,'P'=>15,'Q'=>16,'R'=>17,'S'=>18,'T'=>19,'U'=>20,'V'=>21,'W'=>22,'X'=>23,'Y'=>24,'Z'=>25,'a'=>26,'b'=>27,'c'=>28,'d'=>29,'e'=>30,'f'=>31,'g'=>32,'h'=>33,'i'=>34,'j'=>35,'k'=>36,'l'=>37,'m'=>38,'n'=>39,'o'=>40,'p'=>41,'q'=>42,'r'=>43,'s'=>44,'t'=>45,'u'=>46,'v'=>47,'w'=>48,'x'=>49,'y'=>50,'z'=>51,'0'=>52,'1'=>53,'2'=>54,'3'=>55,'4'=>56,'5'=>57,'6'=>58,'7'=>59,'8'=>60,'9'=>61,'+'=>62,'/'=>63};
        Map<integer, string> hex = new Map<integer, string>{0=>'00',1=>'01',2=>'02',3=>'03',4=>'04',5=>'05',6=>'06',7=>'07',8=>'08',9=>'09',10=>'0A',11=>'0B',12=>'0C',13=>'0D',14=>'0E',15=>'0F',16=>'10',17=>'11',18=>'12',19=>'13',20=>'14',21=>'15',22=>'16',23=>'17',24=>'18',25=>'19',26=>'1A',27=>'1B',28=>'1C',29=>'1D',30=>'1E',31=>'1F',32=>'20',33=>'21',34=>'22',35=>'23',36=>'24',37=>'25',38=>'26',39=>'27',40=>'28',41=>'29',42=>'2A',43=>'2B',44=>'2C',45=>'2D',46=>'2E',47=>'2F',48=>'30',49=>'31',50=>'32',51=>'33',52=>'34',53=>'35',54=>'36',55=>'37',56=>'38',57=>'39',58=>'3A',59=>'3B',60=>'3C',61=>'3D',62=>'3E',63=>'3F',64=>'40',65=>'41',66=>'42',67=>'43',68=>'44',69=>'45',70=>'46',71=>'47',72=>'48',73=>'49',74=>'4A',75=>'4B',76=>'4C',77=>'4D',78=>'4E',79=>'4F',80=>'50',81=>'51',82=>'52',83=>'53',84=>'54',85=>'55',86=>'56',87=>'57',88=>'58',89=>'59',90=>'5A',91=>'5B',92=>'5C',93=>'5D',94=>'5E',95=>'5F',96=>'60',97=>'61',98=>'62',99=>'63',100=>'64',101=>'65',102=>'66',103=>'67',104=>'68',105=>'69',106=>'6A',107=>'6B',108=>'6C',109=>'6D',110=>'6E',111=>'6F',112=>'70',113=>'71',114=>'72',115=>'73',116=>'74',117=>'75',118=>'76',119=>'77',120=>'78',121=>'79',122=>'7A',123=>'7B',124=>'7C',125=>'7D',126=>'7E',127=>'7F',128=>'80',129=>'81',130=>'82',131=>'83',132=>'84',133=>'85',134=>'86',135=>'87',136=>'88',137=>'89',138=>'8A',139=>'8B',140=>'8C',141=>'8D',142=>'8E',143=>'8F',144=>'90',145=>'91',146=>'92',147=>'93',148=>'94',149=>'95',150=>'96',151=>'97',152=>'98',153=>'99',154=>'9A',155=>'9B',156=>'9C',157=>'9D',158=>'9E',159=>'9F',160=>'A0',161=>'A1',162=>'A2',163=>'A3',164=>'A4',165=>'A5',166=>'A6',167=>'A7',168=>'A8',169=>'A9',170=>'AA',171=>'AB',172=>'AC',173=>'AD',174=>'AE',175=>'AF',176=>'B0',177=>'B1',178=>'B2',179=>'B3',180=>'B4',181=>'B5',182=>'B6',183=>'B7',184=>'B8',185=>'B9',186=>'BA',187=>'BB',188=>'BC',189=>'BD',190=>'BE',191=>'BF',192=>'C0',193=>'C1',194=>'C2',195=>'C3',196=>'C4',197=>'C5',198=>'C6',199=>'C7',200=>'C8',201=>'C9',202=>'CA',203=>'CB',204=>'CC',205=>'CD',206=>'CE',207=>'CF',208=>'D0',209=>'D1',210=>'D2',211=>'D3',212=>'D4',213=>'D5',214=>'D6',215=>'D7',216=>'D8',217=>'D9',218=>'DA',219=>'DB',220=>'DC',221=>'DD',222=>'DE',223=>'DF',224=>'E0',225=>'E1',226=>'E2',227=>'E3',228=>'E4',229=>'E5',230=>'E6',231=>'E7',232=>'E8',233=>'E9',234=>'EA',235=>'EB',236=>'EC',237=>'ED',238=>'EE',239=>'EF',240=>'F0',241=>'F1',242=>'F2',243=>'F3',244=>'F4',245=>'F5',246=>'F6',247=>'F7',248=>'F8',249=>'F9',250=>'FA',251=>'FB',252=>'FC',253=>'FD',254=>'FE',255=>'FF'};
     
        string sIn = EncodingUtil.base64Encode(input);
        
        Integer srtIdx = 0;
        string strOut = '';
        for(Integer idx=0; idx < sIn.length()-4; idx+=4){
            strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)) 
                + hex.get(((base64.get(sIn.substring(idx+1, idx+2)) & 15)<<4) | (base64.get(sIn.substring(idx+2, idx+3)) >>> 2)) 
                + hex.get(((base64.get(sIn.substring(idx+2, idx+3)) & 3)<<6) | base64.get(sIn.substring(idx+3, idx+4)));
        }
     
        if(sIn.substring(sIn.length()-1, sIn.length()) != '='){
          Integer idx = sIn.length() - 4;
          strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)) 
                + hex.get(((base64.get(sIn.substring(idx+1, idx+2)) & 15)<<4) | (base64.get(sIn.substring(idx+2, idx+3)) >>> 2)) 
                + hex.get(((base64.get(sIn.substring(idx+2, idx+3)) & 3)<<6) | base64.get(sIn.substring(idx+3, idx+4)));
          
        } 
        else if(sIn.substring(sIn.length()-2, sIn.length()-1) != '='){
            Integer idx = sIn.length() - 4;
            strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)) 
                + hex.get(((base64.get(sIn.substring(idx+1, idx+2)) & 15)<<4) | (base64.get(sIn.substring(idx+2, idx+3)) >>> 2)); 
        } 
        else {
            Integer idx = sIn.length() - 4;
            strOut += hex.get((base64.get(sIn.substring(idx, idx+1)) << 2) | (base64.get(sIn.substring(idx+1, idx+2)) >>> 4)); 
        }
        return strOut;
    }*/

    public string GetOrganizationId() {
        String orgId = UserInfo.getOrganizationId().Mid(0,15);
        return orgId;
    }

    public string GetLiveChatDeploymentId() {
        List<LiveChatDeployment> lcds = [select Id, MasterLabel from LiveChatDeployment where MasterLabel = 'Support Deployment' limit 1];

        if(lcds.size() > 0) {
            LiveChatDeployment lcd = lcds[0];
            return String.valueOf(lcd.Id).Mid(0,15);
        }
        else
            return '';
    }
}