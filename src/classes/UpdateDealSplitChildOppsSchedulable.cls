/*
    Initial development: MTM
    2014-05-15  MTM CR# XXXX  UpdateDealSplitChildOpportunities.
*/
Global class UpdateDealSplitChildOppsSchedulable implements Schedulable
{

	public UpdateDealSplitChildOppsSchedulable() {} 

	global void execute(SchedulableContext sc)
	{ 
		UpdateDealSplitChildOpportunities batch = new UpdateDealSplitChildOpportunities(); 
		ID batchId = Database.executeBatch(batch); 
	} 
}
/*
UpdateDealSplitChildOppsSchedulable m = new UpdateDealSplitChildOppsSchedulable();
String sch = '0 0 * * * ?';
String jobID = system.schedule('UpdateDealSplitChild Job', sch, m);
*/