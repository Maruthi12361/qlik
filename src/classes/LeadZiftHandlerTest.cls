/*********************************************

	Class:LeadZiftHandlerTest

	Description: 
				Test class for LeadZiftHandler and trigger LeadZift

	Log History:
	2017-06-21    BAD    Created - CHG0031645
	2018-10-09	ext_bjd	 ITRM-26 Fix test related with portal account owner error.
**********************************************/
@isTest
private class LeadZiftHandlerTest {
	
	@isTest 
	static void test_setOwnerToPartner() {

    	User mockPortalAccountUsr = QTTestUtils.createMockPortalUser();
    	LeadZiftHandler tempObj = new LeadZiftHandler();

        System.runAs(mockPortalAccountUsr)
        { 
			User mockPRMBase = QTTestUtils.createMockUserForProfile('PRM - Base');

			Account acc = [SELECT id, Zift_Champion__c FROM Account WHERE ID IN (SELECT AccountID FROM Contact WHERE Id=:mockPRMBase.ContactId)];
			acc.Zift_Champion__c = mockPRMBase.ContactId;
			update acc;

            Lead lead = New Lead (
                FirstName = 'Lead',
                LastName = 'Test1001',
                Company = 'Test',
                Country = 'Sweden',
                Email = 'asd@ad.com',
                Zift_Lead_Partner__c = acc.Id
            );  
            insert lead;			
			
	        Test.startTest();

	        Semaphores.TriggerHasRun(1);
	        lead.Zift_Distribution_Status__c = 'Accepted';
	        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
			update lead;

			Lead l = [SELECT Id, OwnerID FROM Lead WHERE Id=:lead.id];

			system.assertEquals(mockPRMBase.Id, l.OwnerID);
			
			Test.stopTest();
		} 

	}

}