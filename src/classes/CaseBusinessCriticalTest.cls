/*
LOG Changes:

xxxx-xx-xx   XXX: Initial code
2014-02-10  CCE:    CR# 9045 Highlight Sev 1 Cases in Account chatter feed 2 https://eu1.salesforce.com/a0CD000000ZxcAW
                    Added tests TestSev1ChatterPost_NoSubject and TestSev1ChatterPost.
                    This is now being done by a Boomi process so we no longer need the tests (as the relevant part of 
                    the trigger code is commented out)
                    Boomi documentaion at https://qlikview.atlassian.net/wiki/display/ISD/SFDC+Case+%3E+SFDC+FeedPost
 */
/**************************************************************
* Change Log:
* 2012XXXX   MHN    Initial Development
* 20140626   RDZ    Adding a static function to create missing CaseTeamRole
*                   and adding it to testmethod to see if code coverage increases.
* 2015-11-11 NAD    Updated support level names per CR# 57556    
* 2017-10-27 ext_bad    Add creating custom settings
* 2019-26-12 ext_bad  IT-2363 Changed test to check current version of CaseBusinessCriticalHandler
***************************************************************/
@IsTest
private class CaseBusinessCriticalTest {

    static testMethod void testCases() {
        QuoteTestHelper.createCustomSettings();
        Id bhLundId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund' LIMIT 1].Id;
        Id bhLundEntId = [SELECT Id FROM BusinessHours WHERE Name = 'Lund Enterprise (8x7)' LIMIT 1].Id;
        Support_Office__c supportOffice =
                new Support_Office__c(Name = 'Lund', Regional_Support_Manager__c = UserInfo.getUserId(),
                        Business_Hours__c = bhLundId, Enterprise_Business_Hours__c = bhLundEntId);
        insert supportOffice;
        QlikTech_Company__c qtCompany = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'United States', 'United States');
        Account acc = new Account(Name = 'new account', Global_Support_Account__c = true,
                Support_Office__c = supportOffice.Id,
                Support_Level__c = 'Basic',
                QlikTech_Company__c = qtCompany.QlikTech_Company_Name__c, Billing_Country_Code__c = qtCompany.Id);
        insert acc;
        Account_License__c al =
                new Account_License__c(Name = '11111111111', Product__c = 'QSERVER', Account__c = acc.Id,
                        INT_NetSuite_Support_Level__c = 'Basic',
                        Support_From__c = Date.today().addDays(-10), Support_To__c = Date.today().addDays(100),
                        O_S__c = 'Windows 3.1', Environment__c = 'MS Windows', Application__c = 'QVSERVER',
                        Account_License_Group__c = null);
        Account_License__c al2 =
                new Account_License__c(Name = '11111111111', Product__c = 'QSERVER', Account__c = acc.Id,
                        INT_NetSuite_Support_Level__c = 'Enterprise',
                        Support_From__c = Date.today().addDays(-10), Support_To__c = Date.today().addDays(100),
                        O_S__c = 'Windows 3.1', Environment__c = 'MS Windows', Application__c = 'QVSERVER',
                        Account_License_Group__c = null);
        List<Account_License__c> accountLicenses = new List<Account_License__c>{al, al2};
        insert accountLicenses;

        Entitlement_Settings__c settings = Entitlement_Settings__c.getInstance();
        SlaProcess signatureEntitlementProcess = [SELECT Id, Name FROM SlaProcess WHERE Name =:settings.Signature_Entitlement_Process_Name__c LIMIT 1];
        Entitlement ent = [SELECT Id, SLA_Process_Name__c, SlaProcessId FROM Entitlement WHERE Account_License__c = :al.Id];
        ent.SLA_Process_Name__c = settings.Signature_Entitlement_Process_Name__c;
        ent.SlaProcessId = signatureEntitlementProcess.Id;
        update ent;

        Entitlement ent2 = [SELECT Id FROM Entitlement WHERE Account_License__c = :al2.Id];

        Case caseTest = new Case(EntitlementId = ent.Id, Severity__c = '1', Business_Justification__c = 'test', AccountId = acc.Id);
        Case caseTest2 = new Case(EntitlementId = ent2.Id, Severity__c = '1', Business_Justification__c = 'test', AccountId = acc.Id);

        List<Case> tCase = new List<Case>{
                caseTest, caseTest2
        };
        Test.startTest();
        insert tCase;
        Test.stopTest();

        tCase = [SELECT Id, Business_Critical__c, Business_Critical_Send_Email__c, Priority FROM Case WHERE Id = :caseTest.Id OR Id = :caseTest2.Id];
        for (Case cs : tCase) {
            System.assert(cs.Business_Critical__c);
            System.assertEquals('Urgent', cs.Priority);
        }
    }

}