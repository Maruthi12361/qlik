/**************************************************
* Unit test to test OpportunityPreviousOwner trigger
*
* Change Log:
* 2012-11-14 SAN: CR# 7333 Initial Development
* 2013-07-10 SLH: Set From_Lead_Conversion__c to false in the test Opportunity to avoid OP010_Cant_Create_4_EndUserAc_PendingVal validation rule
* 07.02.2017   RVA :   changing methods   
* 2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring 
**************************************************/
@isTest
public class OpportunityPreviousOwnerTest {

   public static testMethod void TestOpportunityPreviousOwnerTrigger() {
        
               QTTestUtils.GlobalSetUp();          
        
        Semaphores.TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = true;

        Profile p = [select id from profile where name='System Administrator']; 
          User firstUser = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');
        System.RunAs(firstUser)
        {
            // Adding an account with BillingCountryCode and Qliktech Company
			/*
           Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();

            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                     Name = 'USA',
                     Country_Name__c = 'USA',
                     QlikTech_Company_Name__c = 'QlikTech Inc',      
                     Subsidiary__c = testSubs1.id 
            );
            insert QTComp;
			*/
			QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech Inc', 'USA', 'United States');
				QTComp.Country_Name__c = 'USA';
			    QTComp.CurrencyIsoCode = 'USD'; 
			update QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
            
            Account acc = new Account(Name = 'Test', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Account_Creation_Reason__c = 'No reason', Billing_Country_Code__c = QTComp.Id, OwnerId = firstUser.Id, From_Lead_Conversion__c = false);
            insert acc;
        
           
            acc.Pending_Validation__c = false;
            update (acc);
            
            User secondUser = createUser('second@dot.com', 'secondnick');
            //Create test Contact
            Contact testCon = new Contact(FirstName='Test1', LastName='Contact1', AccountId=acc.Id, OwnerId=firstUser.Id);
            insert testCon;            
              
            Opportunity testOpp = new Opportunity(
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delte me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Identified',    //StageName = 'Alignment Meeting',
                RecordTypeId = '01220000000DNwY',   //Direct / Reseller - Std Sales Process
                CurrencyIsoCode = 'GBP',
                is_services_opportunity__c = true,
                Signature_Type__c = 'Digital Signature',
                AccountId = testCon.AccountId
                );
             Test.startTest();
        insert testOpp;
                   Test.stopTest();
        // first it was null
       
        System.assertEquals(testOpp.Previous_Owner__c, null);
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        // after owner update the previous owner changed
        testOpp.OwnerId = secondUser.Id;    
        update testOpp;     
        testOpp = refreshOpp(testOpp.Id);
        System.assertEquals(testOpp.Previous_Owner__c, firstUser.Id);
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        // after no owner update the previous owner stays
        testOpp.OwnerId = secondUser.Id;
        update testOpp;
        testOpp = refreshOpp(testOpp.Id);
        System.assertEquals(testOpp.Previous_Owner__c, firstUser.Id);
		Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        // after another owner update the previous owner changed back
        testOpp.OwnerId = firstUser.Id;
        update testOpp;
        testOpp = refreshOpp(testOpp.Id);
        System.assertEquals(testOpp.Previous_Owner__c, secondUser.Id);
                        
        Semaphores.TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = false;

        }
        
 
        
    }

    private static User createUser(String username, String nick) {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Standard User']; 
        UserRole role = [SELECT Id FROM UserRole WHERE Name = 'CEO'];
        User user = new User(Username = username, LastName = 'Test Lastname', Email = username, UserRoleId = role.Id, 
                             Alias = 'tula', CommunityNickname = nick, TimeZoneSidKey = 'Europe/Paris', 
                             LocaleSidKey = 'es_ES', EmailEncodingKey = 'ISO-8859-1', ProfileId = profile.Id, LanguageLocaleKey = 'en_US'); 
        User me = [Select Id From User Where Id = :UserInfo.getUserId()];
        System.RunAs(me) {
        	insert user;
        }
        return user;
    }
            
    private static Opportunity refreshOpp (Id id) {
        return [SELECT Id, OwnerId, Previous_Owner__c FROM Opportunity WHERE Id = :id];
    }           
}