/*****************************************************************************************************************
 Change Log:

20141127	CCE Initial development for CR# 19144 - https://eu1.salesforce.com/a0CD000000muaKy
				Test class for AddCreatedDateToBug.trigger
07.02.2017   RVA :   changing methods
18.02.2019   BAD :   Updating Unoted States to USA createAndInsertAccount() d
******************************************************************************************************************/
@isTest
private class AddCreatedDateToBugTest {
	
	static final String BUGID = '12345';

	@isTest static void test_add_createddate() {
		// Create the Bug, Account, Contact and Case
		//Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
		Bugs__c b = createAndInsertBug();
	    Account a = createAndInsertAccount();
	    Contact c = createAndInsertContact(a);
	    Case c1 = createCase(a, c);
	    c1.Bug__c = b.Id;
	    insert c1;
	    
	    //insert the Qlikview Support Record - this should cause the trigger to fire and populate the Escalation Date field on the Bug
	    test.startTest();    
	    Qlikview_Support_Escalation__c testQlikviewSupportEscalation = new Qlikview_Support_Escalation__c( Case__c = c1.Id);            
        insert testQlikviewSupportEscalation;
        
	    Bugs__c bugRet = [select Id, Escalation_date__c from Bugs__c where Bug_Id__c = :BUGID];
		System.Debug('bugRet = ' + bugRet);

		//test that the Escalation Date field has been populated
	    System.assert(bugRet.Escalation_date__c != null);
	    test.stopTest();
	}

	private static Bugs__c createAndInsertBug() {
        Bugs__c testBug = new Bugs__c( Name = 'Test Bug', Symptoms__c = 'Just testing', Bug_Id__c = BUGID);            
        insert testBug;
        return testBug;
    } 
    
    private static Account createAndInsertAccount() {
        // Adding an account with BillingCountryCode and Qliktech Company
		QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'United States');
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];

        Account testAct = new Account( Name = 'Test Account', Navision_Status__c = 'Something', Billing_Country_Code__c = QTComp.Id);            
        insert testAct;
        return testAct;
    } 
    
    private static Case createCase(Account testAct, Contact ctct) {
        Case c1 = new Case( Status = 'Closed', AccountId = testAct.Id, ContactId = ctct.Id);
        return c1;
    }
    
    private static Contact createAndInsertContact(Account testAct) {
        Contact ctct = new Contact(Email = 'tstEmail@abc.com', FirstName = 'TestFN1', LastName = 'TestLN1', AccountId = testAct.Id, LeadSource='WEB - Web Activity');
        insert ctct;
        return ctct;
    }
	
		
}