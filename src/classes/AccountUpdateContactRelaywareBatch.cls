/**************************************************
* CR# 97533 – Relayware triggers
* Change Log:
* 2016-10-31 BAD Initial creation of class
**************************************************/
global class AccountUpdateContactRelaywareBatch implements Database.Batchable<sObject> {

	String query;
	List<Id> accIds{get;set;}

	public AccountUpdateContactRelaywareBatch(List<Id> accountIds) {
		this.accIds = accountIds;
		query = 'select Id, Synch_with_Relayware__c, Partner_Contact_Approval_Status__c from Contact where ActiveULC__c=true and Left_Company__c=false and accountId in :accIds';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Contact> contactsToUpdate = (List<Contact>)(scope); 
        system.debug('AccountUpdateContactRelaywareBatch: contactsToUpdate.size()' + contactsToUpdate.size());                
		for (Contact con: contactsToUpdate)
		{
			 con.Synch_with_Relayware__c = true; 
		     con.Partner_Contact_Approval_Status__c = true; 
		}
		List<Database.SaveResult> sResults = Database.update(contactsToUpdate, false);
	}
	
	global void finish(Database.BatchableContext BC) {
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Contact Email Update Batch Job Status ' + a.Status);
        mail.setPlainTextBody('The Contact batch job has finished.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });	
		
	}

}