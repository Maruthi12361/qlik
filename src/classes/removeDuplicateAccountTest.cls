/*      * File removeDuplicateAccountTest
        * Description : Test class for removeDuplicateAccountTes
        * @author : Anurag GUpta
        * Date    : 24th Nov 2017
        * User story  :   QCW-4251    
*/

@isTest
public class removeDuplicateAccountTest{
                        
    public static TestMethod void TEST_removeDuplicateAccount(){
      Test.StartTest();  
         Account a = new Account();
         a.name = 'Account1';
         insert a;
       
         entitlement e = new entitlement();
         e.Name='Entitlement1';
         e.AccountID= a.Id;
         insert e;
         
         entitlement f= new entitlement();
         f.name = 'Entitlement1';
         f.AccountId = a.Id;
         insert f;
         
         Contact ct  = new contact();
         ct.LastName = 'LastName';
         ct.AccountId = a.Id;
         
         Case c = new Case();
         c.Area__c  = 'I have an installation issue';
         c.Issue__c = 'I want to report a bug';
         c.Subject = 'Test Case';
         c.Description = 'Case is Raised';
         c.Severity__c  = '2';
         c.Status = 'New';
         c.Type = 'Incident';
         c.Origin = 'Email';
         insert c;
         
         c.entitlementId  = e.Id;
         update c;
         
         Account_license__c acl1 = new Account_license__c();
         acl1.Name = 'Entitlement1';
         insert acl1;
         
         Account_license__c acl2 = new Account_license__c();
         acl2.Name = 'Entitlement1';
         insert acl2;
         
         removeDuplicateAccount r = new removeDuplicateAccount();
         r.execute(null);

  /*       
       //  integer i =10;
         SchedulableContext ctx;
         removeDuplicateAccount obj = new removeDuplicateAccount();
         String sch = '0 0 23 * * ?';
         system.schedule('Test check', sch, obj);
       //  obj.execute(null);
         test.stoptest();
     //    removeDuplicateAccount rd = new removeDuplicateAccount(i);
         
    //     removeDuplicateAccountLicense rdl =new removeDuplicateAccountLicense(); */
       } 
    }