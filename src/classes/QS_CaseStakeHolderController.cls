/************************************************************************
 * @author  SAN
 * 2014-11-19 customer portal project
 *  
 * Controller Class for Case Stake Holder to be used in main Force.com.
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 *
 *************************************************************************/

public with sharing class QS_CaseStakeHolderController {
    // current selected case
    public Case myCase { get; set; }
    
    // all stakeholders of myCase
    public List<user> caseStakeholders { get; set; }

    // stakeholder to be added
    public EntitySubscription stakeholderToBe { get; set; }

    // Id of selected stakeholder
    public String cshIdChosen { get; set; }
    
    public QS_CaseStakeHolderController() {

        Id caseId = (Id)ApexPages.CurrentPage().getparameters().get('id');
        
        if (caseId != null)
        {
            myCase = [SELECT id, CaseNumber, CreatedDate, AccountId, Subject, Description, Status, EnvironmentOfCase__r.name, EnvironmentOfCase__c, OwnerId, Owner.FirstName, Owner.LastName FROM Case 
                   WHERE Id = :caseId];
        
            stakeholderToBe = new EntitySubscription(ParentId=myCase.Id);
        }
        caseStakeholders = myCase == null ? new List<User>() : getCaseStakeholders();
    
    }
    
    private List<User> getCaseStakeholders() {
        Set<Id> holderIds = new Set<Id>();
        for (EntitySubscription es : [select SubscriberId from EntitySubscription where ParentId=:myCase.Id LIMIT 1000]) {
            System.debug('QS_CaseController stakeholderid: ' + es.SubscriberId);
            holderIds.Add(es.SubscriberId);
        }
        return [select id, Name, Email from User where Id in :holderIds];
    }    
    
    public PageReference addStakeholder()
    {
        try
        {
            stakeholderToBe.ParentId = myCase.Id;
            if(Network.getNetworkId() == null) {
                User currentUser = [Select Id, Name, ContactId, AccountId, (Select MemberId, NetworkId From NetworkMemberUsers where Network.Status = 'Live' LIMIT 1) From User where Id = :UserInfo.getUserId() LIMIT 1];
                stakeholderToBe.NetworkId = (currentUser != null && currentUser.NetworkMemberUsers != null  
                                                && (!currentUser.NetworkMemberUsers.isEmpty()) && currentUser.NetworkMemberUsers[0].NetworkId != null )
                                                ? currentUser.NetworkMemberUsers[0].NetworkId : Network.getNetworkId();
            } else {
                stakeholderToBe.NetworkId = Network.getNetworkId();
            }
            if (QS_CaseHelper.addStakeholder(stakeholderToBe)) {
                caseStakeholders = myCase == null ? new List<User>() : getCaseStakeholders();
                stakeholderToBe = new EntitySubscription(ParentId = myCase.Id);
            }
        }
        catch (Exception ex) {
            System.debug(ex.getMessage());
        }
        return null;
    }   

    public PageReference delStakeholder()
    {
        try 
        {
            if (QS_CaseHelper.deleteStakeholder(myCase.Id, cshIdChosen)) {
                caseStakeholders = myCase == null ? new List<User>() : getCaseStakeholders();
            }
        }
        catch (Exception ex)
        {
            System.debug(ex.getMessage());
        }   
        return null;
    }    
}