/**
* Class: Test class for QS_Qoncierge_WebForm_Cntrl. 
* 
* Changelog:
*    2017-01-17 : Ramakrishna Kini: Change case origin value from Qconierge WebForm to Support Portal Guest. CR# 101517
* 2017-10-04 BAD : CHG0030362 - Rename Qoncierge to Customer Support
* 2017-01-17 ext_bad : LCE-48    - Add button to create Case and Contact during live chat
* 2018-04-23 IT-573 Fix Country and Description error messages - ext_bad
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*/
public without sharing class QS_Qoncierge_WebForm_Cntrl {

    private static Id contactRecordTypeId;
    private static Id ownerContactAttachment;

    private Case caseObj { get; set; }
    public Case tempCaseObj { get; set; }
    public Contact contactTempObj { get; set; }
    public Contact newContactChat { get; set; }
    public QS_CaseWizard_Controller caseWizardRef { get; set; }
    public String contactCompany { get; set; }
    public String contactAccountUsername { get; set; }
    public Boolean isFailure { get; set; }
    public Id contactChatRecordTypeId { get; set; }
    public String prodLicenseId { get; set; }

    private final static String CONTACT_RECORD_TYPE = 'Business Contact';
    private final static String CONTACT_OWNER = 'Qliktech Webservice';

    public QS_Qoncierge_Country_License_Support__c qonciergeCountrySupport { get; set; }

    public QS_Qoncierge_WebForm_Cntrl() {
        if (caseObj == null) {
            caseObj = new Case();
        }
        if (tempCaseObj == null) {
            tempCaseObj = new Case();
        }
        if (contactTempObj == null) {
            contactTempObj = new Contact();
        }
        ownerContactAttachment = null;
        isFailure = false;
        populateCasewizardParams();
    }

    private void populateCasewizardParams() {

        if (caseWizardRef != null) {
            caseObj = caseWizardRef.caseObj;
        }
    }

    public PageReference populateCountrySupport() {
        if (contactTempObj != null && contactTempObj.Contact_Country__c != null) {
            qonciergeCountrySupport = QS_Qoncierge_Country_License_Support__c.getInstance(contactTempObj.Contact_Country__c);
        } else {
            qonciergeCountrySupport = null;
        }
        return null;
    }

    public void populateContactFields() {
        newContactChat = retrieveContact(false);
    }

    public boolean getApexMessages() {
        return ApexPages.hasMessages();
    }

    public PageReference submitDetails() {
        if (String.isBlank(contactCompany)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Company: You must enter a value'));

            return null;
        }
        try {
            populateCasewizardParams();

            if (caseWizardRef != null && caseWizardRef.caseObj != null && contactTempObj.Email != null && contactTempObj.Email != '') {
                /*List<Contact> contacts = [
                        SELECT Id, Name, Email, MailingCountry, AccountId, Email_Has_Bounced__c
                        FROM Contact
                        WHERE Email = :contactTempObj.Email AND Left_Company__c = FALSE
                ];
                Boolean con = contacts != null && contacts.size() == 1;*/

                Contact contactObj = retrieveContact(true);
                if (contactObj != null && contactObj.Id != null && contactObj.AccountId != null) {

                    isFailure = false;

                    if (contactobj.Email_Has_Bounced__c != null && contactobj.Email_Has_Bounced__c == 1) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'The email provided has bounced and is no longer valid, please provide a different email address. If you think this is incorrect, please specify the email to be validated in the details above.'));
                        return null;
                    }

                    //Set the case owner to the queue assigned from categories
                    caseWizardRef.nextCaseDetail();
                    populateCasewizardParams();

                    if (caseWizardRef.caseObj.Description == null || caseWizardRef.caseObj.Description == '') {
                        addErrorMessage();
                        return null;
                    }

                    createCase(true, contactObj.MailingCountry, contactObj.AccountId, contactObj.Id);
                    caseWizardRef.finishGuestCase = true;

                    try {
                        sendEmail();
                    } catch (Exception ex) {
                        isFailure = true;
                        caseWizardRef.isQonciergeFailure = true;
                        caseWizardRef.showException = true;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, ex.getMessage()));
                        return null;
                    }
                }
            }
        } catch (Exception ex) {
            isFailure = true;
            caseWizardRef.isQonciergeFailure = true;
            caseWizardRef.showException = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Something went wrong.. Please try again!! '));
            return null;
        }
        return null;
    }

    private void createCase(boolean con, String mailingCountry, String accountId, String contactId) {
        String licenseNumber = populateLicenseNumber(con, mailingCountry);
        populateProdLicenseId(licenseNumber);

        if (tempCaseObj.License_No__c != null) {
            licenseNumber = tempCaseObj.License_No__c;
        }

        Case newCase = insertCase(accountId, contactId, licenseNumber);
        createAttachment(con, newCase.Id);

        caseObj = [SELECT Id, CaseNumber, Subject, Status, Description, ContactId FROM Case WHERE Id = :newCase.Id LIMIT 1];
        caseWizardRef.caseObj = caseObj;
        tempCaseObj = caseObj;
    }

    private void populateProdLicenseId(String licenseNumber) {
        if (licenseNumber != null && licenseNumber.contains('No license –')) {
            system.debug('Searching for Entitlement with name: ' + licenseNumber);
            List<Entitlement> prodLicenses = [SELECT id FROM Entitlement WHERE Name LIKE :licenseNumber LIMIT 1];
            system.debug('prodLicenses.Size(): ' + prodLicenses.size());
            if (prodLicenses.size() > 0) {
                prodLicenseId = prodLicenses[0].Id;
            }
        }
    }

    private String populateLicenseNumber(boolean con, String mailingCountry) {
        if (con) {
            qonciergeCountrySupport = QS_Qoncierge_Country_License_Support__c.getInstance(mailingCountry);
        } else {
            populateCountrySupport();
        }

        if (qonciergeCountrySupport != null && qonciergeCountrySupport.Temp_Account__c != null) {
            List<Account> tempAcc = [
                    SELECT Id, Support_Office__r.Name
                    FROM Account
                    WHERE Id = :qonciergeCountrySupport.Temp_Account__c
            ];
            if (tempAcc != null && tempAcc.size() > 0) {
                return ('No license – ' + tempAcc[0].Support_Office__r.Name);
            }
        }
        return tempCaseObj.License_No__c;
    }

    private void addErrorMessage() {
        String errorMessage = '* ' + System.Label.QS_Description_Mandatory;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, errorMessage));
        caseWizardRef.showFinalStep = true;
        caseWizardRef.showErrorMessageRequiredFields = false;
    }

    private Case insertCase(String accountId, String contactId, String licenseNumber) {
        Case newCase = new Case(
                RecordTypeId = caseObj.RecordTypeId, AccountId = accountId, ContactId = contactId,
                Severity__c = caseObj.Severity__c, Type = caseObj.Type, Subject = caseObj.Subject,
                Status = 'New', Origin = 'Support Portal Guest', License_No__c = licenseNumber,
                //Add field in category matrix to set this value according to area/issue
                Description = caseObj.Description, Area__c = caseObj.Area__c, Issue__c = caseObj.Issue__c,
                Entitlementid = prodLicenseId, Case_Submitted__c = System.now());

        // Write the case to the database and get case number back
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId = '01Q20000000Hv27EAC';
        dmlOpts.EmailHeader.triggerUserEmail = true;
        dmlOpts.EmailHeader.triggerOtherEmail = true;
        dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
        //database.insert(newCase, dmlOpts);
        Database.SaveResult sr = database.insert(newCase, dmlOpts);

        // Iterate through each returned result
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully inserted Case. Case ID: ' + sr.getId());
        } else {
            // Operation failed, so get all errors
            for (Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Case fields that affected this error: ' + err.getFields());
            }
        }
        return newCase;
    }

    private void createAttachment(boolean con, String newCaseId) {
        if (con) {

            con = false;

            String txt = '';
            txt += 'First Name - \n';
            txt += +contactTempObj.FirstName + '  \r\n';
            txt += 'Last Name - \n';
            txt += +contactTempObj.LastName + '  \r\n';
            txt += 'Company - \n';
            txt += +contactCompany + '  \r\n';
            txt += 'Country - \n';
            txt += +contactTempObj.Contact_Country__c + '  \r\n';
            txt += 'Email - \n';
            txt += +contactTempObj.Email + '  \r\n';
            txt += 'Phone - \n';
            txt += +contactTempObj.Phone + '  \r\n';
            txt += 'Licence Number - \n';
            txt += +tempCaseObj.License_No__c + '  \r\n';


            Blob txtBlob = Blob.valueOf(txt); //Convert it to a blob

            Attachment attach = new Attachment(); //Make an attachment
            attach.Name = 'text.txt';
            attach.Body = txtBlob;
            attach.ContentType = 'application/txt'; //Signal what the file's MIME type is
            attach.ParentID = newCaseId;
            insert attach;
        }
    }

    private void sendEmail() {

        EmailTemplate et = [
                SELECT id
                FROM EmailTemplate
                WHERE DeveloperName = :System.Label.QS_Qoncierge_Case_Email_Template
        ];  // Qoncierge_Support_Request_Confirmation
        // ###
        // ### Get org-wide email address for Qoncierge support from custom settings
        // ###
        String theSender = 'NOT_ENABLED';
        Qoncierge_Support_Email__c qSender = Qoncierge_Support_Email__c.getInstance('Valid');

        if (qSender != null) {
            theSender = qSender.email__c;
        } else {
            throw new qonciergeWebformException('The Sender email was not set correctly. Please contact customersupport@qlik.com');
        }

        List<OrgWideEmailAddress> owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :theSender LIMIT 1];

        // Create outbound email with Case Id and Contact
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        // Prepare the email
        mail.setTargetObjectId(caseObj.ContactId);
        mail.setWhatId(caseObj.Id);
        mail.setSaveAsActivity(false);
        mail.setReplyTo(System.Label.QS_Qoncierge_Reply_Email_Address); //'customersupport@qlikview.com'
        // mail.setSubject('QlikView Support Request - Confirmation');  // WHEN NOT USING ORG-WIDE sender email
        mail.setUseSignature(false);
        mail.setTemplateId(et.id);

        if (owea != null && !owea.isEmpty() && owea.size() > 0) {
            mail.setOrgWideEmailAddressId(owea[0].Id);
        } else {
            mail.setSenderDisplayName(System.Label.QS_Qoncierge_Sender_Name); // Customer Support
        }

        // Send the email as user 'Qliktech Webservice'
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                mail
        });


    }

    private Contact retrieveContact(boolean saveNewRecord) {

        // Get contact having Work E-mail as email address if any
        List<Contact> contacts = [
                SELECT Id, Name, MailingCountry, Email, AccountId, Email_Has_Bounced__c
                FROM Contact
                WHERE Email = :contactTempObj.Email AND Left_Company__c = FALSE
        ];

        if (contacts == null || contacts.isEmpty() || contacts.size() == 0
                || (contacts != null && !contacts.isEmpty() && contacts.size() > 1)) {
            // Get user id to set as owner for the contact
            Id contactOwnerId = getOwner();

            Contact newCon = createContact(contactOwnerId);
            if (saveNewRecord) {
                // Save new contact to data base
                insert newCon;
            }
            return newCon;

        } else if (contacts != null && !contacts.isEmpty() && contacts.size() > 0 && contacts.size() == 1) {
            return contacts[0];
        }

        return null;

    }

    private Contact createContact(Id contactOwnerId) {
        populateCountrySupport();
//        String accountSearchText = ((qonciergeCountrySupport != null && qonciergeCountrySupport.Support_office__c != null)
//                ? 'Temp - Support Office ' + qonciergeCountrySupport.Support_office__c : 'Temp Qoncierge');
//        system.debug('accountSearchText: ' + accountSearchText);


        //List<Account> tempAcc = [SELECT Id FROM Account WHERE Name LIKE :accountSearchText LIMIT 1];

        // Get record type Id
        contactChatRecordTypeId = getContactRecordTypeId(CONTACT_RECORD_TYPE);


        // Create new contact with values from email
        Contact newCon = new Contact(
                RecordTypeId = contactChatRecordTypeId,
                OwnerId = contactOwnerId,
                AccountId = qonciergeCountrySupport.Temp_Account__c, //tempAcc[0].Id,
                FirstName = contactTempObj.FirstName,
                LastName = contactTempObj.LastName,
                Email = contactTempObj.Email,
                Phone = contactTempObj.Phone,
                MailingCountry = contactTempObj.Contact_Country__c,
                LeadSource = 'SUP - Support Request',
                Description =
                        'Customer Support Web Form Contact\r\n' +
                                'Company: ' + ((contactCompany == null) ? '' : contactCompany) + '\r\n' +
                                'Country: ' + ((contactTempObj.Contact_Country__c == null) ? '' : contactTempObj.Contact_Country__c) + '\r\n' +
                                'Licence Number: ' + ((tempCaseObj.License_No__c == null) ? '' : tempCaseObj.License_No__c) + '\r\n' +
                                'Qlikview.com user name: ' + ((contactAccountUsername == null) ? '' : contactAccountUsername)
        );


        return newCon;
    }

    private static Id getContactRecordTypeId(String recTypeName) {
        if (contactRecordTypeId != null) {
            return contactRecordTypeId;
        } else {
            contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recTypeName).getRecordTypeId();
            return contactRecordTypeId;
        }
    }


    private static Id getOwner() {
        if (ownerContactAttachment != null) {
            return ownerContactAttachment;
        } else {
            ownerContactAttachment = [SELECT Id, Name FROM User WHERE Name = :CONTACT_OWNER LIMIT 1][0].Id;
            return ownerContactAttachment;
        }
    }

    public class qonciergeWebformException extends Exception {
    }
}