/*
----------------------------------------------------------------------------
|  Class: BugWeightSys_Manager
|
|  Filename: BugWeightSys_Manager.cls
|
|  Author: Peter Friberg, Fluido Sweden AB
|
|  Description:
|    Class managing the Bug External Weighting System calculations
|
| Change Log:
| 2013-10-23  PetFri  Initial Development
| 2013-10-28  PetFri  Updates each Bug with number of cases
| 2013-10-29  PetFri  Uses active Entitlements to find Account License.
|                     Number of days calculation rewritten, now uses Long.
| 2013-11-07  PetFri  Uses active Entitlement/SLA Process to find SLA level
| 2015-11-11  NAD     Updated support level names per CR# 57556
| 2018-01-24  ext_vos CHG0030671: move the "set the Number_of_cases__c field" logic to CaseTrigger.
----------------------------------------------------------------------------
*/

public class BugWeightSys_Manager {

    // These constants define the matrix of values and factors
    // and are subject to change whenever needed.

    public final Map<String,Integer> severity_Weight_Value = 
        new Map<String,Integer> {
            '1' => 15, 
            '2' => 7, 
            '3' => 4
        };

    public final Integer severity_Weight_Factor = 20;

    public final Map<String,Integer> priority_Weight_Value =
        new Map <String,Integer> {
            'Critical'     => 10, 
            'Urgent'       => 10,
            'High'         => 5,
            'Medium'       => 2,
            'Medium / Low' => 2,
            'Low'          => 2
        };

    public final Integer priority_Weight_Factor = 15;

    public final Map<String,Double> cals_Weight_Factor = 
        new Map<String,Double> {
            'Session'      => 0.173,
            'Named'        => 0.016,
            'Doc'          => 0.004,
            'Usage'        => 0.001,
            'Uncapped/IAS' => 0.806
        };

    public final Double cals_Uncapped_Weight_Value = 10.0;

    public final Integer dse_Weigth_Value = 7;

    public final Integer dse_Weigth_Factor = 4;

    public final Integer noCust_Weight_Threshold = 3;

    public final Integer noCust_Weight_Factor = 3;

    public final Map<Integer,List<Long>> timeInQueue_Range =
        new Map<Integer,List<Long>> {
             0 => new List<Long> {  0L,        30L },
             5 => new List<Long> { 30L,        60L },
             7 => new List<Long> { 60L,        90L },
            10 => new List<Long> { 90L, 999999999L }
        };

    public final Integer timeInQueue_Weight_Factor = 2;

    public final Map<String,Integer> supportLevel_Weight_Value =
        new Map<String,Integer> {
            'Enterprise Support SLA'  => 10,
            'Basic Support SLA'       => 0,
            'No SLA'                  => 0
        };

    public final Integer supportLevel_Weight_Factor = 1;

    public final map<String,List<Integer>> serviceClassRange =
        new Map<String,List<Integer>> {
            'Low'  => new List<Integer> {   0,       150 },
            'Med'  => new List<Integer> { 150,       426 },
            'High' => new List<Integer> { 426, 999999999 }
        };

    public final map<String,String> serviceClassValue =
        new Map<String,String> {
            'Low'  => '3',
            'Med'  => '2',
            'High' => '1'
        };

    /*
    |----------------------------------------------------------
    |  Constructor: BugWeightSys_Manager
    |
    |  Description:
    |      Empty constructor
    |
    |  Input parameters:
    |      None
    |----------------------------------------------------------
    */
    
    public BugWeightSys_Manager() {
        
        // Empty constructor
    }

    /*
    |----------------------------------------------------------
    |  Method: retrieveBugs
    |
    |  Description:
    |      Read all non-closed Bugs from database and return
    |      the list of Bugs. Cases related to the Bugs are also
    |      read from database and will be available through the
    |      Cases__r attribute on each Bug record.
    |
    |  Input parameters:
    |      None
    |
    |  Returns:
    |      List<Bugs__c>  - List of non-closed Bugs
    |----------------------------------------------------------
    */

    public List<Bugs__c> retrieveBugs() {

        // Get list of all Bugs that are NOT Closed
        List<Bugs__c> bugs = new List<Bugs__c>([
            SELECT id,
                   name,
                   severity__c,
                   status__c,
                   bug_weight__c,
                   number_of_cases__c,
                   createddate,
                (SELECT id,
                        priority,
                        severity__c,
                        casenumber,
                        designated_support_engineer__c,
                        entitlementid,
                        entitlement.slaprocess.name,
                        entitlement.account_license__c,
                        entitlement.account_license__r.name,
                        entitlement.status,
                        entitlement.session_cal__c,
                        entitlement.named_user_cal__c,
                        entitlement.document_cal__c,
                        entitlement.usage_cal__c,
                        entitlement.uncapped__c
                 FROM Cases__r)
            FROM Bugs__c WHERE NOT(status__c LIKE 'Close%')]);

        return bugs;
    }

    /*
    |----------------------------------------------------------
    |  Method: calcTimeInQueue
    |
    |  Description:
    |      Calculates the correct weight from
    |      the specified ranges of ages.
    |
    |  Input parameters:
    |      ageInDays : Integer - The age in days of the Bug
    |
    |  Returns:
    |      Integer - The calculated weight for time in queue
    |----------------------------------------------------------
    */

    public Integer calcTimeInQueue(Long ageInDays) {

        Integer value = 0;

        // Try all ranges
        for (Integer point : timeInQueue_Range.keySet()) {

            // Correct range found, break loop
            if (ageInDays >= timeInQueue_Range.get(point)[0] &&
                ageInDays <  timeInQueue_Range.get(point)[1]) {

                // Return the correct weight
                value = point;
                break;
            }
        }

        // Return the correct value
        return value;
    }

    /*
    |----------------------------------------------------------
    |  Method: calcBugWeight
    |
    |  Description:
    |      Calculates the resulting Bug weight depending on
    |      all parameters specified in the value/factor matrix.
    |      Parameters are read from Case, Account,
    |      Account License and Bug itself.
    |
    |  Input parameters:
    |      bug : Bugs__c - The bug to calculate the weight for
    |
    |  Returns:
    |      Integer - The correct bug weight calculated from
    |                all parameters.
    |----------------------------------------------------------
    */

    public Integer calcBugWeight(Bugs__c bug) {

        Integer totalWeight = 0;
        Integer highestSeverity = 0;
        Integer highestSupportLevel = 0;
        Integer highestPriority = 0;
        Integer timeInQueue = 0;
        Integer dse = 0;
        Integer cals = 0;
        Integer noCust = 0;
        Map<Id,Account_License__c> uniqueAccLics = new Map<Id,Account_License__c>();

        system.debug('**** BUG=' + bug.name);

        // Investigate on related cases
        for (Case c :bug.cases__r) {

            Integer severity = 0;
            Integer priority = 0;
            Integer supportLevel = 0;

            system.debug('----- CASE=' + c.caseNumber);

            // Find highest Case severity
            if (c.severity__c != null) {
                severity = severity_Weight_Value.get(c.severity__c);
                if (severity > highestSeverity)
                    highestSeverity = severity;
            }

            // Find highest support level (SLA)
            if (c.entitlement.status == 'Active' &&
                c.entitlement.slaprocess.name != null) {
                supportLevel = supportLevel_Weight_Value.get(c.entitlement.slaprocess.name);
                if (supportLevel > highestSupportLevel)
                    highestSupportLevel = supportLevel;
            }

            // Find highest Case priority
            if (c.priority != null) {
                priority = priority_Weight_Value.get(c.priority);
                if (priority > highestPriority)
                    highestPriority = priority;
            }

            // Investigate if designated support engineer is specified
            if (c.designated_support_engineer__c != null)
                dse += dse_Weigth_Value;

            system.debug('SEVERITY='      + severity);
            system.debug('SUPPORT_LEVEL=' + supportLevel);
            system.debug('PRIORITY='      + priority);

            // CALs are calculated only if case has:
            // severity 1 or 2 AND priority Critical, Urgent or High
            if (severity >= severity_Weight_Value.get('2') &&
                priority >= priority_Weight_Value.get('High')) {

                system.debug('CASE HAS SEV >= 2 && PRIO >= HIGH');

                // Create new account licenses in a map to make them unique
                // if there is an active entitlement with an account license 
                if (c.entitlementid != null && 
                    c.entitlement.status == 'Active' &&
                    c.entitlement.account_license__c != null) {

                    system.debug('ACTIVE ACCOUNT LICENSE FOUND=' + c.entitlement.account_license__r.name);

                    // Only save (create) unique account licenses
                    if (!uniqueAccLics.containsKey(c.entitlement.account_license__c)) {

                        system.debug('ACC LIC IS UNIQUE');

                        system.debug('session_cal__c='    + c.entitlement.session_cal__c);
                        system.debug('named_user_cal__c=' + c.entitlement.named_user_cal__c);
                        system.debug('document_cal__c='   + c.entitlement.document_cal__c);
                        system.debug('usage_cal__c='      + c.entitlement.usage_cal__c);
                        system.debug('uncapped__c='       + c.entitlement.uncapped__c);

                        // Create new account license object from case
                        uniqueAccLics.put(
                            c.entitlement.account_license__c,
                            new Account_License__c(
                                name              = c.entitlement.account_license__r.name,
                                session_cal__c    = c.entitlement.session_cal__c,
                                named_user_cal__c = c.entitlement.named_user_cal__c,
                                document_cal__c   = c.entitlement.document_cal__c,
                                usage_cal__c      = c.entitlement.usage_cal__c,
                                uncapped__c       = (c.entitlement.uncapped__c == 'True') ? true : false));
                    }
                }
            }
        }

        system.debug('HIGHEST SEVERITY='      + highestSeverity);
        system.debug('HIGHEST SUPPORT_LEVEL=' + highestSupportLevel);
        system.debug('HIGHEST PRIORITY='      + highestPriority);

        // Investigate CALs if there are any Account Licenses
        if (uniqueAccLics.size() > 0) {

            Double calsFractional = 0.0;

            system.debug('Calculating cals parts for ' + uniqueAccLics.size() + ' account licenses');

            // Calulate weight from all CAL parts
            for (Account_License__c al : uniqueAccLics.values()) {
                
                system.debug('Calculate for AccLic= ' + al.name);

                // Take care nulls. If null use 0 else use value. 
                Double session  = (al.session_cal__c    != null ? al.session_cal__c : 0);
                Double named    = (al.named_user_cal__c != null ? al.named_user_cal__c : 0);
                Double document = (al.document_cal__c   != null ? al.document_cal__c : 0);
                Double usage    = (al.usage_cal__c      != null ? al.usage_cal__c : 0);
                Double uncapped = (al.uncapped__c       == true ? cals_Uncapped_Weight_Value : 0);

                // Calculate total cals value
                calsFractional +=
                    session  * cals_Weight_Factor.get('Session') +
                    named    * cals_Weight_Factor.get('Named') +
                    document * cals_Weight_Factor.get('Doc') +
                    usage    * cals_Weight_Factor.get('Usage') +
                    uncapped * cals_Weight_Factor.get('Uncapped/IAS');

                system.debug('+ session='  + session  * cals_Weight_Factor.get('Session'));
                system.debug('+ named='    + named    * cals_Weight_Factor.get('Named'));
                system.debug('+ document=' + document * cals_Weight_Factor.get('Doc'));
                system.debug('+ usage='    + usage    * cals_Weight_Factor.get('Usage'));
                system.debug('+ uncapped=' + uncapped * cals_Weight_Factor.get('Uncapped/IAS'));

                system.debug('CALS_FRACTIONAL=' + calsFractional);
            }

            // Round to next higher integer
            cals = Math.round(calsFractional + 0.5);

            system.debug('CALS=' + cals);
        }

        system.debug('#CUSTOMERS=' + uniqueAccLics.size());

        // Investigate number of customers
        if (uniqueAccLics.size() >= noCust_Weight_Threshold) {
            noCust = uniqueAccLics.size();
        }

        // Time in queue is calculated only if bug has cases with:
        // severity 1 or 2 AND priority Critical, Urgent or High
        if (highestSeverity >= severity_Weight_Value.get('2') &&
            highestPriority >= priority_Weight_Value.get('High')) {

            // Calculate time in queue part. Uses unix date diff in milliseconds
            // days = 1000 (milliseconds) * 60 (seconds) * 60 (minutes) * 24 (hours)

            Long start     = bug.createddate.getTime();
            Long stop      = DateTime.now().getTime();
            Long ageInMs   = stop - start;
            Long ageInDays = ageInMs / (1000L * 60L * 60L * 24L);

            system.debug('AGE IN DAYS=' + ageInDays);

            // Calculate weight from age in days
            timeInQueue = calcTimeInQueue(ageInDays);

            system.debug('TIME IN QUEUE=' + timeInQueue);
        }

        // Calculate the sum of all parts into a total resulting weight
        totalWeight =
            highestSeverity     * severity_Weight_Factor +
            highestPriority     * priority_Weight_Factor +
            highestSupportLevel * supportLevel_Weight_Factor +
            timeInQueue         * timeInQueue_Weight_Factor +
            noCust              * noCust_Weight_Factor +
            dse                 * dse_Weigth_Factor +
            cals;               // Factor already included in this value

        system.debug('TOTAL WEIGHT=' + totalWeight);

        // Return resulting weight
        return totalWeight;
    }

    /*
    |----------------------------------------------------------
    |  Method: calcBugServiceClass
    |
    |  Description:
    |      Calculates the correct service class string
    |      from the specified ranges of values.
    |
    |  Input parameters:
    |      weight : Integer - The weight of the Bug
    |
    |  Returns:
    |      String - The calculated service class
    |----------------------------------------------------------
    */

    public String calcBugServiceClass(Integer weight) {

        String value = '';

        // Try all ranges
        for (String sc : serviceClassRange.keySet()) {

            // Correct range found, break loop
            if (weight >= serviceClassRange.get(sc)[0] &&
                weight <  serviceClassRange.get(sc)[1]) {

                // Return the correct weight
                value = serviceClassValue.get(sc);
                break;
            }
        }

        // Return the correct value
        return value;
    }

    /*
    |----------------------------------------------------------
    |  Method: processBugs
    |
    |  Description:
    |      Process the bugs specified by parameter bugs.
    |      The weight for each bug will be calculated and
    |      updated in each Bug record.
    |
    |  Input parameters:
    |      bugs : List<Bugs__c>  -  List of bugs to process
    |
    |  Returns:
    |      Processed Bugs will be available through the
    |      parameter bugs.
    |----------------------------------------------------------
    */

    public void processBugs(List<Bugs__c> bugs) {

        // Variables for all related account ids and all related case records
        Set<Id> accIds = new Set<Id>();

        // Get all related account ids and all related case records
        for (Bugs__c bug :bugs) {

            // There is at least one case attached 
            if (bug.cases__r != null && bug.cases__r.size() > 0) {

                // Calculate the weight, service class and
                // number of cases for the bug
                Integer bugWeight      = calcBugWeight(bug);
                bug.bug_weight__c      = bugWeight;
                bug.service_class__c   = calcBugServiceClass(bugWeight);
            }
            else {
                // There are no attached cases, 
                // clear weight and sevice classon the Bug
                bug.bug_weight__c = null;
                bug.service_class__c = null;
            }
        }
    }

    /*
    |----------------------------------------------------------
    |  Method: execute
    |
    |  Description:
    |      Retrieves all bugs and calculates the weight for
    |      each one. Database is updated with weights for
    |      each Bug record.
    |
    |  Input parameters:
    |      None
    |
    |  Returns:
    |      List<Bugs__c> - List of calculated Bugs
    |----------------------------------------------------------
    */

    /*
    public List<Bugs__c> execute() {

        // Retrieve all bugs from database
        List<Bugs__c> bugs = retrieveBugs();

        // Process all bugs to update weights
        if (bugs.size() > 0 ) {
            processBugs(bugs);
        }
        
        return bugs;
    }
    */
}