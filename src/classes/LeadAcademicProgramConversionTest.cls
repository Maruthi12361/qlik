/**
    08.02.2013 Fluido CR#6184  Testclass for LeadAcademicProgramConversion.trigger
    2013-07-10 CCE CR# 6546 https://eu1.salesforce.com/a0CD000000NL228 Find Way to
                   Eliminate Qualified - B from Lead & Contact Status picklist values
    2014-11-10 Fluido Added assert on Academic Contact Name & Academic Contact Id (CR-13357)
               + Adjusted the test so that Oppty is created in conversion process
    2015-10-22 IRN removed seeAllData and added globalSetUp since the winter 16 release
    06.02.2017  RVA :   changing CreateAcounts methods
    08.01.2018  Pramod Kumar V  Test class coverage
    02.05.2018 Changed seeAllData => false | Added testAccountAddressCreation method | Added test setup
    2019-04-05  extbad IT-1692 add testLeadConvert. add SeeAllData=true because lead convert with
                                new object creation throws an error. it is because Validation rules
                                does not work with hierarchy custom settings during this process.
                                Removed Setup because SeeAllData=true tests can't contain setup methods.
 */
@IsTest(SeeAllData=true) //do not remove. SeeAllData=false will cause errors. more details in the comments above
public class LeadAcademicProgramConversionTest {

    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';

    private static Id academicLeadRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Academic_Program_Lead' LIMIT 1].Id;
    private static Id academicAccountRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Academic_Program_Account' LIMIT 1].Id;
    private static Id academicOppoRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Academic_Program_Opportunity' LIMIT 1].Id;

    static testMethod void testLeadConvert() {
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
                Country_Name__c = 'USA',
                Subsidiary__c = sub.id
        );
        insert QTComp;
        String qlikId = QTComp.Id;

        Long dt = System.now().getTime();
        Lead lead = new Lead(
                FirstName = 'First name test' + dt, LastName = 'test' + dt, Company = 'Test company' + dt,
                Opp_Reg_Rejected__c = false, Email = 'test' + dt + '@testtesttest.com',
                RecordTypeId = academicLeadRT, Signed_License_Agreement_Received__c = true,
                Country_Code__c = qlikId, Estimated_Close_Date__c = date.today().addDays(1));
        insert lead;

        Semaphores.LeadTriggerHandlerAfterUpdate = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadAcademicProgramConversion_HasRunAfter = false;

        Test.startTest();
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(lead.Id);
        lc.setDoNotCreateOpportunity(false);
        lc.setConvertedStatus('Lead - Converted');

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());

        Test.stopTest();

        lead = [
                SELECT ConvertedAccountId, ConvertedOpportunityId, ConvertedContactId, RecordTypeId
                FROM Lead
                WHERE Id = :lead.Id
        ];

        Account a = [SELECT RecordTypeId FROM Account WHERE Id = :lead.ConvertedAccountId];
        System.assertEquals(academicAccountRT, a.RecordTypeId);

        Opportunity dbOpp = [
                SELECT Academic_Contact_Name__c, RecordTypeId, Revenue_Type__c, StageName, Type
                FROM Opportunity
                WHERE Id = :lead.ConvertedOpportunityId
        ];

        System.assertEquals(academicOppoRT, dbOpp.RecordTypeId);
        System.assertEquals('Academic Program', dbOpp.Revenue_Type__c);
        System.assertEquals('AP - Open', dbOpp.StageName);
        System.assertEquals('New Customer', dbOpp.Type);

        System.assertEquals(
                lead.ConvertedContactId, dbOpp.Academic_Contact_Name__c,
                'Academic contact name should match the related contact!'
        );
    }

    static testMethod void testAccountAddressCreation() {
        System.debug('debug_UserInfo.getProfileId() ' + UserInfo.getProfileId());
        List<Address__c> adderessesBefore = new List<Address__c>([
                SELECT id
                FROM Address__c
                WHERE Country__c = 'Sweden'
        ]);

        Long dt = System.now().getTime();
        Lead lead = new Lead(
                LastName = 'test' + dt, FirstName = 'test' + dt, Company = 'Test company' + dt, Country = 'Sweden',
                Email = 'test' + dt + '@testtesttest.com', RecordTypeId = academicLeadRT, Academic_Lead_Type__c = 'Academic Program Account');
        insert lead;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(lead.Id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Lead - Converted');  //CCE CR# 6546

        Test.startTest();
        Semaphores.LeadTriggerHandlerAfterUpdate = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadAcademicProgramConversion_HasRunAfter = false;

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        Test.stopTest();

        System.assert(lcr.isSuccess());

        List<Address__c> adderesses = new List<Address__c>([
                SELECT id
                FROM Address__c
                WHERE Country__c = 'Sweden'
        ]);
        System.assertEquals(adderessesBefore.size() + 2, adderesses.size());

    }
}
