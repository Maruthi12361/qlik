public with sharing class CaseSearchController 
{

	public string CaseId = '';	
	private Case PrivateCase = null;

	private String soql
	{
		get;
		set;
	}

	public List<Case> Cases {
		get;
		set;
	}

	public CaseSearchController()
	{
		if (System.currentPagereference().getParameters().containsKey('CaseId'))
		{
			CaseId = System.currentPagereference().getParameters().get('CaseId');			
		}
	}
	 
	public String sortDir {
		get  
		{ 
			if (sortDir == null) 
			{  
				sortDir = 'asc'; 
			} 
			return sortDir;  
		}
	    set;
	}
 
	public String sortField {
    	get
    	{
			if (sortField == null) 
			{
				sortField = 'CaseNumber'; 
			} 
			return sortField;  
		}
    	set;
	}	
	
	public void toggleSort() 
	{
		sortDir = sortDir.equals('asc') ? 'desc' : 'asc';    
    	runQuery();
    }	
	
  
  	public String debugSoql 
  	{
    	get 
    	{ 
			return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; 
		}
    	set;
	}	
	
	public Case ThisCase
	{
		get 
		{
			if (PrivateCase == null && CaseId != null)
			{
				PrivateCase = [select Id, Subject, Bug_ID__c, Problem_Case2__c, CaseNumber from Case where Id = :CaseId Limit 1];
				PrivateCase.Problem_Case2__c = null;
			}
		
			return PrivateCase;
		}
		
		set
		{
			PrivateCase = ThisCase;
		}
	}
	
	public string getCaseId()
	{
		Case C = ThisCase;
		
		if (C != null)
		{
			return C.Id;
		}
		
		return null;
	}
	
	public void runQuery()
	{
 		try 
 		{
      		cases = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
    	} 
    	catch (Exception e) 
    	{
      		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Search failed: ' + e.getMessage()));
    	}
  	}	
		
	public PageReference runSearch() {
	 
	    String CaseNumber = Apexpages.currentPage().getParameters().get('CaseNumber');
	    String Subject = Apexpages.currentPage().getParameters().get('Subject');
	    String BugID = Apexpages.currentPage().getParameters().get('BugID');
	    	 
	    soql = 'select Id, Subject, Bug_ID__c, CaseNumber from Case where Id != null';
	    if (!CaseNumber.equals(''))
	    	soql += ' and CaseNumber LIKE \'%'+String.escapeSingleQuotes(CaseNumber)+'%\'';
	    if (!Subject.equals(''))
	    	soql += ' and Subject LIKE \'%'+String.escapeSingleQuotes(Subject)+'%\'';
	    if (!BugID.equals(''))
	    	soql += ' and Bug_ID__c LIKE \'%'+String.escapeSingleQuotes(BugID)+'%\'';  
	 	    
	    runQuery();
	 
    	return null;
	}	
	
	public PageReference backToParentCase() {
		return new ApexPages.StandardController(ThisCase).view();
	}
	 
	public PageReference relateCase() {
		
		if (ThisCase.Problem_Case2__c == null)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No case to relate'));
			return null;	
		}
		
		try
		{
			// Fetch the related object
			Case RelatedCase = [select Id, Problem_Case2__c from Case where Id = :ThisCase.Problem_Case2__c LIMIT 1];
			
			if (RelatedCase == null)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No case to relate'));
				return null;				
			}
			
			RelatedCase.Problem_Case2__c = ThisCase.Id;
			update RelatedCase;
			
			ThisCase.Problem_Case2__c = null;
		}
		catch (Exception Ex)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while relating case: ' + Ex.getMessage()));
			return null;			
		}
		return null;	
	}	

	

}