public with sharing class EP_ApplicationProgressBarController {
     public String accid;
     public String expertId;
     public String CurrentPageURL 
     {
        get {

            return ApexPages.currentPage().getUrl().toLowerCase();
        }
        set ;
     }
    public Account account {get; set;}
    public Solution_Profiles__c SolProfile{get; set;}
    public Partner_Expertise_Area__c PartnerExpertiseArea {get; set;}
    
    public EP_ApplicationProgressBarController()
    {
        User u = [select AccountId, ContactId, Contact.ActiveULC__c, ProfileId from User where id = :UserInfo.getUserId()];
        if(u.AccountId!=null) 
        {
            accid=u.AccountId;
        }
        else
        {
            accid=System.currentPageReference().getParameters().get('acc');
        }
        account = [SELECT Name,Id, RecordTypeId FROM Account WHERE Id=:accId];
        expertId = System.currentPageReference().getParameters().get('expertId');
        system.debug('Account:' + account); 
        system.debug('Expert Id:' + expertId);
        PartnerExpertiseArea = [Select Id, 
                                Expertise_Area__c
                                from Partner_Expertise_Area__c 
                                where Partner_Account_Name__c=: accId and  Id =:expertId];
        
    }
      

     public List<Partner_Expertise_Application_Lookup__c> Partnerlookupobjs
       {
        get {

                Partnerlookupobjs =[Select Name,
                                    Account_Name__r.Name,
                                    Account_Name__r.Id,
                                    Account_Name__c,
                                    Expertise_Area__r.Name,
                                    Expertise_Area__r.Id,
                                    Expertise_Area__r.Expertise_Area__c,
                                    Expertise_Area__r.Status__c,
                                    Expertise_Area__r.Expertise_Application_Eligibility__c,
                                    Expertise_Area__r.Expertise_Designation_Status__c,
                                    Solution_Profile_1_Name__r.Name,
                                    Solution_Profile_1_Name__r.Id,
                                    Solution_Profile_1_Name__r.Status__c,
                                    Solution_Profile_One_Status__c,
                                    Solution_Profile_2_Name__r.Name,
                                    Solution_Profile_2_Name__r.Id,
                                    Solution_Profile_2_Name__r.Status__c,
                                    Solution_Profile_Two_Status__c,
                                    Solution_Profile_3_Name__r.Name,
                                    Solution_Profile_3_Name__r.Id,
                                    Solution_Profile_3_Name__r.Status__c,
                                    Solution_Profile_Three_Status__c,
                                    Solution_Profile_4_Name__r.Name,
                                    Solution_Profile_4_Name__r.Id,
                                    Solution_Profile_4_Name__r.Status__c,
                                    Solution_Profile_Four_Status__c
                                    from Partner_Expertise_Application_Lookup__c  where Account_Name__c=:accid and Expertise_Area__c=:expertId];

              return Partnerlookupobjs;
            }
          set;
       }
      public String getStyleClassForSolProf1()
      {
        return getStyleClass(Partnerlookupobjs[0].Solution_Profile_1_Name__r.Status__c);
      }
      public String getStyleClassForSolProf2()
      {
        return getStyleClass(Partnerlookupobjs[0].Solution_Profile_2_Name__r.Status__c);
      }
      public String getStyleClassForSolProf3()
      {
        return getStyleClass(Partnerlookupobjs[0].Solution_Profile_3_Name__r.Status__c);
      }
      public String getStyleClassForSolProf4()
      {
        return getStyleClass(Partnerlookupobjs[0].Solution_Profile_4_Name__r.Status__c);
      }
      public String getStyleClass(String SolProfStatus)
      {
          if(SolProfStatus == 'In Process') return 'inprogress';
          else if(SolProfStatus == 'Ready for Review') return 'readyforreview';
          else if(SolProfStatus == 'Under Review') return 'underreview';
          else if(SolProfStatus == 'Approved') return 'completed';
          else if(SolProfStatus == 'Rejected') return 'rejected';
          else return '';
       }
       
}