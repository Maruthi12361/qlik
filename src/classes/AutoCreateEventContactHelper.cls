/*2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.*/

public with sharing class AutoCreateEventContactHelper {
	
	public static void createEventAutomaticaly(List<Contact> triggernew){
		 List<Event> events = new List<Event>();    
    	for (Contact newContact: triggernew) {

		// need to check for Name, but also BOTH Start and End dates, 
		// or else the Event insertion will fail if one is empty.
        if ( (newContact.Event_DateTime__c != null) && (newContact.Event_End_DateTime__c != null) && (newContact.Event_Name__c != null) ) {
          
          	// ensure that the start date is before the end date
          	if (newContact.Event_DateTime__c > newContact.Event_End_DateTime__c)
          		newContact.Event_End_DateTime__c = newContact.Event_DateTime__c.addDays(1);

			// ensure that the end date is no more that 14 days from the start date
          	if (newContact.Event_DateTime__c.addDays(14) < newContact.Event_End_DateTime__c)
          		newContact.Event_End_DateTime__c = newContact.Event_DateTime__c.addDays(14);

            	events.add(new Event(
                Subject = newContact.Event_Name__c,
                WhoId = newContact.Id,
                OwnerId = newContact.OwnerId,
                Meeting_Method__c = newContact.Meeting_Method__c,
                ActivityDateTime = newContact.Event_DateTime__c,
                EndDateTime = newContact.Event_End_DateTime__c));
        	}
        	newContact.Event_Name__c = null;
        	newContact.Event_DateTime__c = null;               
   		}
   		insert events;
	}
}