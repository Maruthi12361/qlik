/**
 * Created by ext_bad on 31.01.18.
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 */
@IsTest
private class QS_CoveoSearchTest {
    public static testMethod void TestGuest() {
        QTTestUtils.GlobalSetUp();

        Test.startTest();
        QS_CoveoSearch contr = new QS_CoveoSearch();
        Test.stopTest();

        system.assertNotEquals(null, contr.loginUrl);
    }

    public static testMethod void TestCommunityUser() {
        QTTestUtils.GlobalSetUp();

        List<Profile> ps = [select name from Profile
        where Profile.UserLicense.Name like '%Partner%' and
        Name like 'PRM%' and
        name != 'PRM - BASE' and
        (not name like '%READ ONLY%') and
        name != 'PRM - Base JAPAN' and
        name != 'PRM External Content User'];
        System.assert(ps.size() > 0, 'No profile found with Partner user license');

        User partnerUser = QTTestUtils.createMockUserForProfile(ps[0].Name);
        partnerUser = [select id, name, contact.Id, contact.Name from user where id = :partnerUser.id];

        //Sets the owner so the user has access to its contact
        Contact cu = [SELECT Id, OwnerId, AccountId, Name FROM Contact WHERE Id =: partnerUser.ContactId];
        cu.OwnerId = partnerUser.Id;
        update cu;
        Id lundBusinesshours = [select Id from BusinessHours where Name = 'Lund' limit 1].Id;
        Support_Office__c cancunSupportOffice = new Support_Office__c(Name = 'Cancun', Regional_Support_Manager__c = partnerUser.Id, Business_Hours__c = lundBusinesshours);

        Account partnerAcc = [select Id, Name from Account where Id = :cu.AccountId];
        partnerAcc.Support_Office__c = cancunSupportOffice.Id;
        update partnerAcc;

        System.runAs(partnerUser) {
            Test.startTest();
            QS_CoveoSearch contr = new QS_CoveoSearch();
            Test.stopTest();

            system.assertEquals(null, contr.loginUrl);
        }
    }
}