/****************************************************************************************************
*   Change log:
*
*   27-05-2019  ext_vos     add asserts; add test for CHG0036251 (text updates).
*
****************************************************************************************************/
@isTest
private class SCC_Case_ExclamationExtensionTest {

	private static Id recTypeId;

	public static testMethod void Test1() {
		
		QTTestUtils.GlobalSetUp();

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.Account_Support_Information__c = 'Test';
        Account testAccount2 = TestDataFactory.createAccount('Test AccountName origin', qtc);
        testAccount2.Account_Support_Information__c = 'Test2';
        testAccount2.Red_Account__c = true;
        testAccount2.Special_SLA_customer__c = true;
        testAccount2.Customer_Success_Manager__c = UserInfo.getUserId();

        List<Account> accounts = new List<Account>();
        accounts.add(testAccount);
        accounts.add(testAccount2);
        insert accounts;     
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        // Running as the current user for code coverage only
    	Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);

    	caseObj.Account_Origin__c = testAccount2.Id;
    	insert caseObj;

    	Test.startTest();
    	ApexPages.StandardController stdController = new ApexPages.StandardController(caseObj);
    	SCC_Case_ExclamationExtension controller = new SCC_Case_ExclamationExtension(stdController);

        // add asserts to test
    	System.assert(!controller.getcaseHasEscalation());
    	
        System.assert(controller.getIsAccountInfo());
        controller.getAccountInfoHelpText();
        System.assert(controller.accountInfoHelpText.contains('Strategic Account'));
        System.assert(controller.accountInfoHelpText.contains('Special SLA customer:'));
        System.assert(controller.accountInfoHelpText.contains('Account Support Information:'));
       
    	Test.stopTest();
	}

	private static Id getCaseRecordTypeId(String recordTypeName) {
        if (recTypeId != null) {
            return recTypeId;
        } else {                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}