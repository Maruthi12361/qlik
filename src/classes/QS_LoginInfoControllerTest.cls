/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
public with sharing class QS_LoginInfoControllerTest {
	public static testmethod void testNoSettings() {
		QS_LoginInfoController controller = new QS_LoginInfoController();
		system.debug('controller.logoutUrl: ' + controller.logoutUrl);
		system.assert(controller.logoutUrl == '');
	}
	public static testmethod void testWithSettings() {

		QS_Partner_Portal_Urls__c settings = new QS_Partner_Portal_Urls__c();
		settings.Name = 'Qliktech';
		settings.Support_Portal_Logout_Page_Url__c = 'www.testurl.com';
		insert settings;

		QS_LoginInfoController controller = new QS_LoginInfoController();
		system.assert(controller.logoutUrl == 'www.testurl.com');
	}
}