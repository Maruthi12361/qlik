/*********************************************************
* TestUpdateClosedOppQuotesHandler:
*   Test class to help testing UpdateClosedOppQuotesHandler
*
* Change Log:
*  2017-09-11 Anjuna Baby QCW-3635   Test class for checking the functionalities on UpdateClosedOppQuotesHandler.
*  2017-12-14 Pramod Kumar V Increase code coverage
*********************************************************/

@isTest
private class TestUpdateClosedOppQuotesHandler {
    
    static testMethod void test_OpportunityQuoteUpdate(){
        QuoteTestHelper.createCustomSettings();
        
        User testUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
            
            RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
            RecordType recTypeAccPartner = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Partner_Account' AND SobjectType = 'Account'];
            RecordType recTypeQuote = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'Quote' AND SobjectType = 'SBQQ__Quote__c'];
            Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
            insert subs;
            
            QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
            insert QTcomp;
           
            Account accEndUser = QuoteTestHelper.createAccount(QTcomp, recTypeAccEndUser, 'testEndUserAcc');
            accEndUser.Payment_Terms__c = '45 days';
            accEndUser.Territory_Country__c = 'France';
            insert accEndUser;
            
            Contact contEndUser = QuoteTestHelper.createContact(accEndUser.Id);
            insert contEndUser;
 
            List<Address__c> addrs = new List<Address__c>{
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Billing'),
                QuoteTestHelper.createAddress(accEndUser.Id, contEndUser.Id, 'Shipping')
            };
            insert addrs;
            
            List<Address__c> addrsToUpdate = new List<Address__c>();
            
            for(Address__c address : addrs){
                address .Valid_Address__c = true;
                addrsToUpdate.add(address);
            }
            update addrsToUpdate;
            
            Test.startTest();
            
            Opportunity opp= new Opportunity(
                                        RecordTypeId = [Select id From RecordType where DeveloperName ='Sales_QCCS'].Id,
                                        Name = 'TestOppPartner',
                                        CurrencyIsoCode = 'USD',
                                        Short_Description__c = 'Partner Reseller',
                                        StageName = 'Goal Identified',
                                        CloseDate = Date.today().addDays(7),
                                        Type = 'New Customer',
                                        AccountId = accEndUser.Id,
                                        ForecastCategoryName = 'Omitted',
                                        Revenue_Type__c = 'Direct',
                                        Split_Opportunity__c=true
                                        );
            insert opp;

            SBQQQuoteTriggerHandler.IsAfterInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsAfterUpdateProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeInsertProcessing = true;
            SBQQQuoteTriggerHandler.IsBeforeUpdateProcessing = true;
            
            
             SBQQ__Quote__c quote = QuoteTestHelper.createQuote(recTypeQuote, contEndUser.Id, accEndUser, '' , 'Direct', 'Open', 'Quote', true, opp.Id);
             quote.Quote_Recipient__c = contEndUser.Id;
             insert quote;
             
             SBQQ__Quote__c quote1 = QuoteTestHelper.createQuote(recTypeQuote, contEndUser.Id, accEndUser, '' , 'Direct', 'Waiting for Approval', 'Quote', true, opp.Id);
             quote1.Quote_Recipient__c = contEndUser.Id;
             insert quote1;
             
             SBQQ__Quote__c quote2 = QuoteTestHelper.createQuote(recTypeQuote, contEndUser.Id, accEndUser, '' , 'Direct', 'Waiting for Approval', 'Quote', true, opp.Id);
             quote2.Quote_Recipient__c = contEndUser.Id;
             insert quote2;
             /*
             sbaa__Approval__c approvals=new sbaa__Approval__c ();
             approvals.sbaa__Status__c='Waiting for Approval';
             approvals.sbaa__ApprovalStep__c=1;
             approvals.sbaa__RecordField__c=quote2.id;
             insert approvals;
             */
             UpdateClosedOppQuotesHandler.OpportunityQuoteUpdate = false;
             opp.stageName = 'Closed Won';
             opp.Included_Products__c = 'Qlik Sense';
             update opp;
             
             system.assertEquals(opp.stageName, 'Closed Won');
             Test.stopTest();
            
        }    
    }
     
}