/**************************************************
* Change Log:
* 
* 2016-08-15    CCE Initial development. CR# 92771 - https://eu1.salesforce.com/a0CD000000zNao2 - 
*                                        New updates to the Marketo Support Request 
* 2019-02-08    CCE CHG0035276 - populate default user to Requestor field for Marketing Data Request record type.
**************************************************/
public with sharing class DispatcherORNewController {

    public DispatcherORNewController(ApexPages.StandardController controller) {
        this.controller = controller;
    }

    public PageReference getRedir() {

        Id IdRT = ApexPages.currentPage().getParameters().get('RecordType');
        System.Debug('DispatcherORNewController: IdRT = ' + IdRT);


        PageReference newPage;
        //PageReference newPage = new PageReference('a3c/e');
        //System.Debug('DispatcherORNewController: newPage = ' + newPage);

        if ((IdRT == '012D0000000KJMD') || (IdRT == '012D0000000K9Lj')) {   //Marketo Support Requests or Marketing Data Request
            newPage = new PageReference('/a3c/e?retURL=/a3c/o&nooverride=true&RecordType='+IdRT+'&CF00ND0000003jauL_lkid='+userinfo.getuserid()+'&CF00ND0000003jauL='+userinfo.getFirstName()+ ' ' + userinfo.getLastName());
            System.Debug('DispatcherORNewController: newPage = ' + newPage);
            return newPage.setRedirect(true);
        } else {
            System.Debug('DispatcherORNewController: null IdRT = ' + IdRT);
            return null;
        }

    }

    private final ApexPages.StandardController controller;
}