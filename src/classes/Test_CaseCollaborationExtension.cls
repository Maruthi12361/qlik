/**********************************************************
ChangeLog
    2013-08-07  CCE Test class for CaseCollaborationExtension.
                Initial creation for CR# 8441.

***********************************************************/
@isTest(SeeAllData=true)
private class Test_CaseCollaborationExtension
{
    static testMethod void TestCaseCollaborationExtension() 
    {
        RecordType caseRT = [select id From RecordType where SobjectType = 'Case' AND name =: 'QlikTech Master Support Record Type' limit 1];
        Id pId = [Select Id From Profile Where Name Like 'System Administrator' Limit 1].Id; 
        User johnBrown = new User();
        johnBrown = [select Id from User where profileId=:pId and isActive=true limit 1];
        
        Case testCase = new Case(RecordTypeId = caseRT.Id,
                                Subject = 'Test Case N',
                                Status = 'Not Opened',
                                Description = 'Desc: Test Case N',    
                                Category__c = 'Applications', 
                                Requester_Name__c = johnBrown.Id,
                                Priority = 'Medium',
                                SuppliedEmail = 'abc@abc.com');
        insert testCase;
        
        test.startTest();
        ApexPages.StandardController StandardController = new ApexPages.StandardController(testCase );
        CaseCollaborationExtension controller = new CaseCollaborationExtension(StandardController);
        String issueDescription = controller.issueDescription;
        String DefaultCollaborationTeam = controller.DefaultCollaborationTeam;
        Boolean bRenderButton = controller.bRenderButton;
        List<SelectOption> options = controller.getCollaborationTeams();
        System.debug('Test Case Collaboration: size = ' + options.size());
        System.assert(options != null);
        //controller.updateIssueDescription();
        controller.issueDescription = 'test';
        controller.requestCollaboration();
        //controller.updateChatter();
        Case retCase = [Select Id, Status from Case where Id = :testCase.Id];
        //changed.Commented by Rodion.Was error
        //  System.assert(retCase.Status == 'Collaboration Requested');
        //
        controller.reloadPage();
        controller.issueDescription = 'test';
        controller.delayCollaboration();        
        Case caze = controller.caze;
        controller.caze = caze;
        test.stopTest();
    }
}