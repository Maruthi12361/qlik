/*
	Changelog:
	2017-03-27 : Roman Dovbush (4front) : Initial development
	29.03.2017 Rodion Vakulvoskyi fix test failures commenting QuoteTestHelper.createCustomSettings();
	2017-04-21  MTM      Fix limit errors
	2018-08-09 : Aslam Kamal QCW-2934 Fixed failures
	* 02.09.2017 : Srinivasan PR- fix for query error
*   18-04-2018 : AIN BSL-10 fix
    13-02-2019 : Fixed soql for new contact duplication rule
*/

@isTest
private class SBBQQQuoteSOIHelperTest {

	@testSetup static void testSetup() {
		QTTestUtils.GlobalSetUp();
		QTTestUtils.SetupNSPriceBook('USD');
		//QuoteTestHelper.createCustomSettings();

		Id AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('Account', 'End_User_Account').Id;
		Profile p = [select id from profile where name='System Administrator'];
		User u = new User(alias = 'standt', email='standarduser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standarduser@hourserr.com',
        INT_NetSuite_InternalID__c = '5307');

		User u1 = new User(alias = 'test', email='standardtestuser@testorg.com',
        emailencodingkey='UTF-8', lastname='TestingNSId', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username=System.now().millisecond() + '_' + 'standardtestuser@hourserr.com');
        insert u1;

        System.runAs(u) {
			Account  testAccount = QTTestUtils.createMockAccount('TestCompany', u, true);
	        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
			testAccount.Name = 'Test Val Rule 1';
	        update testAccount;

			QlikTech_Company__c companyQT = [Select Id, QlikTech_Company_Name__c, Country_Name__c From QlikTech_Company__c Where Country_Name__c = 'France'];
			System.assertEquals('France', companyQT.Country_Name__c);

	        Contact testContact = QTTestUtils.createMockContact(); // partner contact, lastname = 'RDZTestLastName'
	       	Contact testContact1 = new Contact(); // typical account contact;
	       	testContact1.FirstName = 'Roman';
	       	testContact1.Lastname = 'Dovbush';
	       	testContact1.Email = 'test@test.com';
	       	testContact1.HasOptedOutOfEmail = false;
	       	testContact1.AccountId = testAccount.Id;
	       	insert testContact1;

	       	Contact testContact2 = new Contact(); // typical account contact;
	       	testContact2.FirstName = 'Ivan';
	       	testContact2.Lastname = 'Stelmah';
	       	testContact2.Email = 'test@test.com';
	       	testContact2.HasOptedOutOfEmail = false;
	       	testContact2.AccountId = testAccount.Id;
	       	insert testContact2;


	        RecordTYpe rTypeAcc2 = QuoteTestHelper.getRecordTypebyDevName('Account', 'Partner_Account');
	        Account testPartnerAccount = QuoteTestHelper.createAccount(companyQT, rTypeAcc2, 'Partner'); // acc name = 'PartnerAcc'
	        testPartnerAccount.Partner_Margin__c = 12;
	        testPartnerAccount.Navision_Status__c = 'Partner';
	        testPartnerAccount.Territory_Country__c = 'France';
			insert	testPartnerAccount;
			testContact.AccountId = testPartnerAccount.id;
			update testContact;

			//testContact1.AccountId = testPartnerAccount.id;
			//update testContact1;

			Address__c address = new Address__c();
	        address.Account__c = testPartnerAccount.Id;
	        address.Address_Contact__c = testContact.Id;
	        address.Valid_Address__c = true;
	        address.Address_Type__c = 'Billing';
	        address.Country__c = 'France';
	        address.Address_1__c = '10 Road Island';
	        address.City__c  = 'Paris';
	        insert address;
			/******* changes for qcw-2934 start *********/
            insert QuoteTestHelper.createPCS(testPartnerAccount);
             /******* changes for qcw-2934 start *********/
	    }
	}

	@isTest static void test_createSOI_onPrimaryQuote() {
		User u = [Select Id, Lastname, Email, INT_NetSuite_InternalID__c From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rTypeQuote = QuoteTestHelper.getRecordTypebyDevName('SBQQ__Quote__c', 'Quote');
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name like 'PartnerAcc%'];
        Contact testContact = [Select Id, Name From Contact Where Lastname like 'RDZTestLastName%'];
        Contact testContact1 = [Select Id, Name From Contact Where Lastname = 'Dovbush'];
        Contact testContact2 = [Select Id, Name From Contact Where Lastname = 'Stelmah'];
        RecordType oppRT = QuoteTestHelper.getRecordTypebyDevName('Opportunity', 'Sales_QCCS');

   		System.runAs(u) {
			Test.startTest();
			Opportunity testNewOpp = QuoteTestHelper.createOpportunity(testAccount, testPartnerAccount.Id, oppRT);
			testNewOpp.Revenue_Type__c = 'Direct';
			insert testNewOpp;
            System.assertNotEquals(null, testNewOpp.Id);

   //         Opportunity testNewOpp1 = QuoteTestHelper.createOpportunity(testAccount, testPartnerAccount.Id, oppRT);
			//insert testNewOpp1;
   //         System.assertNotEquals(null, testNewOpp1.Id);

			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact1.id, testAccount, '', 'Direct', 'Open', 'Quote', true, ''+testNewOpp.Id);
            insert quoteForTest;
            System.assertNotEquals(null, quoteForTest.Id);

            Sphere_of_Influence__c soi = [Select Id, Opportunity__c, Contact__c From Sphere_of_Influence__c Where Opportunity__c =: quoteForTest.SBQQ__Opportunity2__c];
            System.assertNotEquals(null, soi.Id);

            Map<Id, List<Sphere_of_Influence__c>> mapSOI = SBBQQQuoteSOIHelper.createSoiMap(new List<Sphere_of_Influence__c>{soi});
            Map<Id, List<Sphere_of_Influence__c>> mapOppSOI = SBBQQQuoteSOIHelper.createOppSoiMap(new List<Sphere_of_Influence__c>{soi});

            Test.stopTest();
		}

	}

	@isTest static void TestUpdateQuote() {
		Contact testContact2 = [Select Id, Name From Contact Where Lastname = 'Dovbush'];
		RecordType rTypeQuote = QuoteTestHelper.getRecordTypebyDevName('SBQQ__Quote__c', 'Quote');
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name like 'PartnerAcc%'];
        RecordType oppRT = QuoteTestHelper.getRecordTypebyDevName('Opportunity', 'Sales_QCCS');
		User u = [Select Id, Lastname, Email, INT_NetSuite_InternalID__c From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];

		System.runAs(u) {
			Test.startTest();
			Opportunity testNewOpp = QuoteTestHelper.createOpportunity(testAccount, testPartnerAccount.Id, oppRT);
			testNewOpp.Revenue_Type__c = 'Direct';
			insert testNewOpp;
			SBQQ__Quote__c quoteForTest1 = QuoteTestHelper.createQuote(rTypeQuote, testContact2.id, testAccount, '', 'Direct', 'Open', 'Quote', true, ''+testNewOpp.Id);
            insert quoteForTest1;
            System.assertNotEquals(null, quoteForTest1.Id);


            quoteForTest1 = [Select Id, SBQQ__Primary__c From SBQQ__Quote__c Where Id =: quoteForTest1.Id];
            quoteForTest1.Short_Description__c = 'test';
            update quoteForTest1;
			Test.stopTest();
		}
	}
	@isTest static void test_updateQuote_andCreateSOI() {
		User u = [Select Id, Lastname, Email, INT_NetSuite_InternalID__c From User Where Lastname = 'Testing' And Email = 'standarduser@testorg.com'];
		RecordType rTypeQuote = QuoteTestHelper.getRecordTypebyDevName('SBQQ__Quote__c', 'Quote');
		Account testAccount = [Select Id, Name From Account Where Name = 'Test Val Rule 1'];
		Account testPartnerAccount = [Select Id, Name From Account Where Name like 'PartnerAcc%'];
        Contact testContact = [Select Id, Name From Contact Where Lastname like 'RDZTestLastName%'];
        Contact testContact1 = [Select Id, Name From Contact Where Lastname = 'Dovbush'];
        RecordType oppRT = QuoteTestHelper.getRecordTypebyDevName('Opportunity', 'Sales_QCCS');

   		System.runAs(u) {
			
			Opportunity testNewOpp = QuoteTestHelper.createOpportunity(testAccount, testPartnerAccount.Id, oppRT);
			testNewOpp.Revenue_Type__c = 'Direct';
			insert testNewOpp;
            System.assertNotEquals(null, testNewOpp.Id);

            Test.startTest();

			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, testContact1.id, testAccount, '', 'Direct', 'Open', 'Quote', true, ''+testNewOpp.Id);
            insert quoteForTest;
            System.assertNotEquals(null, quoteForTest.Id);

            Sphere_of_Influence__c soi = [Select Id, Opportunity__c, Contact__c, Quote_Recipient__c From Sphere_of_Influence__c Where Opportunity__c =: quoteForTest.SBQQ__Opportunity2__c];
            System.assertNotEquals(null, soi.Id);
            Map<Id, List<Sphere_of_Influence__c>> mapSOI = SBBQQQuoteSOIHelper.createSoiMap(new List<Sphere_of_Influence__c>{soi});

            SBBQQQuoteSOIHelper.updateQuoteOppSOI(new List<SBQQ__Quote__c>{quoteForTest}, mapSOI, new Set<String>{testContact1.id}, new Set<String>{testNewOpp.id});

            quoteForTest = [Select Id, SBQQ__Primary__c From SBQQ__Quote__c Where Id =: quoteForTest.Id];
            quoteForTest.SBQQ__Primary__c = true;
            update quoteForTest;

   			Test.stopTest();
		}

	}


}