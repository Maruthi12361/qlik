/********
* NAME : UpdateExistingAccountLicenses
* Description: Script that should be run once to populate all existing accountLicenses with sell through partner and support provided by from corresponding NsSupportContract
* Create by: IRN 2016-01-19
*
*Change Log:
******/
global class UpdateExistingAccountLicenses implements Database.Batchable<sObject>, schedulable{

    global UpdateExistingAccountLicenses ()
    {
        System.Debug('--UpdateExistingAccountLicenses--');
    }

    global void execute (SchedulableContext sc)
    {
        System.Debug('--excecute--');
        ID batchId = Database.executeBatch(this); 
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        System.Debug('Starting: UpdateExistingAccountLicenses');
        return Database.getQueryLocator([Select Contract_Item_Account_License__c, NS_Support_Contract__c from NS_Support_Contract_Item__c where Contract_Item_Account_License__c!= null and NS_Support_Contract__c != null]);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope){
        List<NS_Support_Contract_Item__c> items = (List<NS_Support_Contract_Item__c>)scope;
        Map<Id, Id> accountLicenseMap = new Map<Id, Id>();
        List<Id> contractIds = new List<Id>();
        for(NS_Support_Contract_Item__c item : items){
            if(!accountLicenseMap.containsKey(item.Contract_Item_Account_License__c)){
                accountLicenseMap.put(item.Contract_Item_Account_License__c, item.NS_Support_Contract__c);
                contractIds.add(item.NS_Support_Contract__c);
            }
        }

        List<NS_Support_Contract__c> contracts = [Select Id, Reseller__c, Responsible_Partner__c from NS_Support_Contract__c where Id in : contractIds];
        Map<Id, NS_Support_Contract__c> uniqueContracts = new Map<Id, NS_Support_Contract__c>();
        for(NS_Support_Contract__c c : contracts){
            if(!uniqueContracts.containsKey(c.Id)){
                uniqueContracts.put(c.Id, c);
            }
        }

        List<Account_License__c> licenses = [Select id, Support_Provided_By__c, Selling_Partner__c from Account_License__c where Id in :accountLicenseMap.keyset()];
        
        Integer BATCHSIZE = 200;
        if (licenses == null || licenses.size() == 0)
        {
            return;
        }

        for (Integer i = 0; i < (licenses.Size() / BATCHSIZE) + 1; i++)
        {
            List<Account_License__c> tobeUpdated = new List<Account_License__c>();
            List<Account_License__c> tempList;
            if (licenses.size() > ((i + 1) * BATCHSIZE))
            {
                tempList = getRange(licenses, i * BATCHSIZE, BATCHSIZE);
            }
            else
            {
                tempList = getRange(licenses, i * BATCHSIZE, licenses.size() - (i * BATCHSIZE));
            }

            for(Account_License__c l : tempList){
                Boolean add = false;
                Id contractId = accountLicenseMap.get(l.Id);
                NS_Support_Contract__c contract = uniqueContracts.get(contractId);

                if(l.Selling_Partner__c != contract.Reseller__c){//sell through partner
                    l.Selling_Partner__c = contract.Reseller__c;
                    add = true;
                }
                if(l.Support_Provided_By__c != contract.Responsible_Partner__c){
                    l.Support_Provided_By__c = contract.Responsible_Partner__c;
                    add = true;
                }

                if(add){
                    tobeUpdated.add(l);
                }
            }
            System.debug('Account_License__c to update ' + tobeUpdated.size());
            update tobeUpdated;
        }
    }

    public List<Account_License__c> getRange(List<Account_License__c> fromList, Integer fromIndex, Integer amount)
    {
        List<Account_License__c> returnList = new List<Account_License__c>();
        for(Integer i = fromIndex; i < fromIndex + amount; i++){
            returnList.add(fromList.get(i));
        }
        return returnList;
    }

    global void finish(Database.BatchableContext bc){
    }


     /* To Schedule the job use
     //
    UpdateExistingAccountLicenses u = new UpdateExistingAccountLicenses();
    String sch = '0 0 * * * ?';
    String jobID = system.schedule('UpdateExistingAccountLicenses Job', sch, u);

*/
  
}