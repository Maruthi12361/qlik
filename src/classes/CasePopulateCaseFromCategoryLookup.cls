/*
* Change log: 
*
* 2018-09-27    ext_vos CHG0034706: set default value for Case.Type, if there is no appropriate CategoryLookup. Formatting code.
*/
public with sharing class CasePopulateCaseFromCategoryLookup {
    public static Map<String, Category_Lookup__c> areasIssuesCategoryLookup;

    public static void PopulateCase(List<Case> triggerNew) {
        String liveChatRecordTypeId;
        Schema.RecordTypeInfo liveChatRecordTypeInfo = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Live_Chat_Support_Record_Type');
        if (liveChatRecordTypeInfo != null) {
            liveChatRecordTypeId = liveChatRecordTypeInfo.getRecordTypeId();
        }
        String qlikTechMasterRecordTypeId;
        Schema.RecordTypeInfo qlikTechMasterRecordTypeInfo = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('QlikTech_Master_Support_Record_Type');
        if (qlikTechMasterRecordTypeInfo != null) {
            qlikTechMasterRecordTypeId = qlikTechMasterRecordTypeInfo.getRecordTypeId();
        }

        areasIssuesCategoryLookup = new Map<String, Category_Lookup__c>();
        Set<String> areasIssues = new Set<String>();
        for (Case caseObj : triggerNew) {
            if ((caseObj.RecordTypeId != null && (caseObj.RecordTypeId == qlikTechMasterRecordTypeId || caseObj.RecordTypeId == liveChatRecordTypeId))
                    && caseObj.Area__c != null && caseObj.Issue__c != null) {
                areasIssues.add(caseObj.Area__c + '-' + caseObj.Issue__c);
            }
        }
        //Name is area, Category_Level__c is issue
        if (areasIssues.size() > 0) {
            List<Category_Lookup__c> categoryLookups = [select Id, Area_Issue__c, Category_Level__c, Case_Owner__c, Service_Request_Type__c,
                    Severity__c, Functional_Area__c, Question_Prompt_1__c, Question_Prompt_2__c,
                    Question_Prompt_3__c, Question_Prompt_4__c, Question_Prompt_5__c
            from Category_Lookup__c
            where Area_Issue__c in :areasIssues];
            for (Category_Lookup__c cl : categoryLookups) {
                areasIssuesCategoryLookup.put(cl.Area_Issue__c, cl);
            }
        } else {
            return;
        }

        for (Case caseObj : triggerNew) {
            if (!areasIssuesCategoryLookup.containsKey(caseObj.Area__c + '-' + caseObj.Issue__c)) {
                caseObj.Type = 'Incident';
            } else {
                if (caseObj.Area__c != null && caseObj.Issue__c != null && areasIssuesCategoryLookup.containsKey(caseObj.Area__c + '-' + caseObj.Issue__c)) {
                    Category_Lookup__c categoryLookupSelectedRecord = areasIssuesCategoryLookup.get(caseObj.Area__c + '-' + caseObj.Issue__c);

                    if (categoryLookupSelectedRecord != null) {
                        if (String.isNotBlank(categoryLookupSelectedRecord.Case_Owner__c) && categoryLookupSelectedRecord.Case_Owner__c == 'Team Qoncierge') {
                            caseObj.Type = 'Service Request';
                        } else {
                            caseObj.Type = 'Incident';
                        }
                        if (String.isNotBlank(categoryLookupSelectedRecord.Service_Request_Type__c) && caseObj.Service_Request_Type__c == null) {
                            caseObj.Service_Request_Type__c = categoryLookupSelectedRecord.Service_Request_Type__c;
                        }
                        if (String.isNotBlank(categoryLookupSelectedRecord.Severity__c) && caseObj.Severity__c == null) {
                            caseObj.Severity__c =  categoryLookupSelectedRecord.Severity__c;
                        }
                        if (String.isNotBlank(categoryLookupSelectedRecord.Functional_Area__c) && caseObj.Functional_Area__c == null) {
                            caseObj.Functional_Area__c =  categoryLookupSelectedRecord.Functional_Area__c;
                        }
                        if (caseObj.Description == null
                                && (categoryLookupSelectedRecord.Question_Prompt_1__c != null || categoryLookupSelectedRecord.Question_Prompt_2__c != null
                                || categoryLookupSelectedRecord.Question_Prompt_3__c != null || categoryLookupSelectedRecord.Question_Prompt_4__c != null
                                || categoryLookupSelectedRecord.Question_Prompt_5__c != null)) {

                            String descriptionText = createDescriptionText(new List<string>{categoryLookupSelectedRecord.Question_Prompt_1__c,
                                    categoryLookupSelectedRecord.Question_Prompt_2__c,
                                    categoryLookupSelectedRecord.Question_Prompt_3__c,
                                    categoryLookupSelectedRecord.Question_Prompt_4__c,
                                    categoryLookupSelectedRecord.Question_Prompt_5__c});
                            // retain the original value of descriptionText in originalDescriptionText variable
                            if (String.isNotBlank(descriptionText)) {
                                caseObj.Description = descriptionText;
                            }
                        }
                    }
                }
            }
        }
    }

    public static String createDescriptionText(List<string> questions) {
        string descriptionText = '';
        if(questions != null) {
            for(string question : questions) {
                if(question != null && question != '') {
                    descriptionText += question + '\n\n';
                }
            }
        }
        return descriptionText;

    }
}