/*************************************************
* 
* Class: LeadUpdateAutoCreateEventHandler
*
* Changelog: 
*   2013-07-08  CCE CR# 8551 https://eu1.salesforce.com/a0CD000000ZB4gl Reduce number of 
*                   SOQL queries made by AutoCreateEvent_LeadUpdate.trigger
* 
*	2017-10-25 AYS BMW-402 :  Migrated from AutoCreateEvent_LeadUpdate.trigger.
*					   
****************************************************/

public class LeadUpdateAutoCreateEventHandler {
	public LeadUpdateAutoCreateEventHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, Map<Id, Lead> triggerOldMap) {
	
	    List<Event> events = new List<Event>();
	    List<Lead> leads = new List<Lead>();
	    // gather the user/owner Ids
	    List<Id> userIds = new List<Id>();
	    for (Lead newLead : triggerNew) {
	        if (newLead.Event_DateTime__c != triggerOldMap.get(newLead.Id).Event_DateTime__c && newLead.Event_Name__c != triggerOldMap.get(newLead.Id).Event_Name__c && 
	           newLead.Event_End_DateTime__c != triggerOldMap.get(newLead.Id).Event_End_DateTime__c && (newLead.Event_DateTime__c != null) && (newLead.Event_End_DateTime__c != null) 
	           && (newLead.Event_Name__c != null) && newLead.OwnerId != null){
	               userIds.add(newLead.OwnerId);
	               leads.add(newLead);
	           }
	           //System.debug('newLead.Id=' + newLead.Id+', OwnerId='+newLead.OwnerId + ', Event_Name__c =' + newLead.Event_Name__c + ', Event_End_DateTime__c=' + newLead.Event_End_DateTime__c + ', Event_DateTime__c=' + newLead.Event_DateTime__c + 'Event_Name__c=' + newLead.Event_Name__c);
	    }
	    //System.debug('AutoCreateEvent_LeadUpdate - ' + userIds);
	    // create a map of the Owners of the leads
	    if(!leads.isEmpty()){//UIN
	        Map<Id,User> ownerMap = new Map<Id,User>([select Id, IsActive from User where Id in :userIds]);
	        
	        // get the user that will be assigned any events/leads that happen to belong to inactive users
	        //User defaultUser = [select Id from User where FirstName = 'Anna' and LastName = 'Forsgren' limit 1];  //CCE CR# 8551
	    
	        // will hold value to be assigned to lead and event
	        Id ownerId;
	        
	        for (Lead newLead: Leads) {
	    
	            // need to check for Name, but also BOTH Start and End dates, 
	            // or else the Event insertion will fail if one is empty.
	            if (newLead.Event_DateTime__c != triggerOldMap.get(newLead.Id).Event_DateTime__c && newLead.Event_Name__c != triggerOldMap.get(newLead.Id).Event_Name__c && 
	               newLead.Event_End_DateTime__c != triggerOldMap.get(newLead.Id).Event_End_DateTime__c && (newLead.Event_DateTime__c != null) && (newLead.Event_End_DateTime__c != null) && (newLead.Event_Name__c != null) ) {
	              
	                // check to ensure that owner of the lead is active, use default owner if not
	    
	                if (ownerMap.get(newLead.OwnerId) != null && ownerMap.get(newLead.OwnerId).IsActive) {
	                    ownerId = newLead.OwnerId;
	                } else {
	                    ownerId = '00520000001PLHEAA4'; //User Id for Anna Forsgren //CCE CR# 8551
	                    //ownerId = defaultUser.Id;
	                    system.Debug('Changing Lead Owner to Anna Forsgren');
	                    newLead.OwnerId = ownerId;
	                }
	                
	                // ensure that the start date is before the end date
	                if (newLead.Event_DateTime__c > newLead.Event_End_DateTime__c)
	                    newLead.Event_End_DateTime__c = newLead.Event_DateTime__c.addDays(1);
	    
	                // ensure that the end date is no more that 14 days from the start date
	                if (newLead.Event_DateTime__c.addDays(14) < newLead.Event_End_DateTime__c)
	                    newLead.Event_End_DateTime__c = newLead.Event_DateTime__c.addDays(14);
	              
	                events.add(new Event(
	                    Subject = newLead.Event_Name__c,
	                    WhoId = newLead.Id,
	                    OwnerId = ownerId,
	                    Meeting_Method__c = newLead.Meeting_Method__c,
	                    ActivityDateTime = newLead.Event_DateTime__c,
	                    EndDateTime = newLead.Event_End_DateTime__c) 
	                );
	            }
	            newLead.Event_Name__c = null;
	            newLead.Event_DateTime__c = null;               
	       }
	       if(!events.isEmpty())
	           insert events;
	   	}
	}
}