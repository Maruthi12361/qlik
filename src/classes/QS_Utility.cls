/************************************************************
* Utility class for Qlik support portal
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/

public with sharing class QS_Utility {
    private static List<string> approvedULCLevels;
    
    public static string GetQS_Persona_Category(string Personas) {
        string searchCategory = '';
        for(string s : Personas.split(';'))
        {
            s = s.trim();
            QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues(s);
            if(categoryRecord != null)
            {
                //searchCategory = searchCategory == '' ? categoryRecord.Article_Categories__c : searchCategory + ',' + categoryRecord.Article_Categories__c;

                searchCategory = searchCategory + (searchCategory == '' ? '' : ', ') + categoryRecord.Article_Categories__c;
            }
        }
        return searchCategory;
    }
    
    public static boolean HasULCLevel(id contactId, string ulcLevel)
    {
        boolean found = false;
        try{
            if(approvedULCLevels == null) {
                approvedULCLevels = new List<string>();
                List<Assigned_ULC_Level__c>  ulcLevels = [select Id, ULCLevelId__r.Name, Status__c from Assigned_ULC_Level__c where contactId__c =: contactId and ULCLevelId__r.Status__c = 'Active'];
                for(Assigned_ULC_Level__c level : ulcLevels) {
                    if(level.Status__c.Contains('Approved')) {
                        approvedULCLevels.Add(level.ULCLevelId__r.Name);
                    }
                }
            }
           
            for(string level : approvedULCLevels) {
                if(level.toLowerCase() == ulcLevel.toLowerCase())
                    found = true;
            }
        }
        catch (Exception e) {
            System.debug('ERROR:' + e);
        }
        return found;
    }
}