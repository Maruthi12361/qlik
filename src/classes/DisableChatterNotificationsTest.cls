/**
* Change log:
*
*   04-04-2019  ext_vos, extbad CHG0030387: Create.
*/
@isTest
public class DisableChatterNotificationsTest {

    @TestSetup
    public static void setup() {
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccountInt = TestDataFactory.createAccount('New QlikTech Test', qtc);
        insert testAccountInt;
    }

    static testMethod void testFlow() {
        // check admin user for test only. Handler method includes "isRunningTest".
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        User u = new User(FirstName = 'userDisableChatter', Alias = 'admtus',
                Email = 'user' + System.now().millisecond() + '@chemtest.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocalesIdKey = 'en_US', ProfileId = p.Id, Country = 'Sweden',
                TimeZonesIdKey = 'America/Los_Angeles', Username = 'admtus' + System.now().millisecond() + '@chemtest.com',
                UserPreferencesDisableAllFeedsEmail = false);
        insert u;

        User testUser = [SELECT UserPreferencesDisableAllFeedsEmail FROM User WHERE Id =: u.Id];
        System.assert(!testUser.UserPreferencesDisableAllFeedsEmail);
        List<NetworkMember> nms = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :u.Id];
        for (NetworkMember nm : nms) {
            System.assert(!nm.PreferencesDisableAllFeedsEmail);
        }

        Test.startTest();
        Database.executeBatch(new DisableChatterNotifications());
        Test.stopTest();

        testUser = [SELECT UserPreferencesDisableAllFeedsEmail FROM User WHERE Id =: u.Id];
        System.assert(testUser.UserPreferencesDisableAllFeedsEmail);
        nms = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :u.Id];
        for (NetworkMember nm : nms) {
            System.assert(nm.PreferencesDisableAllFeedsEmail);
        }
    }
}