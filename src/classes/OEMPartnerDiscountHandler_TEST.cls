/*
	Change Log
	2017-03-27		TJG		Increase test coverage to 100%
	2017-05-17 MTM Set QCW-1633 OEM Subscription deals
	2017-05-30 Rodion Vakulovskyi edited to fix teset error qcw 2496
    2017-09-03 Srinivasan PR - fix for test class error QCW 2934
    2018-04-18 AIN - Fixes test class for validation rule in BSL-10
 */

@isTest
public class OEMPartnerDiscountHandler_TEST
{
    static testMethod void runOEMPartnerTest() {

    	Profile p = [select id from profile where name='System Administrator'];
    	User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com',
			emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          	localesidkey='en_US', profileid = p.Id,
          	timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.RunAs(mockSysAdmin)
        {
		    QTTestUtils.GlobalSetUp();
        	QTTestUtils.SetupNSPriceBook('GBP');

			QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');
			qtc.Country_Name__c = 'Sweden';
			qtc.CurrencyIsoCode = 'SEK';
			update qtc;
            Account act = new Account(
            	name='Test Account',
            	OwnerId = mockSysAdmin.Id,
            	Navision_Customer_Number__c = '12345',
            	Legal_Approval_Status__c = 'Legal Approval Granted',
				QlikTech_Company__c = qtc.QlikTech_Company_Name__c,
				Partner_Margin__c = 12,
				RecordTypeId = '01220000000DOG4AAO',
	        	Navision_Status__c = 'Partner',
            	Billing_Country_Code__c = qtc.Id,
            	BillingStreet = '417',
            	BillingState ='CA',
            	BillingPostalCode = '94104',
            	BillingCountry = 'United States',
            	BillingCity = 'SanFrancisco',
            	Shipping_Country_Code__c = qtc.Id,
            	ShippingStreet = '417',
            	ShippingState ='CA',
            	ShippingPostalCode = '94104',
            	ShippingCountry = 'United States',
            	ShippingCity = 'SanFrancisco');
            insert act;

            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            //Create test Opportunity
            //Id AccRecordTypeId_EndUserAccount = QuoteTestHelper.getRecordTypebyDevName('End_User_Account').Id;
 			Account  testAccount = QTTestUtils.createMockAccount('TestCompany', mockSysAdmin, true);
	        testAccount.RecordtypeId = '012D0000000KBYxIAO';  //OEM End User Account
			testAccount.Name = 'Test Val Rule 1';
	        update testAccount;

            Contact ct = new Contact(AccountId=testAccount.Id,lastname='Contact1',firstname='Test');
            insert ct;

            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'OEM',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = '01220000000DXq5AAG',   // OEM - Order
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = testAccount.Id,
				End_User_Account_del__c = testAccount.Id,
                OEM_Partner_Account__c=act.Id

            );

        	Test.startTest();
            insert testNewOpp;

            testNewOpp = [select OEM_Partner_Discount__c from Opportunity where Id = :testNewOpp.Id];
            system.debug('Discount = ' + testNewOpp.OEM_Partner_Discount__c);
			system.assert(testNewOpp.OEM_Partner_Discount__c > 11.0);

            Test.stopTest();
        }
    }
	// Test SBQQQuoteSetFields.PopulateProductFilterOnQuotes
	static testMethod void runOEMSubscriptionTest() {

    	Profile p = [select id from profile where name='System Administrator'];
    	User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com',
			emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          	localesidkey='en_US', profileid = p.Id,
          	timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.RunAs(mockSysAdmin)
        {
		    QTTestUtils.GlobalSetUp();
        	QlikTech_Company__c qtc = QTTESTUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'SWE');

            Account  testAccount = QTTestUtils.createMockAccount('TestCompany', mockSysAdmin, true);
	        testAccount.RecordtypeId = '012D0000000KBYxIAO';  //OEM End User Account
			testAccount.Name = 'Test Val Rule 1';
	        update testAccount;

            Contact ct = new Contact(AccountId=testAccount.Id,lastname='Contact1',firstname='Test', Email = 'rodion@gmail.com');
            insert ct;

            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'OEM',
                CloseDate = Date.today(),
				Solution_Type__c = 'Hosted Subscription Items Only',
				//Subscription_Start_Date__c = Date.today(),
				//Subscription_Unit__c = 'Months',
				//No_of_Subscription_Units__c = '3',
                StageName = 'Alignment Meeting',
                RecordTypeId = '01220000000DXq5AAG',   // OEM - Order
                CurrencyIsoCode = 'GBP',
                Signature_Type__c = 'Digital Signature',
                Customers_Business_Pain__c = 'To address the Alignment section must be completed on large deals error',
                AccountId = testAccount.Id,
				End_User_Account_del__c = testAccount.Id
            );
			insert testNewOpp;
			Test.startTest();
			RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote'];
			insert QuoteTestHelper.createPCS(testAccount);//2934
			SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rTypeQuote, ct.id, testAccount, '', 'OEM', 'Open', 'Quote', false, testNewOpp.Id);
            quoteForTest.CurrencyIsoCode = 'GBP';
            insert quoteForTest;
			SBQQ__Quote__c quoteForTest1 = [Select Id, Product_filter_type__c From SBQQ__Quote__c  where Id =: quoteForTest.Id];

            System.assertNotEquals(null, quoteForTest1.Id);
			System.assertEquals('Subscription', quoteForTest1.Product_filter_type__c);

            Test.stopTest();
        }
	}
}