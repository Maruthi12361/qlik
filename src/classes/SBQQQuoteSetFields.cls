/**
* Class: SBQQQuoteSetFields.
* Description: Class is used to populate some fields on managed SBQQ__Quote__c object.
*              It can be used instead of formula fields, validation rules, workflows.

* PCS Logic : Populate Partner Category Status – STP (PartnerCategoryStatusSTP__c) and
Partner Category Status – Second (PartnerCategoryStatusSecond__c)
Every Partner may have multiple Active PCS records. This logic cycles through the Active PCS records to select fields on the
Quote to decide which Account to use as the “partner.”The logic runs on every quote create and edit.
[SBQQQuoteSetFields handler class from Quote Trigger (runs on before insert and before update)]
For more details refer QCW-2934 changes start.
*
* Change log:
*   2017-02-06 - Roman Dovbush - Class created.
*   2017-02-15 - Emelie Öhrn   - added method populateRevenueTypeOnAmendQuotes.
*   2017-03-16 - Rodion Vakulovskyi   - added method for Invoicing_Address_Same_As_Billing QCW-1890
*   2017-05-17 MTM Set QCW-1633 OEM Subscription deals
*   2017-07-07 MTM  QCW-2606  Address validations
*   2017-08-01 Aslam QCW-2934 added new method populatePartnerCategoryStatus
*   2017-07-21 Oleksandr@4Front QCW-2396
*   2017-09-11	Nikhil QCW-3755 Added a check for Account RecordType
*   2017-10-08	Oleksandr@4Front QCW-4073
*   2017-10-11	MTM QCW-4073
*   2017-11-23  Bala E QCW-4465 - added logic for partner purchse revenue type in populatePartnerCategoryStatus()
*   2018-01-15 Viktor@4front - change list from secondPartnerListTemp to pcsListTemp (line 523)
*   2018-08-22 Anjuna BSL-619 - Added Documentation of PCS logic.
*	2018-10-01 Linus Löfberg BSL-895 - Added setting PartnerCategoryStatusSTP__c for MSP quotes on "Strategic"-program version.
*	2018-10-29 Linus Löfberg BSL-1085 - Optimize PCS code on Quoting and add test coverage
*/

public class SBQQQuoteSetFields {
    private static final String ADDRTYPE = 'Billing';
    private static final String MESSAGECONST = 'Invoicing Address Of Type Billing must be same as selected Billing address';
    //Cross region selling validation
    public static void MarketValidation(List<SBQQ__Quote__c> triggerNew, Map<ID, SBQQ__Quote__c> oldMap)
    {

        List<Id> accountIds = new List<Id>();
        for(SBQQ__Quote__c quote: triggerNew) {
            if(String.isNotEmpty(quote.SBQQ__Account__c) && String.isNotEmpty(quote.Sell_Through_Partner__c))
            {
                if(oldMap != null)
                {
                    SBQQ__Quote__c oldQuote = oldMap.get(quote.Id);
                    if(oldQuote.SBQQ__Account__c == quote.SBQQ__Account__c &&
                    oldQuote.Sell_Through_Partner__c == quote.Sell_Through_Partner__c)
                    {
                        break;
                    }
                }
                accountIds.add(quote.SBQQ__Account__c);
                accountIds.add(quote.Sell_Through_Partner__c);
            }
        }
        if (accountIds.size() ==  0)
        {
            return;
        }
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Territory_Country__c, Billing_Country_Code__r.Market_Name__c
        From Account
        Where Id In :accountIds and Billing_Country_Code__c != null]);

        for(SBQQ__Quote__c quote: triggerNew) {
            if(String.isNotEmpty(quote.SBQQ__Account__c) && String.isNotEmpty(quote.Sell_Through_Partner__c))
            {
                Account userAcc = accountMap.get(quote.SBQQ__Account__c);
                Account partnerAcc = accountMap.get(quote.Sell_Through_Partner__c);
                if(userAcc.Territory_Country__c != partnerAcc.Territory_Country__c &&
                userAcc.Billing_Country_Code__r.Market_Name__c != partnerAcc.Billing_Country_Code__r.Market_Name__c)
                {
                    quote.SBQQ__Account__c.AddError('Cross region selling not allowed. Choose an account from allowed selling countries');
                }
            }
        }
    }


    public static void invoiceAddressCheck(List<SBQQ__Quote__c> inputList, Map<ID, SBQQ__Quote__c> oldMap) {
        List<String> listOfAddresses= new List<String>();
        for(SBQQ__Quote__c quoteItem: inputList) {
            if(quoteItem.Invoicing_Address__c != oldMap.get(quoteItem.id).Invoicing_Address__c){
                if(!String.isEmpty(quoteItem.Invoicing_Address__c)){
                    listOfAddresses.add(quoteItem.Select_Billing_Address__c);
                    listOfAddresses.add(quoteItem.Invoicing_Address__c);
                }
            }
        }
        System.debug(listOfAddresses);
        Map<Id, Address__c> addressMap = new Map<Id, Address__c>([Select Id, Valid_Address__c, Address_Type__c From Address__c Where Id In : listOfAddresses]);
        System.debug(addressMap);
        for(SBQQ__Quote__c quoteItem: inputList) {
            if(addressMap.containsKey(quoteItem.Select_Billing_Address__c) && addressMap.containsKey(quoteItem.Invoicing_Address__c)) {
                if(addressMap.get(quoteItem.Invoicing_Address__c).Address_Type__c == ADDRTYPE && quoteItem.Select_Billing_Address__c != quoteItem.Invoicing_Address__c) {
                    quoteItem.addError(MESSAGECONST);
                }
            }
        }

    }

    //QCW-2396 Note: Price Book moved to CustomQuoteTriggerHandler
    public static void PopulateOnAmendQuotes(List<SBQQ__Quote__c> triggerNew){
        Map<Id, SBQQ__Quote__c> opportunitieIds = new Map<Id, SBQQ__Quote__c>();
        for(SBQQ__Quote__c quoteItem : triggerNew){
            if(quoteItem.SBQQ__Type__c == 'Amendment'){
                opportunitieIds.put(quoteItem.SBQQ__Opportunity2__c, quoteItem);
            }
        }
        for(Opportunity o: [Select  Id,
        Partner_Contact__c,
        //Second_Partner_Contact__c,
        Sell_Through_Partner__c,
        Short_Description__c,
        Second_Partner__c,
        Revenue_type__c
        FROM Opportunity
        WHERE Id In : opportunitieIds.keySet()])
        {
            SBQQ__Quote__c quote = opportunitieIds.get(o.Id);
            quote.Revenue_type__c = o.Revenue_Type__c;
            quote.Partner_Contact__c = o.Partner_Contact__c;
            //quote.Second_Partner_Contact__c = o.Second_Partner_Contact__c;
            quote.Second_Partner__c = o.Second_Partner__c;
            quote.Sell_Through_Partner__c = o.Sell_Through_Partner__c;
            quote.Short_Description__c = o.Short_Description__c;
        }
        //Ensure this is called only within method
        SetDefaultAddressesOnAmend(triggerNew);
    }

    private static void SetDefaultAddressesOnAmend(List<SBQQ__Quote__c> triggerNew)
    {
        List<Id> accountIds = new List<Id>();
        Map<Id, String> quoteAccountMap = new Map<Id, String>();
        for(SBQQ__Quote__c quoteItem : triggerNew){
            String acct;
            if(quoteItem.SBQQ__Type__c == 'Amendment'){
                if(ECustomsHelper.PartnerRevenueTypes.contains(quoteItem.Revenue_Type__c))
                acct = quoteItem.Sell_Through_Partner__c;
                else acct = quoteItem.SBQQ__Account__c;
                quoteAccountMap.put(quoteItem.Id,acct);
                accountIds.add(acct);
            }
        }
        Map<String, Account> mapOfAccounts = new Map<String, Account>();
        for (Account accountItem : [Select Id, (Select Id, Name,
        Account__c,
        Address_Type__c
        From Account_Addresses__r
        Where Default_Address__c = true
        AND Address_Type__c in ('Billing', 'Shipping')) from Account Where Id =: accountIds])
        {
            mapOfAccounts.put(accountItem.Id,accountItem);
        }

        for(SBQQ__Quote__c quote : triggerNew){
            List<Address__c> addresses = new List<Address__c>();
            String acct1 = quoteAccountMap.get(quote.Id);
            if(acct1 != null)
            {
                addresses = mapOfAccounts.get(acct1).Account_Addresses__r;
                for(Address__c address: addresses)
                {
                    if(address.Address_Type__c == 'Billing')
                    {
                        quote.Select_Billing_Address__c = address.Id;
                        quote.Invoicing_Address__c = address.Id;
                    }
                    if(address.Address_Type__c == 'Shipping') quote.Shipping_Address__c = address.Id;

                }
            }
        }
    }
    //QCW-2396 END
    /*
    Populate quote field Product_filter_type__c. This is used for OEM Subscription products filtering
    */
    public static void PopulateProductFilterOnQuotes(List<SBQQ__Quote__c> triggerNew){
        Map<Id, SBQQ__Quote__c> opportunitieIds = new Map<Id, SBQQ__Quote__c>();
        for(SBQQ__Quote__c q : triggerNew){
            if( (q.Revenue_type__c == 'OEM' || q.Revenue_type__c == 'OEM Recruitment') &&
            q.SBQQ__Opportunity2__c != null && q.Product_filter_type__c != 'Subscription' &&
            q.Subscription_Start_Date_Editable__c != null){
                opportunitieIds.put(q.SBQQ__Opportunity2__c, q);
            }
        }
        if(opportunitieIds.size() > 0)
        {
            //removing Subscription_Start_Date__c from query line 181
            List<Opportunity> opportunities = [Select Id, Solution_Type__c,Revenue_Type__c From Opportunity Where Id In : opportunitieIds.keySet()];
            for(Opportunity opp : opportunities){
                SBQQ__Quote__c quote = opportunitieIds.get(opp.Id);
                if((opp.Solution_Type__c != null &&  opp.Solution_Type__c.Contains('Subscription')))
                quote.Product_filter_type__c = 'Subscription';
            }
        }
    }

    public static void CheckAddresses(List<SBQQ__Quote__c> inputList, Map<ID, SBQQ__Quote__c> oldMap) {
        List<String> accts= new List<String>();
        Boolean CheckEndUser = false, CheckPartner = false;
        for(SBQQ__Quote__c quoteItem: inputList) {//
            if((quoteItem.SBQQ__Account__c != null && oldMap ==null) ||
            (oldMap != null && quoteItem.SBQQ__Account__c != oldMap.get(quoteItem.id).SBQQ__Account__c)){
                accts.add(quoteItem.SBQQ__Account__c);
                CheckEndUser = true;
            }
            if(ECustomsHelper.PartnerRevenueTypes.contains(quoteItem.Revenue_Type__c) &&
            (quoteItem.Sell_Through_Partner__c != null && oldMap ==null) ||
            (oldMap != null && quoteItem.Sell_Through_Partner__c != oldMap.get(quoteItem.id).Sell_Through_Partner__c)){
                accts.add(quoteItem.Sell_Through_Partner__c);
                CheckPartner = true;
            }
        }
        Map<String, List<Address__c>> mapOfAddresses = new Map<String, List<Address__c>>();
        for (Address__c itemAddress : [SELECT Id, Name,
        Account__c,
        Address_Type__c
        FROM Address__c
        WHERE Default_Address__c = true
        AND Address_Type__c in('Billing', 'Shipping')
        AND Account__c in :accts]) {
            List<Address__c> addressList;
            if(mapOfAddresses.containsKey(itemAddress.Account__c))
            addressList = mapOfAddresses.get(itemAddress.Account__c);
            else addressList = new List<Address__c>();
            addressList.Add(itemAddress);
            mapOfAddresses.put(itemAddress.Account__c, addressList);
        }
        if (!Test.isRunningTest()) {
            for(SBQQ__Quote__c quoteItem: inputList)
            {
                List<Address__c> accAddress =new List<Address__c>();
                if(quoteItem.SBQQ__Account__c != null && CheckEndUser)
                {
                    accAddress = mapOfAddresses.get(quoteItem.SBQQ__Account__c);
                    if(accAddress == null || accAddress.size() <2 )
                    quoteItem.SBQQ__Account__c.AddError('Default Billing/Shipping address missing on Account record. Please go back and fill Address info.');
                }
                if(quoteItem.Sell_Through_Partner__c != null && CheckPartner &&
                ECustomsHelper.PartnerRevenueTypes.contains(quoteItem.Revenue_Type__c))
                {
                    accAddress = mapOfAddresses.get(quoteItem.Sell_Through_Partner__c);
                    if(accAddress == null || accAddress.size() <2)
                    quoteItem.Sell_Through_Partner__c.AddError('Default Billing/Shipping address missing on Account record. Please go back and fill Address info.');
                }
            }
        }
    }

    public static void populatePartnerCategoryStatus(List<SBQQ__Quote__c> inputList) {
        /* As the first step find all active PCS records under second partner, sell through partner and Account of currently processing Quote */
        String errorText = Label.ErrorMessageForPCS;
        String errorText2 = Label.ErrorMessageForPCS2;
        String errorTextOEM = Label.ErrorMessageForOEM;

        Map<Id, List<Partner_Category_Status__c>> quoteSTPPCSMap = new Map<Id, List<Partner_Category_Status__c>>();
        Map<Id, List<Partner_Category_Status__c>> quoteSecondPartnerPCSMap = new Map<Id, List<Partner_Category_Status__c>>();
        Map<Id, List<Partner_Category_Status__c>> quoteAccountPCSMap = new Map<Id, List<Partner_Category_Status__c>>();

        Map<Id, Map<String, List<Partner_Category_Status__c>>> quoteSTPProgramVersionPCSMap = new Map<Id, Map<String, List<Partner_Category_Status__c>>>();
        Map<Id, Map<String, List<Partner_Category_Status__c>>> quoteSecondPartnerProgramVersionPCSMap = new Map<Id, Map<String, List<Partner_Category_Status__c>>>();
        Map<Id, Map<String, List<Partner_Category_Status__c>>> quoteAccountProgramVersionPCSMap = new Map<Id, Map<String, List<Partner_Category_Status__c>>>();

        Map<Id, String> accountRecordTypesMap = new Map<Id, String>();

        for (SBQQ__Quote__c thisQuote : inputList) {
            accountRecordTypesMap.put(thisQuote.SBQQ__Account__c, thisQuote.SBQQ__Account__r.RecordType.Name);

            if (thisQuote.Sell_Through_Partner__c != null && !quoteSTPPCSMap.containsKey(thisQuote.Sell_Through_Partner__c)) {
                quoteSTPPCSMap.put(thisQuote.Sell_Through_Partner__c, new List<Partner_Category_Status__c>());
                quoteSTPProgramVersionPCSMap.put(thisQuote.Sell_Through_Partner__c, new Map<String, List<Partner_Category_Status__c>>());
            }

            if (thisQuote.Second_Partner__c != null && !quoteSecondPartnerPCSMap.containsKey(thisQuote.Second_Partner__c)) {
                quoteSecondPartnerPCSMap.put(thisQuote.Second_Partner__c, new List<Partner_Category_Status__c>());
                quoteSecondPartnerProgramVersionPCSMap.put(thisQuote.Second_Partner__c, new Map<String, List<Partner_Category_Status__c>>());
            }

            if (thisQuote.SBQQ__Account__c != null && !quoteAccountPCSMap.containsKey(thisQuote.SBQQ__Account__c)) {
                quoteAccountPCSMap.put(thisQuote.SBQQ__Account__c, new List<Partner_Category_Status__c>());
                quoteAccountProgramVersionPCSMap.put(thisQuote.SBQQ__Account__c, new Map<String, List<Partner_Category_Status__c>>());
            }
        }

        for (Partner_Category_Status__c pcs : [SELECT Id, Program_Version__c, Partner_Category__c, Partner_Account__c FROM Partner_Category_Status__c WHERE Partner_Level_Status__c = 'Active' AND (Partner_Account__c IN :quoteSTPProgramVersionPCSMap.keySet() OR Partner_Account__c IN :quoteSecondPartnerProgramVersionPCSMap.keySet() OR Partner_Account__c IN :quoteAccountProgramVersionPCSMap.keySet())]) {
            if (quoteSTPPCSMap.containsKey(pcs.Partner_Account__c)) {
                quoteSTPPCSMap.get(pcs.Partner_Account__c).add(pcs);
                if (quoteSTPProgramVersionPCSMap.get(pcs.Partner_Account__c).containsKey(pcs.Program_Version__c)) {
                    quoteSTPProgramVersionPCSMap.get(pcs.Partner_Account__c).get(pcs.Program_Version__c).add(pcs);
                } else {
                    List<Partner_Category_Status__c> initialPCSList = new List<Partner_Category_Status__c>();
                    initialPCSList.add(pcs);
                    quoteSTPProgramVersionPCSMap.get(pcs.Partner_Account__c).put(pcs.Program_Version__c, initialPCSList);
                }
            }

            if (quoteSecondPartnerPCSMap.containsKey(pcs.Partner_Account__c)) {
                quoteSecondPartnerPCSMap.get(pcs.Partner_Account__c).add(pcs);
                if (quoteSecondPartnerProgramVersionPCSMap.get(pcs.Partner_Account__c).containsKey(pcs.Program_Version__c)) {
                    quoteSecondPartnerProgramVersionPCSMap.get(pcs.Partner_Account__c).get(pcs.Program_Version__c).add(pcs);
                } else {
                    List<Partner_Category_Status__c> initialPCSList = new List<Partner_Category_Status__c>();
                    initialPCSList.add(pcs);
                    quoteSecondPartnerProgramVersionPCSMap.get(pcs.Partner_Account__c).put(pcs.Program_Version__c, initialPCSList);
                }
            }

            if (quoteAccountPCSMap.containsKey(pcs.Partner_Account__c)) {
                quoteAccountPCSMap.get(pcs.Partner_Account__c).add(pcs);
                if (quoteAccountProgramVersionPCSMap.get(pcs.Partner_Account__c).containsKey(pcs.Program_Version__c)) {
                    quoteAccountProgramVersionPCSMap.get(pcs.Partner_Account__c).get(pcs.Program_Version__c).add(pcs);
                } else {
                    List<Partner_Category_Status__c> initialPCSList = new List<Partner_Category_Status__c>();
                    initialPCSList.add(pcs);
                    quoteAccountProgramVersionPCSMap.get(pcs.Partner_Account__c).put(pcs.Program_Version__c, initialPCSList);
                }
            }
        }

        /* Loop through the Quote records */
        for (SBQQ__Quote__c thisQuote : inputList) {

            //BSL-2932 - Skipping pcs defeaults for fulfillment deals
            if(thisQuote.Revenue_Type__c == 'Fulfillment') {
                continue;
            }

            /* Recordtype = Distributor */
            if (thisQuote.Revenue_Type__c == 'Distributor') {

                // First Partner //
                /* Look at the PCS records comes under Sell Through Partner field on Quote. If one exists with program version = Distributor,
                select it for PCS – STP. If there is none exist or more than one exist, throw an error. */

                if (thisQuote.Sell_Through_Partner__c != null) {
                    if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Distributor') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Distributor').size() == 1) {
                        thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Distributor')[0].Id;
                    } else {
                        //throw error - none or multiple matches found
                        thisQuote.AddError(errorText);
                    }
                }

                // First Partner //
                /* To find PCS-second, loop through PCS records under Second Partner account of the quote.
                1) check for record with program version= QPP and Partner Category = Resell
                2) if one found assign to PartnerCategoryStatusSecond__c field.
                3) If more than one found, throw an error. If none found, go to next step
                4) if none found search PCS record with program version=Partner Program 4.1. Repeat step 2 and 3
                5) if none found search PCS record with program version=Master Reseller, Repeat step 2 and 3
                6) if none found, throw an error */

                // Second Partner //
                if (thisQuote.Second_Partner__c != null) {
                    Integer counter = 0;
                    if (quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).containsKey('QPP')) {
                        for (Partner_Category_Status__c secondPartnerPCS : quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('QPP')) {
                            if (secondPartnerPCS.Partner_Category__c.equals('Resell')) {
                                thisQuote.PartnerCategoryStatusSecond__c = secondPartnerPCS.Id;
                                counter++;
                                if (counter > 1) {
                                    //throw error - multiple records meeting criteria
                                    thisQuote.AddError(errorText2);
                                }
                            }
                        }
                    }

                    if (counter == 0) {
                        if (quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).containsKey('Partner Program 4.1') && quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Partner Program 4.1').size() == 1) {
                            thisQuote.PartnerCategoryStatusSecond__c = quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Partner Program 4.1')[0].Id;
                        } else if (quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).containsKey('Master Reseller') && quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Master Reseller').size() == 1) {
                            thisQuote.PartnerCategoryStatusSecond__c = quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Master Reseller')[0].Id;
                        } else {
                            //throw error - none or multiple match criteria
                            thisQuote.AddError(errorText2);
                        }
                    }
                } // Second Partner //
            } // End of Distributor

            /*  Recordtype = MSP
            Look at the PCS records comes under Account field on Quote. If one found with program version=QPP or program version=Strategic, and partner category=MSP, select it as PCS-STP
            If there is none exist or more than one exist, throw an error. */

            else if (thisQuote.Revenue_Type__c == 'MSP') {
                if (quoteAccountPCSMap.get(thisQuote.SBQQ__Account__c).isEmpty()) {
                    //QCW-3755
                    if (accountRecordTypesMap.containsKey(thisQuote.SBQQ__Account__c) && !accountRecordTypesMap.get(thisQuote.SBQQ__Account__c).equals('Partner Account')) {
                        thisQuote.AddError(System.Label.MSP_ErrorMessage_AccountCheck);
                    } else {
                        //throw error
                        thisQuote.AddError(errorText);
                    }
                } else {
                    Integer counter = 0;
                    for (Partner_Category_Status__c accountPCS : quoteAccountPCSMap.get(thisQuote.SBQQ__Account__c)) {
                        if ((accountPCS.Program_Version__c == 'QPP' || accountPCS.Program_Version__c == 'Strategic') && accountPCS.Partner_Category__c == 'MSP') {
                            thisQuote.PartnerCategoryStatusSTP__c = accountPCS.Id;
                            counter++;
                            if (counter > 1) {
                                //throw error
                                thisQuote.AddError(errorText);
                            }
                        }
                    }
                }
            } // end of MSP

            /*  Recordtype= OEM or OEM Recruitment
            loop through the PCS records comes under Account field on Quote. If one found with program version=OEM, select it as PCS-STP
            If there is none exist or more than one exist, throw an error. */

            else if (thisQuote.Revenue_Type__c == 'OEM' || thisQuote.Revenue_Type__c == 'OEM Recruitment') {
                if (thisQuote.SBQQ__Account__c != null) {
                    Integer counter = 0;
                    if (quoteAccountProgramVersionPCSMap.get(thisQuote.SBQQ__Account__c).containsKey('OEM')) {
                        for (Partner_Category_Status__c pcsOEM : quoteAccountProgramVersionPCSMap.get(thisQuote.SBQQ__Account__c).get('OEM')) {
                            thisQuote.PartnerCategoryStatusSTP__c = pcsOEM.Id;
                            counter++;
                            if (counter > 1) { //no record or multiple records meet criteria- throw error
                                thisQuote.AddError(errorTextOEM);
                            }
                        }
                    }
                }
            }  // end of OEM

            /*  Recordtype= Partner Purchase
            Look at the PCS records comes under Sell Through Partner field on Quote.
            1) check for record with program version= Distributor
            2) if one found assign to PartnerCategoryStatusSecond__c field.
            3) else check for PCS record with program version=QPP, Repeat step 2
            4)else check for PCS record with program version=Master Reseller, Repeat step 2
            5)else check for PCS record with program version=Technology Partner, Repeat step 2
            6)else check for PCS record with program version=Global SI, Repeat step 2
            7)else check for PCS record with program version=Partner Program 4.1, Repeat step 2
            8)else if none found, throw an error */

            else if (thisQuote.Revenue_Type__c == 'Partner Purchase') {
                // First Partner //
                if (thisQuote.Sell_Through_Partner__c != null) {
                    if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Distributor') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Distributor').size() == 1) {
                        thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Distributor')[0].Id;
                    } else if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('QPP') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('QPP').size() == 1) {
                        thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('QPP')[0].Id;
                    } else if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Master Reseller') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Master Reseller').size() == 1) {
                        thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Master Reseller')[0].Id;
                    } else if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Technology Partner') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Technology Partner').size() == 1) {
                        thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Technology Partner')[0].Id;
                    } else if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Global SI') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Global SI').size() == 1) {
                        thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Global SI')[0].Id;
                    } else if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Partner Program 4.1') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Partner Program 4.1').size() == 1) {
                        thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Partner Program 4.1')[0].Id;
                    } else {
                        //throw error
                        thisQuote.AddError(errorText);
                    }
                }
            }  // end of Partner Purchase

            /* For all other Record Types */
            /* To find PCS-STP, loop through PCS records under Sell Through Partner of the quote.
            1) check for record with program version= QPP and Partner Category = Resell
            2) if one found assign to PartnerCategoryStatusSTP__c field.
            3) If more than one found, throw an error. If none found, go to next step
            4) if none found search PCS record with program version=Partner Program 4.1. Repeat step 2 and 3
            5) if none found search PCS record with program version=Master Reseller Repeat step 2 and 3
            6) if none found, throw an error */

            else {
                // First Partner //
                if (thisQuote.Sell_Through_Partner__c != null) {
                    Integer counter = 0;
                    if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('QPP')) {
                        for (Partner_Category_Status__c stpPCS : quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('QPP')) {
                            if (stpPCS.Partner_Category__c == 'Resell') {
                                thisQuote.PartnerCategoryStatusSTP__c = stpPCS.Id;
                                counter++;
                                if (counter > 1) {
                                    //throw error
                                    thisQuote.AddError(errorText);
                                }
                            }
                        }
                    }
                    if (counter == 0) {
                        if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Partner Program 4.1') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Partner Program 4.1').size() == 1) {
                            thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Partner Program 4.1')[0].id;
                        } else if (quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).containsKey('Master Reseller') && quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Master Reseller').size() == 1) {
                            thisQuote.PartnerCategoryStatusSTP__c = quoteSTPProgramVersionPCSMap.get(thisQuote.Sell_Through_Partner__c).get('Master Reseller')[0].id;
                        } else {
                            //throw error
                            thisQuote.AddError(errorText);
                        }
                    }
                }
                // First Partner else ends//

                /*To find PCS-Second Partner, loop through PCS records under Second Partner Account of the quote.
                1) check for record with program version= QPP and Partner Category = Resell
                2) if one found assign to PartnerCategoryStatusSecond__c field.
                3) If more than one found, throw an error. If none found, go to next step
                4) if none found search PCS record with program version=Partner Program 4.1. Repeat step 2 and 3
                5) if none found search PCS record with program version=Master Reseller Repeat step 2 and 3
                6) if none found, throw an error */

                if (thisQuote.Second_Partner__c != null) {
                    // Second Partner //
                    Integer counter = 0;
                    if (quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).containsKey('QPP')) {
                        for (Partner_Category_Status__c secondPartnerPCS : quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('QPP')) {
                            if (secondPartnerPCS.Partner_Category__c == 'Resell') {
                                thisQuote.PartnerCategoryStatusSecond__c = secondPartnerPCS.Id;
                                counter++;
                                if (counter > 1) {
                                    //throw error
                                    thisQuote.AddError(errorText2);
                                }
                            }
                        }
                    }
                    if (counter == 0) {
                        if (quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).containsKey('Partner Program 4.1') && quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Partner Program 4.1').size() == 1) {
                            thisQuote.PartnerCategoryStatusSecond__c = quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Partner Program 4.1')[0].Id;
                        } else if (quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).containsKey('Master Reseller') && quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Master Reseller').size() == 1) {
                            thisQuote.PartnerCategoryStatusSecond__c = quoteSecondPartnerProgramVersionPCSMap.get(thisQuote.Second_Partner__c).get('Master Reseller')[0].Id;
                        } else {
                            //throw error
                            thisQuote.AddError(errorText2);
                        }
                    }
                } // Second partner else ends //
            }
        }
    }

    public static void HandleQuoteSubscriptionFields(List<SBQQ__Quote__c> triggerNew) {
        List<SBQQ__Quote__c> listUpd = new List<SBQQ__Quote__c>();
        List<Id> listOppIds = new List<Id>();
        for (SBQQ__Quote__c qt : triggerNew) {
            if (qt.Revenue_Type__c == 'MSP' && qt.SBQQ__Type__c != 'Amendment') {
                if (qt.SBQQ__Status__c == 'Order Placed' ||  qt.SBQQ__Status__c == 'Accepted By Customer') {
                    qt.Subscription_Start_Date_Editable__c = Date.today().toStartOfMonth();
                } else {
                    listOppIds.add(qt.SBQQ__Opportunity2__c);
                }
            }
        }

        //Set subscription Start date for MSP
        if (listOppIds.size() > 0) {
            Map<Id,Opportunity> mapOpps = new Map<Id,Opportunity>([SELECT Id, Revenue_Type__c, CloseDate FROM Opportunity WHERE Id IN: listOppIds]);
            for (SBQQ__Quote__c q: triggerNew) {
                Opportunity relatedOpp = mapOpps.get(q.SBQQ__Opportunity2__c);
                q.Subscription_Start_Date_Editable__c = relatedOpp.CloseDate.toStartOfMonth();
            }
        }
    }

    public static void PopulateSubsTerm(List<SBQQ__Quote__c> triggerNew) {
        for (SBQQ__Quote__c q : triggerNew) {
            //String value = String.valueOf(q.No_of_Subscription_Units__c);
            //q.SBQQ__SubscriptionTerm__c = Integer.valueOf(Decimal.valueOf(value));
            if (!String.isEmpty(q.No_of_Subscription_Units__c)) {
                q.SBQQ__SubscriptionTerm__c = Decimal.valueOf(q.No_of_Subscription_Units__c);
            } else {
                q.SBQQ__SubscriptionTerm__c = null;
            }
        }
    }
}