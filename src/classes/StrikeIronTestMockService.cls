/**
	Rodion Vakulvoskyi : 28.03.2017 : MockTestService for QtStrikeironCom generated class
**/
@isTest
public class StrikeIronTestMockService implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
			QtStrikeironCom.TestAdvancedVerifyResponse_element respElement = new QtStrikeironCom.TestAdvancedVerifyResponse_element();
   response.put('response_x', respElement); 
   }
}