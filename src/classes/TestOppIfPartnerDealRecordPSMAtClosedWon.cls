/*
Name:  TestOppIfPartnerDealRecordPSMAtClosedWon.cls

Purpose:
-------
CR# 6188 - Cleanup Opportunity Workflows
======================================================

History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     CCE         2012-10-30  Initial development.
1.2     MTM         2013-11-28          CR# 10154 Add new Sales Credit Information fields
    Test class for OpportunityIfPartnerDealRecordPSMAtClosedWon.trigger
    (Records the Qonnect Manager ID at Closed Won
    There was a Workflow Rule called "if partner deal record PSM at Closed Won" which this trigger intends
    to duplicate so we can deactivate the workflow and thus free up some Opp Object references as we
    have run out.)
        MHG         2014-06-18  Fixed broken test case due to new forecasting
		17.03.2017 : Rodion Vakulvsokyi 
*/
@isTest
private class TestOppIfPartnerDealRecordPSMAtClosedWon {

    static void InsertPriceBookEntry()
    {
        Pricebook2 nspb = new Pricebook2();
        nspb.Name = 'NS Pricebook';
        insert nspb;

        Id standardPriceBookId = Test.getStandardPricebookId();

        List<Product2> products = new List<Product2>();
        Product2 p1 = new Product2(Name='Licenses', isActive=true, Family='Licenses', Deferred_Revenue_Account__c = '26000 Deferred Revenue', Income_Account__c = '26000 Deferred Revenue');
        products.add(p1);
        insert products;

        
        
        PriceBook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];
            
        PricebookEntry pbe1 = QTTestUtils.createMockPricebookEntry(standardPriceBookId, pb2NS, p1, 'GBP');
        
    }
    static testMethod void TestWithSellThroughPartner() {
        System.debug('TestWithSellThroughPartner: Starting');
     Semaphores.TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = true;

                 Profile p = [select id from profile where name='System Administrator']; 
          User us = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');


         System.runAs(us) {

        InsertPriceBookEntry();
        QTTestUtils.GlobalSetup();

   
         Subsidiary__c subs = new Subsidiary__c(
            Legal_Entity_Name__c = 'test',
            Netsuite_Id__c = 'test'
            );
            insert subs;
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
            Country_Name__c = 'USA',
            Subsidiary__c = subs.id
        );
        insert QTComp;

        ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
                
        
        
            //Create test Account
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account();
            testAcnt.name='Test1 Account1';
            testAcnt.OwnerId = us.Id;
            testAcnt.Navision_Customer_Number__c = '12345';
            testAcnt.Legal_Approval_Status__c = 'Legal Approval Granted';           
            testAcnt.Qonnect_Manager__c = us.id;            
            
            testAcnt.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;         
            testAcnt.Billing_Country_Code__c = QTComp.Id;
            testAcnt.BillingStreet = '417';
            testAcnt.BillingState ='CA';
            testAcnt.BillingPostalCode = '94104';
            testAcnt.BillingCountry = 'United States';
            testAcnt.BillingCity = 'SanFrancisco';
            
            testAcnt.Shipping_Country_Code__c = QTComp.Id;
            testAcnt.ShippingStreet = '417';
            testAcnt.ShippingState ='CA';
            testAcnt.ShippingPostalCode = '94104';
            testAcnt.ShippingCountry ='United States';
            testAcnt.ShippingCity = 'SanFrancisco';
            testAcnt.Account_EIS_New__c = us.id;
            Account TestPartnerAccount = new Account(
            Name = 'My Partner',
            Navision_Status__c = 'Partner',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            Billing_Country_Code__c = QTComp.Id                         
            );
            
            List<Account> AccToInsert = new List<Account>();
            AccToInsert.Add(testAcnt);
            AccToInsert.Add(TestPartnerAccount);
            insert AccToInsert;
            
            Contact PartnerContact = new Contact(
                FirstName = 'Mr Partner',
                LastName = 'PP',
                OwnerId=us.Id, 
                AccountId = TestPartnerAccount.Id
            );
            insert PartnerContact;
        
            //Create test Contact
            Contact testContact = new Contact(FirstName='Test1', 
                LastName='Contact1', 
                AccountId=testAcnt.Id, 
                OwnerId=us.Id, 
                LeadSource='WEB - Web Activity');
            insert testContact;
            
          RecordType recordType  = [SELECT Id, Name from  RecordType where sObjecttype = 'Opportunity' and DeveloperName = 'OEM_Recruitment_CCS'];
      //Create test Opportunity
          Opportunity testNewOpp = New Opportunity (
            Short_Description__c = 'TestOpp - delete me',
            Name = 'TestOpp - Delete me',
            Type = 'New Customer',
            Revenue_Type__c = 'Reseller',
            CloseDate = Date.today(),
            StageName = 'Goal Confirmed',
            ForecastCategoryName = 'Omitted',   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
            RecordTypeId = recordType.Id,//'01220000000DugI',  // CCS - Customer Centric Selling
            CurrencyIsoCode = 'GBP',
            AccountId = testContact.AccountId,
            Sell_Through_Partner__c = testAcnt.Id,
            Licensing_model__c = 'Other',
            Yearly_revenue_projections__c = '< $25K',
            Software_Segment__c = 'Other',
            Target_Industry__c = 'Other',
            Deployment_model__c = 'Other',
            Solution_Name__c = 'Other',
            No_of_customers__c = '1-100',
            Included_Products__c = 'Qlik Sense',
            Signature_Type__c = 'Digital Signature',
            Partner_Contact__c = PartnerContact.Id
          );                               
            Test.startTest();
            insert testNewOpp;
    
            
            OpportunityLineItem LineItem = new OpportunityLineItem();
            
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;
            
            insert LineItem;    
                    
           testNewOpp.Short_Description__c = 'TestOpp - Del';
        testNewOpp.StageName = 'Closed Won';
            testNewOpp.Finance_Approved__c = true;        
            
           
            //update testNewOpp;
                                
            test.stopTest();
            
            Opportunity UpdatedOpp = [select Id, Active_EIS__c, Referring_Qonnect_Manager_at_Closed_Won__c, Qonnect_Manager_at_close__c, Qonnect_Manager_Name_at_Closed_Won__c, Referring_Qonnect_Manager_Closed_Won_ID__c from Opportunity where Id = :testNewOpp.Id];
            //Commented because of new Validation rule
            //System.assertEquals(us.Id, UpdatedOpp.Qonnect_Manager_at_close__c);
            //CR# 8969
            //System.assertEquals(UpdatedOpp.Active_EIS__c, us.Id);
            //System.assertEquals('QonMngr QonMngr', UpdatedOpp.Qonnect_Manager_Name_at_Closed_Won__c);
            //Below 2 values are never set in trigger?
            //System.assertEquals('QonMngr QonMngr', UpdatedOpp.Referring_Qonnect_Manager_at_Closed_Won__c);
            //System.assertEquals(QMuser.Id, UpdatedOpp.Referring_Qonnect_Manager_Closed_Won_ID__c);

        }
        Semaphores.TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = false;
        System.debug('TestWithSellThroughPartner: Finishing');                  
    }
    
    static testMethod void TestWithoutSellThroughPartner() {
        System.debug('TestWithoutSellThroughPartner: Starting');
        Semaphores.TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = true;

                       Profile p = [select id from profile where name='System Administrator']; 
          User us = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');


         System.runAs(us) {
        InsertPriceBookEntry();
        QTTestUtils.GlobalSetup();
          Subsidiary__c subs = new Subsidiary__c(
            Legal_Entity_Name__c = 'test',
            Netsuite_Id__c = 'test'
            );
            insert subs;
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'USA',
            QlikTech_Company_Name__c = 'QlikTech Inc',
            Country_Name__c = 'USA',
            Subsidiary__c = subs.id

        );
        insert QTComp;
        
        Id RT = [select Id from RecordType where sObjectType = 'Account' and Name = 'Partner Account' limit 1].Id;
        
        ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
                
       

            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account();
            testAcnt.name='Test1 Account1';
            testAcnt.OwnerId = us.Id;
            testAcnt.Navision_Customer_Number__c = '12345';
            testAcnt.Legal_Approval_Status__c = 'Legal Approval Granted';           
            testAcnt.Qonnect_Manager__c = us.id;            
            testAcnt.RecordTypeId = RT; //Partner Account       
            
            testAcnt.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;         
            testAcnt.Billing_Country_Code__c = QTComp.Id;
            testAcnt.BillingStreet = '417';
            testAcnt.BillingState ='CA';
            testAcnt.BillingPostalCode = '94104';
            testAcnt.BillingCountry ='United States';
            testAcnt.BillingCity = 'SanFrancisco';
            
            testAcnt.Shipping_Country_Code__c = QTComp.Id;
            testAcnt.ShippingStreet = '417';
            testAcnt.ShippingState ='CA';
            testAcnt.ShippingPostalCode = '94104';
            testAcnt.ShippingCountry ='United States';
            testAcnt.ShippingCity = 'SanFrancisco';                     
            insert testAcnt;
            
            //Create test Contact
            Contact testContact = new Contact(FirstName='Test1', 
                LastName='Contact1',
                 AccountId=testAcnt.Id, 
                 OwnerId=us.Id, 
                 LeadSource='WEB - Web Activity');
            insert testContact;
            
            //Create test Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp - delete me',
                Name = 'TestOpp - Delete me',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Goal Confirmed',
                ForecastCategoryName = 'Omitted',   //Added to exclude from Opportunity_ManageForecastProducts.triggerinsert Opp;
                RecordTypeId = [Select id From Recordtype where DeveloperName = 'Sales_QCCS'].id,   // CCS - Customer Centric Selling
                CurrencyIsoCode = 'GBP',
                AccountId = testContact.AccountId,
                Referring_Contact__c = testContact.Id,
                Included_Products__c = 'Qlik Sense',
                Signature_Type__c = 'Digital Signature',
                Opportunity_Origin__c = 'SI Driven Solution',
                

            Finance_Approved__c = true  

            );                               
              Test.startTest();
            insert testNewOpp;

            
            OpportunityLineItem LineItem = new OpportunityLineItem();
            
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;
            
            insert LineItem;    
                    
            //testNewOpp.Short_Description__c = 'TestOpp - Del';
            //testNewOpp.StageName = 'Closed Won';
            //testNewOpp.Finance_Approved__c = true;        
            
        
            
           // update testNewOpp;
                                
            test.stopTest(); 
            
            Opportunity UpdatedOpp = [select Id, Qonnect_Manager_at_close__c, Qonnect_Manager_Name_at_Closed_Won__c from Opportunity where Id = :testNewOpp.Id];
            //System.assertEquals(QMuser.Id, UpdatedOpp.Qonnect_Manager_at_close__c);
           // System.assertEquals('QonMngr QonMngr', UpdatedOpp.Qonnect_Manager_Name_at_Closed_Won__c);
        }
        Semaphores.TestOppIfPartnerDealRecordPSMAtClosedWonHasRun = false;
        System.debug('TestWithoutSellThroughPartner: Finishing');                   
    }
    
}