/******************************************************
* Various testing utility methods
*
* Change log:
*
* 2017-11-09  ext_vos CR#CHG0031444 Add required fields for pse__Proj__c creation.
* 2019-04-05  ext_bjd ITRM-225 added additional fix for TestAssignProjectManagerToApprover
********************************************************/
public class TestUtil
{
    public static pse__Region__c testRegion;
    public static pse__Practice__c testPractice;
    public static pse__Grp__c testGroup;
    public static Contact testResource;
    public static pse__Proj__c testProject;
    public static pse__Assignment__c testAssignment;
    public static Date testAssignmentStartDate;
    public static Date testAssignmentEndDate;

    // creates a region, project, resource, and assignment to the project
    public static void createBasicTestData()
    {
        createTestData(true, false, false, false, false, false, false, false, false, true, false, false, true);

        testAssignmentStartDate = Date.newInstance(2009, 7, 5); // sun
        testAssignmentEndDate = Date.newInstance(2009, 7, 11); // sat

        testAssignment = createAssignment(testAssignmentStartDate, testAssignmentEndDate);
    }

    public static pse__Assignment__c createAssignment(Date startDate, Date endDate)
    {
        pse__Schedule__c s1 = new pse__Schedule__c();
        s1.pse__Start_Date__c = startDate;
        s1.pse__End_Date__c = endDate;
        s1.pse__Monday_Hours__c = 8;
        s1.pse__Tuesday_Hours__c = 8;
        s1.pse__Wednesday_Hours__c = 8;
        s1.pse__Thursday_Hours__c = 8;
        s1.pse__Friday_Hours__c = 8;
        insert s1;

        if (TestUtil.testRegion != null)
        {
            addTestRegionStaffingPermission();
        }

        pse__Assignment__c a1 = new pse__Assignment__c();
        a1.pse__Schedule__c = s1.Id;
        a1.pse__Resource__c = TestUtil.testResource.Id;
        a1.pse__Bill_Rate__c = 200;
        a1.pse__Cost_Rate_Amount__c = 100;
        a1.pse__Project__c = TestUtil.testProject.Id;
        insert a1;

        return a1;
    }

    public static void addTestRegionStaffingPermission()
    {
        insert new pse__Permission_Control__c(pse__User__c=UserInfo.getUserId(), pse__Region__c=TestUtil.testRegion.Id, pse__Staffing__c=true);
    }

    public static void createTestData(Boolean createRegion, Boolean createParentRegion,
       Boolean createPractice, Boolean createParentPractice,
       Boolean createGroup, Boolean createParentGroup,
       Boolean createProject, Boolean createParentProject,
       Boolean createResource)
    {
        createTestData(createRegion, createParentRegion, false,
           createPractice, createParentPractice, false,
           createGroup, createParentGroup, false,
           createProject, createParentProject, false,
           createResource);
    }

    public static void createTestData(Boolean createRegion, Boolean createParentRegion, Boolean createTopRegion,
       Boolean createPractice, Boolean createParentPractice, Boolean createTopPractice,
       Boolean createGroup, Boolean createParentGroup, Boolean createTopGroup,
       Boolean createProject, Boolean createParentProject, Boolean createTopProject,
       Boolean createResource)
    {
        if (createRegion)
        {
            //RegionManager.ENFORCE_MAX_TOP_LEVEL_NODES = false;

            List<pse__Region__c> rList = new List<pse__Region__c>();


            testRegion = new pse__Region__c(Name='test');
            rList.add(testRegion);
            insert rList;

        }

        if (createProject)
        {
            List<pse__Proj__c> pList = new List<pse__Proj__c>();

            testProject = new pse__Proj__c(  Name='test',
                                        pse__Is_Active__c=true,
                                        //pse__Is_Billable__c=true,
                                        pse__Allow_Timecards_Without_Assignment__c=true,
                                        pse__Allow_Expenses_Without_Assignment__c=true,
                                        CurrencyIsoCode = 'USD',
                                        pse__Stage__c = 'In Progress',
                                        pse__Start_Date__c = Date.today().addDays(-10),
                                        pse__End_Date__c = Date.today().addDays(30),
                                        Key_Engagement_Features__c = 'QlikView',
                                        Purpose_of_Engagement__c = 'Test',
                                        Sales_Classification__c = 'Internal',
                                        Customer_Critial_Success_Factors__c = 'Test',
                                        Invoicing_Type__c = 'Deferred');
            if (testRegion != null)
                testProject.pse__Region__c = testRegion.Id;
            if (testPractice != null)
                testProject.pse__Practice__c = testPractice.Id;
            if (testGroup != null)
                testProject.pse__Group__c = testGroup.Id;

            pList.add(testProject);
            insert pList;

            // 10/26/10 - JF - Custom validation requires approved budget before marking billable
            // Add Approved budgets and update projects to Billable

            pse__Budget__c testBudget = new pse__Budget__c(Name = 'test budget', pse__Amount__c = 120,
                        pse__Effective_Date__c = Date.today(), pse__Status__c = 'Approved',
                        pse__Project__c = testProject.Id, CurrencyIsoCode = 'USD');
            insert testBudget;
            testProject.pse__Is_Billable__c = true;
            update testProject;
        }

        if (createResource)
        {
            createTestResource();
        }
    }

    public static void createTestResource()
    {
        List<Contact> cList = [select Id from Contact where pse__Salesforce_User__c=:UserInfo.getUserId()];
        for (Contact c : cList)
        {
            c.pse__Salesforce_User__c = null;
        }
        if (!cList.isEmpty())
            update cList;

        pse__Work_Calendar__c wc = new pse__Work_Calendar__c(Name='test');
        insert wc;

        testResource = new Contact(LastName='test',pse__Resource_Role__c='Consultant',pse__Salesforce_User__c=UserInfo.getUserId(),
            pse__Is_Resource__c=true,pse__Is_Resource_Active__c=true, pse__Work_Calendar__c=wc.Id, CurrencyIsoCode = 'USD');
        if (testRegion != null)
            testResource.pse__Region__c = testRegion.Id;
        if (testPractice != null)
            testResource.pse__Practice__c = testPractice.Id;
        if (testGroup != null)
            testResource.pse__Group__c = testGroup.Id;

        insert testResource;
    }
}
