/****************************************************************
*
*  TestUploadSoWCntr 
*  03.05.2017 : Rodion Vakulvsokyi : QCW-2228 unit test
*  29.06.2017 : Aslam Kamal : QCW-2228 updated test
*  02.09.2017 : Srinivasan PR- fix for query error
*  02.04.2019 : UIN BFC-2092  unit test update and test class cleanup on April 3rd 2019
*****************************************************************/
@isTest
public class TestUploadSoWCntr  {
    private static final String PARTNERACC = '01220000000DOFzAAO';
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';

    @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id From User where id =: UserInfo.getUserId()];

        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        update testAccount;

        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        //QlikTech company
        createQlikTech('France', 'FR', testSubs.id);
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c where Country_Name__c = 'France' limit 1];
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType 
                                     where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
        insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        insert testContact;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
        insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
        insert pbEntryTest;

    }

    private static testmethod void processTest1() {

        //Query data from setup
        RecordType rType = [Select Id From RecordType Where developerName = 'Quote'];
        Contact testContact  = [Select Id From Contact ];
        Account testAccount = [Select Id From Account where recordtypeid = :AccRecordTypeId_EndUserAccount];
        

        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
            quoteForTest.SBQQ__Primary__c = true;
            quoteForTest.Sector__c='Healthcare';
            quoteForTest.Industry__c='Healthcare';
            quoteForTest.Function__c='Healthcare';
            quoteForTest.Solution_Area__c='Healthcare - Emergency Medicine';
           Test.startTest();
           insert quoteForTest;
            
           update quoteForTest;
        
        Test.stopTest();

        Opportunity createdOpp = [Select id, SBQQ__PrimaryQuote__c, SBQQ__PrimaryQuote__r.SBQQ__Status__c From Opportunity];

        ApexPages.StandardController testStandardController = new ApexPages.StandardController(createdOpp);
        UploadSoWCntr testContr = new UploadSoWCntr(testStandardController);
        System.assertEquals(false, testContr.primaryQuoteStatus);
          
    }

    public static testmethod void processTest2() {
        //Query data from setup
        RecordType rType = [Select Id From RecordType Where developerName = 'Quote'];
        Contact testContact  = [Select Id From Contact ];
        Account testAccount = [Select Id From Account where recordtypeid = :AccRecordTypeId_EndUserAccount];

        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
            quoteForTest.SBQQ__Primary__c = true;
            quoteForTest.Sector__c='Healthcare';
            quoteForTest.Industry__c='Healthcare';
            quoteForTest.Function__c='Healthcare';
            quoteForTest.Solution_Area__c='Healthcare - Emergency Medicine';
           Test.startTest();
           insert quoteForTest;
           quoteForTest.SBQQ__Status__c = 'Approved';
           update quoteForTest;
        
        Test.stopTest();

        Opportunity createdOpp = [Select id, SBQQ__PrimaryQuote__c, SBQQ__PrimaryQuote__r.SBQQ__Status__c From Opportunity];

        ApexPages.StandardController testStandardController = new ApexPages.StandardController(createdOpp);
        UploadSoWCntr testContr = new UploadSoWCntr(testStandardController);
        System.assertEquals(true, testContr.primaryQuoteStatus);
    }
	
	public static testmethod void processTest3() {
       	//Query data from setup
        RecordType rType = [Select Id From RecordType Where developerName = 'Quote'];
        Contact testContact  = [Select Id From Contact ];
        Account testAccount = [Select Id From Account where recordtypeid = :AccRecordTypeId_EndUserAccount];
       
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
            quoteForTest.SBQQ__Primary__c = true;
            quoteForTest.Sector__c='Healthcare';
            quoteForTest.Industry__c='Healthcare';
            quoteForTest.Function__c='Healthcare';
            quoteForTest.Solution_Area__c='Healthcare - Emergency Medicine';

			Test.startTest();
				insert quoteForTest;
			Test.stopTest();
        Opportunity createdOpp = [Select id, SBQQ__PrimaryQuote__c, SBQQ__PrimaryQuote__r.SBQQ__Status__c From Opportunity];
        createdOpp.sow_Status__c = 'Finance Approved';
        update createdOpp;
        Opportunity createdOpp1 = [Select id, SBQQ__PrimaryQuote__c, SBQQ__PrimaryQuote__r.SBQQ__Status__c,sow_Status__c  From Opportunity];
        system.debug('1111111111'+createdOpp1);
        ApexPages.StandardController testStandardController = new ApexPages.StandardController(createdOpp1);
        UploadSoWCntr testContr = new UploadSoWCntr(testStandardController);
        System.assertEquals(true, testContr.OppSowStatus);
    }

    private static void createQlikTech(String countryName, String countryAbbr, Id subsId){
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr,
                                            Country_Name__c = countryName,
                                            Subsidiary__c = subsId);
        insert qlikTechIns;
    }
}