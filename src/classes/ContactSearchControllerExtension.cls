/*******************************
*
* 2019-03-17 AIN Fixes test class error caused by duplicate rules
*
*******************************/
public with sharing class ContactSearchControllerExtension
{
    private final Contact ctt;
    
    public ContactSearchControllerExtension(ApexPages.StandardController stdController)
    {
        this.ctt = (Contact)stdController.getRecord();
    }
    //Properties
    public Contact[] results
    {
        get
        {
            if(results == null) results = new Contact[0];
            return results;
        }
        set {results = value;}
    }
    
    public Boolean hasSearched
    {
        get
        {
            if(hasSearched == null) hasSearched = false;
            return hasSearched;
        }
        set {hasSearched = value;}
    }
    
    public Boolean resultsFound
    {
        get
        {
            if(results.size() == 0)
                return false;
            else
                return true;
        }
    }
    
    public Boolean lastNameStartsWith
    {
        get
        {
            if(lastNameStartsWith == null) lastNameStartsWith = true;
            return lastNameStartsWith;
        }
        set {lastNameStartsWith = value;}
    }
    
    public Boolean firstNameStartsWith
    {
        get
        {
            if(firstNameStartsWith == null) firstNameStartsWith = true;
            return firstNameStartsWith;
        }
        set {firstNameStartsWith = value;}
    }
    
    public Boolean emailStartsWith
    {
        get
        {
            if(emailStartsWith == null) emailStartsWith = true;
            return emailStartsWith;
        }
        set {emailStartsWith = value;}
    }
    
    public String contactEmail
    {
        get
        {
            if(contactEmail == null) contactEmail = '';
            return contactEmail;
        }
        set {contactEmail = value;}
    }
    //Actions
    public PageReference searchContacts()
    {
        results.clear();
        hasSearched = true;
        
        //Validation rules
        if(ctt.LastName != null && ctt.LastName.length() == 1)
        {
            ctt.LastName.addError('Please enter a minimum of 2 characters then click Search again.');
            hasSearched = false;
            return null;
        }
        if(ctt.FirstName != null && ctt.FirstName.length() == 1)
        {
            ctt.FirstName.addError('Please enter a minimum of 2 characters then click Search again.');
            hasSearched = false;
            return null;
        }
        //Validation rules passsed, continue
        //Prepare SOQL Query
        String soqlQuery = 'select Id, Name, Email, AccountId, Account.Name, Account.QlikTech_Company__c, OwnerId, Owner.Name from Contact where Id != null ';
        if(ctt.LastName != null)
        {
            if(lastNameStartsWith)
                soqlQuery += 'and LastName like \'' + ctt.LastName.replace('\'', '\\\'') + '%\' ';
            else
                soqlQuery += 'and LastName like \'%' + ctt.LastName.replace('\'', '\\\'') + '%\' ';
        }
        if(ctt.FirstName != null)
        {
            if(firstNameStartsWith)
                soqlQuery += 'and FirstName like \'' + ctt.FirstName.replace('\'', '\\\'') + '%\' ';
            else
                soqlQuery += 'and FirstName like \'%' + ctt.FirstName.replace('\'', '\\\'') + '%\' ';
        }
        if(contactEmail.length() > 0)
        {
            if(emailStartsWith)
                soqlQuery += 'and Email like \'' + contactEmail.replace('\'', '\\\'') + '%\' ';
            else
                soqlQuery += 'and Email like \'%' + contactEmail.replace('\'', '\\\'') + '%\' ';
        }
        
        soqlQuery += 'order by Name limit 1000';
        
        results = Database.query(soqlQuery);

        if (results.size() == 0)
            ctt.LastName.addError('No contacts found. Please modify the search criteria and click Search again or click Create New.');

        return null;
    }
    
    public PageReference newContact()
    {
        String recordType = ApexPages.currentPage().getParameters().get('RecordType');
        String retURL = ApexPages.currentPage().getParameters().get('retURL');
        String saveURL = ApexPages.currentPage().getParameters().get('saveURL');  
        String accid = ApexPages.currentPage().getParameters().get('accid');
        
        String url = '/003/e?nooverride=1';
        url += recordType == null? '' : '&RecordType=' + EncodingUtil.urlEncode(recordType, 'UTF-8');
        url += accid == null ? '' : '&accid=' + EncodingUtil.urlEncode(accid, 'UTF-8');
        url += retURL == null ? '' : '&retURL=' + EncodingUtil.urlEncode(retURL, 'UTF-8');
        url += saveURL == null ? '' : '&saveURL=' + EncodingUtil.urlEncode(saveURL, 'UTF-8');
        url += ctt.LastName == null ? '' : '&name_lastcon2=' + EncodingUtil.urlEncode(ctt.LastName, 'UTF-8');
        url += ctt.FirstName == null ? '' : '&name_firstcon2=' + EncodingUtil.urlEncode(ctt.FirstName, 'UTF-8');
        url += contactEmail == '' ? '' : '&con15=' + EncodingUtil.urlEncode(contactEmail, 'UTF-8');

        PageReference newContactPage = new PageReference(url);
        newContactPage.setRedirect(true);
        return newContactPage;
    }
    //Test method
    public static testmethod void testContactSearchControllerExtension()
    {
        Test.setCurrentPageReference(Page.ContactSearch);
        Contact testContact = new Contact();
        ApexPages.StandardController sc = new ApexPages.StandardController(testContact);

        Test.startTest();

        ContactSearchControllerExtension csce = new ContactSearchControllerExtension(sc);
        
        //Test properties
        csce.hasSearched = null;
        Boolean hasSearched = csce.hasSearched;
        System.assert(!hasSearched);
        csce.hasSearched = true;
        hasSearched = csce.hasSearched;
        System.assert(hasSearched);

        Contact[] contactArray = new Contact[0];
        contactArray.add(new Contact());
        contactArray.add(new Contact());
        csce.results = contactArray;
        System.assertEquals(contactArray, csce.results);
        System.assertEquals(2, csce.results.size());
        csce.results = null;
        System.assertNotEquals(null, csce.results);

        csce.lastNameStartsWith = null;
        Boolean lastNameStartsWith = csce.lastNameStartsWith;
        System.assert(lastNameStartsWith);
        csce.lastNameStartsWith = false;
        lastNameStartsWith = csce.lastNameStartsWith;
        System.assert(!lastNameStartsWith);

        csce.firstNameStartsWith = null;
        Boolean firstNameStartsWith = csce.firstNameStartsWith;
        System.assert(firstNameStartsWith);
        csce.firstNameStartsWith = false;
        firstNameStartsWith = csce.firstNameStartsWith;
        System.assert(!firstNameStartsWith);

        csce.emailStartsWith = null;
        Boolean emailStartsWith = csce.emailStartsWith;
        System.assert(emailStartsWith);
        csce.emailStartsWith = false;
        emailStartsWith = csce.emailStartsWith;
        System.assert(!emailStartsWith);

        String testContactEmail = csce.contactEmail;
        System.assertEquals('', testContactEmail);
        testContactEmail = 'testContactEmail';
        csce.contactEmail = testContactEmail;
        System.assertEquals('testContactEmail', csce.contactEmail);

        //Test validation rules
        csce.ctt.LastName = '1';
        PageReference searchPage = csce.searchContacts();
        System.assertEquals(null, searchPage);
        csce.ctt.LastName = '';
        csce.ctt.FirstName = '1';
        searchPage = csce.searchContacts();
        System.assertEquals(null, searchPage);
        
        //Test search with all search criteria blank
        csce.ctt.LastName = '';
        csce.ctt.FirstName = '';
        csce.contactEmail = '';
        searchPage = csce.searchContacts();
        System.assertEquals(null, searchPage);
        
        //Test search with all set to starts with search
        csce.ctt.LastName = 'test';
        csce.ctt.FirstName = 'test';
        csce.contactEmail = 'test';
        csce.lastNameStartsWith = true;
        csce.firstNameStartsWith = true;
        csce.emailStartsWith = true;
        searchPage = csce.searchContacts();
        System.assertEquals(null, searchPage);
        
        //Test search with all set to contains search
        csce.lastNameStartsWith = false;
        csce.firstNameStartsWith = false;
        csce.emailStartsWith = false;
        searchPage = csce.searchContacts();
        System.assertEquals(null, searchPage);
        
        //Test search with zero results
        csce.ctt.LastName = 'aasdfadssdfsfasdf';
        searchPage = csce.searchContacts();
        System.assertEquals(null, searchPage);
        System.assert(!csce.resultsFound);
        
        //Test search with one result
        testContact = new Contact(LastName='Baelish', FirstName='Petyr', Email='littlefinger@masterof.coin');
        insert testContact;
        csce.ctt.LastName = 'Baelish';
        csce.ctt.FirstName = 'Petyr';
        csce.contactEmail = 'littlefinger@masterof.coin';
        searchPage = csce.searchContacts();
        System.assertEquals(null, searchPage);
        System.assert(csce.resultsFound);
        
        //Test newContact no query params
        csce.ctt.LastName = null;
        csce.ctt.FirstName = null;
        csce.contactEmail = null;
        String newPageUrl = csce.newContact().getUrl();
        System.assert(newPageUrl.contains('/003/e'));
        System.assert(newPageUrl.contains('nooverride=1'));
        System.assert(!newPageUrl.contains('RecordType=123'));
        System.assert(!newPageUrl.contains('accid=123'));
        System.assert(!newPageUrl.contains('retURL=123'));
        System.assert(!newPageUrl.contains('name_lastcon2'));
        System.assert(!newPageUrl.contains('name_firstcon2'));
        System.assert(!newPageUrl.contains('con15'));

        //Test newContact with query params
        csce.ctt.LastName = 'Baelish';
        csce.ctt.FirstName = 'Petyr';
        csce.contactEmail = 'littlefinger@masterof.coin';
        PageReference testPageReference = new PageReference(Page.ContactSearch.getUrl() + '?RecordType=123&accid=123&retURL=123');
        Test.setCurrentPageReference(testPageReference);
        newPageUrl = csce.newContact().getUrl();
        System.assert(newPageUrl.contains('/003/e'));
        System.assert(newPageUrl.contains('nooverride=1'));
        System.assert(newPageUrl.contains('RecordType=123'));
        System.assert(newPageUrl.contains('accid=123'));
        System.assert(newPageUrl.contains('retURL=123'));
        System.assert(newPageUrl.contains('name_lastcon2=Baelish'));
        System.assert(newPageUrl.contains('name_firstcon2=Petyr'));
        System.assert(newPageUrl.contains('con15=littlefinger'));
        
        Test.stopTest();
    }
}