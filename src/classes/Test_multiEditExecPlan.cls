/*
	Class: Test_multiEdit_ExecPlan
	
	Changelog:
		2013-05-09	CCE		Initial development. Test class for multiEdit_ExecPlan_Controller and multiEdit_ExecPlan page.
		07.02.2017   RVA :   changing methods 							
*/
@isTest
private class Test_multiEditExecPlan {

    static testMethod void TestAddingNewRecord() {
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
			QTComp.Country_Name__c = 'United Kingdom';
		update QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
		
		Account_Plan__c TestAccountPlan = new Account_Plan__c(
			Account__c = TestPartnerAccount.Id,
			Name = 'TestAccPlan1',
			Approved_by_Sales_Productivity__c = true,
			Last_Review_Date__c = Date.Today()
		);
		insert TestAccountPlan;
		
		//and add a child object
		List<Execution_Plan__c> ExecutionPlanList = new List<Execution_Plan__c>();
		Execution_Plan__c TestPlan1 = new Execution_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Strategies__c = 'Some stuff'
		);
		ExecutionPlanList.add(TestPlan1);
		
		Execution_Plan__c TestPlan2 = new Execution_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Strategies__c = 'Some stuff'
		);
		ExecutionPlanList.add(TestPlan2);		
		insert ExecutionPlanList;
		
		test.startTest();
		PageReference pageRef = new PageReference('/apex/multiEdit_ExecPlan?accountPlanId=' + TestAccountPlan.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(TestAccountPlan);
		multiEdit_ExecPlan_Controller controller = new multiEdit_ExecPlan_Controller(StandardController);

		List<Execution_Plan__c> multiList = controller.getExecutionPlans();
		System.assert(multiList != null);
		//we should get our original 2 records plus the new blank one
		System.assert(multiList.size() == 3);	
		
		//Next we will modify an existing record
		Execution_Plan__c existing = multiList[0];
		existing.Strategies__c = 'Different stuff';
		// and edit the new blank record
		Execution_Plan__c newPlan = multiList[2];
		newPlan.Strategies__c = 'New stuff';
		controller.QuickSaveSet();	
		
		List<Execution_Plan__c> newList = controller.getExecutionPlans();
		System.assert(newList != null);
		//we should get our original 2 records plus the new one plus a new blank one
		System.assert(newList.size() == 4);
		//test we modified an existing record	
		Execution_Plan__c testModify = newList[0];
		System.assert(testModify.Strategies__c == 'Different stuff');				
				
		test.stopTest();
    }
    
    static testMethod void TestCancelingNewRecord() {
	/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
			QTComp.Country_Name__c = 'United Kingdom';
		update QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
		
		Account_Plan__c TestAccountPlan = new Account_Plan__c(
			Account__c = TestPartnerAccount.Id,
			Name = 'TestAccPlan1',
			Approved_by_Sales_Productivity__c = true,
			Last_Review_Date__c = Date.Today()
		);
		insert TestAccountPlan;
		
		//and add a child object
		List<Execution_Plan__c> ExecutionPlanList = new List<Execution_Plan__c>();
		Execution_Plan__c TestPlan1 = new Execution_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Strategies__c = 'Some stuff'
		);
		ExecutionPlanList.add(TestPlan1);
		
		Execution_Plan__c TestPlan2 = new Execution_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Strategies__c = 'Some stuff'
		);
		ExecutionPlanList.add(TestPlan2);		
		insert ExecutionPlanList;
		
		test.startTest();
		PageReference pageRef = new PageReference('/apex/multiEdit_ExecPlan?accountPlanId=' + TestAccountPlan.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(TestAccountPlan);
		multiEdit_ExecPlan_Controller controller = new multiEdit_ExecPlan_Controller(StandardController);

		List<Execution_Plan__c> multiList = controller.getExecutionPlans();
		System.assert(multiList != null);
		//we should get our original 2 records plus the new blank one
		System.assert(multiList.size() == 3);	
		
		//Edit the new blank record and then Cancel it
		Execution_Plan__c newPlan = multiList[2];
		newPlan.Strategies__c = 'New stuff';
		controller.CancelSet();		//do the Cancel	
		
		List<Execution_Plan__c> newList = controller.getExecutionPlans();
		System.assert(newList != null);
		//we should get our original 2 records plus a new blank one
		System.assert(newList.size() == 3);	
				
		test.stopTest();
    }
    
    static testMethod void TestSaveWithoutAddingNewRecord() {
	/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary(); 
		QlikTech_Company__c QTComp = new QlikTech_Company__c(
			Name = 'GBR',
			QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Country_Name__c = 'United Kingdom',
			Subsidiary__c = testSubs1.id						
		);
		insert QTComp;
		*/
		QlikTech_Company__c QTComp = QTTestUtils.createMockQTCompany('QlikTech UK Ltd', 'GBR', 'United States');
			QTComp.Country_Name__c = 'United Kingdom';
		update QTComp;
		QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
				
        Account TestPartnerAccount = new Account(
			Name = 'My Partner',
			Navision_Status__c = 'Partner',
			QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
			Billing_Country_Code__c = QTComp.Id							
		);
		insert TestPartnerAccount;
		
		Account_Plan__c TestAccountPlan = new Account_Plan__c(
			Account__c = TestPartnerAccount.Id,
			Name = 'TestAccPlan1',
			Approved_by_Sales_Productivity__c = true,
			Last_Review_Date__c = Date.Today()
		);
		insert TestAccountPlan;
		
		//and add a child object
		List<Execution_Plan__c> ExecutionPlanList = new List<Execution_Plan__c>();
		Execution_Plan__c TestPlan1 = new Execution_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Strategies__c = 'Some stuff'
		);
		ExecutionPlanList.add(TestPlan1);
		
		Execution_Plan__c TestPlan2 = new Execution_Plan__c(
			Account_Plan__c = TestAccountPlan.Id,
			Strategies__c = 'Some stuff'
		);
		ExecutionPlanList.add(TestPlan2);		
		insert ExecutionPlanList;
		
		test.startTest();
		PageReference pageRef = new PageReference('/apex/multiEdit_ExecPlan?accountPlanId=' + TestAccountPlan.Id);
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController StandardController = new ApexPages.StandardController(TestAccountPlan);
		multiEdit_ExecPlan_Controller controller = new multiEdit_ExecPlan_Controller(StandardController);

		List<Execution_Plan__c> multiList = controller.getExecutionPlans();
		System.assert(multiList != null);
		//we should get our original 2 records plus the new blank one
		System.assert(multiList.size() == 3);	
		
		//Next we will modify an existing record
		Execution_Plan__c existing = multiList[0];
		existing.Strategies__c = 'Different stuff';
		//and Save the change
		controller.SaveSet();
		
		List<Execution_Plan__c> newList = controller.getExecutionPlans();
		System.assert(newList != null);
		//we should get our original 2 records plus a new blank one
		System.assert(newList.size() == 3);	
		
		//test we modified an existing record	
		Execution_Plan__c testModify = newList[0];
		System.assert(testModify.Strategies__c == 'Different stuff');			
				
		test.stopTest();
    }

}