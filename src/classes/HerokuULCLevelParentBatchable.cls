/*

    HerokuULCLevelParentBatchable 

    These are batchable class that run to update ulc account parent for contacts everyday for the ULC Heroku Application

    SAN     2016-01-06

 */
 global class HerokuULCLevelParentBatchable implements Database.Batchable<sObject>{
    
    public HerokuULCLevelParentBatchable (){
        //& Retrieve configuration settings at the process level.

    }

    @TestVisible
    private string GetParentNavisionStatus(string ParentId, Integer RecursionLevel) {
        List<Account> accounts = [SELECT Adopted_Navision_Status__c, Navision_Status__c, ParentId FROM Account WHERE Id = :ParentId];
        if (accounts.size() == 0) {
            return null;
        }
        Account a = accounts.get(0);
        if (a.Navision_Status__c == 'Customer' || a.Adopted_Navision_Status__c == 'Customer') {
            return 'Customer';
        } else if (a.Navision_Status__c == 'Partner' || a.Adopted_Navision_Status__c == 'Partner') {
            return 'Partner';
        } else if (a.ParentId != null) {
            if (RecursionLevel == 3) {
                return null;
            }
            return GetParentNavisionStatus(a.ParentId, RecursionLevel++);
        }
        return null;
    }

     global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Account.ParentId, Account_Type__c FROM Contact WHERE Account.ParentId != null]);        
    }
        
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        List<Contact> changedParent = (List<Contact>)scope;

        //use set so only keep unique accountids
        Set<Id> accountIds = new Set<Id>();                 
        for (Contact al : changedParent) accountIds.add(al.Id);

        List<Contact> licenseToContacts = new List<Contact>();        
        //& contact thas has valid licenses
        for (Contact e : [SELECT Id, Account.ParentId FROM Contact 
                              WHERE AccountId IN :accountIds]){
            e.ULC_ParentAccount__c = GetParentNavisionStatus(e.Account.ParentId, 0);
            licenseToContacts.add(e);
        }

        if (licenseToContacts.size() > 0) update licenseToContacts;


    }
    
    global void finish(Database.BatchableContext BC){}   
}