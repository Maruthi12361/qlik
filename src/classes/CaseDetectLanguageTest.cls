@isTest
private class CaseDetectLanguageTest {

	private static Id recTypeId;

	public static testMethod void TestNormal() {
		QTTestUtils.GlobalSetUp();

		Category_Lookup__c catLook = new Category_Lookup__c();
		catLook.Name = 'I have a setup and configuration issue';
		catLook.Category_Level__c = 'I have an issue building a custom authentication solution';
		catLook.Severity__c = '2';
		catLook.Case_Owner__c = 'Team Technical';
		catLook.Service_Request_Type__c = 'Test';
		catLook.Question_Prompt_1__c = 'Line 1';
		catLook.Question_Prompt_2__c = 'Line 2';
		catLook.Question_Prompt_3__c = 'Line 3';
		catLook.Question_Prompt_4__c = 'Line 4';
		catLook.Question_Prompt_5__c = 'Line 5';
		catLook.Record_Type_Name__c	= 'QlikTech Master Support Record Type';
		insert catLook;

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.Account_Support_Information__c = 'Test';

        insert testAccount;

        
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        // Running as the current user for code coverage only

    	Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);
    	caseObj.Description = 'Detect this Line 1 Line 2 Line 3 Line 4 Line 5';
    	caseObj.Area__c = 'I have a setup and configuration issue';
    	caseObj.Issue__c = 'I have an issue building a custom authentication solution';
    	

    	//Test.setMock(HttpCalloutMock.class, new GoogleTranslateUtilsMock());
    	test.startTest();
    	insert caseObj;
    	test.stopTest();
    	
	}
	public static testMethod void TestNoAreaIssue() {
		QTTestUtils.GlobalSetUp();

		Category_Lookup__c catLook = new Category_Lookup__c();
		catLook.Name = 'I have a setup and configuration issue';
		catLook.Category_Level__c = 'I have an issue building a custom authentication solution';
		catLook.Severity__c = '2';
		catLook.Case_Owner__c = 'Team Technical';
		catLook.Service_Request_Type__c = 'Test';
		catLook.Question_Prompt_1__c = 'Line 1';
		catLook.Question_Prompt_2__c = 'Line 2';
		catLook.Question_Prompt_3__c = 'Line 3';
		catLook.Question_Prompt_4__c = 'Line 4';
		catLook.Question_Prompt_5__c = 'Line 5';
		catLook.Record_Type_Name__c	= 'QlikTech Master Support Record Type';
		insert catLook;

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.Account_Support_Information__c = 'Test';

        insert testAccount;

        
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        // Running as the current user for code coverage only

    	Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);
    	caseObj.Description = 'Detect this Line 1 Line 2 Line 3 Line 4 Line 5';

    	//Test.setMock(HttpCalloutMock.class, new GoogleTranslateUtilsMock());
    	test.startTest();
    	insert caseObj;
    	test.stopTest();
    	
	}
	public static testMethod void TestNotPresentAreaIssue() {
		QTTestUtils.GlobalSetUp();

		Category_Lookup__c catLook = new Category_Lookup__c();
		catLook.Name = 'I have a setup and configuration issue';
		catLook.Category_Level__c = 'I have an issue building a custom authentication solution';
		catLook.Severity__c = '2';
		catLook.Case_Owner__c = 'Team Technical';
		catLook.Service_Request_Type__c = 'Test';
		catLook.Question_Prompt_1__c = 'Line 1';
		catLook.Question_Prompt_2__c = 'Line 2';
		catLook.Question_Prompt_3__c = 'Line 3';
		catLook.Question_Prompt_4__c = 'Line 4';
		catLook.Question_Prompt_5__c = 'Line 5';
		catLook.Record_Type_Name__c	= 'QlikTech Master Support Record Type';
		insert catLook;

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        testAccount.Account_Support_Information__c = 'Test';

        insert testAccount;

        
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        // Running as the current user for code coverage only

    	Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);
    	caseObj.Description = 'Detect this Line 1 Line 2 Line 3 Line 4 Line 5';
    	caseObj.Area__c = 'Nein';
    	caseObj.Issue__c = 'Nein';
    	//Test.setMock(HttpCalloutMock.class, new GoogleTranslateUtilsMock());
    	test.startTest();
    	insert caseObj;
    	test.stopTest();
    	
	}
	private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}