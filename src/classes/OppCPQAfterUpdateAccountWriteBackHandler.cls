/****************************************************************
*
*  OppCPQAfterUpdateAccountWriteBackHandler
*
*  Trigger will write back "hidden" opp fields to the Opp Account. 
*  This to solve issues with CPQ. 	
*
*  2016-05-18 Roman@4front : Class created. Migrated from Opportunity_CPQAfterUpdateAccountWriteBack.trigger
*					CR# 4495: Partners write to Accounts when not resp partners
*  2017-06-22 MTM  QCW-2711 remove sharing option 
*****************************************************************/

public class OppCPQAfterUpdateAccountWriteBackHandler {
	public OppCPQAfterUpdateAccountWriteBackHandler() {
		
	}

	public static void handle(List<Opportunity> triggerNew, Map<Id,Opportunity> triggerOldMap) {
		List<Account> Accounts = new List<Account>();
		System.debug('OppCPQAfterUpdateAccountWriteBackHandler: started');
		
		for (Opportunity Opp : triggerNew)
		{
			if (Opp.Quote_Status__c == 'Order Placed' && triggerOldMap.get(Opp.Id).Quote_Status__c != 'Order Placed')
			{
				Account UpdateAccount = new Account(ID = Opp.AccountId,
													//Support_Level__c = Opp.Support_Level_2account__c,
													//Support_Per__c = Opp.Support_Percentage_2account__c,
													First_Agreement_Date__c = Opp.First_Agreement_Date_2account__c
												   );  
				Accounts.Add(UpdateAccount);	
			}
		}
		System.debug('OppCPQAfterUpdateAccountWriteBackHandler: finished');

		if (Accounts.size() > 0)
		{
			update (Accounts);
		}	
	}
}