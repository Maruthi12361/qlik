public class OpportunityLineItemUIElement {

	string ListPrice;
	string oliProductName;
	string oliProductCode;
	Decimal priceBookListPrice = 0.0;
	string currencyCode;
	
	OpportunityLineItem oli;
	boolean	render = false;
	public OpportunityLineItemUIElement()
	{
		
	}
	
	public void setListPrice(String s)
	{
		
	}
	
	public void setPriceBookListPrice(Decimal d)
	{
		priceBookListPrice = d;
	}
	
	public String getCurrencyCode()
	{
		return currencyCode;
	}
	
	public void setCurrCode(String c)
	{
		currencyCode = c;
	}
	
	public String getListPrice()
	{
		String val = '';
		if (null != oli.ListPrice)
		{
			val = oli.CurrencyIsoCode + ' ' + 	oli.ListPrice;
		}
		else
		{
			val = currencyCode + ' ' + priceBookListPrice;	
		}
		return val;
	}
	
	public string getOliProductName()
	{ 
		return oliProductName; 
	}
 	public void setOliProductName(string p) 
 	{ 
 		oliProductName = p;
 	 		
 	}
 	
 	public string getOliProductCode()
	{ 
		return oliProductCode; 
	}
 	public void setOliProductCode(string p) 
 	{ 
 		oliProductCode = p;
 	 		
 	}
 	
 	public boolean getrender()
	{ 
		return render; 
	}
 	public void setrender(boolean p) 
 	{ 
 		render = p;
 	 		
 	}
 	string oliNav;
	public string getOliNav()
	{ 
		return oliNav; 
	}
 	public void setOliNav(string p) 
 	{ 
 		oliNav = p;
 	 		
 	}
	public OpportunityLineItem getOli()
	{ 
		return oli; 
	} 	
 	public void setOli(OpportunityLineItem oli)
 	{ 
 		this.oli = oli; 
 	}
}