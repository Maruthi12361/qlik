/**************************************************
*
* Change Log:
* 
* 2017-01-31    CCE Initial development. CR# 102586 - Vistex Data Deployment and Access V2
*               Test class for CampaignUpdateMDFProject.trigger, MDFVistexCreateCampaign.trigger, MDFVistexCreateCampaignHelper.cls
*
**************************************************/
@isTest
private class CampaignUpdateMDFProjectTest {
/*****    
    public static vistex_mdf__CompanyLocation__c mdf_cl = new vistex_mdf__CompanyLocation__c();
    public static vistex_mdf__MDFProgram__c mdf_pg = new vistex_mdf__MDFProgram__c();
    public static vistex_mdf__MDFQuarter__c mdf_qtr = new vistex_mdf__MDFQuarter__c();
    public static vistex_mdf__MDFFund__c mdf_fund = new vistex_mdf__MDFFund__c();
    public static Account acc = new Account(); //TODO create the account - we need to set a user as the Partner Marketing Manager
        

    @isTest static void testCreateMDFProjectRecord_WithEndDateInThePast() {
        QttestUtils.GlobalSetUp();  
        //determine if settings exist, otherwise use default settings
        QTVistexCampaignActivityTypeMap__c settings = QTVistexCampaignActivityTypeMap__c.getOrgDefaults();
        
        if(settings.CreatedDate == null) {
            QTVistexCampaignActivityTypeMap__c s = CampaignUpdateMDFProjectTest.newSettings();
            upsert s;
        }
        CampaignUpdateMDFProjectTest.CreateAccount();
        CampaignUpdateMDFProjectTest.CreateCompanyLocationRecord();
        CampaignUpdateMDFProjectTest.CreateMDFProgramRecord();
        CampaignUpdateMDFProjectTest.CreateQuarterRecord();
        CampaignUpdateMDFProjectTest.CreateFundRecord(mdf_pg.Id);

        //create the MDF Project record and insert it
        vistex_mdf__MDFProject__c mdfProject = new vistex_mdf__MDFProject__c();
        mdfProject.vistex_mdf__CompanyLocation__c = mdf_cl.Id;
        mdfProject.vistex_mdf__CompanyUserLoginId__c = 'Bob55@thiscompany.com.test';
        mdfProject.vistex_mdf__CompanyUserName__c = 'Bob55';
        mdfProject.vistex_mdf__AccountManagerEmail__c = 'accmang.anyone@qlik.com.test';
        mdfProject.Name = '0001016';
        mdfProject.vistex_mdf__ProjectName__c = 'My big test project';
        mdfProject.vistex_mdf__Program__c = mdf_pg.Id;
        mdfProject.vistex_mdf__ActivityType__c = 'Sponsorship';
        mdfProject.vistex_mdf__BudgetType__c = 'Contra';
        mdfProject.vistex_mdf__Fund__c = mdf_fund.Id;
        mdfProject.vistex_mdf__Description__c = 'A test';
        mdfProject.vistex_mdf__StartDate__c = Date.newInstance(2016, 4, 2);
        mdfProject.vistex_mdf__EndDate__c = Date.newInstance(2016, 5, 1);
        mdfProject.vistex_mdf__ProjectStatus__c = 'Approved - Final';
        mdfProject.vistex_mdf__AmountApproved__c = 100.00;
        mdfProject.vistex_mdf__Account__c = acc.Id;
        
        Test.startTest();
        insert mdfProject;
        Test.stopTest();
        System.debug('testCreateMDFProjectRecord: mdfProject.Id = ' + mdfProject.Id);
        Campaign cmp = [SELECT Id, MDF_Project_ID__c FROM Campaign WHERE MDF_Project_ID__c =: mdfProject.Id];
        System.debug('testCreateMDFProjectRecord: cmp = ' + cmp);
        System.debug('testCreateMDFProjectRecord: mdfProject.vistex_mdf__ProjectName__c = ' + mdfProject.vistex_mdf__ProjectName__c);
        System.assertEquals(mdfProject.Id, cmp.MDF_Project_ID__c);
    }

    @isTest static void testCreateMDFProjectRecord_WithEndDateInTheFuture() {
        QttestUtils.GlobalSetUp();  
        //determine if settings exist, otherwise use default settings
        QTVistexCampaignActivityTypeMap__c settings = QTVistexCampaignActivityTypeMap__c.getOrgDefaults();
        
        if(settings.CreatedDate == null) {
            QTVistexCampaignActivityTypeMap__c s = CampaignUpdateMDFProjectTest.newSettings();
            upsert s;
        }
        CampaignUpdateMDFProjectTest.CreateAccount();
        CampaignUpdateMDFProjectTest.CreateCompanyLocationRecord();
        CampaignUpdateMDFProjectTest.CreateMDFProgramRecord();
        CampaignUpdateMDFProjectTest.CreateQuarterRecord();
        CampaignUpdateMDFProjectTest.CreateFundRecord(mdf_pg.Id);

        //create the MDF Project record and insert it
        vistex_mdf__MDFProject__c mdfProject = new vistex_mdf__MDFProject__c();
        mdfProject.vistex_mdf__CompanyLocation__c = mdf_cl.Id;
        mdfProject.vistex_mdf__CompanyUserLoginId__c = 'Bob55@thiscompany.com.test';
        mdfProject.vistex_mdf__CompanyUserName__c = 'Bob55';
        mdfProject.vistex_mdf__AccountManagerEmail__c = 'accmang.anyone@qlik.com.test';
        mdfProject.Name = '0001016';
        mdfProject.vistex_mdf__ProjectName__c = 'My big test project';
        mdfProject.vistex_mdf__Program__c = mdf_pg.Id;
        mdfProject.vistex_mdf__ActivityType__c = 'Sponsorship';
        mdfProject.vistex_mdf__BudgetType__c = 'Contra';
        mdfProject.vistex_mdf__Fund__c = mdf_fund.Id;
        mdfProject.vistex_mdf__Description__c = 'A test';
        mdfProject.vistex_mdf__StartDate__c = Date.today().addDays(-2);
        mdfProject.vistex_mdf__EndDate__c = Date.today().addDays(+2);
        mdfProject.vistex_mdf__ProjectStatus__c = 'Approved - Final';
        mdfProject.vistex_mdf__AmountApproved__c = 100.00;
        mdfProject.vistex_mdf__Account__c = acc.Id;
        
        Test.startTest();
        insert mdfProject;
        Test.stopTest();
        System.debug('testCreateMDFProjectRecord: mdfProject.Id = ' + mdfProject.Id);
        Campaign cmp = [SELECT Id, MDF_Project_ID__c FROM Campaign WHERE MDF_Project_ID__c =: mdfProject.Id];
        System.debug('testCreateMDFProjectRecord: cmp = ' + cmp);
        System.debug('testCreateMDFProjectRecord: mdfProject.vistex_mdf__ProjectName__c = ' + mdfProject.vistex_mdf__ProjectName__c);
        System.assertEquals(mdfProject.Id, cmp.MDF_Project_ID__c);
    }

    @isTest static void testUpdateMDFProjectRecord_ChangeActivityTypeStartDateEndDateAmountApproved() {
        QttestUtils.GlobalSetUp();  
        //determine if settings exist, otherwise use default settings
        QTVistexCampaignActivityTypeMap__c settings = QTVistexCampaignActivityTypeMap__c.getOrgDefaults();
        Date todaysDate = Date.today();
        
        if(settings.CreatedDate == null) {
            QTVistexCampaignActivityTypeMap__c s = CampaignUpdateMDFProjectTest.newSettings();
            upsert s;
        }
        CampaignUpdateMDFProjectTest.CreateAccount();
        CampaignUpdateMDFProjectTest.CreateCompanyLocationRecord();
        CampaignUpdateMDFProjectTest.CreateMDFProgramRecord();
        CampaignUpdateMDFProjectTest.CreateQuarterRecord();
        CampaignUpdateMDFProjectTest.CreateFundRecord(mdf_pg.Id);

        //create the MDF Project record and insert it
        vistex_mdf__MDFProject__c mdfProject = new vistex_mdf__MDFProject__c();
        mdfProject.vistex_mdf__CompanyLocation__c = mdf_cl.Id;
        mdfProject.vistex_mdf__CompanyUserLoginId__c = 'Bob55@thiscompany.com.test';
        mdfProject.vistex_mdf__CompanyUserName__c = 'Bob55';
        mdfProject.vistex_mdf__AccountManagerEmail__c = 'accmang.anyone@qlik.com.test';
        mdfProject.Name = '0001016';
        mdfProject.vistex_mdf__ProjectName__c = 'My big test project';
        mdfProject.vistex_mdf__Program__c = mdf_pg.Id;
        mdfProject.vistex_mdf__ActivityType__c = 'Sponsorship';
        mdfProject.vistex_mdf__BudgetType__c = 'Contra';
        mdfProject.vistex_mdf__Fund__c = mdf_fund.Id;
        mdfProject.vistex_mdf__Description__c = 'A test';
        mdfProject.vistex_mdf__StartDate__c = todaysDate.addDays(-2);
        mdfProject.vistex_mdf__EndDate__c = todaysDate.addDays(+2);
        mdfProject.vistex_mdf__ProjectStatus__c = 'Approved - Final';
        mdfProject.vistex_mdf__AmountApproved__c = 100.00;
        mdfProject.vistex_mdf__Account__c = acc.Id;
        insert mdfProject;
        
        Test.startTest();
        mdfProject.vistex_mdf__ActivityType__c = 'Menu Based Campaigns (EMEA Only)';
        mdfProject.vistex_mdf__StartDate__c = todaysDate.addDays(-3);
        mdfProject.vistex_mdf__EndDate__c = todaysDate.addDays(+3);
        mdfProject.vistex_mdf__AmountApproved__c = 150.00;
        update mdfProject;        
        Test.stopTest();

        System.debug('testCreateMDFProjectRecord: mdfProject.Id = ' + mdfProject.Id);
        Campaign cmp = [SELECT Id, MDF_Project_ID__c, Type, Campaign_Sub_Type__c, StartDate, EndDate, ActualCost FROM Campaign WHERE MDF_Project_ID__c =: mdfProject.Id];
        System.debug('testCreateMDFProjectRecord: cmp = ' + cmp);
        System.assertEquals('DM - Direct Marketing', cmp.Type);
        System.assertEquals('DM - Email Campaign', cmp.Campaign_Sub_Type__c);
        System.assertEquals(todaysDate.addDays(-3), cmp.StartDate);
        System.assertEquals(todaysDate.addDays(+3), cmp.EndDate);
        System.assertEquals(150.00, cmp.ActualCost);
    }

    @isTest static void testUpdateMDFProjectRecord_ChangeProjectStatus_ProjectExpiredWithEndDateInThePast() {
        QttestUtils.GlobalSetUp();  
        //determine if settings exist, otherwise use default settings
        QTVistexCampaignActivityTypeMap__c settings = QTVistexCampaignActivityTypeMap__c.getOrgDefaults();
        Date todaysDate = Date.today();
        
        if(settings.CreatedDate == null) {
            QTVistexCampaignActivityTypeMap__c s = CampaignUpdateMDFProjectTest.newSettings();
            upsert s;
        }
        CampaignUpdateMDFProjectTest.CreateAccount();
        CampaignUpdateMDFProjectTest.CreateCompanyLocationRecord();
        CampaignUpdateMDFProjectTest.CreateMDFProgramRecord();
        CampaignUpdateMDFProjectTest.CreateQuarterRecord();
        CampaignUpdateMDFProjectTest.CreateFundRecord(mdf_pg.Id);

        //create the MDF Project record and insert it
        vistex_mdf__MDFProject__c mdfProject = new vistex_mdf__MDFProject__c();
        mdfProject.vistex_mdf__CompanyLocation__c = mdf_cl.Id;
        mdfProject.vistex_mdf__CompanyUserLoginId__c = 'Bob55@thiscompany.com.test';
        mdfProject.vistex_mdf__CompanyUserName__c = 'Bob55';
        mdfProject.vistex_mdf__AccountManagerEmail__c = 'accmang.anyone@qlik.com.test';
        mdfProject.Name = '0001016';
        mdfProject.vistex_mdf__ProjectName__c = 'My big test project';
        mdfProject.vistex_mdf__Program__c = mdf_pg.Id;
        mdfProject.vistex_mdf__ActivityType__c = 'Sponsorship';
        mdfProject.vistex_mdf__BudgetType__c = 'Contra';
        mdfProject.vistex_mdf__Fund__c = mdf_fund.Id;
        mdfProject.vistex_mdf__Description__c = 'A test';
        mdfProject.vistex_mdf__StartDate__c = todaysDate.addDays(-5);
        mdfProject.vistex_mdf__EndDate__c = todaysDate.addDays(+3);
        mdfProject.vistex_mdf__ProjectStatus__c = 'Approved - Final';
        mdfProject.vistex_mdf__AmountApproved__c = 100.00;
        mdfProject.vistex_mdf__Account__c = acc.Id;
        insert mdfProject;
        
        Test.startTest();
        mdfProject.vistex_mdf__ProjectStatus__c = 'Project Expired';
        mdfProject.vistex_mdf__EndDate__c = todaysDate.addDays(-2);
        update mdfProject;        
        Test.stopTest();

        System.debug('testCreateMDFProjectRecord: mdfProject.Id = ' + mdfProject.Id);
        Campaign cmp = [SELECT Id, Status  FROM Campaign WHERE MDF_Project_ID__c =: mdfProject.Id];
        System.debug('testCreateMDFProjectRecord: cmp = ' + cmp);
        System.assertEquals('Completed', cmp.Status);
    }

    //Create and MFP Project record with no Campaign then update the status to create the Campaign
    @isTest static void testUpdateMDFProjectRecord_CreateMFPProject_ThenUpdateWithApprovedFinalStatus() {
        QttestUtils.GlobalSetUp();  
        //determine if settings exist, otherwise use default settings
        QTVistexCampaignActivityTypeMap__c settings = QTVistexCampaignActivityTypeMap__c.getOrgDefaults();
        Date todaysDate = Date.today();
        
        if(settings.CreatedDate == null) {
            QTVistexCampaignActivityTypeMap__c s = CampaignUpdateMDFProjectTest.newSettings();
            upsert s;
        }
        CampaignUpdateMDFProjectTest.CreateAccount();
        CampaignUpdateMDFProjectTest.CreateCompanyLocationRecord();
        CampaignUpdateMDFProjectTest.CreateMDFProgramRecord();
        CampaignUpdateMDFProjectTest.CreateQuarterRecord();
        CampaignUpdateMDFProjectTest.CreateFundRecord(mdf_pg.Id);

        //create the MDF Project record and insert it
        vistex_mdf__MDFProject__c mdfProject = new vistex_mdf__MDFProject__c();
        mdfProject.vistex_mdf__CompanyLocation__c = mdf_cl.Id;
        mdfProject.vistex_mdf__CompanyUserLoginId__c = 'Bob55@thiscompany.com.test';
        mdfProject.vistex_mdf__CompanyUserName__c = 'Bob55';
        mdfProject.vistex_mdf__AccountManagerEmail__c = 'accmang.anyone@qlik.com.test';
        mdfProject.Name = '0001016';
        mdfProject.vistex_mdf__ProjectName__c = 'My big test project';
        mdfProject.vistex_mdf__Program__c = mdf_pg.Id;
        mdfProject.vistex_mdf__ActivityType__c = 'Sponsorship';
        mdfProject.vistex_mdf__BudgetType__c = 'Contra';
        mdfProject.vistex_mdf__Fund__c = mdf_fund.Id;
        mdfProject.vistex_mdf__Description__c = 'A test';
        mdfProject.vistex_mdf__StartDate__c = todaysDate.addDays(-5);
        mdfProject.vistex_mdf__EndDate__c = todaysDate.addDays(+3);
        mdfProject.vistex_mdf__ProjectStatus__c = 'Approved - Initial';
        mdfProject.vistex_mdf__AmountApproved__c = 100.00;
        mdfProject.vistex_mdf__Account__c = acc.Id;
        insert mdfProject;
        
        Test.startTest();
        mdfProject.vistex_mdf__ProjectStatus__c = 'Approved - Final';
        mdfProject.vistex_mdf__AmountApproved__c = 150.00;
        update mdfProject;        
        Test.stopTest();

        System.debug('testCreateMDFProjectRecord: mdfProject.Id = ' + mdfProject.Id);
        Campaign cmp = [SELECT Id, ActualCost  FROM Campaign WHERE MDF_Project_ID__c =: mdfProject.Id];
        System.debug('testCreateMDFProjectRecord: cmp = ' + cmp);
        System.assertEquals(mdfProject.vistex_mdf__AmountApproved__c, cmp.ActualCost);
    }

    public static QTVistexCampaignActivityTypeMap__c newSettings() {        
        QTVistexCampaignActivityTypeMap__c settings = new QTVistexCampaignActivityTypeMap__c();                
        settings.MDF_Activity_Type_1__c = 'Sponsorship,EV - Event,EV - Seminar,Customer & Prospect Events (Seminars or Webcasts),EV - Event,EV - Seminar,Email or Direct Mail Campaigns,DM - Direct Marketing,DM - Email Campaign';
        settings.MDF_Activity_Type_2__c = 'Collateral & Media Content,CN - Content,CN - Whitepapers,Telemarketing,TM - Telemarketing,TM - 3rd Party Telemarketing';
        settings.MDF_Activity_Type_3__c = 'Search Engine Marketing,AD - Advertising,AD - Paid Search,Advertising,AD - Advertising,AD - Online Advertising';
        settings.MDF_Activity_Type_4__c = 'Paid Social Media,AD - Advertising,AD - Paid Social,Website Design or Optimization (Master Resellers Only),INF - Infrastructure,INF - Other';
        settings.MDF_Activity_Type_5__c = 'Training,INF - Infrastructure,INF - Employee Development,Concierge Marketing Plan (Americas Only),INF - Infrastructure,INF - Other';
        settings.MDF_Activity_Type_6__c = 'Menu Based Campaigns (EMEA Only),DM - Direct Marketing,DM - Email Campaign,Concierge/Menu Based campaigns (APAC Only),INF - Infrastructure,INF - Other';
        return settings;        
    }

    public static void CreateCompanyLocationRecord() {        
        mdf_cl.Name = 'This Company';
        mdf_cl.vistex_mdf__Active__c = true;
        mdf_cl.vistex_mdf__Address1__c = 'Myroad';
        mdf_cl.vistex_mdf__City__c = 'AnyCity';
        mdf_cl.vistex_mdf__CompanyLocationNumber__c = acc.Id; 
        mdf_cl.vistex_mdf__StateProvince__c = 'Somewhere';
        mdf_cl.vistex_mdf__PostalCode__c = '90590';
        mdf_cl.vistex_mdf__RegionName__c = 'AMERICAS';
        mdf_cl.vistex_mdf__Account__c = acc.Id;
        insert mdf_cl;
    }
    
    public static void CreateMDFProgramRecord() {        
        mdf_pg.Name = 'Qlik Partner MDF';
        mdf_pg.vistex_mdf__Active__c = true;
        mdf_pg.vistex_mdf__ExternalKey__c = '134';
        mdf_pg.vistex_mdf__ProgramType__c = 'MDF';
        mdf_pg.vistex_mdf__ProgramAcronym__c = 'QL01QPMDF';
        insert mdf_pg;
    }
    
    public static void CreateQuarterRecord() {        
        mdf_qtr.Name = '2016-Q2';
        mdf_qtr.vistex_mdf__ExternalKey__c = '134';
        mdf_qtr.vistex_mdf__StartDate__c = Date.newInstance(2016, 4, 1);
        mdf_qtr.vistex_mdf__EndDate__c = Date.newInstance(2016, 6, 30);
        insert mdf_qtr;
    }
    
    public static void CreateFundRecord(Id ProgramId) {        
        mdf_fund.Name = 'LATAM';
        mdf_fund.vistex_mdf__ExternalKey__c = '11366';
        mdf_fund.vistex_mdf__Program__c = ProgramId;
        insert mdf_fund;
    }
    
    public static void CreateAccount() {        
        User u = QTTestUtils.createMockSystemAdministrator();
        acc = QTTestUtils.createMockAccount('MyTestAccount', u, false);
        acc.Partner_Marketing_Manager__c = u.Id;
        Insert acc;  
    }
*****/    
}