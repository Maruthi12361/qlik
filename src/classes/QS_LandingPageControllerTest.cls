/**
 * Ain  2017-03-10 Initial implementation

 * TODO: User trigger framework to disable CheckTriggerPortalControl.trigger (or handler) to get higher coverage when it's in place
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 */
@isTest
private class QS_LandingPageControllerTest {

    //Test Data Setup Method
    @testSetup static void  createTestData() {

        QTTestUtils.GlobalSetUp();
        Semaphores.SetAllSemaphoresToFalse();

        QlikTech_Company__c qtComp_SWE = QTTestUtils.createMockQTCompany('QlikTech Benelux', 'SWE', 'Sweden', 'SEEMEA');
        //QTCompany[1] = qtComp_HUN.Id;

        //Create and insert the Accounts
        List<Account> accList = new List<Account>();
        string accName = '';

        //0
        accName = 'Customer account no contract';
        accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Customer', BillingCountry = 'Sweden', BillingCity = 'aaa', BillingStreet='aasdasd',
                BillingPostalCode = '12345', BillingState ='aaaaa',
                Support_Office__c = null) );

        //1
        accName = 'Partner account';
        accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Partner', Territory_Country__c = 'Sweden', Territory_city__c = 'aaa', Territory_street__c='aasdasd',
                Territory_Zip__c = '12345', Teritory_State_Province__c ='aaaaa',Territory_State_Province_free_input__c ='aaaaa',
                Support_Office__c = null) );

        //2
        accName = 'Not Partner/Customer';
        accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Potato', Territory_Country__c = 'Sweden', Territory_city__c = 'aaa', Territory_street__c='aasdasd',
                Territory_Zip__c = '12345', Teritory_State_Province__c ='aaaaa',Territory_State_Province_free_input__c ='aaaaa',
                Support_Office__c = null) );

        //3
        accName = 'Customer account supported by Qlik';
        accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Customer', BillingCountry = 'Sweden', BillingCity = 'aaa', BillingStreet='aasdasd',
                BillingPostalCode = '12345', BillingState ='aaaaa',
                Support_Office__c = null) );

        //4
        accName = 'Qlik';
        accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Customer', BillingCountry = 'Sweden', BillingCity = 'aaa', BillingStreet='aasdasd',
                BillingPostalCode = '12345', BillingState ='aaaaa',
                Support_Office__c = null) );

        //5
        accName = 'Customer account supported by Partner';
        accList.add(new Account( Name = accName, QlikTech_Company__c='aaaa', Navision_Status__c = 'Customer', BillingCountry = 'Sweden', BillingCity = 'aaa', BillingStreet='aasdasd',
                BillingPostalCode = '12345', BillingState ='aaaaa',
                Support_Office__c = null) );

        insert accList;

        List<Contract> contractList = new List<Contract>();
        //0
        contractList.add(new Contract(
                AccountId = accList[3].Id,
                Support_Provided_By__c = accList[4].Id,
                NS_Status__c = 'Active'));
        //1
        contractList.add(new Contract(
                AccountId = accList[5].Id,
                Support_Provided_By__c = accList[1].Id,
                NS_Status__c = 'Active'));

        insert contractList;


        List<Contact> conList = new List<Contact>();
        //0
        conList.Add(new Contact(FirstName = 'CustNoAssignedLevels', LastName = 'Contact', Email= 'testemail1@asdasd.com', AccountId = accList[0].Id, TriggerPortalControl__c = true));
        //1
        conList.Add(new Contact(FirstName = 'PartNoAssignedLevels', LastName = 'Contact', Email= 'testemail2@asdasd.com', AccountId = accList[1].Id));
        //2
        conList.Add(new Contact(FirstName = 'LeftCompany', LastName = 'Contact', Left_Company__c = true, Email= 'testemail3@asdasd.com', AccountId = accList[1].Id));
        //3
        conList.Add(new Contact(FirstName = 'Not Customer Not Partner', LastName = 'Contact', Email= 'testemail4@asdasd.com', AccountId = accList[2].Id));
        //4
        conList.Add(new Contact(FirstName = 'CustCPView', LastName = 'Contact', Email= 'testemail5@asdasd.com', AccountId = accList[0].Id));
        //5
        conList.Add(new Contact(FirstName = 'CustCPLog', LastName = 'Contact', Email= 'testemail6@asdasd.com', AccountId = accList[0].Id));
        //6
        conList.Add(new Contact(FirstName = 'CustCPViewQlikSupport', LastName = 'Contact', Email= 'testemail7@asdasd.com', AccountId = accList[3].Id));
        //7
        conList.Add(new Contact(FirstName = 'CustCPLogQlikSupport', LastName = 'Contact', Email= 'testemail8@asdasd.com', AccountId = accList[3].Id));
        //8
        conList.Add(new Contact(FirstName = 'CustCPViewPartnerSupport', LastName = 'Contact', Email= 'testemail9@asdasd.com', AccountId = accList[5].Id));
        //9
        conList.Add(new Contact(FirstName = 'CustCPLogPartnerSupport', LastName = 'Contact', Email= 'testemail10@asdasd.com', AccountId = accList[5].Id));
        //10
        conList.Add(new Contact(FirstName = 'CustNoAssignedLevelsWithUser', LastName = 'Contact', Email= 'testemail11@asdasd.com', AccountId = accList[0].Id));
        //11
        conList.Add(new Contact(FirstName = 'CustNoAssignedLevelsWithInUser', LastName = 'Contact', Email= 'testemail12@asdasd.com', AccountId = accList[0].Id));
        //12
        conList.Add(new Contact(FirstName = 'CustNoAssignedLevelsWithDeadUser', LastName = 'Contact', Email= 'testemail12@asdasd.com', AccountId = accList[0].Id));
        //13
        conList.Add(new Contact(FirstName = 'CustNoAssignedLevelsWithDeadUser2', LastName = 'Contact', Email= 'testemail12@asdasd.com', AccountId = accList[0].Id));
        //14
        conList.Add(new Contact(FirstName = 'CustNoAssignedLevelsWithDeadUser3', LastName = 'Contact', Email= 'testemail12@asdasd.com', AccountId = accList[0].Id));
        //14
        conList.Add(new Contact(FirstName = 'CustNoAssignedLevelsWithDeadUser4', LastName = 'Contact', Email= 'testemail12@asdasd.com', AccountId = accList[0].Id));

        insert conList;

        List<Profile> plist = [SELECT Id, Name FROM profile WHERE name = 'Customer Portal Case Logging Access' or name = 'System Administrator' order by Name asc];

        List<User> userList = new List<User>();
        //0
        userList.Add(new User (ProfileId = plist[0].Id, Username = 'testemail11@asdasd.com',
                FirstName = 'CustNoAssignedLevelsWithUser', LastName = 'User',
                Email = 'testemail11@asdasd.com', ContactId = conList[10].Id,
                FederationIdentifier = 'CustNoLevWithUser', CommunityNickname = 'CustNoLevWithUser',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short'
        ));
        //1
        userList.Add(new User (ProfileId = plist[0].Id, Username = 'testemail12@asdasd.com',
                FirstName = 'CustNoAssignedLevelsWithInactiveUser', LastName = 'User',
                Email = 'testemail12@asdasd.com', ContactId = conList[11].Id,
                FederationIdentifier = 'CustNoLevWithInUser', CommunityNickname = 'CustNoLevWithInUser',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short', IsActive = false
        ));
        //2
        userList.Add(new User (ProfileId = plist[1].Id, Username = 'testemail13@asdasd.com',
                FirstName = 'CustNoLevDeadUser', LastName = 'User',
                Email = 'testemail13@asdasd.com',
                FederationIdentifier = 'CustNoLevDeadUser', CommunityNickname = 'CustNoLevDeadUser',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short', IsActive = false
        ));
        //3
        userList.Add(new User (ProfileId = plist[1].Id, Username = 'testemail14@asdasd.com',
                FirstName = 'CustNoLevDeadUser2', LastName = 'User',
                Email = 'testemail14@asdasd.com',
                FederationIdentifier = 'CustNoLevDeadUser2', CommunityNickname = 'CustNoLevDeadasd',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short'
        ));

        //4
        userList.Add(new User (ProfileId = plist[0].Id, Username = 'testemail15@asdasd.com',
                FirstName = 'CustNoLevDeadUser3', LastName = 'User',
                Email = 'testemail14@asdasd.com', IsActive = false, ContactId = conList[14].Id,
                FederationIdentifier = 'CustNoLevDeadUser3', CommunityNickname = 'CustNoLevDeadUser3',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short'
        ));
        //5
        userList.Add(new User (ProfileId = plist[1].Id, Username = 'testemail16@asdasd.com',
                FirstName = 'CustNoLevDeadUser3', LastName = 'User',
                Email = 'testemail14@asdasd.com',
                FederationIdentifier = 'CustNoLevDeadUser3', CommunityNickname = 'CustNoLevDeadUserYY',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short'
        ));

        //4
        userList.Add(new User (ProfileId = plist[0].Id, Username = 'testemail17@asdasd.com',
                FirstName = 'CustNoLevDeadUser4', LastName = 'User',
                Email = 'testemail14@asdasd.com', ContactId = conList[15].Id,
                FederationIdentifier = 'CustNoLevDeadUser4', CommunityNickname = 'CustNoLevDeadUser4',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short'
        ));
        //5
        userList.Add(new User (ProfileId = plist[1].Id, Username = 'testemail18@asdasd.com',
                FirstName = 'CustNoLevDeadUser4', LastName = 'User',
                Email = 'testemail14@asdasd.com', IsActive = false,
                FederationIdentifier = 'CustNoLevDeadUser4', CommunityNickname = 'CustNoLevDeadUserXX',
                Emailencodingkey='UTF-8', Languagelocalekey='en_US', localesidkey='en_US',
                Timezonesidkey='America/Los_Angeles', Alias = 'Short'
        ));



        insert userList;


        List<ULC_Level__c> ULCLevels = new List<ULC_Level__c>();
        ULCLevels.Add(new ULC_Level__c(Name = 'CPVIEW',Description__c = 'Customer Portal view Access',Status__c = 'Active'));
        ULCLevels.Add(new ULC_Level__c(Name = 'CPLOG',Description__c = 'Customer Portal logging Access',Status__c = 'Active'));
        ULCLevels.Add(new ULC_Level__c(Name = 'PRMBASE',Description__c = 'Partner base Access',Status__c = 'Active'));
        insert ULCLevels;

        List<Assigned_ULC_Level__c> assignedULCLevels = new List<Assigned_ULC_Level__c>();
        assignedULCLevels.Add(new Assigned_ULC_Level__c(ContactId__c = conList[4].Id, ULCLevelId__c = ULCLevels[0].Id, Status__c = 'Approved'));
        assignedULCLevels.Add(new Assigned_ULC_Level__c(ContactId__c = conList[5].Id, ULCLevelId__c = ULCLevels[1].Id, Status__c = 'Approved'));

        assignedULCLevels.Add(new Assigned_ULC_Level__c(ContactId__c = conList[6].Id, ULCLevelId__c = ULCLevels[0].Id, Status__c = 'Approved'));
        assignedULCLevels.Add(new Assigned_ULC_Level__c(ContactId__c = conList[7].Id, ULCLevelId__c = ULCLevels[1].Id, Status__c = 'Approved'));

        assignedULCLevels.Add(new Assigned_ULC_Level__c(ContactId__c = conList[8].Id, ULCLevelId__c = ULCLevels[0].Id, Status__c = 'Approved'));
        assignedULCLevels.Add(new Assigned_ULC_Level__c(ContactId__c = conList[9].Id, ULCLevelId__c = ULCLevels[1].Id, Status__c = 'Approved'));
        insert assignedULCLevels;

        List<ULC_Details__c> ulcDetailsList = new List<ULC_Details__c>();
        //Customer user with no assigned ulc levels
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custnolevels', ContactId__c = conList[0].Id));
        //Partner user with no assigned ulc levels
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'partnolevels', ContactId__c = conList[1].Id));
        //Ulc details where contact is null
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'nullcontact', ContactId__c = null));
        //Contact with left_company__c = true
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'leftcompany', ContactId__c = conList[2].Id));
        //Account with erp status not customer or partner
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'notcustorpart', ContactId__c = conList[3].Id));
        //Customer with cpview
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custcpview', ContactId__c = conList[4].Id));
        //Customer with cplog
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custcplog', ContactId__c = conList[5].Id));
        //Customer with cpview and support contract with qlik
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custcpviewqlik', ContactId__c = conList[6].Id));
        //Customer with cplog and support contract with qlik
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custcplogqlik', ContactId__c = conList[7].Id));
        //Customer with cpview and support contract with partner
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custcpviewpartner', ContactId__c = conList[8].Id));
        //Customer with cplog and support contract with partner
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custcplogpartner', ContactId__c = conList[9].Id));
        //Customer with no assigned ulc levels and an active user with all details correct
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custnolevwithuser', ContactId__c = conList[10].Id));
        //Customer with no assigned ulc levels and an inactive user with all details correct
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custnolevwithinuser', ContactId__c = conList[11].Id));
        //Customer with no assigned ulc levels and a 'dead' user not linked to the correct contact
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custnolevdeaduser', ContactId__c = conList[12].Id));
        //Customer with no assigned ulc levels and a 'dead' user with the wrong nickname
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custnolevdeaduser2', ContactId__c = conList[13].Id));
        //Customer with no assigned ulc levels and both an active and inactive user with the same fedid
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custnolevdeaduser3', ContactId__c = conList[14].Id));
        //Customer with no assigned ulc levels and 2 inactive users, one with the correct fedid
        ulcDetailsList.add(new ULC_Details__c(ULCName__c = 'custnolevdeaduser4', ContactId__c = conList[15].Id));

        insert ulcDetailsList;
    }

    @isTest static void test1() {

        PageReference pageRef = Page.QS_LandingPage;

        User guestUser = [select id, name from user where id = '005D0000004wTRsIAM'];

        system.Test.setCurrentPage(pageRef);

        QS_LandingPageController controller = new QS_LandingPageController();

        ApexPages.currentPage().getParameters().put('username', '');
        ApexPages.currentPage().getParameters().put('relaystate', 'www.google.com');
        controller.CheckULC();
        controller.redirectToSupportPortalWithRedirect();
        controller.redirectToQlikId();

        ApexPages.currentPage().getParameters().put('username', 'Not found');
        controller.CheckULC();

        test.startTest();

        system.runAs(guestUser) {
            controller.CheckULCStatus('custnolevels');
            controller.CheckULCStatus('partnolevels');
            controller.CheckULCStatus('nullcontact');
            controller.CheckULCStatus('leftcompany');
            controller.CheckULCStatus('notcustorpart');
            controller.CheckULCStatus('custcpview');
            controller.CheckULCStatus('custcplog');
        }

        controller.checkStatusController();
        controller.stopCheckingStatus();

        test.stopTest();
    }

    @isTest static void test2() {
        PageReference pageRef = Page.QS_LandingPage;

        User guestUser = [select id, name from user where id = '005D0000004wTRsIAM'];

        system.Test.setCurrentPage(pageRef);

        QS_LandingPageController controller = new QS_LandingPageController();

        ApexPages.currentPage().getParameters().put('username', '');
        controller.CheckULC();

        ApexPages.currentPage().getParameters().put('username', 'Not found');
        controller.CheckULC();

        test.startTest();

        system.runAs(guestUser) {
            controller.CheckULCStatus('custcpviewqlik');
            controller.CheckULCStatus('custcplogqlik');
            controller.status = 'creatingportaluser';
            controller.checkStatusController();
            controller.CheckULCStatus('custcpviewpartner');
            controller.CheckULCStatus('custcplogpartner');
        }
        controller.CheckULCStatus('partnolevels');
        controller.CheckULCStatus('custcpview');
        controller.CheckULCStatus('custcplog');

        controller.checkStatusController();
        controller.stopCheckingStatus();

        test.stopTest();
    }

    @isTest static void test3() {
        PageReference pageRef = Page.QS_LandingPage;

        User guestUser = [select id, name from user where id = '005D0000004wTRsIAM'];

        system.Test.setCurrentPage(pageRef);

        QS_LandingPageController controller = new QS_LandingPageController();

        ApexPages.currentPage().getParameters().put('username', '');
        controller.CheckULC();

        ApexPages.currentPage().getParameters().put('username', 'Not found');
        controller.CheckULC();

        test.startTest();

        system.runAs(guestUser) {
            controller.CheckULCStatus('custnoLevwithuser');
            controller.CheckULCStatus('custnolevwithinuser');
            controller.CheckULCStatus('custnolevdeaduser');
            controller.CheckULCStatus('custnolevdeaduser2');
            controller.CheckULCStatus('custnolevdeaduser3');
            controller.CheckULCStatus('custnolevdeaduser4');
        }
        controller.CheckULCStatus('custcpviewqlik');
        controller.CheckULCStatus('custcplogqlik');
        controller.CheckULCStatus('Ccustcpviewpartner');
        controller.CheckULCStatus('custcplogpartner');

        controller.checkStatusController();
        controller.status = 'CreatingPortalUser';
        controller.stopCheckingStatus();
        controller.SendEmailToDev('Hello', 'Hello');
        controller.CheckLicenses('LolBollen');

        test.stopTest();
    }



}