/******************************************************
    Class: OpportunityLastModifiedHandlerTest 
    	Apart from OpportunityLastModifiedHandler, this class
    	should also cover
    		OpportunityWhenEISCreateOppsHandler
    		OppIfPartnerDealRecPSMAtClosedWonHandler
    Changelog:
        2016-06-14  TJG		Migrated from TestDealSplitClose.
        	Fixed too many SOQL error and made sure
        	OpportunityLastModifiedHandler is 100% covered
		24.03.2017 Rodion Vakulovskyi
		2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring
        *11-1-2018 Shubham Added bypass rule true to pass validation error 
******************************************************/
@isTest
private class OpportunityLastModifiedHandlerTest {
    //static final String OppRecordTypeId = '012D0000000KEKO'; //QTNS '012f0000000CuglAAC'; //Qlikbuy CCS Standard II on QTNS  2014-06-04
    
   @testSetup
    public static void Setup() {
        QuoteTestHelper.createCustomSettings();
    }
    
    public static testMethod void TestDealSplitCloseCheckDealSplitStage()
    {
        InsertCustomSettings();
		Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        Semaphores.OpportunityTriggerBeforeUpdate2 = false;
        Semaphores.OpportunityTriggerAfterUpdate2 = false;
        SetUpData();
        
        Test.startTest();
        Semaphores.DealSplitUpdateHasRun = false;
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        Semaphores.OpportunityTriggerBeforeUpdate2 = false;
        Semaphores.OpportunityTriggerAfterUpdate2 = false;
        system.debug('Stage of parent '+dsParent.StageName);
        dsParent.StageName = 'Deal Split-Closed Won';
        update dsParent;
        
        dsChild1 = [select StageName, Amount from Opportunity where Id = :dsChild1.Id];
        System.assertEquals(dsChild1.StageName, 'Closed Won');           
        System.assertEquals(dsChild1.Amount, 10000);
        
        dsParent = [select Deal_Split_Percentage__c, StageName, ForecastCategoryName, ByPass_Rules__c from Opportunity where Id = :dsParent.Id];           
        //System.assertEquals(dsParent.ForecastCategoryName, 'Omitted');
        System.assertEquals(dsParent.Deal_Split_Percentage__c, 10);    

        Semaphores.DealSplitUpdateHasRun = false;
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        Semaphores.OpportunityTriggerBeforeUpdate2 = false;
        Semaphores.OpportunityTriggerAfterUpdate2 = false;
        dsParent.StageName = 'OEM - Closed Won';
        dsParent.ByPass_Rules__c = true;
        update dsParent;        

        dsChild1 = [select StageName, Amount from Opportunity where Id = :dsChild1.Id];
        //System.assertEquals(dsChild1.StageName, 'OEM - Closed Won');           
        System.assertEquals(dsChild1.Amount, 10000);
        
        Test.stopTest();
    }
    
    public static testMethod void TestDealSplitCheckDealSplitChildTeamMember()
    {
        InsertCustomSettings();

        SetUpData();
        
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {    
            Test.startTest();
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            dsChild1 = [select Id, CreatedById from Opportunity where Id = :dsChild1.Id];
            
            OpportunityTeamMember teamMember = [select UserId, TeamMemberRole from OpportunityTeamMember where OpportunityId =:dsChild1.Id];
            
            System.assertEquals(dsChild1.CreatedById,teamMember.UserId);
            System.assertEquals(teamMember.TeamMemberRole, 'Sales Rep');
            
            Test.stopTest();
        }
    }
    
    public static testMethod void TestDealSplitCheckDealSplitChildAmount()
    {
        InsertCustomSettings();

        SetUpData();
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {                      
            Test.startTest();
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 10000);
            Semaphores.DealSplitUpdateHasRun = false;
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            Semaphores.OpportunityTriggerAfterUpdate = false;
            dsParent.Amount = 500000;
            update dsParent;
            
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 50000);
            
            Test.stopTest();
        }
    }
    
    public static testMethod void TestDealSplitCheckDealSplitPercentageOnAddingNewChild()
    {
        InsertCustomSettings();

        user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.createMockAccount('Deal Split Test Account', user);
        dsParent = createOpportunity('DSTests', 'DS Parent test Opp', 'New Customer', 'Direct', 'Deal Split Create', OppRecordTypeId, 'GBP', user, account);
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            Test.startTest();
            
            Semaphores.DealSplitUpdateHasRun = false;
            Semaphores.OpportunityTriggerBeforeInsert = false;
        	Semaphores.OpportunityTriggerAfterInsert = false;
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            Semaphores.OpportunityTriggerAfterUpdate = false;
            Opportunity newDSChild = QTTestUtils.createOpportunity('Child2 Tests', 'DS Child2 test Opp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account, dsParent, 50);            
            
            System.assertEquals(newDSChild.Deal_Split_Percentage__c, 50);
            
            dsParent = [select Deal_Split_Percentage__c from Opportunity where Id = :dsParent.Id];           
            
            System.assertEquals(dsParent.Deal_Split_Percentage__c, 50);
            
            Test.stopTest();
        }
        
    }
        
    public static testMethod void TestDealSplitCheckAmountOnAddingNewChild()
    {
        InsertCustomSettings();
        SetUpData();
        Test.startTest();
        Semaphores.TriggerHasRun('OppUpdateContactSOIisAfterisInsert');
        Semaphores.TriggerHasRun('OppUpdateContactSOIisAfterisUpdate');
        Semaphores.TriggerHasRun('OppUpdateContactSOIisBeforeisInsert');
        Semaphores.TriggerHasRun('OppUpdateContactSOIisBeforeisUpdate');
        Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisAfterisInsert');
        Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisBeforeisInsert');
        Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisAfterisUpdate');
        Semaphores.TriggerHasRun('Opp_OnlyBDRManagerCanChangeBDRCreatedGoalDiscoveryisBeforeisUpdate');
        Semaphores.AccountSharingRulesOnPartnerPortalAccountsHasRun = true;        
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After = true;
        Semaphores.Opportunity_ManageForecastProductsIsInsert = true;
        Semaphores.Opportunity_ExecDealsVisibility_Insert = true;
        Semaphores.Opportunity_ExecDealsVisibility_Update = true;
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        Semaphores.OpportunityTriggerBeforeInsert = false;
        Semaphores.OpportunityTriggerAfterInsert = false;
        ApexSharingRules.TestingOpportunityShare = false;
        Semaphores.DealSplitUpdateHasRun = false;
        
        Opportunity newDSChild = QTTestUtils.createOpportunity('Child2 Tests', 'DS Child2 test Opp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account, dsParent, 50);
        //System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            
            
            System.assertEquals(newDSChild.Deal_Split_Percentage__c, 50);
            
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 10000);
            
            newDSChild = [select Amount from Opportunity where Id = :newDSChild.Id];
            System.assertEquals(newDSChild.Amount, 50000);
            
            
        }
        Test.stopTest(); 
    }
   
    public static testMethod void TestDealSplitCheckDealSplitPercentage()
    {
        InsertCustomSettings();
        SetUpData();
        System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            Test.startTest();
            dsChild1.Deal_Split_Percentage__c = 30;
            Semaphores.DealSplitUpdateHasRun = false;
            Semaphores.OpportunityTriggerBeforeUpdate = false;
            Semaphores.OpportunityTriggerAfterUpdate = false;
            update dsChild1;
            
            Opportunity dsParent1 = [select Id, Deal_Split_Percentage__c from Opportunity where Id = :dsParent.Id];           
            
            System.assertEquals(dsParent1.Deal_Split_Percentage__c, 30);
            
            dsChild1 = [select Amount from Opportunity where Id = :dsChild1.Id];
            System.assertEquals(dsChild1.Amount, 30000);
            
            Test.stopTest();
        }
    }
    
    public static testMethod void TestDealSplitCheckChildNavisionInvoiceNumber()
    {
        InsertCustomSettings();
        SetUpData();
            
        Test.startTest();
        dsParent.Navision_Invoice_Number__c = '1234567';
        Semaphores.DealSplitUpdateHasRun = false;
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        update dsParent;
        
        dsChild1 = [select Navision_Invoice_Number__c from Opportunity where Id = :dsChild1.Id];
        System.assertEquals(dsChild1.Navision_Invoice_Number__c, '1234567');
        
        Test.stopTest();        
    } 
    
    public static testMethod void TestAccountISEROnOpportunityClose()
    {
        InsertCustomSettings();

        user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.CreateAccountForInternalUsers('Deal Split Test Account', user);    
        String OppRecordTypeId2 = '012D0000000KEKOIA4';
        RecordType rTypeOpp = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');    
        dsParent = QuoteTestHelper.createOpportunity(account, '', rTypeOpp);
        insert dsParent;
        //createOpportunity('DSTests', 'DS Parent test Opp', 'New Customer', 'Direct', 'Deal Split Create', OppRecordTypeId, 'GBP', user, account);

        dsParent.StageName = 'Deal Split-Closed Won';
        Semaphores.OppUpdateAccOwnerNameHasRun = false;
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        update dsParent;
        
        Test.startTest();
        
        Opportunity dsParent1 = [select Id, 
                                        Account_ISER__c, 
                                        Account_ISER_at_Closed_Won__c, 
                                        Account_ISER_at_Closed_Won_ID__c 
                                from Opportunity 
                                where Id = :dsParent.Id];           
        
        System.assertEquals(dsParent1.Account_ISER__c, User.Id);
        System.assertEquals(dsParent1.Account_ISER_at_Closed_Won__c, 'Testing');            
        System.assertEquals(dsParent1.Account_ISER_at_Closed_Won_ID__c, User.Id);
        Test.stopTest();        
    }
  
    private static void SetUpData()
    {
		user = QTTestUtils.createMockOperationsAdministrator();//createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.createMockAccount('Deal Split Test Account', user);
        contact = QTTestUtils.createMockContact(account.id);
		/*
        user = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Alliances Manager');
        account = QTTestUtils.CreateAccountForInternalUsers('Deal Split Test Account', user);  
		*/                                    
        dsParent = createOpportunity('DSTests', 'DS Parent test Opp', 'New Customer', 'Direct', 'Deal Split Create', OppRecordTypeId, 'GBP', user, account);
        Semaphores.DealSplitUpdateHasRun = false;
        Semaphores.OpportunityTriggerBeforeUpdate = false;
        Semaphores.OpportunityTriggerAfterUpdate = false;
        Semaphores.OpportunityTriggerBeforeInsert = false;
        Semaphores.OpportunityTriggerAfterInsert = false;
        
        dsChild1 = QTTestUtils.createOpportunity('Child1 Tests', 'DS Child1 test Opp', 'New Customer', 'Direct', 'Goal Identified', '01220000000DoEj', 'GBP', user, account, dsParent, 10);
        System.assertEquals(dsChild1.StageName, 'Goal Identified');  
    }
    
    private static Opportunity createOpportunity(String sShortDescription, String sName, String sType, String sRevenueType, String sStage, Id OppRecordId, String sCurrencyIsoCode, User user, Account acc)
    {
        Opportunity opp = New Opportunity (
                Short_Description__c = sShortDescription,
                Name = sName,
                Type = sType,
                Revenue_Type__c = sRevenueType,
                CloseDate = Date.today(),
                StageName = sStage, 
                RecordTypeId = OppRecordId, 
                CurrencyIsoCode = sCurrencyIsoCode,
                AccountId = acc.Id,
                Split_Opportunity__c = true,
                Signature_Type__c = 'Digital Signature',
                Amount = 100000
                ); 
           insert opp;
           return opp;
    }
    static User user;
    static Account account;
    static Contact contact;
    static Opportunity dsParent;
    static Opportunity dsChild1;
    static Opportunity dsChild2;

    public static void InsertCustomSettings()
    {
		System.runAs(QTTestUtils.createMockOperationsAdministrator())
        {
            NS_Settings_Detail__c settings = new NS_Settings_Detail__c();
            settings.Name = 'NSSettingsDetail';
            settings.Qlikbuy_II_Opp_Record_Type__c = '012D0000000KEKO';
            settings.NSPricebookRecordTypeId__c = '01sD0000000JOeZ';
            settings.UserRecordTypeIdsToExclude__c = '005D0000002URF0';
            settings.RecordTypesToExclude__c = '012D0000000KB2N,01220000000Hc6G,01220000000DNwY,01220000000J1KR,01220000000DugI,01220000000Dmf5, 012f00000008xHQ';
            insert settings;
    
            QTCustomSettings__c settings2 = new QTCustomSettings__c();
            settings2.Name = 'Default';
            settings2.eCustoms_QlikBuy_RecordTypes__c = '01220000000J1KQAA0,01220000000J1KRAA0,012D0000000KEKO';
            settings2.RecordTypesForecastOmitted__c = '01220000000DNwZAAW';
            settings2.OppRecTypesAllow0s__c = '012f00000008xHQAAY';
            settings2.OEM_Record_Types__c = '01220000000DXq5AAG,01220000000DXqZAAW,01220000000DXqjAAG,012D0000000JpIxIAK,01220000000DNwZAAW';
            insert settings2;
			Steelbrick_Settings__c cs3 = new Steelbrick_Settings__c();
              cs3.Name = 'SteelbrickSettingsDetails';
              cs3.SBPricebookId__c = '01s20000000E0PWAA0';
			  cs3.Steelbrick_OppRecordType__c = /*'012260000000YTi'*/QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
           insert cs3;
          
        }

    }
	/*
    static Id OppRecordTypeId
    {
        get
        {
            if(NS_Settings_Detail__c.getInstance('NSSettingsDetail') != null) {
                return  NS_Settings_Detail__c.getInstance('NSSettingsDetail').Qlikbuy_II_Opp_Record_Type__c;        
            }
            else
                return '012D0000000KEKO';
        }
    }
	*/
	static Id OppRecordTypeId
    {
        get
        {	
			
		
    		if(Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails') !=null){
				return Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').Steelbrick_OppRecordType__c;
			}else { return null;}
			//return QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;
			//return '012D0000000KEKOIA4';
			
        }         
    }
	
}