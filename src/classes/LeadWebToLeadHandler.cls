/**
* Class created 11-04-2018 AYS BMW-721
*/

public with sharing class LeadWebToLeadHandler {

    public static void handle(List<Lead> triggerNew) {

    	System.debug('LeadWebToLeadHandler Started');

    	Lead_Settings__c lSettings = Lead_Settings__c.getInstance();//.BoomiIntegrationUserId__c;

        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;
 
        List<Lead> l_LeadsCopy = new List<Lead>();
   
        for(Lead lead : triggerNew) {
            if(	UserInfo.getUserId() == lSettings.User_Boomi_Integration__c && 
            	lead.LeadSource == lSettings.Source_Web_Activity__c && 
            	lead.RecordTypeId == lSettings.RT_Standard_lead__c
            	){

            	System.debug('debug_lead.OwnerId ' + lead.OwnerId);
            	
                Lead leadCopy = lead.clone(true, true, false, false);
                leadCopy.setOptions(dmo);
                l_LeadsCopy.add(leadCopy);
            }            
        }
       
        update l_LeadsCopy;
    }

}