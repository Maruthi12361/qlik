/**************************************************
* CR# 20060 – Add Predictive Lead Score Grade and Overall Priority
* Change Log:
* 2015-03-12 Madhav Kakani: Test case for ContactScore_Trigger
* 2015-06-11 CCE Added test for clearing of follow-up flags (testContactScoreTrigger_ClearFU_RequiredFlags)
* 2015-07-13 CCE/Madhav Kakani CR# 30746 - Create New Predictive Lead Score Field and Revise Grade changes
* 2016-04-21 CCE CR# 82611 - Replacing Lead Status value "Follow-Up Accepted" with new value "Follow-Up Attempt 1"
* 2016-10-20 CCE CR# 95899 - Removing testmethod testContactScoreTrigger as no longer required as code it was testing has been removed from the trigger
* 2018-03-29 CCE CHG0033445 - Add test for "Set Conversica options field to Stop (case sensitive)"
* 2018-10-12 CCE CHG0034825 BMW-1054 Update test due to code changes for this CHG
* 2018-10-12 CCE CHG0034697 BMW-1015 Update test due to code changes for this CHG
**************************************************/
@isTest
private class ContactScore_TriggerTest {
    //static testmethod void testContactScoreTrigger(){
    //    User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

    //    System.RunAs(mockSysAdmin) {                      
    //        Account acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
    //        system.assert(acct.Id != null);
    //        Contact ct = QTTestUtils.createMockContact(acct.Id);
    //        system.assert(ct.Id != null);
            
    //        Test.startTest();

    //        Semaphores.ContactScoreTrigger_HasRunBefore = false;
    //        ct.Predictive_Lead_Score__c = 96;
    //        ct.Contact_Status__c = 'Follow-Up Rejected';
    //        update ct;
    //        Contact ctTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Contact WHERE Id=:ct.Id];
    //        system.assert(ctTmp.Predictive_Lead_Score_Grade__c == 'A');
    //        system.assert(ctTmp.Predictive_Lead_Score_Numeric_Grade__c == '5');

    //        Semaphores.ContactScoreTrigger_HasRunBefore = false;
    //        ct.Predictive_Lead_Score__c = 82;
    //        update ct;
    //        ctTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Contact WHERE Id=:ct.Id];
    //        system.assert(ctTmp.Predictive_Lead_Score_Grade__c == 'B');
    //        system.assert(ctTmp.Predictive_Lead_Score_Numeric_Grade__c == '4');

    //        Semaphores.ContactScoreTrigger_HasRunBefore = false;
    //        ct.Predictive_Lead_Score__c = 50;
    //        update ct;
    //        ctTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Contact WHERE Id=:ct.Id];
    //        system.assert(ctTmp.Predictive_Lead_Score_Grade__c == 'C');
    //        system.assert(ctTmp.Predictive_Lead_Score_Numeric_Grade__c == '3');

    //        Semaphores.ContactScoreTrigger_HasRunBefore = false;
    //        ct.Predictive_Lead_Score__c = 1;
    //        update ct;
    //        ctTmp = [SELECT Predictive_Lead_Score_Grade__c, Predictive_Lead_Score_Numeric_Grade__c FROM Contact WHERE Id=:ct.Id];
    //        system.assert(ctTmp.Predictive_Lead_Score_Grade__c == 'D');
    //        system.assert(ctTmp.Predictive_Lead_Score_Numeric_Grade__c == '1');

    //        Test.stopTest();
    //    }
    //}

    static testmethod void testContactScoreTrigger_ClearFU_RequiredFlags(){
        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

        System.RunAs(mockSysAdmin) { 
            Account acct = QTTestUtils.createMockAccount('Test Account', mockSysAdmin);
            system.assert(acct.Id != null);
            Contact ct = QTTestUtils.createMockContact(acct.Id);
            system.assert(ct.Id != null);
            //Set Contact to required start state
            ct.Contact_Status__c = 'Follow-Up Attempt 5';
            ct.Event_Follow_Up_Required__c = true;
            ct.Existing_Follow_Up_Required__c = true;
            ct.New_Follow_Up_Required__c = true;
            ct.AVA__AVAAI_action_required__c = true;
            ct.Number_of_Follow_Up_Voicemails__c = 5;
            ct.Number_of_Follow_Up_Emails__c = 1;
            update ct;

            Contact ctTmp = [SELECT Overall_Follow_up_Required__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.Overall_Follow_up_Required__c == true);
            
            Test.startTest();
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            ct.Number_of_Follow_Up_Voicemails__c = 6;
            //ct.Contact_Status__c = 'Contacted-Additional Work Required';
            //ct.Follow_Up_Disqualified_Reason__c = 'No Response';
            update ct;
            
            ctTmp = [SELECT Event_Follow_Up_Required__c, Existing_Follow_Up_Required__c, New_Follow_Up_Required__c, Overall_Follow_up_Required__c, AVA__AVAAI_options__c FROM Contact WHERE Id=:ct.Id];
            system.assert(ctTmp.Event_Follow_Up_Required__c == false);
            system.assert(ctTmp.Existing_Follow_Up_Required__c == false);
            system.assert(ctTmp.New_Follow_Up_Required__c == false);
            system.assert(ctTmp.Overall_Follow_up_Required__c == false);
            system.assert(ctTmp.AVA__AVAAI_options__c == 'Stop');
            
            Test.stopTest();
        }
    }

}