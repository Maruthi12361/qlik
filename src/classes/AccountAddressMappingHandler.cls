/**     * File Name:AccountAddressMappingHandler
        * Description : This handler class is used to map the billing to territory address or vice versa on account creation or updation
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Sept 30th 2016 RamakrishnaKini     Added new trigger logic.
        1.1 28 Sept 2017 Shubham Gupta QCW-3971 added logic for UK field MApping fix---- line 141
*/
public class AccountAddressMappingHandler {
	
	static Map<Id, Account> mIdAccount = new Map<Id, Account>();
    static List<Account> lBillAddrUpdAccs = new List<Account>();
    static List<Account> lTerrAddrUpdAccs = new List<Account>();
	
	public AccountAddressMappingHandler() {
		
	}

	/*  @Description :This common method is used to update territory or billing address fields on account on before insert trigger context.
    
        @parameter inputList: Trigger.new Account list
   
    */
	public static void onBeforeInsert(List<Account> inputList) {

		for(Account acc: inputList){
			if(String.isNotBlank(acc.Territory_Country__c) || String.isNotBlank(acc.Territory_Zip__c) || String.isNotBlank(acc.Territory_city__c)
				|| String.isNotBlank(acc.Territory_street__c) || String.isNotBlank(acc.Teritory_State_Province__c) || String.isNotBlank(acc.Territory_State_Province_free_input__c)){
				lBillAddrUpdAccs.add(acc);
			}else if(String.isNotBlank(acc.BillingCountry) || String.isNotBlank(acc.BillingPostalCode) || String.isNotBlank(acc.BillingCity)
				|| String.isNotBlank(acc.BillingStreet) || String.isNotBlank(acc.BillingState)){
				lTerrAddrUpdAccs.add(acc);
			} 
		}
		if(!lBillAddrUpdAccs.isEmpty())
			updAccountBillingAddress(lBillAddrUpdAccs); 
		if(!lTerrAddrUpdAccs.isEmpty())
		 	updAccountTerritoryAddress(lTerrAddrUpdAccs);
	 	lBillAddrUpdAccs.clear();
	 	lTerrAddrUpdAccs.clear();     
    }
    
    /*  @Description :This common method is used to update territory or billing address fields on account on before update trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap 
        @parameter oInputList: Trigger.old Account list  
    */
    public static void onBeforeUpdate(List<Account> inputList, Map<id, Account> inputMap, Map<id, Account> oInputMap) { 
    	for(Account acc: inputList){
			if((acc.Territory_Country__c != oInputMap.get(acc.Id).Territory_Country__c && acc.Territory_Country__c != acc.BillingCountry) || 
				(acc.Territory_street__c != oInputMap.get(acc.Id).Territory_street__c  && acc.Territory_street__c != acc.BillingStreet) || 
					(acc.Territory_Zip__c != oInputMap.get(acc.Id).Territory_Zip__c  && acc.Territory_Zip__c != acc.BillingPostalCode) ||
						(acc.Territory_State_Province_free_input__c != oInputMap.get(acc.Id).Territory_State_Province_free_input__c  && acc.Territory_State_Province_free_input__c != acc.BillingState) || 
							(acc.Teritory_State_Province__c != oInputMap.get(acc.Id).Teritory_State_Province__c && acc.Teritory_State_Province__c != acc.BillingState) || 
								(acc.Territory_city__c != oInputMap.get(acc.Id).Territory_city__c  && acc.Territory_city__c != acc.BillingCity)){
				lBillAddrUpdAccs.add(acc);
			}else if((acc.BillingCountry != oInputMap.get(acc.Id).BillingCountry && acc.Territory_Country__c != acc.BillingCountry) || 
						(acc.BillingStreet != oInputMap.get(acc.Id).BillingStreet  && acc.Territory_street__c != acc.BillingStreet) || 
							(acc.BillingPostalCode != oInputMap.get(acc.Id).BillingPostalCode  && acc.Territory_Zip__c != acc.BillingPostalCode) ||
								(acc.BillingState != oInputMap.get(acc.Id).BillingState  && ((acc.Territory_State_Province_free_input__c != acc.BillingState)  || 
									(acc.Teritory_State_Province__c != acc.BillingState))) || 
										(acc.BillingCity != oInputMap.get(acc.Id).BillingCity  && acc.Territory_city__c != acc.BillingCity)){
				lTerrAddrUpdAccs.add(acc);
			}
		}
		if(!lBillAddrUpdAccs.isEmpty())
			updAccountBillingAddress(lBillAddrUpdAccs); 
		if(!lTerrAddrUpdAccs.isEmpty())
		 	updAccountTerritoryAddress(lTerrAddrUpdAccs);
	 	lBillAddrUpdAccs.clear();
	 	lTerrAddrUpdAccs.clear();     
    }

    
    /*  @Description :This common method is used to update territory or billing address fields on account on after update trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap   
    */
    public static void onAfterInsert(List<Account>  inputList, Map<id, Account> inputMap) {     
    }
    
    /*  @Description :This common method is used to update territory or billing address fields on account on after update trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap
        @parameter oInputList: Trigger.old Account list
        @parameter oInputMap: oInputMap     
    */
    public static void onAfterUpdate(List<Account>  inputList, Map<id, Account> inputMap, List<Account>  oInputList, Map<id, Account> oInputMap) {
  
    }
    
    /*  @Description :This common method is used to update territory or billing address fields on account on after delete trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap  
    */
    public static void onAfterDelete(List<Account>  inputList, Map<id, Account> inputMap) {
    }

    /**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/

    /*  @Description :This common method is used to update billing address fields on account.
    
        @parameter lAccounts: Account list
    */
    private static void updAccountBillingAddress(List<Account> lAccounts){
    	for(Account acc: lAccounts){
			if(!String.isBlank(acc.Territory_city__c))
				acc.BillingCity = acc.Territory_city__c;
			if(!String.isBlank(acc.Territory_Country__c)){
				acc.BillingCountry = acc.Territory_Country__c;
			}
			if(!String.isBlank(acc.Territory_street__c))
				acc.BillingStreet = acc.Territory_street__c;
			if(!String.isBlank(acc.Territory_Zip__c))
				acc.BillingPostalCode = acc.Territory_Zip__c;
			if(!String.isBlank(acc.Teritory_State_Province__c))
				acc.BillingState = acc.Teritory_State_Province__c;
			else if(!String.isBlank(acc.Territory_State_Province_free_input__c))
				acc.BillingState = acc.Territory_State_Province_free_input__c;
    	}
    }

    /*  @Description :This common method is used to update territory address fields on account.
    
        @parameter lAccounts: Account list
    */
    private static void updAccountTerritoryAddress(List<Account> lAccounts){
    	for(Account acc: lAccounts){
			if(!String.isBlank(acc.BillingCity))
				acc.Territory_city__c = acc.BillingCity;
			if(!String.isBlank(acc.BillingCountry)){
				if('USA'.equalsIgnoreCase(acc.BillingCountry))
					acc.Territory_Country__c = 'United States';
				else if('UK'.equalsIgnoreCase(acc.BillingCountry))
					acc.Territory_Country__c = 'United Kingdom';
				else
					acc.Territory_Country__c = acc.BillingCountry;
			}
			if(!String.isBlank(acc.BillingStreet))
				acc.Territory_street__c = acc.BillingStreet;
			if(!String.isBlank(acc.BillingPostalCode))
				acc.Territory_Zip__c = acc.BillingPostalCode;
			if(!String.isBlank(acc.BillingState)) {
				acc.Teritory_State_Province__c = acc.BillingState;
				acc.Territory_State_Province_free_input__c = acc.BillingState;
			}
    	}
    }

}