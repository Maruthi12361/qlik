/****************************************************************
*
*  Class: LeadPRMReferralConfirmAcceptFieldHandler
*
*  Changelog:
*  	2017-10-25 AYS BMW-402 : Migrated from Lead_PRMReferral_ConfirmLeadAcceptedField.trigger.
* 
*****************************************************************/

public class LeadPRMReferralConfirmAcceptFieldHandler {
	public LeadPRMReferralConfirmAcceptFieldHandler() {
		
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
		//Check if its a Referral Lead and the Lead Accepted field has been checked otherwise show an error.
	    for (integer i = 0; i < triggerNew.size(); i++)
	    {
	        Lead NewLead = triggerNew[i];
	        Lead OldLead = triggerOld[i];

	        if ((NewLead.isConverted == true) && (OldLead.isConverted == false) && (NewLead.RecordTypeId == '012D0000000K781IAC') && (NewLead.Lead_Accepted__c == false))	//PRM Referral Opp Reg record type qttest=012L00000004JyJ live=012D0000000K781IAC
	        {
	        	NewLead.addError('Cannot convert Referral lead until Lead Accepted is ticked. Please Cancel the conversion and tick Lead Accepted');
	        	if(Test.isRunningTest()) throw new CustomException('Cannot convert Referral lead until Lead Accepted is ticked. Please Cancel the conversion and tick Lead Accepted');
	        }
	    }
	}
}