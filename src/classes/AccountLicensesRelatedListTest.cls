/********
* NAME :AccountLicensesRelatedListTest
* Description: Tests for Helper class to populate AccountLicensesRelatedList
*
*Change Log:
    IRN      2016-02-03   Partner share project Created test class
    IRN      2016-03-18   Added test for scheduable batch job
    Pramod Kumar V 13-1-2017  Test class coverage
******/
@isTest
private class AccountLicensesRelatedListTest{
    
    private static void Setup(){
      //create contract
      NS_Support_Contract__c contract = new NS_Support_Contract__c();
      insert contract;
      List<Account_License__c> licenseList = new List<Account_License__c>();
      Account_License__c license1 = new Account_License__c();
      license1.Name = 'test1';
      licenseList.add(license1);
      Account_License__c license2 = new Account_License__c();
      license2.Name = 'test2';
      licenseList.add(license2);
      insert licenseList;

      List<NS_Support_Contract_Item__c> items = new List<NS_Support_Contract_Item__c> ();
      NS_Support_Contract_Item__c item1 = new NS_Support_Contract_Item__c();
    item1.NS_Support_Contract__c = contract.Id;
    item1.Contract_Item_Account_License__c = license1.id;
    items.add(item1);
    
    NS_Support_Contract_Item__c item2 = new NS_Support_Contract_Item__c();
    item2.NS_Support_Contract__c = contract.Id;
    item2.Contract_Item_Account_License__c = license1.id;
    items.add(item2);
    
    NS_Support_Contract_Item__c item3 = new NS_Support_Contract_Item__c();
    item3.NS_Support_Contract__c = contract.Id;
    item3.Contract_Item_Account_License__c = license2.id;
    items.add(item3);

    insert items;
    }
     
     private static testMethod void TestPopulateList(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
         Setup();
         List<NS_Support_Contract_Item__c> items = [Select Contract_Item_Account_License__c, NS_Support_Contract__c, Account_License_for_RL__c from NS_Support_Contract_Item__c];
         System.assertEquals(items.size(), 3);
         Map<String, Integer> accountLicensesCountMap = new Map<String, Integer>();
         for(Integer i = 0; i<items.size(); i++){
           String key = items[i].Contract_Item_Account_License__c + ' ' +items[i].NS_Support_Contract__c;
           if(!accountLicensesCountMap.containsKey(key)){
             if(items[i].Account_License_for_RL__c != null){
               accountLicensesCountMap.put(key, 1);  
             }  
           }else{
             if(items[i].Account_License_for_RL__c != null){
               Integer nbr = accountLicensesCountMap.get(key);  
               nbr = nbr +1;
             }
           }
         }

         for(Integer count :accountLicensesCountMap.values()){
           System.assertEquals(count, 1);
         }

        test.stopTest();
    }

    private static testMethod void TestBatchJob(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
         Setup();
         AccountLicensesRelatedList alrl = new AccountLicensesRelatedList();
         List<NS_Support_Contract_Item__c> scope =[Select Id, Contract_Item_Account_License__c, Account_License_for_RL__c, NS_Support_Contract__c from NS_Support_Contract_Item__c where Contract_Item_Account_License__c != null order by Contract_Item_Account_License__c];
         alrl.execute(null, scope);
          //ID batchId = Database.executeBatch(alrl);
          //Pramod
         Database.executeBatch(alrl);
         List<NS_Support_Contract_Item__c> items = [Select Contract_Item_Account_License__c, NS_Support_Contract__c, Account_License_for_RL__c from NS_Support_Contract_Item__c];
         System.assertEquals(items.size(), 3);
         Map<String, Integer> accountLicensesCountMap = new Map<String, Integer>();
         for(Integer i = 0; i<items.size(); i++){
           String key = items[i].Contract_Item_Account_License__c + ' ' +items[i].NS_Support_Contract__c;
           if(!accountLicensesCountMap.containsKey(key)){
             if(items[i].Account_License_for_RL__c != null){
               accountLicensesCountMap.put(key, 1);  
             }  
           }else{
             if(items[i].Account_License_for_RL__c != null){
               Integer nbr = accountLicensesCountMap.get(key);  
               nbr = nbr +1;
             }
           }
         }

         for(Integer count :accountLicensesCountMap.values()){
           System.assertEquals(count, 1);
         }

        test.stopTest();
    }

     private static testMethod void TestSendEmail(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Setup();
        AccountLicensesRelatedList alrl = new AccountLicensesRelatedList();
        DmlException  e = new DmlException ();
        e.setMessage('test');
        List<NS_Support_Contract_Item__c> scope =[Select Id, Contract_Item_Account_License__c, Account_License_for_RL__c, NS_Support_Contract__c from NS_Support_Contract_Item__c where Contract_Item_Account_License__c != null order by Contract_Item_Account_License__c];
        String[] toAddresses = new String[] {'test@gmail.com'};
        alrl.sendEmail(e, scope ,toAddresses);
        test.stopTest();
    }
}