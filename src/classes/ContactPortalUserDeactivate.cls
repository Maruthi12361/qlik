public with sharing class ContactPortalUserDeactivate {
    
    private Contact contact;
    private ApexPages.StandardController controller;
    
    public ContactPortalUserDeactivate(ApexPages.StandardController controller) {
        this.controller = controller; 
        contact = (Contact) controller.getRecord();
    }
    
    public PageReference deactivatePortalUser() {       
        //Username, email, alias, community nickname needs to be obscured (usually we add current day & year to the fields), 
        //federation id needs to be emptied. User object should be deactivated.
        List<User> users = [SELECT Id, Username, Email, Alias, CommunityNickname, FederationIdentifier
                            FROM User WHERE ContactId = :contact.Id 
                            AND IsPortalEnabled = true AND IsActive = true
                            ];
        
        if (users.size() > 0) deactivateUser(users[0]); 
        
        return controller.cancel();
    }
    
    public static void deactivateUser(User user) {
        String timeStamp = '('+String.valueof(Date.today())+')';
        String objectName = 'User';
        user.Username = combineAndTruncateIfNecessary(user.Username, fieldLength(objectName, 'Username'), timeStamp);
        user.Email = combineAndTruncateIfNecessary(user.Email, fieldLength(objectName, 'Email'), timeStamp);
        
        user.Alias = combineAndTruncateIfNecessary(user.Alias, fieldLength(objectName, 'Alias'), timeStamp);
        user.CommunityNickname = combineAndTruncateIfNecessary(user.CommunityNickname, fieldLength(objectName, 'CommunityNickname'), timeStamp);
        user.FederationIdentifier = '';
        //the field ContactId is not writeable
        //user.ContactId = null;
        user.IsActive = false;
        user.IsPortalEnabled = false;
        update user;
    }
    
    public static String combineAndTruncateIfNecessary(String s, Integer maxLength, String addition) {
        if (maxLength >= s.length() + addition.length()) {
            return s + addition;
        }
        Integer charsToTruncate = s.length() + addition.length() - maxLength;
        Integer firstStringSize = s.length() - charsToTruncate;
        Integer additionStringSize = firstStringSize < 0 ? addition.length() + firstStringSize : addition.length();
        if (additionStringSize < 0) return '';
                
        if (firstStringSize > 0) return s.substring(0, firstStringSize) + addition;
        else if (additionStringSize > 0) return addition.substring(0, additionStringSize);
        else return s + addition; 
    }
    
    private static Integer fieldLength(String objectName, String field) {
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        if (objectType == null) return -1;
        Schema.SObjectField ofield = objectType.getDescribe().fields.getMap().get(field);
        if (ofield != null) return ofield.getDescribe().getLength();
        else return -1;
    }
}