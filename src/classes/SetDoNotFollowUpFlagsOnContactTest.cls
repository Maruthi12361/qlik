/***************************************
Class: SetDoNotFollowUpFlagsOnContactTest
Description: Test class for SetDoNotFollowUpFlagsOnContact

Changes Log:
2018-05-24	CCE		Initial code: BMW-848 CHG0033967
					
****************************************/
@isTest
private class SetDoNotFollowUpFlagsOnContactTest {
	
	@isTest static void test_method_one() {
		List<Id> contacts = new List<Id>();
		Contact testUserContact = QTTestUtils.createMockContact();
		contacts.add(testUserContact.Id);
		SetDoNotFollowUpFlagsOnContact.setDoNotFollowUpFlags(contacts);
		Contact cRet = [SELECT DoNotCall, Marketing_Suspended__c, Marketing_Suspended_Date__c, Marketing_Suspended_Reason__c FROM Contact WHERE Id = :testUserContact.Id];
		System.assertEquals(true, cRet.Marketing_Suspended__c);
	}
	
}