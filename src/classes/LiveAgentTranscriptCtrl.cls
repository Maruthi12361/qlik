// Custom controller class for the Live_Agent_Transcript visualforce page
// CR# 11434 March 07, 2014 - Madhav Kakani - Fluido Oy - Initial Implementation
// LCE-35 2018-01-30 - AIN - Changes to the post chat survey
// LCE-53 2018-02-04 - AIN - Changes to the post chat survey
// LCE-63 2018-02-04 - ext_bad - Add case number to the saved file
// IT-1897 - 29.05.2019 - extbad  Update 'Close Chat Survey Id'
// IT-2124 - 03.09.2019 - extciz Update survey link

public class LiveAgentTranscriptCtrl {
    public static final String CHAT_NUMBER_LABEL = 'If you need to reference the chat in the future, your chat number is: ';
    public static final String GUEST_USER_ID = '005D0000004wTRsIAM';

    public String sUserId {get;set;}
    public Live_Agent_Transcript__c lat {get;set;}
    public String attachedRecords {get;set;}
    public string caseId {get;set;}
    public string caseNumber {get;set;}
    private Case caseObj {get;set;}
    public string caseURL {get;set;}
    public boolean isSandbox {get;set;}
    public string surveyId {get;set;}
    public boolean isGuest{get;set;}
    public string surveyLink{get;set;}
                
    public LiveAgentTranscriptCtrl() {
        isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        QTCustomSettingsHier__c qsCSettingsHier = QTCustomSettingsHier__c.getInstance();
        surveyId = isSandbox ? qsCSettingsHier.Close_Chat_Survey_Id_QA__c : qsCSettingsHier.Close_Chat_Survey_Id__c;
        surveyLink = isSandbox ? qsCSettingsHier.Survey_Link_For_Live_Chat_QA__c : qsCSettingsHier.Survey_Link_For_Live_Chat__c;
        caseId = '';
        attachedRecords = ApexPages.currentPage().getParameters().get('attachedRecords');
        if (attachedRecords == null) {
            return;
        }
        JSONParser parser = JSON.createParser(attachedRecords);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'CaseId')) {
                parser.nextToken();
                caseId = parser.getText();
                break;
            }
        }
        if (caseId != '') {
            List<Case> cases = [select id, CaseNumber from Case where id = :caseId];
            if (cases.size() > 0) {
                caseObj = cases[0];
                caseUrl = Page.QS_CaseDetails + '?caseid='+caseId;
                caseNumber = caseObj.CaseNumber;
            }
        }
        isGuest = UserInfo.getUserId() == GUEST_USER_ID;
    }

    public PageReference saveTranscript() {
        String t = ApexPages.currentPage().getParameters().get('transcript');
        String ck = ApexPages.currentPage().getParameters().get('chatkey');

        if (t != null) { // OK this is called from the live agent post chat visualforce page
            // save the transcript in a temporary object
            Live_Agent_Transcript__c latNew = new Live_Agent_Transcript__c();
            latNew.Name = ck;
            latNew.Transcript__c = CHAT_NUMBER_LABEL + caseNumber + '\r\n\r\n' + t;
            insert latNew;

            String cd = ApexPages.currentPage().getParameters().get('chatDetails');
            if (cd != null) {
                JSONParser parser = JSON.createParser(cd);
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'userid')) {
                        parser.nextToken();
                        sUserId = parser.getText();
                        break;
                    }
                }
            }
        } else { // OK this is called from the live agent transcript visualforce page
            List<Live_Agent_Transcript__c> lstLat = [SELECT Id, Transcript__c FROM Live_Agent_Transcript__c WHERE Name=:ck];
            if (!lstLat.isEmpty()) {
                lat = lstLat[0];
            }
        }
        return null;
    }

}