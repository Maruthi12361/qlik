/**************************************************
* Change Log:
* 2019-07-03 Test class for ApexDebugLog
* 2019-07-03 CCE Jira BMW-1595
**************************************************/
@isTest
public class ApexDebugLogTest{
    testMethod
    static void createErrorLog(){
        try{
            Integer result = 1 / 0;
        }
        catch(Exception ex){
            new ApexDebugLog().createLog(
                new ApexDebugLog.Error(
                    'ApexDebugLogTest',
                    'createErrorLog',
                    NULL,
                    ex
                )
            );

            List<Apex_Debug_Log__c> lstLogsCreated = [
                SELECT  Id, Type__c, Apex_Class__c, Method__c
                FROM    Apex_Debug_Log__c
                WHERE   Method__c = 'createErrorLog'
            ];

            System.assertEquals(1, lstLogsCreated.size());
            System.assertEquals('Error', lstLogsCreated.get(0).Type__c);
            System.assertEquals('ApexDebugLogTest', lstLogsCreated.get(0).Apex_Class__c);
        }
    }

    testMethod
    static void createInformationLog(){
        new ApexDebugLog().createLog(
            new ApexDebugLog.Information(
                'ApexDebugLogTest',
                'createInformationLog',
                NULL,
                'Logging Information from an Apex Class - ApexDebugLogTest'
            )
        );

        List<Apex_Debug_Log__c> lstLogsCreated = [
            SELECT  Id, Type__c, Apex_Class__c, Method__c, Message__c
            FROM    Apex_Debug_Log__c
            WHERE   Method__c = 'createInformationLog'
        ];

        System.assertEquals(1, lstLogsCreated.size());
        System.assertEquals('Information', lstLogsCreated.get(0).Type__c);
        System.assertEquals('ApexDebugLogTest', lstLogsCreated.get(0).Apex_Class__c);
        System.assertEquals('Logging Information from an Apex Class - ApexDebugLogTest', lstLogsCreated.get(0).Message__c);
    }

    testMethod
    static void ws_createErrorLog(){
        try{
            Integer result = 1 / 0;
        }
        catch(Exception ex){
            ApexDebugLog.createLog(
                '{"Type" : "Error","ApexClass" : "ApexDebugLogTest","Method" : "createErrorLog","RecordId" : "","Message" : "System.MathException: Divide by 0","StackTrace" : "Line: 1, Column: 1 System.MathException: Divide by 0"}'
            );

            List<Apex_Debug_Log__c> lstLogsCreated = [
                SELECT  Id, Type__c, Apex_Class__c, Method__c
                FROM    Apex_Debug_Log__c
                WHERE   Method__c = 'createErrorLog'
            ];

            System.assertEquals(1, lstLogsCreated.size());
            System.assertEquals('Error', lstLogsCreated.get(0).Type__c);
            System.assertEquals('ApexDebugLogTest', lstLogsCreated.get(0).Apex_Class__c);
        }
    }

    testMethod
    static void ws_createInformationLog(){
        ApexDebugLog.createLog(
            '{"Type" : "Information","ApexClass" : "ApexDebugLogTest","Method" : "createInformationLog","RecordId" : "","Message" : "Logging Information from an Apex Class - ApexDebugLogTest"}'
        );

        List<Apex_Debug_Log__c> lstLogsCreated = [
            SELECT  Id, Type__c, Apex_Class__c, Method__c, Message__c
            FROM    Apex_Debug_Log__c
            WHERE   Method__c = 'createInformationLog'
        ];

        System.assertEquals(1, lstLogsCreated.size());
        System.assertEquals('Information', lstLogsCreated.get(0).Type__c);
        System.assertEquals('ApexDebugLogTest', lstLogsCreated.get(0).Apex_Class__c);
        System.assertEquals('Logging Information from an Apex Class - ApexDebugLogTest', lstLogsCreated.get(0).Message__c);
    }
}