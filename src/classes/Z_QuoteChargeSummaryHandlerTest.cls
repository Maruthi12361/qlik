/*
BSL-1950 test Class by shubham Gupta to cover Z_QuoteChargeSummaryHandler
*/

@isTest
private class Z_QuoteChargeSummaryHandlerTest {
	
	@isTest static void test_QSPAdjusment_Population() {
		
        QuoteTestHelper.createCustomSettings();
        
        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Uniqueness level over 9000', '','65000-480');
        insert endUserAccount;
        endUserAccount.Pending_Validation__c =FALSE;
        update endUserAccount;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Revenue_Type__c = 'Direct';
        insert testOpp;

        zqu__Quote__c testQuote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        testQuote.zqu__BillToContact__c = endUserContact.id;
        testQuote.zqu__SoldToContact__c = endUserContact.id;
        testQuote.Quote_Status__c = 'Open';
        testQuote.zqu__Hidden_Subscription_Name__c = 'A-S00000663';
        insert testQuote;

        zqu__QuoteAmendment__c testamendment = Z_TestFactory.makeQuoteAmendment(testQuote);
        insert testamendment;

        Product2 prod = Z_TestFactory.makeProduct();
        prod.Name = 'S8071';
        insert prod;

        zqu__ProductRatePlan__c testProdRatePlan = Z_TestFactory.makeProductRatePlan(prod.Id,prod.zqu__ZuoraId__c);
        insert testProdRatePlan;

        zqu__QuoteRatePlan__c testRatePlan = Z_TestFactory.makeQRP(testQuote,testamendment,testProdRatePlan);
        testRatePlan.Name = 'Qlik Sense Enterprise User Model Site';
        insert testRatePlan;

        zqu__ProductRatePlanCharge__c testProdRPCharge = Z_TestFactory.makeProductRatePlanCharge(testProdRatePlan.id,'Recurring','Tiered Pricing','S8071');

        zqu__QuoteChargeSummary__c testsummary = Z_TestFactory.makeQCS(testQuote,testRatePlan,testProdRPCharge,'S8071');
        testsummary.zqu__Quantity__c = 5;
        insert testsummary;

        zqu__Quote__c testQuote1 = [select id,QSP_Adjustment_Quantity__c from zqu__Quote__c where id = :testQuote.id];

        system.assertEquals(testQuote1.QSP_Adjustment_Quantity__c,testsummary.zqu__Quantity__c);

        //.Parent_Quote__c = testQuote.id;
        //update testsummary;


	}
	
}