/**
* QS_EnvironmentLookupControllerTest 
*
* Change log:
*
* 2019-04-30 - AIN IT-1597 Created for Support Portal Redesign
*
*/
@isTest
public class QS_EnvironmentLookupControllerTest {
    static testMethod void test() {
    	Account a = new Account();
    	a.Name = 'Test account';
    	insert a;

    	List<Environment__c> environments = new List<Environment__c>();
    	Environment__c e1 = new Environment__c();
    	e1.Account__c = a.Id;
    	e1.Type__c = 'Production';

    	Environment__c e2 = new Environment__c();
    	e2.Account__c = a.Id;
    	e2.Type__c = 'Test';

    	Environment__c e3 = new Environment__c();
    	e3.Account__c = a.Id;
    	e3.Type__c = 'Development';

    	environments.add(e1);
    	environments.add(e2);
    	environments.add(e3);
    	insert environments;

        QS_EnvironmentLookupController controller = new QS_EnvironmentLookupController();
        controller.RefreshEnvironment();
        controller.getShowAccountColumn();
        Id testId = controller.resultsForAccount;
        testId = controller.selectedEnvironmentId ;


        System.currentPageReference().getParameters().put('accountId', a.id);
        controller = new QS_EnvironmentLookupController();
        //controller.showForAccount = a.id;
        controller.RefreshEnvironment();
    }
}