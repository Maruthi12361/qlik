/************************************************************************
 * @author  SAN
 * 2014-11-04 customer portal project
 *  
 * wrapper Class for License List View to help with multiple selections with checkboxes
 * 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
 *
 *************************************************************************/
public with sharing class QS_EntitlementWrapper {

    public Boolean checked{ get; set; }
    public Entitlement cat { get; set;}

    public QS_EntitlementWrapper(){
        cat = new Entitlement();
        checked = false;
    }

    public QS_EntitlementWrapper(Entitlement c){
        cat = c;
        checked = false;
    }

/*    public static testMethod void testMe() {

        QS_EntitlementWrapper cw = new QS_EntitlementWrapper();
        System.assertEquals(cw.checked,false);

        QS_EntitlementWrapper cw2 = new QS_EntitlementWrapper(new Entitlement(name='Test1'));
        System.assertEquals(cw2.cat.name,'Test1');
        System.assertEquals(cw2.checked,false);
    }*/

}