/********************************************************

ULC_DisableUserLeftCompanySchedUser
Scheduler for ULC_DisableUserLeftCompany
Runs the method DeactivateSFDCUsers which must be run in a different context from the rest.

ChangeLog:
2015-02-10	AIN 	Initial implementation
***********************************************************/

global with sharing class ULC_DisableUserLeftCompanySchedUser implements Schedulable {
	
	global static void execute (SchedulableContext sc)
	{
		ULC_DisableUserLeftCompany.DeactivateSFDCUsers();
	}
}