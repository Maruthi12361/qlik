/****************************************************************
*
*  TestRedirectorAndValidatorContr
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*  17.03.2017 : Rodion Vakulvsokyi
*  23.03.2017 : Rodion Vakulvsokyi fixed test coverage
*  24.03.2017 Rodion Vakulovskyi
*  22.09.2017 Sergii Grushai fixed test coverage
*  07.02.2018 : Reshma - Fix for validation error QCW-4968
*****************************************************************/
@isTest
public class TestRedirectorAndValidatorContr{
    static final Id EndUserAccRecTypeId = '01220000000DOFuAAO';
    static final RecordType oppRecordType = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
    static final integer NBR_OF_ACCOUNTS = 10;
    static final RecordType internalNfrQuote = QuoteTestHelper.getRecordTypebyDevName('Internal_NFR_Quote');
    static final RecordType quoteRecordType = QuoteTestHelper.getRecordTypebyDevName('Quote');

    @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
    }

    @isTest
    public static void testWithoutOpp() {

        Account testAccount = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        testAccount.RecordtypeId = EndUserAccRecTypeId;
        testAccount.BillingCountry = 'France';
        update testAccount;

        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{
            QuoteTestHelper.createQlikTech('France', 'FR', testSubs.id),
            QuoteTestHelper.createQlikTech('Afghanistan', 'AF', testSubs.id),
            QuoteTestHelper.createQlikTech('Japan', 'JP', testSubs.id),
            QuoteTestHelper.createQlikTech('Albania', 'AL', testSubs.id)
        };
        insert listOfcreate;

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        insert testContact;

        Test.setCurrentPageReference(new PageReference('Page.RedirectorAndValidator'));
        System.currentPageReference().getParameters().put('accId', testAccount.id);
        System.currentPageReference().getParameters().put('type', 'Quote');
        System.currentPageReference().getParameters().put('oppId', '');

        Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
        Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');

        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(quoteRecordType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
        insert quoteForTest;

        ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
        RedirectorAndValidatorContr testCntr = new RedirectorAndValidatorContr(sc);
        testCntr.redirectToQuote();
        System.currentPageReference().getParameters().put('type', 'Internal_NFR_Quote');
        SBQQ__Quote__c quoteForTest1 = QuoteTestHelper.createQuote(internalNfrQuote, testContact.id, testAccount, '', 'Not For Resale', 'Open', 'Quote', false, '');
        quoteForTest1.Quote_Recipient__c = testContact.id;
        insert quoteForTest1;

        ApexPages.StandardController sc1 = new ApexPages.StandardController(quoteForTest1);
        RedirectorAndValidatorContr testCntr1 = new RedirectorAndValidatorContr(sc1);
        testCntr1.redirectToQuote();
    }

    @isTest
    public static void testWithOpp() {
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', new User(Id = UserInfo.getUserId()), true);
        testAccount.RecordtypeId = EndUserAccRecTypeId;
        testAccount.BillingCountry = 'France';
        update testAccount;

        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];

        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c>{
            QuoteTestHelper.createQlikTech('France', 'FR', testSubs.id),
            QuoteTestHelper.createQlikTech('Afghanistan', 'AF', testSubs.id),
            QuoteTestHelper.createQlikTech('Japan', 'JP', testSubs.id),
            QuoteTestHelper.createQlikTech('Albania', 'AL', testSubs.id)
        };

        insert listOfcreate;

        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
        insert testContact;

        Test.setCurrentPageReference(new PageReference('Page.RedirectorAndValidator'));

        Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
        billAddress.Country__c = 'United Kingdom';
        insert billAddress;
        Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
        insert shippAddress;

        Opportunity oppForTest = QuoteTestHelper.createOpportunity(testAccount, '', oppRecordType);
        insert oppForTest;

        System.currentPageReference().getParameters().put('accId', '');
        System.currentPageReference().getParameters().put('oppId', oppForTest.id);
        System.currentPageReference().getParameters().put('type', 'Quote');

        Test.startTest();
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(quoteRecordType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
        insert quoteForTest;
        Test.stopTest();

        ApexPages.StandardController sc = new ApexPages.StandardController(quoteForTest);
        RedirectorAndValidatorContr testCntr = new RedirectorAndValidatorContr(sc);
        testCntr.redirectToQuote();
        testCntr.getFixedUrl();
    }
}