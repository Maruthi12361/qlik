/**
* Change log:
*
*   04-01-2019  ext_vos, extbad CHG0030387: Disable chatter emails for all external users.
*/
public class CustomUserTriggerHandler {
    private final static String CUSTOMER_PROFILES_STARTS = 'Customer Portal%';
    private final static String EVENTFORCE_PROFILES_STARTS = 'Eventforce Portal%';
    private final static String EVENTFORCE_PROFILES_NON_CONTAINS = '%Employee%';
    private final static String PARTNER_PROFILE_STARTS = 'PRM%';
    private final static String PARTNER_PROFILE_STARTS_PSE = 'PSE - PRM%';
    private final static String PARTNER_PROFILE_NON_CONTAINS_OLD = '%OLD%';
    private final static String PARTNER_PROFILE_NON_CONTAINS_READ = '%READ ONLY%';

    public static void onBeforeUpdate(List<User> inputList, Map<Id, User> oldMap) {
        Q2CWSettings__c customSettings = Q2CWSettings__c.getInstance(UserInfo.getOrganizationId());
        if (customSettings != null && !customSettings.Disable_User_trigger__c) {
            processUserAssignment(inputList, oldMap);
        }
        disableChatterEmailsForExternalUsers(inputList);
        disableChatterEmailsForExternalNetworkMembers(inputList);
    }

    public static void onAfterInsert(List<User> inputList) {
        disableChatterEmailsForExternalNetworkMembers(inputList);
    }

    public static void onBeforeInsert(List<User> inputList) {
        disableChatterEmailsForExternalUsers(inputList);
    }

    private static void processUserAssignment(List<User> inputList, Map<Id, User> oldMap) {
        List<User> listOFSelectedUsers = new List<User>();
        Set<Id> userIds = new Set<Id>();
        for (User userItem : inputList) {
            if (userItem.use_Salesforce_CPQ__c != oldMap.get(userItem.id).use_Salesforce_CPQ__c && userItem.use_Salesforce_CPQ__c) {
                listOFSelectedUsers.add(userItem);
                userIds.add(userItem.Id);
            }
        }
        List<PermissionSetAssignment> permSetAssignmentOldList = [
            Select id, PermissionSetId, AssigneeId, PermissionSet.Name 
            From PermissionSetAssignment 
            WHERE (PermissionSet.Name = 'AdvancedApprovalsUser' OR PermissionSet.Name  = 'SteelBrickCPQCustomerUser') 
            AND Assignee.IsActive = TRUE AND AssigneeId IN :userIds];

        //checking for Existing assignments
        Map<Id, List<PermissionSetAssignment>> userToPermAssignment = new Map<Id, List<PermissionSetAssignment>>();
        for (PermissionSetAssignment item : permSetAssignmentOldList) {
            if (!userToPermAssignment.containsKey(item.AssigneeId)) {
                userToPermAssignment.put(item.AssigneeId, new List<PermissionSetAssignment>());
                userToPermAssignment.get(item.AssigneeId).add(item);
            } else {
                userToPermAssignment.get(item.AssigneeId).add(item);
            }
        }
        if (!listOFSelectedUsers.isEmpty()) {
            List<PermissionSet> listOfPermissionsSets = [
                    SELECT Id, Name
                    FROM PermissionSet
                    WHERE Name = 'AdvancedApprovalsUser' OR Name = 'SteelBrickCPQCustomerUser'
            ];
            List<PermissionSetAssignment> listOfAsignees = new List<PermissionSetAssignment>();
            for (User userItem : listOFSelectedUsers) {
                for (PermissionSet itemPermSet : listOfPermissionsSets) {
                    if (!userToPermAssignment.containsKey(userItem.id)) {
                        PermissionSetAssignment permSeetAssignment = new PermissionSetAssignment(PermissionSetId = itemPermSet.id, AssigneeId = userItem.id);
                        listOfAsignees.add(permSeetAssignment);
                    }
                }
            }
            if (!listOfAsignees.isEmpty()) {
                upsert listOfAsignees;
            }
        }
    }

    private static List<User> getExtUsers(List<User> newUsers) {
        Map<Id, Profile> profiles = new Map<Id, Profile>([
                SELECT Id
                FROM Profile
                WHERE Name LIKE :CUSTOMER_PROFILES_STARTS
                OR (Name LIKE :EVENTFORCE_PROFILES_STARTS AND (NOT Name LIKE :EVENTFORCE_PROFILES_NON_CONTAINS))
                OR ((Name LIKE :PARTNER_PROFILE_STARTS OR Name LIKE :PARTNER_PROFILE_STARTS_PSE)
                AND (NOT Name LIKE :PARTNER_PROFILE_NON_CONTAINS_OLD)
                AND (NOT Name LIKE :PARTNER_PROFILE_NON_CONTAINS_READ))
        ]);

        List<User> extUsers = new List<User>();
        Boolean isUserExternal;
        for (User us : newUsers) {
            isUserExternal = us.isActive == true
                    && profiles.keySet().contains(us.ProfileId)
                    && us.ContactId != null;
            if (isUserExternal) {
                extUsers.add(us);
            }
        }
        return extUsers;
    }

    private static void disableChatterEmailsForExternalNetworkMembers(List<User> newUsers) {
        // check isUserExternal and disable chatter notifications for network member
        List<String> externalUserIds = new List<String>();
        List<User> externalUsers = getExtUsers(newUsers);
        for (User us : externalUsers) {
            externalUserIds.add(us.Id);
        }

        List<NetworkMember> networkMembers = [
                SELECT Id, PreferencesDisableAllFeedsEmail
                FROM NetworkMember
                WHERE MemberId IN :externalUserIds
        ];

        List<NetworkMember> nmToUpdate = new List<NetworkMember>();
        for (NetworkMember nm : networkMembers) {
            if (!nm.PreferencesDisableAllFeedsEmail) {
                nm.PreferencesDisableAllFeedsEmail = true;
                nmToUpdate.add(nm);
            }
        }
        update nmToUpdate;
    }

    private static void disableChatterEmailsForExternalUsers(List<User> newUsers) {
        // check isUserExternal and disable chatter notifications
        List<User> externalUsers = getExtUsers(newUsers);
        for (User us : externalUsers) {
            if (!us.UserPreferencesDisableAllFeedsEmail) {
                us.UserPreferencesDisableAllFeedsEmail = true;
            }
        }
    }

}