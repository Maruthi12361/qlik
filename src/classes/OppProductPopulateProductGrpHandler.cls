/*
    2017-10-02 AIN Class created to populate the Product_Group__c field on opp for QCW-2862
*/
public with sharing class OppProductPopulateProductGrpHandler {

    private static boolean isDelete = false;
    public static void onAfterInsert(List<OpportunityLineItem>  inputList, Map<id, OpportunityLineItem> inputMap) {
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler onAfterInsert start');
        isDelete = false;
        PopulateProductGroup(inputList);
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler onAfterInsert end');
    }
    public static void onAfterUpdate(List<OpportunityLineItem>  inputList) {
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler onAfterUpdate start');
        isDelete = false;
        PopulateProductGroup(inputList);
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler onAfterUpdate end');
    }
    public static void onAfterDelete(List<OpportunityLineItem>  inputList, Map<id, OpportunityLineItem> inputMap) {
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler onAfterDelete start');
        isDelete = true;
        PopulateProductGroup(inputList);
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler onAfterDelete end');
    }
    public static void PopulateProductGroup(List<OpportunityLineItem> productList) {
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler PopulateProductGroup start');

        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler PopulateProductGroup number of items: ' + productList.Size());
        Map<Id, Set<String>> uniqueProductGroups = new Map<Id, Set<String>>();
        for(OpportunityLineItem prod : productList){
        	system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler prod.Product_Group_From_Quoteline__c: ' + prod.Product_Group_From_Quoteline__c);
            string prodGroup = isDelete || prod.Product_Group_From_Quoteline__c == null || prod.Product_Group_From_Quoteline__c == '' ? '' : prod.Product_Group_From_Quoteline__c;
            if(prod.OpportunityId == null)
                continue;
            if(!uniqueProductGroups.containsKey(prod.OpportunityId)) {
                //Set<String> newSet = new Set<String>{prod.Product_Group_From_Quoteline__c};
                uniqueProductGroups.put(prod.OpportunityId, new Set<String>{prodGroup});
            }
            else if(prodGroup != '' && !uniqueProductGroups.get(prod.OpportunityId).contains(prodGroup)) {
                uniqueProductGroups.get(prod.OpportunityId).add(prodGroup);
            }
        }

        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        for(Id id : uniqueProductGroups.keySet()){
            string concatenatedString = '';
            for(string s : uniqueProductGroups.get(id)) {
                if(s != '')
                    concatenatedString += s + ';';
            }
            concatenatedString = concatenatedString.removeEnd(';');
            oppsToUpdate.add(new Opportunity(Id = Id, Product_Groups__c = concatenatedString));

            system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler OpportunityId: ' + Id);
            system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler Product_Groups__c: ' + concatenatedString);
        }
        if(!oppsToUpdate.isEmpty())
            update oppsToUpdate;
        system.debug(LoggingLevel.WARN, 'OppProductPopulateProductGroupHandler PopulateProductGroup end');
    }
}