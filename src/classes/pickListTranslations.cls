public class pickListTranslations {

	public static String ReturnEmployee(Integer EmpCount)
		{
			system.debug(EmpCount);
			system.debug('*** In Trigger');
			String sRet = '';
			
			if (EmpCount < 5 )
				{
					system.debug('Its 1-4');
					sRet = '1-4';
				}
			if (EmpCount > 4 & EmpCount < 10)
				{
					system.debug('Its <10');
					sRet = '5-9';
				}
			if (EmpCount > 9 & EmpCount < 20)
				{
					system.debug('Its <20');
					sRet = '10-19';
				}
			if (EmpCount > 19 & EmpCount < 50)
				{
					system.debug('Its <50');
					sRet = '20-49';
				}
			if (EmpCount > 49 & EmpCount < 100)
				{
					sRet = '50-100';
				}
			if (EmpCount > 99 & EmpCount < 200)
				{
					sRet = '100-199';
				}
			if (EmpCount > 199 & EmpCount < 500)
				{
					sRet = '200-499';
				}
			if (EmpCount > 499 & EmpCount < 1000)
				{
					sRet = '500-999';
				}
			if (EmpCount > 999 & EmpCount < 1500)
				{
					sRet = '1000-1499';
				}
			if (EmpCount > 1499 & EmpCount < 2000)
				{
					sRet = '1500-1999';
				}
			if (EmpCount > 1999 & EmpCount < 3000)
				{
					sRet = '2000-2999';
				}
			if (EmpCount > 2999 & EmpCount < 4000)
				{
					sRet = '3000-3999';
				}
			if (EmpCount > 3999 & EmpCount < 5000)
				{
					sRet = '4000-4999';
				}
			if (EmpCount > 4999 & EmpCount < 10000)
				{
					sRet = '5000-9999';
				}
			if (EmpCount > 9999)
				{
					sRet = '10000+';
				}
				
			system.debug('*** Sending Back ' + sRet);			
			return sRet;
				
			
		}


}