/*
Name:  CaseMilestoneUtils.cls
Copyright © 2011  salesforce.com consulting
======================================================
======================================================
Purpose:
-------
======================================================
======================================================
History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     Mark Cane&  2011-10-06  Initial development.
1.1     Mark Cane&  2011-10-13  Added Status!=Closed clause to case gathering to avoid conflict with case close sruvery trigger.
1.2     MTM  2013-07-26 CR 9118
1.3     ext_vos     2017-12-14  CR#CHG0030519 Add new communication task subject.
*/
public with sharing class CaseMilestoneUtils {
    public final static String CASE_MILESTONE_NAME_FIRST_RESPONSE = 'First Response';
    public final static String CASE_MILESTONE_NAME_CUSTOMER_COMMUNICATION = 'Customer Communication';
    public final static String TASK_SUBJECT_FIRST_RESPONSE = 'First Response';
    public final static String TASK_SUBJECT_EMAIL = 'Email';
    public final static String TASK_SUBJECT_CALL = 'Call';
    public final static String TASK_SUBJECT_MEETING = 'Meeting';
    public final static String TASK_SUBJECT_CASE_CLARITY = 'Case Clarity';
    public final static String TASK_SUBJECT_BS_CASE_FEED = 'B+S Case Feed';

    public static void completeMilestonesFromTask(Map<ID, Task> whatIds){
        Set<ID> firstResponseCMIds = new Set<ID>();
        Set<ID> communicationCMIds = new Set<ID>();
        Set<ID> communicationViolationCaseIds = new Set<ID>();
        
        //& retrieve open cases within active SLA process.
        for (Case c : [select Id,
                        (select Id, MilestoneType.Name, IsCompleted, IsViolated, TargetDate from CaseMilestones order by TargetDate asc) 
                        from Case 
                        where Id in:whatIds.keySet()
                        and EntitlementId<>null and SlaStartDate<>null and SlaExitDate=null
                        and isClosed=false]){
            ID openFirstResponseMS=null;
            ID hasCommunicationMS=null;
            Boolean hasViolatedCommunicationMS=false;
            
            for (CaseMilestone cm : c.CaseMilestones){
                //aaron: commented condition to fit issue 30 requirements.
                if (cm.MilestoneType.Name==CaseMilestoneUtils.CASE_MILESTONE_NAME_FIRST_RESPONSE && cm.IsCompleted==false /*&& cm.IsViolated==false*/){
                    openFirstResponseMS = cm.Id;
                } else if (cm.MilestoneType.Name==CaseMilestoneUtils.CASE_MILESTONE_NAME_CUSTOMER_COMMUNICATION && cm.IsViolated==true){
                    hasViolatedCommunicationMS = true;
                    hasCommunicationMS = cm.Id;
                } else if (cm.MilestoneType.Name==CaseMilestoneUtils.CASE_MILESTONE_NAME_CUSTOMER_COMMUNICATION){
                    hasCommunicationMS = cm.Id;
                }
            }
            
            //& if task subject is First Response and case has incomplete First Reponse milestone then complete it.
            if (whatIds.get(c.Id).Subject==CaseMilestoneUtils.TASK_SUBJECT_FIRST_RESPONSE && openFirstResponseMS!=null){
                firstResponseCMIds.add(openFirstResponseMS);
            } else if (isCommunicationTask(whatIds.get(c.Id)) && hasCommunicationMS!=null){
                communicationCMIds.add(hasCommunicationMS);
                
                if (hasViolatedCommunicationMS) communicationViolationCaseIds.add(c.Id);
            }
        }   
        
        if (firstResponseCMIds.size()>0) completeFirstResponseMilestone(firstResponseCMIds);
        if (communicationViolationCaseIds.size()>0) recordViolatedCommunicationMilestoneReset(communicationViolationCaseIds);       
        if (communicationCMIds.size()>0) resetCommunicationMilestone(communicationCMIds);
    }
    
    public static Boolean isCommunicationTask(Task t){
        
        if (t.Subject==TASK_SUBJECT_EMAIL ||
                t.Subject==TASK_SUBJECT_CALL ||
                t.Subject==TASK_SUBJECT_MEETING ||
                t.Subject==TASK_SUBJECT_CASE_CLARITY ||
                isBSCaseFeed(t.Subject))
            return true;
        else
            return false;
    }

    public static void completeMilestonesFromEmail(Map<ID, String> caseEmails){
        Set<ID> firstResponseCMIds = new Set<ID>();
        Set<ID> communicationCMIds = new Set<ID>();
        Set<ID> communicationViolationCaseIds = new Set<ID>();
        
        //& retrieve open cases within active SLA process.
        for (Case c : [select Id,
                        (select Id, MilestoneType.Name, IsCompleted, IsViolated, TargetDate from CaseMilestones order by TargetDate asc), 
                        (select Id from EmailMessages where Incoming=false)
                        from Case 
                        where Id in:caseEmails.keySet()
                        and EntitlementId<>null and SlaStartDate<>null and SlaExitDate=null
                        and isClosed=false]){
            ID openFirstResponseMS=null;
            ID hasCommunicationMS=null;
            Boolean hasViolatedCommunicationMS=false;
            
            for (CaseMilestone cm : c.CaseMilestones){
                //aaron: commented condition to fit issue 30 requirements.
                if (cm.MilestoneType.Name==CaseMilestoneUtils.CASE_MILESTONE_NAME_FIRST_RESPONSE && cm.IsCompleted==false /* && cm.IsViolated==false*/){
                    openFirstResponseMS = cm.Id;
                } else if (cm.MilestoneType.Name==CaseMilestoneUtils.CASE_MILESTONE_NAME_CUSTOMER_COMMUNICATION && cm.IsViolated==true){
                    hasViolatedCommunicationMS = true;
                    hasCommunicationMS = cm.Id;
                } else if (cm.MilestoneType.Name==CaseMilestoneUtils.CASE_MILESTONE_NAME_CUSTOMER_COMMUNICATION){
                    hasCommunicationMS = cm.Id;
                }
            }
                        
            //& if email is the 1st outbound and case has incomplete First Reponse milestone then complete it.
            if (caseEmails.get(c.Id)=='FR' && openFirstResponseMS!=null){
                firstResponseCMIds.add(openFirstResponseMS);
                //aaron added coment below:
            } else if (/*caseEmails.get(c.Id)=='CC' && */hasCommunicationMS!=null){
                communicationCMIds.add(hasCommunicationMS);
                
                if (hasViolatedCommunicationMS) communicationViolationCaseIds.add(c.Id);
            }
        }
        
        if (firstResponseCMIds.size()>0) completeFirstResponseMilestone(firstResponseCMIds);
        if (communicationViolationCaseIds.size()>0) recordViolatedCommunicationMilestoneReset(communicationViolationCaseIds);
        if (communicationCMIds.size()>0) resetCommunicationMilestone(communicationCMIds);
    }

    //& private helpers.
    private static void completeFirstResponseMilestone(Set<ID> caseMilestoneIds){
        List<CaseMilestone> listCM = new List<CaseMilestone>();
        Map<ID, Case> cases = new Map<ID, Case>();
        
        for (CaseMilestone cm : [select Id, CaseId from CaseMilestone 
                                    where Id in :caseMilestoneIds]){
            cm.CompletionDate = Datetime.now();
            listCM.add(cm);
            
            cases.put(cm.CaseId, null);
        }
        if (listCM.size()>0) update listCM;
        
        for (Case c: [select Id from Case where Id in:cases.keySet()]){
            c.X1st_Response_Timestamp__c = Datetime.now();
            c.Last_Communication_Timestamp__c = Datetime.now();
            cases.put(c.Id, c);
        }
        if (cases.size()>0) update cases.values();
    }

    private static void resetCommunicationMilestone(Set<ID> caseMilestoneIds){
        List<CaseMilestone> listCM = new List<CaseMilestone>();
        Map<ID, Case> cases = new Map<ID, Case>();
        
        for (CaseMilestone cm : [select Id, CaseId from CaseMilestone 
                                    where Id in :caseMilestoneIds]){
            cm.StartDate = DateTime.now();

            listCM.add(cm);
            
            cases.put(cm.CaseId, null);
        }
        if (listCM.size()>0) update listCM;
        
        for (Case c: [select Id from Case where Id in:cases.keySet()]){
            c.Last_Communication_Timestamp__c = Datetime.now();
            cases.put(c.Id, c);
        }
        if (cases.size()>0) update cases.values();
    }

    private static void recordViolatedCommunicationMilestoneReset(Set<ID> caseIds){
        List<CaseComment> comments = new List<CaseComment>();
        
        for (CaseMilestone cm : [select Id, CaseId, TargetDate from CaseMilestone 
                                where CaseId in:caseIds 
                                and MilestoneType.Name=:CASE_MILESTONE_NAME_CUSTOMER_COMMUNICATION
                                and isViolated=true]){
            CaseComment cc = new CaseComment();
            
            cc.CommentBody = CASE_MILESTONE_NAME_CUSTOMER_COMMUNICATION+' milestone reset after SLA violation, Target Date '+cm.TargetDate+' missed.';
            cc.ParentId = cm.CaseId;
            cc.IsPublished = false;
            
            comments.add(cc);
        }
        if (comments.size()>0) insert comments;
    }
    
    private static Boolean isBSCaseFeed(String taskSubject) {
        return taskSubject != null && taskSubject.contains(TASK_SUBJECT_BS_CASE_FEED);
    }
}