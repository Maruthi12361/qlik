/********
* NAME : OpportunityChangeCurrency
* Description: Helper class to change an opportunity currency, 
*              in addition to that it also removes all line items and quotes on the opp.
* 
*
*Change Log:
*   2014-07-10 Updated a condition of logging
    MTM 2014-11-28 CR# 17952
    MTM 2014-12-03 CR# 19145
    TJG 2015-04-28 CR# 37640 As discussed with Azeem check if there are still quotes before make Boomi call
    TJG 2015-07-02 CR# 37640 As pointed out by Natalie, need to delete Approval Records as well
    Andrew Lokotosh 2016-06-08 Comented Forecast_Amount fields line 74-80
******/
global class OpportunityChangeCurrency {
    private static String message = '';
    webservice static String ChangeCurrency(String oppId, String newCurrency)
    {
        System.debug('Going to change currency to '+ newCurrency);
        if(oppId != null && newCurrency != null){
            try
            {
                Opportunity opp = GetOpp(oppId);
                
                if(opp != null && RefreshOpportunityInfo(opp, newCurrency)){   
                    SetOppCurrencyByBoomi(opp.Id, opp.INT_NetSuite_InternalID__c , newCurrency);                   
                }
            }
            catch(DmlException e){
                system.Debug('Error in OpportunityChangeCurrency : ' + e); 
                message += e;
            }
        }
        return message;
        
    }
    
    private static Opportunity GetOpp(String OppId){
        for(Opportunity opp :[SELECT Id, CurrencyIsoCode, INT_NetSuite_InternalID__c, Quote_Expiration_Date__c, Quote_Status__c
             FROM Opportunity WHERE Id =: oppId]){
                return opp;
            }
        return null;
    }
    
    private static Boolean RefreshOpportunityInfo(Opportunity opp, String newCurrency){             
        if(opp.CurrencyIsoCode == newCurrency){
            message = 'You selected the same currency.';
            return false;
        }  
        /* 20141203 MTM CR# 19145
         * Order of execution/update are important here, Please do not change
         * If changing this, need to test three cases 
         * One with No quotes and Zero prices
         * SEcond with no quote and non zero License forecast amounts means there will be oppty Line items
         * Third with Quote 
         */
        List<NS_Quote__c> allQs = [SELECT Id FROM NS_Quote__c WHERE Opportunity__c = :opp.Id];
        if(allQs.size() > 0)
        {
            message += 'Deleted quote(s). \n';
            delete allQs; 
        }
        // 2015-07-02 if there are Approval Records, then delete them
        List<Approval__c> allARs = [SELECT Id FROM Approval__c WHERE Opportunity__c = :opp.Id];
        if (allARs.size() > 0)
        {
            message += 'Deleted Approval Record(s).\n';
            delete allARs;
        }
               
        //Delete amounts on the opp.

        //opp.License_Forecast_Amount__c = null;
        //opp.License_Forecast_Amount__c = 7;
        //opp.Education_Forecast_Amount__c = null;
        //opp.Support_Forecast_Amount__c = null;
        //opp.Consultancy_Forecast_Amount__c = null;
       // opp.Misc_Forecast_Amount__c = null;
       // opp.Total_Maintenance_Amount__c = null;

        update opp;        
     
        List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where OpportunityId= :opp.Id];
        System.debug('olis.size() = ' + olis.size());
        if(olis.size() > 0)
        {
            message += 'Deleted Opportunity Line Items. \n'; 
            delete olis;
        }
        
        //Change currency to new one        
        opp.CurrencyIsoCode = newCurrency;        
        opp.Quote_Expiration_Date__c = null;
        opp.Quote_Status__c = null;

        opp.Quote_Approval_Reason_Request__c = null;
        update opp;                               
        
        message += 'Changed opportunity currency to ' + newCurrency + '. \n';

        //TJG 2015-04-28 CR# 37640 As discussed with Azeem check if there are still quotes
        List<NS_Quote__c> nsqs = [SELECT Id from NS_Quote__c where Opportunity__c = :opp.Id and IsDeleted<>true];
        if (nsqs.size() > 0)
        {
            message += 'However, there are still quotes for this opportunity. Do not call Boomi!';
            return false;
        }
        
        BoomiUtil.updateBoomiStatus(opp.Id);
        
        return true;
    }
    webservice static String SetOppCurrencyByBoomi(String oppId, String newCurrency)
    {
        Opportunity opp = GetOpp(oppId);
        if(opp != null){                    
            SetOppCurrencyByBoomi(opp.Id, opp.INT_NetSuite_InternalID__c, newCurrency);                  
        }
        return message;
    }
    private static Boolean SetOppCurrencyByBoomi(String sfOppId, String nsOppId, String newCurr){
        /*<Record type="Opportunity" environment="00DL00000028EPu(or current environment ID)">
            <ID>160506</ID> INT_NetSuite_InternalID__c
            <secondaryID>006L0000004IoGK</secondaryID> Opportunity.Id
        </Record>*/

        //Only ask boomi to update if this opp exists in Netsuite
        System.debug('SetOppCurrencyByBoomi sfOppId:' +sfOppId+ ' nsOppId:' + nsOppId + ' newCurr:' + newCurr);

        if(nsOppId != null && nsOppId != '')    {
            String orgId = UserInfo.getOrganizationId();
            String boomiMessage = '<Record type="Opportunity" environment="' + orgId + '"><ID>' + nsOppId + '</ID><secondaryID>' + sfOppId + '</secondaryID></Record>';
            BoomiUtil.callBoomi(sfOppId, 'updateNSOppCurrency', boomiMessage);

            System.debug('Called boomi with :' + boomiMessage);
            
            message += '\nResetting the Opportunity... \n It can take few minutes to reset all opportunity information.';
            return true;
        }
        return false;
    }
}