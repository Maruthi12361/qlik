/**************************************************************
* Change Log:
* 2019-12-18 extcqb: Added tests for IT-2321
* 2019-12-12   extbad   IT-2329    do not allow Chatter posts to Closed Attunity Cases for External users
* 2020-02-11   extbad   IT-2479   Disable posts to 'Chat Resolved' Status also
***************************************************************/
@isTest
private class ChatterPostsAccessValidatorTest {
    static testMethod void testFeedItemToAttunityCase() {
        String dtnow = '' + System.now();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName' + dtnow, qtc);
        insert testAccount;
        Contact testContact = TestDataFactory.createContact('test_Thomas' + dtnow, 'test_Jones' + dtnow, 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;
        Profile profileRec = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Case Logging Access - Attunity' LIMIT 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        User communityUser = TestDataFactory.createUser(profileId, 'tSboxAttunityCs@qlikTech.com', 'tSboxAttunityCs', String.valueOf(testContact.Id));
        insert communityUser;

        Case cs = new Case();
        System.runAs(communityUser) {
            cs.Product__c = 'Not product related';
            cs.OwnerId = UserInfo.getUserId();
            insert cs;
        }

        cs.Status = CaseClosedAttunityHandler.CASE_CLOSED_STATUSES.get(0);
        update cs;

        Test.startTest();
        String error = '';
        System.runAs(communityUser) {
            try {
                FeedItem fi = new FeedItem(ParentId = cs.Id, Body = 'Test comment');
                insert fi;
            } catch (DmlException ex) {
                System.debug('#### ' + ex);
                error = ex.getMessage();
            }
        }
        Test.stopTest();

        System.assert(String.isNotBlank(error));
        System.assert(error.contains(ChatterPostsAccessValidator.ATTUNITY_CASE_ERROR_MESSAGE));
    }

    static testMethod void testFeedCommentToAttunityCase() {
        String dtnow = '' + System.now();
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName' + dtnow, qtc);
        insert testAccount;
        Contact testContact = TestDataFactory.createContact('test_Thomas' + dtnow, 'test_Jones' + dtnow, 'testSandboxTJ@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        testContact.LeadSource = 'Qlikmarket';
        insert testContact;
        Profile profileRec = [SELECT Id FROM Profile WHERE Name = 'Customer Portal Case Logging Access - Attunity' LIMIT 1];
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        User communityUser = TestDataFactory.createUser(profileId, 'tSboxAttunityCs@qlikTech.com', 'tSboxAttunityCs', String.valueOf(testContact.Id));
        insert communityUser;

        Case cs = new Case();
        System.runAs(communityUser) {
            cs.Product__c = 'Not product related';
            cs.OwnerId = UserInfo.getUserId();
            insert cs;
        }

        cs.Status = CaseClosedAttunityHandler.CASE_CLOSED_STATUSES.get(1);
        update cs;

        FeedItem fi = new FeedItem(ParentId = cs.Id, Body = 'Test comment');
        insert fi;

        Test.startTest();
        String error = '';
        System.runAs(communityUser) {
            try {
                FeedComment fc = new FeedComment(FeedItemId = fi.Id, CommentBody = 'Test comment');
                insert fc;
            } catch (DmlException ex) {
                error = ex.getMessage();
            }
        }
        Test.stopTest();

        System.assert(String.isNotBlank(error));
        System.assert(error.contains(ChatterPostsAccessValidator.ATTUNITY_CASE_ERROR_MESSAGE));
    }

    @IsTest
    static void testFeedItemValidation() {
        //given
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;

        //when
        FeedItem feedItem = new FeedItem(ParentId = testAccount.Id, Body = 'Test body');
        insert feedItem;

        //then
        System.assertEquals(1, [SELECT COUNT() FROM FeedItem WHERE ParentId = :testAccount.Id]);


        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        insert testContact;
        Id profileId = [SELECT Id FROM Profile WHERE Name = :ChatterPostsAccessValidator.ATTUNITY_READ_ONLY_PROFILE_NAME LIMIT 1].Id;
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));

        System.RunAs(communityUser) {
            DmlException dmlException;
            try {
                //when
                FeedItem feedItem2 = new FeedItem(ParentId = testAccount.Id, Body = 'Test body2');
                insert feedItem2;
            } catch (DmlException ex) {
                dmlException = ex;
            }
            //then
            System.assertNotEquals(null, dmlException);
            System.assert(dmlException.getMessage().contains(ChatterPostsAccessValidator.ERROR_MESSAGE));
        }
    }

    @IsTest
    static void testFeedCommentValidation() {
        //given
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        FeedItem feedItem = new FeedItem(ParentId = testAccount.Id, Body = 'Test body');
        insert feedItem;

        //when
        FeedComment feedComment = new FeedComment(CommentBody='test', FeedItemId = feedItem.Id);
        insert feedComment;

        //then
        System.assertEquals(1, [SELECT COUNT() FROM FeedComment WHERE FeedItemId = :feedItem.Id]);

        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        insert testContact;
        Id profileId = [SELECT Id FROM Profile WHERE Name = :ChatterPostsAccessValidator.ATTUNITY_READ_ONLY_PROFILE_NAME LIMIT 1].Id;
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));

        System.RunAs(communityUser) {
            DmlException dmlException;
            try {
                //when
                FeedComment feedComment2 = new FeedComment(CommentBody='test', FeedItemId = feedItem.Id);
                insert feedComment2;
            } catch (DmlException ex) {
                dmlException = ex;
            }
            //then
            System.assertNotEquals(null, dmlException);
            System.assert(dmlException.getMessage().contains(ChatterPostsAccessValidator.ERROR_MESSAGE));
        }
    }
}