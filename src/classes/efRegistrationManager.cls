//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//          
// Change History
// --------------
// 	2013-01-08		SAN		Adding changes for partner paying by invoice						
//*********************************************************/
public class efRegistrationManager{
    private efContactWrapper c;
    private efRegistrationWrapper r;
    private efEventWrapper e;
    private Account account;
    private String attendeeType = efConstants.ATTENDEE_TYPE_ATTENDEE;
    private String subAttendeeType = efConstants.SUB_ATTENDEE_TYPE_CUSTOMER;
    private String userType = efConstants.ATTENDEE_TYPE_ATTENDEE + ':' + efConstants.SUB_ATTENDEE_TYPE_CUSTOMER;
    private String finalUserType = attendeeType;    
    private Events__c event;
    private Id registrationId;
    private String regStatus;    
    private Map<String,String> configMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);
    private CampaignMember cm = null;
    
        
    // Initialisation.
    
    public efRegistrationManager(Id contactId, Id registrationId){                         
        initCollectInfoById(contactId, registrationId);
    }

    private void initCollectInfoById(Id contactId, Id registrationId){
        Contact contact = [Select c.Id,
                                  c.AccountId,
                                  c.Account.Fax,
                                  c.Account.Name,
                                  c.Account.Phone,
                                  c.Account.Company_Size__c,
                                  c.Account.Industry,
                                  c.MailingStreet,
                                  c.MailingCountry,
                                  c.MailingCity,
                                  c.MailingState,
                                  c.MailingPostalCode,
                                  c.Account.ShippingCity,
                                  c.Account.ShippingCountry,
                                  c.Account.ShippingPostalCode,
                                  c.Account.ShippingState,
                                  c.Account.ShippingStreet,
                                  c.Account.BillingCity,
                                  c.Account.BillingCountry,
                                  c.Account.BillingPostalCode,
                                  c.Account.BillingState,
                                  c.Account.BillingStreet,
                                  c.Account.Billling_Country_Code_Name__c,
                                  c.Email,
                                  c.Name,
                                  c.FirstName,
                                  c.LastName,
                                  c.MobilePhone,
                                  c.Phone,
                                  c.ownerid,
                                  c.job_level__c,
                                  c.job_function__c,
                                  c.Title,
                                  c.Job_Title__c,
                                  (
                                      Select Id,
                                             registrant__c,
                                             Track_Interest__c,
                                             Why_Register__c,
                                             Track_Of_Interest_Lkup__c,
                                             Want_to_learn__c,
                                             Badge_First_Name__c,
                                             Badge_Last_Name__c,                                             
                                             Emergency_Contact__c,
                                             Emergency_Phone__c,
                                             Emergency_Relationship__c,
                                             Event__c,
                                             Special_Requirement__c,
                                             Status__c,
                                             Wizard_Page_Saved__c,
                                             Separate_Name_On_Badge__c,
                                             portal_attendee_type__c,
                                             attendee_subtype__c,
                                             Require_special_assistance__c,
                                             Mobile_Phone__c,
                                             Employee_Department__c,
                                             Session_Topics__c,
                                             Primary_Role__c,
                                             Payment_Timestamp__c,
                                             Reg_Number__c,
                                             PromoCode__c,
                                             Name,
                                             CurrencyIsoCode,
                                             Has_Extended_Stay_Addon__c,
                                             Extended_Stay_End_Date__c,
                                             Extended_Stay_Start_Date__c,
                                             Spouse_Added__c,
                                             Spouse_First_Name__c,
                                             Spouse_Last_Name__c,
                                             Type__c,
                                             Invoice_Address__c,
                                             Vat_No__c,
                                             Agree_to_share_info__c                                       
                                      From Registrations1__r
                                      Where Id=:registrationId
                                  )
                           From Contact c
                           Where Id=:contactId
                           Order By CreatedDate];

        event = [Select Name,
                        Event_Start_Date__c,
                        Event_End_Date__c,
                        Opportunity_Owner__c,
                        Attendee_Label_Activity_Intro__c,
                        Registration_Label_Training_Header__c,
                        Registration_Label_Training_SubHeader__c,
                        Registration_Label_Summary_Financial_Sum__c,
                        Registration_La__c,
                        Registration_Label_Summary_Group__c,
                        Registration_Label_Summary_Group2__c,
                        Registration_Label_Summary_Group3__c,
                        Registration_Label_Summary_TermsAndCondi__c,
                        Registration_Label_Summary_Payment_Redir__c,
                        Registration_Label_Summary_Invoice__c,
                        Registration_Label_Summary_Invoice_Conf__c,
                        Registration_Label_Summary_Training_Inv__c,
                        Registration_Label_Finish_Payment_Conf__c,
                        Registration_Label_Finish_Go_AttPortal__c,
                        Registration_Label_Finish_Error__c,
                        Registration_Label_Finish_Go_Event__c,
                        Banner_Image_Document__c,
                        Thumbnail_Image_Document__c,
                        Session_Registration_Open_Date__c,
                        Has_Networking_Activities__c,
                        Has_Sessions__c,
                        Has_Training__c,
                        Pricebook_Name__c,
                        Has_Extended_Stay_Addon__c, 
                        Extended_Stay_Summary__c,
                        Extended_Stay_Policy__c, 
                        Extended_Stay_Start_Date__c,  
                        Extended_Stay_End_Date__c,
                        Extended_Stay_Product__c,
                        Has_Spouse_Offer_Addon__c,
                        Spouse_Offer_Product__c,
                        Spouse_Offer_Summary__c,
                        Campaign__c                     
                 From Events__c
                 Where Id =:contact.Registrations1__r[0].Event__c];

        if (event.Campaign__c != null){
            List<CampaignMember> CMs = [Select Status, 
                                                 Id,
                                                 ContactId,
                                                 CampaignId 
                                          From CampaignMember
                                          Where ContactId = :contact.Id and CampaignId = :event.Campaign__c]; 
            if (CMs.size() > 0){
                cm = CMs[0];
            }
        }
        e = new efEventWrapper(event);
        
        c = new efContactWrapper(contact);
        account = contact.Account;
        
        if(contact.Registrations1__r != null && contact.Registrations1__r.size() > 0){
            if(!efUtility.isNull(contact.Registrations1__r[0].portal_attendee_type__c)){
                attendeeType = contact.Registrations1__r[0].portal_attendee_type__c;
            }

            finalUserType = attendeeType;
            subAttendeeType = efUtility.giveValue(contact.Registrations1__r[0].attendee_subtype__c);
            userType = attendeeType;

            r = new efRegistrationWrapper(contact.Registrations1__r[0], contact);
            
            List<Payment_Details__c> pList = [Select Id From Payment_Details__c Where RegistrationId__c=:r.getId() And Return_Code__c<>'1' Limit 1];
            if (pList!=null && pList.size()>0)
                r.setPaymentDetails(pList[0]);
        }
    }  
    // End initialisation.
    
    
    // Registration management methods.

    public void refreshRegStatus(){
        Registration__c[] freshRegistration = [Select Id,
                                                      Status__c,
                                                      Wizard_Page_Saved__c,
                                                      Payment_Timestamp__c
                                               From Registration__c
                                               Where Id = :r.getRegistration().Id];
        
        if (freshRegistration != null && freshRegistration.size()>0){
            r.setStatus(freshRegistration[0].Status__c);
            r.setPaymentTimestamp(freshRegistration[0].Payment_Timestamp__c);
        }
    }
    
    public void reloadRegistration(){
        initCollectInfoById(c.getContact().Id, r.getId());
    }

    public void flushRegistration(){
        update account;
        
        if (UserInfo.getUserType()!='PowerPartner')
            update c.getContact();
                
        Registration__c  registration = r.getRegistration(); 
        
        System.debug('FINDME: flush ' + registration.Portal_Attendee_Type__c + ' ' + registration.Attendee_SubType__c);
        
        registration.Event__c = event.Id;
        registration.ownerId = c.getContact().ownerid;

        registration.name = event.Name + ' - ' + c.getContact().FirstName + ' ' + c.getContact().LastName;
                
        registration.portal_attendee_type__c = getAttendeeType();
        registration.attendee_subtype__c = getSubAttendeeType();

        if(!registration.Separate_Name_On_Badge__c){
            registration.Badge_First_Name__c = c.getContact().FirstName;
            registration.Badge_Last_Name__c = c.getContact().LastName;
        }       
                
        if(!efUtility.isNull(registration.Id)){
            update registration;
            efUtility.updateDebugLog('Successfully Updated Registration Record :: '+registration.Id,'S1010','Success');
        } else{
            registration.account__c = account.Id;
            insert registration;
            efUtility.updateDebugLog('Successfully Inserted Registration Record :: '+registration.Id,'S1011','Success');
        }            
        
        if (e.getEvent().Campaign__c != null){
            if(cm == null)
            {
                cm = new CampaignMember();
                cm.ContactId = c.getContact().Id;
                cm.CampaignId = e.getEvent().Campaign__c;
                cm.Status = efConstants.CAMPAIGN_MEMBER_CREATE;
                insert cm;
            }
        }        
            
        Payment_Details__c p = r.getPaymentDetails(); // Comment& : need to cache the payment details.        
        r = new efRegistrationWrapper(registration,c.getContact());
        r.setPaymentDetails(p);        
    }
    
    public String getOppRecordType(){
        
        String rt = [Select Id From RecordType Where sObjectType='Opportunity' And Name='Eventforce'].Id;
        if (rt==null){
            // TODO & : log the missing record type.
            rt = [Select Id From RecordType Where sObjectType='Opportunity' Limit 1].Id;
        }
        return rt;
    }
    
    public String getOppOwner(){
        String owner = e.getEvent().Opportunity_Owner__c;
        if (owner==null){
            // TODO & : log the missing owner field.
            owner='';   
        }
        return owner;
    }   
    // End Registration management methods.
    

    // Accessors.
    
    public String getAttendeeType(){
        return attendeeType;
    }
    
    public void setAttendeeType(String at){
        attendeeType = at;
    }
    
    public String getSubAttendeeType(){
        return subAttendeeType;
    }
    
    public void setSubAttendeeType(String sat){
        subAttendeeType = sat;
    }
    
    public String getUserType(){
        return userType;
    }

    public String getFinalUserType(){
        return finalUserType;
    }
    
    public efContactWrapper getContactWrapper(){
        return c;
    }
    
    public efRegistrationWrapper getRegistrationWrapper(){
        return r;
    }
    
    public efEventWrapper getEventWrapper(){
        return e;
    }
    
    public Account getAccount(){
        return account;
    }
    
    public Boolean getIsRegisteredUser(){
        return (r.getStatus()=='Registered' || r.getStatus()=='Invoiced');
    }
    // End Accessors. 
    
    
    // Statics    
    public static PageReference checkRegistrationId(String registrationId){
        
        User u = [Select email, contact.Id, contact.AccountId, contact.Email, contact.FirstName,contact.LastName From User Where Id=:UserInfo.getUserId()]; 
                    
        if (u.contact.Id==null || u.contact.AccountId==null)
            return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
        
        Registration__c[] freshRegistration = [Select Id,
                                                      Status__c,
                                                      Wizard_Page_Saved__c,
                                                      Payment_Timestamp__c
                                               From Registration__c
                                               Where Id=:registrationId And Registrant__r.AccountId=:u.contact.AccountId];        
        if (freshRegistration==null || freshRegistration.size()==0){
            return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
        } else if (freshRegistration[0].Status__c==efConstants.REG_STATUS_REGISTERED || freshRegistration[0].Status__c==efConstants.REG_STATUS_INVOICED){
            return efAppNavigator.pr(efAppNavigator.ATTENDEE_PORTAL_HOME);
        } else{           
            return null;
        }                
    }
    
    public static PageReference checkRegistrationIdPortal(String registrationId){
        User u = [Select Email, contact.Id, contact.AccountId, contact.Email, contact.FirstName,contact.LastName From User Where Id=:UserInfo.getUserId()]; 
        String contactId = null;
        String accountId = null;
          
        if (u.contact.Id==null && u.contact.AccountId == null)
        {
            // there are some users who don't have contact populated in User record, try to get data from Contact instead

                List<Contact> listContacts = [Select Id, Email, AccountId From Contact Where Email=:u.Email Limit 1];
                if (listContacts!=null && listContacts.size()==1)
                {
                    contactId = listContacts[0].Id;
                    accountId = listContacts[0].AccountId;
                } 
                else
                {
                    return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
                }             
        }          
        else
        {
            contactId = u.contact.Id;
            accountId = u.contact.AccountId;
        }  
        Registration__c[] freshRegistration = [Select Id,
                                                      Status__c,
                                                      Wizard_Page_Saved__c,
                                                      Payment_Timestamp__c
                                               From Registration__c
                                               Where Id=:registrationId And Registrant__r.AccountId=:accountId];        
        if (freshRegistration==null || freshRegistration.size()==0){
            return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
        } else if (freshRegistration[0].Status__c==efConstants.REG_STATUS_REGISTERED || freshRegistration[0].Status__c==efConstants.REG_STATUS_INVOICED){
            return null;
        } else            
            return efAppNavigator.pr(efAppNavigator.EVENTS_HOME);
    }    
    // End statics.          
}