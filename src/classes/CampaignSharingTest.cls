/******************************************************

    Class: CampaignSharingTest
    
    Changelog:
        2016-03-07  CCE     Created file

******************************************************/
@isTest
public class CampaignSharingTest {
    
    //PRM - Sales Dependent Territory + QlikBuy
    static final Id PrmProfileId = '00e20000001OyLwAAK';
    // Partner Account Record Type ID
    static final Id PartnerAccRecTypeId = '01220000000DOFzAAO';

    //Create a Campaign which is not associated with any Account etc, then create a Partner Account and add a Contact and Partner User for the Contact
    //Check the sharing rules on the Campaign to ensure it is not shared with the Partner User.
    //Add the Account Id to the Campaign and then test that the Campaign now has a new manual Sharing rule. 
    private static testMethod void testUpdateSharing(){
        System.debug('CampaignSharingTest: testUpdateSharing starting');
        QTTestUtils.GlobalSetUp();
        ApexSharingRules.TestingCampaignShare = true;
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        //Create Campaign as a Sys Admin
        Campaign aCampaign = createMockCampaign();
        System.debug('CampaignSharingTest: testUpdateSharing aCampaign = ' + aCampaign);
        
        //Create Partner Account
        Account acc = createPartnerAccount(me);
        System.debug('CampaignSharingTest: testUpdateSharing acc = ' + acc);

        //Create Contact on Partner Account
        Contact con = createContact(me, acc);
        System.debug('CampaignSharingTest: testUpdateSharing con = ' + con);

        //Create Partner User for Contact
        User usrPartner = createMockPartnerUser(me, con, PrmProfileId);
        System.debug('CampaignSharingTest: testUpdateSharing usrPartner = ' + usrPartner);

        User uRet = [SELECT Id, AccountId, ContactId, Profile__c, ProfileId, Username, UserRoleId, UserType from User where ContactId = :con.Id];
        System.debug('CampaignSharingTest: testUpdateSharing uRet = ' + uRet);

        //Test if Partner User can see Campaign (should be no)
        List<CampaignShare> campaignShare = [SELECT CampaignAccessLevel, CampaignId, Id, RowCause, UserOrGroupId FROM CampaignShare where RowCause = 'Manual' and CampaignId = :aCampaign.Id];
        System.debug('CampaignSharingTest: testUpdateSharing campaignShare = ' + campaignShare);
        System.Assert(campaignShare.size() == 0);   //We should not yet have any Manual shares on this Campaign

        Test.startTest();
        //Update Campaign "Partner 1" field with Partner Account
        aCampaign.Primary_Partner_Involved__c = acc.Id;
        update aCampaign;

        Test.stopTest();
        //Test if Partner User can see Campaign (should be yes)
        List<CampaignShare> campaignShareRet = [SELECT CampaignAccessLevel, CampaignId, Id, RowCause, UserOrGroupId FROM CampaignShare where RowCause = 'Manual' and CampaignId = :aCampaign.Id];
        System.debug('CampaignSharingTest: testUpdateSharing campaignShareRet = ' + campaignShareRet);
        System.Assert(campaignShareRet.size() > 0);   //We should have one Manual shares on this Campaign
    }

    private static testMethod void testInsertSharing(){
        System.debug('CampaignSharingTest: testInsertSharing starting');
        QTTestUtils.GlobalSetUp();
        ApexSharingRules.TestingCampaignShare = true;
        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        //Create Partner Account
        Account acc = createPartnerAccount(me);
        System.debug('CampaignSharingTest: testInsertSharing acc = ' + acc);

        //Create Contact on Partner Account
        Contact con = createContact(me, acc);
        System.debug('CampaignSharingTest: testInsertSharing con = ' + con);

        //Create Partner User for Contact
        User usrPartner = createMockPartnerUser(me, con, PrmProfileId);
        System.debug('CampaignSharingTest: testInsertSharing usrPartner = ' + usrPartner);

        User uRet = [SELECT Id, AccountId, ContactId, Profile__c, ProfileId, Username, UserRoleId, UserType from User where ContactId = :con.Id];
        System.debug('CampaignSharingTest: testInsertSharing uRet = ' + uRet);

        Test.startTest();
        //Create Campaign as a Sys Admin
        Campaign newCamp = new Campaign();
        User saUser = QTTestUtils.createMockUserForProfile('System Administrator');
        System.runAs(saUser)
        {
            newCamp.Name = 'MyTestCampaign02';
            newCamp.Planned_Opportunity_Value__c = 10000;
            newCamp.Primary_Partner_Involved__c = acc.Id;
            insert(newCamp);
        }
        System.debug('CampaignSharingTest: testInsertSharing newCamp = ' + newCamp);

        //Test if Partner User can see Campaign (should be yes)
        List<CampaignShare> campaignShareRet = [SELECT CampaignAccessLevel, CampaignId, Id, RowCause, UserOrGroupId FROM CampaignShare where RowCause = 'Manual' and CampaignId = :newCamp.Id];
        System.debug('CampaignSharingTest: testUpdateSharing campaignShareRet = ' + campaignShareRet);
        System.Assert(campaignShareRet.size() > 0);   //We should have one Manual shares on this Campaign

        //coverage - try adding a share where it already exists
        List<Id> lstCampId = new List<Id>();
        lstCampId.add(newCamp.Id);
        System.runAs(saUser)
        {
            CampaignSharing.UpdateCampaignSharing(lstCampId);
        }

        //test that the share gets removed when we remove the Partner Account from the "Partner 1" field
        newCamp.Primary_Partner_Involved__c = null;
        update(newCamp);

        Test.stopTest();
        campaignShareRet = [SELECT CampaignAccessLevel, CampaignId, Id, RowCause, UserOrGroupId FROM CampaignShare where RowCause = 'Manual' and CampaignId = :newCamp.Id];
        System.debug('CampaignSharingTest: testUpdateSharing campaignShareRet = ' + campaignShareRet);
        System.Assert(campaignShareRet.size() == 0);   //We should have removed the Manual shares that was on this Campaign

        
    }

    private static testMethod void testUpdateSharingWithNoCampaigns() {
        System.debug('CampaignSharingTest: testUpdateSharing starting');
        QTTestUtils.GlobalSetUp();
        ApexSharingRules.TestingCampaignShare = true;
        List<Id> campIds = new List<Id>();
        Boolean bRet = CampaignSharing.UpdateCampaignSharing(campIds);
    }

    private static testMethod void testUpdateSharingWithCampaignsWithNoPrimaryPartner() {
        System.debug('CampaignSharingTest: testUpdateSharing starting');
        QTTestUtils.GlobalSetUp();
        ApexSharingRules.TestingCampaignShare = true;
        List<Id> campIds = new List<Id>();

        campIds.add('7017E00000097M5');    //a fake campaign Id    
        Boolean bRet = CampaignSharing.UpdateCampaignSharing(campIds);
    }


    private static Campaign createMockCampaign()
    {
        Campaign newCamp = new Campaign();
        System.runAs(QTTestUtils.createMockUserForProfile('System Administrator'))
        {
            newCamp.Name = 'MyTestCampaign01';
            newCamp.Planned_Opportunity_Value__c = 10000;
            insert(newCamp);
        }
        return newCamp;
    }

    private static User createMockPartnerUser(User me){

        User aUser = QTTestUtils.createMockUserForProfile('PRM - Independent Territory + QlikBuy');        
        aUser.IsPortalEnabled = true;        
        System.runAs (me) 
        {
            update aUser;
        }
        return aUser;
    }

    public static User createMockPartnerUser(User me, Contact con, Id profId)
    {
        User u = new User(
            alias = 'adminUsr', email='adminUser@xtest.sndbox.com',
            Emailencodingkey='UTF-8', lastname='adminTest', 
            Languagelocalekey='en_US', localesidkey='en_US',
            Profileid = profId,
            Timezonesidkey='America/Los_Angeles', 
            ContactId = con.Id,
            Username = System.now().millisecond() + '_' + 'newuser@xtest.sndbox.com'
        );

        System.runAs ( me ) 
        {
            insert u;
            u.IsPortalEnabled = true;
            update u;
        }

        return u;
    }

    private static Account createPartnerAccount(User aUser)
    {
        Account acc = new Account();
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('TestQTCompany', 'GBP', 'United Kingdom');
        System.runAs(aUser)
        {
            acc.Billing_Country_Code__c = qtComp.Id;
            acc.QlikTech_Company__c = qtComp.QlikTech_Company_Name__c;
            acc.Name = 'partneracc';
            acc.RecordTypeId = PartnerAccRecTypeId;
            Insert acc;
        } 
        return acc;
    }

    private static Contact createContact(User aUser, Account acc)
    {
        Contact con = new Contact();
        System.runAs(aUser)
        {
            con.FirstName = 'Anycon';
            con.Lastname = 'test01';
            con.AccountId = acc.Id;
            con.Email = 'Anycon.test01@test.sndbx.com';
            Insert con;
        } 
        return con;
    }

}