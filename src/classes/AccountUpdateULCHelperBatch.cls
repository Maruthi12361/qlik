/*
    2018-01-23  AIN     Initial implementation for CHG0032866
*/
global class AccountUpdateULCHelperBatch implements Database.Batchable<sObject> {
    
    String query;
    public Set<Id> accIds;
    global AccountUpdateULCHelperBatch(Set<ID> accIdsVar) {
        accIds = accIdsVar;
        query = 'select Id, Toggle_dirty__c from ULC_Details__c where ULCStatus__c = \'Active\' AND ContactId__r.Accountid in :accIds';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ULC_Details__c> scope) {
        for (ULC_Details__c ulc : scope) {
            ulc.Toggle_dirty__c = !ulc.Toggle_dirty__c;
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}