/**
* Change log:
*
*  2018-07-04  ext_vos  CHG0033919: add testResellTrainingCardNotify().
*  2019-03-05  ext_vos	CHG0035575: add testSendEmailAboutCPQEducationCards().
*****************************************************************/
@isTest
public class TrainingCardManagementTriggerTest {

    @TestSetup 
    static void setup() {
        QuoteTestHelper.createCustomSettings();
        List<QTCustomSettings__c> settings = [select Id, OppEducationServicesEmailAddress__c from QTCustomSettings__c where Name = 'Default' limit 1];
        if (settings.isEmpty()) {
            QTCustomSettings__c settings2 = new QTCustomSettings__c();
            settings2.Name = 'Default';
            settings2.OppEducationServicesEmailAddress__c = 'test@test.com';
            settings2.QlikNoReplyEmailAddress__c = 'no-reply@qlik.com';
            settings2.QlikEducationOrgWideAddress__c = 'QlikEducation@test.com';
            insert settings2;
        } else {
            settings[0].OppEducationServicesEmailAddress__c = 'test@test.com';
            settings[0].QlikEducationOrgWideAddress__c = 'QlikEducation@test.com';
            update settings[0];
        }        
        Training_Card_Email_Links__c links = new Training_Card_Email_Links__c();
        insert links; 

        String folderId = [select Id from Folder where DeveloperName = 'Images_and_Logos' and Type = 'Document' limit 1].Id;
        List<Document> docs = new List<Document>();
        Document doc1 = new Document();
        doc1.Name = 'Qlik_Sidebar_Graphic' + '_TEST';
        doc1.DeveloperName = 'Qlik_Sidebar_Graphic' + '_TEST';
        doc1.FolderId = folderId;
        docs.add(doc1);
        Document doc2 = new Document();
        doc2.Name = 'Qlik_Education_Service_Logo' + '_TEST';
        doc2.DeveloperName = 'Qlik_Education_Service_Logo' + '_TEST';
        doc2.FolderId = folderId;
        docs.add(doc2);
        insert docs; 
    }

    public static testmethod void testResellTrainingCardNotify() {
        User tUser = [Select id From User where id =: UserInfo.getUserId()];
        System.runAs(tUser) {
            EmailTemplate e = new EmailTemplate (DeveloperName = 'Resell_Training_Card_Notify_Email_Template', FolderId = UserInfo.getUserId(),
                                                TemplateType= 'Text', Name = 'Resell_Training_Card_Notify_Email_Template', Subject = 'Test CUSTOMER_COMPANY', 
                                                HtmlValue = 'PARTNER_FN, PARTNER_LN, PARTNER_COMPANY, CUSTOMER_FN, CUSTOMER_LN, CUSTOMER_COMPANY, CARD_NUMBER, ' 
                                                            + 'CARD_BALANCE, CARD_EXP_DATE, CARD_COMPANY, CARD_COUNTRY, CURRENT_YEAR, QLIK_LOGO, EDUCATION_LOGO, LINE_IMAGE, ' 
                                                            + 'LINE_NEW_IMAGE, FOOTER_IMAGE, QLIKTECH_LOGO_LINK, FACEBOOK_LOGO_LINK, TWITTER_LOGO_LINK, LINKEDIN_LOGO_LINK'
                                                            + 'BUNDLE_LINK, CARD_CHECK_BALANCE_LINK, COMMUNITY_LINK, CONTACT_US_LINK, FACEBOOK_LINK, FAQ_LINK, GENERAL_LINK,'
                                                            + 'LINKED_IN_LINK, REGISTER_LINK, SERVICE_TRAINING_LINK, SUBSCRIBE_LINK, TERMS_LINK, TRAINING_LINK, TWITTER_LINK');
            insert e;
        }
        
        RecordType PARecordType = [select Id, Name from RecordType 
                                        where Name = 'Partner Account' and sobjecttype='Account'];            
        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
        qtComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :qtComp.Id];
        Account partnerAcc = new Account(Name = 'TestAccount', QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
                                            Billing_Country_Code__c = qtComp.Id, RecordTypeId = PARecordType.Id,
                                            Navision_Status__c = 'Partner');
        insert partnerAcc;
        Contact parCon = new Contact(LastName = 'parenttestcon', AccountId = partnerAcc.id);
        insert parCon;
        RecordType ptAcc = [select Id from RecordType where DeveloperName = 'End_User_Account' and SobjectType = 'Account'];
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', tUser, true);
        testAccount.RecordtypeId = ptAcc.Id;
        update testAccount;
        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c> {
                                                        createQlikTech('France', 'FR', testSubs.id), 
                                                        createQlikTech('Afghanistan', 'AF', testSubs.id), 
                                                        createQlikTech('Japan', 'JP', testSubs.id), 
                                                        createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        Account testAccount2 = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount2;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
        RecordType rType = [Select id From RecordType Where developerName = 'Sales_QCCS']; 
         
        Opportunity opp = QuoteTestHelper.createOpportunity(testAccount, '', rType);
        insert opp;

        Profile prUser = [select Id from Profile where Name = 'Custom: Api Only User'];
        User testUser = new User(Alias = 'standt', Email = 'ResellTrainingCard@test.com',
                                    EmailEncodingKey =' UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                                    LocalesIdKey='en_US', ProfileId = prUser.Id,
                                    TimeZonesIdKey='America/Los_Angeles', UserName='ResellTrainingCard@test.com');
        
        System.runAs(testUser) {

            opp.CloseDate = Date.today().addDays(10); 
            opp.StageName = 'Closed Won';
            opp.Revenue_Type__c = 'Reseller';
            opp.Partner_Contact__c = parCon.Id;
            opp.Sell_Through_Partner__c = testAccount2.Id;
            opp.Included_Products__c = 'Qlik Sense';
            Semaphores.OpportunityTriggerBeforeUpdate2 = false;
            Semaphores.OpportunityTriggerAfterUpdate = false;
            update opp;

            // check email sending flag
            System.assert(!TrainingCardManagementTriggerHandler.RESELL_EMAIL_WAS_SENT);
            
            // init email sending
            Semaphores.TrainingCardManagementTriggerAfterInsert = false;
            Date d = Date.today();
            List<Voucher_Management__c> vm = new List<Voucher_Management__c>();
            vm.add(new Voucher_Management__c(OpportunityId__c = opp.Id, Voucher_Number__c = '111111', Expiry_Date__c = d.addDays(10), Quantity__c = 125));
            vm.add(new Voucher_Management__c(OpportunityId__c = opp.Id, Voucher_Number__c = '222222', Expiry_Date__c = d.addDays(15)));
            insert vm;

            // check email sending flag
            System.assert(TrainingCardManagementTriggerHandler.RESELL_EMAIL_WAS_SENT);
        }
    }

	public static testmethod void testSendEmailAboutCPQEducationCards() {
		Test.startTest();	
        
        Profile prUser = [select Id from Profile where Name = 'System Administrator'];
       	User testUser = new User (ProfileId = prUser.Id, Alias = 'sadmU', Email = 'sadm@test.com',
						            EmailEncodingKey =' UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
						            LocalesIdKey='en_US', TimeZonesIdKey='America/Los_Angeles', 
						            UserName=System.now().millisecond() + '_' +'sadm@test.com');
       	Opportunity opp;
       	System.runAs(testUser) {
       		User tUser = [Select id From User where id =: UserInfo.getUserId()];
	        RecordType paRecordType = [select Id, Name from RecordType where Name = 'Partner Account' and SObjectType = 'Account'];
	        QlikTech_Company__c qtComp = QTTestUtils.createMockQTCompany('QlikTech Inc','USA', 'USA');
	        qtComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :qtComp.Id];
	        Account partnerAcc = new Account(Name = 'TestAccount', QlikTech_Company__c = qtComp.QlikTech_Company_Name__c, 
	                                            Billing_Country_Code__c = qtComp.Id, RecordTypeId = paRecordType.Id,
	                                            Navision_Status__c = 'Partner');
	        insert partnerAcc;
	        Contact parCon = new Contact(LastName = 'parenttestcon', AccountId = partnerAcc.id);
	        insert parCon;        
			RecordType rTypeAcc = [select Id, Name, DeveloperName from RecordType where DeveloperName = 'End_User_Account' and SObjectType = 'Account']; 
	        Account testAccount = QTTestUtils.createMockAccount('TestCompany', tUser, true);
	        testAccount.RecordtypeId = rTypeAcc.Id;
	        update testAccount;
	  		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];

	        Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
	        List<QlikTech_Company__c> listOfcreate = new List<QlikTech_Company__c> {
	        												createQlikTech('France', 'FR', testSubs.id), 
	                                                        createQlikTech('Afghanistan', 'AF', testSubs.id), 
	                                                        createQlikTech('Japan', 'JP', testSubs.id), 
	                                                        createQlikTech('Albania', 'AL', testSubs.id)};
	        insert listOfcreate;
	        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
	        testContact.HasOptedOutOfEmail = false;
	        insert testContact;

	        RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
	        Date closeDate = Date.today();
	        closeDate.addMonths(2);
	        opp = new Opportunity(Short_Description__c = 'TestOpp', Name = 'TestOpp1578', Type = 'New Customer', Revenue_Type__c = 'Direct',
	    										CloseDate = Date.today(), StageName = 'Alignment Meeting', RecordTypeId = rt.Id, CurrencyIsoCode = 'GBP', 
	    										AccountId = testAccount.Id, Signature_Type__c = 'Digital Signature', ForecastCategoryName = 'Omitted');
	        insert opp;
	    }	

		// use Pricebook with different currency
		QTTestUtils.SetupNSPriceBook('GBP');
        ID pbookId = [Select Id from PricebookEntry where IsActive = true and CurrencyIsoCode = 'GBP' and Product2.Family = 'Education' LIMIT 1].Id;
        
        // create OpportunityLineItem for ONS training card
        OpportunityLineItem lineItemONS = new OpportunityLineItem();           
        lineItemONS.OpportunityId = opp.Id;
        lineItemONS.UnitPrice = 10;
        lineItemONS.Quantity = 2;
        lineItemONS.PricebookEntryId = pbookId;    
        lineItemONS.Voucher_Code__c = 'TS-0001578'; 
        lineItemONS.Application_Name__c = 'ONS';
        insert lineItemONS; 

		// create OpportunityLineItem for ILT training card
        OpportunityLineItem lineItemILT = new OpportunityLineItem();           
        lineItemILT.OpportunityId = opp.Id;
        lineItemILT.UnitPrice = 10;
        lineItemILT.Quantity = 2;
        lineItemILT.PricebookEntryId = pbookId;    
        lineItemILT.Voucher_Code__c = 'TS-0001579'; 
        lineItemILT.Application_Name__c = 'CLS';   
        insert lineItemILT; 
			
		Test.stopTest();

		QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];        
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        Account testAccount2 = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
        insert  testAccount2;
        Contact parCon = [select Id from Contact where LastName = 'parenttestcon'];        
 		Profile pUser = [select Id from Profile where Name = 'Custom: Api Only User'];
        User tUser = new User(Alias = 'standt', Email = 'newTrainingCard@qlik.com',
						            EmailEncodingKey =' UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
						            LocalesIdKey='en_US', ProfileId = pUser.Id,
						            TimeZonesIdKey='America/Los_Angeles', UserName='newTrainingCard@test.com');
 		System.runAs(tUser) {
            opp.CloseDate = Date.today().addDays(10); 
            opp.StageName = 'Closed Won';
            opp.Revenue_Type__c = 'Reseller';
            opp.Partner_Contact__c = parCon.Id;
            opp.Sell_Through_Partner__c = testAccount2.Id;
            opp.Included_Products__c = 'Qlik Sense';
            opp.VoucherStatus__c = 'Vouchers Created and Active';
            Semaphores.OpportunityTriggerBeforeUpdate2 = false;
            Semaphores.OpportunityTriggerAfterUpdate = false;
            update opp;

            // check email sending flag
            System.assert(!TrainingCardManagementTriggerHandler.ONS_EMAIL_WAS_SENT);
            
            // init email sending
            Semaphores.TrainingCardManagementTriggerAfterInsert = false;
            Date d = Date.today();
            List<Voucher_Management__c> vm = new List<Voucher_Management__c>();
            vm.add(new Voucher_Management__c(OpportunityId__c = opp.Id, Voucher_Number__c = 'TS-0001579', Expiry_Date__c = d.addDays(10), Quantity__c = 125, Opportunity_Line_Item_Id__c = lineItemILT.Id));
            vm.add(new Voucher_Management__c(OpportunityId__c = opp.Id, Voucher_Number__c = 'TS-0001578', Expiry_Date__c = d.addDays(15), Opportunity_Line_Item_Id__c = lineItemONS.Id));
            
            insert vm;

            // check email sending flag
            System.assert(TrainingCardManagementTriggerHandler.ONS_EMAIL_WAS_SENT);
            System.assert(TrainingCardManagementTriggerHandler.ILT_EMAIL_WAS_SENT);
            System.assert(TrainingCardManagementTriggerHandler.CUR_EMAIL_WAS_SENT);
        }
    }

    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
            								Country_Code_Two_Letter__c = countryAbbr, 
            								Country_Name__c = countryName, 
            								Subsidiary__c = subsId);
   		return qlikTechIns;
    }

}