public with sharing class ContractedPriceExtController {
    private Id quoteId;
    public SBAA__Approval__c[] approvals {get; set;}
    public ContractedPriceExtController(ApexPages.StandardController stdController) {
        quoteId = stdController.getId();
    }
    public PageReference onSubmit() {
        if (quoteId != null) {
            SBAA.ApprovalAPI.submit(quoteId, SBAA__Approval__c.ContractedPrice__c);
        }
        return new PageReference('/' + quoteId);
    }
    public PageReference onRecall() {
        if (quoteId != null) {
            SBAA.ApprovalAPI.recall(quoteId, SBAA__Approval__c.ContractedPrice__c);
        }
        return new PageReference('/' + quoteId);
    }
}