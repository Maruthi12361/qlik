/***************************************************
* SpecificationAssignApproverTest
*
* 20140923	RDZ		Initial developement, to fix issue with triggers with no code coverage.
*					CR# 16608 to fix QA test classes
***************************************************/


@isTest
private class SpecificationAssignApproverTest
{
	public static SLX__Change_Control__c createMockSalesCR(Id Requestor)
	{
		SLX__Change_Control__c ChangeControl = new SLX__Change_Control__c(
        Business_Owner__c = '005D0000001qrhb',    //Simon McAllister
        Business_Area__c = 'Sales');
        ChangeControl.Name = 'My test change control';
        ChangeControl.SLX__Requestor__c = Requestor;
		insert ChangeControl;
		return ChangeControl;
	}

	public static SLX__Change_Control__c createMockSalesCR()
	{
		
		//Create CR with Requestor current user id
		return createMockSalesCR(UserInfo.getUserId());
	}

	@isTest
	static void SpecificationAssignApproverOnInsert()
	{
		// Given
		//CR
		SLX__Change_Control__c ChangeControl = createMockSalesCR();

        //Specification
        Specification__c Specification = new Specification__c();
        Specification.Change_Control__c = ChangeControl.Id;
        Specification.Name = 'Requirement 1';
    	Test.startTest();            
		// When Specification is inserted
		insert Specification;
        
        Specification = [select Id, Name, Requestor__c from Specification__c where Id = :Specification.Id];

		// Then Specification requestor shoudl be the same as change control requestor.
		System.assertEquals(UserInfo.getUserId(), Specification.Requestor__c);
		Test.stopTest();
	}
	
}