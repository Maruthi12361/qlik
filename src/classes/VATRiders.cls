public class VATRiders {
        
        List<Rider__c> Riders = null;

        public List<Rider__c> getRiders()
        {
                if (Riders != null)
                {
                        return Riders;
                }
                
        Date d = Date.newInstance(Date.today().year(), 1, 1);
        
        if (System.currentPagereference().getParameters().containsKey('Year'))
        {
            try
            {
                d = Date.newInstance(Integer.valueOf(System.currentPagereference().getParameters().get('Year')), 1, 1);
            }
            catch(System.Exception Ex)
            {
                
            }
        }               
        
                string soql = 'Select r.Years_Ridden_for_QT__c, r.User__c, r.Travelling_Via__c, r.Travel_Leader__c, r.Total_Distance_km__c, r.SystemModstamp, r.Representing__c, r.OwnerId, r.Number__c, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsDeleted, r.Id, r.Flight_Details__c, r.CurrencyIsoCode, r.CreatedDate, r.CreatedById, r.Bike__c, r.Bike_Link__c From Rider__c r WHERE r.Years_Ridden_for_QT__c like \'%' + d.Year() + '%\' ORDER BY r.Name';

                Riders = database.query(soql);          
                
                return Riders;
        }

        public static testMethod void ControllerTest_1()
        {
                VATRiders VB = new VATRiders();
                
                List<Rider__c> RiderEntries = VB.getRiders();
                
                VB = new VATRiders();
                                
                System.currentPageReference().getParameters().put('Year', '2000');
                
                RiderEntries = VB.getRiders();          
        } 

}