/*BSL-1224 created by shubham Gupta 9/1/2019
This Handler is set on Account trigger and will fire if DUNS is populated or updated, it will check if any other account exst 
with same DUNS number and will match the Accountstatus of both the accounts having same DUNS number 
*/

public class DunsHandlerAccount {
    

    public static void populateCustomer(List<Account> newAccounts, Map<id,Account> oldaccmap){
        Map<String,String> dunstostatus = new Map<String,String>();
        Set<String> duns = new Set<String>();
        List<Opportunity> opptonaccount = new List<Opportunity>();
        List<id> nomatchaccount = new List<id>();
        Map<id,Set<String>> acctoppstage = new Map<id,Set<String>>();
        for(Account acc:newAccounts){
            if(acc.Domestic_Ultimate_DUNS__c != oldaccmap.get(acc.id).Domestic_Ultimate_DUNS__c && acc.Domestic_Ultimate_DUNS__c != '000000000' && !String.isBlank(acc.Domestic_Ultimate_DUNS__c) && String.isBlank(acc.AccountStatus__c)){
            duns.add(acc.Domestic_Ultimate_DUNS__c);
            }
        }

        if(!duns.isEmpty()){
        List<Account> resAccounts = [select id,AccountStatus__c,Domestic_Ultimate_DUNS__c from Account where Domestic_Ultimate_DUNS__c IN :duns AND (AccountStatus__c = 'Customer' OR AccountStatus__c = 'Prospect')];
        Set<String> foundDuns = new Set<String>();
        if(!resAccounts.isEmpty()){
            
            for(Account acct:resAccounts){
                foundDuns.add(acct.Domestic_Ultimate_DUNS__c);
                dunstostatus.put(acct.Domestic_Ultimate_DUNS__c,acct.AccountStatus__c);
            }
        }

        for(Account acc:newAccounts){
            if(foundDuns.contains(acc.Domestic_Ultimate_DUNS__c)){
                    acc.AccountStatus__c = dunstostatus.get(acc.Domestic_Ultimate_DUNS__c);
                }
                else{
                    nomatchaccount.add(acc.id);
                }
            }
        if(!nomatchaccount.isEmpty()){
            opptonaccount = [Select id,AccountID,StageName from Opportunity where AccountID IN :nomatchaccount];
            if(!opptonaccount.isEmpty()){
                For(Opportunity opp:opptonaccount){
                    if(acctoppstage.containskey(opp.AccountID)){
                        acctoppstage.get(opp.AccountID).add(opp.Stagename.toLowerCase());
                    }
                    else{
                    acctoppstage.put(opp.AccountID,new Set<String>{opp.Stagename.toLowerCase()});
                    }
                }
            }
            system.debug('map came'+acctoppstage);
            For(Account acc:newAccounts){
                if(acctoppstage.containsKey(acc.Id)){
                    if((acctoppstage.get(acc.Id).contains('closed won') || acctoppstage.get(acc.Id).contains('oem - closed won')) && !acctoppstage.get(acc.Id).contains('closed lost')){
                    acc.AccountStatus__c = 'Customer';
                    system.debug('closed found');
                    }
                    else if(!acctoppstage.get(acc.Id).contains('closed lost')){
                    system.debug('open found');
                    acc.AccountStatus__c = 'Prospect';
                    }
                }
            }
        }
    }
    }
    public static void dunsChangesAfter(List<Account> newAccounts, Map<id,Account> oldaccmap){
        Set<Id> acctMatched = new Set<Id>();
        List<Opportunity> oppties = new List<Opportunity>();
        List<Opportunity> toupdate = new List<Opportunity>();
        for(Account acc:newAccounts){
            if(acc.AccountStatus__c != oldaccmap.get(acc.id).AccountStatus__c && acc.AccountStatus__c == 'Customer' && String.isEmpty(oldaccmap.get(acc.id).AccountStatus__c))
            acctMatched.add(acc.ID);
        }
        if(!acctMatched.isEmpty()){
        oppties = [Select id,New_Customer__c,AccountID,StageName from Opportunity where New_Customer__c = true AND (NOT StageName like '%Closed%') AND AccountID IN :acctMatched];
        if(!oppties.isEmpty()){
            For(Opportunity opp:oppties){
                    opp.New_Customer__c = false;
                    toupdate.add(opp);
            }
            if(!toupdate.isEmpty()){
                update toupdate;
            }  
        }
    }
    }
}