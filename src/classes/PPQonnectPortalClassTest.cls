@isTest
private class PPQonnectPortalClassTest {
    
    static testmethod  void MarketingPortalClassTest2(){
        setupContent();
        Test.startTest();
        User u=   createUser();
        PPQonnectPortalClass m = new PPQonnectPortalClass();
        m.getContentVersions();
        Test.stopTest();
        
    }
    
    static testmethod  void MarketingPortalClassTest(){
        setupContent();
        setupNewdata();
        setCustomSetting();
        Test.startTest();
         User u=   createUser();
        Test.stopTest();
        PPQonnectPortalClass m = new PPQonnectPortalClass();
        //m.createPortalPublisherMap();
        system.runAs(u) {
             setupNewdata();
         m.getUserInfo();
          m.getTask();
          m.redirect();
          m.checkAccess();  
          m.newTask();
          m.frameUrl();
          m.getCaseInfo(); 
          m.getOptyInfo();  
          m.setHomelinks(); 
          m.getContentVersions();
          m.getWhatsNew();
          m.getUpcomingEvent();
          m.getBusinessInfo();
        }
    }
    
    public static void setCustomSetting()// sets customsetting if there is no value in custom setting.
      {
        List<PPConfigSettings__c> idList = [select Id__c from PPConfigSettings__c];
        if(idList.isEmpty())
            {
                PPConfigSettings__c e = new PPConfigSettings__c(Name='optyListView',Id__c='');
                PPConfigSettings__c e1 = new PPConfigSettings__c(Name='caseListView',Id__c='');
                PPConfigSettings__c e2 = new PPConfigSettings__c(Name='custListView',Id__c='');
                PPConfigSettings__c e3 = new PPConfigSettings__c(Name='eventsListView',Id__c='');
                PPConfigSettings__c e4 = new PPConfigSettings__c(Name='NewsListView',Id__c='');
                idList.add(e);
                idList.add(e1);
                idList.add(e2);
                idList.add(e3);
                idList.add(e4);
                insert idList;
            }
           
        }
        
    public static User createUser()// creates partner portal user
    {
        QlikTech_Company__c q = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'USA', 'USA');
        Account acct = new Account(Name='Test Account1', BillingCountry='United States',BillingState='WWT', CurrencyISOCode='USD',QlikTech_Company__c='CT',QlikTech_Region__c='CT',Billing_Country_Code__c=q.Id,Website='www.qt.com',BillingPostalCode='566676',BillingStreet='teststreet',BillingCity='testcity');
        insert acct;
        Contact cntact= new Contact(LastName='ContactName',AccountId=acct.id, Email = 'aaa@bbb.com',FirstName='Contact FirstName',Phone='123456',T_C_MDF_Accepted__c = true);
        insert cntact;
        acct.IsPartner = true; 
        update acct;
        Business_Plan__c b = new Business_Plan__c(Q1_Revenue_Target__c=10,Q2_Revenue_Target__c=10,Q3_Revenue_Target__c=10,Q4_Revenue_Target__c=10,Yearly_Revenue_Target__c=100,Partner_Account__c=acct.Id);
        b.valid_From__c= system.today();
        insert b;
        Profile p = [select id from Profile where name like '%PRM%' and UserLicense.Name = 'Gold Partner' limit 1];   
        User u;
        User u1 = [select Id,DefaultCurrencyIsoCode,CurrencyIsoCode,ContactId,AccountId from User where Id=:System.UserInfo.getUserId()  limit 1 ];
        System.runas(u1)
          {
            UserRole roleId = new Userrole(PortalType='Partner',PortalAccountId =acct.Id);
            insert(roleId);
            u = new User(alias = 'standt', email='standarduser@testorg.com', emailencodingkey='UTF-8', languagelocalekey='en_US', 
                    localesidkey='en_US',timezonesidkey='America/Los_Angeles', username='standarduser2315@testorg.com', lastname = 'lname',
                    contactid= cntact.id, profileId = p.Id,UserRoleId=roleId.Id, CompanyName = acct.id);//
            insert u; 
          }  
          PPMarketingPortalClass.testContact = [SELECT Id, Name, FirstName, LastName, Email, Phone, OwnerId, Account_Country_Code__c, Account_Street__c,Account_City__c, Account_State_Province__c, Account_Zip_Postal_Code__c, pse__Region__c FROM Contact Where Id=:u.ContactId limit 1];
          return u;
     }
	public static ContentVersion setupContent() // inserts content and download history
    { 
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.VersionData = Blob.valueOf('testString');      
        //CTS
        contentVersion.Description = 'Description TEST ';
        contentVersion.PathOnClient = 'test.txt';
        insert contentVersion; 
        List<ContentVersionHistory> hisList = new List<ContentVersionHistory>();     
        ContentVersionHistory cvh = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');   
        ContentVersionHistory cvh1 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        ContentVersionHistory cvh2 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        ContentVersionHistory cvh3 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        ContentVersionHistory cvh4 = new ContentVersionHistory(contentVersionId=contentVersion.Id,field = 'contentVersionDownloaded');
        hisList.add(cvh);
        hisList.add(cvh1);
        hisList.add(cvh2);
        hisList.add(cvh3);
        hisList.add(cvh4);
        insert hisList; 
        System.debug('hislist---'+hisList); 
        
        
   
        return contentVersion;  
    }
    private static void setupNewdata(){
        List<What_s_New__c> wl = new List<What_s_New__c>();
        wl.add(new What_s_New__c(Active__c = true,Published_Date__c = system.today(),Headline__c ='TEST 1',Link__c='www.google.com'));
        wl.add(new What_s_New__c(Active__c = true,Published_Date__c = system.today(),Headline__c ='TEST 2',Link__c='www.google.com'));
        wl.add(new What_s_New__c(Active__c = true,Published_Date__c = system.today(),Headline__c ='TEST 3',Link__c='www.google.com'));
        wl.add(new What_s_New__c(Active__c = true,Published_Date__c = system.today(),Headline__c ='TEST 4',Link__c='www.google.com'));
        wl.add(new What_s_New__c(Active__c = true,Published_Date__c = system.today(),Headline__c ='TEST 5',Link__c='www.google.com'));
        wl.add(new What_s_New__c(Active__c = true,Published_Date__c = system.today(),Headline__c ='TEST 6',Link__c='www.google.com'));
        insert wl;
        List<Upcoming_Events__c> Upl = new List<Upcoming_Events__c>();
        Upl.add(new Upcoming_Events__c(Active__c = true,Published_Date__c = system.today(),Event_Name__c ='TEST 1',Link__c='www.google.com'));
        Upl.add(new Upcoming_Events__c(Active__c = true,Published_Date__c = system.today(),Event_Name__c ='TEST 2',Link__c='www.google.com'));
        Upl.add(new Upcoming_Events__c(Active__c = true,Published_Date__c = system.today(),Event_Name__c ='TEST 3',Link__c='www.google.com'));
        Upl.add(new Upcoming_Events__c(Active__c = true,Published_Date__c = system.today(),Event_Name__c ='TEST 4',Link__c='www.google.com'));
        Upl.add(new Upcoming_Events__c(Active__c = true,Published_Date__c = system.today(),Event_Name__c ='TEST 5',Link__c='www.google.com'));
        Upl.add(new Upcoming_Events__c(Active__c = true,Published_Date__c = system.today(),Event_Name__c ='TEST 6',Link__c='www.google.com'));
         insert Upl; 
        
        List<Task> tl = new List<Task>();
        List<user> u = [select id,contactId from user where id=:Userinfo.getUserId()];
        tl.add(new Task(Description='test1',subject='TESTS5',ActivityDate=system.today(),WhoId=u[0].contactid,Type='TK - Email'));
        tl.add(new Task(Description='test2',subject='TESTS4',ActivityDate=system.today(),WhoId=u[0].contactid,Type='TK - Email'));
        tl.add(new Task(Description='test3',subject='TESTS3',ActivityDate=system.today(),WhoId=u[0].contactid,Type='TK - Email'));
        tl.add(new Task(Description='test4',subject='TESTS2',ActivityDate=system.today(),WhoId=u[0].contactid,Type='TK - Email'));
        tl.add(new Task(Description='test5',subject='TESTS1',ActivityDate=system.today(),WhoId=u[0].contactid,Type='TK - Email'));
        insert tl;
    }
}