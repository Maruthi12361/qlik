/******************************************************

    Class: SEPForUser_Controller
    
    Changelog:
        2013-09-12  KMH     Initial development. CR# 9284 - Controller to provide
                            link betweent the User object and the Sales Enablement Program custom object
                            Controller class for SEPMultiple_Edit.page and SEPForUser pages.
        2014-02-14  SLH     Restricted enablement list to running user i.e. only show records for this user
                            Query restricted in getSalesEnablementProgramList and getReturnSEPList
        
******************************************************/

public with sharing class SEPForUser_Controller {

    list <Sales_Enablement_Program__c> SEPObjects = new List<Sales_Enablement_Program__c>();
    public string ErrorMessage { get; set; } 
    private String Userid;
 
    public SEPForUser_Controller(ApexPages.StandardController controller) {

     //Save the User Id so we can navigate back to the record when we do a Save or Cancel
     Userid = ApexPages.currentPage().getParameters().get('id'); 
    }
    
    // Used on SEPMultiple_Edit page
    public list<Sales_Enablement_Program__c> getSalesEnablementProgramList()
    {
        Userid = ApexPages.currentPage().getParameters().get('User_Id__c');
         
        List<Sales_Enablement_Program__c> l_SEP = new List<Sales_Enablement_Program__c>();
        l_SEP = [Select Id,Sales_Enablement_Program__c,Name,Comments__c,Schedule_Date__c,Completed_Date__c  from Sales_Enablement_Program__c  where User_Id__c = :Userid];
                        
        SEPObjects.clear(); //so we don't duplicate the list when we addAll()
        SEPObjects.addAll(l_SEP);
        
        
        Sales_Enablement_Program__c sep = new Sales_Enablement_Program__c();
        sep.User_Id__c = Userid;
        SEPObjects.add(sep);
        
        return SEPObjects;
    
    }
    //Used on SEPForUser page
    public list<Sales_Enablement_Program__c> getReturnSEPList()
    {
        SEPObjects = [Select Id,Sales_Enablement_Program__c,Name,Comments__c,Schedule_Date__c,Completed_Date__c from Sales_Enablement_Program__c where User_Id__c = :Userid];
        
        return SEPObjects;
    
    }
    
    //Used on SEPForUser page to redirect to SEPMultiple_Edit page
    public PageReference RedirectToSEPPage() {
        
        Pagereference p;        
        
        //p = new Pagereference('/apex/SEPMultiple_Edit?User_Id__c=' + Userid);
        p = new Pagereference('/');
        //p = ApexPages.currentPage();
        p.getParameters().put('retURL', 'apex/SEPMultiple_Edit?User_Id__c=' + Userid);
        p.setRedirect(true); 
        return p;
    }
    
    // Used on SEPMultiple_Edit page for Cancel
    public PageReference CancelSet() {
        
        PageReference p ;
        p = new Pagereference('/'+ Userid);     
        p.setRedirect(true);
        return p;           
    }
    
    // Used on SEPMultiple_Edit page 
    public PageReference QuickSaveSet() {       
        DoTheSave();
        return System.currentPageReference();           
    }
    // Used on SEPMultiple_Edit page for Save & Add 
    public PageReference SaveSet() {
        if (!DoTheSave()) return System.currentPageReference(); //if the Save failed we stay on the same page so we see the captured error messages
        
        PageReference pr = new PageReference('/');  
        if (Userid != null)
        {       
            pr = new PageReference('/' + Userid);
              
        }       
        pr.setRedirect(true);
        return pr;          
    }
    
    
    //Save any changes to the page. If no changes have been made to the record we added earlier then we remove it before we save.
    public boolean DoTheSave() {
        boolean errFlg = true;
        try {
            System.debug('multiSEPEdit: SEPObjects[SEPObjects.size()-1] = ' + SEPObjects[SEPObjects.size()-1]);
            Sales_Enablement_Program__c sep = SEPObjects[SEPObjects.size()-1];
            if ((sep.Sales_Enablement_Program__c == 'None') && sep.Schedule_Date__c == null && sep.Completed_Date__c == null && sep.Comments__c == null)
            {
                Sales_Enablement_Program__c delSoEX = SEPObjects.remove(SEPObjects.size()-1);   //remove the unused record 
                System.debug('multiSoEEdit: delSoEX = ' + delSoEX);
            }       
            upsert SEPObjects;
        }
        catch(Exception e) {
            System.debug('ERROR with multiSEPEdit: ' + e);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR with multiSEPEdit: ' + e.getMessage());
            ApexPages.addMessage(msg);
            errFlg = false;
        }
        return errFlg;      
    }
    
    // Returns the Edit view of the Sales_Enablement_Program object when clicked on edit link on SEPForUser
     public PageReference EditSEP()
    {
        ErrorMessage = '';
        
        if (System.currentPagereference().getParameters().get('SEPid') == '')
        {
            return System.currentPageReference();               
        }       

        try
        {
            String SEPName = System.currentPagereference().getParameters().get('SEPid');
            Sales_Enablement_Program__c SoE = [select Id from Sales_Enablement_Program__c where Name = :SEPName LIMIT 1];
            ApexPages.StandardController SoEController = new ApexPages.StandardController(SoE);
            
            PageReference EditRef = SoEController.edit();
           
            EditRef.getParameters().put('retURL', '/'+Userid);
            return EditRef;
        }
        catch (System.Exception Ex)
        {
            System.debug('EditSoE cought exception: ' + Ex.getMessage());
        }
        
        return System.currentPageReference();
    }
    // Delete the record from Sales_Enablement_Program object when clicked on delete link on SEPForUser
    public PageReference DeleteSEP()
    {
        ErrorMessage = '';
        
        if (System.currentPagereference().getParameters().get('SEPid') == '')
        {
            return System.currentPageReference();   
        }

        try
        {       
            String SEPName = System.currentPagereference().getParameters().get('SEPid');
            delete [select Id from Sales_Enablement_Program__c where Name = :SEPName];
        }
        catch (System.Exception Ex)
        {
            ErrorMessage = 'Could not delete Sequence of Events: ' + Ex.getMessage();
        }       
        //return System.currentPageReference();
        return null;       
    }   
    
    // Retruns the detail view of Sales_Enablement_Program object when clicked on SEP ID link on SEPForUser
    public PageReference DetailSEP()
    {
        ErrorMessage = '';
        
        if (System.currentPagereference().getParameters().get('SEPid') == '')
        {
            return System.currentPageReference();               
        }       

        try
        {
            String SEPName = System.currentPagereference().getParameters().get('SEPid');
            Sales_Enablement_Program__c SoE = [select Id from Sales_Enablement_Program__c where Name = :SEPName LIMIT 1];
            ApexPages.StandardController SoEController = new ApexPages.StandardController(SoE);
            
            PageReference DetailRef = SoEController.view();
            
            DetailRef.getParameters().put('retURL', '/'+Userid);
            return DetailRef;
        }
        catch (System.Exception Ex)
        {
            System.debug('EditSoE cought exception: ' + Ex.getMessage());
        }
        
        return System.currentPageReference();
    }

}