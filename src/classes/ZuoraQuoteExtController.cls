public with sharing class ZuoraQuoteExtController {
    private Id zquoteId;
    public ZuoraQuoteExtController(ApexPages.StandardController stdController) {
        zquoteId = stdController.getId();
    }
    public PageReference onSubmit() {
        if (zquoteId != null) {
            SBAA.ApprovalAPI.submit(zquoteId, SBAA__Approval__c.Zuora_Quote__c);
        }
        return new PageReference('/' + zquoteId);
    }
    public PageReference onRecall() {
        if (zquoteId != null) {
            SBAA.ApprovalAPI.recall(zquoteId, SBAA__Approval__c.Zuora_Quote__c);
        }
        return new PageReference('/' + zquoteId);
    }
}