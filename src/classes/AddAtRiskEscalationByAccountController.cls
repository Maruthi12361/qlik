/**
* Change log:
*
*   2019-07-24 ext_ciz	CHG0036474: create At_risk_escalation__c object by Account.
*   2019-09-30 ext_bad  IT-2173: Move functionality to the component
*/
public with sharing class AddAtRiskEscalationByAccountController {
    public String escId { get; set; }
    public String accId { get; set; }

    public AddAtRiskEscalationByAccountController getThis() {
        return this;
    }

    public AddAtRiskEscalationByAccountController() {
        escId = ApexPages.currentPage().getParameters().get('escId');
        accId = ApexPages.currentPage().getParameters().get('accId');
    }

    public PageReference back() {
        PageReference pg = new PageReference('/apex/AtRiskEscalationsList?accId=' + accId);
        pg.setRedirect(true);
        return pg;
    }
}