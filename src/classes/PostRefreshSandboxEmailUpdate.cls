/*
* File PostRefreshSandboxEmailUpdate
    * @description : Class for Post deployemnt activity for updating Lead and Contact Emails.
                     It will be executed after QA refresh in prod.
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       09.01.2018   Pramod Kumar V      Created Class 
  */
//global with sharing class PostRefreshSandboxEmailUpdate{
Public class PostRefreshSandboxEmailUpdate{

public void PostRefreshEmailUpdate () {
    
   
   // Boolean IsSandbox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
   // User u = [select Id, username from User where Id = :UserInfo.getUserId()];
    
    //if(IsSandbox=true && u.username.tolowercase().endswith('.qa')) { 
    UpdateContactEmailBatch updContactEmaiBatch = new UpdateContactEmailBatch();
    Database.executeBatch(updContactEmaiBatch,2000);
   
    UpdateLeadEmailBatch updLeadEmaiBatch = new UpdateLeadEmailBatch();
    Database.executeBatch(updLeadEmaiBatch,2000);
   
    // }
   
   }   
}