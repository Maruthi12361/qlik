/********
* NAME : OpportunityChangeRevenueTypeTest
* Description: test class to change an opportunity revenue type
* 
*
*Change Log:
*   2014-09-06 MTM Initial development
    MTM 2014-11-28 CR# 17952
    MTM 2014-12-03 CR# 19145
        Andrew Lokotosh 2016-06-08 Comented Forecast_Amount fields line 157
    Roman Dovbush (4front) : 2017-03-27 : Some methods were not executed, because try/catch block didn't throw an error.
                                          Got 'Internal Salesforce Error: 447044606-153587 (-581268086) (-581268086)' in
                                          testOpportunityChangeRevenueType_MockOpportunity so commented it out.
******/
@isTest
public with sharing class OpportunityChangeRevenueTypeTest {
    static void setupCustomSettings() {
        QTCustomSettings__c setting = new QTCustomSettings__c();
        setting.Name = 'Default';
        setting.BoomiBaseURL__c = 'https://www.google.com';
        setting.BoomiToken__c = 'BoomiToken';
        insert setting;
    }

    static testMethod void testOpportunityChangeRevenueType_NullParameters() {

        String bId = 'boomiId__test';       
        Test.startTest();
        String message = OpportunityChangeRevenueType.ChangeRevenueType(null, '', null, null, null,null);
        
      //  System.assert(message == '');
        Test.stopTest();
    }

    // getting 'Internal Salesforce Error: 447044606-153587 (-581268086) (-581268086)' on this test, so commented it out
    //static testMethod void testOpportunityChangeRevenueType_MockOpportunity(){
    //    //setupCustomSettings();
    //    QTTestUtils.GlobalSetUp();
    //    QTTestUtils.SetupNSPriceBook('GBP');

    //    User objUser = QTTestUtils.createMockOperationsAdministrator();
    //    Opportunity opp;
    //    Sphere_Of_Influence__c soi;
    //    List<sObject> toInsert = new List<sObject>();
    //    Pricebook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];
    //    try
    //    {
    //        System.RunAs(objUser)
    //        {       
    //            Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeRevenueType', objUser);
                
    //            RecordType rt = [SELECT Id, DeveloperName from RecordType where DeveloperName = 'Sales_QCCS'];
                
    //            if (rt != null){                    
    //                //Create and insert account and opp record type Sales QCCS
    //                Test.startTest();
				//		opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
    //                Test.stopTest();                    
                    
    //                opp = [SELECT Revenue_Type__c from Opportunity where Id= :opp.Id LIMIT 1];
    //                opp.PriceBook2Id = pb2NS.Id;
    //                update opp;
    //                //Add license product
    //                OpportunityLineItem oli = QTTestUtils.createSmallBusinessServerOppLineItem(opp, true, true);

    //               // System.Assert(opp.Revenue_Type__c == 'Direct');
                    
    //                //Call ChangeRevenueType to change the RevenueType to Direct
    //                OpportunityChangeRevenueType.ChangeRevenueType(opp.Id, 'Direct QSG', null,null,null,null);
                    
    //                //Get opp atributes to check if method set the correct values, removed all OLIs and Quotes.
    //                opp = [SELECT Revenue_Type__c from Opportunity where Id= :opp.Id LIMIT 1];
    //                List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where Id= :oli.Id LIMIT 1];
                    
    //                //System.debug('opp.Revenue_Type__c : ' +opp.Revenue_Type__c);
    //                //System.debug('0pp.olis count : ' + olis.size());

    //                // System.Assert(opp.Revenue_Type__c == 'Direct QSG');
    //                // System.Assert(olis.size() == 0);
    //                //Call ChangeRevenueType to change the RevenueType to the same value - Direct QSG
    //            }//end if rt
    //        }//RunAs
    //    }//Try  
    //    catch(Exception ex)
    //    {
    //        System.Debug('Issue saving opp, soi or olis: ' + ex);
    //    }
        
    //}
    
    static testMethod void testOpportunityChangeRevenueType_NoLineItems(){
        //setupCustomSettings();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');

        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
         
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeRevenueType', objUser);
                
                RecordType rt = [SELECT Id, DeveloperName from RecordType where DeveloperName = 'Sales_QCCS'];
                
                if (rt != null){                    
                    //Create and insert account and opp record type Sales QCCS
                    Test.startTest(); 
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);                                        
                    Test.stopTest();                  
                    
                    opp = [SELECT Revenue_Type__c from Opportunity where Id= :opp.Id LIMIT 1];
                    
                //    System.Assert(opp.Revenue_Type__c == 'Direct');
                    
                    //Call ChangeRevenueType to change the RevenueType to Direct
                    OpportunityChangeRevenueType.ChangeRevenueType(opp.Id, 'Direct QSG', null,null,null,null);
                    
                    //Get opp atributes to check if method set the correct values, removed all OLIs and Quotes.
                    opp = [SELECT Revenue_Type__c from Opportunity where Id= :opp.Id LIMIT 1];
                    List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where OpportunityId = :opp.Id LIMIT 1];
                    
                    System.debug('opp.Revenue_Type__c : ' +opp.Revenue_Type__c);
                    System.debug('0pp.olis count : ' + olis.size());

                 //   System.Assert(opp.Revenue_Type__c == 'Direct QSG');
                //    System.Assert(olis.size() == 0);
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);
        }
                          
    }
    
    static testMethod void testOpportunityChangeRevenueType_NoQuote(){
        //setupCustomSettings();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');

        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
                            
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeRevenueType', objUser);
                
                RecordType rt = [SELECT Id, DeveloperName from RecordType where DeveloperName = 'Sales_QCCS'];
                
                if (rt != null){                    
                    //Create and insert account and opp record type Qlikbuy II
                    Test.startTest();
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                    Test.stopTest();                     
                    //opp.License_Forecast_Amount__c = 10000;
                    update opp;
                                      
                    opp = [SELECT Revenue_Type__c from Opportunity where Id= :opp.Id LIMIT 1];
                    
                  //  System.Assert(opp.Revenue_Type__c == 'Direct');
                    
                    //Call ChangeRevenueType to change the RevenueType to Direct
                    OpportunityChangeRevenueType.ChangeRevenueType(opp.Id, 'Direct QSG', null,null,null,null);
                    
                    //Get opp atributes to check if method set the correct values, removed all OLIs and Quotes.
                    opp = [SELECT Revenue_Type__c from Opportunity where Id= :opp.Id LIMIT 1];
                    List<OpportunityLineItem> olis = [SELECT Id from OpportunityLineItem where OpportunityId = :opp.Id LIMIT 1];
                    
                    System.debug('opp.Revenue_Type__c : ' +opp.Revenue_Type__c);
                    System.debug('0pp.olis count : ' + olis.size());

                //    System.Assert(opp.Revenue_Type__c == 'Direct QSG');
                //    System.Assert(olis.size() == 0);
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);   
        }
                           
    }
    
  
    static testMethod void testSetOppChangeRevenueTypeByBoomi_MockOpportunity(){
        //setupCustomSettings();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');

        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();
        Pricebook2 pb2NS = [select Id from Pricebook2 where Name = 'NS Pricebook'];
        //try
        //{
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeRevenueType', objUser);
            
                RecordType rt = [SELECT Id, DeveloperName from RecordType where DeveloperName = 'Sales_QCCS'];
            
                //if (rt != null){
                    //Create and insert account and opp record type Sales_QCCS
                    Test.startTest();
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                    Test.stopTest();
					opp.INT_NetSuite_InternalID__c = '11';
                    opp.PriceBook2Id = pb2NS.Id;
                    update opp;
                                  
                    //Add license product
                    OpportunityLineItem oli = QTTestUtils.createSmallBusinessServerOppLineItem(opp, true, true);
                                    
                    //Call SetOppRevenueTypeByBoomi to change the RevenueType to Direct QSG
                    Test.setMock(HttpCalloutMock.class, new MockBoomiUtilsResponse());
                    String message = OpportunityChangeRevenueType.SetOppRevenueTypeByBoomi(opp.Id, 'Direct QSG');   
                    String message2 = OpportunityChangeRevenueType.SetOppRevenueTypeByBoomi(opp.Id, 'Direct');                   
               //     System.AssertEquals(message, '');                    
                    
                //}//end if rt
            }//RunAs
        //}//Try  
        //catch(Exception ex)
        //{
        //   System.Debug('Issue saving opp, soi or olis: ' + ex);   
        //}
        
    }

    static testMethod void testSetOppChangeRevenueTypeByBoomi_MockOpportunity_MissingNetsuiteIntID(){
        //setupCustomSettings();
        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');

        User objUser = QTTestUtils.createMockOperationsAdministrator();
        Opportunity opp;
        Sphere_Of_Influence__c soi;
        List<sObject> toInsert = new List<sObject>();               
        
        try
        {
            System.RunAs(objUser)
            {       
                Account acc = QTTestUtils.createMockAccount('TestQlikbuyIIOppToChangeCurr', objUser);
                
                RecordType rt = [SELECT Id, DeveloperName from RecordType where DeveloperName = 'Sales_QCCS'];
                
                if (rt != null){
                    //Create and insert account and opp record type Qlikbuy II
                    Test.startTest();
						opp = QTTestUtils.createMockOpportunity('sShortDescription', 'sName', 'Existing Customer', 'Direct', 'Goal Identify', ''+rt.Id, 'GBP', objUser, true);
                    Test.stopTest();                   
                    //Add license product
                    OpportunityLineItem oli = QTTestUtils.createSmallBusinessServerOppLineItem(opp, true, false);
                    
                    //Call SetOppRevenueTypeByBoomi to change the ChangeRevenueType to Direct CTW
                    String message = OpportunityChangeRevenueType.SetOppRevenueTypeByBoomi(opp.Id, 'Direct QSG');
                    
                //    System.Assert(message == '');                    
                    
                }//end if rt
            }//RunAs
        }//Try  
        catch(Exception ex)
        {
            System.Debug('Issue saving opp, soi or olis: ' + ex);   
        }
        
     }

    
}