/*
----------------------------------------------------------------------------
|  Class: EmailQonciergeServiceRequestToCase
|
|  Author: Peter Friberg, Fluido Sweden AB
|
|  Purpose: Inbound email service - QonciergeToCase
|
|  Description:
|    Class controlling the creation of a case when an email is sent from
|    the Qoncierge Webform.
|
| Change Log:
| 2013-04-16  Fluido(PetFri)  Initial Development
| 2013-06-14  Fluido(PetFri)  Added mail excepton handling and new support
|                             for new email from web form.
| 2013-07-01  Fluido(PetFri)  Removed debug email copying.
|                             New file header.
| 2013-08-12 MTM              Format Email Body to handle HTML format
| 2013-08-21 MTM              CR# 9273 Qoncierge Service Request to Case - fix for reported issues
| 2013-11-04 CCE              CR# 9928 Added "AND Left_Company__c = false" to contacts select as
|							  Cases shouldn't be linked to a Contact who is flagged as "Left Company".
| 2014-04-29 MTM			  CR# 12221 Change file extension on Qoncierge Webform Case attachment
| 2017-10-04 BAD              CHG0030362 - Rename Qoncierge to Customer Support
----------------------------------------------------------------------------
*/

global class EmailQonciergeServiceRequestToCase implements Messaging.InboundEmailHandler {

    /*
    |----------------------------------------------------------
    |  Method: handleInboundEmail
    |
    |  Description:
    |      Handles the incoming email from Qoncierge web form.
    |
    |  Input parameters:
    |      email     - The email contents (standard parameter)
    |      envelope  - The email envelope (standard parameter)
    |
    |  Returns:
    |      result    - Always successful
    |----------------------------------------------------------
    */

    global Messaging.InboundEmailResult handleInboundEmail(
        Messaging.InboundEmail email, 
        Messaging.InboundEnvelope envelope) {
        
        // Create positive result
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        result.success = true;
        
        string emailBody = getEmailBody(email);
        // Create a map of fields and values from email body
        map<String,String> valueMap = createMapFromBody(emailBody);
        
        // Try to find contact based on work email address
        String theEmail = valueMap.get('Work E-Mail');
        system.debug('GOT WORK EMAIL: "' + theEmail + '"');
                
        // Get user id to set as owner for the contact
        User owner =
            [SELECT Id, Name FROM User
             WHERE Name = 'Qliktech Webservice'];
        
        // Get contact having Work E-mail as email address if any
        list<Contact> contacts = [SELECT Id, Name, Email, AccountId FROM Contact
                                  WHERE Email = :theEmail AND Left_Company__c = false];	//CR# 9928

        // Variables to hold the Ids
        Id theContactId = null;
        Id theAccountId = null;
        Id theCaseOwnerId = null;
        
        // Set case owner and id from queue
        Group caseOwner = [SELECT Id, Name FROM Group
                           WHERE (type = 'Queue') AND
                                 (DeveloperName = 'Qoncierge_Service')][0];
        theCaseOwnerId = caseOwner.Id;
        
        // Variable to hold all info from the web form
        string allInfo = ''; 
        
        // ###
        // ### No contacts found!
        // ###
        if (contacts == null || contacts.size() == 0) {

            // Get temp account
            Account tempAcc = [SELECT Id FROM Account WHERE Name LIKE 'Temp Qoncierge'][0];
            
            // Get record type Id
            Id recTypeId = [SELECT Id, SobjectType, Name FROM RecordType
                            WHERE Name = 'Business Contact' AND
                            SobjectType ='Contact'
                            LIMIT 1].Id;
            
            String cName = valueMap.get('Company');
            String qName = valueMap.get('QlikView Account User Name');

            // Create new contact with values from email
            Contact newCon = new Contact(
                RecordTypeId = recTypeId,
                OwnerId = owner.Id,
                AccountId = tempAcc.Id,
                FirstName = valueMap.get('First Name'),
                LastName = valueMap.get('Last Name'),
                Email = valueMap.get('Work E-Mail'),
                Phone = valueMap.get('Phone Number incl. Country Code'),
                MailingCountry = valueMap.get('Country'),
                LeadSource = 'SUP - Support Request',
                Description =
                    'Customer Support Web Form Contact\r\n'+
                    'Company: ' + ((cName == null) ? '' : cName) +'\r\n'+
                    'Qlikview.com user name: ' + ((qName == null) ? '' : qName)
            );
            
            // Save new contact to data base
            insert newCon;
            
            // Get the contact and account ids
            theContactId = newCon.Id;
            theAccountId = tempAcc.Id;
        }
        
        // ###
        // ### One single contact found
        // ###
        else if (contacts.size() == 1) {
            
            // Get the contact and account ids
            theContactId = contacts[0].Id;
            theAccountId = contacts[0].AccountId;
        }
        
        // ###
        // ### Multiple contacts found
        // ###
        else if (contacts.size() > 1) {

            // Get temp account
            Account tempAcc = [SELECT Id FROM Account WHERE Name LIKE 'Temp Qoncierge'][0];
            // Get record type Id
            Id recTypeId = [SELECT Id, SobjectType, Name FROM RecordType
                            WHERE Name = 'Business Contact' AND
                            SobjectType ='Contact'
                            LIMIT 1].Id;
                            
            String cName = valueMap.get('Company');
            String qName = valueMap.get('QlikView Account User Name');

            // Create new contact with values from email
            Contact newCon = new Contact(
                RecordTypeId = recTypeId,
                OwnerId = owner.Id,
                AccountId = tempAcc.Id,
                FirstName = valueMap.get('First Name'),
                LastName = valueMap.get('Last Name'),
                Email = valueMap.get('Work E-Mail'),
                Phone = valueMap.get('Phone Number incl. Country Code'),
                MailingCountry = valueMap.get('Country'),
                LeadSource = 'SUP - Support Request',
                Description =
                    'Customer Support Web Form Contact\r\n'+
                    'Company: ' + ((cName == null) ? '' : cName) +'\r\n'+
                    'Qlikview.com user name: ' + ((qName == null) ? '' : qName)
            );
            
            // Save new contact to data base
            insert newCon;
            
            // Get the contact and account ids
            theContactId = newCon.Id;
            theAccountId = tempAcc.Id;
            
            // Add all info from web form for QlikView to analyse
            allInfo = allInfo + '\r\n\r\nMultiple contacts found! Details from Customer Support email below:';
            for (string key : valueMap.keySet()) {
                allInfo = allInfo + '\r\n' + key + ': ' + valueMap.get(key);
            }
        }
        
        // Set record type to "QlikTech_Master_Support_Record_Type"
        Id recTypeId = [SELECT Id, SobjectType, Name FROM RecordType
                        WHERE Name = 'QlikTech Master Support Record Type' AND
                              SobjectType ='Case'
                        LIMIT 1].Id;
        
        // ###
        // ### Create new case and set fields
        // ###
        Case newCase = new Case(
            RecordTypeId = recTypeId,
            AccountId = theAccountId,
            ContactId = theContactId,
            OwnerId = theCaseOwnerId,
            Severity__c = '3',
            Type = 'Service Request',
            Subject = 'Customer Support - ' + valueMap.get('Topic'),
            Status = 'New',
            Origin = 'Qoncierge Webform',
            License_No__c = valueMap.get('License Number'),
            Service_Request_Type__c = valueMap.get('Topic'),
            Description = valueMap.get('Description') + allInfo);
        
        // Write the case to the database and get case number back
        insert newCase;
        
        // Create a list for the attachments
        list<Attachment> attachments = new list<Attachment>();
        system.debug('Email body = ' +  emailBody);
        // Add the original email as an html attachment
        attachments.add(new Attachment(
            ParentId = newCase.Id,
            Name = 'Webform: ' + email.subject + '.txt',
            ContentType = 'text/plain',
            Body = Blob.valueOf(emailBody)));

        // Create binary attachments if any
        if (email.binaryAttachments != null) {

            for (Integer i = 0; i < email.binaryAttachments.size(); i++) {
                attachments.add(
                    new Attachment(
                        ParentId = newCase.Id,
                        Name = email.binaryAttachments[i].filename,
                        Body = email.binaryAttachments[i].body));
            }
        }

        // Create text attachments if any
        if (email.textAttachments != null) {

            for (Integer i = 0; i < email.textAttachments.size(); i++) {
                attachments.add(
                    new Attachment(
                        ParentId = newCase.Id,
                        Name = email.textAttachments[i].filename,
                        Body = Blob.valueOf(email.textAttachments[i].body)));
            }
        }
        
        // Add attachments to case if any
        if (attachments.size() > 0) {
            insert attachments;
        }
        
        // Get email template, make this a CUSTOM SETTING!!!
        EmailTemplate et =
            [SELECT id FROM EmailTemplate
             WHERE DeveloperName='Qoncierge_Webform_Received'];  // Qoncierge_Support_Request_Confirmation

        
        // ###
        // ### Get org-wide email address for Qoncierge support from custom settings
        // ###
        String theSender = 'NOT_ENABLED';
        Qoncierge_Support_Email__c qSender = Qoncierge_Support_Email__c.getInstance('Valid');
        if (qSender != null) {
           theSender = qSender.email__c;
        }
        OrgWideEmailAddress[] owea =
            [SELECT Id FROM OrgWideEmailAddress
             WHERE Address = :theSender];
        
        // Create outbound email with Case Id and Contact
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        // String[] toAddresses = new String[] {valueMap.get('Work E-Mail')};
        // mail.setToAddresses(toAddresses);  // WHEN NOT USING ORG-WIDE sender email
        
        // Prepare the email
        mail.setTargetObjectId(theContactId);
        mail.setWhatId(newCase.Id);
        mail.setSaveAsActivity(false);
        mail.setReplyTo('customersupport@qlik.com');
        // mail.setSubject('QlikView Support Request - Confirmation');  // WHEN NOT USING ORG-WIDE sender email
        mail.setUseSignature(false);
        mail.setTemplateId(et.id);
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea[0].Id);
        }
        else {
            mail.setSenderDisplayName('Qlik Customer Support');
        }
        
        try {
            // Send the email as user 'Qliktech Webservice'
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
        catch (system.EmailException ex) {
            // If the sending of the email fails, due to non existing
            // email address, do nothing
        }
        
        // Return result
        return result;
    }
     /*
    |----------------------------------------------------------
    |  Method: getEmailBody
    |
    |  Description:
    |      If Plain Text body is available use it as it is
    |      If not get html body and strip off HTTML tags
    |  Input parameters:
    |      Messaging.InboundEmail - 
    |
    |  Returns:
    |      String  The formatted E-mail bodyText
    |              
    |----------------------------------------------------------
    */
    private static String getEmailBody(Messaging.InboundEmail email)
    {
        System.Debug('Parsing plainText from email');
        
        String bodyText = email.plainTextBody;
        
        if (bodyText == null)
        {
            System.Debug('PlainText part not available. Parsing  HTML body from email');
            bodyText = email.htmlBody;
            System.Debug('Original Message' + bodyText);        
            String result = bodyText.replaceAll('</p>', '\n');
            String HTML_TAG_PATERN = '<.*?>';
            
            pattern patt = pattern.compile(HTML_TAG_PATERN);
            
            matcher match = patt.matcher(result);
            bodyText = match.replaceAll('');
            System.Debug('After replace Message' + bodyText);
            bodyText = bodyText.unescapeHtml4();
            System.Debug('Message after unescapeHtml4' + bodyText);
        }
        return bodyText;
    }
    /*
    |----------------------------------------------------------
    |  Method: createMapFromBody
    |
    |  Description:
    |      Find keywords (headings) and values in the supplied
    |      parameter body. Return a map with keywords and
    |      corresponding valuess.
    |
    |  Input parameters:
    |      body    String - The string containing all keywords
    |                       and values created by Qoncierge
    |                       form sent as email.
    |
    |  Returns:
    |      Map<String,String>  The map of all found keywords
    |                          and corresponding values
    |----------------------------------------------------------
    */
        
    private static Map<String,String> createMapFromBody(String body) {
        
        // Create and populate a set of keywords
        // used in the Qoncierge support request email
        Set<String> keywords = new Set<String>{
            'First Name',
            'Last Name',
            'Company',
            'Work E-Mail',
            'Phone Number incl. Country Code',
            'QlikView Account User Name',
            'License Number',
            'Topic',
            'Description',
            'Attachment',
            'Country' };
        
        // Create map of key/value pairs
        map<String,String> valueMap = new map<String,String>();        
        String bodyCopy = body;
        // Split body into lines        
        list<String> lines = bodyCopy.split('\n',0);

        // Temp variables to hold key/value pairs
        String index = '';
        String value = '';

        // Parse all lines in the body
        for (String line : lines) {
            
            // Save a raw copy of the line
            String lineCopy = line;
            system.debug('LINE "' + lineCopy + '"');
            
            // Check for starting and ending '*'
            if (line.startsWith('*') && line.endsWith('*')) {
                // Remove trailing ***
                while (lineCopy.length() > 0 && lineCopy.endsWith('*')) {
                    lineCopy = lineCopy.SubstringBeforeLast('*');
                }
                // Remove prepending ***
                while (lineCopy.length() > 0 && lineCopy.startsWith('*')) {
                    lineCopy = lineCopy.SubstringAfter('*');
                }
            }
            if (keywords.contains(lineCopy.normalizeSpace())) {
                
                // Save last detected key/value pair
                if (index != '' && value != '') {
                    valueMap.put(index, value);
                    system.debug('====> SAVING { ' + index + ' => ' + value + '}');
                    index = '';
                    value = '';
                }
                
                // Get new keyword
                //index = line.substringBetween('*');
                index = lineCopy.normalizeSpace();
            }
            
            // Check for value, do not add blank lines to value
            else if (index != '' && line.normalizeSpace().length() > 0) {
                
                // Filter out email address
                if (index == 'Work E-Mail') {
                    // Get email address between < >
                    value = lineCopy.substringAfter('<').substringBefore('>');
                    
                    system.debug('LINE2 "' + lineCopy + '"');
                        
                    // If no email address found between < >
                    // expect a string ending with multiple *
                    if (value.length() == 0) {
                        
                        // Remove trailing ***
                        while (lineCopy.length() > 0 && lineCopy.endsWith('*')) {
                            lineCopy = lineCopy.SubstringBeforeLast('*');
                        }
                    }
                    // Email address found between < >
                    else {
                        // Remove mailto: if present
                        if (value.contains(':')) {
                        
                            // Get mail address after mailto:
                            lineCopy = value.substringAfterLast(':');
                            system.debug('LINE3 "' + lineCopy + '"');
                        }
                    }
                    
                    // Set value
                    value = lineCopy;
                }
                // Append line to value (may be multi line value)
                else {
                    while (lineCopy.length() > 0 && lineCopy.endsWith('*')) {
                        lineCopy = lineCopy.SubstringBeforeLast('*');
                    }
                    value = value + '\r\n' + lineCopy;
                }
            }
        }
        
        // Save last found key/value pair
        if (index != '' && value != '') {
            valueMap.put(index, value);
        }
        
        // Return newly created key/value map
        return valueMap;
    }
}