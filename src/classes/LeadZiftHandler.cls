/******************************************************

    Class: LeadZiftHandler
    
    Initiator: Björn Andersson
    
    Changelog:
        2017-06-20  BAD            	Handler class for LeadZift trigger
        						   	When Zift Sales Distribution is set to "Accepted", Lead Owner must be updated to a Partner. 
               					   	The field Zift Champion is the partner that needs to become the Lead owner
               					   	CHG0031645
******************************************************/
public class LeadZiftHandler {

	public LeadZiftHandler() {
	}

	public static void handle(List<Lead> triggerNew, List<Lead> triggerOld) {
		System.debug('LeadZiftHandler: Starting');


        Set<Id> contactIDs = new Set<Id>();
        for (Integer i = 0; i < triggerNew.size(); i++)
        {
        	if(triggerOld[i].Zift_Distribution_Status__c != triggerNew[i].Zift_Distribution_Status__c && triggerNew[i].Zift_Distribution_Status__c == 'Accepted' && triggerNew[i].Zift_Champion__c != null)
            	contactIDs.add(triggerNew[i].Zift_Champion__c);
        }

        if(contactIDs.size() > 0)
        {
			System.debug('BAD contact found continue with change');
	        //fetch the Portal UserID for the Zift Champion, we need that for setting OwnerID
			Map<Id, Id> MapContactToUser = new Map<Id, Id>();
	        for(User user : [select ContactId, Id from User where ContactId in :contactIDs])
	        {
	        	MapContactToUser.put(user.ContactID,user.Id);
	        }		        

	        //Check if Zift distribution status equals "Accepted", if so we can update the Lead owner
	        for(integer i=0; i < triggerNew.size(); i++)
	        {
	            if(triggerOld[i].Zift_Distribution_Status__c != triggerNew[i].Zift_Distribution_Status__c && triggerNew[i].Zift_Distribution_Status__c == 'Accepted' && triggerNew[i].Zift_Champion__c != null) 
	            {
            		Id userID = MapContactToUser.get(triggerNew[i].Zift_Champion__c);

					if(userID != null)
					{
						triggerNew[i].OwnerID = userID;	
						System.debug('BAD set OwnerID to ' + userID);
					}                    
	            }
	        }		
    	}

	}

}