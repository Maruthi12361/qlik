/*
    Created: 2019-01-02
    Created By: Linus Löfberg

    Change log:
        2019--01-02 - Class created for BSL-1293
        2019-01-02 - AIN - Fixed errors related to new duplicate rule
*/
@isTest
private class ZuoraSubscrptionsRelatedListCTRLEXT_TEST {

    @testSetup
    static void testSetup() {
        QuoteTestHelper.createCustomSettings();
    }

    @isTest
    static void test_load_relatedlist() {
        Test.startTest();

        Id partnerProfileId = [SELECT Id FROM Profile WHERE Name='PRM - Independent Territory + QlikBuy'].Id;

        Account endUserAccount = Z_TestFactory.makeAccount('Sweden', 'Test Account 259', '','65000-480');
        endUserAccount.Pending_Validation__c =FALSE;
        insert endUserAccount;

        Account partner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        insert partner;

        Contact endUserContact = Z_TestFactory.makeContact(endUserAccount);
        insert endUserContact;

        Contact partnerContact = Z_TestFactory.makeContact(partner);
        partnerContact.FirstName = 'AVERYUNIQUEFIRSTNAMEASDASERASR';
        partnerContact.LastName = 'AVERYUNIQUELASTNNAMEASDADAQRWEAWR';
        partnerContact.Email = 'asdriusdfkjvhslkdfvjsklf@aslcdkhjavkjhl.com';
        insert partnerContact;

        Account secondPartner = Z_TestFactory.makePartnerAccount('United States', 'Test Account 229', '','65000-480');
        insert secondPartner;

        Partner_Category_Status__c pcs = Z_TestFactory.createSpecificPCS(partner.Id, 'Distributor', 'Distributor', 'Distributor');
        insert pcs;

        Partner_Category_Status__c pcs2 = Z_TestFactory.createSpecificPCS(secondPartner.Id, 'QPP', 'Elite', 'Resell');
        insert pcs2;

        User user = new User(alias = 'test123', email='test123@noemail.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = partnerProfileId, country='United States',IsActive =true,
        ContactId = partnerContact.Id,
        timezonesidkey='America/Los_Angeles', username='testerpartnersupreme@noemail.com');
        insert user;

        Opportunity testOpp = Z_TestFactory.makeOpportunity(endUserAccount, endUserContact);
        testOpp.Sell_Through_Partner__c = partner.Id;
        testOpp.Partner_Contact__c = partnerContact.Id;
        testOpp.Second_Partner__c = secondPartner.Id;
        testOpp.Revenue_Type__c = 'Distributor';
        insert testOpp;

        zqu__Quote__c zquote = Z_TestFactory.makeQuote2(testOpp, endUserAccount);
        zquote.zqu__BillToContact__c = endUserContact.id;
        zquote.zqu__SoldToContact__c = endUserContact.id;
        insert zquote;

        zquote = [SELECT Id, zqu__Number__c FROM zqu__Quote__c WHERE Id = :zquote.Id];
        
        Zuora__CustomerAccount__c customerAcc = new Zuora__CustomerAccount__c(Zuora__Account__c = partner.Id, name='Test Account');
        insert customerAcc;
        
        Zuora__Subscription__c subscription = new Zuora__Subscription__c(Zuora__Account__c = endUserAccount.Id, 
        Zuora__QuoteNumber__c = zquote.zqu__Number__c, Zuora__InvoiceOwner__c = customerAcc.Id);
        insert subscription;

        Test.stopTest();

        System.runAs(user) {
            ApexPages.StandardController stdAccount = new ApexPages.StandardController(endUserAccount);
            ZuoraSubscrptionsRelatedListCTRLEXT controller = new ZuoraSubscrptionsRelatedListCTRLEXT(stdAccount);
            System.assertEquals(1, controller.subscriptionRecords.size());
        }
    }
}