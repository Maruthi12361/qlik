/**************************************************
* Author: Alberto De Diego from Fluido Oy
*
* Change Log:
* 2012-09-19 Alberto de Diego: CR# 5107
*        Initial Development
* 2014-10-06 CCE CR# 17120 rename ServiceOperations and add ConsultingServiceOperations
*
**************************************************/
@isTest
private class CaseSalesOpsTest {	
	static testmethod void caseSalesOpsTriggerTest() {
        Test.StartTest();
		List<RecordType> types = [SELECT Id FROM RecordType WHERE Name = 'Sales Ops Requests'];
		
		if (types.size() > 0) {
			//no user or contact exists with the same email address as the incoming case -> a contact is created with the email info
			String testEmail = 'test1@test1.com';            
			Case c = new Case(RecordTypeId = types[0].Id, SuppliedEmail = testEmail);
			insert c;
			
			List<Contact> cs = [SELECT Id FROM Contact WHERE Email = :testEmail];
			System.assertEquals(1, cs.size());			     
			Test.StopTest();
        }
    }
    
  //  static testmethod void caseServiceOperationsTest() {
  //      Test.StartTest();
		//List<RecordType> types = [SELECT Id FROM RecordType WHERE Name = 'ServiceOperations'];
		
		//if (types.size() > 0) {
		//	//no user or contact exists with the same email address as the incoming case -> a contact is created with the email info
		//	String testEmail = 'test123@test1.com';            
		//	Case c = new Case(RecordTypeId = types[0].Id, SuppliedEmail = testEmail);
		//	insert c;
			
		//	List<Contact> cs = [SELECT Id FROM Contact WHERE Email = :testEmail];
		//	System.assertEquals(1, cs.size());
  //          Test.StopTest();
  //      }
  //  }
    static testmethod void caseEducationServicesOperationsTest() {
        Test.StartTest();
		List<RecordType> types = [SELECT Id FROM RecordType WHERE Name = 'EducationServicesOperations'];
		
		if (types.size() > 0) {
			//no user or contact exists with the same email address as the incoming case -> a contact is created with the email info
			String testEmail = 'test123@test1.com';            
			Case c = new Case(RecordTypeId = types[0].Id, SuppliedEmail = testEmail);
			insert c;
			
			List<Contact> cs = [SELECT Id FROM Contact WHERE Email = :testEmail];
			System.assertEquals(1, cs.size());
            Test.StopTest();
        }
    }
    
    static testmethod void caseConsultingServiceOperationsTest() {
        Test.StartTest();
		List<RecordType> types = [SELECT Id FROM RecordType WHERE Name = 'ConsultingServiceOperations'];
		
		if (types.size() > 0) {
			//no user or contact exists with the same email address as the incoming case -> a contact is created with the email info
			String testEmail = 'test123@test1.com';            
			Case c = new Case(RecordTypeId = types[0].Id, SuppliedEmail = testEmail);
			insert c;
			
			List<Contact> cs = [SELECT Id FROM Contact WHERE Email = :testEmail];
			System.assertEquals(1, cs.size());
            Test.StopTest();
        }
    }
    
    static testmethod void caseSalesOpsTriggerSameUserTest() { 
        Test.StartTest();
        List<RecordType> types = [SELECT Id FROM RecordType WHERE Name = 'Sales Ops Requests'];
		
		if (types.size() > 0) {            	
            //a user exists with the same email address as the incoming case -> a contact is created with the same info as the user
			String testEmail2 = 'test2@test2234.com';
			createUser(testEmail2, 'someNick');
			Case c2 = new Case(RecordTypeId = types[0].Id, SuppliedEmail = testEmail2);
			insert c2;
			
			List<Contact> cs2 = [SELECT Id FROM Contact WHERE Email = :testEmail2];
			System.assertEquals(1, cs2.size());
            Test.StopTest();
		}
	}
	
	private static User createUser(String username, String nick) {
		Profile profile = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
    	User user = new User(Username = username, LastName = 'Test Lastname', Email = username, 
    						 Alias = 'tula', CommunityNickname = nick, TimeZoneSidKey = 'Europe/Paris', 
    						 LocaleSidKey = 'es_ES', EmailEncodingKey = 'ISO-8859-1', ProfileId = profile.Id, LanguageLocaleKey = 'en_US'); 
    	insert user;
    	return user;
	}
	
}