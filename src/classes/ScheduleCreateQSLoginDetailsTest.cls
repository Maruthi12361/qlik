/********************************************************
* CLASS: ScheduleCreateQSLoginDetailsTest
* DESCRIPTION: Test class for class ScheduleCreateQSLoginDetails
*
* CHANGELOG:    
*	2018-11-15 - BAD - Added Initial logic
*********************************************************/
@isTest
private class ScheduleCreateQSLoginDetailsTest
{
	@isTest
	static void test_ScheduleQSLoginDetails()
	{
		User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();

		List<Web_Activity__c> lstWA = new List<Web_Activity__c>();

        Lead lead = new Lead(
	        LastName = 'LastName',
	        Company = 'Company', 
	        IsUnreadByOwner = True,
	        Country='Sweden',
	        Email='BADTestLead@test.com.sandbox',
	        Phone='3333'
        	);
        insert lead;
        
        ULC_Details__c ulc = new ULC_Details__c(
            LeadId__c = lead.Id,
            ULCName__c = 'qtestbad',
            ULCStatus__c = 'Active'
        	);
        insert ulc;

        Web_Activity__c wa = new Web_Activity__c(
			Name = 'waBAD1',
			Sender__c = 'QlikID',
			Type__c = 'QSD_FirstLogin',
			Status__c = 'Pending',
			Content__c = '{"TimeStamp":"2018-10-04T11:11:11.916Z","ULCUsername":"qtestbad"}'
        	);
        insert wa;
        lstWA.add(wa);

        Test.startTest();	        
		
		ScheduleCreateQSLoginDetails sh1 = new ScheduleCreateQSLoginDetails('QSD_FirstLogin');
		String sch = '0 0 23 * * ?'; 
		system.schedule('Test QS Schedule', sch, sh1); 
		
        Test.stopTest();
	}
}