/********
* NAME : QuoteCreationValidationTests
* Description: Test class to validate the QuoteCreationValidation
*
*
*Change Log:
*   MAW 2015-02-17 Class creation
2016-06-08    Andrew Lokotosh Commented Forecast_Amount fields line  178
06.02.2017  RVA :   changing CreateAcounts methods
16.03.2017 : Rodion Vakulovskyi : changes for Tests Errors
22.03.2017 : Rodion Vakulovskyi : changes for Validation failures
23.03.2017 : Rodion Vakulvsokyi
02.09.2017 : Srinivasan PR- fix for query error
22.12.2017 : Pramod Kumar V-Test class coverage
13.02.2019 : BAD fixing test class due to Duplicate rule added

******/
@isTest
public with sharing class QuoteCreationValidationTests {

    @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
    }

    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';

    // /* pramod
    static testMethod void testQuoteCreation_NullParameters() {
        Test.startTest();
        String message = QuoteCreationValidation.Validate(null);

        System.assert(message == '');
        Test.stopTest();
    }

    static testMethod void testQuoteCreation_DirectOpportunity(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        User objUser= GetMockUser();
        System.RunAs(objUser)
        {
            Opportunity opp = GetFullyLoadedOpp(objUser, 'Direct');
            String retMessage = QuoteCreationValidation.Validate(opp.Id);
            System.debug('Opportunity return message : ' + retMessage);
            // System.Assert(retMessage == 'True');
        }

        Test.stopTest();
    }

    static testMethod void testQuoteCreation_RessellerOpportunity(){
        QTTestUtils.GlobalSetUp();
        Semaphores.SetAllSemaphoresToTrue();
        User objUser= GetMockUser();

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', thisUser, true);
        testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
        update testAccount;
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c];
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
        where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
        insert  testPartnerAccount;
        Contact testContact = QuoteTestHelper.createContact(testPartnerAccount.id);
        insert testContact;


        System.RunAs(objUser){

            Opportunity opp = GetFullyLoadedOpp(objUser, 'Direct');
            opp.Revenue_Type__c = 'Fulfillment';
            opp.Sell_Through_Partner__c = testPartnerAccount.id;
            opp.Fulfillment_Margin__c = 10;
            opp.Partner_Contact__c = testContact.id;

            update opp;
            String retMessage = QuoteCreationValidation.Validate(opp.Id);

            update testPartnerAccount;
            String retMessage2 = QuoteCreationValidation.Validate(opp.Id);
            System.debug('Opportunity return message : ' + retMessage);

            Semaphores.SetAllSemaphoresToFalse();

            Test.startTest();
            testPartnerAccount.Billing_Contact__c = null;
            testPartnerAccount.Billing_Country_Code__c = null;
            testPartnerAccount.INT_NetSuite_InternalID__c = null;
            update testPartnerAccount;

            Opportunity opp2 = GetFullyLoadedOpp(objUser, 'Direct');
            opp2.Revenue_Type__c = 'Fulfillment';
            opp2.Sell_Through_Partner__c = testPartnerAccount.id;
            opp2.Fulfillment_Margin__c = 10;
            opp2.Partner_Contact__c = testContact.id;
            update opp2;
            String retMessage4 = QuoteCreationValidation.Validate(opp2.Id);
            Test.stopTest();

            // System.Assert(retMessage == 'True');
        }


    }

    // pramod
    // Test is not present in QuoteCreationValidation
    // static testMethod void testQuoteCreation_DirectOpportunity_missingFirstApprover() {
    //     QTTestUtils.GlobalSetUp();
    //     Test.startTest();
    //
    //     User objUser= GetMockUser();
    //     System.RunAs(objUser) {
    //         Opportunity opp = GetFullyLoadedOpp(objUser, 'Direct');
    //         opp.First_Quote_Approver_Training__c = null;
    //         update opp;
    //
    //         String retMessage = QuoteCreationValidation.Validate(opp.Id);
    //         System.debug('Opportunity return message : ' + retMessage);
    //         System.Assert(retMessage != 'True');
    //     }
    //
    //     Test.stopTest();
    // }
    // pramod

    static testMethod void testQuoteCreation_ClosedOpportunity(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        User objUser= GetMockUser();
        System.RunAs(objUser)
        {
            Opportunity opp = GetFullyLoadedOpp(objUser, 'Direct');
            opp.StageName = 'Closed Won';
            //insert opp;
            //  System.Assert(QuoteCreationValidation.IsValidOpp(opp) != True);
        }

        Test.stopTest();
    }

    static testMethod void testQuoteCreation_DirectOpportunity_MissingRevType(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        User objUser= GetMockUser();
        System.RunAs(objUser)
        {
            Opportunity opp = GetFullyLoadedOpp(objUser, '');
            String retMessage = QuoteCreationValidation.Validate(opp.Id);
            System.debug('Opportunity return message : ' + retMessage);
            // System.Assert(retMessage != 'True');
        }

        Test.stopTest();
    }

    static testMethod void testQuoteCreation_DirectOpportunity_MissingSegment(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        User objUser= GetMockUser();
        System.RunAs(objUser)
        {
            Opportunity opp = GetFullyLoadedOpp(objUser, 'Direct');
            opp.Account.Segment_New__c = null;
            update opp.Account;
            String retMessage = QuoteCreationValidation.Validate(opp.Id);
            System.debug('Opportunity return message : ' + retMessage);
            // System.Assert(retMessage != 'True');
        }

        Test.stopTest();
    }

    static testMethod void testQuoteCreation_DirectOpportunity_MissingSOI(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        User objUser= GetMockUser();
        System.RunAs(objUser)
        {
            Opportunity opp = GetFullyLoadedOpp(objUser, 'Direct', false);
            String retMessage = QuoteCreationValidation.Validate(opp.Id);
            System.debug('Opportunity return message : ' + retMessage);
            //    System.Assert(retMessage != 'True');
        }

        Test.stopTest();
    }

    static User GetMockUser(){
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        objUser.NS_Quote_Approval_Role__c = 'PU';
        update objUser;
        return objUser;
    }

    static Opportunity GetFullyLoadedOpp(User objUser, String RevenueType){
        return GetFullyLoadedOpp( objUser, RevenueType, True);
    }
    public static integer NumCreated = 0;
    static Opportunity GetFullyLoadedOpp(User objUser, String RevenueType, Boolean addSOI){
        Opportunity opp;
        try
        {
            NumCreated++;
            Account acc = QTTestUtils.createMockAccount('testOpportunityQuoteCreation ' + '_'+System.now().millisecond() + '_' + NumCreated, objUser);

            Contact cntct = new Contact(AccountId=acc.Id,lastname='Contact' + '_'+System.now().millisecond() + '_' + NumCreated,firstname='Test ' + '_'+System.now().millisecond() + '_' + NumCreated, Email = 'maw' + '_'+System.now().millisecond() + '_' + NumCreated + '@gmail' + System.now().millisecond() + NumCreated +'.com');
            insert cntct;

            acc.Segment_New__c = 'Enterprise - Target';
            acc.Billing_Contact__c = cntct.Id;
            update acc;

            RecordType rt = [SELECT Id, Name from RecordType where DeveloperName like 'Sales_QCCS'];

            if (rt != null){
                //Create and insert account and opp record type Qlikbuy II
                opp = New Opportunity (
                Short_Description__c = 'sShortDescription',
                Name = 'sName',
                Type = 'Existing Customer',
                Revenue_Type__c = RevenueType,
                CloseDate = Date.today().addDays(5),//Adding closing date as today+ 5 to ensure test classes do not fail on overnight release
                StageName = 'Goal Identify',
                RecordTypeId = ''+rt.Id,
                CurrencyIsoCode = 'USD',
                AccountId = acc.Id,
                Account = acc,
                Customers_Business_Pain__c = 'First project scheduled',
                Signature_Type__c = 'Digital Signature',
                //Consultancy_Forecast_Amount__c = 1,

                First_Quote_Approver_License__c = objUser.Id,
                First_Quote_Approver_Service__c= objUser.Id,
                First_Quote_Approver_Training__c= objUser.Id,

                Owner = objUser
                );
                insert opp;

                System.debug('Opportunity  : ' + opp.Id);

                if(addSOI){
                    Sphere_Of_Influence__c soi = new Sphere_of_Influence__c();
                    soi.Opportunity__c = opp.Id;
                    soi.Contact__c = cntct.Id;
                    soi.Role__c = 'Beneficiary';
                    soi.Quote_Recipient__c = true;
                    insert soi;
                }
                System.debug('Opportunity Owner: '+ opp.Owner);

                return opp;

            }//end if rt
        }
        catch(Exception ex)
        {
            // System.Assert(false, 'Issue creating opp for validation: ' + ex);
        }
        return opp;
    }
    // pramod */

    static testMethod void TestOpportunity() {
        Test.startTest();

        QTTestUtils.GlobalSetUp();
        User objUser = QTTestUtils.createMockOperationsAdministrator();
        RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
        Semaphores.Opportunity_ManageForecastProductsHasRun_Before = true;
        Semaphores.Opportunity_ManageForecastProductsHasRun_After= true;
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales QCCS').getRecordTypeId();
        Id rtId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Partner Not For Resale').getRecordTypeId();
        Id rtId4 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End User Account').getRecordTypeId();
        Id rtId3 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();

        Subsidiary__c subs = QuoteTestHelper.createSubsidiary();
        insert subs;

        QlikTech_Company__c QTcomp = QuoteTestHelper.createQlickTechCompany(subs.id);
        insert QTcomp;

        RecordType recTypeAccEndUser = [SELECT Id, DeveloperName FROM Recordtype WHERE DeveloperName = 'End_User_Account' AND SobjectType = 'Account'];
        Account testAccount3 = new Account(name='Test3 Account3Name2', recordtypeid=rtId4,Territory_Country__c='France',Territory_street__c='test',Territory_city__c='test',Territory_Zip__c='2222');
        testAccount3.Territory_Country__c='France';
        testAccount3.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
        testAccount3.Billing_Country_Code__c = QTcomp.Id;
        insert testAccount3;

        Opportunity opp;

        Opportunity opp1 = new Opportunity (name='test',recordtypeid=rtId1,Account=testAccount3);
        opp1.CurrencyIsoCode = 'GBP';
        opp1.Short_Description__c = 'sShortDescription';
        opp1.Revenue_Type__c='Direct';
        opp1.StageName = 'Goal Identified';
        opp1.VoucherStatus__c = 'Vouchers Issue';
        opp1.Closed_date_for_conga__c =System.now();
        opp1.CloseDate=System.Today();
        opp1.Signature_Type__c=null;
        opp1.ForecastCategoryName='Pipeline';
        opp1.Signature_Type__c=null;
        opp1.Owner=null;
        insert opp1;

        RecordType recTypeAccPartner = [Select id, DeveloperName From Recordtype Where DeveloperName = 'Partner_Account' and SobjectType = 'Account'];
        Account testAccount = QuoteTestHelper.createAccount(QTcomp, recTypeAccPartner, 'Test AccountName');
        testAccount.Territory_Country__c = 'France';
        testAccount.Name = 'Test AccountName';
        insert testAccount;
        system.assertNotEquals(testAccount.id,null);

        Contact conPartner = QuoteTestHelper.createContact(testAccount.id);
        conPartner.Email='abc@qlik.com';
        insert conPartner;

        Opportunity opp2 = QuoteTestHelper.createOpportunity(testAccount3, testAccount.id, rt);
        opp2.Revenue_Type__c='Reseller';
        opp2.Partner_Contact__c=conPartner.id;
        opp2.ForecastCategoryName='Pipeline';
        insert opp2;
        system.assertNotEquals(opp2.id,null);

        Test.stopTest();

        QuoteCreationValidation.IsValidOpp(opp);
        QuoteCreationValidation.IsValidOpp(opp1);
        QuoteCreationValidation.IsValidOpp(opp2);
    }
}