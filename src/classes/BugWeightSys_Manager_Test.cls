/*
----------------------------------------------------------------------------
|  Class: BugWeightSys_Manager_Test
|
|  Filename: BugWeightSys_Manager_Test.cls
|
|  Author: Peter Friberg, Fluido Sweden AB
|
|  Description:
|    Class testing the functionality in class BugWeightSys_Manager
|
| Change Log:
| 2013-10-15  PetFri  Initial Development
| 2013-11-07  PetFri  Uses active Entitlement/SLA Process to find SLA level
| 2013-11-21  PetFri  Added test cases for schedulable and batchable classes
|                     Renamed TestFactory to BugWeightSys_TestFactory
| 2016-04-07  IRN     added to testmethod testBuildTestBugs and testbuildTestAccountLicenses
| 2018-01-24  ext_vos CHG0030671: delete the definition for Bugs__r.Number_of_cases__c field because it has default value. 
----------------------------------------------------------------------------
*/

@isTest
private class BugWeightSys_Manager_Test {

    //
    // Test to verify the retrieveBugs() and processBugs() methods
    //
    //UIN
    @testSetup static void setup() {
        System.runAs(new User(Id = Userinfo.getUserId())) 
        {
        //QuoteTestHelper.createCustomSettings();
            QTTestUtils.GlobalSetUp();
        }

    }
    
    private static testMethod void test_1() {
    
        // --- Set up mock data --- //
        List<Account> accounts = BugWeightSys_TestFactory.buildTestAccounts(10);
        INSERT accounts;
        
        List<Account_License__c> accLics = BugWeightSys_TestFactory.buildTestAccountLicenses(10, accounts);
        INSERT accLics;
        
        List<SlaProcess> slaProcesses = BugWeightSys_TestFactory.buildTestSlaProcesses();
        
        List<Entitlement> entitlements = BugWeightSys_TestFactory.buildTestEntitlements(10, accounts, accLics, slaProcesses);
        INSERT entitlements;

        // List<Contact> contacts = BugWeightSys_TestFactory.buildTestContacts(10, accounts);
        // INSERT contacts;
        
        BusinessHours bh = [Select Name, id From BusinessHours
                            Where Name = '24x7'];
                                            
        List<Bugs__c> bugs = BugWeightSys_TestFactory.buildTestBugs(10);
        for(integer i = 0; i < 5; i++) {
            bugs[i].Status__c = 'Closed';
        }
        INSERT bugs;
        
        List<Case> cases = BugWeightSys_TestFactory.buildTestCases(10, entitlements, bh, bugs);
        INSERT cases; 
        
        // --- End set up mock data --- //
      
        Test.startTest();
        
        // Instasiate the BugWeightSys_Manager and run the execute method.
        BugWeightSys_Manager bwm = new BugWeightSys_Manager();
        List<Bugs__c> executedBugs = bwm.retrieveBugs();

        // Process all bugs to update weights
        if (executedBugs.size() > 0 ) {
            bwm.processBugs(executedBugs);
        }

        // Stop test        
        Test.stopTest();
    
        // Get all bugs that are not closed
        List<Bugs__c> bugsRes = [ SELECT id,
                                   name,
                                   severity__c,
                                   status__c,
                                   bug_weight__c,
                                   number_of_cases__c,
                                   createddate,
                                (SELECT id,
                                        priority,
                                        severity__c,
                                        casenumber,
                                        designated_support_engineer__c,
                                        entitlementid,
                                        entitlement.slaprocess.name,
                                        entitlement.account_license__c,
                                        entitlement.account_license__r.name,
                                        entitlement.status,
                                        entitlement.session_cal__c,
                                        entitlement.named_user_cal__c,
                                        entitlement.document_cal__c,
                                        entitlement.usage_cal__c,
                                        entitlement.uncapped__c
                                 FROM Cases__r) 
                            FROM Bugs__c WHERE NOT(status__c LIKE 'Close%')] ;
                            
        // Verify the results  
        System.assertEquals(5, bugsRes.size()); 
    }
    
    //
    // Test to verify the calcTimeInQueue(Long ageInDays) method
    //
    private static testMethod void test_2() {
    
        // --- Set up mock data --- //
        List<Account> accounts = BugWeightSys_TestFactory.buildTestAccounts(2);
        INSERT accounts;
        
        List<Account_License__c> accLics = BugWeightSys_TestFactory.buildTestAccountLicenses(10, accounts);
        INSERT accLics;
        
        List<SlaProcess> slaProcesses = BugWeightSys_TestFactory.buildTestSlaProcesses();
        
        List<Entitlement> entitlements = BugWeightSys_TestFactory.buildTestEntitlements(10, accounts, accLics, slaProcesses);
        INSERT entitlements;
        
        List<Contact> contacts = BugWeightSys_TestFactory.buildTestContacts(10, accounts);
        INSERT contacts;
        
        BusinessHours bh = [Select Name, id From BusinessHours
                            Where Name = '24x7'];
                                            
        List<Bugs__c> bugs = BugWeightSys_TestFactory.buildTestBugs(10);
        INSERT bugs;
        
        List<Case> cases = BugWeightSys_TestFactory.buildTestCases(10, entitlements, bh, bugs);
        INSERT cases; 
        
        // --- End set up mock data --- //
      
        Test.startTest();
        
        // Instanciate the BugWeightSys_Manager and run the method.
        BugWeightSys_Manager bwm = new BugWeightSys_Manager();
        Integer weight4timeInQueue = bwm.calcTimeInQueue(10);
        
        Test.stopTest();
             
        // Verfiy the results  
        System.assertEquals(0, weight4timeInQueue);
    }
    
    //
    // Test to verify the calcBugWeight(Bugs__c bug) method
    //
    private static testMethod void test_3() {

        User dse = QTTestutils.createMockSystemAdministrator();
    
        /* --- Set up mock data --- */
        List<Account> accounts = BugWeightSys_TestFactory.buildTestAccounts(10);      
        accounts[1].QT_Designated_Support_Contact__c = dse.Id;
        INSERT accounts;
        
        List<Contact> contacts = BugWeightSys_TestFactory.buildTestContacts(10, accounts);
        contacts[1].DSE_Access__c = true;
        INSERT contacts;
        
        List<Account_License__c> accLics = new List<Account_License__c>();
        for(Integer i = 0; i < 3; i++) {
            Account_License__c accLic = new Account_License__c();
            accLic.account__c = i < accounts.size() ? accounts[i].id : accounts[0].id;
            accLic.name = 'AccLic_' + String.valueOf(i);
            accLic.session_cal__c = 1 + i;
            accLic.named_user_cal__c = 1 + i;
            accLic.document_cal__c = 1 + i;
            accLic.usage_cal__c = 1 + i;
            accLic.uncapped__c = math.mod(i,2) == 0 ? true : false;
            accLics.add(accLic); 
        }
        INSERT accLics;
        
        List<SlaProcess> slaProcesses = BugWeightSys_TestFactory.buildTestSlaProcesses();
        
        List<Entitlement> entitlements = BugWeightSys_TestFactory.buildTestEntitlements(10, accounts, accLics, slaProcesses);
        INSERT entitlements;
        
        BusinessHours bh = [Select Name, id From BusinessHours
                            Where Name = '24x7'];
                                            
        List<Bugs__c> bugs = new List<Bugs__c>();
        Bugs__c bug = new Bugs__c();
        bug.name = '1'; 
        bug.severity__c = '1';
        bug.status__c = 'Open';
        bug.bug_Id__c = '1';
        bug.currencyIsoCode = 'SEK';
        bug.service_class__c = '1';
        bugs.add(bug);
        
        bug = new Bugs__c();
        bug.name = '2'; 
        bug.severity__c = '2';
        bug.status__c = 'Open';
        bug.bug_Id__c = '2';
        bug.currencyIsoCode = 'SEK';
        bug.service_class__c = '2';
        bugs.add(bug);
          
        INSERT bugs;
        
        //List<Case> cases = BugWeightSys_TestFactory.buildTestCases(10, contacts, bh, bugs);
        List<Case> cases = new List<Case>();
        Case c = new Case();
        c.contactID = contacts[0].id;
        c.origin = 'Email';
        c.severity__c = '2';
        c.businessHours = bh;
        c.status = 'New';
        c.priority = 'High';
        c.subject = 'SUBJECT';
        c.entitlementId = entitlements[0].id;
        c.description = 'Case with high priority';
        c.bug__c = bugs[0].id; 
        cases.add(c);

        c = new Case();
        c.contactID = contacts[1].id; 
        c.origin = 'Email';
        c.severity__c = '2';
        c.businessHours = bh;
        c.status = 'New';
        c.priority = 'Urgent';
        c.subject = 'SUBJECT';
        c.entitlementId = entitlements[0].id;
        c.description = 'Case with urgent priority';
        c.bug__c = bugs[1].id;
        //c.designated_support_engineer__c = [SELECT id FROM User LIMIT 1].id;
        cases.add(c);
        
        INSERT cases; 
        
        /* --- End set up mock data --- */
      
        Test.startTest();

        // Instantiate the BugWeightSys_Manager and run the method.
        BugWeightSys_Manager bwm = new BugWeightSys_Manager();
        List<Bugs__c> bugs2 = bwm.retrieveBugs();
        //bwm.processBugs(bugs2);
        List<Integer> bugWeights = new List<Integer>();
        for(Integer i = 0; i < bugs2.size(); i++) {
            System.debug('bwm.calcBugWeight(bugs2[i]) = ' + bwm.calcBugWeight(bugs2[i]));
            bugWeights.add(bwm.calcBugWeight(bugs2[i]));
        }
        Test.stopTest();
             
        // Verfiy the results  
        System.assertEquals(224, bugWeights[0]);
        System.assertEquals(327, bugWeights[1]); 
    }

    //
    // Test to verify calculation when Bug having no Cases
    //
    private static testMethod void test_4() {

        List<Bugs__c> bugs = new List<Bugs__c>();
        Bugs__c bug = new Bugs__c();
        bug.name = '1'; 
        bug.severity__c = '1';
        bug.status__c = 'Open';
        bug.bug_Id__c = '1';
        bug.currencyIsoCode = 'SEK';
        bug.service_class__c = '1';
        bugs.add(bug);
        
        bug = new Bugs__c();
        bug.name = '2'; 
        bug.severity__c = '2';
        bug.status__c = 'Open';
        bug.bug_Id__c = '2';
        bug.currencyIsoCode = 'SEK';
        bug.service_class__c = '2';
        bugs.add(bug);
          
        INSERT bugs;

        // --- End set up mock data --- //
      
        Test.startTest();
        
        // Instasiate the BugWeightSys_Manager and run the execute method.
        BugWeightSys_Manager bwm = new BugWeightSys_Manager();
        List<Bugs__c> executedBugs = bwm.retrieveBugs();

        // Process all bugs to update weights
        if (executedBugs.size() > 0 ) {
            bwm.processBugs(executedBugs);
        }

        // Stop test        
        Test.stopTest();

        for (Bugs__c b : executedBugs) {
            System.assertEquals(null, b.bug_weight__c);
            System.assertEquals(null, b.service_class__c);
            System.assertEquals(0, b.number_of_cases__c);
        }
    }

    // Test manual first scheduling and batch
    private static testMethod void test_5() {

        Test.startTest();

        // Schedule the test job
        String jobId = System.schedule(
            'testBugWeightCalculation',
            '0 0 0 3 9 ? 2033',
            new BugWeightSys_Scheduler());

        // Get the information from the CronTrigger API object
        CronTrigger ct = [
            SELECT  Id, CronExpression, TimesTriggered, NextFireTime
            FROM    CronTrigger
            WHERE   id = :jobId];

        // Verify the expressions are the same
        System.assertEquals('0 0 0 3 9 ? 2033', ct.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);

        //// Verify the next time the job will run
        System.assertEquals('2033-09-03 00:00:00', 
        String.valueOf(ct.NextFireTime));

        Test.stopTest();
    }

    // Test cyclic auto scheduling and batch
    private static testMethod void test_6() {

        Test.startTest();

        // Schedule the test job
        String jobId = System.schedule(
            'testBugWeightCalculation',
            BugWeightSys_Scheduler.CRON_EXP, 
            new BugWeightSys_Scheduler());

        // Get the information from the CronTrigger API object
        CronTrigger ct = [
            SELECT  Id, CronExpression, TimesTriggered, NextFireTime
            FROM    CronTrigger
            WHERE   id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(BugWeightSys_Scheduler.CRON_EXP, ct.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
    }

    private static testMethod void test_7() {

        List<Bugs__c> bugs = new List<Bugs__c>();
        Bugs__c bug = new Bugs__c();
        bug.name = '1'; 
        bug.severity__c = '1';
        bug.status__c = 'Open';
        bug.bug_Id__c = '1';
        bug.currencyIsoCode = 'SEK';
        bug.service_class__c = '1';
        bugs.add(bug);
        
        bug = new Bugs__c();
        bug.name = '2'; 
        bug.severity__c = '2';
        bug.status__c = 'Open';
        bug.bug_Id__c = '2';
        bug.currencyIsoCode = 'SEK';
        bug.service_class__c = '2';
        bugs.add(bug);
          
        INSERT bugs;

        // --- End set up mock data --- //
      
        Test.startTest();

        BugWeightSys_BatchJob batchApex =
            new BugWeightSys_BatchJob(BugWeightSys_Scheduler.query);           
        Id batchprocessid = Database.executeBatch(batchApex); 

        Test.stopTest();
    }

    private static testMethod void testBuildTestBugs(){
        Test.startTest();
        List<Bugs__c>bugs = BugWeightSys_TestFactory.buildTestBugs(2);
        System.assertEquals(2, bugs.size());
        Test.stopTest();
    }

    private static testMethod void testbuildTestAccountLicenses(){
        //QTTestUtils.GlobalSetUp();
        Test.startTest();
        User u = QTTestUtils.createMockUserForProfile('System Administrator');
        Account acc = QTTestUtils.createMockAccount('account name', u);
        Account acc2 = QTTestUtils.createMockAccount('account name2', u);
        Account acc3 = QTTestUtils.createMockAccount('account name3', u);
        List<Account> accs = new List<Account>();
        accs.add(acc);
        accs.add(acc2);
        accs.add(acc3);
        List<Account_License__c>licenses = BugWeightSys_TestFactory.buildTestAccountLicenses(3, accs);
        System.assertEquals(3, licenses.size());
        Test.stopTest();
    }
}