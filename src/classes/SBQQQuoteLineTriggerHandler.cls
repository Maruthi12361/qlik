/**
* Class: SBQQQuoteLineTriggerHandler
* @description Handler class for SBQQQuoteLineTrigger.trigger.
* 
* Changelog:
*    2016-11-17 : Roman Dovbush (4front) : Initial development.
*    2017-03-26 : Roman Dovbush (4front) : Commented out methods that are not used.
*    2017-11-17 : UIN Ramakrishna Kini QCW-4282 : Calling quote line limits calculation methods
*/


public class SBQQQuoteLineTriggerHandler {
    // This should be used in conjunction with the ApexTriggerComprehensive.trigger template
    // The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx
    private boolean m_isExecuting = false;
    private integer batchSize = 0;
    Static boolean IsAfterInsertProcessing = false;
    Static boolean IsAfterUpdateProcessing = false;
    Static boolean IsAfterDeleteProcessing = false;
    Static boolean IsBeforeInsertProcessing = false;
    Static boolean IsBeforeUpdateProcessing = false;
    Static boolean IsBeforeDeleteProcessing = false;

    public SBQQQuoteLineTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        batchSize = size;
    }

    public void OnBeforeInsert(List<SBQQ__QuoteLine__c> newSBQQQuoteLines){
        // Changes as part of QCW-4282
        if(!IsBeforeInsertProcessing) {
            IsBeforeInsertProcessing = true;
            CalculateQuoteLineLimits.onBeforeInsert(newSBQQQuoteLines);
        }
    }

    public void OnAfterInsert(SBQQ__QuoteLine__c[] newSBQQQuoteLines){
        Set<Id> qLID = new Set<Id>();
        if(!IsAfterInsertProcessing) {
            IsAfterInsertProcessing = true;
            System.debug('QLines: ' + newSBQQQuoteLines);
            //CalculateQuoteLineLimits.onAfterInsert(newSBQQQuoteLines);
            PopulatePartnerDiscountDescriptOnQuote.onAfterInsertUpdateDelete(newSBQQQuoteLines);
        }
    }

    @future public static void OnAfterInsertAsync(Set<ID> newSBQQQuoteLineIDs){
        System.debug('QLines: ' + newSBQQQuoteLineIDs);
    }

    public void OnBeforeUpdate(List<SBQQ__QuoteLine__c> oldSBQQQuoteLines, List<SBQQ__QuoteLine__c> newSBQQQuoteLines, Map<ID, SBQQ__QuoteLine__c> newSBQQQuoteLineMap, Map<ID, SBQQ__QuoteLine__c> oldSBQQQuoteLineMap){
        // Changes as part of QCW-4282
        if(!IsBeforeUpdateProcessing) {
            IsBeforeUpdateProcessing = true;
            System.debug('QLines: Inside before update');
            //Added by UIN as part of QCW-4282
            CalculateQuoteLineLimits.onBeforeUpdate(newSBQQQuoteLines, oldSBQQQuoteLines, newSBQQQuoteLineMap, oldSBQQQuoteLineMap);
            IsBeforeUpdateProcessing = false;
        }
    }

    public void OnAfterUpdate(SBQQ__QuoteLine__c[] oldSBQQQuoteLines, SBQQ__QuoteLine__c[] newSBQQQuoteLines, Map<ID, SBQQ__QuoteLine__c> newSBQQQuoteLineMap, Map<ID, SBQQ__QuoteLine__c> oldSBQQQuoteLineMap){
        if(!IsAfterUpdateProcessing) {
            IsAfterUpdateProcessing = true;
            System.debug('QLines: ' + newSBQQQuoteLines);
            PopulatePartnerDiscountDescriptOnQuote.onAfterInsertUpdateDelete(newSBQQQuoteLines);
        }
    }
    // this methods not used, so they are commented out
    @future public static void OnAfterUpdateAsync(Set<ID> updatedSBQQQuoteLineIDs){
    }

    public void OnBeforeDelete(List<SBQQ__QuoteLine__c> SBQQQuoteLinesToDelete, Map<ID, SBQQ__QuoteLine__c> SBQQQuoteLineMap){
        
    }

    public void OnAfterDelete(List<SBQQ__QuoteLine__c> deletedSBQQQuoteLines, Map<ID, SBQQ__QuoteLine__c> SBQQQuoteLineMap){
        if(!IsAfterDeleteProcessing) {
            IsAfterDeleteProcessing = true;
            System.debug('!!!QLines in handler: ' + deletedSBQQQuoteLines);
            //CalculateQuoteLineLimits.onAfterDelete(deletedSBQQQuoteLines, SBQQQuoteLineMap);
        }
    }

    @future public static void OnAfterDeleteAsync(Set<ID> deletedSBQQQuoteLineIDs){
        System.debug('!!!Deleted items: ' + deletedSBQQQuoteLineIDs);
        PopulatePartnerDiscountDescriptOnQuote.onAfterDelete(deletedSBQQQuoteLineIDs);

    }

    // this methods not used, so they are commented out
    public void OnUndelete(List<SBQQ__QuoteLine__c> restoredSBQQQuoteLines){

    }

    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }

    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }

    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }
}