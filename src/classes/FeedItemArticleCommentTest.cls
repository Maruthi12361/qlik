/**************************************************
* Author: Alberto de Diego from Fluido Oy
*
* Change Log:
* 2015-04-16 Madhav Kakani: Added test case for CR 20197
* 2013-01-08 Madhav Kakani: Replaced Article_Comment__c with Case object
* 2012-11-15 Alberto de Diego: CR# 4623
*        Initial Development
* 2015-10-26 RDZ    Winter 16 Issue - RDZ adding mock knowledge article - Left all commented out. Needs rework.
**************************************************/
@isTest
private class FeedItemArticleCommentTest {
    //Removed this test due to test failures related to the winter 16  release
    static testmethod void testFeedItemArticleCommentTrigger(){

        FeedItemArticleComment.RunTriggerWhenTest = true;
        //This util class insert at least one knowledge article so we can query on them.
        QTTestUtils.createMockKnowledgeArticle();
        KnowledgeArticle article = [SELECT Id FROM KnowledgeArticle Limit 1].get(0);
        
        Test.startTest();
        FeedItem feedItem = new FeedItem(ParentId = article.Id, Body = 'Test body'); 
        insert feedItem;
        
        //test the FeedItemArticleComment trigger
        List<Case> comment = [SELECT Id, Feed_Item__c FROM Case WHERE Knowledge_article__c = :article.Id];
        System.assertEquals(feedItem.Id, comment[0].Feed_Item__c);
        Test.stopTest();        
    }
    private static testmethod void testCaseKnowledgeArticleCreation()
    {
        FeedItemArticleComment.RunTriggerWhenTest = true;
        //This util class insert at least one knowledge article so we can query on them.
        QTTestUtils.createMockKnowledgeArticle();
        KnowledgeArticle article = [SELECT Id FROM KnowledgeArticle Limit 1].get(0);
        System.assert(article.Id != null, 'Mock Article insert did not work.');
                
        FeedItem feedItem = new FeedItem(ParentId = article.Id, Body = 'Test body'); 
        insert feedItem;

        ApexTrigger t = [Select ID, Name, Status from ApexTrigger Where Name='FeedItemArticleComment'];
        List<Case> comment = [SELECT Id, Feed_Item__c, CreatedById FROM Case WHERE Knowledge_article__c = :article.Id];
        System.assert(comment.size()>0 , '---Comments>0 ->If no comments then trigger is deactivated or not working as expected (FeedItemArticleComment).');
        
    } 
}