/****************************************************************
*
*  QuoteSharingRulesOnSellTPHandlerTest
*
*  2017-02-02 RVA: Q2CW, test class for QuoteSharingRecalculation, handler for setting sharing rule on a quote.
*  06.02.2017  RVA :   changing CreateAcounts methods
*  Rodion Vakulvsokyi 14.03.2017
*  Shubham Gupta 24.07.2017
*  02.09.2017 : Srinivasan PR- fix for query error
*  06-09-2017 Linus Löfberg Q2CW-2953 setting quote recipient to conform with added validation conditions.
*****************************************************************/
@isTest
public class QuoteSharingRulesOnSellTPHandlerTest {
    static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
    @isTest
    public static void processTest() {
        QuoteTestHelper.createCustomSettings();
        User testUser = [Select id, contactId From User where id =: UserInfo.getUserId()];
        System.runAs(testUser) {
        /*
            Subsidiary__c testSubs = QuoteTestHelper.createSubsidiary();
            insert testSubs;
        QlikTech_Company__c testQtCompany = QuoteTestHelper.createQlickTechCompany(testSubs.id);
            insert testQtCompany;
        */
        Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
            testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
            update testAccount;

         Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;

            Address__c billAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Billing');
            billAddress.Country__c = 'United Kingdom';
            insert billAddress;


            Address__c shippAddress = QuoteTestHelper.createAddress(testAccount.id, testContact.id, 'Shipping');
            insert shippAddress;

            Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
        QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
        List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        RecordTYpe rTypeAcc = [Select Id, DeveloperName From RecordType where DeveloperName = 'End_User_Account'];
        /*
        Account testAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc, 'EndUser');
            insert  testAccount;
        */

            Test.startTest();
        RecordTYpe rTypeAcc2 = [Select Id, DeveloperName From RecordType
                                  where DeveloperName = 'Partner_Account' and sobjecttype='Account'];// fix for query error
        Account testPartnerAccount = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount;
        Account testPartnerAccount2 = QuoteTestHelper.createAccount(testQtCompany, rTypeAcc2, 'Partner');
            insert  testPartnerAccount2;

        Contact testContact2 = QuoteTestHelper.createContact(testPartnerAccount2.id);
            insert testContact2;
        Id pricebookId = Test.getStandardPricebookId();

        Product2 productForTest = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
            insert productForTest;

        PricebookEntry pbEntryTest = QuoteTestHelper.createPriceBookEntry(productForTest.id, '01s20000000E0PWAA0');
            insert pbEntryTest;

        RecordType rType = [Select id From RecordType Where developerName = 'Quote'];
        SBQQ__Quote__c quoteForTest = QuoteTestHelper.createQuote(rType, testContact.id, testAccount, '', 'Direct', 'Open', 'Quote', false, '');
           //quoteForTest.SBQQ__Primary__c = true;
           //quotefortest.Quote_Recipient__c = 'test@gmail.com';
           insert quoteForTest;

        SBQQ__Quote__c quoteForTest2 = QuoteTestHelper.createQuote(rType, testContact2.id, testAccount, testPartnerAccount2.id, 'Reseller', 'Open', 'Quote', false, '');
        quoteForTest2.OwnerId = UserInfo.getUserId();
        quotefortest2.Quote_Recipient__c = testContact.Id;
        insert quoteForTest2;

        System.debug(quoteForTest2);


            update quoteForTest;
         SBQQ__QuoteLine__c testQuoteLine = QuoteTestHelper.createQuoteLine(quoteForTest.id, productForTest.id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');
        insert testQuoteLine;

        QuoteSharingRulesOnSellTPHandler.onAfterInsert(new List<SBQQ__Quote__c>{quoteForTest});
        Test.stopTest();

        List<SBQQ__Quote__Share> listOfSharing = [Select id from SBQQ__Quote__Share];
        System.assertEquals(false, listOfSharing.size() < 0);
        }
    }

    private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                                            Country_Code_Two_Letter__c = countryAbbr,
                                            Country_Name__c = countryName,
                                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
}