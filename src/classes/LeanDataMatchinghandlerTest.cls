@isTest
private class LeanDataMatchinghandlerTest {
    
    @testSetup static void setup(){
        List<QlikTech_Company__c> QlikCompanies = new List<QlikTech_Company__c>();

        QlikTech_Company__c qc = QTTESTUtils.createMockQTCompany('QlikTech USA', 'USA', 'USA');
        qc.Country_Name__c = 'USA';
        qc.QlikTech_Region__c = 'NORDIC';
        qc.QlikTech_Sub_Region__c = 'NORDIC';
        qc.QlikTech_Operating_Region__c = 'NA';
        qc.Language__c = 'English';
        QlikCompanies.add(qc);
        upsert QlikCompanies;
        
        List<Account> Accounts = new List<Account>();
        Account a = new Account();
        a.Name = 'The Test Customer';
        a.Navision_Status__c = 'Customer';
        a.BillingCountry = 'USA';
        a.BillingState = 'Arkansas';
        a.Segment__c = 'Strategic - Key';
        a.Billing_Country_Code__c = QlikCompanies[0].Id;
        a.QlikTech_Company__c = QlikCompanies[0].QlikTech_Company_Name__c;
        a.Shipping_Country_Code__c = QlikCompanies[0].Id;
        Accounts.add(a);
        insert Accounts;
        
        List<Lead> Leads = new List<Lead>();
        Lead l = new Lead();
        l.FirstName = 'First';
        l.LastName = 'Last';
        l.Email = 'first.last@lead.com';
        l.Country_Code__c = QlikCompanies[0].Id;
        l.Country_Name__c = QlikCompanies[0].Id;
        l.Country = 'United States';
        l.State = 'CA';
        l.Company = 'The Lead';
        Leads.add(l);
        
        Lead l1 = new Lead();
        l1.FirstName = 'FirstTest';
        l1.LastName = 'LastTest';
        l1.Email = 'first.last2@lead.com';
        l1.Country_Code__c = QlikCompanies[0].Id;
        l1.Country_Name__c = QlikCompanies[0].Id;
        l1.Country = 'United States';
        l1.Company = 'The Lead1';
        l1.State = 'CA';
        Leads.add(l1);
        insert Leads;
        }
    @isTest 
    static void test_1(){
        Test.startTest();
        Lead ll = [SELECT Id FROM Lead WHERE Email = 'first.last@lead.com'];
        ll.LeanData__Reporting_Matched_Account__c = [SELECT Id FROM Account WHERE Name = 'The Test Customer'].id;
        ll.LeanData_Segment__c = 'Strategic - Key';
        ll.LeanData_Mailing_State__c = 'Arkansas';
        ll.LeanData_Mailing_Country__c = 'USA';
        ll.LeanData_ABM_Tier__c = '1';
        update ll;
        
        Test.stopTest();
    }
    @isTest 
    static void test_2(){
        Test.startTest();
        Lead lll = [SELECT Id FROM Lead WHERE Email = 'first.last2@lead.com'];
        lll.LeanData__Reporting_Matched_Account__c = [SELECT Id FROM Account WHERE Name = 'The Test Customer'].id;
        lll.LeanData_Segment__c = 'Strategic - Key';
        lll.LeanData_Mailing_State__c = 'California';
        lll.LeanData_Mailing_Country__c = 'USA';
        update lll;
        Test.stopTest();
    }
}