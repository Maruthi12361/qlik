public class DelegateApprovalRequestAction {
 
 
 	@InvocableMethod(label='Populate Approver' description='Populates the approver field with the owner or delegate')
 	public static void populateApprover(List<Id> ids) {
 		
 		Opportunity[] opps = [SELECT Id, Active_PSM__c, StageName FROM Opportunity WHERE Id IN: ids];
 		DelegateApprovalRequest.handle(null, opps);
 		update opps;
 		
 	}
 
 
    
}