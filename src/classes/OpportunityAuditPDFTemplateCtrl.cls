// CR# 10936
// controller class behind the visualforce page: Opportunity_Audit_PDF_Template
// Change log:
// February 02, 2014 - Initial Implementation - Madhav Kakani - Fluido Oy
public with sharing class OpportunityAuditPDFTemplateCtrl {
    private Opportunity opp;
    public List<OpportunityLineItem> lItems {get;set;}
    public List<Non_Licence_Related_ProductsCO_Non_Li__c> lNRPItems {get;set;}
    public Boolean showItems1 {get;set;}    
    public Decimal quote_ref {get;set;}
    
    public OpportunityAuditPDFTemplateCtrl(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();

        showItems1 = false;
        lItems = [SELECT Name__c, Quantity, ListPrice, CPQ_List_Price__c, Discount__c,
                    Account_License_RO__c, Partner_Margin_RO__c, TotalPrice
                    FROM OpportunityLineItem WHERE OpportunityId = :opp.Id];
       if(lItems.size() > 0) showItems1 = true;
        try {
            Quote__c q = [SELECT Quote_Id__c FROM Quote__c WHERE Primary__c = true AND Opportunity__c = :opp.Id];
            quote_ref = q.Quote_Id__c;
        } catch(Exception e) {}
    }
}