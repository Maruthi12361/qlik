/******************************************************

    Class: SBQQSubscriptionSharing
    Subscription record is shared if

    1.  Partner is the “Sell Through Partner” 
        Level of access: read/write

    2.  Partner is set as the End User Account on the Subscription record 
        Level of access: read/write

    Changelog:
        2017-01-22  TJG     Created file
                            
******************************************************/
public without sharing class SBQQSubscriptionSharing {
    public static Boolean UpdateSBQQSubscriptionSharing(List<Id> subsIds)
    {

        System.debug('-=->Update SBQQSubscription Sharing, subsIds.size() = ' + subsIds.size());

        if (subsIds.size() == 0)
        {
            return true;
        }
        
        List<SBQQ__Subscription__c> subs4Ids = [
            SELECT
            	Name, SBQQ__Account__c, OwnerId, SBQQ__Contract__r.Sell_through_Partner__c
            FROM
                SBQQ__Subscription__c
            WHERE
                Id IN :subsIds 
        ];

        List<Id> endUsers = new List<Id>();
        List<Id> sellTPs  = new List<Id>();

        for (SBQQ__Subscription__c subsc : subs4Ids)
        {
            endUsers.add(subsc.SBQQ__Account__c);
            sellTPs.add(subsc.SBQQ__Contract__r.Sell_Through_Partner__c);
        }

        List<SBQQ__Subscription__c> allSubs = [
            SELECT
                Name, SBQQ__Account__c, OwnerId, SBQQ__Contract__r.Sell_through_Partner__c
            FROM
                SBQQ__Subscription__c
            WHERE
                Id IN :subsIds 
                OR SBQQ__Contract__r.Sell_Through_Partner__c IN :sellTPs
                OR SBQQ__Account__c IN :endUsers 
        ];

        // recalculate sharing for these subscrunities 
        Set<Id> uniqueAccIds = new Set<Id>();
        Map<Id, Id> SBQQSubscriptionToSellThroughPartner = new Map<Id, Id>();
        Map<Id, Id> SBQQSubscriptionToEndUserAccount = new Map<Id, Id>();
        // Fetch Lists of SellThroughPartners and ReferringContacts
        for (SBQQ__Subscription__c anySub : allSubs)
        {
            if (!isNullOrEmpty(anySub.SBQQ__Contract__r.Sell_Through_Partner__c))
            {
                if (!uniqueAccIds.contains(anySub.SBQQ__Contract__r.Sell_Through_Partner__c))
                {
                    uniqueAccIds.add(anySub.SBQQ__Contract__r.Sell_Through_Partner__c);
                }

                if (!SBQQSubscriptionToSellThroughPartner.containsKey(anySub.Id))
                {
                    SBQQSubscriptionToSellThroughPartner.put(anySub.Id, anySub.SBQQ__Contract__r.Sell_Through_Partner__c);
                }
            }

            if (anySub.SBQQ__Account__c != null)
            {
                if (!uniqueAccIds.contains(anySub.SBQQ__Account__c))
                {
                    uniqueAccIds.add(anySub.SBQQ__Account__c);
                }
                if (!SBQQSubscriptionToEndUserAccount.containsKey(anySub.Id))
                {
                    SBQQSubscriptionToEndUserAccount.put(anySub.Id, anySub.SBQQ__Account__c);
                }
            }
        }

        Boolean noNewShares = false;
        // If no UniqueAccounts found we can quite now
        if (uniqueAccIds.size() == 0)
        {
            System.debug('UpdateSBQQSubscriptionSharing: No unique accounts found.');            
            noNewShares =  true;
        }

        // Fetch all the PortalRoles
        Map<Id, Id> accountToRole = new Map<Id,Id>();
        Set<Id> uniquePortalRoles = new Set<Id>();
        List<Id> portalRoleIds = new List<Id>();

        List<UserRole> userRoles = ApexSharingRules.GetListOfUserRolesByPortalAccountIds(uniqueAccIds, 'Partner', 'Executive');
        
        for (UserRole usrRole :userRoles)
        {
            if (!uniquePortalRoles.contains(usrRole.Id))
            {
                uniquePortalRoles.add(usrRole.Id);
                portalRoleIds.add(usrRole.Id);
            }
            accountToRole.put(usrRole.PortalAccountId, usrRole.Id);
        }

        System.debug('uniqueAccIds=' + uniqueAccIds + ', uniquePortalRoles=' + uniquePortalRoles + '###### userRoles=' + userRoles);

        if (uniquePortalRoles.size() == 0)
        {            
            System.debug('UpdateSBQQSubscriptionSharing: No unique PortalRoles found.');
            noNewShares =  true;
        }

        Map<Id,Id> roleToGroup = new Map<Id,Id>();

        List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(portalRoleIds, 'RoleAndSubordinates');
        for (Group grp : groups)
        {
            roleToGroup.put(grp.RelatedId, grp.Id);
        }

        if (roleToGroup.size() == 0)
        {
            System.debug('UpdateSBQQSubscriptionSharing: No unique Groups found.');
            noNewShares =  true;
        }
        // All data loaded continue to create sharing rules
        List<SBQQ__Subscription__Share> newSBQQSubscriptionShares = new List<SBQQ__Subscription__Share>();

        if (!noNewShares)
        {
            for (SBQQ__Subscription__c subscr : allSubs)
            {
                Id partnerId = SBQQSubscriptionToSellThroughPartner.containsKey(subscr.Id) ? SBQQSubscriptionToSellThroughPartner.get(subscr.Id) : 
                         ( SBQQSubscriptionToEndUserAccount.containsKey(subscr.Id) ? SBQQSubscriptionToEndUserAccount.get(subscr.Id) : null ); 
                System.debug('partnerId=' + partnerId);
                System.debug('accountToRole=' + accountToRole);

                if (partnerId != null && accountToRole.containsKey(partnerId))
                {
                    Id stPartnerRole = accountToRole.get(partnerId);
                    if (roleToGroup.containsKey(stPartnerRole))
                    {
                        newSBQQSubscriptionShares.add(CreateNewSBQQSubscriptionShare(subscr.Id, 'Edit', roleToGroup.get(stPartnerRole)));
                    }
                }
            }
        }

        // retrieving existing Manual SBQQSubscription Sharing
        List<SBQQ__Subscription__Share> subsShares = [select Id, ParentId, UserOrGroupId, AccessLevel from SBQQ__Subscription__Share 
                    where ParentId in :subsIds and RowCause = 'Manual'];

        // filter out those need to be deleted
        List<SBQQ__Subscription__Share> toDel = new List<SBQQ__Subscription__Share>();
        for (SBQQ__Subscription__Share oldShare : subsShares)
        {
            Boolean matched = false;
            for (integer i=0; i<newSBQQSubscriptionShares.size(); i++)
            {
                if (oldShare.AccessLevel == newSBQQSubscriptionShares[i].AccessLevel &&
                    oldShare.ParentId == newSBQQSubscriptionShares[i].ParentId &&
                    oldShare.UserOrGroupId == newSBQQSubscriptionShares[i].UserOrGroupId )
                {
                    matched = true;
                    // already in place, no need to insert again
                    newSBQQSubscriptionShares.remove(i);
                }
            }
            if (!matched)
            {
                toDel.add(oldShare);
            }
        } 
        // delete those sharing that are no longer valid
        if (toDel.size() > 0)
        {
            System.debug('delete SBQQSubscriptionSharing ' + toDel);
            try
            {
                Delete toDel;
            }
            catch (Exception ex)
            {
                System.debug('Failed to delete SBQQSubscription Shares, Exception: ' + ex.getMessage() + '  At line: ' + ex.getLineNumber());
            }
        }

        // Insert sharing if there is the need
        if (newSBQQSubscriptionShares.size() > 0)
        {
            Database.Insert(newSBQQSubscriptionShares, false);
        }
        return true;
    }

	public static Boolean isNullOrEmpty(String s) {
        return (null == s) || (0 == s.trim().length());
    }

    public static SBQQ__Subscription__Share CreateNewSBQQSubscriptionShare(string parentId, string accessLevel, string userOrGroupId)
    {
        SBQQ__Subscription__Share subscShare = new SBQQ__Subscription__Share();

        subscShare.ParentId = parentId;
        subscShare.AccessLevel = accessLevel;
        subscShare.UserOrGroupId = userOrGroupId;

        return subscShare;
    }
}