/******************************************************

    Class: CovertOpenOpportunities
    
    Changelog:
  2016-06-08    Andrew Lokotosh Commented Forecast_Amount fields line  73-75,94
  06.02.2017  RVA :   changing CreateAcounts methods	 
  24.03.2017 Rodion Vakulovskyi                         
******************************************************/




@isTest
private class ClearNutureOnCloseWonTest {
    static Id OppRecordTypeId = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS').id;//'012D0000000KEKO';
    static Id AccRecordTypeId = '01220000000DOFu';  //End User Account

    static testMethod void TestNutureIsCleared() {
        /*
        User userStd = QTTestUtils.createMockUserForProfile('Custom: QlikBuy Sales Std User');
            Account testAcnt = QTTestUtils.createMockAccount('Imperial Venture', userStd, true);

            RecordType rt = [select Id from recordtype where Name like '%CCS Standard%' order by Id desc limit 1];
            
            Opportunity testOpp = New Opportunity (
                Short_Description__c = 'Test nuture Opp',
                Name = 'Test for nuture',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today().addDays(7),
                StageName =  'Closed Won', // 'Goal Discovery', 
                RecordTypeId = rt.Id,    //  '01220000000DNwZAAW', 
                CurrencyIsoCode = 'USD',
                Pre_GI_Stage__c = true,
                ForecastCategoryName = 'Omitted',
                AccountId = testAcnt.Id,
                //License_Amount_USD__c = 1000, 
                License_Forecast_Amount__c = 1000
                ); 
            */
        Test.setMock(WebserviceMock.class, new WebServiceMockDispatcher());
        QTCustomSettings__c Settings = new QTCustomSettings__c(Name = 'Default');
        Settings.OEM_Record_Types__c = '01220000000DNwZAAW';
        Insert Settings;
        QuoteTestHelper.createCustomSettings();
		/*
        Subsidiary__c testSubs = TestQuoteUtil.createSubsidiary();
        // Adding an account with BillingCountryCode and Qliktech Company
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'United Kingdom',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs.id              
        );
        insert QTComp;
        
        Account acc = new Account(
            Name = 'Test',
            RecordTypeId = AccRecordTypeId, //End User Account
            Legal_Approval_Status__c = 'Legal Approval Auto Approved',
            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c,
            BillingStreet = '41 London Road',
            BillingState ='Berks',
            BillingPostalCode = 'RG1 6UT',
            BillingCountry = 'United Kingdom',
            BillingCity = 'Reading',
            
            Shipping_Country_Code__c = QTcomp.Id,
            ShippingStreet = '41 London Road',
            ShippingState ='Berks',
            ShippingPostalCode = 'RG1 6UT',
            ShippingCountry = 'United Kingdom',
            ShippingCity = 'Reading',
            Billing_Country_Code__c = QTComp.Id);

        insert acc;
        */
		User testUser = [Select id From User where id =: UserInfo.getUserId()];
		Account  testAcnt = QTTestUtils.createMockAccount('TestCompany', testUser, true);
	        testAcnt.RecordtypeId = AccRecordTypeId;
	        update testAcnt;
        test.startTest();
        Opportunity testOpp = new Opportunity(
            Name = 'Test Opp PF',
            StageName = 'Goal Identified',
            ForecastCategoryName = 'Omitted', // 'Pipeline',
            CloseDate = Date.today(),
            AccountId = testAcnt.Id,
            RecordTypeId = OppRecordTypeId,
            Type = 'Existing Customer',
            Revenue_Type__c = 'Direct',
           // License_Forecast_Amount__c = null,
            //Consultancy_Forecast_Amount__c = null,
            //Education_Forecast_Amount__c = null,
            Function__c = 'Other',
            Solution_Area__c = 'Other',
            Signature_Type__c = 'Digital Signature'
         //   Support_Forecast_Amount__c = null       
        );
            


            //Test.startTest();
            
            insert testOpp;

            System.debug('TestNutureIsCleared ---- Nuture = ' + testOpp.Pre_GI_Stage__c);

            testOpp = [select Id, Pre_GI_Stage__c from Opportunity where Id = :testOpp.Id];

            System.assertEquals(false, testOpp.Pre_GI_Stage__c);

            testOpp.Pre_GI_Stage__c = true;
            testOpp.StageName = 'Prove Value';
            testOpp.Included_Products__c = 'Qlik Sense';
            update testOpp;

            testOpp = [select Id, Pre_GI_Stage__c from Opportunity where Id = :testOpp.Id];
            
            System.assertEquals(true, testOpp.Pre_GI_Stage__c);

            testOpp.StageName = 'Closed Won';
            update testOpp;

            testOpp = [select Id, Pre_GI_Stage__c from Opportunity where Id = :testOpp.Id];

            System.debug('TestNutureIsCleared ---- Nuture = ' + testOpp.Pre_GI_Stage__c);

            System.assertEquals(false, testOpp.Pre_GI_Stage__c);

            Test.stopTest();
    }
    
}