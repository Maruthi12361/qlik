/******************************************************

    Class: AccountSharing
    
    Changelog:
        2015-11-25  IRN     Created file
        2019-03-04  BAD - readding file, and removing PRM related sharing - CPLOG remains
                            
******************************************************/
public class AccountSharing {

   public static void updateAccountSharing(List<Id> accounts){
        accountRemoveManualSharing(accounts);
        startSharing(accounts);
   }

   public static List<Account> getListOfAccounts(List<Id> accounts){
      System.debug('---getListOfAccounts--- ' + accounts);
       Integer BATCHSIZE = 200;
       List<Account> result = new List<Account>();

        if (accounts == null || accounts.size() == 0)
        {
            return result;
        }

        for (Integer i = 0; i < (accounts.size() / BATCHSIZE) + 1; i++)
        {
            List<Id> tempList;
            if (accounts.size() > ((i + 1) * BATCHSIZE))
            {
                tempList = ApexSharingRules.getRange(accounts, (i * BATCHSIZE), BATCHSIZE);
            }
            else
            {
                tempList = ApexSharingRules.getRange(accounts, (i * BATCHSIZE), accounts.size() - (i * BATCHSIZE));
            }
            // TJG 2016-01-12 removed Responsible_Partner__c from the next line
            List<Account> temp = [select Id, OwnerId, Owner.Contact.Account.Id, Owner.UserRoleId, Owner.Is_Partner_User__c  from Account where Id in :tempList];
            result.addAll(temp);
        }
        System.debug('---result--- ' + result);
        return result;

   }

  public static void addRelatedListAccountSharing(List<Responsible_Partner__c> responsiblePartners){
    System.debug('----addRelatedListAccountSharing---' + responsiblePartners);
      //check if sharing is done for this account and this id, if so do not share once agin
      List<AccountShare> relatedListAddSharing = new List<AccountShare>();
      List<AccountShare> relatedListUpdateSharing = new List<AccountShare>();
      List<Id> endusers = new List<Id>();               
      List<Id> partners = new List<Id>();
      for(Integer i = 0; i< responsiblePartners.size(); i++){
        if(responsiblePartners[i].Partner__c != null){
          endusers.add(responsiblePartners[i].End_User__c);
          partners.add(responsiblePartners[i].Partner__c); 
        }
      }

        // Fetch all related Roles
        Map<Id, List<Id>> accountToRole = new Map<Id, List<Id>>();
        Set<Id> uniquePortalRoleIDs = new Set<Id>();
        List<UserRole> userRoles = ApexSharingRules.getPartnerRoles(partners, 'CustomerPortal', 'Executive');
        for(Integer i = 0; i<userRoles.size(); i++){
            if(accountToRole != null && accountToRole.containsKey(userRoles[i].PortalAccountId)){
              List<Id> temp = accountToRole.get(userRoles[i].PortalAccountId);
              temp.add(userRoles[i].Id);
            }else{
              List<Id> temp = new List<Id>();
              temp.add(userRoles[i].Id);
              accountToRole.put(userRoles[i].PortalAccountId, temp);  
            }
            if (!uniquePortalRoleIDs.Contains(userRoles[i].Id))
            {
                uniquePortalRoleIDs.Add(userRoles[i].Id);
            }
        }

      Set<Id> roleToGroup = new Set<Id>();
      Map<Id, Id>groupMap = new Map<Id, Id>();
      // Fetch all groups associated with the roles
      List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(new List<Id>(uniquePortalRoleIDs), 'RoleAndSubordinates');
      for(Integer i = 0; i < groups.size(); i++){
          roleToGroup.add(groups[i].Id);
          groupMap.put(groups[i].RelatedId ,groups[i].Id);
      }
      List<Id> userOrGroupIds = new List<Id>();
      userOrGroupIds.addAll(roleToGroup);

      List<AccountShare> accountShare = [select Id, UserOrGroupId, AccountId, AccountAccessLevel, CaseAccessLevel, ContactAccessLevel, OpportunityAccessLevel from AccountShare where RowCause = 'Manual' and AccountId in :endusers and UserOrGroupId in :userOrGroupIds];
      Map<Id, Map<Id, AccountShare>> accountShareMap = new Map<Id, Map<Id, AccountShare>>();
      for(Integer i = 0; i< accountShare.size(); i++){
          if(!accountShareMap.containsKey(accountshare[i].AccountId)){
              Map<Id, AccountShare> sharingmap = new Map<Id, Accountshare>();
              sharingmap.put(accountShare[i].UserOrGroupId, accountshare[i]);
              accountShareMap.put(accountshare[i].AccountId,sharingmap);
          }else{
              Map<Id, AccountShare> sharingmap = accountShareMap.get(accountshare[i].AccountId);
              if(!sharingmap.containsKey(accountShare[i].UserOrGroupId)){
                sharingmap.put(accountShare[i].UserOrGroupId, accountShare[i]);
              }
          }
      }

      for(Integer i = 0; i< responsiblePartners.size(); i++){
          Map<Id, AccountShare> groupSharingMap = accountShareMap.get(responsiblePartners[i].End_User__c); //exist today
          List<Id> userRoleIds = accountToRole.get(responsiblePartners[i].Partner__c);//fetch roles for this partner
          if(userRoleIds != null){
            for(Integer j = 0; j<userRoleIds.size(); j++){
              Id userRoleId = userRoleIds[j];
              if(userRoleId != null)
              {
                Id groupId = groupMap.get(userRoleId);
                System.debug('groupsharingMap' + groupSharingMap);
                if(groupsharingMap == null || !groupSharingMap.containsKey(groupId)){
                  relatedListAddSharing.add(createNewAccountShare(responsiblePartners[i].End_User__c, 'read', 'None', 'None', 'None', groupId)); 
                }else{
                  AccountShare uppdatedAccountSharing = updateSharing(groupSharingMap.get(groupId), 'read', 'None', 'None', 'None');
                    if(uppdatedAccountSharing != null){
                          relatedListUpdateSharing.add(uppdatedAccountSharing);  
                    }
                }
              }
            }
          }
      }
      System.debug('---add sharing ' + relatedListAddSharing);
      System.debug('---update sharing sharing ' + relatedListUpdateSharing);
      insert relatedListAddSharing;
      update relatedListUpdateSharing;
   }

  public static void deleteRelatedListAccountSharing(List<Responsible_Partner__c> responsiblePartners){
    
    Set<AccountShare> tobeDeleted = new Set<AccountShare>();
    List<Id> endusers = new List<Id>();
    List<Id> partners = new List<Id>();
    Map<Id, Set<Id>>endusersMap = new Map<id, Set<Id>>();
    for(Integer i = 0; i<responsiblePartners.size(); i++){
        endusers.add(responsiblePartners[i].End_User__c);
        partners.add(responsiblePartners[i].Partner__c);
        if(endusersMap.containskey(responsiblePartners[i].End_User__c)){
            endusersMap.get(responsiblePartners[i].End_User__c).add(responsiblePartners[i].Partner__c);
          }else{
            Set<Id> temp = new Set<Id>();
            temp.add(responsiblePartners[i].Partner__c);
            endusersMap.put(responsiblePartners[i].End_User__c, temp);
          }
    }
    
        // Fetch all related Roles
        Map<Id, List<Id>> accountToRole = new Map<Id, List<Id>>();
        Set<Id> uniquePortalRoleIDs = new Set<Id>();        
        List<UserRole> userRoles = ApexSharingRules.getPartnerRoles(partners, 'CustomerPortal', 'Executive');
        for(Integer i = 0; i<userRoles.size(); i++){
           if(accountToRole != null && accountToRole.containsKey(userRoles[i].PortalAccountId)){
            List<Id> temp = accountToRole.get(userRoles[i].PortalAccountId);
            temp.add(userRoles[i].Id);
          }else{
            List<Id> temp = new List<Id>();
            temp.add(userRoles[i].Id);
            accountToRole.put(userRoles[i].PortalAccountId, temp);
          }
            if (!uniquePortalRoleIDs.Contains(userRoles[i].Id))
            {
                uniquePortalRoleIDs.Add(userRoles[i].Id);
            }
        }

      Set<Id> roleToGroup = new Set<Id>();
      Map<Id, Id>groupMap = new Map<Id, Id>();
      // Fetch all groups associated with the roles
      List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(new List<Id>(uniquePortalRoleIDs), 'RoleAndSubordinates');
      for(Integer i = 0; i < groups.size(); i++){
          roleToGroup.add(groups[i].Id);
          groupMap.put(groups[i].RelatedId ,groups[i].Id);
      }
      List<Id> userOrGroupIds = new List<Id>();
      userOrGroupIds.addAll(roleToGroup);

      List<AccountShare> accountShare = [select Id, UserOrGroupId, AccountId, AccountAccessLevel, CaseAccessLevel, ContactAccessLevel, OpportunityAccessLevel from AccountShare where RowCause = 'Manual' and AccountId in :endusers and UserOrGroupId in :userOrGroupIds];
      Map<Id, Map<Id, AccountShare>> accountShareMap = new Map<Id, Map<Id, AccountShare>>();
      for(Integer i = 0; i< accountShare.size(); i++){
          if(!accountShareMap.containsKey(accountshare[i].AccountId)){
              Map<Id, AccountShare> sharingmap = new Map<Id, Accountshare>();
              sharingmap.put(accountShare[i].UserOrGroupId, accountshare[i]);
              accountShareMap.put(accountshare[i].AccountId,sharingmap);
          }else{
              Map<Id, AccountShare> sharingmap = accountShareMap.get(accountshare[i].AccountId);
              if(!sharingmap.containsKey(accountShare[i].UserOrGroupId)){
                sharingmap.put(accountShare[i].UserOrGroupId, accountShare[i]);
              }
          }
      }  

      //sharing done do to record owener
      Map<Id, Id> sharingDoneDueToRecordOwner = sharingDoneDueToRecordOwner(endusers);
      Map<Id, Boolean> sharingDoneDueToSellThroughPartnerOnOpportunity = sharingDoneDueToSellThroughPartnerOnOpportunity(endusers, partners, endusersMap);

      for(Integer i = 0; i< responsiblePartners.size(); i++){
          Map<Id, AccountShare> groupSharingMap = accountShareMap.get(responsiblePartners[i].End_User__c); //maual sharing that exists today for this account
          Set<Id>partnersForThisAccount = endusersMap.get(responsiblePartners[i].End_User__c);
          if(groupSharingMap != null && !groupsharingMap.isEmpty() && (!sharingDoneDueToSellThroughPartnerOnOpportunity.containsKey(responsiblePartners[i].End_User__c)  || !sharingDoneDueToSellThroughPartnerOnOpportunity.get(responsiblePartners[i].End_User__c))){
              for(AccountShare share : groupSharingMap.values()){
                for(Id p : partnersForThisAccount){
                    if(accountToRole == null || !accountToRole.containsKey(p)){
                      break;
                    }else{
                      List<Id> userRoleIds = accountToRole.get(p);
                      for(Integer j = 0; j<userRoleIds.size(); j++){
                        Id UserRoleId = userRoleIds[j];
                        if(groupMap.containsKey(userRoleId)){ 
                          if(groupMap.get(userRoleId) == share.UserOrGroupId){
                            if(sharingDoneDueToRecordOwner.containsKey(responsiblePartners[i].End_User__c)){
                              Id partner = sharingDoneDueToRecordOwner.get(responsiblePartners[i].End_User__c);
                              if(partner != null && accountToRole.containsKey(partner)){
                                break;
                              }else{
                                tobeDeleted.add(share);   
                              }

                            }else{
                              tobeDeleted.add(share);    
                            }
                            
                          }
                        }  
                      }
                      
                    } 
                }
            }
          }
      }
    System.debug('Tobe deleted ' + tobeDeleted);
    List<AccountShare> tobeDeletedList = new List<AccountShare>();
    tobeDeletedList.addAll(tobeDeleted);
    delete tobeDeletedList;
   }

  public static Map<Id, Id> sharingDoneDueToRecordOwner(List<Id> accountsIds){
      Map<Id, Id> result = new Map<Id, Id>();
      List<Account> accounts = getListOfAccounts(accountsIds);

      for (Integer i = 0; i< accounts.size(); i++) 
      {
          if(accounts[i].Owner != null && accounts[i].Owner.Is_Partner_User__c == 'YES'){
            result.put(accounts[i].Id, accounts[i].OwnerId); 
          }else{
            result.put(accounts[i].Id, accounts[i].OwnerId); 
          }
      }

      return result;
   }

   public static Map<Id, Boolean> sharingDoneDueToSellThroughPartnerOnOpportunity(List<Id> accountId, List<Id> partner, Map<Id, Set<Id>>endusersMap){
      Map<Id, Boolean> result = new Map<Id, Boolean>();
      List<Opportunity> opps = [SELECT Id, AccountId, Sell_Through_Partner__c FROM opportunity WHERE stageName NOT IN ('Closed Lost', 'Goal Rejected') AND Sell_Through_Partner__c  in :partner AND AccountId in :accountId];
      for(Integer i = 0; i<opps.size(); i++){
          if(endusersMap.containsKey(opps[i].AccountId)){
              Set<Id> partners = endusersMap.get(opps[i].AccountId);
              if(partners.contains(opps[i].Sell_Through_Partner__c)){
                  result.put(opps[i].accountId, true);        
              }
          } 
      }
      return result;
   }

   public static AccountShare updateSharing(AccountShare oldSharing, String accessLevel, String caseAccess, String opportunityAccess, String contactAccess){
      boolean updateNeeded = false;
      //AccountAccessLevel, CaseAccessLevel, ContactAccessLevel, OpportunityAccessLevel
      if(ApexSharingRules.getAccessTypeValue(oldSharing.AccountAccessLevel) < ApexSharingRules.getAccessTypeValue(accessLevel)){
          updateNeeded = true;
          oldSharing.AccountAccessLevel = accessLevel;
      }
      if(ApexSharingRules.getAccessTypeValue(oldSharing.CaseAccessLevel) < ApexSharingRules.getAccessTypeValue(caseAccess)){
          updateNeeded = true;
          oldSharing.CaseAccessLevel = caseAccess;
      }
      if(ApexSharingRules.getAccessTypeValue(oldSharing.ContactAccessLevel) < ApexSharingRules.getAccessTypeValue(contactAccess)){
          updateNeeded = true;
          oldSharing.ContactAccessLevel = contactAccess;
      }
      if(ApexSharingRules.getAccessTypeValue(oldSharing.OpportunityAccessLevel) < ApexSharingRules.getAccessTypeValue(opportunityAccess)){
          updateNeeded = true;
          oldSharing.OpportunityAccessLevel = opportunityAccess;
      }
      if(updateNeeded){
          return oldSharing;
      }else{
          return null;
      }
   }

   public static void startSharing(List<Id> acc){
      System.debug('-------startsharing-------');
      addCPAccountSharingRules(acc);
      //we need to readd old sharing due to when an account owner is changed on an account all manual sharing are removed
      reAddAccountShareDueToPartnerExistsInRelatedList(acc);
      reAddAccountShareDueTOSellThroughPartnerOnAnOpportunity(acc);
   }


    public static void addCPAccountSharingRules(List<Id> acc)
    {
        Map<Id, Id> accountToRole = new Map<Id, Id>();
        Set<Id> uniquePortalRoleIDs = new Set<Id>();

        // Fetch all related Roles
        List<UserRole> userRoles = ApexSharingRules.getPartnerRoles(acc, 'CustomerPortal', 'Executive');
        for(Integer i = 0; i<userRoles.size(); i++){
            accountToRole.put(userRoles[i].PortalAccountId, userRoles[i].Id);
            if (!uniquePortalRoleIDs.Contains(userRoles[i].Id))
            {
                uniquePortalRoleIDs.Add(userRoles[i].Id);
            }
        }

        if (accountToRole.Size() == 0)
        {
            System.debug('Exiting -- no CP users exists on this account');
            return;
        }

        Map<Id, Id> roleToGroup = new Map<Id, Id>();

        // Fetch the groups associated with the roles
        List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(new List<Id>(uniquePortalRoleIDs), 'RoleAndSubordinates');
        for(Integer i = 0; i<groups.size(); i++){
          roleToGroup.put(groups[i].RelatedId, groups[i].Id);
        }

        List<AccountShare> newAccountShares = new List<AccountShare>();
        for (Integer i = 0; i<acc.size(); i++)
        {
            if (accountToRole.ContainsKey(acc[i]))
            {
                if (roleToGroup.ContainsKey(accountToRole.get(acc[i])))
                {
                    newAccountShares.add(createNewAccountShare(acc[i], 'Read', 'Edit', 'None', 'None', roleToGroup.get(accountToRole.get(acc[i]))));
                }
            }
        }

        if (newAccountShares.Size() > 0)
        {
            Insert newAccountShares;
        }
    }

    public static void reAddAccountShareDueToPartnerExistsInRelatedList(List<Id> accountIds){
         List<responsible_Partner__c> respPartners = [Select Id, End_User__c, Partner__c  from responsible_Partner__c where End_User__c in :accountIds];
         addRelatedListAccountSharing(respPartners);  
    }


    public static void reAddAccountShareDueTOSellThroughPartnerOnAnOpportunity(List<Id> accountIds){
      List<Opportunity> opps = [SELECT Id, Sell_Through_Partner__c, AccountId  FROM opportunity WHERE stageName NOT IN ('Closed Lost', 'Goal Rejected') AND Sell_Through_Partner__c  != :'' AND AccountId in :accountIds];
      Map<Id, List<Opportunity>> accountOpportunityMap = new Map<Id, List<Opportunity>>();
      Map<Id, Id> sellThroughPartnerOpportunityMap = new Map<Id, Id>();
      List<Id> allSellThroughPartners = new List<Id>();
      for(Integer i = 0; i<opps.size(); i++){
        if(accountOpportunityMap.containsKey(opps[i].AccountId)){
            List<Opportunity> tempOppList = accountOpportunityMap.get(opps[i].AccountId);
            tempOppList.add(opps[i]);
        }else{
          List<Opportunity> tempOppList = new List<Opportunity>();
          tempOppList.add(opps[i]);
          accountOpportunityMap.put(opps[i].AccountId, tempOppList);
        }
        allSellThroughPartners.add(opps[i].Sell_Through_Partner__c);
        sellThroughPartnerOpportunityMap.put(opps[i].Id, opps[i].Sell_Through_Partner__c);
      }


        // Fetch all related Roles
        Map<Id, Id> accountToRole = new Map<Id, Id>();
        Set<Id> uniquePortalRoleIDs = new Set<Id>();
        List<UserRole> userRoles = ApexSharingRules.getPartnerRoles(allSellThroughPartners, 'CustomerPortal', 'Executive');
        for(Integer i = 0; i<userRoles.size(); i++){
            accountToRole.put(userRoles[i].PortalAccountId, userRoles[i].Id);
            if (!uniquePortalRoleIDs.Contains(userRoles[i].Id))
            {
                uniquePortalRoleIDs.Add(userRoles[i].Id);
            }
        }

      Set<Id> roleToGroup = new Set<Id>();
      Map<Id, Id>groupMap = new Map<Id, Id>();
      // Fetch all groups associated with the roles
      List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(new List<Id>(uniquePortalRoleIDs), 'RoleAndSubordinates');
      for(Integer i = 0; i < groups.size(); i++){
          roleToGroup.add(groups[i].Id);
          groupMap.put(groups[i].RelatedId, groups[i].Id);
      }
      List<Id> userOrGroupIds = new List<Id>();
      userOrGroupIds.addAll(roleToGroup);

      List<AccountShare> accountShare = [select Id, UserOrGroupId, AccountId, AccountAccessLevel, CaseAccessLevel, ContactAccessLevel, OpportunityAccessLevel from AccountShare where RowCause = 'Manual' and AccountId in : allSellThroughPartners and UserOrGroupId in :userOrGroupIds];
      Map<Id, Map<Id, AccountShare>> accountShareMap = new Map<Id, Map<Id, AccountShare>>();
      for(Integer i = 0; i< accountShare.size(); i++){
          if(!accountShareMap.containsKey(accountshare[i].AccountId)){
              Map<Id, AccountShare> sharingmap = new Map<Id, Accountshare>();
              sharingmap.put(accountShare[i].UserOrGroupId, accountshare[i]);
              accountShareMap.put(accountshare[i].AccountId,sharingmap);
          }else{
              Map<Id, AccountShare> sharingmap = accountShareMap.get(accountshare[i].AccountId);
              if(!sharingmap.containsKey(accountShare[i].UserOrGroupId)){
                sharingmap.put(accountShare[i].UserOrGroupId, accountShare[i]);
              }
          }
      }

      List<AccountShare> relatedListAddSharing = new List<AccountShare>();
      List<AccountShare> relatedListUpdateSharing = new List<AccountShare>();
      for(List<Opportunity> opportunities :accountOpportunityMap.values()){
        for(Integer i = 0; i<opportunities.size(); i++){
          Id partnerRolesId = accountToRole.get(opportunities[i].Sell_Through_Partner__c);
          Id userOrGroupId = groupMap.get(partnerRolesId);
          if(userOrGroupId != null){
            if(accountShareMap.containsKey(opportunities[i].AccountId)){
              Map<Id, AccountShare> shareMaptemp = accountShareMap.get(opportunities[i].AccountId);
              for(AccountShare share :shareMaptemp.values()){
                if(share.UserOrGroupId == userOrGroupId){
                  AccountShare uppdatedAccountSharing = updateSharing(share, 'read', 'None', 'None', 'None');
                  if(uppdatedAccountSharing != null){
                    relatedListUpdateSharing.add(uppdatedAccountSharing);  
                  }    
                }else{
                  relatedListAddSharing.add(createNewAccountShare(opportunities[i].AccountId, 'read', 'None', 'None', 'None', userOrGroupId));    
                }
              }
              
          }else{ 
              relatedListAddSharing.add(createNewAccountShare(opportunities[i].AccountId, 'read', 'None', 'None', 'None', userOrGroupId));
            }
          }
          
        }
      }

      insert relatedListAddSharing;
      update relatedListUpdateSharing;
    }

    public static AccountShare createNewAccountShare(Id accountId, String accessLevel, String caseAccess, String opportunityAccess, String contactAccess, Id userOrGroupId)
    {
      System.debug('-----createNewAccountShare----- for' + accountId);
        AccountShare accountShare = new AccountShare();

        accountShare.AccountId = accountId;
        accountShare.AccountAccessLevel = accessLevel;
        accountShare.CaseAccessLevel = caseAccess;
        accountShare.ContactAccessLevel = contactAccess;
        accountShare.OpportunityAccessLevel = opportunityAccess;
        accountShare.UserOrGroupId = userOrGroupId;

        return accountShare;
    }

    public static void accountRemoveManualSharing(List<Id> accountIDs)
    {
        Integer BATCHSIZE = 200;
        List<AccountShare> accountShareIDs = new List<AccountShare>();

        if (accountIDs == null || accountIDs.size() == 0)
        {
            return;
        }
        // Split requests down to a 200 a time. SOQL query string may
        // only be 10000 characters long
        for (Integer i = 0; i < (accountIDs.Size() / BATCHSIZE) + 1; i++)
        {
            List<Id> tempList;
            if (accountIDs.size() > ((i + 1) * BATCHSIZE))
            {
                tempList = ApexSharingRules.getRange(accountIDs, i * BATCHSIZE, BATCHSIZE);
            }
            else
            {
                tempList = ApexSharingRules.getRange(accountIDs, i * BATCHSIZE, accountIDs.size() - (i * BATCHSIZE));
            }

            List<Accountshare> temp = [select Id from AccountShare where RowCause = 'Manual' and AccountId in :tempList];
            
            accountShareIDs.addAll(temp);
        }

        if (accountShareIDs.size() > 0)
        {
          System.debug('Delete accountsharing ' + accountShareIDs);
          delete accountShareIDs;
        }
    }
}