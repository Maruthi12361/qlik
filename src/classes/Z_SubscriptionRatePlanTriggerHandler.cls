/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Linus Löfberg
* @version        1.0
* @created        2019-10-17
* @modified       2019-10-17
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
*
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class Z_SubscriptionRatePlanTriggerHandler {

    private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;

    public Z_SubscriptionRatePlanTriggerHandler(boolean isExecuting, integer size) {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void OnBeforeInsert(Zuora__SubscriptionRatePlan__c[] newRecords) {

    }

    public void OnAfterInsert(Zuora__SubscriptionRatePlan__c[] newRecords) {
        Map<String, Zuora__SubscriptionRatePlan__c> licenseRatePlanMap = new Map<String, Zuora__SubscriptionRatePlan__c>();

        for (Zuora__SubscriptionRatePlan__c srp : newRecords) {
            if (srp.LicenseKey__c != null) {
                licenseRatePlanMap.put(srp.LicenseKey__c, srp);
            }
        }

        List<Account_License__c> accountLicenses = [SELECT Id, Name, Subscription__c FROM Account_License__c WHERE Name IN :licenseRatePlanMap.KeySet()];
        for (Account_License__c al : accountLicenses) {
            if (licenseRatePlanMap.containsKey(al.Name)) {
                al.Subscription__c = licenseRatePlanMap.get(al.Name).Zuora__Subscription__c;
            }
        }
        update accountLicenses;
    }

    // public void OnBeforeUpdate(Zuora__Subscription__c[] oldRecords, Zuora__Subscription__c[] updatedRecords, Map<ID, Zuora__Subscription__c> recordMap) {
    //
    // }
    //
    // public void OnAfterUpdate(Zuora__Subscription__c[] oldRecords, Zuora__Subscription__c[] updatedRecords, Map<ID, Zuora__Subscription__c> recordMap) {
    //
    // }
}