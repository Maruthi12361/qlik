/**************************************************
*
* Class tested: CaseAssignedToChangedHandler
* Change Log:
*
* 2018-05-10    ext_bad     CHG0032643
*
**************************************************/
@isTest
public with sharing class CaseAssignedToChangedTest {
    private static testMethod void testEmail(){
        User current = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        User notCurrent = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() AND IsActive = TRUE LIMIT 1];

        Account testAct = new Account(
                Name = 'Test Account',
                Navision_Status__c = 'Something',
                QlikTech_Company__c = 'QlikTech Nordic AB'
        );
        insert testAct;

        Contact ctct = new Contact(
                Email = 'testEmail@gmail.com',
                FirstName = 'TestFN',
                LastName = 'TestLN',
                AccountId = testAct.Id,
                LeadSource = 'WEB - Web Activity'
        );
        insert ctct;

        Case c1 = new Case(
                Status = 'Closed',
                Subject = 'Subject',
                AccountId = testAct.Id,
                ContactId = ctct.Id,
                Assigned_To__c = notCurrent.Id
        );

        insert c1;

        c1 = [select Id, Assigned_To__c from Case where Id = : c1.Id];
        c1.Assigned_To__c = current.Id;

        Test.startTest();
        Semaphores.CustomCaseTriggerAfterUpdate = false;
        update c1;
        Test.stopTest();
    }

}