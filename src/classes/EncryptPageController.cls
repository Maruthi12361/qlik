public with sharing class EncryptPageController {

public string valueOne { get; set; }
public String subId{ get; set; }

public EncryptPageController(){
subId =apexpages.currentpage().getparameters().get('subid');
}

public PageReference iWantMyJSValues() {
valueOne = Apexpages.currentPage().getParameters().get('one');
List<String> encStr = valueOne.split(',');
system.debug('valueOne'+valueOne.length());
system.debug('valueOne1'+encStr[1].length());
system.debug('valueOne2'+encStr[0]);
system.debug('valueOne3'+encStr[1]);
Subsidiary__c subs = new Subsidiary__c();
subs= [select id,Base64SignatureImage__c  from subsidiary__c where id =:subId];
subs.Base64SignatureImage__c = encStr[1];
update subs;
PageReference ref= new PageReference('/'+subId);
    ref.setredirect(true);
    return ref;
}
}