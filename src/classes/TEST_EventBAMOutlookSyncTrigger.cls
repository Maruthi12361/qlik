/*********************************************
TEST_EventBAMOutlookSyncTrigger
Description: Test trigger on Event called EventBAMOutlookSync.trigger

Log History:
2012-05-09		RDZ		Initial Development
07.02.2017   RVA :   changing methods
**********************************************/

@isTest
public class TEST_EventBAMOutlookSyncTrigger {
	
	/******************************************************

		testEventBAMOutlookSyncEventType_EV_PreSales_Activity
	
		This method tests if the Event on Creation and Update for EventType = 'EV-PreSales Activity'.
		Set the WhatId, WhoId and WhatEntityName properly.
		
		Changelog:
			2012-05-09	RDZ		Created method
			07.02.2017   RVA :   changing methods 	
	******************************************************/	
	
	static testMethod void testEventBAMOutlookSyncEventType_EV_PreSales_Activity() {

		QTTestUtils.GlobalSetup();
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c Q = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'GBR',
            QlikTech_Company_Name__c = 'United Kingdoms',
			Subsidiary__c = testSubs1.id
			);
        insert Q;
		*/
		QlikTech_Company__c Q = QTTestUtils.createMockQTCompany('United Kingdoms', 'GBR', 'United States');
			Q.Country_Name__c = 'GBR';
		update Q;
		//QlikTech_Company__c Q = [SELECT Id FROM QlikTech_Company__c WHERE Name = 'GBR'];
        

		Account testAcc = new Account (
		Name= 'RDZ Test 9273',
		Billing_Country_Code__c = Q.Id, 
		Navision_Customer_Number__c = '12345')	;	

		insert testAcc;
		testAcc = [select Id, Name, Billing_Country_Code__c, Navision_Customer_Number__c, OwnerId from Account where Name = 'RDZ Test 9273' LIMIT 1];		
		
		
		// Create a new Event object
		Event e = new Event(WhatId__c=testAcc.Id
							,Type = 'EV - Presales Activity'
							,DurationInMinutes=10
							,ActivityDateTime=Datetime.now());
		
		upsert e;
		
		for(Event ev : [select WhatEntityName__c, WhatId__c, WhoId__c, what.Id, what.Type, Type, WhatId, WhoId from Event where WhatId = :testAcc.Id])
		{
			System.AssertEquals(ev.what.Type, ev.WhatEntityName__c);
			System.AssertEquals(ev.WhatId__c, ev.WhatId);
			System.AssertEquals(ev.WhoId__c, ev.WhoId);
		}
	}
	
	/******************************************************

		testEventBAMOutlookSyncOtherEventType
	
		This method tests if the Event on Creation and Update for other Event Types (!= 'EV-PreSales Activity').
		Values for WhatId__c, WhoId__c and WhatEntityName__c should be null.
		
		Changelog:
			2012-05-09	RDZ		Created method
				
	******************************************************/	
	static testMethod void testEventBAMOutlookSyncOtherEventType() {

		QTTestUtils.GlobalSetup();
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c Q = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'GBR',
            QlikTech_Company_Name__c = 'United Kingdoms',
			Subsidiary__c = testSubs1.id
			);
        insert Q;
		*/
		QlikTech_Company__c Q = QTTestUtils.createMockQTCompany('United Kingdoms', 'GBR', 'United States');
			Q.Country_Name__c = 'GBR';
		update Q;
		//QlikTech_Company__c Q = [SELECT Id FROM QlikTech_Company__c WHERE Name = 'GBR'];
        
		Account testAcc = new Account (
		Name= 'RDZ Test 9273',
		Billing_Country_Code__c = Q.Id, 
		Navision_Customer_Number__c = '12345')	;	

		insert testAcc;
		testAcc = [select Id, Name, Billing_Country_Code__c, Navision_Customer_Number__c from Account where Name = 'RDZ Test 9273' LIMIT 1];		
		
		// Create a new Event object
		Event e = new Event(WhatId__c=testAcc.Id
							,Type = 'Other'
							,DurationInMinutes=10
							,ActivityDateTime=Datetime.now());
		
		insert e;
		
		System.AssertEquals(null, e.WhatId);
		System.AssertEquals(null, e.WhoId);
		System.AssertEquals(null, e.WhatEntityName__c);
	}
	
	/******************************************************

		testEventBAMOutlookSyncNullWhatId
	
		This method tests if the Event on Creation and Update when WhatId is null and Type (!= 'EV-PreSales Activity').
		Values for WhatId__c, WhoId__c and WhatEntityName__c should be null.
		
		Changelog:
			2012-05-09	RDZ		Created method
				
	******************************************************/	
	static testMethod void testEventBAMOutlookSyncNullWhatId() {


		QTTestUtils.GlobalSetup();
		/*
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
		QlikTech_Company__c Q = new QlikTech_Company__c(
            Name = 'GBR',
            Country_Name__c = 'GBR',
            QlikTech_Company_Name__c = 'United Kingdoms',
			Subsidiary__c = testSubs1.id
			);
        insert Q;
		*/
		QlikTech_Company__c Q = QTTestUtils.createMockQTCompany('United Kingdoms', 'GBR', 'United States');
			Q.Country_Name__c = 'GBR';
		update Q;
		//QlikTech_Company__c Q = [SELECT Id FROM QlikTech_Company__c WHERE Name = 'GBR'];
        
		Account testAcc = new Account (
		Name= 'RDZ Test 9273',
		Billing_Country_Code__c = Q.Id, 
		Navision_Customer_Number__c = '12345')	;	

		insert testAcc;
		testAcc = [select Id, Name, Billing_Country_Code__c, Navision_Customer_Number__c from Account where Name = 'RDZ Test 9273' LIMIT 1];		
		
		// Create a new Event object
		Event e = new Event(WhatId__c=testAcc.Id
							,Type = 'EV - Presales Activity'
							,DurationInMinutes=10
							,ActivityDateTime=Datetime.now()
							);
		
		insert e;
		
		e.WhatId__c = null;
		update e;
		
		System.AssertEquals(null, e.WhatId);
		System.AssertEquals(null, e.WhoId);
		System.AssertEquals(null, e.WhatEntityName__c);
	}
}