/*
* File RefreshApp
    * @description : Performs Sandbox Refresh activity.Logic moved from Sandbox Update Tab
                     It will be executed after QA refresh in prod.
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification
    1       20.01.2018   Pramod Kumar V      Created Class
    2       28.04.2018   Pramod Kumar v      ITRM-60
    3       25.07.2018   ext_bjd             QAR-64 Added runUpdate method for asynchronous invoke other methods
    4       06.03.2019   ext_bjd             ITRM-320 Added method getUrlForInstance to determine instance url and update
    *       custom settings Support_Portal_Login_Url__c and Support_Portal_Url__c dynamically
  */

public class UpdateCustomSettings {
    @TestVisible
    private static final String URL_PART_HTTPS = 'https://';
    @TestVisible
    private static final String URL_PART_QLIKSUPPORT = 'qliksupport';
    @TestVisible
    private static final String URL_PART_QLIKID = 'qlikid';
    @TestVisible
    private static final String URL_PART_QLIK_COM = 'qlik.com/portal/support?relaystate=';
    @TestVisible
    private static final String URL_PART_FORCE_COM = 'force.com';

    @Future
    public static void runUpdate(String SandboxName) {
        UpdateCustomSettings cs = new UpdateCustomSettings();
        RefreshSandboxData__mdt RefSandBox = cs.getRefreshSandboxDataBySandboxName(sandboxName);
        cs.UpdateCustomSettingsfromMdt(RefSandBox);
    }

    public RefreshSandboxData__mdt getRefreshSandboxDataBySandboxName(String SandboxName) {
        String defaultKeyValue = 'DEFAULTVALUES';
        String gSandboxName = SandboxName;
        Boolean isSandbox = PostRefreshHandler.isSandboxFinder();
        System.debug('sandboxName ' + gSandboxName);
        RefreshSandboxData__mdt rs = null;
        Map<String, RefreshSandboxData__mdt> RefreshSandboxItem = new Map<String, RefreshSandboxData__mdt>();
        for (RefreshSandboxData__mdt item : [
                SELECT Id, MasterLabel, Support_Portal_Live_Agent_API_Endpoint__c, Support_Portal_Login_Page_Url__c,
                        Support_Portal_Login_Url__c, Support_Portal_Url__c, Support_Portal_Url_Base__c, BoomiToken__c,
                        Support_Portal_Logout_Page_Url__c, Support_Portal_CSS_Base__c, Support_Portal_index_allow_options__c,
                        SpringAccountPrefix__c, CMInstanceUrl__c, QlikMarket_Website__c, BoomiBaseURL__c, NSQuoteApprovals__c, ULC_Base_URL__c
                FROM RefreshSandboxData__mdt
        ]) {
            RefreshSandboxItem.put(item.MasterLabel.toUppercase(), item);
        }
        if (!isSandbox && Test.isRunningTest()) {
            rs = getProdCustomSettingSandboxData();
        } else if (RefreshSandboxItem.containsKey(gSandboxName.toUppercase())) {
            System.debug('!!!! sand ' + RefreshSandboxItem.get(gSandboxName));
            rs = RefreshSandboxItem.get(gSandboxName.toUppercase());
        } else if (RefreshSandboxItem.containsKey(defaultKeyValue)) {
            System.debug('!!!! sand ' + RefreshSandboxItem.get(defaultKeyValue));
            rs = RefreshSandboxItem.get(defaultKeyValue);
        }
        return rs;
    }

    public void UpdateCustomSettingsfromMdt(RefreshSandboxData__mdt rs) {
        QTCustomSettings__c Qtsetting = QTCustomSettings__c.getOrgDefaults();
        Steelbrick_Settings__c SBsetting = Steelbrick_Settings__c.getOrgDefaults();
        QS_Partner_Portal_Urls__c QSsetting = QS_Partner_Portal_Urls__c.getOrgDefaults();
        QVM_Settings__c qvmSetting = QVM_Settings__c.getOrgDefaults();
        List<String> urlsInstance = getUrlForInstance();

        if (rs != null) {
            Qtsetting.ULC_Base_URL__c = rs.ULC_Base_URL__c;
            Qtsetting.BoomiBaseURL__c = rs.BoomiBaseURL__c;
            Qtsetting.BoomiToken__c = rs.BoomiToken__c;
            SBsetting.BoomiBaseURL__c = rs.BoomiBaseURL__c;
            SBsetting.BoomiToken__c = rs.BoomiToken__c;

            qvmSetting.QlikMarket_Website__c = rs.QlikMarket_Website__c;
            SBsetting.SpringAccountPrefix__c = rs.SpringAccountPrefix__c;
            SBsetting.CMInstanceUrl__c = rs.CMInstanceUrl__c;

            QSsetting.Support_Portal_CSS_Base__c = rs.Support_Portal_CSS_Base__c;
            QSsetting.Support_Portal_index_allow_options__c = rs.Support_Portal_index_allow_options__c;
            QSsetting.Support_Portal_Live_Agent_API_Endpoint__c = rs.Support_Portal_Live_Agent_API_Endpoint__c;
            QSsetting.Support_Portal_Login_Page_Url__c = rs.Support_Portal_Login_Page_Url__c;
            QSsetting.Support_Portal_Login_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM : rs.Support_Portal_Login_Url__c;
            QSsetting.Support_Portal_Logout_Page_Url__c = rs.Support_Portal_Logout_Page_Url__c;
            QSsetting.Support_Portal_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + URL_PART_QLIKID + '-' + urlsInstance[0]+ '.' + URL_PART_QLIK_COM + URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM : rs.Support_Portal_Url__c;
            QSsetting.Support_Portal_Url_Base__c = rs.Support_Portal_Url_Base__c;

            update Qtsetting;
            update SBsetting;
            update qvmSetting;
            update QSsetting;
        }
    }

    @TestVisible
    private static List<String> getUrlForInstance() {
        // Logic with possible options:
        // 1) cs80.salesforce.com               -- 3 parts, Instance is 1st part, name not determinable
        // 2) qlik--qa.cs107.my.salesforce.com  -- 5 parts, Instance is 2nd part, name is 1nd part
        // 3) qlik.my.salesforce.com            -- 4 parts, Instance and name is not determinable
        List<String> instances = new List<String>();
        List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').replace('qlik--','').split('\\.');

        if (parts.size() == 3) {
            instances.add(null);
        } else if (parts.size() == 5) {
            instances.add(parts[0]);
            instances.add(parts[1]);
        } else {
            instances.add(null);
        }
        return instances;
    }

    @TestVisible
    private static RefreshSandboxData__mdt getProdCustomSettingSandboxData(){
        QTCustomSettings__c Qtsetting = QTCustomSettings__c.getOrgDefaults();
        Steelbrick_Settings__c SBsetting = Steelbrick_Settings__c.getOrgDefaults();
        QS_Partner_Portal_Urls__c QSsetting = QS_Partner_Portal_Urls__c.getOrgDefaults();
        QVM_Settings__c qvmSetting = QVM_Settings__c.getOrgDefaults();
        List<String> urlsInstance = getUrlForInstance();

        RefreshSandboxData__mdt rs = new RefreshSandboxData__mdt();

        rs.ULC_Base_URL__c = Qtsetting.ULC_Base_URL__c;
        rs.BoomiBaseURL__c = Qtsetting.BoomiBaseURL__c;
        rs.BoomiBaseURL__c = SBsetting.BoomiBaseURL__c;

        rs.QlikMarket_Website__c = qvmSetting.QlikMarket_Website__c;
        rs.SpringAccountPrefix__c = SBsetting.SpringAccountPrefix__c;
        rs.CMInstanceUrl__c = SBsetting.CMInstanceUrl__c;

        rs.Support_Portal_CSS_Base__c = QSsetting.Support_Portal_CSS_Base__c;
        rs.Support_Portal_Live_Agent_API_Endpoint__c = QSsetting.Support_Portal_Live_Agent_API_Endpoint__c;
        rs.Support_Portal_Login_Page_Url__c = QSsetting.Support_Portal_Login_Page_Url__c;
        rs.Support_Portal_Login_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM : rs.Support_Portal_Login_Url__c;
        rs.Support_Portal_Logout_Page_Url__c = QSsetting.Support_Portal_Logout_Page_Url__c;
        rs.Support_Portal_Url__c = (urlsInstance.size() > 1) ? URL_PART_HTTPS + URL_PART_QLIKID + '-' + urlsInstance[0] + '.' + URL_PART_QLIK_COM + URL_PART_HTTPS + urlsInstance[0] + '-' + URL_PART_QLIKSUPPORT + '.' + urlsInstance[1] + '.' + URL_PART_FORCE_COM : rs.Support_Portal_Url__c;
        rs.Support_Portal_Url_Base__c = QSsetting.Support_Portal_Url_Base__c;

        return rs;
    }
}
