/*
* File CopyAssignmentsBatchTest
    * @description : Unit test for CopyAssignmentsBatch 
    * @purpose : Test class coverage
    * @author : Pramod Kumar V
    * Modification Log ===============================================================
    Ver     Date         Author              Modification 
    1       04.01.2018    Pramod Kumar V     Created Class

*/
@isTest(seeAlldata=false)
private class CopyAssignmentsBatchTest{
    
   
    static testMethod void test_CopyAssignmentsBatch() {
    CopyAssignmentsBatch ca=new CopyAssignmentsBatch ();
    Database.executeBatch(ca);
    }
  }