/****************************************************************
*
*  OpportunityMarketTypeHandler
*
*  2016-05-19 TJG : Migrated from the following trigger
					OpportunityMarketType 
*  2017-06-22 MTM  QCW-2711 remove sharing option
*  2019-12-05 FEH: Updated to only query accounts once
*****************************************************************/
public class OpportunityMarketTypeHandler {

    
    private static List<Account> opportunityAccounts;
    private static List<Account> getOpportunityAccounts(List<Id> accIds) {
        if(opportunityAccounts == null || Test.isRunningTest()) {
          opportunityAccounts = [Select Id, Owner.UserRole.Name, Named_Account__c, Segment_New__c, Billling_Country_Code_Name__c FROM Account WHERE Id In :accIDs];
        }
        return opportunityAccounts; 
    }  
    
	public static void handle(List<Opportunity> triggerNew)
	{
	    //Start by getting a list of the Account Id's for each Opportunity
	    List<string> OppIDs = new List<string>();
	    for (Opportunity Opp : triggerNew)
	    {
	        OppIDs.Add(Opp.AccountId);  //Add the Account Id    
	    }
	    //Create and populate a Map with the Account information we will need   
	    Map<Id, Account> MapOfAcc = New Map<Id, Account>(getOpportunityAccounts(OppIDs));
	    
	    // EDIT 19.09.2012 : Exclude Academic Program Opportunity records from this trigger, Id from Custom Setting
	    Id academidProgramOpportunityId;
	    if(Academic_Program_Settings__c.getInstance('AcademicProgramSetting') != null) {
	        academidProgramOpportunityId = Academic_Program_Settings__c.getInstance('AcademicProgramSetting').OpportunityRecordTypeId__c;
	    }
	    
	    //Loop through each Opp updating the Market Type field as we go
	    for (Opportunity newOpp : triggerNew)
	    {
	        if(academidProgramOpportunityId != null && newOpp.RecordTypeId == academidProgramOpportunityId){
	            continue;
	        }
	        //Check record type for CR# 4268
	        QTCustomSettings__c MySetting = QTCustomSettings__c.getValues('Default');
	        if (MySetting == null)
	        {
	            continue;               
	        }   
	        String OEMRecordType = MySetting.OEM_Record_Types__c;
	        
	        //OEM - Order, OEM - Run Rate, OEM - Standard Sales Process, OEM Recruitment (CCS), OEM Prepay
	        if (OEMRecordType.contains(newOpp.RecordTypeId))    
	        {
	            newOpp.Market_Type__c = 'OEM';
	        }
	        else
	        {   
	            if (!MapOfAcc.containsKey(newOpp.AccountId))
	            {
	                continue;
	            }
	            // fetch the Account info from the map using the Opp account id 
	            Account Acc = MapOfAcc.get(newOpp.AccountId);
	            //More checking to do.
	            System.debug('Checking Named Account');
	            System.debug('newOpp.Account.Id' + Acc.Id);
	            System.debug('newOpp.Account.Named_Account__c=' + Acc.Named_Account__c);
	            // We will set the Market Type field according to the requested conditions from the CR3998
	            // If 'Named Account' = true then set 'Market Type' to 'Named Account'
	            if (Acc.Named_Account__c)
	            {
	                System.debug('newOpp.Account.Named_Account__c=' + newOpp.Account.Named_Account__c);
	                newOpp.Market_Type__c = 'Named Account';
	            }
	            else
	            { 
	                if (Acc.Segment_New__c == null || Acc.Segment_New__c == '' || Acc.Segment_New__c.contains('Enterprise')) //added 2013-03-08 as per Stuart request                
	                {
	                    newOpp.Market_Type__c = 'Mid-Market';
	                }
	                else if (Acc.Segment_New__c.contains('Small Business')) //CCE 2013-02-01    CR# 7268                
	                {
	                    newOpp.Market_Type__c = 'Lower SMB';
	                }
	                else
	                {
	                    newOpp.Market_Type__c = Acc.Segment_New__c;
	                }               
	            }
	        }
	    }
	}
}