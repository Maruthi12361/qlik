/***************************************
Class: CampaignMemberHelper
Description: Will contain methods to help to update CampaignMembers and Test Methods

Changes Log:
2012-01-01	RDZ		Initial code: https://eu1.salesforce.com/a3RD0000000L5M1
2012-02-29	RDZ		Fixing bug https://eu1.salesforce.com/a3RD0000000L5M1
2012-07-27  RDZ		CR# 4327 https://eu1.salesforce.com/a0CD000000G1NjL
					To Clear up values from Lead/Contacts when they are stored in the campaign member
2012-10-10  TJG		Minor adjustments. Added local set in checkAndCleanValues to prevent duplicates
					and a local Boolean variable in CheckAndUpdateSObjectAndCampaingMember 

2013-05-22	YKA		CR# 8148: https://eu1.salesforce.com/a0CD000000YHuFd
					(Fix CampaignMemberStatus_Trigger)
					Changed code of methods	setupCampaignMember and checkAndCleanValues to deal with bulk records 

2015-08-13	Linus Löfberg @ 4front		Moved test to new class (TestCampaignMemberStatus_Trigger)
2018-05-30  CCE  CHG0033789 BMW-751 Add new Source URL field with larger character limit			
2018-09-28  CCE  CHG0034670 BMW-1008 Do not clear field "CM_Source_URL_NEW__c" on Lead and Contact	
2019-04-17  CRW/CCE CHG0035768 BMW-1404 Event flow model for Conversica 		
****************************************/

public with sharing class CampaignMemberHelper {
	
	public static void CopyValuesToCampaignMember(CampaignMember cm,  SObject objDetail)
	{
		if (objDetail instanceof Contact)
		{
			System.Debug('CM in setup:((Contact) objDetail).Source_ID2__c' + ((Contact) objDetail).Source_ID2__c);
			cm.CM_Source_ID2__c =  ((Contact) objDetail).Source_ID2__c;
			cm.CM_Source_Partner__c =  ((Contact) objDetail).Partner_Source__c;
			cm.CM_Source_URL_NEW__c =  ((Contact) objDetail).Source_URL_NEW__c;
			cm.CM_Source_Site__c =  ((Contact) objDetail).Source_Site__c;
			cm.CM_Keywords__c =  ((Contact) objDetail).Keywords__c;
			
		}
		if (objDetail instanceof Lead)
		{
			System.Debug('CM in setup:((Lead) objDetail).Source_ID2__c' + ((Lead) objDetail).Source_ID2__c);
			cm.CM_Source_ID2__c =  ((Lead) objDetail).Source_ID2__c;
			cm.CM_Source_Partner__c =  ((Lead) objDetail).Partner_Source__c;
			cm.CM_Source_URL_NEW__c =  ((Lead) objDetail).Source_URL_NEW__c;
			cm.CM_Source_Site__c =  ((Lead) objDetail).Source_Site__c;
			cm.CM_Keywords__c =  ((Lead) objDetail).Keywords__c;
			cm.New_Lead__c = ((Lead) objDetail).Lead_Age__c < 1?true:false ;
		}
		
		if (cm.Number_Of_Form_Submissions__c == null)
		{
			cm.Number_Of_Form_Submissions__c = 0;
		}
		cm.Number_Of_Form_Submissions__c = cm.Number_Of_Form_Submissions__c + 1;
		System.Debug('CM in setup: ' + cm);
		
	}
	
	public static void BlankValuesInSObject(SObject objDetail, List<SObject> ObjToUpdate )
	{
		Lead cloneLead;
		Contact cloneContact;
		if (objDetail instanceof Lead)
		{
			//Blank values in Contacts
			cloneLead = ((Lead)objDetail).clone();
			cloneLead.Source_ID2__c = null;
			cloneLead.Partner_Source__c = '';
			//cloneLead.Source_URL_NEW__c = '';
			cloneLead.Source_Site__c = '';
			cloneLead.Keywords__c = '';
			
			//Add Contact/Lead to update list
			ObjToUpdate.add(cloneLead);
		}
		if (objDetail instanceof Contact)
		{
			//Blank values in Contacts
			cloneContact = ((Contact)objDetail).clone();
			cloneContact.Source_ID2__c = null;
			cloneContact.Partner_Source__c = '';
			//cloneContact.Source_URL_NEW__c = '';
			cloneContact.Source_Site__c = '';
			cloneContact.Keywords__c = '';
			
			//Add Contact/Lead to update list
			ObjToUpdate.add(cloneContact);
		}
				
	}

	public static void setupCampaignMember(List<CampaignMember> cmList)
	{
		List<SObject> ObjToUpdate = new List<SObject>();
		List<Id> LeadIds = new List<Id>();
		List<Id> ContIds = new List<Id>();
		// Map of Contact Id, Campaign Member 
		Map<Id, CampaignMember> mapCIDCm = new Map<Id, CampaignMember>();
		// Map of Lead Id, Campaign Member 
		Map<Id, CampaignMember> mapLIDCm = new Map<Id, CampaignMember>();
			
		for(CampaignMember cm : cmList)
		{			
			if (cm.ContactId != null)
			{
				ContIds.add(cm.ContactId);	
				mapCIDCm.put(cm.ContactId, cm);			
			}
			else if(cm.leadId != null)
			{
				LeadIds.add(cm.leadId);
				mapLIDCm.put(cm.leadId, cm);
			}			
		}
		
		if (ContIds != null)
		{	
			for (Contact objDetail:[Select Id, Original_Source_ID2__c, Source_ID2__c, Partner_Source__c, Source_URL_NEW__c, Source_Site__c, Keywords__c from Contact where Id in :ContIds])
	    	{	    		
	    		CopyValuesToCampaignMember(mapCIDCm.get(objDetail.Id), objDetail);
				BlankValuesInSObject(objDetail,ObjToUpdate);							
			}
		}
		if(LeadIds != null)
		{
			for (Lead objDetail:[Select Id, Original_Source_ID2__c, Source_ID2__c, Partner_Source__c, Source_URL_NEW__c, Source_Site__c, Keywords__c, Lead_Age__c from Lead where Id in :LeadIds])
	    	{
	    		CopyValuesToCampaignMember(mapLIDCm.get(objDetail.Id), objDetail);
				BlankValuesInSObject(objDetail,ObjToUpdate);	
			}
		}				
		
		if (ObjToUpdate.size() > 0)
		{
			try
			{
				update(ObjToUpdate);	
			}
			catch(System.Exception expep)
			{				
				//ObjToUpdate[0].addError('Unbale to update Lead/Contact');				
			}
						
		}
	}
	
	
	public static void checkAndCleanValues(List<CampaignMember> cmList)
	{
		List<SObject> ObjToUpdate = new List<SObject>();
		// local set to prevent duplicate leads from being added to the list
		Set<SObject> MyObjSet = new Set<SObject>();
				
		List<Id> LeadIds = new List<Id>();
		List<Id> ContIds = new List<Id>();
		// Map of Contact Id, Campaign Member 
		Map<Id, CampaignMember> mapCIDCm = new Map<Id, CampaignMember>();
		// Map of Lead Id, Campaign Member 
		Map<Id, CampaignMember> mapLIDCm = new Map<Id, CampaignMember>();
			
		for(CampaignMember cm : cmList)
		{			
			if (cm.ContactId != null)
			{
				ContIds.add(cm.ContactId);	
				mapCIDCm.put(cm.ContactId, cm);			
			}
			else if(cm.leadId != null)
			{
				LeadIds.add(cm.leadId);
				mapLIDCm.put(cm.leadId, cm);
			}			
		}
		
		if (ContIds != null && ContIds.size()>0)
		{	
			for (Contact objDetail:[Select Id, Original_Source_ID2__c, Source_ID2__c, Partner_Source__c, Source_URL_NEW__c, Source_Site__c, Keywords__c from Contact where Id in :ContIds])
		    	{
		    		if (!MyObjSet.contains(objDetail)) {
		    			MyobjSet.add(objDetail);
			    		//Check if its necesary to clear values from contact
			    		CheckAndUpdateSObjectAndCampaingMember(objDetail, mapCIDCm.get(objDetail.Id), ObjToUpdate);		    			
		    		}
				}
		}
		
		if(LeadIds != null && LeadIds.size() >0)
		{
			for (Lead objDetail:[Select Id, Original_Source_ID2__c, Source_ID2__c, Partner_Source__c, Source_URL_NEW__c, Source_Site__c, Keywords__c, Lead_Age__c from Lead where Id in :LeadIds])
	    	{
	    		if (!MyObjSet.contains(objDetail)) {
	    			MyobjSet.add(objDetail);
		    		//Check if its necesary to clear values from Lead
		    		CheckAndUpdateSObjectAndCampaingMember(objDetail, mapLIDCm.get(objDetail.Id), ObjToUpdate);
	    		}
			}
		}	
		System.Debug('Updating Lead/contacts: ' + ObjToUpdate.size());		
		if (ObjToUpdate!= null && ObjToUpdate.size() > 0)
		{
			try
			{
				update(ObjToUpdate);	
			}
			catch(System.Exception expep)
			{				
				//ObjToUpdate[0].addError('Unbale to update Lead/Contact');				
			}						
		}
		
	}



	public static void CheckAndUpdateSObjectAndCampaingMember(SObject objDetail, CampaignMember cm, List<SObject> ObjToUpdate )
	{
		Lead cloneLead;
		Contact cloneContact;
		Boolean needed = false;
		if (objDetail instanceof Lead)
		{
			//Blank values in Contacts if the value is the same as in CampaignMember
			cloneLead = ((Lead)objDetail).clone();
			System.debug('!!!!!!!!!!!Are same values Campaign Member and Lead fields???!!!!!!!!!!!!!!!!!!!!');
			System.debug('cloneLead.Source_ID2__c  == cm.CM_Source_ID2__c ' + cloneLead.Source_ID2__c +  ' == ' + cm.CM_Source_ID2__c);
			System.debug('cloneLead.Partner_Source__c == cm.CM_Source_Partner__c ' + cloneLead.Partner_Source__c + ' == ' + cm.CM_Source_Partner__c);
			System.debug('cloneLead.Source_URL_NEW__c  == cm.CM_Source_URL_NEW__c ' + cloneLead.Source_URL_NEW__c + ' == ' +cm.CM_Source_URL_NEW__c);
			System.debug('cloneLead.Source_Site__c  == cm.CM_Source_Site__c ' + cloneLead.Source_Site__c + ' == ' + cm.CM_Source_Site__c);
			System.debug('cloneLead.Keywords__c  == cm.CM_Keywords__c ' + cloneLead.Source_ID2__c +' == ' + cm.CM_Source_ID2__c);
			System.debug('!!!!!!!!!!!END!!!!!!!!!!!!!!!!!!!!');
			if (cloneLead.Source_ID2__c == cm.CM_Source_ID2__c)
			{
				needed = true;
				cloneLead.Source_ID2__c = null;
			}
			if (cloneLead.Partner_Source__c == cm.CM_Source_Partner__c)
			{
				needed = true;
				cloneLead.Partner_Source__c = '';
			}
			
			if (cloneLead.Source_URL_NEW__c == cm.CM_Source_URL_NEW__c)
			{
				needed = true;
				cloneLead.Source_URL_NEW__c = '';
			}
			
			if (cloneLead.Source_Site__c == cm.CM_Source_Site__c)
			{
				needed = true;
				cloneLead.Source_Site__c = '';
			}
			
			if (cloneLead.Keywords__c == cm.CM_Keywords__c)
			{
				needed = true;
				cloneLead.Keywords__c = '';
			}
			
			//Add Contact/Lead to update list
			if (needed) {
				ObjToUpdate.add(cloneLead);
			}
		}
		
		if (objDetail instanceof Contact)
		{
			//Blank values in Contacts
			cloneContact = ((Contact)objDetail).clone();
			
			System.debug('!!!!!!!!!!!Are same values Campaign Member and Contact fields???!!!!!!!!!!!!!!!!!!!!');
			System.debug('cloneLead.Source_ID2__c  == cm.CM_Source_ID2__c ' + cloneContact.Source_ID2__c +  ' == ' + cm.CM_Source_ID2__c);
			System.debug('cloneLead.Partner_Source__c == cm.CM_Source_Partner__c ' + cloneContact.Partner_Source__c + ' == ' + cm.CM_Source_Partner__c);
			System.debug('cloneLead.Source_URL_NEW__c  == cm.CM_Source_URL_NEW__c ' + cloneContact.Source_URL_NEW__c + ' == ' +cm.CM_Source_URL_NEW__c);
			System.debug('cloneLead.Source_Site__c  == cm.CM_Source_Site__c ' + cloneContact.Source_Site__c + ' == ' + cm.CM_Source_Site__c);
			System.debug('cloneLead.Keywords__c  == cm.CM_Keywords__c ' + cloneContact.Source_ID2__c +' == ' + cm.CM_Source_ID2__c);
			System.debug('!!!!!!!!!!!END!!!!!!!!!!!!!!!!!!!!');
			
			if (cloneContact.Source_ID2__c == cm.CM_Source_ID2__c)
			{
				needed = true;
				cloneContact.Source_ID2__c = null;
			}
			if (cloneContact.Partner_Source__c == cm.CM_Source_Partner__c)
			{
				needed = true;
				cloneContact.Partner_Source__c = '';
			}
			
			if (cloneContact.Source_URL_NEW__c == cm.CM_Source_URL_NEW__c)
			{
				needed = true;
				cloneContact.Source_URL_NEW__c = '';
			}
			
			if (cloneContact.Source_Site__c == cm.CM_Source_Site__c)
			{
				needed = true;
				cloneContact.Source_Site__c = '';
			}
			
			if (cloneContact.Keywords__c == cm.CM_Keywords__c)
			{
				needed = true;
				cloneContact.Keywords__c = '';
			}
			
			//Add Contact/Lead to update list
			if (needed) {
				ObjToUpdate.add(cloneContact);
			}
		}		
	}

    //changes for conversica phase 2 BMW-1404
    public static void PopulateConversicaEventFieldsOnUpdate(List<CampaignMember> CMList,List<String> CMID){
        Map<Id, CampaignMember> mapLIDCm = new Map<Id, CampaignMember>();
        Map<Id, CampaignMember> mapCIDCm = new Map<Id, CampaignMember>();
        if (CMID.size()>0){
            for (CampaignMember CMToUpdate:[Select Id,FirstRespondedDate,Status,LeadId,ContactId,Lead.AVA__AVAAI_conversation_status__c,Contact.AVA__AVAAI_conversation_status__c,Lead.AVA__AVAAI_conversation_stage__c,Contact.AVA__AVAAI_conversation_stage__c,Campaign.Type,Campaign.Name,Campaign.Campaign_Sub_Type__c,Campaign.Publishable_Name__c From CampaignMember WHERE Id IN :CMID])
            {
                //Do not change the order of the following if else. 
                // We have seen that when convertng a Lead CampaignMember that after conversion the CampaignMember can end up with the LeadId and ContactId fields both populated.
                // To avoid problems we process the ContactId first and thus ignore the LeadId if there is one. If the Lead has not been converted then ContactId is not
                // populated so we only follow the LeadID path.
                system.debug('CampaignMemberHelper CMToUpdate = ' + CMToUpdate);
                if (String.isNotBlank(CMToUpdate.ContactId)){
                        if(!(CMToUpdate.Contact.AVA__AVAAI_conversation_stage__c == 'Messaging' || CMToUpdate.Contact.AVA__AVAAI_conversation_stage__c == 'Preparing Lead')) {
                    	mapCIDCm.put(CMToUpdate.ContactId,CMToUpdate);
                        system.debug('CampaignMemberHelper mapCIDCm = ' + mapCIDCm);
                    } 
                } else if (String.isNotBlank(CMToUpdate.LeadId)){
                    if(!(CMToUpdate.Lead.AVA__AVAAI_conversation_stage__c == 'Messaging' || CMToUpdate.Lead.AVA__AVAAI_conversation_stage__c == 'Preparing Lead')) {
                    mapLIDCm.put(CMToUpdate.LeadId,CMToUpdate);
                    system.debug('CampaignMemberHelper mapLIDCm = ' + mapLIDCm);
                    }
                }
            }
        }
        if (mapLIDCm.size()>0){
        	List<Lead> leadToUpdate = new List<Lead>([Select Id,AVA__AVAAI_conversation_status__c,Latest_Event_Campaign_Response_Date__c,Latest_Event_Campaign_Member_Status__c,Latest_Event_Campaign_Publishable_Name__c,Latest_Campaign_Interaction__c,Latest_Campaign_Sub_Type__c,Latest_Campaign_Type__c From Lead WHERE Id IN:mapLIDCm.keySet()]);
            system.debug('CampaignMemberHelper leadToUpdate = ' + leadToUpdate);
            for (Lead ltu : leadToUpdate){
                CampaignMember CM = mapLIDCm.get(ltu.Id);
                if(CM.FirstRespondedDate != null){ltu.Latest_Event_Campaign_Response_Date__c = Cm.FirstRespondedDate;}
                if(String.isNotBlank(CM.Status)){ltu.Latest_Event_Campaign_Member_Status__c=Cm.Status;}
                if(String.isNotBlank(CM.Campaign.Publishable_Name__c)){ltu.Latest_Event_Campaign_Publishable_Name__c=CM.Campaign.Publishable_Name__c;}
                if(String.isNotBlank(CM.Campaign.Name)){ltu.Latest_Campaign_Interaction__c = CM.Campaign.Name;}
                if(String.isNotBlank(CM.Campaign.Campaign_Sub_Type__c)){ltu.Latest_Campaign_Sub_Type__c = CM.Campaign.Campaign_Sub_Type__c;}
                if(String.isNotBlank(CM.Campaign.Type)){ltu.Latest_Campaign_Type__c=CM.Campaign.Type;}
            }
	        if (leadToUpdate.size()>0) { 
	        	update leadToUpdate;
	        	system.debug('CampaignMemberHelper leadToUpdate = ' + leadToUpdate);
	        }
        }
        if (mapCIDCm.size()>0){
            List<Contact> contactToUpdate = new List<Contact>([Select Id,AVA__AVAAI_conversation_status__c,Latest_Event_Campaign_Response_Date__c,Latest_Event_Campaign_Member_Status__c,Latest_Event_Campaign_Publishable_Name__c,Latest_Event_Campaign_Interaction__c,Latest_Event_Sub_Type__c,Latest_Event_Campaign_Type__c From Contact WHERE Id IN:mapCIDCm.keySet()]);
            system.debug('CampaignMemberHelper contactToUpdate = ' + contactToUpdate);
            for (Contact ctu : contactToUpdate)
            {
                CampaignMember CM = mapCIDCm.get(ctu.Id);
                if(CM.FirstRespondedDate != null){ctu.Latest_Event_Campaign_Response_Date__c = Cm.FirstRespondedDate;}
                if(String.isNotBlank(CM.Status)){ctu.Latest_Event_Campaign_Member_Status__c=Cm.Status;}
                if(String.isNotBlank(CM.Campaign.Publishable_Name__c)){ctu.Latest_Event_Campaign_Publishable_Name__c=CM.Campaign.Publishable_Name__c;}
                if(String.isNotBlank(CM.Campaign.Name)){ctu.Latest_Event_Campaign_Interaction__c = CM.Campaign.Name;}
                if(String.isNotBlank(CM.Campaign.Campaign_Sub_Type__c)){ctu.Latest_Event_Sub_Type__c = CM.Campaign.Campaign_Sub_Type__c;}
                if(String.isNotBlank(CM.Campaign.Type)){ctu.Latest_Event_Campaign_Type__c=CM.Campaign.Type;}
            }
	        if (contactToUpdate.size()>0) { 
	        	update contactToUpdate;
	        	system.debug('CampaignMemberHelper contactToUpdate = ' + contactToUpdate);
	        }
        }
    }

}