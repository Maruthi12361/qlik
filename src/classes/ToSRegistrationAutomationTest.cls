/**************************************************************** 
* 
* ToSRegistrationAutomationTest.class
* 
* Test class for ToSRegistrationAutomation.trigger.
* 
* 2016-11-29 NAD : Test class created. Coverage: 100%
*****************************************************************/ 

@isTest
private class ToSRegistrationAutomationTest {
	
	static testMethod void ToSRegistrationAutomationTest() {

		//create ToS Registration Object
		TOS_Registration__c tosObject = new TOS_Registration__c();

		tosObject.Bill_to_First_Name__c = 'TFirst';
		tosObject.Bill_to_last_name__c = 'TLast';
		tosObject.Bill_to_Company__c = 'Test Company';
		tosObject.Billto_Address_Line_1__c = '1 Testing Lane';
		tosObject.Billto_Country__c = 'Finland';
		tosObject.Billto_City__c = 'Lahti';
		tosObject.Billto_Country_Code__c = 'FI';
		tosObject.Billto_Post_Code__c = '15140';
		tosObject.TOS_Legal_Approval_Status__c = 'Legal Approval Required';
		tosObject.Schedule_Start_date__c = system.today();
		tosObject.TOS_Contact_First_Name__c = 'TTFirst';
		tosObject.TOS_Contact_Last_name__c = 'TTLast';
		tosObject.TOS_Contact_Email__c = 'test@test.com';

		//activate trigger
		insert tosObject;

		//need to query the process instance object in order to get the status
		String result = [SELECT status from ProcessInstance where TargetObjectId =: tosObject.Id].Status;

		//verify ToS registration approval is pending.
		System.assertEquals(result, 'Pending'); 
	}
}