/**
Change log
*   15.1.2020 	: Shubham Gupta - BSL-3174
16.1.2020 	: Ajay Santhosh - BSL-3283: Check for Quote Approval Reason.
**/  
public class ZuoraQuote_Preview_Approval_Val_Cont {
    
    public boolean ValidationSucceeded {get;set;}
    public boolean approvalReasonNotNeeded {get;set;}
    public id QuoteId {get;set;}
    public zqu__Quote__c quote {get;set;}
    public SBAA__Approval__c[] approvals {get;set;} 
    
    public ZuoraQuote_Preview_Approval_Val_Cont(){
        ValidationSucceeded = true;
        approvalReasonNotNeeded = true;
        QuoteId = (ApexPages.CurrentPage().getparameters().get('id') != null && ApexPages.CurrentPage().getparameters().get('id') != '' ) ? (Id)ApexPages.CurrentPage().getparameters().get('id') : null;
        
        if(QuoteId == null)
            return;
        
        List<zqu__Quote__c> quotes = [select id,Name,QDI_Product_Count__c, QDISourceAndTargetExists__c, Quote_Approval_Reason__c from zqu__Quote__c where id =:QuoteId];
        
        quote = quotes[0];
        
        if(quote.QDI_Product_Count__c > 0 && !quote.QDISourceAndTargetExists__c){
            ValidationSucceeded = false;
        }
        
        //To find if any approvals are required or not for the Quote
        try{
            approvals = SBAA.ApprovalAPI.preview(quote.Id, SBAA__Approval__c.Quote__c);
            if(String.isBlank(quote.Quote_Approval_Reason__c) && approvals != NULL && approvals.size()>0){
                approvalReasonNotNeeded = false;
            }
        }catch(exception e){
            system.debug('exception ' + e);
        }
        
    }
    
    /*public PageReference redirectToApproval() {
    if(!ValidationSucceeded || !isQuoteApprovalReasonNeeded)
    return null;
    PageReference pageRef = Page.SBAA__PreviewApprovals;
    pageref.getParameters().put('id', QuoteId);
    pageRef.setRedirect(true);
    return pageRef;
    }*/
    
    public PageReference onSubmit() {
        if(quote == NULL || !ValidationSucceeded || !approvalReasonNotNeeded)
            return null;
        SBAA.ApprovalAPI.submit(QuoteId, SBAA__Approval__c.Zuora_Quote__c);
        PageReference pr = new PageReference('/' + QuoteId);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference saveQuote(){
        if(quote != NULL && String.isNotBlank(quote.Quote_Approval_Reason__c)){
            try{
                update quote;
                approvalReasonNotNeeded = true;
            }
            catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            }
        }
        return null; 
    }
}