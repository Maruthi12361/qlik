/*
Controller for the Techspurt Thursdays page
2018-05-08 IT-597 added filter for STT Webinars
2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*/
public with sharing class QS_SupportTechspertThursdays {

	public QS_Search search {get; set;}
	
	public QS_SupportTechspertThursdays () {

        system.debug('start QS_SearchResultsController');

        String selectedProduct = 'All';
        String orderBy = 'Last Updated';
        
        Map<string, string> categorySearch = new Map<string, string>();
        categorySearch.put('Product__c','STT_Webinar');
        //categorySearch.put('All','All'); 
        search = new QS_KnowledgeSearch('STT Webinar', categorySearch , orderBy );
        search.pageSize = 10;
        //search = new QS_KnowledgeSearch('Desktop', categorySearch , orderBy );
        
       
    }

}