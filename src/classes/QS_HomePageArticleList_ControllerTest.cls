/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
* 2019-05-13 extbad IT-1685 Remove visit QS_DataCapture creation
************************************************************/
@isTest
private class QS_HomePageArticleList_ControllerTest {

    // Unit test for QS_HomePageArticleList_Controller Apex class and QS_HomePageArticleList Visualforce Component
    static testMethod void testHomePageArticleList_Controller() {
        createArticle();

        string articleNumber = [Select KnowledgeArticleId From KnowledgeArticleVersion Where IsLatestVersion = true AND Language = 'en_US' AND PublishStatus = 'Online' limit 1].KnowledgeArticleId;
        
        // creates Billing Address to be used on Account record
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Business Users';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;
        
         //Create Contact
        Contact testContact2 = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox2@qlikTech.com', '+44-7878787878', testAccount.Id);
        //testContact2.Persona__c = 'Decision Maker';
        insert testContact2;
        
        // Create Community User
        User communityUser2 = TestDataFactory.createUser(profileId, 'testSandbox2@qlikTech.com', 'tSbox2', String.valueOf(testContact2.Id));
        insert communityUser2;
        
        
        // Create Article_Categories__c Custom Setting record
        QS_Persona_Category_Mapping__c businessUser = TestDataFactory.createPersonaCategoryMapping( 'Business Users', 'Product:Server_Publisher:AJAX, Product:Server_Publisher:Mobile, Product:Server_Publisher:IE_Plugin, Product:Server_Publisher:Desktop_Client', 'Layout & Visualizations');
        insert businessUser;
        
        Test.startTest();
        QS_HomePageArticleList_Controller articleListController = new QS_HomePageArticleList_Controller();
        System.assertEquals(articleListController.unauthenticated, true);
        System.assertEquals(articleListController.searchCategory, '');
        
        System.runAs(communityUser) {
            articleListController = new QS_HomePageArticleList_Controller();
            System.assertEquals(articleListController.unauthenticated, false);
            QS_Persona_Category_Mapping__c categoryRecord = QS_Persona_Category_Mapping__c.getValues('Business Users');
            System.assertEquals(articleListController.searchCategory, (categoryRecord != null ? categoryRecord.Article_Categories__c : ''));
        }
        System.runAs(communityUser2) {
            articleListController = new QS_HomePageArticleList_Controller();
            System.assertEquals(articleListController.unauthenticated, false);
            System.assertEquals(articleListController.searchCategory, '');
        }

        List<SelectOption> ProductOptionListFiltered = articleListController.ProductOptionListFiltered;
        string  selectedProduct  = QS_HomePageArticleList_Controller.selectedProduct;
        articleListController.refreshProductCategory();
        articleListController.redirect();
         
        Test.stopTest();
    }

    private static ID createArticle() {
        String objType;
        List<String> kavNames = new List<String>(getKavNames());
        if (kavNames.isEmpty()){
            return null;
        }
        objType = kavNames.get(0);

        SObject kavObj = Schema.getGlobalDescribe().get(objType).newSObject();
        kavObj.put('Title','Foo Foo Foo!!!' + System.now().millisecond());
        kavObj.put('UrlName', 'foo-foo-foo-' + System.now().millisecond());
        kavObj.put('Summary', 'This is a summary!!! Foo. Foo. Foo.');
        kavObj.put('Language', 'en_US');
        insert kavObj;

        // requery the kavObj to get the KnowledgeArticleId on it that is created automatically
        String q = 'select id, KnowledgeArticleId from ' +objType+ ' where Id = \'' +kavObj.get('Id')+  '\' and PublishStatus = \'Draft\' and Language = \'en_US\'';
        List<SObject> kavs = Database.query(q);
        if (kavs.isempty()) {
            return null;
        }
        kavObj = kavs[0];
        KbManagement.PublishingService.publishArticle((ID)(kavObj.get('KnowledgeArticleId')),true);
        return (ID)(kavObj.get('KnowledgeArticleId'));
    }
     private static Set<String> getKavNames() {
        Set<String> kavNames;
        kavNames = new Set<String>();
        Map<String,Schema.SOBjectType> gd = Schema.getGlobalDescribe();

        for (String s : gd.keySet()) {
            if (s.contains('__kav')) {
                kavNames.add(s);
            }
        }
        return kavNames;
    }
    
    
}