//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//          
// Change History
// --------------
//  2013-01-08      SAN     Adding changes for partner paying by invoice        
//  2014-03-24      SAN     Adding boolean to hide/display register button when registration is closed for new user                
//*********************************************************/
public class efEventWrapper{
    public Registration__c registration { get;set; }
    public String eventId { get;set; }
    public String eventName { get;set; }  
    public String thumbnailImageUrl { get;set; }
    public String vision { get;set; }    
    public Boolean isRegistered { get;set; }
    public Boolean isRegistrationClosed { get;set; }
    public String registrationId { get;set; }
    public String campaignId { get;set; }
    private String level = null;
    private Events__c e;

    public efEventWrapper(){
        e = new Events__c();
    }
    
    public efEventWrapper(Events__c e){
        this.e = e;
    }
    
    public Events__c getEvent(){
        return e;
    }
    
    public String getPriceBookId(){
        return [Select Id From PriceBook2 Where Name=:e.Pricebook_Name__c Limit 1].Id;
    }
    
    public String getEventName(){
        return efUtility.giveValue(e.Name);
    }
    
    public Boolean getShowSessions(){
        if (e.Has_Sessions__c==null || !e.Has_Sessions__c) return false;        
        if (e.Session_Registration_Open_Date__c==null) return false;
        
        return (e.Session_Registration_Open_Date__c<=System.Today());
    }
    
    public Boolean getShowActivities(){
        if (e.Has_Networking_Activities__c==null || !e.Has_Networking_Activities__c) return false;
        if (e.Session_Registration_Open_Date__c==null) return false;
        return (e.Session_Registration_Open_Date__c<=System.Today());       
    }

    public String getAttendeeLabelActivityIntro(){
        return e.Attendee_Label_Activity_Intro__c;
    }
        
    public String getRegistrationLabelTrainingHeader(){
        return e.Registration_Label_Training_Header__c;
    }
        
    public String getRegistrationLabelTrainingSubHeader(){
        return e.Registration_Label_Training_SubHeader__c;
    }
    
    public String getRegistrationLabelSummaryPaymentRedirect(){
        return e.Registration_Label_Summary_Payment_Redir__c;
    }

    public String getRegistrationLabelSummaryInvoice(){
        return e.Registration_Label_Summary_Invoice__c;
    }
    
    public String getRegistrationLabelSummaryInvoiceConf(){
        return e.Registration_Label_Summary_Invoice_Conf__c;
    }
            
    public String getRegistrationLabelSummaryTermsAndConditions(){
        return e.Registration_Label_Summary_TermsAndCondi__c;
    }
    
    public String getRegistrationLabelSummaryFinancialSummary(){
        return e.Registration_Label_Summary_Financial_Sum__c;
    }

    public String getRegistrationLabelSummaryPayment(){
        return e.Registration_La__c;
    }
    
    public String getRegistrationLabelSummaryGroupDiscounts(){
        return e.Registration_Label_Summary_Group__c;
    }
    
    public String getRegistrationLabelSummaryGroup5Plus(){
        return e.Registration_Label_Summary_Group2__c;
    }
    
    public String getRegistrationLabelSummaryGroup10Plus(){
        return e.Registration_Label_Summary_Group3__c;
    }
                    
    public String getRegistrationLabelSummaryMessageTrainingInvalidated(){
        return e.Registration_Label_Summary_Training_Inv__c;
    }
    
    public String getRegistrationLabelFinishPaymentConfirmed(){
        return e.Registration_Label_Finish_Payment_Conf__c;
    }
    
    public String getRegistrationLabelFinishGoAttendeePortal(){
        return e.Registration_Label_Finish_Go_AttPortal__c;
    }

    public String getRegistrationLabelFinishError(){
        return e.Registration_Label_Finish_Error__c;
    }
    
    public String getRegistrationLabelFinishGoEvent(){
        return e.Registration_Label_Finish_Go_Event__c;
    }   
        
    public Boolean getHasExtendedStayAddon(){
        return e.Has_Extended_Stay_Addon__c;
    }
    
    public String getExtendedStaySummary(){
        return e.Extended_Stay_Summary__c;
    }

    public String getExtendedStayPolicy(){
        return e.Extended_Stay_Policy__c;
    }   
    
    public Id getExtendedStayProductId(){
        return e.Extended_Stay_Product__c;
    }
    
    public Boolean getHasSpouseOfferAddon(){
        return e.Has_Spouse_Offer_Addon__c;
    }
    
    public String getSpouseOfferSummary(){
        return e.Spouse_Offer_Summary__c;
    }
    
    public Id getSpouseOfferProductId(){
        return e.Spouse_Offer_Product__c;
    }   
    
    public Id getCampaignId(){
        return campaignId;
    }
    
    public static List<efEventWrapper> fetchMyEvents(String userAcessLevel, String userContact){
        return fetchMyEvents(userAcessLevel, userContact, null);        
    }

    public static List<efEventWrapper> fetchMyEvents(String userAcessLevel, String userContact, String eventId){
        List<efEventWrapper> myEvents = new List<efEventWrapper>();
        List<Events__c> allEvents = new List<Events__c>();
        
        // Comment& : to remove the user access level->Event level filter the line below blanks out any supplied value.
        userAcessLevel='';
        
        if (eventId != null) {
            allEvents = [Select Name, Id, Thumbnail_Image_Document__c, Event_Vision__c, Campaign__c, Session_Registration_Closed_Date__c
                            From Events__c
                            Where Event_End_Date__c >= :System.today() And isActive__c = true And isInternal__c = false And Id = :eventId];                        
        } else if(userContact==null){
            allEvents = [Select Name, Id, Thumbnail_Image_Document__c, Event_Vision__c, Campaign__c, Session_Registration_Closed_Date__c
                            From Events__c
                            Where Event_End_Date__c >= :System.today() And isActive__c = true And isInternal__c = false];
        } else if(userAcessLevel!=''){
            allEvents = [Select Name, Id, Thumbnail_Image_Document__c, Event_Vision__c, Campaign__c, Session_Registration_Closed_Date__c,
                            (Select Id,Name,Status__c From Registrations__r 
                                Where Registrant__c = :userContact And (Status__c='Registered' OR Status__c='Saved for Later' OR Status__c='Invoiced' OR Status__c='Pending'))
                                From    Events__c
                                Where   Event_End_Date__c >= :System.today() And
                                        isActive__c = true And
                                        isInternal__c = false And 
                                        Level__c INCLUDES (:userAcessLevel)];
        } else{
            allEvents = [Select Name, Id, Thumbnail_Image_Document__c, Event_Vision__c, Campaign__c, Session_Registration_Closed_Date__c,
                            (Select Id,Name,Status__c From Registrations__r 
                                Where Registrant__c = :userContact And 
                                    (Status__c='Registered' Or Status__c='Saved for Later' Or Status__c='Invoiced' Or Status__c='Pending'))
                                From    Events__c
                                Where   isActive__c = true And 
                                        isInternal__c = false And
                                        Event_End_Date__c >= :System.today()];
        }
        
        for(Events__c e : allEvents){
            efEventWrapper tempEvent = new efEventWrapper();
            tempEvent.eventId = e.Id;
            tempEvent.eventName = e.Name;
            tempEvent.isRegistered = false;     
            tempEvent.vision = e.Event_Vision__c;
            tempEvent.campaignId = e.Campaign__c;
            tempEvent.isRegistrationClosed = e.Session_Registration_Closed_Date__c>=System.Today();
            
            System.debug('&&&& e.Thumbnail_Image_Document__c :'+e.Thumbnail_Image_Document__c);
            
            tempEvent.thumbnailImageUrl = getDocumentUrl(e.Thumbnail_Image_Document__c);      
            if(userContact!=null && e.Registrations__r.size()>0){
                tempEvent.registration = e.Registrations__r[0];
                tempEvent.registrationId = e.Registrations__r[0].Id;
                if(e.Registrations__r[0].Status__c=='Registered' || e.Registrations__r[0].Status__c=='Invoiced')
                    tempEvent.isRegistered = true;
            }
                        
            myEvents.add(tempEvent);                    
        }
        return myEvents;                                
    }
            
    private static String getDocumentUrl(String documentUniqueName){
        try {
            Document doc = [Select d.Id From Document d Where d.DeveloperName=:documentUniqueName Limit 1];
            String imageid = doc.id; 
            imageid = imageid.substring(0,15);
            return '/servlet/servlet.FileDownload?file=' + imageid;
        } catch (System.Exception Ex) {
            System.debug('&&&& Exception in getDocumentURL :'+Ex.getMessage());
        }
        return '';
    }      
}