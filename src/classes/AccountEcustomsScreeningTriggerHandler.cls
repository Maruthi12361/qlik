/**     * File Name:AccountEcustomsScreeningTriggerHandler
        * Description : This handler class is used to create system message for ecustoms screening trigger tracking.
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Jan 26th 2019 RamakrishnaKini     Added new trigger logic.
*/
public with sharing class AccountEcustomsScreeningTriggerHandler {

    public static List<Id> lAccIds = new List<Id>();

	public AccountEcustomsScreeningTriggerHandler() {
		
	}

    /*  @Description :This common method is used to update billing country code fields on account on before update trigger context.
    
        @parameter inputList: Trigger.new Account list
        @parameter inputMap: Trigger.newMap 
        @parameter oInputList: Trigger.old Account list  
    */
    public static void onAfterUpdate(List<Account> inputList, Map<id, Account> inputMap, Map<id, Account> oInputMap) {
        for(Account acc: inputList){
            if ((acc.ECUSTOMS__Screening_Trigger__c != oInputMap.get(acc.ID).ECUSTOMS__Screening_Trigger__c) && acc.ECUSTOMS__Screening_Trigger__c) { 
                lAccIds.add(acc.Id);
            }
        }
        if(!lAccIds.isEmpty())
            insertsystemMessage(lAccIds, inputMap);
    }


    /**************************************************************************************
    *
    *    Private Methods
    ***************************************************************************************/

    /*  @Description :This method is used to create system message when the screening trigger is changed manually to true.
        @parameter inputList: Accounts whose billing country is populated
    */
    private static void insertsystemMessage(List<Id> lAccountIds, Map<id, Account> inputMap){
        Set<System_Message__c> sMessages = new Set<System_Message__c>();
        Account acct = new Account();
        for(Id accId:lAccountIds){
            if(inputMap.containsKey(accId) && inputMap.get(accId).ECUSTOMS__Screening_Trigger__c){
                acct = inputMap.get(accId);
                System_Message__c sMessage = new System_Message__c();
                sMessage.Account__c = accId;
                sMessage.Error_Type__c = 'Setting Screening Trigger to True from Account handler Class. Manual change.';
                sMessage.Error_Message__c = acct.ECUSTOMS__RPS_Status__c + ' ' + acct.ECUSTOMS__RPS_RiskCountry_Status__c + ' '+ acct.ECUSTOMS__RPS_Date__c;
                sMessages.add(sMessage);
            }
        }

        if(!sMessages.isEmpty())
            insert new List<System_Message__c>(sMessages);

    }
}