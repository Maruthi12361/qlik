//*********************************************************/
// Author: Mark Cane&
// Creation date: 27/08/2010
// Intent:  
//          
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//  2012-02-14  CCE     Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account

//  2014-04-10  TJG     CR 11469 https://eu1.salesforce.com/a0CD000000gAG2M
//                      On Live efTestSuiteRegistration test methods are all failing
//                      System.DmlException: Insert failed. First exception on row 0; 
//                      first error: MIXED_DML_OPERATION, DML operation on setup object is not permitted after you have updated 
//                      a non-setup object (or vice versa): Pricebook2, original object: User: []
//  2015-10-22  IRN     Added Custom settings since the winter 16 release
//  2016-05-11  AIN     Changed admin user in custom settings to Anna Forsgren.
//  2016-06-20  TJG     Anna is leaving too, so try qtweb
//  08.02.2017   RVA :   changing methods
//  12.03.2018   CCE :   added IsActive to adminUser select to fix failing in Prod 
//*********************************************************/
public class efTestSuiteHelper {
	static Id AccRecordTypeId = '01220000000DOFu';  //End User Account
    public static Events__c e;
    public static Events__c eNoReg;
    public static Session__c s; 
    public static Session__c sa;
    public static Registration__c r;
    public static Contact c;
    public static Track__c t;
    public static Announcement__c an;
    public static User registrantUser;
    public static Product2 tp;
    public static Payment_Details__c p;

    public static void setup(){ 

        createCustomSettings();
        // Admin user
        /* CR 11469, reduce SOQL count
        Id pId = [Select Id From Profile Where Name = 'System Administrator'].Id; 
        Id rId = [Select Id From UserRole Where Name = 'CEO'].Id;
        string tt = Datetime.now().format('ymd');
        User adminUser = new User(alias = 'adminu', email='adminuser@testorg.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = pId,
                            timezonesidkey='America/Los_Angeles', username='adminuser' + tt + '@testorg.com', userRoleId=rId);
        insert adminUser;
        */
        // CR 11469, these IDs are unlikely to change
        Id pId = '00e20000000yyUzAAI'; // Profile Id for System Administrator
        Id rId = '00E20000000vrInEAI'; // Role Id for CEO
        User adminUser = [select Id from user where ProfileId= :pId and UserRoleId = :rId and IsActive = true order by Id limit 1];
        // CR 11469
        
        // Pricebook
        PriceBook2 pb = new PriceBook2();
        pb.Name = 'Unit Test Event Pricebook';
        pb.IsActive = true;
        pb.Description = 'Unit Test Event Pricebook';
        insert pb;              
        
        Document td = new Document();
        td.Name = 'UNIT TEST EVENT THUMBNAIL';
        td.DeveloperName = 'UNIT_TEST_EVENT_THUMBNAIL';
        td.FolderId = [Select Id From Folder Where Name='Eventforce' And Type='Document' Limit 1].Id;
        insert td;
        
        Document bd = new Document();
        bd.Name = 'UNIT TEST EVENT BANNER';
        bd.DeveloperName = 'UNIT_TEST_EVENT_BANNER';
        bd.FolderId = [Select Id From Folder Where Name='Eventforce' And Type='Document' Limit 1].Id;
        insert bd;

        Product2 exp = new Product2(); // Extended Stay product.
        exp.ProductCode = 'EXTENDED-STAY';
        exp.Name = 'EXT';
        exp.Description = 'Extended Stay';
        exp.Family = 'Add-On';
        exp.Valid_Attendee_Types__c = 'Customer;Partner';       
        exp.StartDate__c = Date.today().addDays(-1);
        exp.EndDate__c = Date.today().addDays(30);
        exp.IsActive = true;
        exp.Deferred_Revenue_Account__c = '26000 Deferred Revenue';
        exp.Income_Account__c = '26000 Deferred Revenue';
        insert exp;     
                
        e = new Events__c();
        e.Name = 'Unit Test Event';
        e.isActive__c = true;       
        e.Event_Start_Date__c = Date.today().addDays(30);
        e.Event_End_Date__c = Date.today().addDays(35);
        e.Venue_Name__c = 'EIC';        
        e.Venue_Website__c = 'http://www.eic.com';
        e.Venue_City__c = 'Edinburgh';
        e.Website__c = 'http://www.eventforce.com';
        e.Contact_Email__c = 'event@force.com';
        e.Banner_Image_Document__c = td.DeveloperName;
        e.Thumbnail_Image_Document__c = bd.DeveloperName;
        e.Pricebook_Name__c = pb.Name;
        e.Opportunity_Owner__c = adminUser.Id;
        e.Has_Extended_Stay_Addon__c = true;
        e.Extended_Stay_Start_Date__c = e.Event_Start_Date__c.addDays(1);
        e.Extended_Stay_End_Date__c = e.Event_End_Date__c.addDays(1);
        e.Extended_Stay_Product__c = exp.Id;
        e.Extended_Stay_Policy__c = 'Ext policy';
        e.Extended_Stay_Summary__c = 'Ext summary';       
        e.Has_Spouse_Offer_Addon__c = false;
                
        Id emailOWId = [Select Id From OrgWideEmailAddress Where DisplayName Like '%Qonnections%' Limit 1].Id;      
        e.Confirmation_Email_OWD_Id__c = emailOWId;         
        insert e;
        
        exp.Event__c = e.Id;
        update exp;
                
        t = new Track__c();
        t.Event__c = e.Id;
        t.Name = 'Track 1';
        t.Track_Description__c = 'Track 1 Description';
        t.Track_Status__c = 'Active';
        insert t;
        
        s = new Session__c();
        s.Active__c = true;
        s.Capacity__c = 100;
        s.Event__c = e.Id;
        s.HighlightedSession__c = true;
        s.Name = 'Session 1';
        s.Track__c = t.Id;
        s.Session_Room__c = 'Room 1';
        s.Venue__c = 'Venue 1';
        s.Session_Start_Time__c = Datetime.now().addDays(31).addHours(1);
        s.Session_End_Time__c = Datetime.now().addDays(31).addHours(3);
        s.RecordTypeId = [Select Id From RecordType Where SobjectType='Session__c' And Name='Eventforce Session'].Id;
        s.Valid_Attendee_Types__c = 'Customer';
        insert s;
        
        sa = new Session__c();
        sa.Active__c = true;
        sa.Capacity__c = 50;
        sa.Event__c = e.Id;
        sa.HighlightedSession__c = true;
        sa.Name = 'Activity 1';     
        sa.Session_Room__c = 'Room 2';
        sa.Venue__c = 'Venue 2';
        sa.Session_Start_Time__c = Datetime.now().addDays(31).addHours(2);
        sa.Session_End_Time__c = Datetime.now().addDays(31).addHours(4);
        sa.RecordTypeId = [Select Id From RecordType Where SobjectType='Session__c' And Name='Eventforce Activity'].Id;
        sa.Valid_Attendee_Types__c = 'Customer';
        insert sa;      
        
        an = new Announcement__c();
        an.Active__c = true;
        an.Announcement_text__c = 'new announcement';
        an.Event__c = e.Id;
        an.Text__c = 'new announcement';
        insert an;
        
        tp = new Product2(); // Training product.
        tp.ProductCode = 'CCS';
        tp.Name = 'CCS';
        tp.Description = 'CCS Training';
        tp.Discount_Type__c = 'Amount';
        tp.Event__c = e.Id;
        tp.Family = 'Class';
        tp.Valid_Attendee_Types__c = 'Customer';
        tp.Class_Type__c = 'Developer';
        tp.Capacity__c = 50;
        tp.StartDate__c = e.Event_Start_Date__c.addDays(2);
        tp.EndDate__c = e.Event_Start_Date__c.addDays(3);
        tp.IsActive = true;
        tp.Deferred_Revenue_Account__c = '26000 Deferred Revenue';
        tp.Income_Account__c = '26000 Deferred Revenue';
        insert tp;
        
        PriceBookEntry tpSPBE = new PriceBookEntry();
        //tpSPBE.Pricebook2Id = [Select Id From PriceBook2 Where isStandard=true limit 1].Id;
        tpSPBE.Pricebook2Id = Test.getStandardPricebookId();
        tpSPBE.Product2Id = tp.Id;
        tpSPBE.UnitPrice = 100;
        tpSPBE.IsActive = true;
        insert tpSPBE;
        
        PriceBookEntry tpPBE = new PriceBookEntry();
        tpPBE.Pricebook2Id = pb.Id;
        tpPBE.Product2Id = tp.Id;
        tpPBE.UnitPrice = 100;
        tpPBE.UseStandardPrice = true;
        tpPBE.IsActive = true;
        insert tpPBE;       
                        
        eNoReg = new Events__c();
        eNoReg.Name = 'Unit Test Event No Registration';
        eNoReg.isActive__c = true;      
        eNoReg.Event_Start_Date__c = Date.today().addDays(30);
        eNoReg.Event_End_Date__c = Date.today().addDays(35);
        eNoReg.Venue_Name__c = 'EIC';       
        eNoReg.Venue_Website__c = 'http://www.eic.com';
        eNoReg.Venue_City__c = 'Edinburgh';
        eNoReg.Website__c = 'http://www.eventforce.com';
        eNoReg.Contact_Email__c = 'event@force.com';
        eNoReg.Banner_Image_Document__c = td.DeveloperName;
        eNoReg.Thumbnail_Image_Document__c = bd.DeveloperName;
        eNoReg.Pricebook_Name__c = pb.Name;
        eNoReg.Opportunity_Owner__c = adminUser.Id;     
        insert eNoReg;
                
        pId = [Select Id From Profile Where Name Like 'Eventforce Portal – Customer' Limit 1].Id; 
        
        System.runAs(adminUser){     
			/*
			Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();              
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc',
				Subsidiary__c = testSubs1.id           
            );
            insert QTComp;
			*/
			QlikTech_Company__c QTComp = QTTESTUtils.createMockQTCompany('QlikTech UK Ltd', 'United States', 'United States');
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account a = new Account();
            a.Name = 'Unit Test Account';
            a.Industry = 'Banking';
            a.QlikTech_Company__c = QTComp.QlikTech_Company_Name__c;
            a.Billing_Country_Code__c = QTComp.Id;
            insert a;
           

            c = new Contact();
            c.FirstName = 'Sophie';
            c.LastName = 'Grigson';
            c.Email = 'portaluser@testorg.com';
            c.accountId = a.Id;
            //c.MobilePhone = '12124334';       
            insert c;
                         
            registrantUser = new User(alias = 'portalu', email=c.Email,
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = pId,
                            timezonesidkey='America/Los_Angeles', username='portaluser@testorg.com',
                            contactId = c.Id);
            insert registrantUser;
            
            AccountShare ass = new AccountShare();
            ass.AccountId = a.Id;
            ass.AccountAccessLevel = 'Edit';
            ass.CaseAccessLevel = 'None';
            ass.ContactAccessLevel = 'Edit';
            ass.OpportunityAccessLevel = 'None';
            ass.UserOrGroupId = registrantUser.Id;
            insert ass;
        }
        
        System.runAs(registrantUser){
            r = new Registration__c();
            r.Event__c = e.Id;
            r.Registrant__c = c.Id;
            insert r;
            
            p = new  Payment_Details__c(Amount__c = 0.0,
                                       Transaction_Date__c=system.today(),
                                       Return_Code__c='-1', //For Pending
                                       CurrencyIsoCode='USD',
                                       RegistrationId__c=r.Id);
            insert p;           
        }
    }

    public static void createCustomSettings(){
        List<Custom_settings__c> settings = new List<Custom_settings__c>();

        Custom_settings__c se = new Custom_settings__c();
        se.Name='Configuration';
        se.Name__c = 'countryList';
        se.Value__c = 'US/UNITED STATES~AF/AFGHANISTAN~AX/ALAND ISLANDS~AL/ALBANIA~DZ/ALGERIA~AS/AMERICAN SAMOA~AD/ANDORRA~AO/ANGOLA~AI/ANGUILLA~AQ/ANTARCTICA~AG/ANTIGUA AND BARBUDA~AR/ARGENTINA~AM/ARMENIA~AW/ARUBA~AU/AUSTRALIA ~AT/AUSTRIA~AZ/AZERBAIJAN~BS/BAHAMAS~BH/BAHRAIN~BD/BANGLADESH~BB/BARBADOS~BY/BELARUS~BE/BELGIUM~BZ/BELIZE~BJ/BENIN~BM/BERMUDA~BT/BHUTAN~BO/BOLIVIA~BA/BOSNIA AND HERZEGOVINA~BW/BOTSWANA~BV/BOUVET ISLAND~BR/BRAZIL~IO/BRITISH INDIAN OCEAN TERRITORY~BN/BRUNEI DARUSSALAM~BG/BULGARIA~BF/BURKINA FASO~BI/BURUNDI~KH/CAMBODIA~CM/CAMEROON~CA/CANADA~CV/CAPE VERDE~KY/CAYMAN ISLANDS~CF/CENTRAL AFRICAN REPUBLIC~TD/CHAD~CL/CHILE~CN/CHINA~CX/CHRISTMAS ISLAND~CC/COCOS (KEELING) ISLANDS~CO/COLOMBIA~KM/COMOROS~CG/CONGO~CD/CONGO, THE DEMOCRATIC REPUBLIC OF THE~CK/COOK ISLANDS~CR/COSTA RICA~CI/COTE DIVOIRE~HR/CROATIA~CU/CUBA~CY/CYPRUS~CZ/CZECH REPUBLIC~DK/DENMARK~DJ/DJIBOUTI~DM/DOMINICA~DO/DOMINICAN REPUBLIC~EC/ECUADOR~EG/EGYPT~SV/EL SALVADOR~GQ/EQUATORIAL GUINEA~ER/ERITREA~EE/ESTONIA~ET/ETHIOPIA~FK/FALKLAND ISLANDS (MALVINAS)~FO/FAROE ISLANDS~FJ/FIJI~FI/FINLAND~FR/FRANCE~GF/FRENCH GUIANA~PF/FRENCH POLYNESIA~TF/FRENCH SOUTHERN TERRITORIES~GA/GABON~GM/GAMBIA~GE/GEORGIA~DE/GERMANY~GH/GHANA~GI/GIBRALTAR~GR/GREECE~GL/GREENLAND~GD/GRENADA~GP/GUADELOUPE~GU/GUAM~GT/GUATEMALA~GG/GUERNSEY~GN/GUINEA~GW/GUINEA-BISSAU~GY/GUYANA~HT/HAITI~HM/HEARD ISLAND AND MCDONALD ISLANDS~VA/HOLY SEE (VATICAN CITY STATE)~HN/HONDURAS~HK/HONG KONG~HU/HUNGARY~IS/ICELAND~IN/INDIA~ID/INDONESIA~IR/IRAN (ISLAMIC REPUBLIC OF)~IQ/IRAQ~IE/IRELAND~IM/ISLE OF MAN~IL/ISRAEL~IT/ITALY~JM/JAMAICA~JP/JAPAN~JE/JERSEY~JO/JORDAN~KZ/KAZAKHSTAN~KE/KENYA~KI/KIRIBATI~KP/KOREA, DEMOCRATIC PEOPLES REPUBLIC OF~KR/KOREA, REPUBLIC OF~KW/KUWAIT~KG/KYRGYZSTAN~LA/LAO PEOPLES DEMOCRATIC REPUBLIC~LV/LATVIA~LB/LEBANON~LS/LESOTHO~LR/LIBERIA~LY/LIBYAN ARAB JAMAHIRIYA~LI/LIECHTENSTEIN~LT/LITHUANIA~LU/LUXEMBOURG~MO/MACAO~MK/MACEDONIA, THE FORMER YUGOSLAV REPUBLIC~MG/MADAGASCAR~MW/MALAWI~MY/MALAYSIA~MV/MALDIVES~ML/MALI~MT/MALTA~MH/MARSHALL ISLANDS~MQ/MARTINIQUE~MR/MAURITANIA~MU/MAURITIUS~YT/MAYOTTE~MX/MEXICO~FM/MICRONESIA, FEDERATED STATES OF~MD/MOLDOVA, REPUBLIC OF~MC/MONACO~MN/MONGOLIA~MN/MONGOLIA~ME/MONTENEGRO~MS/MONTSERRAT~MA/MOROCCO~MZ/MOZAMBIQUE~MM/MYANMAR~NA/NAMIBIA~NR/NAURU~NP/NEPAL~NL/NETHERLANDS~AN/NETHERLANDS ANTILLES~NC/NEW CALEDONIA~NZ/NEW ZEALAND~NI/NICARAGUA~NE/NIGER~NG/NIGERIA~NU/NIUE~NF/NORFOLK ISLAND~MP/NORTHERN MARIANA ISLANDS~NO/NORWAY~OM/OMAN~PK/PAKISTAN~PW/PALAU~PS/PALESTINIAN TERRITORY, OCCUPIED~PA/PANAMA~PG/PAPUA NEW GUINEA~PY/PARAGUAY~PE/PERU~PH/PHILIPPINES~PN/PITCAIRN~PL/POLAND~PT/PORTUGAL~PR/PUERTO RICO~QA/QATAR~RE/REUNION~RO/ROMANIA~RU/RUSSIAN FEDERATION~RW/RWANDA~SH/SAINT HELENA~KN/SAINT KITTS AND NEVIS~LC/SAINT LUCIA~PM/SAINT PIERRE AND MIQUELON~VC/SAINT VINCENT AND THE GRENADINES~WS/SAMOA~SM/SAN MARINO~ST/SAO TOME AND PRINCIPE~SA/SAUDI ARABIA~SN/SENEGAL~RS/SERBIA~SC/SEYCHELLES~SL/SIERRA LEONE~SG/SINGAPORE~SK/SLOVAKIA~SI/SLOVENIA~SB/SOLOMON ISLANDS~SO/SOMALIA~ZA/SOUTH AFRICA~GS/SOUTH GEORGIA AND THE SOUTH SANDWICH ISL~ES/SPAIN~LK/SRI LANKA~SH/ST. HELENA~PM/ST. PIERRE AND MIQUELON~SD/SUDAN~SR/SURINAME~SJ/SVALBARD AND JAN MAYEN~SZ/SWAZILAND~SE/SWEDEN~CH/SWITZERLAND~SY/SYRIAN ARAB REPUBLIC~TW/TAIWAN, PROVINCE OF CHINA~TJ/TAJIKISTAN~TZ/TANZANIA, UNITED REPUBLIC OF~TH/THAILAND~TL/TIMOR-LESTE~TG/TOGO~TK/TOKELAU~TO/TONGA~TT/TRINIDAD AND TOBAGO~TN/TUNISIA~TR/TURKEY~TM/TURKMENISTAN~TC/TURKS AND CAICOS ISLANDS~TV/TUVALU~UG/UGANDA~UA/UKRAINE~AE/UNITED ARAB EMIRATES~GB/UNITED KINGDOM~UM/UNITED STATES MINOR OUTLYING ISLANDS~UY/URUGUAY~UZ/UZBEKISTAN~VU/VANUATU~VA/VATICAN CITY STATE (HOLY SEE)~VE/VENEZUELA~VN/VIETNAM~VG/VIRGIN ISLANDS (BRITISH)~VI/VIRGIN ISLANDS (U.S.)~WF/WALLIS AND FUTUNA~EH/WESTERN SAHARA~YE/YEMEN~YU/YUGOSLAVIA~ZM/ZAMBIA~ZW/ZIMBABWE';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Configuration';
        se.Name__c = 'adminUser';
        se.Value__c = '00520000000zCfx';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Configuration';
        se.Name__c = 'companySizeList';
        se.Value__c = '1-25 Employees~26-100 Employees~101-500 Employees~501-1000 Employees~>1000 Employees';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Configuration';
        se.Name__c = 'jobLevelList';
        se.Value__c = 'Staff Level~Manager Level~Director Level~VP Level~C Level~Executive Management~Board Level';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Configuration';
        se.Name__c = 'employeeDepartmentList';
        se.Value__c = '105-Products & Technology Executive~110-Technology Executive~111-Core Infrastructure Developers~112-Platform Developers~113-Applications Developers~114-Tampa Development~117-Platform Management~118-Applications Management~119-Tampa Mgmt & Support~120-TQE Executive~121-QA Core~122-QA Platform~124-QA Management~125-TPM - Exec~126-Performance Engineering~127-Engineering Services~128-QA Applications~130-Technical Documentation~131-User Experience~132-TPM Technology~133-TPM - Customer~134-TPM - Products~140-Test Lab~210-Systems Engineering~215-Network Operations~281-Curriculum Development~305-Marketing Executive~320-Industry Solutions~321-Pricing and Product Operations~323-Partner Program Operations~324-Force.com Application Development~326-Technical Services and Evangelists~327-Partner Marketing~328-ISV Recruitment~330-Corp Marketing~335-Marketing Strategy & Operations~350-Apps Product Management~351-Apps Product Marketing~353-Platform Product Management~354-Platform Product Marketing~402-Global Sales~404-Corp Sales Executive~405-Field Sales Executive~407-FinServ Vertical~408-Media Vertical~409-Public Sector AE~410-Field Sales Management~411-Field Sales Engineers~412-Field AE II~413-SE Specialists~414-Verticals Management~415-Tech Vertical~416-Vertical SEs~418-Field AE I~419-Field SE Mgmt~420-Corporate Sales Management~421-Corporate Sales Engineers~422-Small Business AE I~423-General Business AEs~424-Team Edition AEs~425-ECS AEs~427-Mid-Market AEs~428-Sales Support Engineers~429-Corp SE Mgmt~438-Corp Fin Svcs AEs~439-Corp Named Account AEs~450-Sales Dev - SR~454-Sales Dev - FS EBR~455-Sales Dev - GB EBR~456-Sales Dev - MM EBR~457-Sales Dev Exec~458-Services Sales Rep / SSR~459-Sales Dev Mgmt~460-Premium End User Support - P&L~461-Profl Svcs Mgmt~462-Enterprise Consulting Services~463-Education~464-Corporate Sales Services~465-Standard End User Support - Free~466-Public Training~467-Support Operations~470-Bus_Dev~471-Global System Integrators~473-Field Renewals~474-CSM - Field Sales~475-CSM - Corporate Sales~480-Field Operations~481-Internal Training~482-Global Sales Productivity~485-Non-Profit Sales~490-Custom Education~605-Professional Services Executive~814-InStranet Sales~905-Finance Executive~907-Investor Relations~909-WW FP&A~910-Finance Management~912-Revenue Operations Management and Admin~913-Treasury~914-Tax~915-Legal~916-Internal Audit~917-Policy & Intl Development~918-Phys Security & Risk Mgmt~919-Corporate Development~920-Executive~921-Revenue Ops Shared Services~922-Credit and Collections~923-Order Management~924-Compensation~930-IS~931-IT - Application Development~932-IT - Executive~933-IT - Quality Assurance~934-IT - Application Maintenance~935-IT - Infrastructure and Operations~936-Information Security~937-Product Security~938-Trust Exec~939-IT Program Management~940-Foundation~941-Employee Success~942-Recruiting~943-Leadership & Employee Development~944-Total Rewards & Employee Success Ops~945-Real Estate~946-Office Services~947-Procurement~950-AP & Expenses Payable~951-Payroll Operations~952-Global Equity Plan Services~953-Revenue Management~954-Global Accounting Services~955-Compliance & Quality';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Configuration';
        se.Name__c = 'jobFunctionList';
        se.Value__c = 'Administration~Alliances~Consultant~Finance & Accounting~General Management~HR~IT~Marketing~Operations~Product Management~Purchasing & Procurement~Sales~Service~Software Developer';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Employee';
        se.Name__c = 'employeeaccountid';
        se.Value__c = '0012000000I7QJh';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='MAIL';
        se.Name__c = 'Temporary_Response_Template_Attendee';
        se.Value__c = '00XD0000001jtbm';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Mail';
        se.Name__c = 'Basecamp_Response_Template_Attendee';
        se.Value__c = '00XD0000002An2I';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Mail';
        se.Name__c = 'Response_Template_Attendee';
        se.Value__c = '00X20000001J00Z';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Session';
        se.Name__c = 'activityrecordtypeid';
        se.Value__c = '01220000000Hbo0';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Session';
        se.Name__c = 'sessionrecordtypeid';
        se.Value__c = '01220000000Hbo5';
        settings.add(se);

        se = new Custom_settings__c();
        se.Name='Session';
        se.Name__c = 'pagesize';
        se.Value__c = '5';
        settings.add(se);

        insert settings;
        QuoteTestHelper.createCustomSettings();
    } 
}