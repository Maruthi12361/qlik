/*
@author: Malay Desai, Slalom LLC
@date: 01/20/2019
@description: Controller for the QVM "product builder step 1" page
#JIRA ID: BMWPC-68
*/
public without sharing class QVM_ProductBuilderStep1_QC {
    
    public QVM_Product_Data__c product {get; set;}
    public Id pId {get; set;}
    public Boolean showCancelButton {get;set;}
    
    public QVM_ProductBuilderStep1_QC() {
        QVM_Partner_Data__c partnerData = null;
        
        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) return;
        
        pId = ApexPages.currentPage().getParameters().get('id');
        
        if(pId == null) {
            product = new QVM_Product_Data__c();
            product.QVM_Partner__c = QVM.getUserPartnerDataId();
            product.Edit_Wizard__c = false;
            product.Product_Visibility__c = 'All Users'; //Set 'All Users' as default value
            product.Ajax_Demo_Visibility__c = 'All Users'; //Set 'All Users' as default value
            showCancelButton = true;
        } else {
            product = [select Id, QVM_Partner__c, Product_Name__c, Overview__c, Short_Description__c,
            Ajax_Demo_Link__c, Product_Visibility__c, Ajax_Demo_Visibility__c
            from QVM_Product_Data__c where Id = :pId];
            showCancelButton = false;
        }
        
    }
    
    public void catchExceptions() {
        if(product.Short_Description__c.contains('<') || product.Short_Description__c.contains('>')) {
            throw new QVM.newException(Label.QVM_PB_Step_1_Short_Description_HTML_Exception);
        }
        if(product.Ajax_Demo_Link__c != NULL && (!product.Ajax_Demo_Link__c.contains('http://') && !product.Ajax_Demo_Link__c.contains('https://'))) {
            throw new QVM.newException(Label.QVM_PB_Step_1_AJAX_Link_Validation);
        }
    }
    
    public PageReference saveProduct() {
        
        try{
            this.catchExceptions();
            upsert product;
            PageReference next;

            next = Page.QVM_ProductBuilderStep1_QC;
            return next;
        } catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
        
    }


    public PageReference previewPage() {
        try{
            upsert product;
            PageReference preview;
            preview = Page.QVM_ProductPreview_QC;
            preview.getParameters().put('Id', product.Id);
            return preview;
        } catch(Exception e) { ApexPages.addMessages(e); return null; }
    }

    public PageReference nextPage() {
        try{
            this.catchExceptions();
            upsert product;
            PageReference next;
            next = Page.QVM_ProductBuilderStep2_QC;
            next.getParameters().put('Id', product.Id);
            return next;
        } catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
        
    }
    
    public PageReference cancel() {
        PageReference next = Page.QVM_ControlPanelMain_QC;
        return next;
    }
    
    // CCE  |  CR# 40489 Qlik Market - add notice to register to additional pages
    public PageReference checkRegistration() {
        PageReference p = null;
        QVM_Partner_Data__c partnerData = null;
                
        try {
            partnerData = [select Id, Approved__c from QVM_Partner_Data__c where Partner_Account__c =:QVM.getUserAccountId() LIMIT 1];
        } catch(Exception e) {} //do nothing
        if (partnerData == null || partnerData.Approved__c == false) p = Page.QVM_ControlPanelMain_QC;
        return p;
    }




}