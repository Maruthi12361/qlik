/*****************************************************************************************
* 2015-10-22 IRN Webservice Mock for class efQTPaymentGateway, using WebServiceMockDispatcher is prefered, see
* that class for details.
****************************************************************************************/
@isTest
public class efQTPaymentGatewayMock implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    system.debug('efQTPaymentGatewayMock Start ');
    if(request instanceof efQTPaymentGateway.CapturePayment_element)
    {
      system.debug('instanceof efQTPaymentGatewayMock.CapturePayment_element');
      efQTPaymentGateway.CapturePaymentResponse_element cp_element = new efQTPaymentGateway.CapturePaymentResponse_element();
      response.put('response_x', cp_element);
    } else if(request instanceof efQTPaymentGateway.GetPaymentURL_element)
    {
      system.debug('instanceof efQTPaymentGatewayMock.GetPaymentURL_element');
      efQTPaymentGateway.GetPaymentURLResponse_element cp_element = new efQTPaymentGateway.GetPaymentURLResponse_element();
      cp_element.GetPaymentURLResult = 'ddd';
      response.put('response_x', cp_element);
    }else if(request instanceof efQTPaymentGateway.Payment_element)
    {
      system.debug('instanceof efQTPaymentGatewayMock.Payment_element');
      efQTPaymentGateway.PaymentResponse_element pr_element = new efQTPaymentGateway.PaymentResponse_element();
      pr_element.Result = new efQTPaymentGateway.PaymentResult();
      response.put('response_x', pr_element);
    }else if(request instanceof efQTPaymentGateway.GetPaymentStatus_element)
    {
      efQTPaymentGateway.GetPaymentStatusResponse_element psr_element = new efQTPaymentGateway.GetPaymentStatusResponse_element();
      psr_element.GetPaymentStatusResult = '';
      system.debug('instanceof efQTPaymentGatewayMock.GetPaymentStatus_element');
      response.put('response_x', psr_element);
    }
    system.debug('efQTPaymentGatewayMock End');
      
   }
}