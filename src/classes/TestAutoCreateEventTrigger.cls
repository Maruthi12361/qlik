/*********************************************************
* TestAutoCreateEventTrigger:
*   Test class to help testing AutoCreateEvent_OnContact and _OnLead triggers
*
* Change Log:
* xxxxxxxx  xxx     Initial development, ignore who did changes.
* 20140625  RDZ     Adding some test method to increase test coverage.
*                   Adding test method for fixing event date time and end date
2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
*********************************************************/

@isTest
private class TestAutoCreateEventTrigger {

    static testMethod void test_AutoCreateEvent_1() {
        datetime testDateTime = datetime.now();
        datetime testEndDateTime = testDateTime.addMinutes(120);
        String testLastName = 'test';
        String testEventName = 'test event name from lead';
        String testCompany = 'test lead company';

        Account testAccount = new Account();
        testAccount.Name ='Test Account';
        insert testAccount;        
        
        testLastName = 'TestContact';
        testEventName = 'test event name from contact';

        // EVENT from CONTACT testing 
        // insert and update new contact that should cause an event to be created
        Contact contact = new Contact(
            AccountId = testAccount.Id,
            LastName = testLastName,
            Event_Name__c = testEventName,
            Event_DateTime__c = testDateTime
        );
        try
        {
            insert contact;
        }
        catch(System.Exception Ex)
        {
            System.debug('testAutoCreateEvent - Caught exception: ' +Ex.getMessage());          
        }
       
        // retrieve contact just created
        List<Contact> existingContacts = [select AccountId, LastName, Event_Name__c, Event_DateTime__c, Event_End_DateTime__c from Contact where Id = :contact.Id];//Event_Name__c = :testEventName and Event_DateTime__c = :testDateTime limit 1];
        Contact existingContact;
        if (existingContacts.size() <= 0)
        {
            System.assert(false, 'contact 1 insertion failed...');
        }
        else
        {
            existingContact= existingContacts[0];
            System.assertEquals(testLastName, existingContact.LastName);
            //System.assertEquals(testEventName, existingContact.Event_Name__c);
            //System.assertEquals(testDateTime, existingContact.Event_DateTime__c);
        }
        Semaphores.ContactTriggerHandlerBeforeUpdate = false;
        System.assertNotEquals(null, existingContact, 'contact 1 insertion failed...');
        existingContact.Event_End_DateTime__c = testEndDateTime;
        existingContact.Event_Name__c = testEventName;
        existingContact.Event_DateTime__c = testDateTime;

        update existingContact;
                
        // retrieve the event that is created via the contact trigger, assert that a record is returned
        Event contactEvent = [select Subject, ActivityDateTime from Event where Subject = :existingContact.Event_Name__c and ActivityDateTime = :existingContact.Event_DateTime__c limit 1];
        System.assertNotEquals(null, contactEvent, 'event 1 not created when should have been...');

        delete contact;
        existingContact = null;
        contactEvent = null;
        
        // should also test:
        // insert new lead that should not cause an event to be created because Event_Date__c not supplied
        // retrieve the lead just inserted, make sure it exists and is correct
        // insert new lead that should not cause an event to be created because Event_Name__c is not supplied
        // retrieve the lead just inserted, make sure it exists and is correct

        // EVENT from LEAD testing 

        testLastName = 'test';
        testEventName = 'test event name from lead';
        testCompany = 'test lead company';

        // insert and update new lead that should cause an event to be created
        Lead lead = new Lead( 
            Company = testCompany,
            Country = 'Sweden',
            Email = 'asd@asd.com',
            FirstName = 'FirstName',
            LastName = testLastName
        );
        insert lead;
                
        // retrieve the lead just inserted, make sure it exists and is correct
        //Lead existingLead = [select Company, LastName, Event_Name__c, Event_DateTime__c, Event_End_DateTime__c from Lead where Event_Name__c = :testEventName and Event_DateTime__c = :testDateTime limit 1];
        Lead existingLead = [select Company, LastName, Event_Name__c, Event_DateTime__c, Event_End_DateTime__c from Lead where Id = :lead.Id limit 1];
        System.assertNotEquals(null, existingLead, 'lead 1 insertion failed...');

        existingLead.Event_End_DateTime__c = testDateTime.addMinutes(120);
        existingLead.Event_Name__c = testEventName;
        existingLead.Event_DateTime__c = testDateTime;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update existingLead;

        // retrieve the event that is created via the lead trigger, assert that a record is returned
        Event event = [select Subject, ActivityDateTime from Event where Subject = :existingLead.Event_Name__c and ActivityDateTime = :existingLead.Event_DateTime__c limit 1];
        System.assertNotEquals(null, event, 'event 1 not created when should have been...');

        // cleanup
        delete lead;
        existingLead = null;
        event = null;

        // insert new lead that should not cause an event to be created because Event_Date__c not supplied
        testEventName = 'another test event name';
        lead = new Lead( 
            Company = testCompany,
            Country = 'Sweden',
            Email = 'asd@asd.com',
            FirstName = 'FirstName',
            LastName = testLastName,
            Event_Name__c = testEventName
        );
        insert lead;
        
        // retrieve the lead just inserted, make sure it exists and is correct
        existingLead = [select Company, LastName, Event_Name__c, Event_DateTime__c from Lead where Company = :testCompany and LastName = :testLastName limit 1];
        System.assertNotEquals(null, existingLead, 'lead 2 insertion failed...');
        
        Integer eventCount = [select count() from Event where Subject = :testEventName];
        System.assertEquals(0, eventCount, 'event 2 insertion succeeded when it should not have...');

        // cleanup
        delete lead;
        existingLead = null;
        event = null;

    }
    
    static testMethod void test_AutoCreateEvent_2() {

        datetime testDateTime = datetime.now();
        string testLastName = 'test';
        string testCompany = 'test lead company';
        
        // insert new lead that should not cause an event to be created because Event_Name__c is not supplied
        testDateTime = testDateTime + 1;
        Lead lead = new Lead( 
            Company = testCompany,
            LastName = testLastName,
            Country = 'Sweden',
            Email = 'asd@asd.com',
            FirstName = 'FirstName',
            Event_DateTime__c = testDateTime
        );
        insert lead;
        
        // retrieve the lead just inserted, make sure it exists and is correct
        Lead existingLead = [select Company, LastName, Event_Name__c, Event_DateTime__c from Lead where Company = :testCompany and LastName = :testLastName limit 1];
        System.assertNotEquals(null, existingLead, 'lead 3 insertion failed...');
        
        integer eventCount = [select count() from Event where ActivityDateTime = :testDateTime];
        System.assertEquals(0, eventCount, 'event 3 insertion succeeded when it should not have...');

        // cleanup
        delete lead;
        existingLead = null;        
    }    
    
    //Added by CCE 2012_02_21 to improve testing coverage
    static testMethod void test_AutoCreateEvent_3() {

        datetime testDateTime = datetime.now();
        datetime testEndDateTime = testDateTime.addMinutes(120);
        string testLastName = 'test';
        string testCompany = 'test lead event company';
        String testEventName = 'another test event name from lead';
        
        // insert new lead
        Lead lead = new Lead( 
            Company = testCompany,
            LastName = testLastName,
            Country = 'Sweden',
            Email = 'asd@asd.com',
            FirstName = 'FirstName'
        );
        insert lead;
        
        // retrieve the lead just inserted, make sure it exists and is correct
        Lead existingLead = [select Company, LastName, Event_Name__c, Event_DateTime__c from Lead where Company = :testCompany and LastName = :testLastName limit 1];
        System.assertNotEquals(null, existingLead, 'Event_3: lead insertion failed...');
        
        existingLead.Event_Name__c = testEventName;
        existingLead.Event_DateTime__c = testDateTime;
        existingLead.Event_End_DateTime__c = testEndDateTime;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Update existingLead;

        // retrieve the event that is created via the lead trigger, assert that a record is returned, can't use information from lead as that is always set to null in the trigger
        Event event = [select Subject, ActivityDateTime from Event where Subject = :testEventName and ActivityDateTime = :testDateTime limit 1];
        //Event event = [select Subject, ActivityDateTime from Event where Subject = :existingLead.Event_Name__c and ActivityDateTime = :existingLead.Event_DateTime__c limit 1];
        System.assertNotEquals(null, event, 'Event_3: event not created when should have been...');

        // cleanup
        delete lead;
        existingLead = null;
        event = null;        
    }    
    
    //Added by CCE 2012_02_21 to improve testing coverage
    //we are testing the start time is after the end time
    static testMethod void test_AutoCreateEvent_4() {

        datetime testDateTime = datetime.now().addMinutes(120);
        datetime testEndDateTime = datetime.now();
        string testLastName = 'test';
        string testCompany = 'test lead event company';
        String testEventName = 'another test event name from lead';
        
        // insert new lead
        Lead lead = new Lead( 
            Company = testCompany,
            LastName = testLastName,
            Country = 'Sweden',
            Email = 'asd@asd.com',
            FirstName = 'FirstName'
        );
        insert lead;
        
        // retrieve the lead just inserted, make sure it exists and is correct
        Lead existingLead = [select Company, LastName, Event_Name__c, Event_DateTime__c from Lead where Company = :testCompany and LastName = :testLastName limit 1];
        System.assertNotEquals(null, existingLead, 'Event_4: lead insertion failed...');
        
        existingLead.Event_Name__c = testEventName;
        existingLead.Event_DateTime__c = testDateTime;
        existingLead.Event_End_DateTime__c = testEndDateTime;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Update existingLead;

        // retrieve the event that is created via the lead trigger, assert that a record is returned
        //Event event = [select Subject, ActivityDateTime from Event where Subject = :existingLead.Event_Name__c and ActivityDateTime = :existingLead.Event_DateTime__c limit 1];
        Event event = [select Subject, ActivityDateTime from Event where Subject = :testEventName and ActivityDateTime = :testDateTime limit 1];
        System.assertNotEquals(null, event, 'Event_4: event not created when should have been...');

        // cleanup
        delete lead;
        existingLead = null;
        event = null;        
    }    
    
    //Added by CCE 2012_02_21 to improve testing coverage
    //testing end date is less then 14 days after start
    
    static testMethod void test_AutoCreateEvent_5() {

        datetime testDateTime = datetime.now();
        datetime testEndDateTime = testDateTime.addDays(15);
        string testLastName = 'test';
        string testCompany = 'test lead event company';
        String testEventName = 'another test event name from lead';
        
        // insert new lead
        Lead lead = new Lead( 
            Company = testCompany,
            LastName = testLastName,
            Country = 'Sweden',
            Email = 'asd@asd.com',
            FirstName = 'FirstName'
        );
        insert lead;
        
        // retrieve the lead just inserted, make sure it exists and is correct
        Lead existingLead = [select Company, LastName, Event_Name__c, Event_DateTime__c from Lead where Company = :testCompany and LastName = :testLastName limit 1];
        System.assertNotEquals(null, existingLead, 'Event_5: lead insertion failed...');

        existingLead.Event_Name__c = testEventName;
        existingLead.Event_DateTime__c = testDateTime;
        existingLead.Event_End_DateTime__c = testEndDateTime;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Update existingLead;
        
        // retrieve the event that is created via the lead trigger, assert that a record is returned
        //Event event = [select Subject, ActivityDateTime from Event where Subject = :existingLead.Event_Name__c and ActivityDateTime = :existingLead.Event_DateTime__c limit 1];
        Event event = [select Subject, ActivityDateTime from Event where Subject = :testEventName and ActivityDateTime = :testDateTime limit 1];
        System.assertNotEquals(null, event, 'Event_5: event not created when should have been...');

        // cleanup
        delete lead;
        existingLead = null;
        event = null;        
    }


    //Added by RDZ to increase test coverage on event dates with more than 14 days
   
    static testMethod void test_AutoCreateEvent_EventDuration14DaysTest()
     {
        datetime testDateTime = datetime.now();
        datetime testEndDateTime = testDateTime.addDays(16);
        String testLastName = 'test';
        String testEventName = 'test event name from contact';
        String testCompany = 'test create contact for event trigger';
        
        User u = QTTestUtils.createMockSystemAdministrator();
        Account testAccount = QTTestUtils.createMockAccount(testCompany, u);
                

        // EVENT from CONTACT testing 
        // insert and update new contact that should cause an event to be created
        Contact contact = new Contact(
            AccountId = testAccount.Id,
            LastName = testLastName,
            Event_Name__c = testEventName,
            Event_DateTime__c = testDateTime
        );
        try
        {
            insert contact;
        }
        catch(System.Exception Ex)
        {
            System.debug('testAutoCreateEvent - Caught exception: ' +Ex.getMessage());          
        }
       
        // retrieve contact just created
        List<Contact> existingContacts = [select AccountId, LastName, Event_Name__c, Event_DateTime__c, Event_End_DateTime__c from Contact where Id = :contact.Id];//Event_Name__c = :testEventName and Event_DateTime__c = :testDateTime limit 1];
        Contact existingContact;
        if (existingContacts.size() <= 0)
        {
            System.assert(false, 'contact 1 insertion failed...');
        }
        else
        {
            existingContact= existingContacts[0];
            System.assert(existingContact.LastName != null);
            System.assertEquals(testLastName, existingContact.LastName);
        }

        System.assertNotEquals(null, existingContact, 'contact 1 insertion failed...');

        Semaphores.ContactTriggerHandlerBeforeUpdate = false;
        //Update contact with end date more than 14 days from event datetime
        //Trigger should reduce the end date from 16 to 14 days over Event DateTime
        existingContact.Event_End_DateTime__c = testEndDateTime;
        existingContact.Event_DateTime__c = testDateTime;
        existingContact.Event_Name__c = testEventName;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        update existingContact;
                
        // retrieve the event that is created via the contact trigger, assert that a record is returned
        //Event contactEvent = [select Subject, ActivityDateTime, EndDateTime from Event where Subject = :existingContact.Event_Name__c and ActivityDateTime = :existingContact.Event_DateTime__c limit 1];
        Event contactEvent = [select Subject, ActivityDateTime, EndDateTime from Event where Subject = :testEventName and ActivityDateTime = :testDateTime limit 1];
        System.assertNotEquals(null, contactEvent, 'event 1 not created when should have been...');
                
        //If end date is less than startdate, the date is modified to startdate +1
        System.assertEquals(testDateTime.addDays(14), contactEvent.EndDateTime);
        System.assertEquals(testDateTime, contactEvent.ActivityDateTime);

        delete contact;
        existingContact = null;
        contactEvent = null;
    }

    //Added by RDZ to increase test coverage on event dates where end date is lower than startdate
    static testMethod void test_AutoCreateEvent_EventEndDateLessThanStartDate()
     {
        datetime testDateTime = datetime.now();
        datetime testEndDateTime = testDateTime.addDays(-2); // set it two days earlier
        String testLastName = 'test';
        String testEventName = 'test event name from contact';
        String testCompany = 'test create contact for event trigger';
        
        User u = QTTestUtils.createMockSystemAdministrator();
        Account testAccount = QTTestUtils.createMockAccount(testCompany, u);
                

        // EVENT from CONTACT testing 
        // insert and update new contact that should cause an event to be created
        Contact contact = new Contact(
            AccountId = testAccount.Id,
            LastName = testLastName,
            Event_Name__c = testEventName,
            Event_DateTime__c = testDateTime
        );
        try
        {
            insert contact;
        }
        catch(System.Exception Ex)
        {
            System.debug('testAutoCreateEvent - Caught exception: ' +Ex.getMessage());          
        }
       
        // retrieve contact just created
        List<Contact> existingContacts = [select AccountId, LastName, Event_Name__c, Event_DateTime__c, Event_End_DateTime__c from Contact where Id = :contact.Id];//Event_Name__c = :testEventName and Event_DateTime__c = :testDateTime limit 1];
        Contact existingContact;
        if (existingContacts.size() <= 0)
        {
            System.assert(false, 'contact 1 insertion failed...');
        }
        else
        {
            existingContact= existingContacts[0];
            System.assert(existingContact.LastName != null);
            
            System.assertEquals(testLastName, existingContact.LastName);
        }

        System.assertNotEquals(null, existingContact, 'contact 1 insertion failed...');
        Semaphores.ContactTriggerHandlerBeforeUpdate = false;
        //Update contact with end date more than 14 days from event datetime
        //Trigger should reduce the end date from 16 to 14 days over Event DateTime
        existingContact.Event_End_DateTime__c = testEndDateTime;
        existingContact.Event_DateTime__c = testDateTime;
        existingContact.Event_Name__c = testEventName;
        
        update existingContact;
        
        // retrieve the event that is created via the contact trigger, assert that a record is returned
        //Event contactEvent = [select Subject, ActivityDateTime, EndDateTime from Event where Subject = :existingContact.Event_Name__c and ActivityDateTime = :existingContact.Event_DateTime__c limit 1];
        Event contactEvent = [select Subject, ActivityDateTime, EndDateTime from Event where Subject = :testEventName and ActivityDateTime = :testDateTime limit 1];
        System.assertNotEquals(null, contactEvent, 'event 1 not created when should have been...');
                
        //If end date is less than startdate, the date is modified to startdate +1
        System.assertEquals(testDateTime.addDays(1), contactEvent.EndDateTime);
        System.assertEquals(testDateTime, contactEvent.ActivityDateTime);


        delete contact;
        existingContact = null;
        contactEvent = null;
    }


}