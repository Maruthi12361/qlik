/**
 *
 * This class is dummy test purely for coverage so that
 * we can deploy the class
 *
 */
@isTest
private class MetadataServiceQlikTest {

    /*
     *  TEST SETUP
     */

    static testMethod void test_Create()
    {
        System.debug('MetadataServiceQlikTest.test_Create: TEST START');        
        test.startTest();
        
        MetadataService.MetadataPort metaSvc = new MetadataService.MetadataPort();
        MetadataService.createResponse_element responseElement = new MetadataService.createResponse_element();
        MetadataService.Metadata[] metadata = new MetadataService.Metadata[] {};
        MetadataService.Metadata mdatum = new MetadataService.Metadata();
        MetadataService.Workflow wkfl = new MetadataService.Workflow();
        MetadataService.WorkflowRule wkflr = new MetadataService.WorkflowRule();
        MetadataService.RemoteSiteSetting rsetting = new MetadataService.RemoteSiteSetting();
        MetadataService.AsyncResult asyncresult = new MetadataService.AsyncResult();
        MetadataService.checkStatusResponse_element checkEl = new MetadataService.checkStatusResponse_element();
        MetadataService.PackageTypeMembers packageTM = new MetadataService.PackageTypeMembers();
        MetadataService.DebuggingHeader_element dbgHEl = new MetadataService.DebuggingHeader_element();
        MetadataService.LogInfo lInf = new MetadataService.LogInfo();
        MetadataService.SessionHeader_element shEl = new MetadataService.SessionHeader_element();
        MetadataService.retrieveResponse_element rrEl = new MetadataService.retrieveResponse_element();
        MetadataService.DebuggingInfo_element dinfoEl = new MetadataService.DebuggingInfo_element();
        MetadataService.CallOptions_element coEl = new MetadataService.CallOptions_element();
        MetadataService.checkDeployStatus_element cdsEl = new MetadataService.checkDeployStatus_element();
        MetadataService.updateResponse_element urEl = new MetadataService.updateResponse_element();
        MetadataService.UpdateMetadata  uMetadata = new MetadataService.UpdateMetadata();
        MetadataService.FilterItem  fitem = new MetadataService.FilterItem(); 
        MetadataService.WorkflowTimeTrigger  wtimetr = new MetadataService.WorkflowTimeTrigger();
        MetadataService.WorkflowActionReference  waRef = new MetadataService.WorkflowActionReference();
        MetadataService.WorkflowAlert  wfAlert = new MetadataService.WorkflowAlert();
        MetadataService.WorkflowEmailRecipient eRecipient = new MetadataService.WorkflowEmailRecipient();
        MetadataService.WorkflowFieldUpdate wfFupdate = new MetadataService.WorkflowFieldUpdate();
        MetadataService.WorkflowKnowledgePublish kpublish = new MetadataService.WorkflowKnowledgePublish();
        MetadataService.WorkflowOutboundMessage wfOutMesg = new MetadataService.WorkflowOutboundMessage();
        MetadataService.WorkflowSend wfSend = new MetadataService.WorkflowSend();
        MetadataService.WorkflowTask wfTask = new MetadataService.WorkflowTask();

        metadata.add(mdatum);        
        try {
            metaSvc.create(metadata);
        } catch (Exception e) {
            System.debug('MetadataServiceQlikTest.test_Create: Caught exception: ' + e.getMessage());
        }
        test.stopTest();
        System.debug('MetadataServiceQlikTest.test_Create: TESTS DONE');
    
    } 
   
    static testMethod void test_updateMetadata() {
                
        System.debug('MetadataServiceQlikTest.updateMetadata: TEST START');        
        MetadataService.MetadataPort metaSvc = new MetadataService.MetadataPort();
        MetadataService.UpdateMetadata[] metadata = new MetadataService.UpdateMetadata[] {};
        test.startTest();           
        try {
            
            metaSvc.updateMetadata(metadata);
            
        } catch (Exception e) {
            System.debug('MetadataServiceQlikTest.updateMetadata: Caught exception: ' + e.getMessage());
        }
        test.stopTest();
        System.debug('MetadataServiceQlikTest.updateMetadata: TESTS DONE');
    }
    
    static testMethod void test_checkStatus() {
                
        System.debug('MetadataServiceQlikTest.checkStatus: TEST START');        
        MetadataService metaSvc = new MetadataService();
        MetadataService.MetadataPort metaPort = new MetadataService.MetadataPort();
        String[] asyncProcessId = new String[] {};
        test.startTest();                           
        try {
            metaPort.checkStatus(asyncProcessId);
        } catch (Exception ex) {
            System.debug('MetadataServiceQlikTest.checkStatus: Caught exception: ' + ex.getMessage());
        }            
            
        test.stopTest();
        System.debug('MetadataServiceQlikTest.checkStatus: TESTS DONE');
    }
    
}