/******************************************************

    Class: BatchDisableBlackListedLeadsAndContacts

    Changelog:
        2017-09-22 - AYS - BMW-390/CHG0030491 - Created file    

******************************************************/

global class BatchDisableBlackListedLeadsAndContacts implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
	
	private String query = 'SELECT id, ULC_country__c, ULC_city__c, LeadId__c, ContactId__c, ULCStatus__c '+
							'FROM ULC_Details__c WHERE ULCStatus__c = \'Active\'';
	
	final private Set<String> S_BLACKLISTEDCOUNTRIES = new Set<String>();
	final private Set<String> S_BLACKLISTEDCITIES = new Set<String>();
 
	final private String COUNTRY_UKRAINE = 'Ukraine';
	final private String REGION_CRIMEA = 'Crimea';
	final private String STATUS_LEAD_BANNED = 'Follow-Up Flagged in Error';
	final private String STATUS_CONTACT_BANNED  = 'Follow-Up Flagged in Error';
	final private String STATUS_ULS_BANNED  = 'Disabled'; 

	global BatchDisableBlackListedLeadsAndContacts() {
		List<Black_Listed_Portal_Region__c> l_blackListedRegions = Black_Listed_Portal_Region__c.getAll().values();
		for(Black_Listed_Portal_Region__c blackListedRegion : l_blackListedRegions) {
			if(blackListedRegion.isCountry__c){
				S_BLACKLISTEDCOUNTRIES.add(blackListedRegion.Name);
			}else{
				S_BLACKLISTEDCITIES.add(blackListedRegion.Name);
			}
		}
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		List<ULC_Details__c> l_ULC_Details = (List<ULC_Details__c>)scope;
		
		List<ULC_Details__c> l_ULC_DetailsToUpdate = new List<ULC_Details__c>();
		Set<Id> s_ContactIdsToUpdate = new Set<Id>();
		Set<Id> s_LeadIdsToUpdate = new Set<Id>();
		
		for(ULC_Details__c uls_detail : l_ULC_Details) {

			Boolean isBlackListed = false;
			//Check if country is banned
			if(S_BLACKLISTEDCOUNTRIES.contains(uls_detail.ULC_country__c)){
				isBlackListed = true;
			}else if(uls_detail.ULC_country__c == COUNTRY_UKRAINE &&
				uls_detail.ULC_city__c != null && (uls_detail.ULC_city__c.contains(REGION_CRIMEA) || S_BLACKLISTEDCITIES.contains(uls_detail.ULC_city__c))){
				isBlackListed = true;
			}

			if(isBlackListed){
				//deactivate uls_detail
				uls_detail.ULCStatus__c = STATUS_ULS_BANNED;
				l_ULC_DetailsToUpdate.add(uls_detail);

				//get related ids for contacts and leads
				if(uls_detail.LeadId__c != null){
					s_LeadIdsToUpdate.add(uls_detail.LeadId__c);
				}	
				if(uls_detail.ContactID__c != null){
					s_ContactIdsToUpdate.add(uls_detail.ContactId__c);
				}
			}
		}

		if(!s_ContactIdsToUpdate.isEmpty()){
			List<Contact> l_ContactToUpdate = new List<Contact>([
				SELECT id, Contact_Status__c
				FROM Contact
				WHERE Id IN: s_ContactIdsToUpdate
			]);
			for(Contact contact : l_ContactToUpdate) {
				//deactivation marks for contact
				contact.Allow_Partner_Portal_Access__c = false;
				contact.Contact_Status__c = STATUS_CONTACT_BANNED;
			}
			update l_ContactToUpdate;
		}
			
		if(!s_LeadIdsToUpdate.isEmpty()){
			List<Lead> l_LeadsToUpdate = new List<Lead>([
				SELECT id, Status
				FROM Lead
				WHERE Id IN: s_LeadIdsToUpdate
			]);
			for(Lead lead : l_LeadsToUpdate) {
				//do daectivation logic with lead
				lead.Status = STATUS_LEAD_BANNED;
			}				
			update l_LeadsToUpdate;
		}

		if(!l_ULC_DetailsToUpdate.isEmpty()){
			update l_ULC_DetailsToUpdate;
		}
	}
	
	global void execute(SchedulableContext SC){
		BatchDisableBlackListedLeadsAndContacts batchJob = new BatchDisableBlackListedLeadsAndContacts();
		ID batchprocessid = Database.executeBatch(batchJob);
	}

	global void finish(Database.BatchableContext BC) {
		
	}
	

}