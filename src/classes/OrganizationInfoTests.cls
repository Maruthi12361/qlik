@isTest
private class OrganizationInfoTests {

    
    private static testMethod void testGetOrganizationInfo() {
       
        Test.startTest(); 
        
        Organization_Info__c orgInfo = OrganizationInfo.getOrganizationInfo();
        
        Test.stopTest();
        
      	Organization o = [SELECT IsSandbox FROM Organization WHERE Id =: UserInfo.getOrganizationId()];
        System.assertEquals(o.IsSandbox, orgInfo.isSandbox__c);
        
    }    

    
    
}