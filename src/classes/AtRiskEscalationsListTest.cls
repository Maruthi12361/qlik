/**
* Change log:
*
*   2019-10-07 ext_bad  IT-2173: create the test for AtRiskEscalationsListController
*/
@IsTest
private class AtRiskEscalationsListTest {
    static testMethod void testMethod1() {
        Account newAccount = new Account();
        newAccount.Name = 'AtRiskEscalationsListTest';
        insert newAccount;

        ApexPages.StandardController ctrl = new ApexPages.StandardController(newAccount);
        AtRiskEscalationsListController contr = new AtRiskEscalationsListController(ctrl);

        Test.startTest();
        PageReference ref = contr.createNewEscalation();
        Test.stopTest();

        System.assert(ref.getUrl().contains('AddAtRiskEscalationByAccount'));
    }
}