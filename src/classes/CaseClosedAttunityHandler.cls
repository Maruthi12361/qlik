/*
 File Name: CaseClosedAttunityHandler

 2019-12-12 extbad IT-2329 Disable changes to Closed Attunity Cases for external users
 2020-01-31 extbad IT-2456 Disable changes to Closed Cases for attunity profiles only
 2020-02-11 extbad IT-2479 Disable changes to 'Chat Resolved' Status also
*/
public with sharing class CaseClosedAttunityHandler {
    public static final List<String> CASE_CLOSED_STATUSES = new List<String>{'Closed', 'Chat Resolved'};
    public static final List<String> ATTUNITY_PROFILES = new List<String>{
            'Customer Portal Case Logging Access - Attunity',
            'Customer Portal Case Viewing Access - Attunity'
    };
    public static final String ATTUNITY_CASE_ERROR_MESSAGE = 'Edits are not accepted on closed Cases';

    public static void handleCases(Map<Id, Case> newCasesMap, Map<Id, Case> oldCasesMap) {
        // Retrieve the current user profile's name
        User currentUser = [SELECT Id, Name, ProfileId, Profile.Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        String profileName = currentUser != null && currentUser.ProfileId != null && currentUser.Profile.Name != null ? currentUser.Profile.Name : '';

        if (!String.isBlank(profileName) && (ATTUNITY_PROFILES.contains(profileName))) {
            for (Case cs : newCasesMap.values()) {
                if (CASE_CLOSED_STATUSES.contains(cs.Status) && oldCasesMap.get(cs.Id) != null
                        && CASE_CLOSED_STATUSES.contains(oldCasesMap.get(cs.Id).Status)) {
                    cs.addError(ATTUNITY_CASE_ERROR_MESSAGE);
                }
            }
        }
    }
}