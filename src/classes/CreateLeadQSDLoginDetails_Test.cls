/**
	08.07.2017 : UIN : Test class for CreateLeadQSDLoginDetails_Test. Code coverage 92%
**/
@isTest
private class CreateLeadQSDLoginDetails_Test {
	
	@testSetup static void  createTestData() { 
		List<String> emailList = new List<String>{'aaaa@aaa.com', 'bbbbb@aaa.com'};

		insert createNewLead(emailList);
		insert createNewContact(emailList);	

	}


	@isTest
    static void callLeadBatch() {
		// Implement test code
		CreateLeadQSDLoginDetails updleadBatch = new CreateLeadQSDLoginDetails('Lead');
		Database.executeBatch(updleadBatch, 100);
	}

	@isTest
    static void callContactBatch() {
		CreateLeadQSDLoginDetails updContactBatch = new CreateLeadQSDLoginDetails('Contact');
		Database.executeBatch(updContactBatch, 100);
	}

	private static List<Lead> createNewLead(List<String> emailList) {
        List<Lead> lLeads = new List<Lead>();
        for(String email: emailList){
	        Lead lead = new Lead();
	        lead.LastName = 'last';
	        lead.Company = 'Company';   
	        lead.IsUnreadByOwner = True;
	        lead.Country='Sweden';
	        lead.Email=email;
	        lead.Phone='3333';
	        lead.Updated_Total_Login_Count__c = true;
	        lead.QSD_total_logins__c = 4;
	        lLeads.add(lead);
    	}
        return lLeads;        
    }
	

	private static List<Contact> createNewContact(List<String> emailList) {
		List<Contact> lContacts = new List<Contact>();
		for(String email: EmailList){
			Contact ct = new Contact(   FirstName = 'Test LeadCountryISOUpdate',
                                Email = email,
                                LastName = 'Lastname', 
                                QCloudID__c = 'Enterprise',
                                PartnerSourceNo__c = '12345',
                                Updated_Total_Login_Count__c = true,
                                QSD_total_logins__c = 4
                                );
			lContacts.add(ct);
		}
		return lContacts;     
	}
	
}