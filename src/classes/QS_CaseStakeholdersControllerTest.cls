/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
@isTest
private class QS_CaseStakeholdersControllerTest {

	private static Id recTypeId;

	private static testMethod void TestCommunityUser() {

		QTTestUtils.GlobalSetUp();

		QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        insert qtc;
    
        //Create Account
        Account testAccount = TestDataFactory.createAccount('Test AccountName', qtc);
        insert testAccount;
        
        //Create Contact
        Contact testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', testAccount.Id);
        testContact.Persona__c = 'Decision Maker';
        insert testContact;
        
        // Retrieve Csutomer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'Customer Portal Base Access + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;
        
        // Create Community User
        User communityUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert communityUser;

        // Running as the current user for code coverage only

    	Case caseObj = TestDataFactory.createCase('Test subject - KB Article', 'Test description - KB Article', communityUser.Id, '3',
                                    communityUser.contactId, communityUser.accountId, getCaseRecordTypeId('QlikTech Master Support Record Type'), 'Community',
                                    false, null, null, null, null, null, null);

    	insert caseObj;

    	test.startTest();

    	ApexPages.StandardController stdController = new ApexPages.StandardController(caseObj);
    	QS_CaseStakeholdersController controller = new QS_CaseStakeholdersController(stdController);
    	List<EntitySubscription> stakeholders = controller.stakeholders;
    	List<SelectOption> newSubList = controller.newSubList;
    	controller.userId=communityUser.Id;
    	controller.addStakeholder();
    	controller.userId=null;
    	controller.addStakeholder();
    	controller.subId = [select id from EntitySubscription limit 1].Id;
    	controller.removeStakeholder();

    	test.stopTest();
	}
	private static Id getCaseRecordTypeId(String recordTypeName) {
        if(recTypeId != null) {
            return recTypeId;
        } else {
                
            recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            return recTypeId;
        }
    }
}