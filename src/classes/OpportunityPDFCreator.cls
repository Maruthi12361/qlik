// CR# 9885
// apex class called by the javascript in the "Create PDF" custom button on Opportunity object
// Change log:
// September 30, 2013 - Initial Implementation - Madhav Kakani - Fluido Oy
// CR# 10936
// February 02, 2013 - Added CreateAuditPDF function - Madhav Kakani - Fluido Oy
// CR# 24909
// February 23, 2015 - Added CreateSoEPDF function
// April 08, 2015 - Added CreateSoIPDF function CR# 29825
// June 09, 2016 Q2CW added method CreateOldOpportunityToPDF
// February 06, 2017 added check on pricebook Q2CW project IRN
global class OpportunityPDFCreator {
    WebService static String CreatePDF(String oppid, String name) {
        PageReference pdf = Page.Opportunity_PDF_Template;
        pdf.getParameters().put('id', oppid); // add parent id to the parameters for standardcontroller
        
        Blob body; 
        try {
            body = pdf.getContent(); // returns the output of the page as a PDF
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
  
        // create the new attachment
        Attachment attach = new Attachment();
        attach.Body = body;
    
        attach.Name = name + '.pdf';
        attach.IsPrivate = false;        
        attach.ParentId = oppid; // attach the pdf to the opportunity
        try {
            insert attach;
        } catch (DMLException e) {
            return 'Error: ' + e.getMessage();
        }
        
        return 'Success';
    }


    //Q2CW create method to create pdf of old opp line items
    WebService static String CreateOldOpportunityToPDF(String oppid, String name, String stageName) {
        
        Set<String> closedStages  = new Set<String>();
        closedStages.addAll(OpportunityHelperClass.closedStages);
        system.debug('CreateOldOpportunityToPDF ' + stageName);
        if(closedStages.contains(stageName)){
            return 'Closed';
        }
        Opportunity opp = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :oppid LIMIT 1]; 
        Id ID_SB_Pricebook;
        if(Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails') != null) { //if pricebook is already in the new system we should not convert the opportunity
            ID_SB_Pricebook = Steelbrick_Settings__c.getInstance('SteelbrickSettingsDetails').SBPricebookId__c;
        }
        if(opp != null && opp.Pricebook2Id == ID_SB_Pricebook){
            return 'Steelbrick';
        }
        PageReference pdf = Page.Opportunity_PDF_Template_Q2CW;  
        pdf.getParameters().put('id', oppid); // add parent id to the parameters for standardcontroller
        
        Blob body; 
        try {
            body = pdf.getContent(); // returns the output of the page as a PDF
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
  
        // create the new attachment
        Attachment attach = new Attachment();
        attach.Body = body;
    
        attach.Name = name + '- SteelBrick Quote Lines.pdf';
        attach.IsPrivate = false;        
        attach.ParentId = oppid; // attach the pdf to the opportunity
        try {
            insert attach;
        } catch (DMLException e) {
            return 'Error: ' + e.getMessage();
        }
        
        return 'Success';
    }

    WebService static String CreateAuditPDF(String oppid, String name, String url) {
        PageReference pdf = Page.Opportunity_Audit_PDF_Template;        
        pdf.getParameters().put('id', oppid); // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('detail_url', url); // add opp pag detail url
        
        Blob body; 
        try {
            body = pdf.getContent(); // returns the output of the page as a PDF
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
  
        // create the new attachment
        Attachment attach = new Attachment();
        attach.Body = body;
    
        attach.Name = 'Audit Document - ' + name + '.pdf';
        attach.IsPrivate = false;        
        attach.ParentId = oppid; // attach the pdf to the opportunity
        try {
            Database.Saveresult sr = Database.insert(attach, true);
            return sr.getId();
        } catch (DMLException e) {
            return 'Error: ' + e.getMessage();
        }
        return 'Error:';
    }
    
    WebService static String CreateSoEPDF(String oppid, String name, String url) {
        PageReference pdf = Page.Opportunity_SoE_PDF_Template;        
        pdf.getParameters().put('id', oppid); // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('detail_url', url); // add opp pag detail url
        
        Blob body; 
        try {
            body = pdf.getContent(); // returns the output of the page as a PDF
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
 
        // create the new attachment
        Attachment attach = new Attachment();
        attach.Body = body;
    
        attach.Name = 'SoE Document - ' + name + '.pdf';
        attach.IsPrivate = false;        
        attach.ParentId = oppid; // attach the pdf to the opportunity
        try {
            Database.Saveresult sr = Database.insert(attach, true);
            return sr.getId();
        } catch (DMLException e) {
            return 'Error: ' + e.getMessage();
        }

        return 'Error:';

    }
    
    WebService static String CreateSoIPDF(String oppid, String name, String url) {
        PageReference pdf = Page.Opportunity_SoI_PDF_Template;        
        pdf.getParameters().put('id', oppid); // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('detail_url', url); // add opp pag detail url
        
        Blob body; 
        try {
            body = pdf.getContent(); // returns the output of the page as a PDF
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
 
        // create the new attachment
        Attachment attach = new Attachment();
        attach.Body = body;
    
        attach.Name = 'SoI Document - ' + name + '.pdf';
        attach.IsPrivate = false;        
        attach.ParentId = oppid; // attach the pdf to the opportunity
        try {
            Database.Saveresult sr = Database.insert(attach, true);
            return sr.getId();
        } catch (DMLException e) {
            return 'Error: ' + e.getMessage();
        }

        return 'Error:';

    }
}