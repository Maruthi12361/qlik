@isTest
private class efTestSuiteRegisterGroup {

    static testMethod void controllerUnitTest(){
    	efTestSuiteHelper.setup();

		System.runAs(efTestSuiteHelper.registrantUser){
			ApexPages.currentPage().getParameters().put('eventId', efTestSuiteHelper.eNoReg.Id);    	    	
	    	efRegisterGroupController ctl = new efRegisterGroupController();    	
			System.assert(ctl.event!=null);
	    	
			ApexPages.currentPage().getParameters().put('contactId', efTestSuiteHelper.c.Id);    			
			System.assert(ctl.goRegister()!=null);
			ApexPages.currentPage().getParameters().put('registrationId', efTestSuiteHelper.r.Id);		
	    	System.assert(ctl.goAttendeePortal()!=null);
		} 
    }
}