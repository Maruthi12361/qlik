// CR# 29825
// test class to test OpportunityPDFCreator, OpportunitySoIPDFTemplateCtrl and visualforce page: Opportunity_SoI_PDF_Template
// Change log:
// April 8, 2015 - Initial Implementation - Stuart Sleight
// 07.02.2017   RVA :   changing methods  
//24.03.2017 Rodion Vakulovskyi   
@isTest
private class OpportunitySoIPDFCreatorTest {
    static testMethod void testOpportunitySoEPDFTemplateCtrl() {


                Profile p = [select id from profile where name='System Administrator']; 
          User mockSysAdmin = new User(alias = 'standt', email='standarduser@testorg.com', 
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
          localesidkey='en_US', profileid = p.Id, 
          timezonesidkey='America/Los_Angeles', username='standarduser@hourserr.com');

        System.RunAs(mockSysAdmin)
        {

	//    Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
   //     QlikTech_Company__c QTComp = new QlikTech_Company__c(
   //         Name = 'USA',
   //         QlikTech_Company_Name__c = 'QlikTech Inc',      
   //         Country_Name__c = 'USA',
			//Subsidiary__c = testSubs1.id
   //     );
   //     insert QTComp;

        QTTestUtils.GlobalSetUp();
        QTTestUtils.SetupNSPriceBook('GBP');
			QlikTech_Company__c qtc = QTTestUtils.createMockQTCompany('QlikTech Nordic AB', 'SWE', 'United States');
				qtc.Country_Name__c = 'Sweden';
			    qtc.CurrencyIsoCode = 'SEK'; 
			update qtc;
			/*
            QlikTech_Company__c qtc = new QlikTech_Company__c();
            qtc.name = 'SWE';
            qtc.QlikTech_Company_Name__c = 'QlikTech Nordic AB';
            qtc.Country_Name__c = 'Sweden';
            qtc.CurrencyIsoCode = 'SEK';
			qtc.Subsidiary__c = testSubs1.id;
            insert qtc;
			*/
            Account act = new Account(name='Test Account');
            act.OwnerId = mockSysAdmin.Id;
            act.Navision_Customer_Number__c = '12345';
            act.Legal_Approval_Status__c = 'Legal Approval Granted';            
            
            act.QlikTech_Company__c = qtc.QlikTech_Company_Name__c;         
            act.Billing_Country_Code__c = qtc.Id;
            act.BillingStreet = '417';
            act.BillingState ='CA';
            act.BillingPostalCode = '94104';
            act.BillingCountry = 'United States';
            act.BillingCity = 'SanFrancisco';
            
            act.Shipping_Country_Code__c = qtc.Id;
            act.ShippingStreet = '417';
            act.ShippingState ='CA';
            act.ShippingPostalCode = '94104';
            act.ShippingCountry = 'United States';
            act.ShippingCity = 'SanFrancisco';           
            act.Account_Creation_Reason__c = 'Test';
            insert act;
                       
            Contact ct = new Contact(AccountId=act.Id,lastname='Contact1',firstname='Test');
            insert ct;
            
            RecordType rt = QuoteTestHelper.getRecordTypebyDevName('Sales_QCCS');
                
            ID PB = [Select p.Id from PricebookEntry p  where IsActive = true and p.CurrencyIsoCode = 'GBP' LIMIT 1].Id;
            //Create test Opportunity
            Opportunity testNewOpp = New Opportunity (
                Short_Description__c = 'TestOpp',
                Name = 'TestOpp',
                Type = 'New Customer',
                Revenue_Type__c = 'Direct',
                CloseDate = Date.today(),
                StageName = 'Alignment Meeting',
                RecordTypeId = rt.Id,
                CurrencyIsoCode = 'GBP',
                AccountId = ct.AccountId,
                Signature_Type__c = 'Digital Signature',
                ForecastCategoryName = 'Omitted'
            );
                Test.startTest();
            insert testNewOpp;
           
            OpportunityLineItem LineItem = new OpportunityLineItem();           
            LineItem.OpportunityId = testNewOpp.Id;
            LineItem.UnitPrice = 10;
            LineItem.Quantity = 2;
            LineItem.PricebookEntryId = PB;         
            insert LineItem;    
            
            Sequence_of_Event__c soe = new Sequence_of_Event__c();           
            soe.Opportunity_ID__c = testNewOpp.Id;
            soe.S_o_E_Date__c = date.today();
            soe.Checkpoint__c = 'Testing';
            soe.Proposed_Event__c = 'Testing';
            soe.Description__c = 'Testing';
            soe.Cost__c = 100;
            soe.Responsibility__c ='Testing';         
            insert soe;
               Test.stopTest();  
            
            system.assert(testNewOpp.Id != null);

          
            
            String sResp = OpportunityPDFCreator.CreateSoEPDF(testNewOpp.Id, testNewOpp.Name,'http://salesforce.com/'+testNewOpp.Id);
            String pdfId = [SELECT Id from Attachment a where a.ParentId = :testNewOpp.Id LIMIT 1].Id;
            system.assert(sResp == pdfId);

            // Test the OpportunityAuditPDFTemplateCtrl
            PageReference pdf = Page.Opportunity_SoE_PDF_Template;        
            pdf.getParameters().put('id', testNewOpp.id);
            pdf.getParameters().put('detail_url', 'http://salesforce.com/'+testNewOpp.Id);
            Test.setCurrentPageReference(pdf);

            ApexPages.StandardController sc = new ApexPages.standardController(testNewOpp);
            OpportunitySoEPDFTemplateCtrl pdf_x = new OpportunitySoEPDFTemplateCtrl(sc);
            system.assert(pdf_x.showSoE == true);
                              
                 
        }
    }
    
    
} // class OpportunitySoIPDFCreatorTest