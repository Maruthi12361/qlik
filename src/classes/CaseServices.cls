/**********************************************************
* Class: CaseServices
* Description: Todo
* Changes Log:
*   XXXX-XX-XX XX Initial doc
*   2012-01-31 RDZ Adding Aaron Changes for SC2 project
*   2012-02-20 RDZ Adding Aaron Changes for SC2 project Severity 3      
*   2012-08-15 ADD CR# 5160, fixSeverity3 method changed
*   2014-01-23 CCE CR# 10129 - Fix CaseServices trigger. Added code to updateAccountLicenseTechInfo 
*                  method to remove duplicates from acntAccountLicensesToUpdate List
*   2015-10-22 RDZ Winter 16 Issue Adding 31 from 22 
*   2015-11-11 NAD Updated support level names per CR# 57556 
*   2016-02-24 NAD Updated assignPartnerSuppContactToCase() functions per CR# 33068 (Responsible Partner change)
*   2016-02-25 AIN Fixed the map in assignPartnerSuppContactToCase & assignPartnerSuppContactToCase as they weren't using the correct IDs
*   2017-04-26  UIN Added logic to reduce SOQL queries
*   2017-10-25 ext_bad CHG0032086 Rewrite populating Business Hours in the FixSeverity3 (comments in method)
*   2017-11-10 ext_vos CHG0032086 Update populating for Case.BH if Basic level.
*   2018-01-24 ext_vos CHG0030671: add countForBugs() to calculate all cases for related Bugs. 
*   2018-02-01 ext_vos CHG0030671: add logic with Case.Force_Triggers__c flag.
*   2018-05-22 ext_vos CHG0033970: add simplify Partner Performance data collection for 'QlikTech Master Support Record Type'. 
*   2018-09-21 ext_vos CHG0034528: remove the filling of fields in Technical Information. Refactoring for assignPartnerSuppContactToCase and handleCaseEndUserAccountNameCopy.
*   2018-12-17 ext_vos INC0154629: move fixSeverity3 logic to CaseServiceWithoutSharing class.
*   2019-01-22 ext_bad add check for not blank AccountId in assignPartnerSuppContactToCase method.
*   2019-11-27 ext_bad   IT-2324 Populdate Case.Resolution_Date
*************************************************************/
public class CaseServices
{
    //Properties
    public static Map<String, RecordType> recordTypesNameMap
    {
        get
        {
            if(recordTypesNameMap == null)
            {
                recordTypesNameMap = new Map<String, RecordType>();
                for(RecordType rt : [select Id, Name from RecordType where sObjectType = 'Case' and IsActive = true])
                    recordTypesNameMap.put(rt.Name, rt);
            }
            return recordTypesNameMap;
        }
        private set;
    }
    //Methods   
    private static Id getProblemRecordTypeId()
    {
        RecordType[] rt = [select Id from RecordType where SObjectType = 'Case' and IsActive = true and DeveloperName = 'QT_Problem_Case_Record_Type'];
        if(rt.size() > 0)
            return rt[0].Id;
        else
            throw new CustomException('QT Problem Case Record Type Record Type for Cases not found!');
    }

    // Method which takes in the old Cases Map and new Cases List and then sets the Partner Support Contact on the Cases
    public static void assignPartnerSuppContactToCase(List<Case> newCasesList, Map<Id, Case> oldCasesMap) {
        Set<Id> accountIds = new Set<Id>();
        for (Case c : newCasesList) {
            if ((oldCasesMap == null
                    || (oldCasesMap != null && oldCasesMap.get(c.Id).AccountId != c.AccountId)) && String.isNotBlank(c.AccountId)) {
                accountIds.add(c.AccountId);
            }
        }
        if (!accountIds.isEmpty()) {
           List<Responsible_Partner__c> responsiblePartners = [SELECT End_User__r.Id, Partner__r.Partner_Support_Contact__c 
                                                                FROM Responsible_Partner__c WHERE End_User__r.Id IN : accountIds];
            Map<Id, Id> accountsMap = new Map<Id, Id>();            
            if (!responsiblePartners.isEmpty()) { // Added by UIN to improve soql execution count
                for (Responsible_Partner__c rp : responsiblePartners) {
                    accountsMap.put(rp.End_User__r.Id, rp.Partner__r.Partner_Support_Contact__c);
                }
                //Loop over new Cases and set the Partner Support Contact
                for (Case c : newCasesList) {
                    if (!accountsMap.isEmpty() && accountsMap.containsKey(c.AccountId)) { // Added by UIN map empty check
                        c.Partner_Support_Contact__c = accountsMap.get(c.AccountId);
                    }
                }
            }
        }        
    }

    //Method which takes a case and returns all timeline events associated with it, sorted by date descending
    //(Case Comment, Open Activities, Activity History, Case History, & Attachments)
    public static CaseEvent[] getCaseTimeline(CaseModel tvm, String tViewType)
    {
        CaseEvent[] caseEvents = new CaseEvent[0];
        CaseEvent ce;
        //Get Case Comments
        CaseComment[] ccs;
        if(tViewType == 'internal')
            ccs = tvm.getCaseCommentsForInternalTView();            
        else if(tViewType == 'customer')    
            ccs = tvm.getCaseCommentsForCustomerTView();
        for(CaseComment cc : ccs)
        {
            ce = new CaseEvent();
            ce.eventDate = cc.LastModifiedDate;
            ce.eventType = 'Case Comment';
            ce.userName = cc.CreatedBy.Name;
            ce.action = cc.CommentBody;
            caseEvents.add(ce);
        }            
        //Get Activity History
        ActivityHistory[] ahs;
        if(tViewType == 'internal')
            ahs = tvm.getActivityHistoryForInternalTView();
        else if(tViewType == 'customer')    
            ahs = tvm.getActivityHistoryForCustomerTView();
        for(ActivityHistory ah : ahs)
        {
            ce = new CaseEvent();
            ce.eventDate = ah.ActivityDate;
            ce.eventType = 'Activity History';
            ce.userName = ah.Owner.Name;
            ce.action = ah.Subject;
            caseEvents.add(ce);
        }
        //Get Case History
        GenericHistory ghcc = new GenericHistory();
        ghcc.myObject = tvm.record;
        GenericHistory.ObjectHistoryLine[] ohls = ghcc.objectHistory;
        for(GenericHistory.ObjectHistoryLine ohl : ohls)
        {
            ce = new CaseEvent();
            ce.eventDate = ohl.theDate;
            ce.eventType = 'Case History';
            ce.userName = ohl.who;
            ce.action = ohl.action;
            caseEvents.add(ce);
        }
        //Get Open Activities
        OpenActivity[] oas = tvm.getOpenActivityForTView();
        for(OpenActivity oa : oas)
        {
            ce = new CaseEvent();
            ce.eventDate = oa.ActivityDate;
            ce.eventType = 'Open Activity';
            ce.userName = oa.Owner.Name;
            ce.action = oa.Subject;
            caseEvents.add(ce);
        }
        
        //Get Case Attachments
        Attachment[] ats = tvm.getAttachmentsForTView(); 
        for(Attachment a : ats)
        {
            ce = new CaseEvent();
            ce.eventDate = a.LastModifiedDate;
            ce.eventType = 'Attachment';
            ce.userName = a.LastModifiedBy.Name;
            ce.action = a.Name;
            caseEvents.add(ce);
        }
        
        return CaseEvent.sortByDate(caseEvents, false);
    }
    public static Id createProblemCase(Case srcCase)
    {
        Case caseToInsert = new Case();
        caseToInsert.RecordTypeId = getProblemRecordTypeId();
        caseToInsert.ContactId = srcCase.ContactId;
        caseToInsert.Status = 'Proposed';
        caseToInsert.AccountId = srcCase.AccountId;
        caseToInsert.OwnerId = [select Id from Group where Type = 'Queue' and Name = 'Problem Cases'].Id;
        caseToInsert.Account_Type__c = srcCase.Account_Type__c;
        caseToInsert.Priority = srcCase.Priority;
        caseToInsert.Account_Origin__c = srcCase.Account_Origin__c;
        caseToInsert.Severity__c = srcCase.Severity__c;
        caseToInsert.Contact_Origin__c = srcCase.Contact_Origin__c;
        caseToInsert.Origin = srcCase.Origin;
        //caseToInsert.Type = srcCase.Type; //No need to map this as the value is controlled by the record type
        caseToInsert.Service_Request_Type__c = srcCase.Service_Request_Type__c;
        caseToInsert.Extended_Support__c = srcCase.Extended_Support__c;
        caseToInsert.Closure_Code__c = srcCase.Closure_Code__c;
        // MHG 111104: Removed
        //caseToInsert.Reason = srcCase.Reason;
        caseToInsert.Needs_Handover__c = srcCase.Needs_Handover__c;
        caseToInsert.Do_Not_Send_Reminder_email__c = srcCase.Do_Not_Send_Reminder_email__c;
        // MHG 111104: Removed
        //caseToInsert.Pending_Customer_Reply_Start_time__c = srcCase.Pending_Customer_Reply_Start_time__c;
        caseToInsert.Customer_Modified_Case_Last__c = srcCase.Customer_Modified_Case_Last__c;
        caseToInsert.Partner_Support_Contact__c = srcCase.Partner_Support_Contact__c;
        caseToInsert.O_S__c = srcCase.O_S__c;
        caseToInsert.License_No__c = srcCase.License_No__c;
        caseToInsert.Environment__c = srcCase.Environment__c;
        caseToInsert.Client_Version__c = srcCase.Client_Version__c;
        caseToInsert.Product__c = srcCase.Product__c;
        caseToInsert.Server_Version__c = srcCase.Server_Version__c;
        caseToInsert.Publisher_Version__c = srcCase.Publisher_Version__c;
        caseToInsert.Area__c = srcCase.Area__c;
        caseToInsert.Connector_Version__c = srcCase.Connector_Version__c;
        // MHG 111104: Removed
        //caseToInsert.Issue__c = srcCase.Issue__c;
        caseToInsert.Customer_Patch_Version__c = srcCase.Customer_Patch_Version__c;
        caseToInsert.X3rd_Party_Software__c = srcCase.X3rd_Party_Software__c;
        caseToInsert.Subject = srcCase.Subject;
        caseToInsert.Description = srcCase.Description;
        caseToInsert.Resolution__c = srcCase.Resolution__c;
        caseToInsert.Steps_to_reproduce__c = srcCase.Steps_to_reproduce__c;
        caseToInsert.Bug_ID__c = srcCase.Bug_ID__c;
        // MHG 111104: Added
        caseToInsert.Client__c = srcCase.Client__c;
        
        insert caseToInsert;
        
        return caseToInsert.Id;
    }
    //Updates the Problem Case lookup field
    public static void updateProblemCaseField(Case incidentCase, Id problemCaseId)
    {
        incidentCase.Problem_Case2__c = problemCaseId;
        update incidentCase;
    }
    //This method creates a New Case from the Contact and return back the Case Id
    public static Case createNewCaseFromContact(Id contactId, Id recordTypeId)
    {
        Case newCaseFromContact = new Case();
        newCaseFromContact.RecordTypeId = recordTypeId;
        newCaseFromContact.ContactId = contactId;
        //Insert the new Case after setting the ContactId field
        insert newCaseFromContact;
        
        return newCaseFromContact;
    }
    //This method links the Case to the Account License and then updates the Case
    public static void updateCaseFromAcntLicense(Id caseId, Account_License__c acntLicense) {
        Case updateCaseWithAcntLicInfo = new Case(Id = caseId);
        updateCaseWithAcntLicInfo.Account_License__c = acntLicense.Id;
        updateCaseWithAcntLicInfo.Environment__c = acntLicense.Environment__c;
        updateCaseWithAcntLicInfo.License_No__c = acntLicense.Id == null ? '' : acntLicense.Name;
        
        //Update Case with the Account License Info
        update updateCaseWithAcntLicInfo;
    }
    
    //& updates new Cases created from Entitlements.
    public static void updateCasesFromAcntLicenseViaEntitlement(List<Case> cases) {
        //& updated 2011-10-13 to include setting default business hours.
        Set<ID> entitlements = new Set<ID>();        
        for (Case c : cases) {
            System.debug('updateCasesFromAcntLicenseViaEntitlement: Case c = ' + c); //cce temp            
            if (c.EntitlementId != null) {
                entitlements.add(c.EntitlementId);
            }
        }        
        if (entitlements.isEmpty()) {
            return;
        }
        Map<ID, ID> licenseToEntitlement = new Map<ID, ID>();
        Map<ID, ID> entitlementToBusinessHours = new Map<ID, ID>();
        Map<ID, ID> entitlementToRSM = new Map<ID, ID>();
        for (Entitlement e : [select Id, Account_License__c, BusinessHoursId, Account.Support_Office__r.Regional_Support_Manager__c from Entitlement where Id in:entitlements]) {
            licenseToEntitlement.put(e.Account_License__c, e.Id);
            entitlementToBusinessHours.put(e.Id, e.BusinessHoursId);            
            if (e.Account.Support_Office__r.Regional_Support_Manager__c != null) {
                entitlementToRSM.put(e.Id, e.Account.Support_Office__r.Regional_Support_Manager__c);
            }
        }        
        if (licenseToEntitlement.isEmpty()) {
            return;
        }        
        Map<ID, Account_License__c> entitlementToLicense = new Map<ID, Account_License__c>();
        for (Account_License__c al : [select Id, Name, O_S__c, Environment__c, Product__c, Area__c, Issue__c, X3rd_Party_Software__c, Client_Version__c,
                                                Server_Version__c, Publisher_Version__c, Connector_Version__c, Customer_Patch_Version__c
                                        from Account_License__c where Id in:licenseToEntitlement.keySet()]) {
            entitlementToLicense.put(licenseToEntitlement.get(al.Id), al);
        }        
        for (Case c : cases) {
            if (c.EntitlementId != null) {
                Account_License__c acntLicense = entitlementToLicense.get(c.EntitlementId);
                if (acntLicense != null) {             
                    c.Account_License__c = acntLicense.Id;
                    c.Environment__c = acntLicense.Environment__c;
                    c.License_No__c = acntLicense.Id == null ? '' : acntLicense.Name;
                }                                
                if (entitlementToBusinessHours.containsKey(c.EntitlementId) && entitlementToBusinessHours.get(c.EntitlementId) != null) {
                    c.BusinessHoursId = entitlementToBusinessHours.get(c.EntitlementId);
                }                
                //& Set Case RSM field if not populated (this avoids a trigger on Support_Office__c)
                //& the Case RSM is fixed to the RSM for the support office at the point of case creation or when the initial entitlement is added (creation or update). 
                if (entitlementToRSM.containsKey(c.EntitlementId) && entitlementToRSM.get(c.EntitlementId) != null && c.Regional_Support_Manager__c == null) {
                    c.Regional_Support_Manager__c = entitlementToRSM.get(c.EntitlementId);
                }
            }
        }       
    }
    
    //This method copies over the Technical Information fields from Case to the corresponding Account_License__c
    //CCE CR# 10129 - Fix CaseServices trigger. Added code to updateAccountLicenseTechInfo method to remove duplicates from acntAccountLicensesToUpdate List
    public static void updateAccountLicenseTechInfo(List<Case> cases) {
        //List of Account Licenses which need to be updated with the Technical Information from the Case
        List<Account_License__c> acntLicensesToUpdate = new List<Account_License__c>();
        Set<string> IdsToUpdate = new Set<string>();
        //Loop over the Cases which need to be used for updating Account Licenses   
        for (Case c : cases) {
            System.debug('updateAccountLicenseTechInfo: Case c = ' + c); //cce temp
            if (c.Account_License__c != null && c.Update_Technical_Information__c) {
                System.debug('updateAccountLicenseTechInfo: Case c = ' + c); //cce temp
                Account_License__c acntLicense = new Account_License__c(Id = c.Account_License__c);
                acntLicense.O_S__c = c.O_S__c;
                acntLicense.Environment__c = c.Environment__c;
                acntLicense.Product__c = c.Product__c;
                acntLicense.Area__c = c.Area__c;
                acntLicense.Issue__c = c.Issue__c;
                acntLicense.X3rd_Party_Software__c = c.X3rd_Party_Software__c;
                acntLicense.Client_Version__c = c.Client_Version__c;
                acntLicense.Server_Version__c = c.Server_Version__c;
                acntLicense.Publisher_Version__c = c.Publisher_Version__c;
                acntLicense.Connector_Version__c = c.Connector_Version__c;
                acntLicense.Customer_Patch_Version__c = c.Customer_Patch_Version__c;
                if (!IdsToUpdate.contains(c.Account_License__c)) {
                    acntLicensesToUpdate.add(acntLicense);
                    IdsToUpdate.add(c.Account_License__c);
                }
            }
        }
        
        if (!acntLicensesToUpdate.isEmpty()) {   //CR# 10129 - remove duplicates from acntLicensesToUpdate list
            Set<Account_License__c> deDup = new Set<Account_License__c>();
            List<Account_License__c> result = new List<Account_License__c>();

            deDup.addAll(acntLicensesToUpdate); //this line does the de-duplication
            result.addAll(deDup);   //and we put the Set back into a List as we cannot use update on Sets
            update result; 
        }
    }
    //This method fills in the Account License lookup based on the License No field value
    public static void updateCaseAccountLicenseLookup(Case[] cases)
    {
        //Get all license numbers from cases
        Set<String> licenseNums = new Set<String>();
        for(Case c : cases)
        {
            if(c.License_No__c != null)
                licenseNums.add(c.License_No__c);
        }
        //Get account licenses IDs using license numbers
        Map<String, Id> licensesMap = new Map<String, Id>();
        Account_License__c[] als = [select Id, Name from Account_License__c where Name in :licenseNums];
        for(Account_License__c al : als) licensesMap.put(al.Name, al.Id);
        //Update case account license lookups
        for(Case c : cases) c.Account_License__c = licensesMap.get(c.License_No__c);
    }

    // Updates the related Bug.Number_of_cases__c field
    public static void countForBugs(List<Case> newCases, List<Case> oldCases) {
        Case newCase = null;
        Case oldCase = null;
        Set<Id> setIds = new Set<Id>();
        for (Integer i = 0; i < newCases.size(); i++) {
            newCase = newCases[i];
            if (oldCases != null) {
                oldCase = oldCases[i];    
            }            
            if ((oldCase == null && newCase.Bug__c != null)
                || (oldCase != null && oldCase.Bug__c != newCase.Bug__c)
                || (newCase.Force_Triggers__c && newCase.Bug__c != null)) {
                setIds.add(newCase.Bug__c);
            }
        }
        if (setIds.size() > 0) {
            List<Bugs__c> bugsForUpdate = [SELECT id, number_of_cases__c, (SELECT id FROM Cases__r) FROM Bugs__c where id in: setIds];
            for (Bugs__c bug : bugsForUpdate) {
                if (bug.Cases__r != null && bug.Cases__r.size() > 0) {
                    bug.Number_of_cases__c = bug.Cases__r.size();
                } else {
                    bug.Number_of_cases__c = 0;
                }
            }
            update bugsForUpdate;
        }      
    }

    // CHG0033970
    public static void setPartnerPerformanceSection(List<Case> cases) {
        List<RecordType> types = [select Id from RecordType where SobjectType = 'Case' and DeveloperName = 'QlikTech_Master_Support_Record_Type' limit 1];
        if (!types.isEmpty()) {       
            for (Case c : cases) {
                if (c.RecordTypeId == types[0].Id && c.All_Partner_Requirements_met__c) {
                     c.Performance_Support_Entitlement__c = 'Yes';
                     c.Performance_Content_Utilization__c = 'Yes';
                     c.Performance_Communication__c  = 'Yes';
                     c.Performance_Case_Quality__c = 'Yes';
                     c.Performance_Troubleshooting__c = 'Yes';            
                     c.Performance_Collaboration_with_Qlik__c = 'Yes';
                     c.Performance_Reporting_Error_in_Qlik__c = 'Yes';
                     c.All_Partner_Requirements_met__c = false;
                }
            }
        }        
    }

    /*
    * Change Log: 
    * 2013-05-14 Brian Tan: CR# 7953 Initial Development (https://eu1.salesforce.com/a0CD000000YFWXq)
    * 2017-01-24 ext_bad:   LCE-48      Add Live Chat record type
    * 2018-09-28 ext_vos    Move from CustomCaseTriggerHandler. Refactoring.
    */
    public static void handleCaseEndUserAccountNameCopy(List<Case> cases) {
        Set<String> recordTypeIds = new Set<String>();
        recordTypeIds.add(getCaseRecordTypeIdDeveloperName('QlikTech_Master_Support_Record_Type'));       // 01220000000DZqG (QlikTech Master Support Record Type)
        recordTypeIds.add(getCaseRecordTypeIdDeveloperName('QT_Support_Partner_Portal_Record_Type'));     // 01220000000DZgI (QT Support Customer Portal Record Type)
        recordTypeIds.add(getCaseRecordTypeIdDeveloperName('QT_Support_Customer_Portal_Record_Type'));    // 01220000000DZqG (QlikTech Master Support Record Type)
        recordTypeIds.add(getCaseRecordTypeIdDeveloperName('Live_Chat_Support_Record_Type'));             // 012D0000000kiIk (Live Chat Support Record Type)

        for (Case c : cases) {
            if (c.Account_Origin__c == null && c.AccountId != null && recordTypeIds.contains(c.RecordTypeId)) {
                c.Account_Origin__c = c.AccountId;              
            }
        }
    }

    public static void populateResolutionDate(List<Case> cases, Map<Id, Case> oldCasesMap) {
        for (Case cs : cases) {
            if ((oldCasesMap == null && !String.isEmpty(cs.Resolution_Provided__c))
                    || (oldCasesMap != null && cs.Resolution_Provided__c != oldCasesMap.get(cs.Id).Resolution_Provided__c)) {
                cs.Resolution_Date__c = System.now();
            }
        }
    }

    private static String getCaseRecordTypeIdDeveloperName(String developerName) {
        String rtId = null;
        
        RecordTypeInfo info = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(developerName);
        if (info != null) {
            rtId = info.getRecordTypeId();
        }
        return rtId;
    }
}