/*
// 20-05-2014 MTM: CR# 12738 Change Eventforce registration for internal users for new events  
*/
public class efRegistrationMailBuilder{
    private Registration__c registration;
    private String registrationId;
    private Contact registrant;
    private String naText = '';
    private String condition = '';
    private String textTrue = '';
    private String textFalse = '';
    
    public Registration__c getRegistration(){        
        if (registration != null)
            return registration;        
        // We're not using custom settings here for the different records status,
        // cause this is invoked from a trigger and we can't spend a SOQL query
        Registration__c[] allRegistrations = [Select Id,
                                                     	Portal_Attendee_Type__c,
                                                     	Attendee_SubType__c,
                                                     	Has_Extended_Stay_Addon__c,
											 			Extended_Stay_End_Date__c,
											 			Extended_Stay_Start_Date__c,
                                                        Camp_Invite__c,                                                     	
                                                     	Registrant__r.FirstName,
                                                        Registrant__r.LastName,
                                                     	Registrant__r.Name,
                                                     	Registrant__r.Account.Name,
                                                     	Registrant__r.email,
                                                     	Registrant__r.MailingStreet,
                                                     	Registrant__r.MailingCity,
                                                     	Registrant__r.MailingState,
                                                     	Registrant__r.MailingPostalCode,
                                                     	Registrant__r.MailingCountry,
														Event__r.Id,
                                                     	Event__r.Name,
                                                     	Event__r.Event_Start_Date__c,
                                                     	Event__r.Event_End_Date__c,
                                                     	Event__r.Confirmation_Email_Label_Accommodation__c,
                                                     	Event__r.Confirmation_Email_Label_Contact__c,
                                                     	Event__r.Website__c,
                                                     	Event__r.Contact_Email__c,
                                                     	Event__r.Venue_City__c,
                                                     	Event__r.Venue_Name__c,
                                                    	Event__r.Venue_Website__c,
                                                        Event__r.Venue_Country__c,  
                                                        Event__r.Venue_State__c, 
                                                        Event__r.Venue_Zip__c,
                                                        Event__r.Arrival_date__c,                           
	                                                    (
		                                                	Select Amount,
		                                                            Id,
		                                                            StageName
		                                                     From Opportunities__r
	                                                     ),
	                                                     (
	                                                         Select Amount__c,
	                                                                Card_Suffix__c,
	                                                                Card_Type__c,
	                                                                Transaction_Date__c,
	                                                                CreatedDate
	                                                         From R00N70000001wLkTEAU
	                                                         order by CreatedDate desc
	                                                     ),
	                                                     (
	                                                         Select Id,
	                                                                UnitPrice,
	                                                                PricebookEntry.Product2.Name,
	                                                                PricebookEntry.Product2.Family,
	                                                                PricebookEntry.Product2.Startdate__c,
	                                                                PricebookEntry.Product2.Enddate__c,
	                                                                PricebookEntry.Product2.Room__r.Name
	                                                         From R00N70000001wy3TEAQ
	                                                         Where Status__c = 'Added'
	                                                     ),
	                                                     (
	                                                        Select Session__r.Name,
	                                                               Session__r.Session_Start_Time__c,
	                                                               Session__r.Session_End_Time__c,
	                                                               Session__r.Session_Abstract__c,
	                                                               Session__r.Room__r.Name,
	                                                               Session__r.RecordTypeId,
	                                                               RecordTypeId,
	                                                               Best_Practice__c,
	                                                               Technical_Issue__c,
	                                                               Areas_of_Interest__c
	                                                        From R00N70000001wymrEAA
	                                                        Where Status__c = 'Registered'
	                                                        order by Session__r.Session_Start_Time__c
	                                                     )
                                              From Registration__c
                                              Where Id=:registrationId
                                              Order By LastModifiedDate Desc];
        registration = allRegistrations[0];        
        return registration;
    }
	
    public void setRegistrant(Contact registrant){
        this.registrant = registrant;
    }
    
    public Contact getRegistrant(){
        return registrant;
    }
    
    public void setRegistrationId(String registrationId){
    	this.registrationId = registrationId;
    }
    
    public String getRegistrationId(){
    	return registrationId;
    }
    
    public void setNAText(String naText){
    	this.naText = naText;
    }
    
    public String getNAText(){
    	return naText;
    }
    
    public String getAttendeeFirstName() {
    	Registration__c r = getRegistration();
    	return r.Registrant__r.FirstName;
    }

    public String getAttendeeLastName() {
        Registration__c r = getRegistration();
        return r.Registrant__r.LastName;
    }
    
    public String getAttendeeName() {
        Registration__c r = getRegistration();
        return r.Registrant__r.Name;
    }
    
    public String getAccountName() {
        Registration__c r = getRegistration();
        return r.Registrant__r.Account.Name;
    }
    
    public String getAttendeeMail() {
        Registration__c r = getRegistration();
        return r.Registrant__r.email;
    }
    
    public String getAttendeeMailingStreet() {
        Registration__c r = getRegistration();
        return r.Registrant__r.MailingStreet;
    }
    
    public String getAttendeeMailingCity() {
        Registration__c r = getRegistration();
        return r.Registrant__r.MailingCity;
    }
    
    public String getAttendeeMailingState() {
        Registration__c r = getRegistration();
        return r.Registrant__r.MailingState;
    }
    
    public String getAttendeeMailingPostalCode() {
        Registration__c r = getRegistration();
        return r.Registrant__r.MailingPostalCode;
    }
    
    public String getAttendeeMailingCountry() {
        Registration__c r = getRegistration();
        return r.Registrant__r.MailingCountry;
    }
    
    public String getAttendeeType() {
        Registration__c r = getRegistration();
    	return r.Portal_Attendee_Type__c;
    }
            
    public String getEventName(){
        Registration__c r = getRegistration();
    	return r.Event__r.Name;    	
    }
    
    public String getEventWebsite(){
        Registration__c r = getRegistration();
    	return r.Event__r.Website__c;
    }
    
    public String getEventContactEmail(){
        Registration__c r = getRegistration();
    	return r.Event__r.Contact_Email__c;
    }
    public String getInvitedTo(){
        Registration__c r = getRegistration();
        return r.Camp_Invite__c;
    }

    public String getEventFormattedStartDate(){
    	Registration__c r = getRegistration();
    	return efUtility.getFullFormatedDateGMT(r.Event__r.Arrival_date__c);    
    }
    
    public String getEventFormattedEndDate(){
    	Registration__c r = getRegistration();
    	return efUtility.getFullFormatedDateGMT(r.Event__r.Event_End_Date__c);    
    }    
    
    public String getVenueCity(){
    	Registration__c r = getRegistration();
    	return r.Event__r.Venue_City__c;
    }
    
    public String getVenueCombined(){
    	Registration__c r = getRegistration();    	
    	return String.format('{0} {1} {2} {3} {4} ({5})', new String[] {r.Event__r.Venue_Name__c, r.Event__r.Venue_City__c, r.Event__r.Venue_State__c, r.Event__r.Venue_Zip__c,r.Event__r.Venue_Country__c,r.Event__r.Venue_Website__c}); 
    }
    
    public String getAccommodationString(){
    	Registration__c r = getRegistration();

	    String html='';
	    
	    if (r.Event__r.Confirmation_Email_Label_Accommodation__c!=null){
	    	html+=r.Event__r.Confirmation_Email_Label_Accommodation__c;
	    }
    	if (r.Has_Extended_Stay_Addon__c){
			html+='     Extended Stay, ';
			html+='Arrival : '+r.Extended_Stay_Start_Date__c;
			html+='Departure : '+r.Extended_Stay_End_Date__c;
    	}
        return html;    	    	
    }
    
    public String getContactString(){
        Registration__c r = getRegistration();
    	return r.Event__r.Confirmation_Email_Label_Contact__c;    	
    }
    
    public String getTotalAmount(){        
        double amount = 0;
        
        Registration__c r = getRegistration();
        
        for(Opportunity o:r.Opportunities__r){			
			// We could have cancelled Opportunities related to the Registration following failed payment.
            if (o.Amount != null && o.StageName != 'Cancelled') {
                amount =  amount +  o.Amount ;               
            }
        }
        return '$' + efUtility.formatDecimals(String.valueOf(amount));
    }
    
    public String getFormattedDate() {
    	return efUtility.getFormatedDateYear(DateTime.now());
    }
    
    public String getPaymentString(){
        String paymentString = '';
        
        Registration__c r = getRegistration();
        
        Payment_Details__c[] pdList = r.R00N70000001wLkTEAU;
                                       
        if (pdList != null && pdList.size() > 0){
            paymentString = '<br>Payment received date: ' + efUtility.getDateFormated(pdList[0].Transaction_Date__c);
        }
        else{
        	paymentString = naText;
        }        
        return paymentString;
    }

    public String getRecentPaymentString(){
        String paymentString = '';        
        Registration__c r = getRegistration();        
        Payment_Details__c[] pdList = r.R00N70000001wLkTEAU;
                                       
        if (pdList != null && pdList.size() > 0){
        	for (Payment_Details__c pd:pdList){
        		if (pd.CreatedDate.addMinutes(5) > DateTime.now()){
		            paymentString = 'Amount paid: ' + efUtility.formatDecimals(String.valueOf(pd.Amount__c)) +
		                            '<br/>Payment received date: ' + efUtility.getDateFormated(pd.Transaction_Date__c);

                    break;
        		}
        	}
        }

        if (paymentString.length() == 0){
            paymentString = naText;
        }        
        return paymentString;
    }

    public String getConfirmedTrainingItems(){
        String ritems = '';
        String html ='';
        
        Registration__c r = getRegistration();
        
        for (OpportunityLineItem oli:r.R00N70000001wy3TEAQ){
	        if (!efUtility.isNull(oli.PricebookEntry.Product2.Family) &&
	                 oli.PricebookEntry.Product2.Family.trim().equals(efConstants.PROD_FAMILY_CLASS)) {   
	            
	            ritems = ritems  + '<tr><td class="eduName">' +
	                     oli.PricebookEntry.Product2.Name +
	                     '</td><td class="eduTime">';
		            
	            DateTime stdt = oli.PricebookEntry.Product2.Startdate__c ;
                DateTime eddt = oli.PricebookEntry.Product2.Enddate__c ;
        
                String formatedDate = '';
        
		        if(stdt!=null && eddt!=null)
		            formatedDate = stdt.format('E, MMM d') +
		                           ', ' + efUtility.getFormatedTimeWithAM(stdt) +
		                           '-' + efUtility.getFormatedTimeWithAM(eddt);
	            
		        if(!stdt.isSameDay(eddt))
		            formatedDate = formatedDate + '<br/>' + eddt.format('E, MMM d') +
		                           ', ' + efUtility.getFormatedTimeWithAM(stdt) +
		                           '-' + efUtility.getFormatedTimeWithAM(eddt);

                ritems += formatedDate + '</td></tr>';
	        }
        }

        if (ritems.length() > 0){
            html = '<table class="educationItems">';
            html = html + ritems;
            html = html + '</table>';
        }
        else {
            html = naText;
        }        
        return html;
    }
    
    public String getRegistrationItems(){
        String ritems = '';
        String html ='';
        
        Registration__c r = getRegistration();
        
        for (OpportunityLineItem oli:r.R00N70000001wy3TEAQ){        
            if (!efUtility.isNull(oli.PricebookEntry.Product2.Family) &&
               (oli.PricebookEntry.Product2.Family.trim().equals(efConstants.PROD_FAMILY_REGISTRATION) ||
               	oli.PricebookEntry.Product2.Family.trim().equals(efConstants.PROD_FAMILY_ADDON))) {   
                
                ritems = ritems + '<tr><td>' +
                         oli.PricebookEntry.Product2.Name +
                         '</td><td>&nbsp;&nbsp;$' +
                         efUtility.formatNegative(efUtility.formatDecimals(String.valueOf(oli.UnitPrice))) +
                         '</td></tr>';
            }
            else if (!efUtility.isNull(oli.PricebookEntry.Product2.Family) &&
                     oli.PricebookEntry.Product2.Family.trim().equals(efConstants.PROD_FAMILY_CLASS)) {   
                
                ritems = ritems  + '<tr><td>' +
                         oli.PricebookEntry.Product2.Name +
                         '</td><td>&nbsp;&nbsp;$' +
                         efUtility.formatNegative(efUtility.formatDecimals(String.valueOf(oli.UnitPrice))) +
                         '</td></tr>';
            }
            else if (!efUtility.isNull(oli.PricebookEntry.Product2.Family) &&
                     oli.PricebookEntry.Product2.Family.trim().equals(efConstants.PROD_FAMILY_PROMO)) {   
                    
                ritems = ritems + '<tr><td>' +
                         oli.PricebookEntry.Product2.Name +
                         '</td><td>&nbsp;&nbsp;$' +
                         efUtility.formatNegative(efUtility.formatDecimals(String.valueOf(oli.UnitPrice))) +
                         '</td></tr>';
            }            
        }

        if (ritems.length() > 0){
	        html = '<table class="registrationItems" width="100%">'
	        + '<tr><td>Items Purchased:</td><td>&nbsp;</td></tr>';
        
            html = html + ritems;   
            html = html + '</table>';
        }        
        return html;
    }    
}