/****************************************************** 

Class: TEST_Opportunity_UpdateSolutionsProfile 

This class tests the Opportunity_UpdateSolutionsProfile trigger 

Changelog: 
2012-10-22 SAN testing of filling data in field for Solutions Profile 
2014-05-07 SAN change the re ord type id for opp to Qlikbuy CCS Standard II
2015-05-04 SLH Added function and solution area for Op006 validation rule
2015-10-28 IRN Winter 16 Issue - removed seeAllData and called globalSetup instead
*  17.03.2017 : Rodion Vakulvsokyi 
2017-03-26  TJG Fix INVALID_CROSS_REFERENCE_KEY, Record Type ID: this ID value isn't valid for the user: 012D0000000KEKOIA4
2017-06-23 Rodion Vakulvoskyi, updated due to Oppty trigger refactoring 
******************************************************/ 
@IsTest
public with sharing class TEST_Opportunity_UpdateSolutionsProfile {

    public static testMethod void EndUserQlikBuyCCS_Test() {
        System.debug('EndUserQlikBuyCCS_Test: Starting');

        string Sector = 'Financial Services';
        string Industry = 'Banking';

        QTTestUtils.GlobalSetUp();

        SIC_Code__c sicTest = new SIC_Code__c();
        sicTest.Name = 'test';
        sicTest.QlikTech_Sector__c = Sector;
        sicTest.Industry__c = Industry;
        insert sicTest;

         
        test.startTest();
        
        // Adding an account with BillingCountryCode and Qliktech Company
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        
        Account acc = new Account(Name = 'Test opportunity update', 
                            SIC_Code_Name__c = sicTest.Id, 
                            RecordTypeId = '01220000000DOFu', 
                            QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, 
                            Billing_Country_Code__c = QTComp.Id);
        insert acc;

        RecordType rt = [SELECT Id FROM RecordType WHERE Name like '%Sales_QCCS%' limit 1];
        
        Opportunity testOpp = new Opportunity(
        Name = 'Test Opp 1',
        StageName = 'Goal Identified',
        CloseDate = Date.today(),
        AccountId = acc.Id,
        function__c = 'Other',
        solution_area__c = 'Other',
        Signature_Type__c = 'Digital Signature',
        RecordTypeId = rt.Id      //'012D0000000KEKO'
        );
        insert testOpp;        
        Semaphores.OpportunityTriggerBeforeUpdate = false;
       	Semaphores.OpportunityTriggerAfterUpdate = false;
        Opportunity  testOpp2 = [SELECT StageName from Opportunity WHERE Id=:testOpp.Id];
        testOpp2.StageName = 'Goal Confirmed';
        testOpp2.Included_Products__c = 'Qlik Sense';
        update testOpp2;
                                
        testOpp2 = [SELECT Sector__c, Industry__c from Opportunity WHERE Id=:testOpp.Id];

        System.debug('emelie ' + testOpp2);
        System.assertEquals(Sector, testOpp2.Sector__c, 'Sector update failed');
        System.assertEquals(Industry, testOpp2.Industry__c, 'Industry update failed');

        test.stopTest();


        System.debug('EndUserQlikBuyCCS_Test: Finished');

    }

    public static testMethod void PartnerAccountCCS_Test() {
        System.debug('PartnerAccountCCS_Test: Starting');
        QTTestUtils.GlobalSetUp();
        test.startTest();

        string Sector = 'Manufacturing & High Tech';
        string Industry = 'Automotive, Industrial & Aerospace';

        SIC_Code__c sicTest = new SIC_Code__c();
        sicTest.Name = 'test';
        sicTest.QlikTech_Sector__c = Sector;
        sicTest.Industry__c = Industry;
        insert sicTest;
        
        // Adding an account with BillingCountryCode and Qliktech Company
		Subsidiary__c testSubs1 = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c QTComp = new QlikTech_Company__c(
            Name = 'GBR',
            QlikTech_Company_Name__c = 'QlikTech UK Ltd',
			Subsidiary__c = testSubs1.id            
        );
        insert QTComp;
        QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
        
        Account acc = new Account(Name = 'Test', SIC_Code_Name__c = sictest.Id, RecordTypeId = '01220000000DOFz', QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
        insert acc;
        
        Opportunity testOpp = new Opportunity(
        Name = 'Test Opp 1',
        StageName = 'Goal Identified',
        CloseDate = Date.today(),
        AccountId = acc.Id,
        Signature_Type__c = 'Digital Signature',
        RecordTypeId = [select id From recordType where DeveloperName = 'Sales_QCCS'].id
        );
        insert testOpp;        
        
       
        
      Opportunity  testOpp2 = [SELECT StageName from Opportunity WHERE Id=:testOpp.Id];
      
      testOpp2.StageName = 'Goal Confirmed';
      testOpp2.Included_Products__c = 'Qlik Sense';
      update testOpp2;
                                
      testOpp2 = [SELECT Sector__c, Industry__c from Opportunity WHERE Id=:testOpp.Id];
         
 //   System.assertEquals(Sector, testOpp2.Sector__c, 'Sector update failed');
  //  System.assertEquals(Industry, testOpp2.Industry__c, 'Industry update failed');

        test.stopTest();


        System.debug('PartnerAccountCCS_Test: Finished'); 
    }

}