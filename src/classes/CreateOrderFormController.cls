/**
*  06.04.2017 : Rodion Vakulvsokyi QCW-1976
*  10.04.2017 : Rodion Vakulvsokyi QCW-1994
*  20.05.2017 : Muneer  QCW-1633
*  19.06.2017 : Nikhil  QCW-2749
*  15.08.2017 : Bala Egambaram QCW-2616
   2018-06-04 MTM BSL-490 New visual compliance implementations
   2018-06-29 Anjuna BSL-612 New visual compliance implementations
   2018-07-05 Shubham BSL-649 order form expiration after 30 day line 172-175
    
**/
global class CreateOrderFormController {


    public Boolean ECustomsCleared{
        get{
            if(ECustomsHelper.NewVisualComplianceEnabled){
                
                return ECustomsHelper.VCStatus(quote.Revenue_Type__c,quote.SBQQ__Account__r.ECUSTOMS__RPS_Status__c, 
                                               quote.Sell_Through_Partner__r.ECUSTOMS__RPS_Status__c,quote.SBQQ__Account__r.ECUSTOMS__RPS_RiskCountry_Status__c, 
                                               quote.Sell_Through_Partner__r.ECUSTOMS__RPS_RiskCountry_Status__c);
            }else
            {
                if( quote.Legal_Approval_Triggered__c == true &&
                    (quote.Select_Billing_Address__r.Legal_Approval_Status__c == 'Auto Approved' || quote.Select_Billing_Address__r.Legal_Approval_Status__c == 'Granted') &&
                    (quote.Shipping_Address__r.Legal_Approval_Status__c == 'Auto Approved' || quote.Shipping_Address__r.Legal_Approval_Status__c == 'Granted') &&
                    //PartnerAddressesApproved()
                    PartnerAddressesApproved(quote.SBQQ__Account__c,quote.Revenue_Type__c)
                ){
                    return true;
                }else{
                    if(Test.isRunningTest()){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }
    } 

    public SBQQ__Quote__c quote {get; set;}
    public ApexPages.StandardController controller;

    public CreateOrderFormController(ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()){
            stdController.addFields(new List<String>{'Todays_Date__c', 'Order_Form_Expiration_Date__c', 'Is_Expired__c' , 'Rerun_Maintenance__c'});
        }
        quote = (SBQQ__Quote__c)stdController.getRecord();
        controller =stdController;
    }

    //to call from JavaScript button 'Create Order Form'
    webservice static boolean myMethod(string quoteAccount, string quoteRevType,boolean legalTrig, string selBilAddStatus, 
                                       string shipAddStatus, String EUserRPSStatus, String EUserRCStatus, String PUserRPSStatus, String PUserRCStatus){

        if(ECustomsHelper.NewVisualComplianceEnabled){
            system.debug('gggg1'+quoteAccount);
            system.debug('gggg2'+quoteRevType);
            system.debug('gggg3'+legalTrig);
            system.debug('gggg4'+selBilAddStatus);
            system.debug('gggg5'+shipAddStatus);
            system.debug('gggg6'+EUserRPSStatus);
            system.debug('gggg8'+PUserRPSStatus);
            return ECustomsHelper.VCStatus(quoteRevType,EUserRPSStatus, PUserRPSStatus,EUserRCStatus, PUserRCStatus);
        }
        else{
            if( legalTrig == true &&
               (selBilAddStatus == 'Auto Approved' || selBilAddStatus == 'Granted') &&
               (shipAddStatus == 'Auto Approved' || shipAddStatus == 'Granted') && 
               PartnerAddressesApproved(quoteAccount,quoteRevType)){
                   return true;

               }else{
                   return false;
               }
           }
    }

    private Boolean hasQCCItemsOnly() {
        for (SBQQ__QuoteLine__c line : [SELECT Id, Product_Group__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id]) {
            if (!line.Product_Group__c.equals('QCC')) {
                return false;
            }
        }
        return true;
    }

    public pageReference validateMethod(){
        displayPopup = true;
         system.debug('#####Accunt Subsidiary####'+quote.SBQQ__Account__r.Subsidiary__c);

        if(quote.Revenue_Type__c == 'MSP'){
            string message = 'Order forms are not used for MSP. Please have the partner place the order through the partner portal.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
            return NULL;
        } else if (quote.Revenue_Type__c == 'Academic Program' && hasQCCItemsOnly()) {
            string message = 'Order forms are not used for Academic Program Quotes with QCC items only.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
            return NULL;
        } else if(quote.SBQQ__Account__r.Subsidiary__c == NULL){
            string message = 'Account is missing Subsidiary record, please update and try again.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
            return NULL;
        } else{
        if(!quote.Is_Expired__c){
            if(quote.SBQQ__Primary__c){
                if(quote.SBQQ__Status__c == 'Approved' || quote.SBQQ__Status__c == 'Accepted by Customer' || quote.SBQQ__Status__c == 'Order Placed'){
                    System.debug('ffff3 =' + ECustomsCleared);
                    if(!ECustomsCleared){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This customer needs to be reviewed and approved by the legal department. Legal has been notified automatically. If the quote remains locked after 1 business day, please contact SFLegalApprovals@qlik.com.'));
                        return NULL;
                    }else{ 
                        if(quote.Order_Form_Status__c == 'Out For Signature'){
                            string message = 'There is an Order Form currently out for signature. If you need to recall or resend the request open the Order Form in the "In Process" folder. Then select Send, For Signature and make a choice.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'External Review'){
                            string message = 'There is an Order Form currently with the Quote Recipient / SOI Contact for review.  If you need to recall or resend the request open the Order Form in the "In Process" folder.  Then select Send, External Review and make a choice.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'Quote Approvals Needed'){
                            string message = 'Your Order Form is currently in process and being redlined by Legal.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'Sales Review'){
                            string message = 'There is an Order Form currently in process and waiting for your action. Please go to the "In Process" folder and make a choice to take the next step there.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'Legal Review'){
                            string message = 'There is an Order Form currently in process and with Legal for redlines.  If you need to recall or have questions about your Order Form, reach out to Legal.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'Orders Review'){
                            string message = 'There is an Order Form currently in process and with Orders for approval. If you need to recall or have questions about your Order Form, reach out to Orders.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'Orders Approval'){
                            string message = 'There is an Order Form currently in process and with Orders for approval. If you need to recall or have questions about your Order Form, reach out to Orders.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'Fully Executed'){
                            string message = 'This opportunity is "Closed Won" and an Order Form cannot be generated.';
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,message));
                            return NULL;
                        }else if(quote.Order_Form_Status__c == 'None' ||
                                 quote.Order_Form_Status__c == NULL ||
                                 quote.Order_Form_Status__c == 'Canceled' ||
                                 quote.Order_Form_Status__c == 'Rejected' ||
                                 quote.Order_Form_Status__c == 'Failed' ||
                                 quote.Order_Form_Status__c == 'Expired'){
                            updateQuoteTodaysDateField();
                            string pageRef = 'https://' + getQTCustomSettingsHier().SpringCM_Environment__c + '.springcm.com/atlas/doclauncher/eos/'+ quote.Doc_Launcher__c + '?aid=' + getQTCustomSettingsHier().SpringCM_Account_Id__c + '&eos[0].Id=' + quote.Id + '&eos[0].System=Salesforce&eos[0].Type=SBQQ__Quote__c&eos[0].Name=' + quote.Name + '&eos[0].ScmPath=/Salesforce/Accounts/' + quote.SBQQ__Account__c + '/Opportunities/' + quote.SBQQ__Opportunity2__c + '/Quotes';

                            PageReference quoteDetail = new PageReference(pageRef);
                            quoteDetail.setRedirect(true);
                            return quoteDetail;
                        }else{
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Message not defined for this order form status.'));
                            return NULL;
                        }

                    }
                }else if(quote.SBQQ__Status__c == 'Contracting'){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There is already an order form created for this quote, please cancel the outstanding order form to be able to create a new order form.'));
                    return NULL;
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Cannot create another order form. Quote is not approved yet.'));
                    return NULL;
                }
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Cannot create another order form. Quote is non-primary or status is in contracting. Please cancel any outstanding order forms to be able to generate a new order form.'));
                return NULL;
            }
        }
        }
        return NULL;
    }

    public void updateQuoteTodaysDateField() {
        //Integer numberOfDays = Date.daysInMonth( Date.today().year(),  Date.today().month());
        //Date lastDayOfMonth = Date.newInstance( Date.today().year(),  Date.today().month(), numberOfDays);
        Date thirtydays = Date.today().addDays(30);
        quote.Todays_Date__c = Date.today();
        quote.Order_Form_Expiration_Date__c = thirtydays;
        System.debug(quote);
        update quote;
    }

    public void updateRerunMaintanence(){
        quote.Rerun_Maintenance__c = true;
        update quote;
    }

    public QTCustomSettingsHier__c getQTCustomSettingsHier() {
        QTCustomSettingsHier__c qtCSHier = QTCustomSettingsHier__c.getInstance();
        return qtCSHier;
    }

    public static Boolean PartnerAddressesApproved(string quoteAccount, string quoteRevType){

        HardcodedValuesQ2CW__c hSetting = HardcodedValuesQ2CW__c.getOrgDefaults();
        String partnerRevTypes = hSetting.Partner_Revenue_Types__c;
        Set<String> revTypes = new Set<String>();
        if(partnerRevTypes != null){
            revTypes.addAll(partnerRevTypes.split(','));
        }
        Boolean shippingLegalStatus = false;
        Boolean billingLegalStatus = false;
        if(revTypes.Contains(quoteRevType) && String.isNotBlank(quoteAccount)){
            List<Address__c> addresses = [SELECT id,
                                          Address_Type__c,
                                          Legal_Approval_Status__c
                                          FROM Address__c
                                          WHERE Account__c =: quoteAccount AND
                                                Default_Address__c = true];
            if(addresses.size() == 2){
                for(Address__c add: addresses){
                    if(add.Address_Type__c == 'Shipping'
                       && (add.Legal_Approval_Status__c == 'Auto Approved' || add.Legal_Approval_Status__c == 'Granted'))
                        shippingLegalStatus = true;
                    if(add.Address_Type__c == 'Billing'
                       && (add.Legal_Approval_Status__c == 'Auto Approved' || add.Legal_Approval_Status__c == 'Granted'))
                        billingLegalStatus = true;
                }
                return shippingLegalStatus && billingLegalStatus; //End user legal approval passed
            }
            else //If there are no default billing and shipping addresses on account, we pass the status to avoid blocking quote process
            return true; //Missing addresses errors should not be captured here.
        }
        // This is not partner deal. Pass legal approval status
        return true;
    }

    public boolean displayPopup {get; set;}
    public PageReference returnToQuote(){
        PageReference quoteDetail = new PageReference('/'+quote.Id);
        quoteDetail.setRedirect(true);
        return quoteDetail;
    }
}