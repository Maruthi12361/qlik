/**************************************************
Change Log:
RDZ     20140923  Adding test class for trigger User_UpdateExternalSAMLId and to reflect changes for (NS Issue # 2647)  
14/12/2017 Pramod Kumar V Increase Test class coverage
***************************************************/


@isTest
private class User_UpdateExternalSAMLIdTest
{
    static String ExternalSAMLFormat = '{0}@qlikview.com';
    
    @isTest
    static void TwoUsersWithPrimeUserForNormalEmployeeTest()
    {
        // Given
        //Two SFDC users with one prime account for NS
        User usrPrime = QTTestUtils.createMockSystemAdministrator();
        User usrSub = QTTestUtils.createMockOperationsAdministrator();
       

        // When
        //Prime account is set, 
        
        usrSub.Prime_User__c= usrPrime.Id;
        usrPrime.FederationIdentifier = 'zzz';
        usrSub.FederationIdentifier = 'zzz1';
        //insert usrSub;
        try{
        
            //Prime user should be updated prior sub user, as they will need to copy ExternalId on trigger.
            update usrPrime;
           
            update usrSub;
            usrSub.email='abc@qliktech.com';
            usrSub.FederationIdentifier = 'zzz123';
            usrSub.Username='abc@qliktech.com.qa';
            
            update usrSub;  
            
            
        }
        catch(Exception e)
        {
            System.assert(false, String.format('We do not expect a exception {0}-StackTrace {1}', new String[]{e.getMessage(), e.getStackTraceString()}));
        }
        

        // Then, will get the right external saml id

        usrPrime = [select Id, Name, ExternalSAMLId__c, Alias, Prime_User__c, FederationIdentifier  from User where Id = :usrPrime.Id];
        usrSub = [select Id, Name, ExternalSAMLId__c, Alias, Prime_User__c, FederationIdentifier  from User where Id = :usrSub.Id];
        
        //With prime user we should get the same externalSAMLId for user1 and 2 and will be FedId + @qlikview.com
        System.assertEquals(usrPrime.ExternalSAMLId__c, usrSub.ExternalSAMLId__c);
        System.assertEquals(String.format(ExternalSAMLFormat,new String[]{usrPrime.FederationIdentifier}), usrPrime.ExternalSAMLId__c);
        System.assertEquals(String.format(ExternalSAMLFormat,new String[]{usrPrime.FederationIdentifier}), usrSub.ExternalSAMLId__c);
    }

    @isTest
    static void TwoUsersWithNoPrimeUserForNormalEmployeeTest()
    {
        // Given
        //Two SFDC users with one prime account for NS
        User u1 = QTTestUtils.createMockSystemAdministrator();
        User u2 = QTTestUtils.createMockOperationsAdministrator();

        // When
        //Prime account is set, 
        u1.FederationIdentifier = 'zzz';
        u2.FederationIdentifier = 'zzz1';

        update new User[]{u1, u2};

        // Then, will get the right external saml id

        u1 = [select Id, Name, ExternalSAMLId__c, Alias, Prime_User__c, FederationIdentifier  from User where Id = :u1.Id];
        u2 = [select Id, Name, ExternalSAMLId__c, Alias, Prime_User__c, FederationIdentifier  from User where Id = :u2.Id];
        
        //With No prime user we should get the externalSAMLId for user1 and 2 as their own FedId + @qlikview.com
        System.assertNotEquals(u1.ExternalSAMLId__c, u2.ExternalSAMLId__c);
        System.assertEquals(String.format(ExternalSAMLFormat,new String[]{u1.FederationIdentifier}), u1.ExternalSAMLId__c);
        System.assertEquals(String.format(ExternalSAMLFormat,new String[]{u2.FederationIdentifier}), u2.ExternalSAMLId__c);
    }

    @isTest
    static void PartnerUserTest()
    {
        // Given
        //Two SFDC users with one prime account for NS
        User u = QTTestUtils.createMockPRMBASEPartnerUser();
        
        // When
        //Partner User is inserted, ExternalSAMLId__c should be set to its email, 
        u = [select Id, Name, Email, ExternalSAMLId__c, Alias, Prime_User__c, FederationIdentifier  from User where Id = :u.Id];
        // Then, will get the right external saml id which is their email address as externalSAMLId
        
        System.assertEquals(u.Email, u.ExternalSAMLId__c);
    }
}