global class EntitlementAutomaticBatch implements Database.Batchable<SObject> {

	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(
			[SELECT Id, Automatic__c FROM Entitlement 
			 WHERE Automatic__c = false AND 
			 	Id NOT IN :new Set<Id>{'550D0000000DwpaIAC', '550D0000000DysiIAC', '550D0000000DeJQIA0', '550D0000000ChjiIAC', '550D0000000ChjdIAC', 
										 '550D0000000ChjnIAC', '550D0000000ChjxIAC', '550D0000000ChjsIAC', '550D0000000DwnTIAS', '550D0000000DWd5IAG'}]);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> scope){    
		List<Entitlement> es = (List<Entitlement> )scope;   
        for(Entitlement e : es) e.Automatic__c = true;
        update scope;
	}
	
	global void finish(Database.BatchableContext BC){
		
	}
}