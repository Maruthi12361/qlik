/********************************************************
* ScheduleCreateQSDLoginDetails
* Description: This is the schedulable class use to call the class where the activity and campaign association is done for 
leads and contacts
*
* Change Log: 22-06-2017 v1 UIN Added Initial logic
* Change Log: 22-06-2017 v2 UIN CHG0031909 Added batch size
*********************************************************/
global class ScheduleCreateQSDLoginDetails implements Schedulable {
	String sObjectName;
	global ScheduleCreateQSDLoginDetails(String objName) {
        sObjectName = objName;
	}

	global void execute(SchedulableContext sc) {
		CreateLeadQSDLoginDetails b = new CreateLeadQSDLoginDetails(sObjectName);
		database.executebatch(b,100);
	}
}