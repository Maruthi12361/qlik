/*
BSL-1950 - Handler created by Shubham Gupta which will populate QSP adjustment amount on quote object 
which can be used in zuora rule engine
*/

public class Z_QuoteChargeSummaryHandler {
    public static void updateQSPAjustmentOnQuote(List<zqu__QuoteChargeSummary__c> newChargeSummary){
        
    Map<id,Decimal> idtoQuantity = new Map<id,Decimal>();
    Map<id,String> idtosubsname = new Map<id,String>();
    Map<string,decimal> nametoQuantity = new Map<string,decimal>();
    List<zqu__Quote__c> qttoUpdate = new List<zqu__Quote__c>();
        for(zqu__QuoteChargeSummary__c chsum:newChargeSummary){
            if(chsum.Name == 'S8071'){
                idtoQuantity.put(chsum.Parent_Quote__c,chsum.zqu__Quantity__c);
            }
        }

        if(!idtoQuantity.isEmpty()){
            integer cons = 0;
            List<zqu__Quote__c>  parentQuote = [select id,QSP_Adjustment_Quantity__c,zqu__Hidden_Subscription_Name__c from zqu__Quote__c where id IN :idtoQuantity.keyset()];
            
            if(!parentQuote.isEmpty()){
                
                for(zqu__Quote__c zqt:parentQuote){
                    if(!String.isBlank(zqt.zqu__Hidden_Subscription_Name__c))
                    idtosubsname.put(zqt.id, zqt.zqu__Hidden_Subscription_Name__c);
                }

                if(!idtosubsname.isEmpty()){
                    List<Zuora__Subscription__c> subsfetched = [select id,Name,(select id,Name,Zuora__Quantity__c from Zuora__Subscription_Product_Charges__r where (Name = 'EDT0000085' or Name = 'EDT0000105')) from Zuora__Subscription__c where Name IN :idtosubsname.values()];

                    if(!subsfetched.isEmpty()){
                        for(Zuora__Subscription__c subs:subsfetched){
                            if(!subs.Zuora__Subscription_Product_Charges__r.isEmpty()){
                                for(Zuora__SubscriptionProductCharge__c subsprod:subs.Zuora__Subscription_Product_Charges__r){
                                    nametoQuantity.put(subs.Name,subsprod.Zuora__Quantity__c);
                                }
                            }
                        }
                    }

                }

                for(zqu__Quote__c qt:parentQuote){
                        double priorquantity = nametoQuantity.get(idtosubsname.get(qt.id));
                        if(priorquantity != null && priorquantity > 0){
                            qt.QSP_Adjustment_Quantity__c = idtoQuantity.get(qt.id) + priorquantity;
                        }
                        else{
                            qt.QSP_Adjustment_Quantity__c = idtoQuantity.get(qt.id);
                        }
                    qttoUpdate.add(qt);
                }
                if(!qttoUpdate.isEmpty()){
                    update qttoUpdate;
                }
            }
        }
    }
}