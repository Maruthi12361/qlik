/************************************************
*   TestContactSetOwner:
*   Change Log:
*   XXXXXXX     XXX     Initial Development
*   20140626    RDZ     Adding some extra code to improve code coverage. QTNS Go Live
*   20150604    CCE     Adding some extra code to test changes added by CR# 31870
*   20160426    CCE     Updated for revised testing required for changes to the trigger as part of CR# 82611
*   2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
*   2019-02-14  AIN     Changed logic to insert more unique test data to avoid duplicate rule
*   2019-04-15 CCE CHG0035936 BMW-1424 Stop Contact Resource RT from being overwritten on Suspect status
*************************************************/

@isTest
private class TestContactSetOwner {

    @testSetup
    public static void testSetup() {
        QuoteTestHelper.createCustomSettings();
       }
    
    
    static testMethod void TestContactSetOwner() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());
        User u2 = QTTestUtils.createMockSystemAdministrator();
        //User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];
         System.runAs(u2) {   
        
       

          
        //RECORD TYPE ID
       RecordType rtype = [Select Name, Id From RecordType where sObjectType='Contact' LIMIT 1]; 
         
              ContactSetOwner__c mycs = ContactSetOwner__c.getValues('contactset'); 
         if(mycs == null) {             
             mycs = new ContactSetOwner__c(Name= 'contactset');
             mycs.ContactOwnerId__c =u2.id;
             mycs.Contactrecordtype__c=rtype.id;
             insert mycs;
         }   
         
         
        Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run      
            
            //CREATE A Contact 1                    
            Contact Contact = createNewContact(u2.id);
            Contact.Contact_Status__c ='false';                      
            Contact.Archived_Original_Owner__c= u2.Id;           
            insert Contact;
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            //CREATE A Contact 2
            Contact Contact2 = createNewContact(u2.Id);
            Contact2.Contact_Status__c ='Negotiating';
            insert Contact2;
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            //CREATE A Contact 3
            Contact Contact3 = createNewContact(u2.Id);
            Contact3.Contact_Status__c ='false';
            Contact3.Archived_Original_Owner__c= u2.Id;
            Contact3.Old_Record_Type__c=rtype.id;
            
            insert Contact3;
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            //CREATE A Contact 4
            Contact Contact4 = createNewContact(u2.Id);
            Contact4.Archived_Original_Owner__c= u2.Id;  
            Contact4.Contact_Status__c ='';
            insert Contact4; 
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            //CREATE A Contact 4
            Contact ContactOldStatusNull = createNewContact(u2.Id);
            ContactOldStatusNull.Archived_Original_Owner__c= u2.Id;  
            ContactOldStatusNull.Contact_Status__c ='';
            insert ContactOldStatusNull;                   
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            test.startTest();
    
            Contact.Contact_Status__c = 'Negotiating';
            Contact.Old_Record_Type__c=rtype.id;
            update Contact;
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
           
            
            Contact2.Contact_Status__c = 'false';
            update Contact2;
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            Contact3.Contact_Status__c = '';
            update Contact3;
            
            
            Contact4.Contact_Status__c = 'Negotiating';
            Contact4.OwnerId=Contact.Archived_Original_Owner__c;
            Contact4.Old_Record_Type__c=rtype.id;
            update Contact4;
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run

            //Adding Contact_Status__c to one used in structure ContactSetOwner Variable
            //Set< String > structure = new Set< String >{'Archived', 'False', 'Lead - Never'};
            ContactOldStatusNull.Contact_Status__c = 'Lead - Never';
            ContactOldStatusNull.OwnerId=Contact.Archived_Original_Owner__c;
            ContactOldStatusNull.Old_Record_Type__c=rtype.id;
            update ContactOldStatusNull; 
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
                
            Contact.Contact_Status__c = 'Negotiating';
            Contact.OwnerId=Contact.Archived_Original_Owner__c;
            update Contact;
            Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            
            test.stopTest();        
        }
    }
    
    //Adding some extra code to test changes added by CR# 31870 - status changes to 'Follow-Up Rejected'
    static testMethod void TestContactSetOwner_FU_Rejected() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());
        User ContactOwnerForAchive = QTTestUtils.createMockUserForProfile('System Administrator');      
        
        User u2 = QTTestUtils.createMockSystemAdministrator();
        //User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs(u2) {
          
            //RECORD TYPE ID
            RecordType rtype = [Select Name, Id From RecordType where sObjectType='Contact' AND name =: 'Archived' LIMIT 1]; 

            //Create Custom Setting to hold the Archived record type and Archived record holder ID
            ContactSetOwner__c mycs = ContactSetOwner__c.getValues('contactset'); 
            if(mycs == null) {             
                mycs = new ContactSetOwner__c(Name= 'contactset');
                mycs.ContactOwnerId__c = ContactOwnerForAchive.id;
                mycs.Contactrecordtype__c = rtype.id;
                insert mycs;
            }
           
            //CREATE A Contact                   
            //For CR# 31870 - Create a Contact for testing of Status "Follow-Up Rejected"
            Contact Contact_FURejected = createNewContact(u2.id);
            Contact_FURejected.Contact_Status__c ='Unknown';
            insert Contact_FURejected;
            //Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            Contact_FURejected = refreshContact(Contact_FURejected.Id);
            Id OriginalContactRecordTypeId = Contact_FURejected.recordtypeid;    //we can use this to test against later
            System.debug('TestContactSetOwner: Contact_FURejected = ' + Contact_FURejected);
            test.startTest();
            
            //Set the Status and follow up reason fields to a status that should set the record to be Archived
            Contact_FURejected.Contact_Status__c ='Follow-Up Rejected';
            Contact_FURejected.Follow_Up_Rejected_Reason__c ='Invalid Job Function'; // CR# 82611 was'Not Follow-Up Required';
            //Contact_FURejected.Not_Follow_Up_Required_Reason__c ='Invalid Job Function'; // CR# 82611   
            update Contact_FURejected;
            //Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            Contact_FURejected = refreshContact(Contact_FURejected.Id);
            System.debug('TestContactSetOwner: Contact_FURejected = ' + Contact_FURejected);
            
            //Test that the record was Archived
            System.assertEquals(rtype.Id, Contact_FURejected.recordtypeid, 'RecordTypes do not match');    //should be Archived
            System.assertEquals(mycs.ContactOwnerId__c, Contact_FURejected.OwnerId, 'Record Owner does not match');    //should be the User stored in the Custom Setting

            //Set the status to a value that should restore it from an Archived state
            Contact_FURejected.Contact_Status__c ='Unknown';  
            update Contact_FURejected;
            
            Contact_FURejected = refreshContact(Contact_FURejected.Id);
            System.debug('TestContactSetOwner: Contact_FURejected = ' + Contact_FURejected);
            
            //test that the record has been set back to it's original record type and Owner
            System.assertEquals(OriginalContactRecordTypeId, Contact_FURejected.recordtypeid, 'RecordTypes do not match');    //should be back to original record type
            System.assertEquals(u2.Id, Contact_FURejected.OwnerId, 'Record Owner does not match');    //should be back to original owner

            test.stopTest();        
        }
    }
    
    //Adding some extra code to test changes added by CR# 31870 - status changes to 'Follow-Up Disqualified'
    static testMethod void TestContactSetOwner_FU_Disqualified() {
    
        Test.setMock(WebserviceMock.class, new DevQlikViewWebServiceMockImpl());
        User ContactOwnerForAchive = QTTestUtils.createMockUserForProfile('System Administrator');      
        
        User u2 = QTTestUtils.createMockSystemAdministrator();
        //User u2 = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs(u2) {
          
            //RECORD TYPE ID
            RecordType rtype = [Select Name, Id From RecordType where sObjectType='Contact' AND name =: 'Archived' LIMIT 1]; 

            //Create Custom Setting to hold the Archived record type and Archived record holder ID
            ContactSetOwner__c mycs = ContactSetOwner__c.getValues('contactset'); 
            if(mycs == null) {             
                mycs = new ContactSetOwner__c(Name= 'contactset');
                mycs.ContactOwnerId__c = ContactOwnerForAchive.id;
                mycs.Contactrecordtype__c = rtype.id;
                insert mycs;
            }
           
            //CREATE A Contact                   
            //For CR# 31870 - Create a Contact for testing of Status "Follow-Up Rejected"
            Contact Contact_FURejected = createNewContact(u2.id);
            Contact_FURejected.Contact_Status__c ='Unknown';
            insert Contact_FURejected;
            //Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            Contact_FURejected = refreshContact(Contact_FURejected.Id);
            Id OriginalContactRecordTypeId = Contact_FURejected.recordtypeid;    //we can use this to test against later
            System.debug('TestContactSetOwner: Contact_FURejected = ' + Contact_FURejected);
            test.startTest();
            
            //Set the Status and follow up reason fields to a status that should set the record to be Archived
            Contact_FURejected.Contact_Status__c ='Follow-Up Disqualified';
            Contact_FURejected.Follow_Up_Disqualified_Reason__c ='No Fit';
            update Contact_FURejected;
            //Semaphores.TriggerHasRun('ContactSetOwner', 1);    //reset the semaphore flag that will have been set by the previous run
            Semaphores.ContactTriggerHandlerBeforeUpdate = false;
            Contact_FURejected = refreshContact(Contact_FURejected.Id);
            System.debug('TestContactSetOwner: Contact_FURejected = ' + Contact_FURejected);
            
            //Test that the record was Archived
            System.assertEquals(rtype.Id, Contact_FURejected.recordtypeid, 'RecordTypes do not match');    //should be Archived
            System.assertEquals(mycs.ContactOwnerId__c, Contact_FURejected.OwnerId, 'Record Owner does not match');    //should be the User stored in the Custom Setting

            //Set the status to a value that should restore it from an Archived state
            Contact_FURejected.Contact_Status__c ='Unknown';  
            update Contact_FURejected;
            
            Contact_FURejected = refreshContact(Contact_FURejected.Id);
            System.debug('TestContactSetOwner: Contact_FURejected = ' + Contact_FURejected);
            
            //test that the record has been set back to it's original record type and Owner
            System.assertEquals(OriginalContactRecordTypeId, Contact_FURejected.recordtypeid, 'RecordTypes do not match');    //should be back to original record type
            System.assertEquals(u2.Id, Contact_FURejected.OwnerId, 'Record Owner does not match');    //should be back to original owner

            test.stopTest();        
        }
    }

    public static integer NumCreated = 0;
    private static Contact createNewContact(Id ownerId) {
        Contact Contact = new Contact();
        Contact.OwnerId = ownerId;
        Contact.LastName = 'last' +'_' + System.now().millisecond() + '_' + NumCreated;
        Contact.FirstName = 'First' +'_' + System.now().millisecond() + '_' + NumCreated;
        Contact.Email='shshdkjd' +'_' + System.now().millisecond() + '_' + NumCreated +'@fkfk'+ System.now().millisecond() + NumCreated + '.fi';
        Contact.Phone='3333';
        Contact.MobilePhone='3333';
        Contact.Switchboard__c='3333';
        return Contact;        
    }

    private static Contact refreshContact (Id id) {
        return [SELECT Id, Contact_Status__c, OwnerId, recordtypeId, Old_Record_Type__c, Old_Record_Type_Name__c, Archived_Original_Owner__c FROM Contact WHERE Id = :id];
    }    

}