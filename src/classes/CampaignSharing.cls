/******************************************************

    Class: CampaignSharing
    Create a Partner Sharing on campaigns based on Partner 1 field.
    When a partner account is populated in that field share the campaign with the Partner Account and its users. 
    Level of access : Read

    Changelog:
        2016-03-04  CCE     Created file
        2018-06-29  ext_bad     Check 'cmp.Primary_Partner_Involved__c != null' to fix tests on refreshed sandboxes

******************************************************/
public class CampaignSharing {
    // caller needs to make sure only those campaigns whose "Partner 1" field (Primary_Partner_Involved__c) 
    // have been changed are included
    public static Boolean UpdateCampaignSharing(List<Id> campaignIds)
    {
        if (campaignIds.size() == 0)
        {
            return true;
        }

        List<Campaign> campaigns = [SELECT Id, Primary_Partner_Involved__c FROM Campaign WHERE Id in :campaignIds];
        //List<Campaign> campaigns = [SELECT Id, Primary_Partner_Involved__c FROM Campaign WHERE Primary_Partner_Involved__c<>null AND Id in :campaignIds];

        if (campaigns.size() == 0)
        {
            return true;
        }
        System.debug('UpdateCampaignSharing: campaigns =' + campaigns);
        Set<Id> uniqueAccIds = new Set<Id>();
        Map<Id, Id> campaign2Account = new Map<Id, Id>();
        for (Campaign cmp : campaigns)
        {
            if (cmp.Primary_Partner_Involved__c != null) {
                campaign2Account.put(cmp.Id, cmp.Primary_Partner_Involved__c);
                uniqueAccIds.add(cmp.Primary_Partner_Involved__c);
            }
        }

        Boolean noNewShares = false;
        List<CampaignShare> newCampaignShares = new List<CampaignShare>();

        // If no UniqueAccounts found then no new shares needed
        if (uniqueAccIds.size() == 0)
        {
            System.debug('UpdateCampaignSharing: No unique accounts found.');
            noNewShares = true;
        }
        else
        {
            // Fetch all the PortalRoles
            Map<Id, Id> accountToRole = new Map<Id,Id>();
            Set<Id> uniquePortalRoles = new Set<Id>();
            List<Id> portalRoleIds = new List<Id>();

            List<UserRole> userRoles = ApexSharingRules.GetListOfUserRolesByPortalAccountIds(uniqueAccIds, 'Partner', 'Executive');
            
            for (UserRole usrRole :userRoles)
            {
                if (!uniquePortalRoles.contains(usrRole.Id))
                {
                    uniquePortalRoles.add(usrRole.Id);
                    portalRoleIds.add(usrRole.Id);
                }
                accountToRole.put(usrRole.PortalAccountId, usrRole.Id);
            }

            System.debug('UpdateCampaignSharing: uniqueAccIds=' + uniqueAccIds + ', uniquePortalRoles=' + uniquePortalRoles + '###### userRoles=' + userRoles);

            if (uniquePortalRoles.size() == 0)
            {            
                System.debug('UpdateCampaignSharing: No unique PortalRoles found.');
                noNewShares = true;
            }

            Map<Id,Id> roleToGroup = new Map<Id,Id>();

            List<Group> groups = ApexSharingRules.getListOfGroupsByRelatedId(portalRoleIds, 'RoleAndSubordinates');
            for (Group grp : groups)
            {
                roleToGroup.put(grp.RelatedId, grp.Id);
            }

            if (roleToGroup.size() == 0)
            {
                System.debug('UpdateCampaignSharing: No unique Groups found.');
                noNewShares = true;
            }

            if (!noNewShares)
            {
                for (Campaign cnt : campaigns)
                {
                    if (campaign2Account.containsKey(cnt.Id))
                    {
                        Id accountId = campaign2Account.get(cnt.Id);
                        
                        System.debug('accountId=' + accountId);
                        System.debug('accountToRole=' + accountToRole);

                        if (accountToRole.containsKey(accountId))
                        {
                            Id accountIdRole = accountToRole.get(accountId);
                            if (roleToGroup.containsKey(accountIdRole))
                            {
                                newCampaignShares.add(createNewCampaignShare(cnt.Id, 'Read', roleToGroup.get(accountIdRole)));
                            }
                        }
                    }
                }
            }
        }

        // retrieving existing Manual Campaign Sharing
        List<CampaignShare> CampaignShares = [select Id,CampaignId, UserOrGroupId, CampaignAccessLevel from CampaignShare 
                    where CampaignId in :campaignIds and RowCause = 'Manual'];
        System.debug('UpdateCampaignSharing: CampaignShares = ' + CampaignShares);
        // filter out those need to be deleted
        List<CampaignShare> toDel = new List<CampaignShare>();
        for (CampaignShare oldShare : CampaignShares)
        {
            Boolean matched = false;
            for (integer i = 0; i < newCampaignShares.size(); i++)
            {
                if (oldShare.CampaignAccessLevel == newCampaignShares[i].CampaignAccessLevel &&
                    oldShare.CampaignId == newCampaignShares[i].CampaignId &&
                    oldShare.UserOrGroupId == newCampaignShares[i].UserOrGroupId )
                {
                    matched = true;
                    // already in place, no need to insert again
                    newCampaignShares.remove(i);
                }
            }
            if (!matched)
            {
                toDel.add(oldShare);
            }
        } 
        // delete those sharing that are no longer valid
        if (toDel.size() > 0)
        {
            System.debug('UpdateCampaignSharing: toDel = ' + toDel);
            Delete toDel;
        }

        // Insert sharing if there is the need
        if (newCampaignShares.size() > 0)
        {
            Insert newCampaignShares;
        }

        return true;
    }

    public static CampaignShare createNewCampaignShare(Id campaignId, string accessLevel, string usrOrGroupId)
    {
        CampaignShare cShare = new CampaignShare(
                CampaignId = campaignId,
                CampaignAccessLevel = accessLevel,
                UserOrGroupId = usrOrGroupId
            );

        return cShare;
    }
}