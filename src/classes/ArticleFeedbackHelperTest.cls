/**
*
* extbad    2019-09-17 IT-2037 Send an email to the 'Submitted_by'
*/
@IsTest
private class ArticleFeedbackHelperTest {

    static testMethod void testSendFeedbackEmail() {
        Case cs = new Case();
        cs.Submitted_by__c = UserInfo.getUserId();
        cs.Knowledge_Article__c = '123456';
        cs.Subject = 'test';
        cs.Feedback_to_submitter_Actions_taken__c = 'test';
        insert cs; 

        Test.startTest();
        Boolean emailSent = ArticleFeedbackHelper.sendFeedbackEmail(cs.Id);
        Test.stopTest();
 
        System.assert(emailSent);
    }

}