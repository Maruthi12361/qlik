/************************************************************
* TestClass: Test class for QuoteTestHelper
* 
* Log History:
* 2018-03-27 AIN    IT-99 Class created for code coverage
************************************************************/
@isTest
private class QuoteTestHelperTest {
	
	@isTest static void test_method_one() {

		Semaphores.SetAllSemaphoresToTrue();
		QuoteTestHelper.createCustomSettings();
		
		Subsidiary__c sub = QuoteTestHelper.createSubsidiary();
		insert sub;
		
		QlikTech_Company__c comp = QuoteTestHelper.createQlickTechCompany(sub.id);
		insert comp;
		
		RecordType rTypeEnd = [Select id, DeveloperName From RecordType where DeveloperName = 'End_User_Account' and sobjecttype='Account'];
		RecordType rTypePart = [Select id, DeveloperName From RecordType where DeveloperName = 'Partner_Account' and sobjecttype='Account'];
		Account accCust = QuoteTestHelper.createAccount(comp, rTypeEnd, 'Customer');
		accCust.Pending_Validation__c = false;
		
		Account accPart = QuoteTestHelper.createAccount(comp, rTypePart, 'Partner');
		accPart.Pending_Validation__c = false;
		List<Account> accounts = new List<Account>();
		accounts.add(accCust);
		accounts.add(accPart);
		insert accounts;

		Contact con = QuoteTestHelper.createContact(accCust.id);
		insert con;

		Address__c add = QuoteTestHelper.createAddress(accCust.Id, con.Id, 'Shipping');
		insert add;

		RecordType rTypeOpp = [SELECT Id, Name from RecordType where DeveloperName like 'Sales_QCCS' and sobjecttype='Opportunity'];
		Opportunity opp = QuoteTestHelper.createOpportunity(accCust, null, rTypeOpp);
		opp.Revenue_Type__c = 'Direct';
		insert opp;

		RecordType rTypeQuote = [Select id From Recordtype Where DeveloperName = 'Quote' and sobjecttype='SBQQ__Quote__c'];
		SBQQ__Quote__c quote = QuoteTestHelper.createQuote(rTypeQuote, con.id, accCust, null, 'Direct', 'Open', 'Quote', false, opp.Id);
		insert quote;

		Product2 product = QuoteTestHelper.createProduct('QlikView Enterprise Edition Server', 'Licenses', 1, 'QlikView', 'QlikView');
		insert product;

		SBQQ__QuoteLine__c quoteLine = QuoteTestHelper.createQuoteLine(quote.Id, product.Id, 'smth', 3, 'licenseCode', 'Text', 'family', 'USD', 6, 1, 'LiceId', 'lefDetail');

		PricebookEntry pbe = QuoteTestHelper.createPriceBookEntry(product.Id, '01s20000000E0PWAA0');
		insert pbe;

		OpportunityLineItem oppLineItem = QuoteTestHelper.createLineItem(opp.Id, pbe);

		Competitor_Information__c compInfo = QuoteTestHelper.createCompInfo(opp.Id);

		QlikTech_Company__c comp2 = QuoteTestHelper.createQlikTech('Sweden', 'SE', sub.Id);

		Recordtype rt = QuoteTestHelper.getRecordTypebyDevName('Partner_Account');

		List<Partner_Category_Status__c> partCatStat = QuoteTestHelper.createPCS(accPart);


	}	
}