/******************************************************

    OppUpdateContactSOIHandler
    Updates a contact's Is Closed Won SOI field when the Opp stage is Closed Won for certain record types    
    
    Changelog:
        2016-05-23  Roman@4front     Migrated from OppUpdateContactSOI.trigger.
******************************************************/

public without sharing class OppUpdateContactSOIHandler {
    public static Boolean handleFlag = false;


    public OppUpdateContactSOIHandler() {
        
    }

    public static void handle(List<Opportunity> triggerNew, List<Opportunity> triggerOld, Map<Id, Opportunity> triggerOldMap, Boolean isUpdate, Boolean isInsert, Boolean isAfter, Boolean isBefore, Boolean isDelete) {
        if(!handleFlag) {
            handleFlag = true;
            System.debug('---- Starting: OppUpdateContactSOIHandler ----');


            Map<String, Schema.RecordTypeInfo> oppRecordTypes = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();

            // Commented for BMW-1397 - Depreciate QL/Inquiry type fields
            //Set<Id> sOpps = new Set<Id>();
            //if (isAfter && isUpdate && !Semaphores.TriggerHasRun('OppUpdateContactSOI')) {
            //  for (Opportunity opp : triggerNew) {
            //      Opportunity oldOpp = triggerOldMap.get(opp.Id);
            //      // Workflow will set the GI Acceptance Status = GI Accepted for the following stages. However the trigger is not
            //      // getting invoked after the workflow update - thus we cannot check for a change in GI Acceptance Status but instead check  
            //      // for all the relevant stages that invokes the workflow
            //      System.debug('1--- opp: ' + opp.StageName);
            //      System.debug('1--- opp: ' + oldOpp.StageName);
            //      System.debug('1--- opp: ' + opp.GI_Acceptance_Status__c);
            //      System.debug('1--- opp: ' + oldOpp.GI_Acceptance_Status__c);    
            //      if ((oldOpp.GI_Acceptance_Status__c != 'GI Accepted')
            //              && (opp.StageName == 'Goal Confirmed' && oldOpp.StageName != 'Goal Confirmed')  
            //              || (opp.StageName == 'Champion' && oldOpp.StageName != 'Champion')
            //              || (opp.StageName == 'Prove Value' && oldOpp.StageName != 'Prove Value')
            //              || (opp.StageName == 'Negotiating' && oldOpp.StageName != 'Negotiating')
            //              || (opp.StageName == 'Closed Won' && oldOpp.StageName != 'Closed Won')) {
            //          sOpps.add(opp.Id);
            //          System.debug('2--- opp: ' + opp.StageName);
            //          System.debug('2--- opp: ' + oldOpp.StageName);
            //      } else if ((oldOpp.GI_Acceptance_Status__c != 'GI Accepted') && (opp.GI_Acceptance_Status__c == 'GI Accepted')) {
            //          sOpps.add(opp.Id); // the user manually changed the GI Acceptance Status = GI Accepted
            //          System.debug('3--- opp: ' + opp.StageName);
            //          System.debug('3--- opp: ' + oldOpp.StageName);  
            //      }
            //  }
            //  if (!sOpps.isEmpty()) {
            //      // Get all the relevant contacts in the Opportunity's Sphere of Influence
            //      Set<Id> sContacts = new Set<Id>();
            //      for (Sphere_of_Influence__c soi: [SELECT Contact__c, Opportunity__c FROM Sphere_of_Influence__c WHERE Opportunity__c IN :sOpps]) {
            //          if (soi.Contact__c != null) sContacts.add(soi.Contact__c);
            //      }
            //      // Set the SAL Start Dates if they are not set already (Contact status = Goal Identified)
            //      if (!sContacts.isEmpty()) {
            //          List<Contact> lstC = [
            //                  SELECT Id, Contact_Status__c, MQL_to_SAL__c, MQL_Start_Date__c, MQL_Start_Date_Current__c,
            //                          MQL_to_SAL_Age__c, SAL_Start_Date__c, SAL_Start_Date_Current__c
            //                  FROM Contact
            //                  WHERE Id IN :sContacts AND Contact_Status__c = 'Goal Identified'
            //          ];
            //          for (Integer i = 0; i < lstC.size(); i++) {
            //              if (lstC[i].MQL_to_SAL__c == false) { // End of MQL
            //                  lstC[i].MQL_to_SAL__c = true;
            //                  if (lstC[i].MQL_Start_Date__c == null) lstC[i].MQL_Start_Date__c = Date.today(); // historical value
            //                  if (lstC[i].MQL_Start_Date_Current__c == null) lstC[i].MQL_Start_Date_Current__c = Date.today();
            //                  lstC[i].MQL_to_SAL_Age__c += lstC[i].MQL_Start_Date_Current__c.daysBetween(Date.today());
            //              }
            //              if (lstC[i].SAL_Start_Date__c == null) lstC[i].SAL_Start_Date__c = Date.today();    
            //              if (lstC[i].SAL_Start_Date_Current__c == null) lstC[i].SAL_Start_Date_Current__c = Date.today();
            //          }   
            //          if (!lstC.isEmpty()) update lstC;   
            //      }
            //  }
            //}

            // CR 10680 - Copy SOI to DSC in new Deal Split process
            if (isAfter && isInsert) {
                
                
                
                
                
                RecordTypeInfo rti = oppRecordTypes.get('Deal Split Child');
                
                
                //List<RecordType> rt = [SELECT Id from RecordType WHERE SobjectType = 'Opportunity' AND IsActive = true AND Name = 'Deal Split Child'];
                
                
                
                if (rti == null) return;
                System.debug('--- rt:' +rti);
                
                Id rtId = rti.getRecordTypeId(); 

                Map<Id, Id> mapOpps = new Map<Id, Id>(); // map of child opp ids and parent opp ids
                for (Opportunity opp : triggerNew) {
                    if (opp.RecordTypeId == rtId) {
                        mapOpps.put(opp.Id, opp.Deal_Split_Parent_Opportunity__c);
                    }
                }
                if (mapOpps.isEmpty()) return; // no Opps with record type Deal Split Child are created

                // OK, there are some opportunities created of record type deal split child
                // Get the list of SOIs from their parent opportunities
                List<Sphere_of_Influence__c> lstSOI = [
                        SELECT Opportunity__c, Contact__c, Capabilities_Required_Solution__c, Champion_Letter__c,
                                Champion_letter_body__c, Champion_letter_recipient__c, Current_Situation__c, Dependency__c,
                                Formula__c, From_Lead__c, Goal__c, Goal_Letter__c, Goal_Letter_Body__c, Goal_Letter_Recipient__c,
                                Improvement__c, Key_Player_Letter__c, Next_Steps__c, Quote_Recipient__c, Role__c, Send_Champion_letter__c,
                                Send_Goal_Letter__c, Sequence_of_Events_Letter__c, Value__c
                        FROM Sphere_of_Influence__c
                        WHERE Opportunity__c IN :mapOpps.values()
                ];

                List<Sphere_of_Influence__c> lstSOIInsert = new List<Sphere_of_Influence__c>();

                for (Opportunity opp : triggerNew) { // for each child opportunity
                    Id parentOpp = mapOpps.get(opp.Id); // get the parent opportunity id
                    if (parentOpp == null) continue;
                    for (Sphere_of_Influence__c soi : lstSOI) { // could have multiple SOIs for the parent Opp
                        if (parentOpp == soi.Opportunity__c) { // match of parent id with the SOI opportunity id
                            Sphere_of_Influence__c soiNew = new Sphere_of_Influence__c();
                            soiNew.Opportunity__c = opp.Id; // link it to child opportunity
                            soiNew.Contact__c = soi.Contact__c;
                            soiNew.Capabilities_Required_Solution__c = soi.Capabilities_Required_Solution__c;
                            soiNew.Champion_Letter__c = soi.Champion_Letter__c;
                            soiNew.Champion_letter_body__c = soi.Champion_letter_body__c;
                            soiNew.Champion_letter_recipient__c = soi.Champion_letter_recipient__c;
                            soiNew.Current_Situation__c = soi.Current_Situation__c;
                            soiNew.Dependency__c = soi.Dependency__c;
                            soiNew.Formula__c = soi.Formula__c;
                            soiNew.From_Lead__c = soi.From_Lead__c;
                            soiNew.Goal__c = soi.Goal__c;
                            soiNew.Goal_Letter__c = soi.Goal_Letter__c;
                            soiNew.Goal_Letter_Body__c = soi.Goal_Letter_Body__c;
                            soiNew.Goal_Letter_Recipient__c = soi.Goal_Letter_Recipient__c;
                            soiNew.Improvement__c = soi.Improvement__c;
                            soiNew.Key_Player_Letter__c = soi.Key_Player_Letter__c;
                            soiNew.Next_Steps__c = soi.Next_Steps__c;
                            soiNew.Quote_Recipient__c = soi.Quote_Recipient__c;
                            soiNew.Role__c = soi.Role__c;
                            soiNew.Send_Champion_letter__c = soi.Send_Champion_letter__c;
                            soiNew.Send_Goal_Letter__c = soi.Send_Goal_Letter__c;
                            soiNew.Sequence_of_Events_Letter__c = soi.Sequence_of_Events_Letter__c;
                            soiNew.Value__c = soi.Value__c;
                            lstSOIInsert.add(soiNew);
                        }
                    }
                }
                if (!lstSOIInsert.isEmpty()) insert lstSOIInsert;
            }

            if ((isAfter && Semaphores.OppUpdateContactSOI_IsAfter == false) || isBefore) {
                if (isAfter) {
                    Semaphores.OppUpdateContactSOI_IsAfter = true;
                }
                System.debug('---- executing OppUpdateContactSOI');
                // CR 12222 when Send Qlikogram is checked, create solution profile
                if (isUpdate) {
                    try {
                        List<Solution_Profiles__c> spList = new List<Solution_Profiles__c>();
                        for (integer index = 0; index < triggerOld.size(); index++) {
                            Opportunity newOpp = triggerNew[index];
                            if (triggerOld[index].Send_Qlikogram__c != true && newOpp.Send_Qlikogram__c) {
                                Solution_Profiles__c sp = new Solution_Profiles__c(
                                        Account_Name__c = newOpp.AccountId, Name = newOpp.Name, Opportunity_Name__c = newOpp.Id, Solution_Owner__c = UserInfo.getUserId(),
                                        Industry__c = newOpp.Industry__c, Sector__c = newOpp.Sector__c, Function__c = newOpp.Function__c, Solution_Area__c = newOpp.Solution_Area__c,
                                        Department__c = newOpp.Department__c, Business_Issues_Goals__c = newOpp.What_Goal_and_Value_will_the_customer_ac__c
                                );
                                String sellthrou = newOpp.Sell_Through_Partner__c;
                                if (String.isNotEmpty(sellthrou)) {
                                    sp.Solution_Profile_End_User__c = sellthrou;
                                    sp.Account_Name__c = sellthrou;
                                }
                                spList.Add(sp);
                            }
                        }
                        if (spList.size() > 0) {
                            Insert spList;
                        }
                    } catch (Exception ex) {
                        System.debug(ex.getMessage());
                    }
                }
                // End of CR 12222

                List<Opportunity> listOpps;
                if (isDelete) listOpps = triggerOld; else listOpps = triggerNew;

                List<Id> recordTypeIds = new List<Id>(); 

                for(RecordTypeInfo rti: oppRecordTypes.values()) {
                    if(rti.isActive() 
                        && rti.getName() != 'OEM - Order' 
                        && rti.getName() != 'OEM - Run Rate'
                        && rti.getName() != 'OEM - Standard Sales Process'
                        && rti.getName() != 'OEM Prepay'
                        && rti.getName() != 'OEM Prepay') {
                    
                        recordTypeIds.add(rti.getRecordTypeId());
                    }   
                }

                /*List<RecordType> lstRec = [
                        SELECT Id
                        from RecordType
                        WHERE SobjectType = 'Opportunity'
                        AND IsActive = true
                        AND Name NOT IN('OEM - Order', 'OEM - Run Rate',
                                'OEM - Standard Sales Process', 'OEM Prepay',
                                'OEM Recruitment (CCS)')
                ];*/

                // Get the contact SOIs for the Opportunities sent to this trigger
                List<Sphere_of_Influence__c> lstSOICt = [SELECT Contact__c FROM Sphere_of_Influence__c WHERE Opportunity__c IN :listOpps];

                Set<Id> ctID = new Set<Id>(); // prepare the list of contacts in the SOIs
                for (Sphere_of_Influence__c x : lstSOICt) ctID.add(x.Contact__c);

                // Get all SOIs where these contacts are in them
                List<Sphere_of_Influence__c> lstSOIOpp = [
                        SELECT Contact__c, Opportunity__c
                        FROM Sphere_of_Influence__c
                        WHERE Contact__c IN :ctID
                ];

                // prepare a map of all related Opps with Opp stage names (only Opps that are not being deleted)
                Set<Id> oppID = new Set<Id>();
                for (Sphere_of_Influence__c x : lstSOIOpp) {
                    if (isDelete) { // exclude the sent opps if it is trigger delete
                        Boolean bMatched = false;
                        for (Opportunity y : listOpps) {
                            if (y.Id == x.Opportunity__c) bMatched = true; // this Opp is being deleted
                        }
                        if (bMatched == false) oppID.add(x.Opportunity__c); // add it as it not being deleted
                    } else oppID.add(x.Opportunity__c);
                }

                // Get a list of Opps with stage names that are still active
                List<Opportunity> lstOpp = [SELECT Id, StageName FROM Opportunity WHERE Id IN :oppID AND RecordTypeId IN :recordTypeIds];

                Map<Id, String> oppMap = new Map<Id, String>();
                for (Opportunity x : lstOpp) oppMap.put(x.Id, x.StageName);

                // Now figure out the Closed SOI status for each of the contacts
                Map<Id, String> ctIDs = new Map<Id, String>();
                for (Id ct : ctID) { // for each contact
                    Boolean bClosed = false; // assume the opportunity stage is not closed won
                    for (Sphere_of_Influence__c soi : lstSOIOpp) {
                        if (soi.Contact__c == ct) {
                            if (oppMap.get(soi.Opportunity__c) == 'Closed Won') { // Atleast one is a closed won opp
                                bClosed = true;
                                break;
                            }
                        }
                    }
                    if (bClosed == true) ctIDs.put(ct, 'Yes'); else ctIDs.put(ct, 'No');
                }

                List<Contact> lstCt = [SELECT Is_Closed_Won_SOI__c FROM Contact where Id IN :ctIDs.keySet()];
                for (Integer i = 0; i < lstCt.size(); i++) {
                    lstCt[i].Is_Closed_Won_SOI__c = ctIDs.get(lstCt[i].Id);
                }

                if (lstCt.size() > 0) update lstCt;
            }
            System.debug('---- Finishing: OppUpdateContactSOIHandler ----');
        }

    }
}