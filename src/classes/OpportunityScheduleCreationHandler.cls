/*
    Change Log
    2017-05-17 MTM Set QCW-1633 OEM Subscription deals
    2017-06-29 Rodion Vakulovskyi QCW 2810
    2017-09-07 Rodion Vakulovskyi QCW-3766
    2017-09-09 MTM QCW-3803
    2017-09-15 UIN QCW-2494 Async Schedule creation changes
    2017-09-19 NJAIN - QCW-3804 - Schedules to be created from the Opportunity: Subscription Start Date
    2017-09-23 Shubham Gupta QCW-2849 Added logic to create Schedules for Renwable Products Added to Deal split opportunity.
                changes are in line 32 to 62 and 172 to 186.
    2017-11-03 MTM QCW-4355 OEM quotes Start Date alignment
    2018-01-22 Shubham Gupta QCW-4940 child opp schedule creation logic
 */
public class OpportunityScheduleCreationHandler { 
    static public Map<Id,Id> SplitOptyToParOppty = new Map<id,Id>();
    static public Map<Id,List<Id>> reverseSplitOptyToParOppty = new Map<id,List<Id>>();
    Static Boolean BeforeInsertRun = false;
    Static Boolean AfterInsertRun = false;
    Static Boolean BeforeUpdateRun = false;
    Static Boolean AfterUpdateRun = false;
    public static void onBeforeInsert(List<OpportunityLineItem> inputList) {
    }
    
    public static void onBeforeUpdate(List<OpportunityLineItem> inputList) {        
    }
    //Schedule invocation moved to CustomQuoteTriggerHandler
    public static void onAfterInsert(List<OpportunityLineItem>  inputList, Map<id, OpportunityLineItem> inputMap) {             
        if(!AfterInsertRun)
        {
            AfterInsertRun = true;
        //invokation of trigger method
        System.debug('Calling OpportunityScheduleCreationHandler. onAfterInsert');
        System.debug('inputList count ' + inputList.size());
        Set<String> setOfOppsid = new Set<String>();
        List<Opportunity> splitopp = new List<Opportunity>();
        
        List<OpportunityLineItem> lineParentOpp = [select id,Opportunity.ID,Opportunity.Deal_Split_Parent_Opportunity__c from OpportunityLineItem where id in:inputMap.KeySet()];
        if(!lineParentOpp.isEmpty()){
            for(OpportunityLineItem oppline:lineParentOpp){
                SplitOptyToParOppty.put(oppline.Opportunity.ID,oppline.Opportunity.Deal_Split_Parent_Opportunity__c);
                
                if(!string.isEmpty(oppline.Opportunity.Deal_Split_Parent_Opportunity__c)){
                    if(!reverseSplitOptyToParOppty.containsKey(oppline.Opportunity.Deal_Split_Parent_Opportunity__c)){
                        reverseSplitOptyToParOppty.put(oppline.Opportunity.Deal_Split_Parent_Opportunity__c,new List<id>());
                    }
                    reverseSplitOptyToParOppty.get(oppline.Opportunity.Deal_Split_Parent_Opportunity__c).add(oppline.Opportunity.ID);
                }
            }
        }
        
        system.debug('Map created is '+SplitOptyToParOppty);
        for(OpportunityLineItem oppLineItem : inputList) {
            if(string.isBlank(SplitOptyToParOppty.get(oppLineItem.OpportunityId))){
            setOfOppsid.add(oppLineItem.OpportunityId);
            }
            else{
                setOfOppsid.add(SplitOptyToParOppty.get(oppLineItem.OpportunityId));
            }
        }
        
        system.debug('Deal Split Parent oppty is '+setOfOppsid);    
           
        Map<String, SBQQ__Quote__c> mapQuoteFromOpp = queryQuoteFromOpp(setOfOppsid);
        system.debug('map returned id '+mapQuoteFromOpp);
        //processScheduleSub(inputList, mapQuoteFromOpp);
        System.enqueueJob(new AsyncOppLineItemScheduleCreation(inputList, mapQuoteFromOpp));      
       }
    }
    
    public static void onAfterUpdate(List<OpportunityLineItem>  inputList, Map<id, OpportunityLineItem> inputMap, Map<id, OpportunityLineItem> oInputMap){
        /*
        if(!AfterUpdateRun)
        {
            AfterUpdateRun = true;
            System.debug('Calling OpportunityScheduleCreationHandler. onAfterUpdate');
            System.debug('inputList count ' + inputList.size());
            //List<OpportunityLineItemSchedule> itemToCheckList = new List<OpportunityLineItemSchedule>([Select id From OpportunityLineItemSchedule Where OpportunityLineItemId =: inputMap.keySet()]);
            // List<OpportunityLineItem> itemsToProcessList = new List<OpportunityLineItem>();
            //if(itemsToProcessList.isEmpty()) {
            Set<String> setOfOppsid = new Set<String>();
            for(OpportunityLineItem oppLineItem : inputList) {
            System.Debug('oppLineItem.Net_Price__c = ' + oppLineItem.Net_Price__c);
            System.Debug('oInputMap.get(oppLineItem.Id).Net_Price__c = ' + oInputMap.get(oppLineItem.Id).Net_Price__c);
                if((oppLineItem.Net_Price__c > 0 && inputMap.get(oppLineItem.Id).Net_Price__c != oInputMap.get(oppLineItem.Id).Net_Price__c) || (inputMap.get(oppLineItem.Id).quantity != oInputMap.get(oppLineItem.Id).quantity))
                    setOfOppsid.add(oppLineItem.OpportunityId);
            }
            System.Debug('setOfOppsid.size()' + setOfOppsid.size());
            if(!setOfOppsid.isEmpty()){
                Map<String, SBQQ__Quote__c> mapQuoteFromOpp = queryQuoteFromOpp(setOfOppsid);
                deleteOldSchedules(inputList, mapQuoteFromOpp);
                System.enqueueJob(new AsyncOppLineItemScheduleCreation(inputList, mapQuoteFromOpp));
            }
        }
       */ 
    }


    public static void processScheduleSub(List<OpportunityLineItem>  inputList, Map<String, SBQQ__Quote__c> mapQuoteFromOpp) {
        System.debug('Calling OpportunityScheduleCreationHandler.processScheduleSub');
        List<OpportunityLineItemSchedule> itemsToInsert = processSchdeuleSubsUnits(inputList, mapQuoteFromOpp);
        if(!itemsToInsert.isEmpty()) {
            Semaphores.NoTwiceRun = true;
            insert itemsToInsert;
        }
    }

    public static List<OpportunityLineItemSchedule> processSchdeuleSubsUnits(List<OpportunityLineItem>  inputList, Map<String, SBQQ__Quote__c> mapQuoteFromOpp) {
        Date billDate = Date.today();
        List<OpportunityLineItemSchedule> listToScheduleInsert = new List<OpportunityLineItemSchedule>();
        Map<String, Product2> mapSubsOnOppProduct = returnSubsOnOppProduct(inputList);
        System.debug(mapQuoteFromOpp);
        System.debug(mapSubsOnOppProduct);
        //QCW-4940 start
        Map<id,Opportunity> oppIdToOpp= new Map<id,Opportunity>();
        List<Opportunity> opps = [select id,Deal_Split_Parent_Opportunity__c from Opportunity where id In :mapQuoteFromOpp.keySet()];
        for(Opportunity opp:opps){
            oppIdToOpp.put(opp.id,opp);
        }
        for(OpportunityLineItem item : inputList) {
            System.debug(item);
            //system.debug('split oppty on item'+oppIdToOpp.get(item.opportunityID).Deal_Split_Parent_Opportunity__c);
            if(mapSubsOnOppProduct.containsKey(item.id) && mapSubsOnOppProduct.get(item.id) != null) {
                if(mapQuoteFromOpp.containsKey(item.OpportunityId) && mapQuoteFromOpp.get(item.OpportunityId) != null && mapQuoteFromOpp.get(item.OpportunityId).SBQQ__Opportunity2__r.CloseDate != null && ((String.isBlank(oppIdToOpp.get(item.opportunityID).Deal_Split_Parent_Opportunity__c) && String.isBlank(mapQuoteFromOpp.get(item.OpportunityId).Closure_Trigger__c)) || !String.isBlank(oppIdToOpp.get(item.opportunityID).Deal_Split_Parent_Opportunity__c))){
                    if(mapQuoteFromOpp.get(item.OpportunityId).SBQQ__SubscriptionTerm__c == null || mapQuoteFromOpp.get(item.OpportunityId).SBQQ__SubscriptionTerm__c == 0) {
                        mapQuoteFromOpp.get(item.OpportunityId).SBQQ__SubscriptionTerm__c =12;
                    }
                    //QCW-4940 end
                    SBQQ__Quote__c quote = mapQuoteFromOpp.get(item.OpportunityId);
                    if(quote.Billing_Schedule_Start_Date__c != null){                    
                        billDate = quote.Billing_Schedule_Start_Date__c;         
                        if(item.Net_Price__c > 0){
                            System.debug('Adding Schedules');
                            listToScheduleInsert.addAll(createListSchedules(item, billDate,  mapQuoteFromOpp.get(item.OpportunityId).Billing_Schedule_Term__c));
                        }
                    }
                }
            }
        }
        System.debug(listToScheduleInsert);
        return listToScheduleInsert;
    }

    private static  Map<String, Product2> returnSubsOnOppProduct(List<OpportunityLineItem>  inputList) {
        Map<String, Product2> mapProductSetOfId = new Map<String,Product2>();
        Set<Id> oppLineItemProduct2Ids = new Set<Id>();
        for(OpportunityLineItem itemOppItem : inputList){
            if(String.isNotBlank(itemOppItem.Product2Id))
                oppLineItemProduct2Ids.add(itemOppItem.Product2Id);
        }    
        List<Product2> lProd2s = [SELECT Id FROM Product2 WHERE Id IN :oppLineItemProduct2Ids AND SBQQ__SubscriptionType__c = 'Renewable'];
        for(OpportunityLineItem itemOppItem : inputList) {
            for(Product2 itemSubs : lProd2s) {
                if(String.isNotBlank(itemOppItem.Product2Id) && itemSubs.Id == itemOppItem.Product2Id)
                    mapProductSetOfId.put(itemOppItem.id, itemSubs);
            }
        } 
        return mapProductSetOfId;
    }

    public static Map<String, SBQQ__Quote__c> queryQuoteFromOpp(Set<String> setOfOppsid) {
        
        Map<String, SBQQ__Quote__c> mapOfOppIdQuotes = new Map<String, SBQQ__Quote__c>();
        
       
        List<SBQQ__Quote__c> lSBBQuotes = [SELECT Id, SBQQ__Opportunity2__c, Product_filter_type__c, Billing_Schedule_Start_Date__c, Billing_Schedule_Term__c, 
                                            Closure_Trigger__c, SBQQ__StartDate__c, SBQQ__Primary__c, Subscription_Start_Date__c, SBQQ__Opportunity2__r.CloseDate, 
                                            Subscription_End_Date__c, SBQQ__EndDate__c, SBQQ__SubscriptionTerm__c, Revenue_Type__c
                                           FROM SBQQ__Quote__c 
                                           WHERE SBQQ__Primary__c = true AND SBQQ__Opportunity2__c IN :setOfOppsid];
        for(String itemOppId : setOfOppsid) {
            for(SBQQ__Quote__c itemQuote : lSBBQuotes){
            if(!reverseSplitOptyToParOppty.containsKey(itemOppId)){
                if(String.isNotBlank(itemQuote.SBQQ__Opportunity2__c) && itemOppId == itemQuote.SBQQ__Opportunity2__c)
                    mapOfOppIdQuotes.put(itemOppId, itemQuote);    
            }
            else{
              if(String.isNotBlank(itemQuote.SBQQ__Opportunity2__c) && itemOppId == itemQuote.SBQQ__Opportunity2__c)
                  for(Id itemOpportunityId : reverseSplitOptyToParOppty.get(itemOppId)){
                      mapOfOppIdQuotes.put(itemOpportunityId, itemQuote);
                  }
              }
          }
          
        }
        return mapOfOppIdQuotes;
    }

    private static OpportunityLineItemSchedule createScheduleItem(String oppLineId, Decimal revenue, Date scheduleDate) {
        OpportunityLineItemSchedule scheduleItem = new OpportunityLineItemSchedule(
                                                    OpportunityLineItemId = oppLineId,
                                                    Type = 'Revenue',
                                                    Revenue = revenue,
                                                    ScheduleDate = scheduleDate,
                                                    Description = 'Automated creation'
                                                    );
        return scheduleItem;
    }

    private static List<OpportunityLineItemSchedule> createListSchedules(OpportunityLineItem oppLineitem, Date inputDate, Decimal terms) {
        List<OpportunityLineItemSchedule> toInsert = new List<OpportunityLineItemSchedule> ();
        double change = 0;
        Decimal revenue = oppLineitem.Net_Price__c;
        System.Debug('XXXX oppLineitem.Net_Price__c = ' + oppLineitem.Net_Price__c);
        inputDate = inputDate.addMonths(1).toStartofMonth().addDays(-1);
        inputDate = inputDate.addDays(1); //Subscription schedule should start the first day fo next month
        for(Integer i = 0; i < terms; i++) {
            String modulus  = String.valueOf(revenue/terms);
            if( i < terms -1) {
                change += Double.valueOf(modulus.substring(modulus.lastIndexOf('.'),modulus.length()));
                toInsert.add(createScheduleItem(oppLineitem.id, Math.floor(revenue/terms), inputDate.addMonths(i)));
            } else {
                toInsert.add(createScheduleItem(oppLineitem.id, Math.round(revenue/terms + change), inputDate.addMonths(i)));
            }
        }
        return toInsert;
    }

    public static void deleteOldSchedules(List<OpportunityLineItem>  inputList, Map<String, SBQQ__Quote__c> mapQuoteFromOpp) {
        system.debug('Calling OpportunityScheduleCreationHandler.deleteOldSchedules');
        Set<String> setOfLineItems = new Set<String>();
        List<OpportunityLineItemSchedule> itemsToDelete = new List<OpportunityLineItemSchedule>();
        for(OpportunityLineItem itemToCheck : inputList) {
            if(mapQuoteFromOpp.containsKey(itemToCheck.OpportunityId) && String.isBlank(mapQuoteFromOpp.get(itemToCheck.OpportunityId).Closure_Trigger__c)) {
                setOfLineItems.add(itemToCheck.id);
            }
        }
        for(OpportunityLineItemSchedule scheduleItem : [SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId IN :setOfLineItems]) {
            itemsToDelete.add(scheduleItem);
        }        
        if(itemsToDelete.size() > 0){
            delete itemsToDelete;
        }
    }

    
    public class stException extends Exception{}
}