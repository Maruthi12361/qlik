//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efEventforceHomeController{
    private String userProfile = null;
    private String userContact = null;
    private String userAccount = null;
    private String userContactName = null;        
    private String userAttendeeType = null;
    private String userAcessLevel = null;   
    private String eventId  = null;                 
    private efEventWrapper e;
    private User loggedUser;
    private Map<String,String> configMap = efCustomSettings.getValues(efCustomSettings.EMPLOYEE);
    
    public List<efEventWrapper> listEvent { get;set; }
        
    public efEventforceHomeController(){
        loggedUser = [Select FirstName, LastName, Email, Profile.Name, ContactId, Contact.AccountId, Contact.Name From User Where Id=:UserInfo.getUserId()];
        
        userProfile = loggedUser.Profile.Name;
        userContactName = loggedUser.Contact.Name;
        userContact = loggedUser.ContactId;
        userAccount = loggedUser.Contact.AccountId;
        
        //For Employees (Salesforce.com User)
        if(userContact==null && userAccount==null && userContactName==null){
            List<Contact> loggedEmployee = [Select Id, AccountId,Name From Contact Where Email =:loggedUser.Email limit 1];
            if(loggedEmployee.size()>0){
                userAccount = loggedEmployee[0].AccountId;
                userContact = loggedEmployee[0].Id;
                userContactName = loggedEmployee[0].Name;
            }
        }        
        userAttendeeType = getAttendeeType();
        userAcessLevel = getAccessLevel();      
        listEvent = efEventWrapper.fetchMyEvents(userAcessLevel,userContact);
    }   
       
    private String getAttendeeType(){
        if(userProfile.contains('Eventforce Portal – Partner'))
            return 'Partner';
        else if(userProfile.contains('Eventforce Portal – Employee'))
            return 'Employee';
        else if(userProfile.contains('Eventforce Portal – Customer'))
            return 'Customer';
        else if(userProfile.contains('Eventforce Portal – Non Customer'))
            return 'Prospect';
        else if(userProfile.contains('Partner') || userProfile.contains('PRM'))
            return 'Partner';
        else if(userProfile.contains('Customer Portal'))
            return 'Customer';
        else 
            return 'Employee';      
    }
    
    private String getAccessLevel(){
        if(userProfile.contains('Eventforce Portal – Partner'))
            return 'Partner';
        else if(userProfile.contains('Eventforce Portal – Employee'))
            return '';
        else if(userProfile.contains('Eventforce Portal – Non Customer'))
            return 'Base';
        else if(userProfile.contains('Eventforce Portal – Customer'))
            return 'Customer';          
        else if(userProfile.contains('Partner') || userProfile.contains('PRM'))
            return 'Partner';
        else if(userProfile.contains('Customer Portal'))
            return 'Customer';
        else 
            return '';      
    }
        
    public PageReference goRegister(){
        eventId = ApexPages.currentPage().getParameters().get('eventId');
        efEventWrapper selectedEvent;
        String registrationId;
        string ContactId = userContact;

        for (efEventWrapper e : listEvent){        
        	if (e.eventId==eventId){
        		selectedEvent = e;
        		break;
        	}
        }
         
        if (eventId==null || selectedEvent==null){
        	return null;   	
        }
        
        if(selectedEvent.registrationId==null){        	
        	if(userContact==null && userAccount==null && userContactName==null){
            	// Comment& : Contact record for employee does not exist.
                Contact empNewContact = new Contact(AccountId=configMap.get('employeeaccountid'), LastName=loggedUser.LastName, FirstName=loggedUser.FirstName, Email=loggedUser.Email);             
                insert empNewContact;
                
                userAccount = empNewContact.AccountId;
                userContact = empNewContact.Id;
                ContactId = empNewContact.Id;
                if(empNewContact.FirstName!='' || empNewContact.FirstName!=null)
                    userContactName = empNewContact.FirstName + ' ' + empNewContact.LastName;
                else
                    userContactName = empNewContact.LastName;
            } else {
            	ContactId = ApexPages.currentPage().getParameters().get('contactId');
            	if (ContactId == null || ContactId.length() == 0) {
            		if (userContact == null) {
            			return null;
            		}
            		ContactId = userContact;
            	}
            	if (!efUtility.checkContactBelongsToAccount(ContactId, userAccount))
            	{
            		return null;
            	}            	
            }
            
            // Comment& : double-check to prevent multiple registration creation.
            List<Registration__c> listReg = [Select Id From Registration__c 
            								Where Event__c=:eventId And Registrant__c=:ContactId And Status__c=:efConstants.REG_STATUS_SAVED_FOR_LATER
            								Order By CreatedDate Desc];
            if (listReg.size()>0){
            	registrationId = listReg[0].Id;
            }else {
	            Registration__c reg = new Registration__c(Event__c=eventId);
	            reg.Portal_Attendee_Type__c = getAttendeeType();
	            reg.Registrant__c = ContactId;
	            reg.Account__c = userAccount;
	            reg.Status__c = efConstants.REG_STATUS_SAVED_FOR_LATER;
	            reg.Name = selectedEvent.eventName+' - '+ userContactName;                
	            insert reg;
	            
	            registrationId = reg.Id;            	
            }
        } else{
        	registrationId = selectedEvent.registrationId; 
        }
        
        List<Payment_Details__c> pList = [Select Id From Payment_Details__c Where RegistrationId__c=:registrationId Limit 1];
        if (pList==null || pList.size()==0){
   		  	Payment_Details__c p= new  Payment_Details__c(Amount__c = 0.0,
	                                                  		Transaction_Date__c=system.today(),
			                                               	Return_Code__c='-1', //For Pending
			                                               	CurrencyIsoCode='USD',
			                                               	RegistrationId__c=registrationId);
	    	insert p;
        }    
        
        if (selectedEvent.getCampaignId() != null) {
        	List<CampaignMember> CMs = [Select Status, 
		        							   Id, 
		        							   ContactId, 
		        							   CampaignId 
		        						 From CampaignMember 
		        						 Where CampaignId = :selectedEvent.getCampaignId() and ContactId = :ContactId];
        	System.debug('Got these campaigns : (' + CMs.size() + ') ' + CMs);
        	if (CMs.size() == 0){
	        	CampaignMember cm = new CampaignMember();
		        cm.ContactId = ContactId;
		        cm.CampaignId = selectedEvent.getCampaignId();
		        cm.Status = efConstants.CAMPAIGN_MEMBER_CREATE;
		        insert cm;								
        	}			        						 
        }
        
        PageReference pr = efAppNavigator.pr(efAppNavigator.REGISTRATION_ATTENDEE_INFO_PAGE);
        pr.getParameters().put('registrationId', registrationId);
        pr.getParameters().put('contactId', contactId);
        return pr;
    }
    
    
    public PageReference goAttendeePortal(){
    	String registrationId = ApexPages.currentPage().getParameters().get('registrationId');
    	if (registrationId==null || registrationId.length()==0) return null;
    	
        PageReference pr = efAppNavigator.pr(efAppNavigator.ATTENDEE_PORTAL_HOME);
        pr.getParameters().put('registrationId', registrationId);               
        return pr;
    }
    
    public PageReference goGroupRegister(){
    	String eventId = ApexPages.currentPage().getParameters().get('eventId');
    	if (eventId==null || eventId.length()==0) return null;
    	
        PageReference pr = efAppNavigator.pr(efAppNavigator.REGISTRATION_REGISTER_GROUP);
        pr.getParameters().put('eventId', eventId);               
        return pr;
    }    
    
    public PageReference goLogout(){
        return new PageReference('/secur/logout.jsp');
    }    
    
    public string getContactId() {
    	return userContact;
    }  
}