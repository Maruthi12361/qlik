/****************************************************************
*
*  TEST_LicenseDeliveryActivityManager
*
*  06.02.2017  RVA :   changing CreateAcounts methods
*  23.03.2017 : Rodion Vakulvsokyi
*****************************************************************/
@isTest
public class TEST_LicenseDeliveryActivityManager {
	static final String AccRecordTypeId_EndUserAccount = '01220000000DOFu';
	public static testmethod void processTest() {
		User userForTest = [Select id From User Where id =: UserInfo.getUserId()];
		QuoteTestHelper.createCustomSettings();
		/*removed
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c companyQT = TestQuoteUtil.createQCompany(sub.id);
		*/
		Account  testAcc = QTTestUtils.createMockAccount('TestCompany', userForTest, true);
	        testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAcc;
		Account testResseller = TestQuoteUtil.createReseller();
        Contact testContact = TestQuoteUtil.createContact(testResseller .id);
		/*
        Account  testAcc = TestQuoteUtil.createAccount(companyQT.id);
		*/
        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
    	SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
		EmailTemplate emailTemplateToTest = [select id from emailtemplate where name = 'License Key Distribution - Scriptable - Default'];
		LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest inerClass = new LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest();
		inerClass.quoteId = quoteForTest.id;
		inerClass.templateId = emailTemplateToTest.id;
		inerClass.toAddress = userForTest.id;
		inerClass.ccRecipients = userForTest.id;
		List<LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest> innClassList = new List<LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest>{inerClass};
		LicenseDeliveryActivityManager.templateDecider(innClassList);
	}

	public static testmethod void processTest2() {
		User userForTest = [Select id From User Where id =: UserInfo.getUserId()];
		QuoteTestHelper.createCustomSettings();
		/*removed
        Subsidiary__c sub = TestQuoteUtil.createSubsidiary();
        QlikTech_Company__c companyQT = TestQuoteUtil.createQCompany(sub.id);
		*/
		Account  testAcc = QTTestUtils.createMockAccount('TestCompany', userForTest, true);
	        testAcc.RecordtypeId = AccRecordTypeId_EndUserAccount;
	        update testAcc;
		Account testResseller = TestQuoteUtil.createReseller();
        Contact testContact = TestQuoteUtil.createContact(testResseller .id);
		/*
        Account  testAcc = TestQuoteUtil.createAccount(companyQT.id);
		*/
        RecordType rType = [Select id From Recordtype Where DeveloperName = 'Quote'];
    	SBQQ__Quote__c quoteForTest =  TestQuoteUtil.createQuote(rType, testAcc);
		quoteForTest.SBQQ__SalesRep__c = userForTest.Id;
		EmailTemplate emailTemplateToTest = [select id from emailtemplate where name = 'License Key Distribution - Scriptable - Default'];
		LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest inerClass = new LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest();
		inerClass.quoteId = quoteForTest.id;
		inerClass.templateId = emailTemplateToTest.id;
		inerClass.toAddress = userForTest.id;
		inerClass.ccRecipients = userForTest.id;
		List<LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest> innClassList = new List<LicenseDeliveryActivityManager.LicenseDeliveryActivityManagerRequest>{inerClass};
		LicenseDeliveryActivityManager.templateDecider(innClassList);
		LicenseDeliveryActivityManager.createSimpleEmail(quoteForTest, '');
	}

	private static EmailTemplate createEmail() {
		EmailTemplate emailTemplateToTest = new EmailTemplate (
						isActive = true,
						developerName = 'test',
						FolderId = UserInfo.getUserId(),
						TemplateType= 'HTML',
						Name = 'test',
						Body = 'ssssssss',
						HtmlValue = '<p>test</p>'
						//HTMLBody = '<p>test</p>'
						);
	return emailTemplateToTest;
	}
}