//*********************************************************/
// Author: Mark Cane&
// Creation date: 18/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efUtility {
    
    public static String giveValue(String val){
        if(isNull(val))
            return '';
        else 
            return val; 
    }
    
    public static String getURL(String val){
        if(val.indexOf('?')>-1)
            return val.substring(0,val.indexOf('?'));
        else
            return val; 
    }
    
    
    // Datetime methods.
    
    public static String getFormatedDate(DateTime dt){
        if(dt!=null)
            return dt.format('E, MMM d');
        else
            return '';   
    }
    
    public static String getFormatedDateYear(DateTime dt){
        if(dt!=null)
            return dt.format('E, MMM d yyyy');
        else
            return '';   
    }
    
    public static String getFullFormatedDate(DateTime dt){
        if(dt!=null)
            return dt.format('EEEE, MMMM d');
        else
            return '';   
    }
    
    public static String getFullFormatedDateGMT(DateTime dt){
        if(dt!=null)
            return dt.format('EEEE, MMMM d', 'GMT');
        else
            return '';   
    }
                    
    public static String getFormatedTime(DateTime dt){
        if (dt!=null)
            return dt.format('H:mm');
        else
            return '';   
    }
    
    public static String getFormatedTimeWithAM(DateTime dt){
        if (dt!=null)
            return dt.format('h:mm a');
        else
            return '';   
    }
    
    public static String getFormatedYear(DateTime dt){
        if (dt!=null)
            return dt.format('yyyy');
        else
            return '';   
    }
    
    public static DateTime getStringToDate(String strDate){
        if(efUtility.isNull(strDate))
            return null;

        Integer month = Integer.valueOf(strDate.subString(0,strDate.indexOf('/')));
        Integer da = Integer.valueOf(strDate.subString(strDate.indexOf('/')+1,strDate.lastIndexOf('/')));
        Integer year = Integer.valueOf(strDate.substring(strDate.lastIndexOf('/')+1)); 
        DateTime dt = DateTime.newInstanceGmt(year, month, da,0,0,0);       
        return dt; 
    }
    
    public static DateTime getStringToDateNonGMTOld(String strDate){
        if (efUtility.isNull(strDate))
            return null;

        Integer month = Integer.valueOf(strDate.subString(0,strDate.indexOf('/')));
        Integer da = Integer.valueOf(strDate.subString(strDate.indexOf('/')+1,strDate.lastIndexOf('/')));
        Integer year = Integer.valueOf(strDate.substring(strDate.lastIndexOf('/')+1)); 
        DateTime dt = DateTime.newInstance(year, month, da,0,0,0);      
        return dt; 
    } 
    
   	public static DateTime getStringToDateNonGMT(String strDate){
    	if (efUtility.isNull(strDate))
            return null;
            
    	try {
	        Integer month = Integer.valueOf(strDate.subString(0,strDate.indexOf('/')));
	        Integer da = Integer.valueOf(strDate.subString(strDate.indexOf('/')+1,strDate.lastIndexOf('/')));
	        Integer year = Integer.valueOf(strDate.substring(strDate.lastIndexOf('/')+1)); 
	        DateTime dt = DateTime.newInstance(year, month, da,0,0,0);      
        	return dt;     		
    	} catch(Exception ex){
    		return null;
    	}
    } 

    public static String getDateFormated(Date dt){
        if (dt==null)
            return '';
            
        String Day = String.valueOf(dt.day());
        String Month = String.valueOf(dt.month()) ;
        
        if (Day.length() == 1 )
            Day = '0' + Day ;       
        
        if (Month.length() == 1 ) 
            Month = '0' + Month ;
            
        if(dt!=null)
            return Month + '/' + Day + '/' + String.valueOf(dt.year());
        else
            return '';
    }
    
    public static DateTime getStringToDateForSessions(String strDate,String strTime){
        if(efUtility.isNull(strDate))
            return null;
        
        try {
	        Integer month;
	        Integer hour = 0;
	        Integer min = 0;
	        
	        String monthStr = strDate.subString(strDate.indexOf(',')+1);
	        
	        monthStr = monthStr.trim();
	    
	        Map<String, Integer> calendarMap = new Map<String, Integer>{'Jan' => 1, 'Feb' => 2 , 'Mar' => 3 ,'Apr' => 4 ,'May' => 5 ,'Jun' => 6 ,'Jul' => 7 ,'Aug' => 8 ,'Sep' => 9 ,'Oct' => 10 ,'Nov' => 11 ,'Dec' => 12};  
	        
	        System.debug('Month VAl1' + monthStr.substring(0,3));
	        
	        if(monthStr.substring(0,3) != null){
	            month = calendarMap.get(monthStr.substring(0,3));
	            System.debug('Month VAl' + month);
	        }
	        
	        Integer year = 2009;	
	        Integer da = Integer.valueOf(monthStr.subString(monthStr.indexOf(' ')+1));
	            
	        if(strTime.contains('p.m.')){
                String[] valArr = strTime.split(':');
                if(Integer.valueof(valArr[0].trim()) == 12)
                hour = Integer.valueof(valArr[0].trim());
                else 
                hour = Integer.valueof(valArr[0].trim()) + 12;
                
                String[] tempSpilt = valArr[1].split(' ');
                min = Integer.valueof(tempSpilt[0].trim());
            }
            else {
                String[] valArr = strTime.split(':');
                hour = Integer.valueof(valArr[0].trim());
                String[] tempSpilt = valArr[1].split(' ');
                min = Integer.valueof(tempSpilt[0].trim());
                
            }
	        DateTime dt = DateTime.newInstance(year, month, da,hour,min,0);       	        
	        return dt; 
        } catch(Exception e){
            return null;
        }
    }
	// End Datetime methods.
    
    public static String getAttendeeCode(String Attendee){
        Map<String,String> configMap = efCustomSettings.getValues(efCustomSettings.CONFIGURATION);
        String attendeeCode = '';
        
        try{
            attendeeCode = configMap.get(Attendee);
        }
        catch(Exception e){
            attendeeCode = 'DREATT1108'; //For customer
        }        
        return attendeeCode;    
    }
    
    /*************For making it an extension to Registration__c*************************/

    private final Registration__c reg;

    public efUtility(ApexPages.StandardController stdController){
        this.reg = (Registration__c)stdController.getRecord();
    } 

    public static boolean getPaymentStatus(String status){
        if (!efUtility.isNull(status) && status.indexOf('ERROR') == -1){
            return true;
        }        
        return false;
    }
    
    public static String formatDecimals(String amount){
        if(!efUtility.isNull(amount)){
            if(amount.indexOf('.')>-1){
                if(amount.lastIndexOf('.') + 2 == amount.length())
                    return amount + '0';
                else if(amount.lastIndexOf('.') + 3 > amount.length())
                    return amount.substring(amount.lastIndexOf('.') + 3);
                else if(amount.lastIndexOf('.') + 3 < amount.length())
                    return amount.substring(0,amount.lastIndexOf('.') + 3);         
            }
            else {
                return amount + '.00';
            }
        } else{
            return '0.00';
        }        
        return amount;
    }

    public static String formatNegative(String amt){
        if(efUtility.isNumber(amt) && double.valueOf(amt)<0){
            amt = '(' + amt.substring(1,amt.length()) + ')';
        }
        return amt;
    }

    public static String getWrapString(String st, Integer i){
        if(!efUtility.isNull(st) && st.length()>i){
            String newSt = '';
            try{
                while(st.length()>i){
                    newSt = newSt + st.substring(0,i) + '\n';
                    st = st.substring(i);
                }
            }
            catch(Exception e){
                System.debug('WRAPERROR: ' + e.getMessage());
            }             
            newSt = newSt + st;
            return newSt;
        }        
        return st;
    }
    
    public static String generateOrderNumber(){
        String timeFormat='';

        //The date format is yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
        DateTime d = System.now();
        String timestamp = ''+ d.year() +
        d.month() +
        d.day() +
        d.hour() +
        d.minute() +
        d.second() +
        d.millisecond();
        timeFormat= d.formatGmt(timestamp); 
        return timeFormat;
    }
    
    public static boolean isAttendeeType(string stype,Registration__c r){
        if (r!=null &&
            !efUtility.isNull(stype) &&
            !efUtility.isNull(r.portal_attendee_type__c) &&
            stype.trim().equals(efUtility.giveValue(r.portal_attendee_type__c).trim())){
            return true;
        } else{
            return false;
        }
    }
    
    public static Set<String> splitStringToSet(String st){
        if(efUtility.isNull(st))
            return null;
            
        String[] arr = st.split(',');
        Set<String> finalSet = new Set<String>();
        
        for(String s:arr){   
            finalSet.add(s);
        }        
        return finalSet;    
    }
    
    public static List<String> splitStringToList(String st,String ch){
        if(efUtility.isNull(st))
            return null;
            
        String[] arr = st.split(ch);
        List<String> finalSet = new List<String>();
        
        for(String s:arr){   
            finalSet.add(s.trim());
        }        
        return finalSet;    
    }
    
    public Static Map<String,String> splitListToMap(List<String> l, String ch){
        Map<String,String> result = new Map<String,String>();
        
        for (String s:l){
            String[] arr = s.split(ch);
            result.put(arr[0],arr[1]);
        }        
        return result;
    }

    public static String getMessagesInString(List<ApexPages.message> lmsgs){
        String msg = '';
        
        if (lmsgs != null && lmsgs.size() > 0){
            for (ApexPages.Message l:lmsgs){
                /*
                ApexPages.Severity s = l.getSeverity();
                
                if (s != ApexPages.Severity.WARNING)
                {
                    msg = l.getSummary() + ', ';
                }
                */
                
                if (msg != ''){
                    msg += ', ';
                }                
                msg += l.getSummary();
            }
        }
        
        if(msg.lastIndexOf('username already exists') != -1){
            msg = 'User already exists with this Email address'; 
        }           
        return msg;
    }
    
    /************************* VALIDATION STARTS **********************/

    public static Boolean isNull(String value){
        if(value==null || value.trim().equals('')) 
            return true;
    
        return false;
    }
    
    public static Boolean isNumber(String value){
        if (!isNull(value)){
            Pattern noPattern = Pattern.compile('\\d+');
            Matcher noMatcher = noPattern.matcher(value);
            return noMatcher.matches();
        } else{
            return false;
        }
    }
    
    public static Boolean isValidUserID(String userID){
        if (!isNull(userID)){
            Pattern userPattern = Pattern.compile('005[A-Za-z0-9]{12,15}');
            Matcher userMatcher = userPattern.matcher(userID);
            return userMatcher.matches();
        }        
        return false;
    }
    
    public static Boolean isValidEmail(String email){
        if (!isNull(email)){
            Pattern MyPattern = Pattern.compile('[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[ma-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?');
            Matcher MyMatcher = MyPattern.matcher(email.trim().toLowerCase());
            return MyMatcher.matches();
        } else
            return false;
    }

    public static Boolean isText(String s){
        if (!isNull(s)){
            // Should have at least one character,
            // and then character or spaces
            Pattern MyPattern = Pattern.compile('[\\p{L}][\\p{L}\\s]*');
            Matcher MyMatcher = MyPattern.matcher(s.trim().toLowerCase());
            return MyMatcher.matches();
        } else
            return false;
    }

    public static Boolean isValidPhone(String phone){
        if (!isNull(phone)){
            // Between 7 and 30 symbols, with symbol = digit | - | ( | ) | . | space | +
            Pattern phonePattern = Pattern.compile('[\\d\\s\\(\\)\\-\\.\\+]{7,30}');
            Matcher phoneMatcher = phonePattern.matcher(phone.trim());
            return phoneMatcher.matches();
        } else
            return false;
    }

    public static Boolean isValidZipCode(String zipCode){
        if (!isNull(zipCode)){
            // ATRCHANGE            
            Pattern zipPattern = Pattern.compile('[\\s\\-\\d\\p{Alpha}]{1,9}');
            Matcher zipMatcher = zipPattern.matcher(zipCode.trim());
            return zipMatcher.matches();
        } else
            return false;
    }

    public static Boolean isValidState(String state, String country){
        if (efUtility.isNull(state) &&
            (!efUtility.isNull(country) &&
            (country.equals('US') ||
             country.equals('CA')))){
            return false;
        }        
        if (!efUtility.isNull(state)){
            return isText(state) && state.length() <= 40;
        }        
        return true;
    }
    
    /************************* VALIDATION ENDS **********************/

    /**
    * agrassi - Takes out some special characters,
    *           respect to SkipJack payment.
    */
    public static String replaceInsecureChars(String value){
        if (!isNull(value)){
            value = value.replaceAll('&',';amp;');
            value = value.replaceAll('=',';eq;');
        }        
        return value;
    }

    public static String encodeHTML(String str){
        if (!efUtility.isNull(str)){
            str = str.replaceAll('&','&amp;');
            str = str.replaceAll('>','&gt;');
            str = str.replaceAll('<','&lt;');
            str = str.replaceAll('"','&quot;');
        }
        return str;
    }
    
    // New Method added for audit logging purposes
    /*
    Author : srawane
    Parameters : Content of Debug Log, Debug Code and Debug Type(Sucess/Error)
    */
    public static void updateDebugLog(String content, String debugcode, String debugType){    
        Debug_Log__c debugLog = new Debug_Log__c();
        String contactId = null;
        String userId = UserInfo.getUserId();
        User u = [select Id,ContactId from User where Id=:userId];
    
        if(u.ContactId!=null){
            contactId = u.ContactId; 
        }
   
        debugLog.Contact__c = contactId;
        debugLog.Content__c = content;
        debugLog.Debug_Code__c = debugcode;
        debugLog.Debug_Type__c = debugType;
    
        insert debugLog ;
    }
    
    public static String formatSpecialCharc(String stringValue){
        if(stringValue.contains('amp;')){
            stringValue = stringValue.Replace('amp;','');
        }        
        if(stringValue.contains('&lt;BR/&gt;')){
            stringValue = stringValue.Replace('&lt;BR/&gt;','<BR/>');
        }        
        return stringValue;        
    }

    public static List<String> invertStringList(List<String> originalList){
        List<String> invertedList = new List<String>();
        
        for (Integer i = originalList.size() -1 ; i >= 0; i--)
            invertedList.add(originalList[i]);

        return invertedList;
    }

    public static List<Double> invertDoubleList(List<Double> originalList){
        List<Double> invertedList = new List<Double>();
        
        for (Integer i = originalList.size() - 1; i > 0; i--)
            invertedList.add(originalList[i]);
        
        return invertedList;
    }
    
    public static String removedSpecialCharsHotel(String hotelS){
        String returnStr = '';
        
        if(hotelS!=null){
            returnStr = hotelS.replaceAll('&','%26');
        }        
        return returnStr;
    }
    
    public static String replaceNulls(String hotelN){
        String returnStr;
        
        if(hotelN == null){
            returnStr = '';
        } else {
            returnStr = hotelN;
        }         
        System.debug('Coming Here_Inside ::: '+ returnStr);        
        return returnStr;
    }
    
    public static DateTime convertToGMT(DateTime dt){
    	return DateTime.valueOf(dt.format('yyyy-MM-dd HH:mm:ss', 'GMT'));
    }    
    
    public static Boolean checkContactBelongsToAccount(string contactId, string accountId) 
    {    
    	try 
    	{
	    	Contact c = [select Id, AccountId from Contact where Id = :contactId];    	
	    	if (c != null) 
	    	{
	    		return (c.AccountId == accountId);	
	    	}
    	} catch (System.Exception Ex){}
    	return falsE;
    }
    
    public static string getContactIdFromRegistration(string registrationId)
    {
    	Registration__c reg = [select Id, Registrant__c from Registration__c where Id = :registrationId];
    	if (reg != null) 
    	{
    		return reg.Registrant__c;
    	}
    	return null;
    }
    
}