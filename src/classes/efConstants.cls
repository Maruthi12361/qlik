//*********************************************************/
// Author: Mark Cane&
// Creation date: 18/08/2010
// Intent:  
//			
// Change History
// --------------
// 	2013-01-08		SAN		Adding changes for partner paying by invoice						
//*********************************************************/
public class efConstants {    
    public static final String REG_STATUS_SAVED_FOR_LATER = 'Saved for Later';
    public static final String REG_STATUS_REGISTERED = 'Registered';
    public static final String REG_STATUS_INVOICED = 'Invoiced';
    public static final String REG_STATUS_CANCELLATION_REQUESTED = 'Cancellation Requested';
    public static final String REG_STATUS_CANCELLED = 'Cancelled';
    
    public static final String OPP_STATUS_CANCELLED = 'Cancelled';
    public static final String OPP_STATUS_CANCELLATION_REQUESTED = 'Cancellation Requested';
    public static final String OPP_STATUS_PENDING = 'Pending';
    public static final String OPP_STATUS_CLOSED_PAID = 'Closed';
        
    public static final String OLI_STATUS_ADDED = 'Added';
    public static final String OLI_STATUS_CANCELLATION_REQUESTED = 'Cancellation Requested';
    public static final String OLI_STATUS_CANCELLED = 'Cancelled';
    public static final String OLI_STATUS_PAYMENT_DENIED = 'Payment Denied';
    
    public static final String OPP_CONTACT_ROLE_REGISTRANT = 'Registrant';
            
    public static final String PROD_FAMILY_REGISTRATION = 'Registration';    
    public static final String PROD_FAMILY_CLASS = 'Class';
    public static final String PROD_FAMILY_PROMO = 'Promo Code';
    public static final String PROD_FAMILY_ADDON = 'Add-On';

    public static final String PROMO_TYPE_EDUCATION = 'Education';
    public static final String PROMO_TYPE_REGISTRATION = 'Registration';
    
    public static final String PROMO_MESSAGE_CANNOT_APPLY = 'Promo code cannot be applied';
    public static final String PROMO_MESSAGE_USED = 'Promo code used';
    public static final String PROMO_MESSAGE_ALREADY_ADDED = 'Promo code already applied';
    public static final String PROMO_MESSAGE_NOT_VALID = 'Promo code not valid';
    public static final String PROMO_MESSAGE_TRAINING_DISCOUNT_GREATER = 'Training promo code discount greater than amount due';
    public static final String PROMO_MESSAGE_DISCOUNT_GREATER = 'Promo code discount greater than amount due';
    public static final String PROMO_MESSAGE_BLANK = 'Please enter a valid promo code';
    public static final String PROMO_MESSAGE_NO_TRAINING = 'This code applies to training only and no classes have been added';
    
    public static final String ATTENDEE_TYPE_SPONSOR = 'Partner';
    public static final String ATTENDEE_TYPE_EMPLOYEE = 'Employee';
    public static final String ATTENDEE_TYPE_ATTENDEE = 'Customer'; // Comment& : includes prospects.      
    
    public static final String SUB_ATTENDEE_TYPE_CUSTOMER = 'Customer';
    
    public static final String GENERIC_TYPE_CLASS = 'Class';
    public static final String GENERIC_TYPE_SESSION = 'Session';
    public static final String GENERIC_TYPE_ACTIVITY = 'Activity';
    
    public static final String SESSION_STATUS_CANCELLED = 'Cancelled';
    public static final String SESSION_ATTENDANCE_CANCELLED = 'Cancelled';
    public static final String SESSION_ATTENDANCE_REGISTERED = 'Registered';

	public static final Integer CLASS_FULL_WARNING_LIMIT = 80;
	
	public static final String CAMPAIGN_MEMBER_CREATE = 'Invite Link Clicked';
	public static final String CAMPAIGN_MEMBER_REGISTERED = 'Registered';
}