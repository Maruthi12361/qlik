/************************************************************
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
************************************************************/
public with sharing class QS_EnvironmentListController {

    // component Attributes 
    public boolean showSelectionOption { get; set; }
    public Id showForAccount { get; set; }
    public Environment__c selectedEnvironment { get; set; }
    public Id selectedEnvironmentId { get; set; }

    //All environtments
    public List<Environment__c> allEnvironments{ get; set; }
    
    //All test environtment
    public List<EnvironmentWrapper> testEnvironments{ get; set; }
    
    //All development environtment
    public List<EnvironmentWrapper> devEnvironments{ get; set; }
    
    //All Production environtment
    public List<EnvironmentWrapper> prodEnvironments{ get; set; }

    //public Id accountId { get; set; }



    public QS_EnvironmentListController() {
        // get the accountid 
        //this.accountId = getLoggedinUsersAccountId();
        
        
        // loadlist of all environments 
        setEnvironments();
    }


    public void setEnvironments() {
        system.debug('~~~~~inside setEnvironments showForAccount-' + showForAccount);
        
        String queryEnvironment = 'SELECT e.Account__c, e.Virtual__c, e.CreatedDate, e.Version__c, e.Type__c, e.Product__c, e.Product_License__c, e.Product_License__r.Name, e.Operating_System__c, e.Name,' 
                                   + ' e.Description__c, e.Clustered__c, e.Architecture__c,  e.Account__r.Name, e.Last_Used_Date__c'
                                   + ' From Environment__c e';
        if ( showForAccount != null ) {
            queryEnvironment += ' WHERE Account__c =: showForAccount';
        }
        
        //if(caseWizardRef != null || caseDetailRef != null) ********* LM check
        //    queryEnvironment += ' ORDER BY Last_Used_Date__c DESC NULLS Last';
        
        List<Environment__c> environments = (List<Environment__c>) Database.query ( queryEnvironment );
        system.debug('~~~~~inside setEnvironments environments.size-' + environments.size());
        
        //save in global var to be used in Test & Production func
        allEnvironments = environments;
        
        //list for each environment
        prodEnvironments = new List<EnvironmentWrapper>();
        testEnvironments = new List<EnvironmentWrapper>();
        devEnvironments  = new List<EnvironmentWrapper>();
        
        for (Environment__c env : allEnvironments ) {
            if(env.Type__c=='Production') {
                prodEnvironments.add( new EnvironmentWrapper(env) );
            } else if(env.Type__c=='Test') {
                testEnvironments.add( new EnvironmentWrapper(env) );
            } else if(env.Type__c=='Development') {
                devEnvironments.add( new EnvironmentWrapper(env) );
            }
        }
        system.debug('~~~~~inside setEnvironments ProdEnvironment.size-' + prodEnvironments.size() + '--testEnvironments.size-' + testEnvironments.size() + '--devEnvironments.size-' + devEnvironments.size() );
    }
        
    /** 
    * gets account id of logged in user 
    */    
    /*private Id getLoggedinUsersAccountId() {
        return [ Select contact.AccountId from user where id =: Userinfo.getUserid() ].contact.AccountId ;
    }*/       
        
        
    /** 
    * Class to wrap Environment
    */    
    public class EnvironmentWrapper {
        public Environment__c environment { get; set; }
        public Boolean selected {get; set;}
        
        public EnvironmentWrapper ( Environment__c passedinEnvironment ){
            this.environment = passedinEnvironment;
            this.selected = false;
        }
    }        
 
     
     
    public boolean getShowAccountColumn() { 
        if ( showForAccount != null ){
            return false;
        } else {
            return true;
        }
    }
     /*
    public void updateData(){
        selectedEnvironment = passedinEnvironment;
    }     
     */
    public PageReference processSelected() {
        
        system.debug ('~~~~selectedEnvironmentId~'+ selectedEnvironmentId);
        //selectedEnvironment.id = selectedEnvironmentId;
        for ( Environment__c env : allEnvironments ){
            if ( env != null && env.id == selectedEnvironmentId ){
                selectedEnvironment.id = selectedEnvironmentId;
                selectedEnvironment.name = env.name;
                //selectedEnvironment.id = env;
            }
        }
        
        setEnvironments();
        
        // cycle through each list and check if the record is selected
		return null;
	}

 
     
}