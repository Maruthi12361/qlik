//*********************************************************/
// Author: Mark Cane&
// Creation date: 16/08/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
public class efNavigationPanelController{
    private String currentSection;    
    private Boolean showSessions = false;
    private Boolean showActivities = false;
    private String registrationId;
            
    public void setShowSessions(Boolean showSessions){
        this.showSessions = showSessions;
    }
    
    public Boolean getShowSessions(){
        return showSessions;
    }
    
   	public void setShowActivities(Boolean showActivities){
        this.showActivities = showActivities;
    }
    
    public Boolean getShowActivities(){
        return showActivities;
    }
    
    public void setCurrentSection(String currentSection){
        this.currentSection = currentSection;
    }
    
    public String getCurrentSection(){
        return currentSection;
    }
    
    public void setRegistrationId(String registrationId){
        this.registrationId = registrationId;
    }
    
    public String getRegistrationId(){
        return registrationId;
    }
        
    public efNavigationPanelController(){}   
}