/**********************************************************************************************
    Class OppCompetitorWarning

    
        Changelog:
            2014-10-31 MTM      CR# 18551     Initial development            
***********************************************************************************************/
global class OppCompetitorWarning
{
       WebService static void UpdateCompWarning(String Id)
       {
           Opportunity opp = [Select Id, CompWarning__c from Opportunity o where o.Id =: Id];
           opp.CompWarning__c = true;
           update opp;
       }
    	
        
}