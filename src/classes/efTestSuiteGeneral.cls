//*********************************************************/
// Author: Mark Cane&
// Creation date: 27/10/2010
// Intent:  
//			
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/
@isTest
private class efTestSuiteGeneral {

    static testMethod void pickListsUnitTest(){ 
        efTestSuiteHelper.setup();    	
    	   	
    	efPickLists pl = new efPickLists();
    	List<SelectOption> options = new List<SelectOption>();
    	
		options = pl.getCountryList();
		options = pl.getEmployeeList();
		options = pl.getIndustryList();
		options = pl.getWhyNoHotelList();
		options = pl.getEmployeeDepartmentList();
		options = pl.getJobFunctionList();
		options = pl.getJobLevelList();
		options = pl.getTrackList(efTestSuiteHelper.e.Id);
		options = pl.getEventDateList(efTestSuiteHelper.e.Event_Start_Date__c, efTestSuiteHelper.e.Event_End_Date__c);
		options = pl.getUsStateList();
		options = pl.getCaStateList(); 
		options = pl.getTracklist();
		options = pl.getEventExtendedStayArrivalList(efTestSuiteHelper.e.Extended_Stay_Start_Date__c, efTestSuiteHelper.e.Extended_Stay_End_Date__c);
		options = pl.getEventExtendedStayDepartureList(efTestSuiteHelper.e.Extended_Stay_Start_Date__c, efTestSuiteHelper.e.Extended_Stay_End_Date__c);
    }
    
	static testMethod void navigationPanelControllerUnitTest(){
		Boolean b = false;
		String s = '';
		
		efNavigationPanelController ct= new efNavigationPanelController();	
		ct.setShowSessions(true);    
	    b = ct.getShowSessions();   
	   	ct.setShowActivities(true);    
	    b = ct.getShowActivities();   
	    ct.setCurrentSection('currentSection');    
	    s = ct.getCurrentSection();
	    ct.setRegistrationId('registrationId');    
	    s = ct.getRegistrationId();
	}
	
	static testMethod void utilityUnitTest(){
		Boolean b = false;
		String s = '';
		DateTime d;
				
	    s = efUtility.giveValue('val');
	    s = efUtility.getURL('val');	    	    
	    s = efUtility.getFormatedDate(DateTime.now());	
	    s = efUtility.getFormatedDateYear(DateTime.now());	    
	    s = efUtility.getFullFormatedDate(DateTime.now());                    
	    s = efUtility.getFormatedTime(DateTime.now()); 
	    s = efUtility.getFormatedTimeWithAM(DateTime.now());	   
	    s = efUtility.getFormatedYear(DateTime.now());	    
	    d = efUtility.getStringToDate(DateTime.now().format('dd/MM/yyyy'));    
	    d = efUtility.getStringToDateNonGMTOld(DateTime.now().format('dd/MM/yyyy'));    	
	    d = efUtility.getStringToDateNonGMT(DateTime.now().format('dd/MM/yyyy'));
	    s = efUtility.getDateFormated(Date.today());
	    d = efUtility.getStringToDateForSessions(Date.today().format(),'12:22');
	    s = efUtility.getAttendeeCode('Attendee');	
	    b = efUtility.getPaymentStatus('Paid');	    
	    s = efUtility.formatDecimals('125.50');
	    s = efUtility.formatNegative('124.44');	
	    s = efUtility.getWrapString('test123', 1);
	    s = efUtility.generateOrderNumber();
	    	    
	    Set<String> setStr = efUtility.splitStringToSet('121:23423');
	    List<String> listStr = efUtility.splitStringToList('121:23423',':');
	    b = efUtility.isNull('value');   
	    b = efUtility.isNumber('12');	    
	    b = efUtility.isValidUserID('3243');	    
	    b = efUtility.isValidEmail('s@g.com');	
	    b = efUtility.isText('abc');	
	    b = efUtility.isValidPhone('123234343');
	    b = efUtility.isValidZipCode('e99 9hn');
	    b = efUtility.isValidState('state', 'UK');	    
	    s = efUtility.replaceInsecureChars('2342343');	
	    s = efUtility.encodeHTML('TEST');
	    efUtility.updateDebugLog('ERROR MSG', '23', 'WARN');
	    s = efUtility.formatSpecialCharc('123');	
	    listStr = efUtility.invertStringList(new List<String>{'originalList'});
	    List<Double> listDbl = efUtility.invertDoubleList(new List<Double> {123,122});
	    s = efUtility.removedSpecialCharsHotel('hotel1');	    
	    s = efUtility.replaceNulls('hotelN');
	    d = efUtility.convertToGMT(DateTime.now());
	}
}