/*
* Change log: 
* 05-08-2018 - ext_vos - Replace outdated logic. Add testing of redirect to actual search page: QS_CoveoSearch
* 2019-05-05 AIN IT-1597 Updated for Support Portal Redesign
*/

@isTest
private class QS_SearchTest {

    static testMethod void test_searchResultsController() {
        QS_SearchResultsController searchController = new QS_SearchResultsController();
        String redirectUrl = searchController.redirectToActualSearchPage().getUrl();
        System.assert(redirectUrl.contains('QS_CoveoSearch'));
    } 

    static testMethod void test_searchResultsTest() {
        QS_SearchResultsTest searchController = new QS_SearchResultsTest();
        String redirectUrl = searchController.redirectToActualSearchPage().getUrl();
        System.assert(redirectUrl.contains('QS_CoveoSearch'));
    }    

    static testMethod void test_searchResults() {
        QS_SearchResults searchController = new QS_SearchResults();
        String redirectUrl = searchController.redirectToActualSearchPage().getUrl();
        System.assert(redirectUrl.contains('QS_CoveoSearch'));
    } 
    static testMethod void test_qs_search() {
        QS_Search search = new QS_Search();
        List<QS_SearchWrapper> searchResults = search.searchResults;
        search.startIndex = 5;
        search.pageSize = 20;
        search.totalPages = 20;
        search.totalSize = 100;
        search.searchTerms = 'Hello';
        search.doSearch();
        search.previous();
        search.getShowEnd();
        search.next();
        search.getTotalPages();
        search.getPageNumber();
        search.beginning();
        search.end();
        search.getDisablePrevious();
        search.getDisableNext();
    }
    static testMethod void test_qs_searchwrapper() {
        QS_SearchWrapper search = new QS_SearchWrapper();
        search.title = 'title';
        search.description = 'description';
        search.url = 'url';
        search.dateAdded = 'dateAdded';
        search.dateAddedDT = datetime.now();
        search.isVisibleInPkb  = true;
        search.articleid = 'articleid';
        search.views  = 20;
        search.rating = 5;
    }
}