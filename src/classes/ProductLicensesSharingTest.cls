/********
* NAME :ProductLicensesSharingTest
* Description: Tests for ProductLicensesSharing
* 
*
*Change Log:
    IRN      2016-01-11   CR42998 - Partner share project Created test class

******/
@isTest
private class ProductLicensesSharingTest{

    private static Account_License__c accountItemLicense;

    private static NS_Support_Contract__c createMockNSSupportContract(List<Account> acc){
        NS_Support_Contract__c ns = new NS_Support_Contract__c(Name='test', End_User__c = acc[0].Id, Reseller__c = acc[1].Id, Responsible_Partner__c=acc[2].Id);
        insert ns;
        
        accountItemLicense = new Account_License__c( Account__c = acc[1].Id);
        insert accountItemLicense;

        NS_Support_Contract_Item__c item = new NS_Support_Contract_Item__c(NS_Support_Contract__c =ns.Id, Contract_Item_Account_License__c = accountItemLicense.Id);
        insert item;

        return ns;
    }

    private static testMethod void TestPopulateRelatedPartners(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        List<NS_Support_Contract__c> contracts = new List<NS_Support_Contract__c>();
        List<Account> acc = new List<Account>();
        for(Integer i = 0; i<3; i++){
            Account pAccount = new Account(Name = 'partneracc' + i);
            acc.add(pAccount);  
        }
        Insert acc;
        NS_Support_Contract__c c = createMockNSSupportContract(acc);
        contracts.add(c);
        ProductLicensesSharing ps = new ProductLicensesSharing();
        ps.populateRelatedPartners(contracts);
        List<Account_License__c> licenses = [Select id, Support_Provided_By__c, Selling_Partner__c from Account_License__c where Id = :accountItemLicense.Id];
        System.assertEquals(licenses[0].Selling_Partner__c, acc[1].Id);
        test.stopTest();
    }

    private static testMethod void TestDeleteNSSupportContract(){
        QTTestUtils.GlobalSetUp();
        Test.startTest();
        List<NS_Support_Contract__c> contracts = new List<NS_Support_Contract__c>();
        List<Account> acc = new List<Account>();
        for(Integer i = 0; i<3; i++){
            Account pAccount = new Account(Name = 'partneracc' + i);
            acc.add(pAccount);  
        }
        Insert acc;
        NS_Support_Contract__c c = createMockNSSupportContract(acc);
        contracts.add(c);
        ProductLicensesSharing ps = new ProductLicensesSharing();
        ps.populateRelatedPartners(contracts);
        List<Account_License__c> licenses = [Select id, Support_Provided_By__c, Selling_Partner__c from Account_License__c where Id = :accountItemLicense.Id];
        System.assertEquals(licenses[0].Selling_Partner__c, acc[1].Id);
        delete c;
        List<Account_License__c> licensesAfterDelete = [Select id, Support_Provided_By__c, Selling_Partner__c from Account_License__c where Id = :accountItemLicense.Id];
        //System.assertEquals('', ''+licensesAfterDelete[0].Selling_Partner__c);
        test.stopTest();
    }


}