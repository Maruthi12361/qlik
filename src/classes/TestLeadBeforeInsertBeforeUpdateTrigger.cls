/**
 Change Log:

 201302113  TJG Created for Data Dispatch project phase I. This is the unit test class for the
            trigger LeadBeforeInsertBeforeUpdateTrigger

 Populate segmentation fields on the Lead – trigger on the Lead

    Once the Probable Account (Probable_Account__c) on the Lead is selected, trigger on the Lead will populate key segmentation fields:
        No of Employee, text type (No._of _Employees__c, custom field not the standard one named the same)
        SIC Code Name, lookup type (SIC_Code_Name__c)
        Yearly Revenue, text type (Annual_Turnover__c)
        Named Account, checkbox (Named_Account__c from the Account to Named_Account_Lead__c on the Lead)
        Segment, picklist type (Segment_New__c from the Account to Segment__c on the Lead)
        Company, text type ( Name from the Account to Company on the Lead)
    Trigger update will overwrite any existing data in these fields
    These fields will be automatically emptied if the user deletes the probable account
    (for use case when Probable Account had a value but it has been deleted fields should be set to null)

    Once Lead validation is done and filed Data_Validated__c  (checkbox type) is set to True
    field Validated_By__c should be populated with the user who has validated the lead.
    Data_Validated__c field gets updated (checked) by the workflow rule

**/
@isTest
private class TestLeadBeforeInsertBeforeUpdateTrigger {
    static final String DEBUGPREFIX = '-TestDDDDDDD-';
    static final String WA_SOURCE = 'Qonnect Marketing Services';
    static final String LEADSOURCE = 'PART - Partner';
    static final String RT_PARTEROPPREG = '012D0000000JsWAIA0';
    static testMethod void testLeadBeforeInsertBeforeUpdateTrigger() {

        SIC_Code__c s1 = new SIC_Code__c();
        s1.Name = '5171:Petroleum Bulk Stations and Terminals';
        s1.QlikTech_Sector__c = 'Retail & Services';
        s1.QlikTech_Sub_Industry__c = 'Petroleum & Petroleum Related Products';
        s1.Industry__c = 'Wholesale';
        s1.Industry_Description__c = 'Petroleum Bulk Stations and Terminals';
        s1.SIC_Code__c = '5171';

        SIC_Code__c s2 = new SIC_Code__c();
        s2.Name = '8062:General Medical and Surgical Hospitals';
        s2.QlikTech_Sector__c = 'Healthcare';
        s2.QlikTech_Sub_Industry__c = 'Hospitals';
        s2.Industry__c = 'Healthcare';
        s2.Industry_Description__c = 'General Medical and Surgical Hospitals';
        s2.SIC_Code__c = '8062';

        List<SIC_Code__c> sic_codes = new List<SIC_Code__c>();
        sic_codes.add(s1);
        sic_codes.add(s2);
        insert sic_codes;

        Account acc1 = new Account( Name = 'Prob Account 1',
                                        //Segment_New__c = '01JD000000F6tZa', //'00ND0000004nSoQ', // 'Enterprise',
                                        Segment_New__c = 'Enterprise - Target',
                                        SIC_Code_Name__c = s1.Id, //'a0O20000000FMuaEAG',
                                        No_of_Employees__c = 50,
                                        Named_Account__c = true,
                                        Annual_Turnover__c = 5000000.0);
        Account acc2 = new Account( Name = 'Prob Account 2',
                                        //Segment_New__c = '01JD000000F6tZc', // 'Mid-Market',
                                        Segment_New__c  = 'Commercial - Mid-Market',
                                        SIC_Code_Name__c = s2.Id, //'a0O20000000FMyIEAW',
                                        No_of_Employees__c = 50,
                                        Named_Account__c = true,
                                        Annual_Turnover__c = 5000000.0);
        Insert(acc1);
        Semaphores.TriggerHasRun(1);    //clear the semaphore flag as testing
        Insert(acc2);
        System.Debug(DEBUGPREFIX + 'acc1.Id = ' + acc1.Id);
        Lead lead0 = new Lead(
                            Company = 'Great Good Guys AND Girls',
                            LastName = 'LastTest',
                            FirstName = 'FirstTest',
                            Country='Sweden',
                            Email='asd@asd.com',
                            Data_Validated__c = true,
                            //Probable_Account__c = acc1.Id, // '001D000000xmjVvIAI',
                            LeadSource = LEADSOURCE,
                            RecordTypeId = RT_PARTEROPPREG
                            );

        insert lead0;

        lead0 = [select Id, Segment__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account_Lead__c, Company, Annual_Turnover__c from Lead where Id = :lead0.Id];

        test.startTest();

        System.assertEquals(null, lead0.Segment__c);

        lead0.Probable_Account__c = acc2.Id;
        lead0.Data_Validated__c = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate2 = false;
        Update lead0;

        lead0 = [select Id, Segment__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account_Lead__c, Company, Annual_Turnover__c, Validated_By__c from Lead where Id = :lead0.Id];
        //System.assertEquals('01JD000000F6tZc', lead0.Segment__c);
        System.assertEquals('Commercial - Mid-Market', lead0.Segment__c);

        System.assertEquals(null, lead0.Validated_By__c);

        lead0.Probable_Account__c = null;
        lead0.Data_Validated__c = true;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate2 = false;
        Update lead0;
        lead0 = [select Id, Segment__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account_Lead__c, Company, Annual_Turnover__c from Lead where Id = :lead0.Id];
        System.assertEquals(lead0.Segment__c, null);

        lead0.Probable_Account__c = acc1.Id;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate2 = false;
        Update lead0;
        lead0 = [select Id, Segment__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account_Lead__c, Company, Annual_Turnover__c from Lead where Id = :lead0.Id];
        //System.assertEquals('01JD000000F6tZa', lead0.Segment__c);
        System.assertEquals('Enterprise - Target', lead0.Segment__c);

        Semaphores.TriggerHasRun(1);    //clear the semaphore flag as testing
        Semaphores.CustomAccountTriggerBeforeUpdate = false;
        Semaphores.CustomAccountTriggerAfterUpdate = false;
        acc1.Named_Account__c = false;
        acc1.No_of_Employees__c = 100;
        Semaphores.LeadTriggerHandlerBeforeUpdate = false;
        Semaphores.LeadTriggerHandlerAfterUpdate = false;
        Semaphores.LeadTriggerHandlerBeforeUpdate2 = false;
        Update acc1;
        lead0 = [select Id, Segment__c, SIC_Code_Name__c, No_of_Employees__c, Named_Account_Lead__c, Company, Annual_Turnover__c from Lead where Id = :lead0.Id];
        System.assertEquals(false, lead0.Named_Account_Lead__c);
        System.assertEquals(100, lead0.No_of_Employees__c);

        test.stopTest();
    }
}