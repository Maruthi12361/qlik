/****************************************************************
*
*  PopulateSlaProcessNameTest 
*
*  30.03.2017  Navneet :   Created
*****************************************************************/
@isTest
public class PopulateSlaProcessNameTest {
    static final String AccRecordTypeId_EndUserAccount = [Select id from recordtype where developername ='End_User_Account' limit 1].Id;
    public static testmethod void testProceess() {
        QuoteTestHelper.createCustomSettings();
    User testUser = [Select id From User where id =: UserInfo.getUserId()];
   
    Account testAccount = QTTestUtils.createMockAccount('TestCompany', testUser, true);
          testAccount.RecordtypeId = AccRecordTypeId_EndUserAccount;
      testAccount.BillingCountry = 'France';
          update testAccount;
    Subsidiary__c testSubs = [select id From Subsidiary__c limit 1];
    QlikTech_Company__c testQtCompany = [Select id, QlikTech_Company_Name__c From QlikTech_Company__c limit 1];
    List<QlikTech_Company__c> listOfcreate= new List<QlikTech_Company__c>{createQlikTech('France', 'FR', testSubs.id), createQlikTech('Afghanistan', 'AF', testSubs.id), createQlikTech('Japan', 'JP', testSubs.id), createQlikTech('Albania', 'AL', testSubs.id)};
        insert listOfcreate;
        Contact testContact = QuoteTestHelper.createContact(testAccount.id);
            insert testContact;
            
            
            
            List<Entitlement> createentitlement = new List <Entitlement> {};
            Id standardEntitlementProcessId = [select Id from SlaProcess limit 1].Id;      
            createentitlement.add(new Entitlement (
            Name = testAccount.name,
            AccountId = testAccount.Id,
            SlaProcessId = standardEntitlementProcessId ,
            StartDate = system.Today(),
            Type = 'Phone Support'
            
            ));
            Insert createentitlement;

        List<String> exception_List = new List<String>();
        exception_List.add('Error:');
       
        Test.startTest();        
        PopulateSlaProcessName obj = new PopulateSlaProcessName();         
        DataBase.executeBatch(obj);
        obj.sendErrorMails(exception_List);  
        
        Test.stopTest();

       
    }

  private static QlikTech_Company__c createQlikTech(String countryName, String countryAbbr, Id subsId) {
        QlikTech_Company__c qlikTechIns = new QlikTech_Company__c(
                            Country_Code_Two_Letter__c = countryAbbr, 
                            Country_Name__c = countryName, 
                            Subsidiary__c = subsId);
    return qlikTechIns;
    }
}