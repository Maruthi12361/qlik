/* 
Purpose: Partner Expertise Main Progress bar test
Created Date: 6 Jan 2016
Initial development MTM 6 Jan 2016
Change log:
 **/
@isTest
public class EP_ApplicationProgressBar_Test {
	
    Static Account account { get; set; }
    Static Partner_Expertise_Area__c ExpertiseArea {get; set;}
    Static Partner_Expertise_Application_Lookup__c ExpertLookup {get; set;} 
    Static User testUser {get; set;}
    	
	static testmethod void test_ApplicationProgressBarController() {
		Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
            EP_ApplicationProgressBarController controller = new EP_ApplicationProgressBarController();
            System.assertEquals(controller.PartnerExpertiseArea.Id, ExpertiseArea.Id);
        }
        Test.stopTest();
	}
    
    static testmethod void test_ApplicationProgressBarLookupObject() {
		Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
            EP_ApplicationProgressBarController controller = new EP_ApplicationProgressBarController();
            System.assertEquals(controller.Partnerlookupobjs.Size(), 1);
            System.assertEquals(controller.Partnerlookupobjs[0].Id, ExpertLookup.Id);        
        }
        Test.stopTest();
	}
	
    static testmethod void test_getStyleClassForSolProf1() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
            EP_ApplicationProgressBarController controller = new EP_ApplicationProgressBarController();
            String color = controller.getStyleClassForSolProf1();
            System.assertEquals('inprogress', color);
        }
        Test.stopTest();
    }
    
    static testmethod void test_getStyleClassForSolProf2() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
            EP_ApplicationProgressBarController controller = new EP_ApplicationProgressBarController();
            sol2.Status__c = 'Ready for Review';
            update sol2;
            String color = controller.getStyleClassForSolProf2();
            System.assertEquals('readyforreview', color);    
        }
        Test.stopTest();
    }
    
    static testmethod void test_getStyleClassForSolProf3() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
            EP_ApplicationProgressBarController controller = new EP_ApplicationProgressBarController();
            sol3.Status__c = 'Under Review';
            update sol3;
            String color = controller.getStyleClassForSolProf3();
            System.assertEquals('underreview', color);
        }
        Test.stopTest();
    }
    static testmethod void test_getStyleClassForSolProf4() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
            EP_ApplicationProgressBarController controller = new EP_ApplicationProgressBarController();
            sol4.Status__c = 'Approved';
            update sol4;
            String color = controller.getStyleClassForSolProf4();
            System.assertEquals('completed', color);
        }
        Test.stopTest();        
    }
    
    static testmethod void test_getStyleClassForSolProf4Reject() 
    {
        Setup();
        Test.startTest();
        System.runAs(testUser)
        {
            System.currentPageReference().getParameters().put('expertId', ExpertiseArea.Id);
            EP_ApplicationProgressBarController controller = new EP_ApplicationProgressBarController();
            sol4.Status__c = 'Rejected';
            update sol4;
            String color = controller.getStyleClassForSolProf4();
            System.assertEquals('rejected', color);
        }
        Test.stopTest();
    }
	
    public Static void Setup()
    {    
        Contact testContact;
        
        QlikTech_Company__c qtc = TestDataFactory.createQlikTechCompany('SWE', 'QlikTech Nordic AB', 'Sweden', 'SEK');
        
        insert qtc;
        
        //Create Account
        account = TestDataFactory.createAccount('Test AccountName', qtc);
        insert account;
         
		//Create Contacts
		testContact = TestDataFactory.createContact('test_FName', 'test_LName', 'testSandbox@qlikTech.com', '+44-7878787878', account.Id);
        insert testContact;
        
        // Retrieve Customer Profile and get the profile id
        Profile profileRec = [select id from profile where Name = 'PRM - Sales Dependent Territory + QlikBuy' limit 1]; 
        String profileId = profileRec != null && profileRec.Id != null ? profileRec.Id : null;

		// Create Community Users		
		testUser = TestDataFactory.createUser(profileId, 'testSandbox@qlikTech.com', 'tSbox', String.valueOf(testContact.Id));
        insert testUser;
        
        //Create ExpertiseArea
        ExpertiseArea = EP_TestDataSetup.CreatExpertiseArea(account.Id, 'Communcation', testContact);        
        //Create ExpertiseArea lookup object
        ExpertLookup = EP_TestDataSetup.CreatExpertiseLookup(account.Id, ExpertiseArea.Id);        
        
        sol1 = EP_TestDataSetup.CreateSolutionProfile('Solution1', account.Id, ExpertiseArea.Id);
        sol2 = EP_TestDataSetup.CreateSolutionProfile('Solution2', account.Id, ExpertiseArea.Id);
        sol3 = EP_TestDataSetup.CreateSolutionProfile('Solution3', account.Id, ExpertiseArea.Id);
        sol4 = EP_TestDataSetup.CreateSolutionProfile('Solution4', account.Id, ExpertiseArea.Id);
        sol1.OwnerId = testUser.Id;
        sol2.OwnerId = testUser.Id;
        sol3.OwnerId = testUser.Id;
        sol4.OwnerId = testUser.Id;
        List<Solution_Profiles__c> solList = new List<Solution_Profiles__c> {sol1,sol2, sol3, sol4};
        update solList;
        //Assign Solution_Profiles to lookup object        
        ExpertLookup.Solution_Profile_1_Name__c = sol1.Id;
        ExpertLookup.Solution_Profile_2_Name__c = sol2.Id;
        ExpertLookup.Solution_Profile_3_Name__c = sol3.Id;
        ExpertLookup.Solution_Profile_4_Name__c = sol4.Id;
		update ExpertLookup;        
    }
    Static Solution_Profiles__c sol1 {get; set;}
    Static Solution_Profiles__c sol2 {get; set;}
    Static Solution_Profiles__c sol3 {get; set;}    
    Static Solution_Profiles__c sol4 {get; set;}
    
}