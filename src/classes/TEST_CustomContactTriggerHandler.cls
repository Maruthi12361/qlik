@isTest
public class TEST_CustomContactTriggerHandler { 
//forTestPurpose
	
	public static testmethod void processTest() {
		Contract testContract = formTestData();
		testContract.Status = 'Activated';
		update testContract;
	}


	private static Contract formTestData() {
		Account testAcc = QTTestUtils.createSimpleAccount();
		Sales_Channel__c testContractAssetLink = QTTestUtils.createSimpleAssetLink(testAcc.id);
		Contract testContract = QTTestUtils.createSimpleContract(testAcc.id, testContractAssetLink.id);
		return testContract;
	}
}