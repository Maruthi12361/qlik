/**********************************************************
* Class: MilestoneTest
*
* 2019-04-24 extbad IT-1742 Update pse__Assignments after change 'Closed for Timecard Entry' field
*************************************************************/
@IsTest
private class MilestoneTest {

    @TestSetup
    static void setupData() {
        ApexSharingRules.TestingParnershare = true;
    }

    static testMethod void updateAssignmentsTest() {
        Account acc = QTTestUtils.createMockAccount('test acc 1', new User(Id = UserInfo.getUserId()));
        String contactId = createContact(acc.Id);
        String projectId = createProject(acc.Id, contactId);
        pse__Permission_Control__c pc = new pse__Permission_Control__c(pse__User__c = UserInfo.getUserId(),
                pse__Resource__c = contactId, pse__Cascading_Permission__c = true, pse__Staffing__c = true);
        insert pc;
        pse__Permission_Control__c pc2 = new pse__Permission_Control__c(pse__User__c = UserInfo.getUserId(),
                pse__Project__c = projectId, pse__Cascading_Permission__c = true, pse__Staffing__c = true);
        insert pc2;

        pse__Milestone__c milestone = createMilestone(projectId);

        List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();
        for (Integer i = 0; i < 3; i++) {
            pse__Assignment__c assignment = new pse__Assignment__c(Name = 'Test Assignment ' + i,
                    pse__Project__c = projectId, pse__Status__c = 'Open', pse__Bill_Rate__c = 0.0,
                    pse__Cost_Rate_Amount__c = 10.0,
                    pse__Resource__c = contactId, CurrencyIsoCode = 'USD', pse__Milestone__c = milestone.Id);
            assignments.add(assignment);
        }
        insert assignments;

        assignments = [
                SELECT Id, pse__Closed_for_Time_Entry__c, pse__Status__c
                FROM pse__Assignment__c
                WHERE pse__Milestone__c = :milestone.Id
        ];

        System.assertEquals(3, assignments.size());
        for (pse__Assignment__c assignment : assignments) {
            System.assert(!assignment.pse__Closed_for_Time_Entry__c);
            System.assertEquals('Open', assignment.pse__Status__c);
        }

        Test.startTest();
        milestone.pse__Closed_for_Time_Entry__c = true;
        update milestone;
        Test.stopTest();

        assignments = [
                SELECT Id, pse__Closed_for_Time_Entry__c, pse__Status__c
                FROM pse__Assignment__c
                WHERE pse__Milestone__c = :milestone.Id
        ];

        System.assertEquals(3, assignments.size());
        for (pse__Assignment__c assignment : assignments) {
            System.assert(assignment.pse__Closed_for_Time_Entry__c);
            System.assertEquals('Closed', assignment.pse__Status__c);
        }

        milestone.pse__Closed_for_Time_Entry__c = false;
        update milestone;

        assignments = [
                SELECT Id, pse__Closed_for_Time_Entry__c, pse__Status__c
                FROM pse__Assignment__c
                WHERE pse__Milestone__c = :milestone.Id
        ];

        System.assertEquals(3, assignments.size());
        for (pse__Assignment__c assignment : assignments) {
            System.assert(!assignment.pse__Closed_for_Time_Entry__c);
            System.assertEquals('Scheduled', assignment.pse__Status__c);
        }
    }

    static testMethod void updateTimecardsTest() {
        Account acc = QTTestUtils.createMockAccount('test acc 1', new User(Id = UserInfo.getUserId()));
        String contactId = createContact(acc.Id);
        String projectId = createProject(acc.Id, contactId);

        pse__Permission_Control__c pc = new pse__Permission_Control__c(pse__User__c = UserInfo.getUserId(),
                pse__Resource__c = contactId, pse__Cascading_Permission__c = true, pse__Staffing__c = true,
                pse__Timecard_Entry__c = true, pse__Billing__c = true, pse__Invoicing__c = true,
                pse__Expense_Entry__c = true, pse__Expense_Ops_Edit__c = true, pse__Forecast_Edit__c = true,
                pse__Forecast_View__c = true, pse__Resource_Request_Entry__c = true,
                pse__Skills_And_Certifications_Entry__c = true,
                pse__Skills_And_Certifications_View__c = true, pse__Timecard_Ops_Edit__c = true);
        insert pc;
        pse__Permission_Control__c pc2 = new pse__Permission_Control__c(pse__User__c = UserInfo.getUserId(),
                pse__Project__c = projectId, pse__Cascading_Permission__c = true, pse__Staffing__c = true,
                pse__Timecard_Entry__c = true, pse__Billing__c = true, pse__Invoicing__c = true,
                pse__Expense_Entry__c = true, pse__Expense_Ops_Edit__c = true, pse__Forecast_Edit__c = true,
                pse__Forecast_View__c = true, pse__Resource_Request_Entry__c = true,
                pse__Skills_And_Certifications_Entry__c = true,
                pse__Skills_And_Certifications_View__c = true, pse__Timecard_Ops_Edit__c = true);
        insert pc2;

        pse__Milestone__c milestone = createMilestone(projectId);

        List<pse__Assignment__c> assignments = new List<pse__Assignment__c>();
        for (Integer i = 0; i < 3; i++) {
            pse__Assignment__c assignment = new pse__Assignment__c(
                    Name = 'Test Assignment ' + i,
                    pse__Project__c = projectId,
                    pse__Status__c = 'Open',
                    pse__Bill_Rate__c = 0.0,
                    pse__Resource__c = contactId,
                    CurrencyIsoCode = 'USD',
                    pse__Milestone__c = milestone.Id,
                    pse__Cost_Rate_Amount__c = 10.0);
            assignments.add(assignment);
        }
        insert assignments;

        List<pse__Timecard_Header__c> timeCards = new List<pse__Timecard_Header__c>();
        for (Integer i = 0; i < 3; i++) {
            pse__Timecard_Header__c timeCard = new pse__Timecard_Header__c(
                    pse__Project__c = projectId, pse__Status__c = 'Submitted', pse__Assignment__c = assignments.get(i).Id,
                    pse__Resource__c = contactId, CurrencyIsoCode = 'USD', pse__Milestone__c = milestone.Id,
                    pse__Start_Date__c = system.now().addDays(-1).date(), pse__End_Date__c = system.now().addDays(5).date(),
                    pse__Billable__c = false, pse__Invoiced__c = false, pse__Cost_Rate_Amount__c = 10.0,
                    pse__Submitted__c = true);
            timeCards.add(timeCard);
        }
        insert timeCards;

        timeCards = [
                SELECT Id, pse__Billed__c, pse__Invoiced__c
                FROM pse__Timecard_Header__c
                WHERE pse__Milestone__c = :milestone.Id
        ];

        System.assertEquals(3, timeCards.size());
        for (pse__Timecard_Header__c timeCard : timeCards) {
            System.assert(!timeCard.pse__Billed__c);
            System.assert(!timeCard.pse__Invoiced__c);
        }

        Test.startTest();
        milestone.pse__Status__c = 'Approved';
        milestone.pse__Approved__c = true;
        update milestone;
        Test.stopTest();

        // not able to update Milestone or Timecard with Billed = true
    }

    private static pse__Milestone__c createMilestone(String projectId) {
        pse__Milestone__c milestone = new pse__Milestone__c();
        milestone.Name = 'test milestone 1';
        milestone.pse__Project__c = projectId;
        milestone.pse__Target_Date__c = system.now().addDays(3).date();
        milestone.pse__Closed_for_Time_Entry__c = false;
        milestone.CurrencyIsoCode = 'USD';
        milestone.pse__Billed__c = false;
        milestone.pse__Invoiced__c = false;
        milestone.pse__Actual_Date__c = Date.today();
        milestone.pse__Target_Date__c = Date.today().addDays(5);
        insert milestone;
        return milestone;
    }

    private static String createContact(String accountId) {
        Contact contact = new Contact();
        contact.accountId = accountId;
        contact.pse__External_Resource__c = true;
        contact.pse__Is_Resource__c = true;
        contact.pse__Is_Resource_Active__c = true;
        contact.pse__Salesforce_User__c = UserInfo.getUserId();
        contact.lastname = 'Project manager 1';
        contact.firstname = 'Test';
        contact.CurrencyIsoCode = 'USD';
        insert contact;
        return contact.Id;
    }

    private static String createProject(String accountId, String contactId) {
        pse__Proj__c project = new pse__Proj__c(
                Name = 'Test Project 1',
                pse__Is_Active__c = true,
                pse__Stage__c = 'Open',
                pse__Account__c = accountId,
                pse__Start_Date__c = Date.today().addDays(-10),
                pse__End_Date__c = Date.today().addDays(30),
                pse__Project_Manager__c = contactId,
                Key_Engagement_Features__c = 'QlikView',
                Purpose_of_Engagement__c = 'Test',
                Sales_Classification__c = 'Internal',
                Customer_Critial_Success_Factors__c = 'Test',
                CurrencyIsoCode = 'USD',
                Invoicing_Type__c = 'Deferred');
        insert project;

        return project.Id;
    }
}