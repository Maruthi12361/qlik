/* 
Purpose: Partner Expertise controller
Created Date: 13 Nov 2015
Initial delvelpment MTM 13 Nov 2015
Change log:
 **/
public with sharing class EP_PartnerExpertiseController{
    public Partner_Expertise_Area__c PartnerExpertiseArea{get; private set;}
    public EP_PartnerExpertiseController()
    {        
        AccountId = ApexPages.currentPage().getParameters().get('varAccountID');        
        System.debug('Account Id = ' + AccountId);
        InitProfile();
    }
    public Account PartnerAccount 
    { 
        get {
            return [SELECT Id, Name,Partner_Type__c,Qonnect_Fee__c,Partner_Status__c,Qonnect_Fee_Renewal_Date__c  FROM Account WHERE Id =:AccountId];
        } 
        set; 
    }
    private string accId;
    public String AccountId
    {       
        get{ return accId;}
        set{        
            if(value != null && value != '')
            {
                accId = value;
            }            
        }
    }
    private boolean bApproved;
    public Boolean BIsApproved {
        get
        {return bApproved;} 
        set
        {bApproved=value;}
    }
    private String expertiseAreaStatus;
    public String ExpertiseAreaAppStatus
    {
        get{return expertiseAreaStatus;}
        set{expertiseAreaStatus=value;}
    }
    //Used in create Expertise Area screen to show only sectors which are NOT 'Approved' and 'Under Review' status 
    public List <SelectOption> ExpertiseSectorOptions
    {
        get
        {
            list<SelectOption> options = new list<SelectOption>();
              //Schema.sObjectType objType = Partner_Expertise_Area__c.getSObjectType(); 
              Schema.DescribeFieldResult objDescribe = Partner_Expertise_Area__c.Expertise_Area__c.getDescribe();       
              list<Schema.PicklistEntry> values = objDescribe.getPickListValues();
              for (Schema.PicklistEntry a : values)
              { 
                  if(!ExpertiseAreaMap.containsKey(a.getValue()))
                    options.add(new SelectOption(a.getLabel(), a.getValue())); 
              }
              return options;          
        }
    }
    public String ExpertiseAreaSector {get; set;}       
    public Id DedicatedContact {
        get{
            If ( DedicatedContact == null)
            {
                User u = [select ContactId from User where id = :UserInfo.getUserId()];
                return u.ContactId;
            }
            else return DedicatedContact;
        } 
        set;
    } 

    public Boolean ShowUnderReview {get; set;}
    //Create a picklist of Contacts     
    public List<selectOption> ExpertiseContacts {
        get{
            List<selectOption> options = new List<selectOption>(); 
            for (Contact users : [SELECT Id, Name FROM Contact WHERE Account.Id =:AccountId order by Name asc]) 
            { 
                options.add(new selectOption(users.Id, users.Name));
            }
            return options;
        }
    }
    //Used in Partner Expertise Area List area grid in EP_PartnerExpertiseListComponent. Will show 'Approved' and 'Under Review' Apps only
    public List<Partner_Expertise_Area__c> ListExpertiseArea{
        get{
            List<Partner_Expertise_Area__c> listofExp = [Select
                                                         Id, 
                                                         Expertise_Area__c,
                                                         Apply_Date__c,
                                                         Dedicated_Expertise_Contact__c,
                                                         Dedicated_Expertise_Contact__r.Name,
                                                         Expertise_Start_Date__c,
                                                         Expertise_Application_Eligibility__c
                                                         //Partner_Account_Name__r.Partner_Type__c,
                                                         //Qlik_Market_Application__c
                                                         from Partner_Expertise_Area__c 
                                                         where Partner_Account_Name__c=: AccountId
                                                         AND Expertise_Application_Eligibility__c IN ('Under Review','Accepted') ];
                                                         
            return listofExp;
           }     
    }
    
    public List<Partner_Expertise_Area__c> SelectedTable
    {
        get{
            if(ShowUnderReview)
                return ListExpertiseArea;
            else return ListApprovedPE;
        }
    }
       
    public List<Partner_Expertise_Area__c> ListApprovedPE{
        get{
            List<Partner_Expertise_Area__c> listofExp = [Select
            Id, 
            Expertise_Area__c,
            Apply_Date__c,
            Dedicated_Expertise_Contact__c,
            Dedicated_Expertise_Contact__r.Name,
            Expertise_Start_Date__c,
            Expertise_Application_Eligibility__c
            //Partner_Account_Name__r.Partner_Type__c,
            //Qlik_Market_Application__c
            from Partner_Expertise_Area__c where Partner_Account_Name__c=: AccountId                  
            and  Expertise_Application_Eligibility__c = 'Accepted'];
            return listofExp;
        }     
    }
    //Used in create Expertise Area screen to show only sectors which are NOT 'Approved' and 'Under Review' status
    public List<Partner_Expertise_Area__c> ListExpertiseSector{
        get{
            List<Partner_Expertise_Area__c> listofExpSector = [Select
            Id, 
            Expertise_Area__c
            from Partner_Expertise_Area__c where Partner_Account_Name__c=: AccountId                  
            and  Expertise_Application_Eligibility__c NOT IN ('Under Review','Accepted') ];
            return listofExpSector;
        }     
    }
    
    public PageReference Cancel()
    {
        return new PageReference('/apex/QlikExpertiseProgram');
    }
    public PageReference Save()
    {
        if(ExpertiseAreaSector != null && DedicatedContact != null)
        {
            PartnerExpertiseArea = new Partner_Expertise_Area__c(
            Partner_Account_Name__c = AccountId,
            Expertise_Area__c = ExpertiseAreaSector,
            Dedicated_Expertise_Contact__c = DedicatedContact,
            Qlik_Market_App_Status__c = 'Not Assigned',
            Expertise_Designation_Status__c = 'In Process');
            
            if(calculateEligibility())
            {
                PartnerExpertiseArea.Expertise_Application_Eligibility__c='Accepted';
                ExpertiseAreaAppStatus='Accepted';
            }
            else{
                PartnerExpertiseArea.Expertise_Application_Eligibility__c='Under Review';
                ExpertiseAreaAppStatus='Under Review';
            }
            try
            {
                System.Debug('Upserting PartnerExpertiseArea= '+ PartnerExpertiseArea);  
                upsert PartnerExpertiseArea;                
            }
            catch(Exception ex)
            {
                System.Debug('Exception on updating Expertise area object');
                ApexPages.Message errMessage = new ApexPages.Message(ApexPages.Severity.Warning, 'Exception on updating Expertise area Application' + ex.getMessage());
                ApexPages.addMessage(errMessage);                
            }
        }
        try
        {   
            InitLookupObject();         
            LookupObject.Expertise_Area__c = PartnerExpertiseArea.Id;
            upsert(LookupObject);
        }
        
        Catch(DMLException ex)
        {
            System.debug('Issue updating partner expertise area lookup object' + ex.getMessage());
            ApexPages.addMessages(ex);  
        }
        //String msg = String.format(System.Label.EP_ExpertiseAreaApproved_MsgEP, new String[] {PartnerExpertiseArea.Expertise_Area__c});
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,msg));
        return null;
        /*
        String sURL;
        sURL = String.Format('/apex/EP_PartnerExpertiseAreaPage?varAccountId={0}', new String[]{''+AccountId});     
        
        if (sURL != null)
        {
             PageReference pageRef = new ApexPages.Pagereference(sURL);
             pageRef.setRedirect(true);
             return pageRef;
        }
        else
        { 
            return null;
        } 
        */  
    }
    
    public void InitProfile()
    {
        String ProfileId = UserInfo.getProfileId();
        List<Profile> ProfileList = [Select Name FROM Profile where Id=:ProfileId];
        String ProfileName = ProfileList.size()>0?''+ProfileList.get(0):'';
        //PRM-BASE profile
        isPRMBase =  String.isNotBlank(ProfileName) && ProfileName.contains('PRM - Base')? true: false;  
    }
    public Boolean IsPRMBase {get; set;}
    
    public boolean calculateEligibility()
    {       
        // Reseller (old),Sub-Reseller, Value Added Distributor,Registered Partner
        if ((PartnerAccount.Partner_Type__c == 'Reseller (old)' ||
             PartnerAccount.Partner_Type__c == 'Sub-Reseller' ||
             PartnerAccount.Partner_Type__c == 'Value Added Distributor' ||
             PartnerAccount.Partner_Type__c == 'Registered Partner' ) ||
            PartnerAccount.Qonnect_Fee__c <= 0 ||
            PartnerAccount.Partner_Status__c != 'Contracted' ||
            PartnerAccount.Qonnect_Fee_Renewal_Date__c < Date.today() ||
            isPRMBase)
        {
            return false;
        }
        return true;
    }
   
    private Map<String,String> ExpertiseAreaMap
    {
        get{
            Map<String,String>  expMap = new Map<String,String>();
            for ( Partner_Expertise_Area__c c : ListExpertiseArea) 
            {
            	expMap.put(c.Expertise_Area__c, c.Expertise_Area__c); 
        	}
            return expMap;
        }       
    }
    
    public Partner_Expertise_Application_Lookup__c LookupObject {get; set;}
    private void InitLookupObject()
    {
        List<Partner_Expertise_Application_Lookup__c> lookupList = new List<Partner_Expertise_Application_Lookup__c>();
        if(PartnerExpertiseArea.Id != null)
        {            
            lookupList = [Select Name,
                          Account_Name__c,
                          Expertise_Area__c
                          from Partner_Expertise_Application_Lookup__c where Account_Name__c=: AccountId 
                          and Expertise_Area__c=:PartnerExpertiseArea.Id];
        }
        if(!lookupList.isEmpty())
            LookupObject = lookupList[0];
        else
        {
            LookupObject = new Partner_Expertise_Application_Lookup__c();
            LookupObject.Account_Name__c = AccountId;
        }
    }
}