/**     * File Name:AccountBillingCountryCodeHandler
* Description : This handler class is used to update the billing country code on the account record
* @author : Ramakrishna Kini
* Modification Log ===============================================================
Ver     Date         Author         Modification 
1.0 Oct 3rd 2016 RamakrishnaKini     Added new trigger logic.
1.1 MArch 30th 2017 MTM     Modified Logic for country  mismatch.
1.2 July 21st 2017 Shubham Gupta       Modified logic to populate subsidiary if account created on lead conversion.
*/
public class AccountBillingCountryCodeHandler{
    
    static List<Account> lAccs = new List<Account>();
    static Map<String, Id> mQTCompSubsId = new Map<String,Id>();
    static Map<Id, String> mQTSubsIdName = new Map<Id, String>();
    static Set<String> lCountryNames = new Set<String>();
    static Map<Id,Id> billingSubsId = new Map<Id,Id>();//1.2 Added a map to store qlikcompanyID vs SubsdiaryID both comming from Qlikcompany object
    static Set<Id> billingcode = new Set<Id>();//1.2 Added a Set to store Billin_Country_code__c a field on account lookup to Qlikcompany object
    
    /*  @Description :This common method is used to update billing country code fields on account on before insert trigger context.

@parameter inputList: Trigger.new Account list

*/
    public static void onBeforeInsert(List<Account> inputList) {
        for(Account acc: inputList){
            system.debug('Territory Country====> '+ acc.Territory_Country__c);
            system.debug('Billing_Country code=====> '+acc.Billing_Country_Code__c);
            
            //1.2 added below code to populate Subsdiary if Billing_Country_Code__c is not empty and is always comming from lead conversion 
            if(String.isNotBlank(acc.Billing_Country_Code__c)){
                billingcode.add(acc.Billing_Country_Code__c);
                lAccs.add(acc);
            }
            system.debug('billing set====> '+billingcode);
            // code ended 1.2
            
            if(String.isNotBlank(acc.Territory_Country__c) && String.isBlank(acc.Billing_Country_Code__c)){
                lAccs.add(acc);
                //if('United States'.equalsIgnoreCase(acc.BillingCountry))
                //  lCountryNames.add('USA');
                //else
                lCountryNames.add(acc.Territory_Country__c);
            }
        }
        
        //1.2 below will call setSbsdryconverted fucntion if account is created from lead conversion
        if(!billingcode.isEmpty()){
            setSbsdryconverted(lAccs);
        }
        //code ended 1.2
        
        if(!lCountryNames.isEmpty())
            updBillingCountryCode(lAccs);
        lCountryNames.clear();
    }
    
    /*  @Description :This common method is used to update billing country code fields on account on before update trigger context.

@parameter inputList: Trigger.new Account list
@parameter inputMap: Trigger.newMap 
@parameter oInputList: Trigger.old Account list  

*/
    public static void onBeforeUpdate(List<Account> inputList, Map<id, Account> inputMap, Map<id, Account> oInputMap) {
        for(Account acc: inputList){
            System.debug('new acc.Territory_Country__c' + acc.Territory_Country__c + 'old .Territory_Country__c' + oInputMap.get(acc.Id).Territory_Country__c);
            if(String.isNotBlank(acc.Territory_Country__c) && !acc.Territory_Country__c.equalsIgnoreCase(oInputMap.get(acc.Id).Territory_Country__c)){
                System.debug('Adding account');
                lAccs.add(acc);             
                //if('United States'.equalsIgnoreCase(acc.BillingCountry))
                //    lCountryNames.add('USA');
                //else 
                lCountryNames.add(acc.Territory_Country__c);
            }
        }
        if(!lCountryNames.isEmpty())
            updBillingCountryCode(lAccs);
        lCountryNames.clear();
    }
    
    /**************************************************************************************
*
*    Private Methods
***************************************************************************************/
    
    /*  @Description :This method is used to fetch qliktech company details for accounts which have billing country populated
and update the billing country code lookup
@parameter inputList: Accounts whose billing country is populated
*/
    
    private static void updBillingCountryCode(List<Account> lAccounts){
        Map<String, Id> mQTCompNameId = fetchQlikTechComps();
        system.debug('aaaa'+mQTCompNameId);
        if(!mQTCompNameId.isEmpty()){
            for(Account acc: lAccounts){
                system.debug('aaaa1 Territory_Country__c = '+acc.Territory_Country__c);
                /*
if('United States'.equalsIgnoreCase(acc.BillingCountry)){
if(mQTCompNameId.containsKey('USA')){    
acc.Billing_Country_Code__c = mQTCompNameId.get('USA');
system.debug('aaaa2'+acc.Billing_Country_Code__c);
setSubsidiaryDetails(acc, 'USA');
}
}else{
*/
                if(mQTCompNameId.containsKey(acc.Territory_Country__c)){
                    acc.Billing_Country_Code__c = mQTCompNameId.get(acc.Territory_Country__c);
                    setSubsidiaryDetails(acc, acc.Territory_Country__c);
                }
                //}
            }
        }
    }
    
    /*  @Description :This common method is used to set the subsidiary fields on account

@parameter acc: Accounts whose subsidiary fields are to be set
@parameter countryName: Country name on account
*/
    private static void setSubsidiaryDetails(Account acc, String countryName){
        if(mQTCompSubsId.containskey(countryName)){
            acc.Subsidiary__c = mQTCompSubsId.get(countryName);
            if(String.isNotBlank(acc.Subsidiary__c) && mQTSubsIdName.containskey(acc.Subsidiary__c))
                acc.QlikTech_Company__c = mQTSubsIdName.get(acc.Subsidiary__c);
        }       
    }
    
    //1.2 code added to populate subsdiary if account created on lead conversion
    private static void setSbsdryconverted(List<Account> accs){
        system.debug('Inisde setsubconverted====> '+accs);
        for(QlikTech_Company__c fetched : [SELECT id, Name,NS_Country_Name__c, Country_Name__c,Subsidiary__c, Subsidiary__r.name 
                                                 FROM QlikTech_Company__c 
                                                 WHERE id IN :billingcode]){
                                                     billingSubsId.put(fetched.ID, fetched.Subsidiary__c);
                                                 }
        system.debug('company Detail Map========> '+billingSubsId);
        if(!billingSubsId.isEmpty()){
            for(Account ac:accs){
                ac.Subsidiary__c = billingSubsId.get(ac.Billing_Country_Code__c);
                system.debug('Subsdiary populated');
            }
        }
        
    }
    //code ended 1.2
    
    /*  @Description :This common method is used to fetch qliktech company details for accounts which have billing country populated

@return Map containg the qlikTech Company name as key and id as value
*/
    private static Map<String, Id> fetchQlikTechComps(){
        Map<String, Id> mQTCompNameId = new Map<String,Id>();
        for(QlikTech_Company__c comp: [SELECT id, Name,NS_Country_Name__c, Country_Name__c,Subsidiary__c, Subsidiary__r.name 
                                       FROM QlikTech_Company__c 
                                       WHERE NS_Country_Name__c in :lCountryNames]){
                                           if(String.isNotBlank(comp.NS_Country_Name__c) && !mQTCompNameId.containsKey(comp.NS_Country_Name__c)){
                                               mQTCompNameId.put(comp.NS_Country_Name__c, comp.Id);
                                               if(String.isNotBlank(comp.Subsidiary__c)){
                                                   mQTCompSubsId.put(comp.NS_Country_Name__c, comp.Subsidiary__c);
                                                   if(!mQTSubsIdName.containsKey(comp.Subsidiary__c))
                                                       mQTSubsIdName.put(comp.Subsidiary__c, comp.Subsidiary__r.name);
                                               }
                                           }
                                       }
        return mQTCompNameId;
    }
}