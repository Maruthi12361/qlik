/********************************************************
* CLASS: WebActivityUtils
* DESCRIPTION: Supporting class for Web Activity
*
* CHANGELOG:
*   2018-10-15 - BAD - Added Initial logic
*   2019-02-05 - BAD - Added Classes for LeadGeneration
*   2019-02-18 - CRW - Modified Additional Params class to convert Campaign id to 18 digit
*   2019-04-03 - BAD - Adding GA Client ID
*   2019-06-14 - CRW - Added classes for event type lead flow intake
*   2019-08-30 - BAD - BMW-1708 - fixed actvity subject for w2l flow phase2
*********************************************************/
global class WebActivityUtils {

    public WebActivityUtils() {}

    global class WebActivityContent {
        public string ULCUsername = '';
        public string TimeStamp = '';
        public String MessageID;
        public String WebActivityType;
        public String Sender;
        public String Status;
        public String AuditTrail = '';
        public ULCUser OUser;
        public AdditionalParams AdditParams;

        public WebActivityContent() {}

        public WebActivityContent(WebActivityContent WAContent) {
            if (String.isNotBlank(WAContent.TimeStamp)) TimeStamp = WAContent.TimeStamp;
            if (String.isNotBlank(WAContent.ULCUsername)) ULCUsername = WAContent.ULCUsername;
            if (String.isNotBlank(WAContent.MessageID)) MessageID = WAContent.MessageID;
            if (String.isNotBlank(WAContent.WebActivityType)) WebActivityType = WAContent.WebActivityType;
            if (String.isNotBlank(WAContent.Sender)) Sender = WAContent.Sender;
            if (String.isNotBlank(WAContent.AuditTrail)) AuditTrail = WAContent.AuditTrail;
        }
    }

    public class W2LContent {
        public Head head;
        public Body body;
    }

    public class Head {
        public String timestamp;
    }

    public class Body {
        public User user;
        public formdata formdata;
    }

    public class User {
        public List<Ids> ids;
        public String language;
        public String country;
        public String ipAddress;
        public String userAgent;
    }

    public class formdata {
        public String status;
        public String guid;
        public String timestamp;
        public String url;
        public String urlReferrer;
        public Form form;
        public List<Acknowledgements> acknowledgements;
        public Fields fields;
    }

    public class Fields {
        public String campaignID;
        public String userName;
        public String comments;
        public String company;
        public String country;
        public String emailAddress;
        public String firstName;
        public String lastName;
        public String jobTitle;
        public String postalCode;
        public String state;
        public String telephoneNumber;
        public String incentive;
        public String sourceID2;
        public String asset;

    }

    public class Form {
        public String guid;
        public String name;
        public String key;
        public String language;
    }

    public class Content {
        public String referenceType;
        public String contentGUID;
        public Object contentLanguage;
        public String contentVersion;
        public String contentType;
        public String contentURL;
        public String guid;
        public String path;
        public String language;
    }

    public class Ids {
        public String systemName;
        public String systemType;
        public String idType;
        public String id;
    }

    public class Acknowledgements {
        public String typeAck;
        public String name;
        public Boolean isAcceptanceEnabled;
        public Boolean isPreaccepted;
        public Boolean isAccepted;
        public Boolean isRequired;
        public String text;
        public List<Content> contentAck;
        public Integer sortOrder;
        public String key;
        public String guid;
        public String path;
        public String language;
    }

    public class ULCUser {
        public string UserName = '';
        public string Password = '';
        public string FirstName = '';
        public string LastName = '';
        public string Phone = '';
        public string EmailAddress = '';
        public string Account = '';
        public string Address1 = '';
        public string City = '';
        public string State = '';
        public string CountryCode = '';
        public string PostalCode = '';
        public string Country = '';
        public string JobTitle = '';
        public string Department = '';
        public string ULCLevels = '';
        public Boolean EmailOptOut = true;
        public string ContactID = '';
        public string AccountID = '';
        public string LeadID = '';
        public Boolean EmailDownloadTemplate = false;
        public string OwnerID = '';
        public string NavisionStatus = '';
        public Boolean NoAutoCPView = false;
        public string ClaimedQTRel = '';
        public Boolean IsEmployee = false;
        public string ULCDetailsID = '';
        public string LeadSource = '';
        public string LeadSourceDetail = '';
        public string AssetActivityName = '';
        public string PotentialMatches = '';
        public string QCloudID = '';
        public Boolean IsValidationAccount = false;
        public string ZiftpartnerID = '';
        public string Ziftpartnername = '';
        public string Status = '';
        public string Type = '';
        public string Feature = '';
        public string SICCodeID = '';
        public string QliktechCompId = '';
        public string CampaignId = '';
        public string eventType = '';
        public string WebActivityType = '';
        public string Sender = '';
        public string QlikId = '';
        public string ulcqlikId = '';
        public string comments = '';
        public boolean optIn = false;
        public string Product = '';
        public string Subscription = '';
        public string SubscriptionName = '';
        public ULCUser() {
        }


        //ULCUser Constructur for LeadGeneration initiated by qlik.com
        public ULCUser(W2LContent content) {
            if (content.body.formdata.fields.userName != null) {
                if (String.isNotBlank(content.body.formdata.fields.userName)) UserName = content.body.formdata.fields.userName;
            }
            if (String.isNotBlank(content.body.formdata.fields.FirstName)) FirstName = content.body.formdata.fields.FirstName;
            if (String.isNotBlank(content.body.formdata.fields.LastName)) LastName = content.body.formdata.fields.LastName;
            if (String.isNotBlank(content.body.formdata.fields.TelephoneNumber)) Phone = content.body.formdata.fields.TelephoneNumber;
            if (String.isNotBlank(content.body.formdata.fields.EmailAddress)) EmailAddress = content.body.formdata.fields.EmailAddress.toLowerCase();
            if (String.isNotBlank(content.body.formdata.fields.Company)) Account = content.body.formdata.fields.Company;
            if (String.isNotBlank(content.body.formdata.fields.State)) State = content.body.formdata.fields.State;
            if (String.isNotBlank(content.body.formdata.fields.PostalCode)) PostalCode = content.body.formdata.fields.PostalCode;
            if (String.isNotBlank(content.body.formdata.fields.Country)) CountryCode = content.body.formdata.fields.Country;
            if (String.isNotBlank(content.body.formdata.fields.JobTitle)) JobTitle = content.body.formdata.fields.JobTitle;
            if (String.isNotBlank(content.body.formdata.fields.campaignID)) CampaignId = content.body.formdata.fields.campaignID;
            if (String.isNotBlank(content.body.formdata.fields.comments)) comments = content.body.formdata.fields.comments;
            EmailOptOut = false;
            eventType = 'contactUs';
            List<Acknowledgements> acknowledgements = content.body.formdata.acknowledgements;
            if (acknowledgements != null && acknowledgements.size() > 0)
            {
                for(Acknowledgements ack : acknowledgements){
                    if (String.isNotBlank(ack.name) && ack.name.toUpperCase() == 'PRIVACYPOLICY')
                    {
                        if (ack.isAccepted == true){
                            EmailOptOut = false;
                            optIn = true;
                        } else {
                            EmailOptOut = true;
                            optIn = false;
                        }
                    }
                }
            }
            Address1 = '';
            City = '';
            Country = '';
            Department = '';
            ULCLevels = '';
            ClaimedQTRel = '';
            LeadSource = '';
            LeadID = '';
            ULCDetailsID = '';
            LeadSourceDetail = '';
            QCloudID = '';
            AssetActivityName = '';
            ContactID = '';
            AccountID = '';
            OwnerID = '';
            PotentialMatches = '';
            NavisionStatus = '';
            IsValidationAccount = false;
            IsEmployee = false;
            QliktechCompId = '';
            Status = '';
            Type = '';
            Feature = '';
            SICCodeID = '';
            ZiftpartnerID = '';
            Ziftpartnername = '';
        }


        //ULCUser Constructur for LeadGeneration initiated by QlikID
        public ULCUser(ULCUser oUser) {
            if (String.isNotBlank(oUser.UserName)) UserName = oUser.UserName;
            if (String.isNotBlank(oUser.FirstName)) FirstName = oUser.FirstName;
            if (String.isNotBlank(oUser.LastName)) LastName = oUser.LastName;
            if (String.isNotBlank(oUser.Phone)) Phone = oUser.Phone;
            if (String.isNotBlank(oUser.EmailAddress)) EmailAddress = oUser.EmailAddress;
            if (String.isNotBlank(oUser.Account)) Account = oUser.Account;
            if (String.isNotBlank(oUser.Address1)) Address1 = oUser.Address1;
            if (String.isNotBlank(oUser.City)) City = oUser.City;
            if (String.isNotBlank(oUser.State)) State = oUser.State;
            if (String.isNotBlank(oUser.PostalCode)) PostalCode = oUser.PostalCode;
            if (String.isNotBlank(oUser.CountryCode)) CountryCode = oUser.CountryCode;
            if (String.isNotBlank(oUser.Country)) Country = oUser.Country;
            if (String.isNotBlank(oUser.JobTitle)) JobTitle = oUser.JobTitle;
            if (String.isNotBlank(oUser.Department)) Department = oUser.Department;
            if (String.isNotBlank(oUser.ULCLevels)) ULCLevels = oUser.ULCLevels;
            if (String.isNotBlank(oUser.ClaimedQTRel)) ClaimedQTRel = oUser.ClaimedQTRel;
            if (String.isNotBlank(oUser.LeadSource)) LeadSource = oUser.LeadSource;
            if (String.isNotBlank(oUser.LeadID)) LeadID = oUser.LeadID;
            if (String.isNotBlank(oUser.ULCDetailsID)) ULCDetailsID = oUser.ULCDetailsID;
            if (String.isNotBlank(oUser.LeadSourceDetail)) LeadSourceDetail = oUser.LeadSourceDetail;
            if (String.isNotBlank(oUser.QCloudID)) QCloudID = oUser.QCloudID;
            if (String.isNotBlank(oUser.ZiftpartnerID)) ZiftpartnerID = oUser.ZiftpartnerID;
            if (String.isNotBlank(oUser.Ziftpartnername)) Ziftpartnername = oUser.Ziftpartnername;
            if (String.isNotBlank(oUser.AssetActivityName)) AssetActivityName = oUser.AssetActivityName;
            ContactID = '';
            AccountID = '';
            OwnerID = '';
            PotentialMatches = '';
            NavisionStatus = '';
            IsValidationAccount = false;
            IsEmployee = false;
            QliktechCompId = '';
            if (String.isNotBlank(oUser.Status)) Status = oUser.Status;
            if (String.isNotBlank(oUser.Type)) Type = oUser.Type;
            if (String.isNotBlank(oUser.Feature)) Feature = oUser.Feature;
            if (oUser.EmailOptOut != null) EmailOptOut = oUser.EmailOptOut;
            if (oUser.SICCodeID != null) SICCodeID = oUser.SICCodeID;
        }

        //ULC constructor for enterprise lead flow
        public ULCUser(auth0 content){
            if (String.isNotBlank(content.data.email)) UserName = content.data.email;
            if (String.isNotBlank(content.data.firstName)) FirstName = content.data.firstName;
            if (String.isNotBlank(content.data.lastName)) LastName = content.data.lastName;
            if (String.isNotBlank(content.data.telephone)) Phone = content.data.telephone;
            if (String.isNotBlank(content.data.email)) EmailAddress = content.data.email;
            if (String.isNotBlank(content.data.company)) Account = content.data.company;
            if (String.isNotBlank(content.data.jobTitle)) JobTitle = content.data.jobTitle;
            if (String.isNotBlank(content.eventType)) eventType = content.eventType;
            if (content.data.context != null){
                if (String.isNotBlank(content.data.context.campaignID)) CampaignId = content.data.context.campaignID;
                if (String.isNotBlank(content.data.context.webActivityType)) WebActivityType = content.data.context.webActivityType;
                if (content.data.context.privacyPolicyAccepted != null) {
                    if(content.data.context.privacyPolicyAccepted == 'true'){
                        EmailOptOut = false;
                        optIn = true;
                    }else {
                            EmailOptOut = true;
                            optIn = false;
                    }
                }
                if (String.isNotBlank(content.data.context.state)) State = content.data.context.state;
            }
            if (String.isBlank(State) && String.isNotBlank(content.data.state)) State = content.data.state;//State being set for invited user
            if (String.isNotBlank(content.eventType) && content.eventType.contains('user.detail') && content.data.privacyPolicyAccepted != null){
                if(content.data.privacyPolicyAccepted == 'true'){
                    EmailOptOut = false;
                    optIn = true;
                }else {
                    EmailOptOut = true;
                    optIn = false;
                }
            }
            if (content.data.source != null && String.isNotBlank(content.data.source)){
                Sender = content.data.source;
            }
            if (content.data.invitedBySubject != null && String.isNotBlank(content.data.invitedBySubject)){
                ulcqlikId = content.data.invitedBySubject;
            }
            if (content.data.country != null && String.isNotBlank(content.data.country)){
                Country = content.data.country;
            }

            if (content.data.countryCode != null && String.isNotBlank(content.data.countryCode)){
                CountryCode = content.data.countryCode;
            }

            if (String.isNotBlank(content.extensions.subject)){
                if (content.extensions.subject.contains('|')){
                    String parts = content.extensions.subject.substring(content.extensions.subject.indexOf('|') + 1);
                    QlikId = parts;
                }
            }

            Address1 = '';
            City = '';
            Department = '';
            ULCLevels = '';
            ClaimedQTRel = '';
            LeadSource = '';
            LeadID = '';
            ULCDetailsID = '';
            LeadSourceDetail = '';
            QCloudID = '';
            AssetActivityName = '';
            ContactID = '';
            AccountID = '';
            OwnerID = '';
            PotentialMatches = '';
            NavisionStatus = '';
            IsValidationAccount = false;
            IsEmployee = false;
            QliktechCompId = '';
            Status = '';
            Type = '';
            Feature = '';
            SICCodeID = '';
            ZiftpartnerID = '';
            Ziftpartnername = '';
        }

        //ULC constructor for generic lead flow 
        public ULCUser(LeadData content){
            if (String.isNotBlank(content.email)) EmailAddress = content.email;
            if (String.isNotBlank(content.firstName)) FirstName = content.firstName;
            if (String.isNotBlank(content.lastName)) LastName = content.lastName;
            if (String.isNotBlank(content.telephone)) Phone = content.telephone;
            if (String.isNotBlank(content.company)) Account = content.company;
            if (String.isNotBlank(content.jobTitle)) JobTitle = content.jobTitle;
            if (String.isNotBlank(content.campaignID)) CampaignId = content.campaignID;
            if (String.isNotBlank(content.comments)) Comments = content.comments;
            if (content.country != null && String.isNotBlank(content.country)) Country = content.country;
            if (content.countryCode != null && String.isNotBlank(content.countryCode)) CountryCode = content.countryCode;
            if (String.isNotBlank(content.state)) State = content.state;            
            if (content.privacyPolicyAccepted != null) {
                if(content.privacyPolicyAccepted == 'true'){
                    EmailOptOut = false;
                    optIn = true;
                }else {
                    EmailOptOut = true;
                    optIn = false;
                }
            }
            
            Address1 = '';
            City = '';
            Department = '';
            ULCLevels = '';
            ClaimedQTRel = '';
            LeadSource = '';
            LeadID = '';
            ULCDetailsID = '';
            LeadSourceDetail = '';
            QCloudID = '';
            AssetActivityName = '';
            ContactID = '';
            AccountID = '';   
            OwnerID = ''; 
            PotentialMatches = '';
            NavisionStatus = '';
            IsValidationAccount = false;
            IsEmployee = false;
            QliktechCompId = '';
            Status = ''; 
            Type = ''; 
            Feature = ''; 
            SICCodeID = ''; 
            ZiftpartnerID = '';
            Ziftpartnername = '';
        }
    }

    public class RetStruct {
        public Boolean Success;
        public ULCUser OUser;
        public String Message;
        public Lead Lead;
        public Contact Contact;
        public WebActivityContent WAContent;
    }

    public class AdditionalParams {
        public string SupplimentalData;
        public string Asset;
        public string ZiftData;

        public string LeadSourceDetail;
        public string LeadSourceDetailMirror;
        public string Incentive;
        public string SourceId1;
        public string SourceId2;
        public string SourceUrl;
        public string SourceULC;
        public string SourcePartner;
        public string SourceEmployee;
        public string WebActSrc;
        public string LeadSource;
        public string PartnerSource;
        public string TcAccepted;
        public string CampaignId;
        public string Ref;
        public string FreeText;
        public string DBCompname;
        public string WebLeadInterest;
        public string EmployeeRange;
        public string Industry;
        public string SubIndustry;
        public string RegState;
        public string RegCountry;
        public string JobFunction;
        public string Keywords;
        public string Mktotrk;
        public string GAClientID;
        public string taskSubject;

        public AdditionalParams(){
        }

        //AdditionalParams Constructur for LeadGeneration initiated by qlik.com
        public AdditionalParams(W2LContent content) {
            //We can set these fields already from the JSON string we received from Qlik.com.
            //Rest of additional parameters will be set later when we have fetched Campaign and Marketing Asset
            if (String.isNotBlank(content.body.formdata.fields.campaignID)) CampaignId = content.body.formdata.fields.campaignID;
            if (String.isNotBlank(content.body.formdata.fields.sourceID2)) SourceId2 = content.body.formdata.fields.sourceID2;
            if (String.isNotBlank(content.body.formdata.fields.incentive)) Incentive = content.body.formdata.fields.incentive;
            if (String.isNotBlank(content.body.formdata.url)) SourceUrl = content.body.formdata.url;
            if (String.isNotBlank(content.body.formdata.fields.comments)) FreeText = content.body.formdata.fields.comments;

            taskSubject = Incentive + ' - ' + SourceId2;
            //Set Marketo tracking Id
            Mktotrk = '';
            List<Ids> sysIDs = content.body.user.ids;
            if (sysIDs != null && sysIDs.size() > 0)
            {
                for(Ids sysID : sysIDs){
                    if (String.isNotBlank(sysID.systemName) && sysID.systemName.toUpperCase() == 'MARKETO')
                    {
                        Mktotrk = sysID.id;
                    }
                }
            }

            //Set GA Client ID
            GAClientID = '';
            sysIDs = content.body.user.ids;
            if (sysIDs != null && sysIDs.size() > 0)
            {
                for(Ids sysID : sysIDs){
                    if (String.isNotBlank(sysID.systemName) && sysID.systemName.toUpperCase() == 'GOOGLE')
                    {
                        GAClientID = sysID.id;
                    }
                }
            }

        }

        //Additional Params constructor for enterprise lead flow
        public AdditionalParams(auth0 content) {
            if (content.data.context != null){
                if (String.isNotBlank(content.data.context.campaignID)) CampaignId = content.data.context.campaignID;
                if (String.isNotBlank(content.data.context.sourceID2)) SourceId2 = content.data.context.sourceID2;
                if (String.isNotBlank(content.data.context.incentive)) Incentive = content.data.context.incentive;
                if (String.isNotBlank(content.data.context.url)) SourceUrl = content.data.context.url;
                if (String.isNotBlank(content.data.context.sourcePartner)) SourcePartner = content.data.context.sourcePartner;
                if (String.isNotBlank(content.data.context.Marketo)) Mktotrk = content.data.context.Marketo;
                if (String.isNotBlank(content.data.context.GoogleId)) GAClientID = content.data.context.GoogleId;

                if (String.isNotBlank(CampaignId) && CampaignId.length() == 15){
                    Id CID = CampaignId;
                    CampaignId = CID;
                }
                taskSubject = Incentive + ' - ' + SourceId2;
            }
        }

        //Additional Params constructor for Lead Generation (Generic Lead, Snowflake) 
        public AdditionalParams(LeadData content) {
            if (String.isNotBlank(content.campaignID)) CampaignId = content.campaignID;
            if (String.isNotBlank(content.sourceID2)) SourceId2 = content.sourceID2;
            if (String.isNotBlank(content.incentive)) Incentive = content.incentive;
            if (String.isNotBlank(content.url)) SourceUrl = content.url;
            if (String.isNotBlank(content.Marketo)) Mktotrk = content.marketo;
            if (String.isNotBlank(content.GoogleId)) GAClientID = content.googleId;
            if (String.isNotBlank(content.CampaignId) && content.CampaignId.length() == 15){
                Id CID = content.CampaignId;
                CampaignId = CID;
            }
            taskSubject = Incentive + ' - ' + SourceId2;
        }
    }

    public class ZiftParams {
        public string ziftpartnerID;
        public string ziftpartnername;
    }

    public class ULCData {
        public ULCUser oUser;
        public AdditionalParams additionalParams;
    }

    public class auth0{
        public string eventType;
        public data data;
        public extensions extensions;
    }

    public class extensions {
        public string subject;
    }
    public class data{
        public string email;
        public string source;
        public string firstName;
        public string lastName;
        public string company;
        public string countryCode;
        public string country;
        public string jobTitle;
        public string telephone;
        public context context;
        public string invitedBySubject;
        public string privacyPolicyAccepted;
        public string state;
    }

    public class context{
        public string webActivityType;
        public string timestamp;
        public string state;
        public string campaignID;
        public string incentive;
        public string leadSourceDetailMirror;
        public string sourceID2;
        public string googleId;
        public string marketo;
        public string userAgent;
        public string url;
        public string privacyPolicyAccepted;
        public string sourcePartner;
    }

    public class LeadData{
        public string email;
        public string firstName;
        public string lastName;
        public string company;
        public string countryCode;
        public string country;
        public string state;
        public string jobTitle;
        public string telephone;
        public string campaignID;
        public string comments;
        public string googleId;
        public string marketo;
        public string privacyPolicyAccepted;
        public string incentive;
        public string sourceID2;
        public string invitedBySubject;
        public string url;
    } 

//Supporting class methods 
    public static AdditionalParams AdditionalParamsSplit(AdditionalParams additParams){
        string DataToParse = 0 == additParams.SupplimentalData.indexOf('~') ? additParams.SupplimentalData.substring(1) : additParams.SupplimentalData;
        List<string> MyValues = DataToParse.split('~');

        if (math.mod(MyValues.size(), 2) == 1) // If equally divided with two means that something is wrong with string
        {                                      // best effort to fix the string is to append a "~" on the end.
            DataToParse += '~';
            MyValues = DataToParse.Split('~');
        }

        for (Integer i = 0; i < MyValues.size(); i += 2){
            string Key = MyValues[i];
            string Val = MyValues[i + 1];

            switch on Key.toUpperCase() {
                when 'LEADSOURCEDETAIL'         {additParams.LeadSourceDetail = Val;}
                when 'LEADSOURCEDETAILMIRROR'   {additParams.LeadSourceDetailMirror = Val;}
                when 'INCENTIVE'                {additParams.Incentive = Val;}
                when 'SOURCEID1'                {additParams.SourceId1 = Val;}
                when 'SOURCEID2'                {additParams.SourceId2 = Val;}
                when 'SOURCEURL'                {additParams.SourceUrl = Val;}
                when 'SOURCEULC'                {additParams.SourceULC = Val;}
                when 'SOURCEPARTNER'            {additParams.SourcePartner = Val;}
                when 'SOURCEEMPL'               {additParams.SourceEmployee = Val;}
                when 'WEB_ACTIVITY_SOURCE'      {additParams.WebActSrc = Val;}
                when 'LEAD_SOURCE'              {additParams.LeadSource = Val;}
                when 'PARTNERSOURCE'            {additParams.PartnerSource = Val;}
                when 'TCACCEPTED'               {additParams.TcAccepted = Val;}
                when 'CAMPAIGNID'               {additParams.CampaignId = Val;}
                when 'REF'                      {additParams.Ref = Val;}
                when 'FREETEXT'                 {additParams.FreeText = Val;}
                when 'DB_COMPANY_NAME'          {additParams.DBCompname = Val;}
                when 'WEBLEADINTEREST'          {additParams.WebLeadInterest = Val;}
                when 'EMPLOYEE_RANGE'           {additParams.EmployeeRange = Val;}
                when 'INDUSTRY'                 {additParams.Industry = Val;}
                when 'SUB_INDUSTRY'             {additParams.SubIndustry = Val;}
                when 'REGISTRY_STATE '          {additParams.RegState = Val;}
                when 'REGISTRY_COUNTRY'         {additParams.RegCountry = Val;}
                when 'JOB_FUNCTION'             {additParams.JobFunction = Val;}
                when 'KEYWORDS'                 {additParams.Keywords = Val;}
                when '_MKTO_TRK'                {additParams.Mktotrk = EncodingUtil.urlDecode(Val, 'UTF-8');}
            }
            if (String.isNotBlank(additParams.CampaignId) && additParams.CampaignId.length() == 15){
                Id CID = additParams.CampaignId;
                additParams.CampaignId = CID;
                system.debug('campaignid length check >>' + additParams.CampaignId);
            }

            if(String.isNotBlank(additParams.Incentive) && String.isNotBlank(additParams.SourceId2)){
                additParams.taskSubject = additParams.Incentive + ' - ' + additParams.SourceId2;
            }
        }         
        return additParams;
    }


    public static List<WebActivityUtils.WebActivityContent> LoadAdditionalParameters(List<WebActivityUtils.WebActivityContent> lstWAContent)
    {
        Set<String> lstCampaignIDs = new Set<String>();

        //Build list with CampaignIDs
        for(WebActivityUtils.WebActivityContent wa : lstWAContent)
        {
            if((wa.Sender == 'qlik.com' || wa.Sender == 'partner' || wa.Sender == 'snowflake') && String.isNotBlank(wa.oUser.CampaignID) && !lstCampaignIDs.contains(wa.oUser.CampaignID))
                lstCampaignIDs.add(wa.oUser.CampaignID);
        }

        //Create Map with campaign - we also pull fields from Marketing Asset through campaign
        Map<ID, Campaign> mapCampaigns = new Map<ID, Campaign>([SELECT Id, Name, Type, Campaign_Sub_Type__c, Primary_Marketing_Asset__r.Name, Primary_Marketing_Asset__r.Asset_Short_Title__c, Primary_Marketing_Asset__r.Type__c FROM Campaign where id in :lstCampaignIDs]);
        system.debug('LoadAdditionalParameters map ' + mapCampaigns);

        //Create Map with campaign types
        Map<string,string> mapCampaignTypes = new Map<string,string>();
        mapCampaignTypes.put('DG - Remarketing', 'Paid Search - Remarketing');
        mapCampaignTypes.put('DG - Display Advertising', 'Paid Search - Display Advertising');
        mapCampaignTypes.put('DG - Brand Search', 'Paid Search - Brand Search');
        mapCampaignTypes.put('DG - Non Brand Search', 'Paid Search - Non Brand Search');
        mapCampaignTypes.put('WB - Offer', 'Resource Library');
        mapCampaignTypes.put('WB - Qlik Account', 'Qlik Account');
        mapCampaignTypes.put('WB - Community', 'Community');
        mapCampaignTypes.put('WB - Product Registration', 'Product Registration');
        mapCampaignTypes.put('WB - Demo App', 'Demo App');
        mapCampaignTypes.put('WB - Contact Us', 'Contact Us');

        for(WebActivityUtils.WebActivityContent wa : lstWAContent)
        {
            if((wa.Sender == 'qlik.com' || wa.Sender == 'partner' || wa.Sender == 'snowflake') && String.isNotBlank(wa.oUser.CampaignID) && mapCampaigns.containsKey(wa.oUser.CampaignID))
            {
                Campaign camp = mapCampaigns.get(wa.oUser.CampaignID);

                //assign params from marketing asset
                string campaignType = String.isNotBlank(camp.Type) ? camp.Type : '';
                string campaignSubType = String.isNotBlank(camp.Campaign_Sub_Type__c) ? camp.Campaign_Sub_Type__c : '';
                string assetType = String.isNotBlank(camp.Primary_Marketing_Asset__r.Type__c) ? camp.Primary_Marketing_Asset__r.Type__c : '';
                string assetName = String.isNotBlank(camp.Primary_Marketing_Asset__r.Name) ? camp.Primary_Marketing_Asset__r.Name : '';
                string assetShortTitle = String.isNotBlank(camp.Primary_Marketing_Asset__r.Asset_Short_Title__c) ? camp.Primary_Marketing_Asset__r.Asset_Short_Title__c : '';

                //set additional params
                wa.additParams.LeadSourceDetailMirror = GetLeadSourceDetail(campaignType, campaignSubType, assetType, assetName);
                wa.additParams.Keywords = GetParameterByName('kw', wa.additParams.SourceUrl);
                wa.additParams.SourceId1 = GetParameterByName('utm_source', wa.additParams.SourceUrl);
                wa.additParams.PartnerSource = GetParameterByName('sourcepartner', wa.additParams.SourceUrl);
                if(String.isBlank(wa.additParams.PartnerSource) && String.isNotBlank(wa.additParams.SourcePartner))
                {
                    wa.additParams.PartnerSource = wa.additParams.SourcePartner;
                }

                //set Lead Source
                if(String.isNotBlank(campaignType) && (campaignType == 'DG - Digital' || campaignType == 'WB - Web'))
                    wa.oUser.LeadSource = 'WEB - Web Activity';

                system.debug('campType ' + campaignType);
                system.debug('assetType ' + assetType);
                system.debug('assetName ' + assetName);

                //set taskSubject if Incentive and SourceID2 is not passed
                if(String.isBlank(wa.additParams.Incentive) || String.isBlank(wa.additParams.SourceId2))
                    wa.additParams.taskSubject = mapCampaignTypes.get(campaignSubType) + ' - ' + assetType + ' - ' + assetShortTitle;

            }
        }

        system.debug('BAD LoadAdditionalParameters ' + lstWAContent);

        return lstWAContent;
    }


    private static string GetLeadSourceDetail(string campaignType, string campaignSubType, string assetType, string assetName)
    {

        system.debug('BAD GetLeadSourceDetail ' + campaignType + ' ' + campaignSubType + ' ' + assetName);

        if(assetName == 'Branch Developer Site')
        {
            return 'WEB - Branch';
        }
        else if(campaignType == 'DG - Digital')
        {
            return 'WEB - Paid Search';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Offer' && assetType == 'Prod Reg - Attunity Replicate')
        {
            return 'WEB - Attunity Replicate';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Offer')
        {
            return 'WEB - Resource Library';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Contact Us')
        {
            return 'WEB - Contact Us';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Product Registration' && assetType == 'Prod Reg - QlikView Personal Edition')
        {
            return 'WEB - Download QlikView';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Product Registration' && assetType == 'Prod Reg - Qlik Sense Desktop')
        {
            return 'WEB - Download QlikSense';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Product Registration' && assetType == 'Prod Reg - Qlik Sense Cloud Basic')
        {
            return 'WEB - Download QlikCloud';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Product Registration' && assetType == 'Prod Reg - Qlik Sense Cloud Business')
        {
            return 'WEB - Qlik Sense Cloud Business Trial';
        }
        else if(campaignType == 'WB - Web' && campaignSubType == 'WB - Product Registration' && assetType == 'Prod Reg - Qlik Sense Business')
        {
            return 'WEB - Qlik Sense Business Trial';
        }
        else if (campaignType == 'WB - Web' && campaignSubType == 'WB - Product Registration' && assetType == 'Prod Reg - Attunity Replicate')
        {
            return 'WEB - Qlik Replicate';
        }
        return '';

    }



    public static string GetParameterByName(string name, string url)
    {
        String urlid = null;
        if (String.isNotBlank(name) && String.isNotBlank(url)) {
                String[] urlParts = url.split('&');
                //String kw = null;
                if (urlParts.size() > 1) {
                    if (urlParts[0].contains('?')){
                       List<String> addUrlPart = urlParts[0].split('\\?');
                       urlParts.add(addUrlPart[1]);
                       system.debug('url part'+urlParts);
                    }
                    for (String parts:urlParts){
                        //query = urlParts[1];
                        if (parts.startsWith(name+'=')) {
                            urlid = parts.substring(parts.indexOf('=') + 1);

                        }
                    }
                }
                //System.debug('source' + source);
                //System.debug('camgn' );
                System.debug('id' + urlid);
        }

        return urlid;
    }

}
