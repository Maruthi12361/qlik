/*************************************************************************************************************************************

    Changelog:
        2012-02-15  CCE     Added QlikTech_Company__c and Billing to Account creation due to required QlikTech Company field on Account
        2012-02-16  CCE     Added Type='TK - Email' to Task creation due to validation rule 'Task_cant_be_saved_when_Type_equals_None'
        2012-04-11  RDZ     Change Username of the inserted User to have the DateTime. 
        2013-03-12  SAN     separate the test methods into two to bypass the SOQL limit
        2014-09-08  KMH     Added Semaphore flag https://eu1.salesforce.com/a0CD000000iErmb -CR# 12644
        2015-08-25  RDZ     CR# 19480 Adding a new way of creating test user with a qliktech company created on the fly.
        2015-10-22  RDZ     Winter16 issue adding to api 31 from 19
        2018-12-03  CTS     SCU-5 Removal of variables used in Inactive Triggers.
*************************************************************************************************************************************/
@isTest
private class TestTViewContExt 
{
    static testMethod void testTViewValidCase() 
    {
        //Set PageReference  and parameter for Valid Case
        Pagereference pgRef = Page.TView;
        pgRef.getParameters().put('tViewType', 'internal');
        Test.setCurrentPageReference(pgRef); 
        
        //reset semaphores to continue test
        
        //CTS SCU-5 Removal of Inactive Triggers
        //Semaphores.CaseUpdateSupportContact_HasRunAfterUpdate= false;
        //Semaphores.CaseUpdateSupportContact_HasRunAfterDelete= false;

    
        //Create a Valid Case       
        Case testValidCase = createTestCaseAndData();        
        ApexPages.Standardcontroller scTestCaseInternal = new ApexPages.Standardcontroller(testValidCase); 

           
        Test.startTest();
        
        //Block for testing Internal T-View for a Case
        TViewContExt tvce = new TViewContExt(scTestCaseInternal);
        //System.assertEquals(31, tvce.caseEvents.size());
        System.assertEquals(11, tvce.caseEvents.size());
        
        //Block for testing Customer T-View for a Case
        tvce.tViewType = 'customer';
        tvce.initPage();
        System.debug('***** tvce.caseEvents.size(): ' + tvce.caseEvents.size() + ' *****');
        //System.assertEquals(29, tvce.caseEvents.size());    
        System.assertEquals(9, tvce.caseEvents.size());    
              
        Test.stopTest();
    }
    
    static testMethod void testTViewInvalidCase() 
    {
        //Set PageReference  and parameter for Valid Case
        Pagereference pgRef = Page.TView;
        pgRef.getParameters().put('tViewType', 'internal');
        Test.setCurrentPageReference(pgRef); 
        
        //Create a Invalid Case
        Case testInvalidCase = createInvalidTestCase();
        ApexPages.Standardcontroller scTestCaseInvalid = new ApexPages.Standardcontroller(testInvalidCase);
        
        
        Test.startTest();
                    
        //Block for testing Invalid Case        
        TViewContExt tvceInvalid = new TViewContExt(scTestCaseInvalid);
        System.assertEquals(0, tvceInvalid.caseEvents.size());
        
        Test.stopTest();
    }     
    
    //Method to create Invalid Test Case
    static Case createInvalidTestCase()
    {
    
        //Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        //UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        //User thisUserForRunAs = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
    
        //reset semaphores to continue test
        
        //CTS SCU-5 Removal of Inactive Triggers
        //Semaphores.CaseUpdateSupportContact_HasRunAfterUpdate= false;
        //Semaphores.CaseUpdateSupportContact_HasRunAfterDelete= false;   
         
        //Create test data
        //Create test User
        /*User testUser = new User(FirstName = 'Test2', 
                                 LastName= 'User2',
                                 Alias = 'tUser2', 
                                 Email = 'testUser2@testorg.com', 
                                 UserName = 'testUser2@testorg.com', 
                                 EmailEncodingKey='UTF-8', 
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US', 
                                 TimeZoneSidKey='America/Los_Angeles',
                                 ProfileId = testProfile.Id,
                                 UserRoleId = testUserRole.Id);
        */
        
        //Create test data
        //Create test User using SFDC Recomendation to avoid duplicate username
        User testUser =  QTTestUtils.createMockSystemAdministrator();//Util.createUser('TestUserName', 'Testttt4', testProfile.Id, testUserRole.Id);
        //insert testUser;
        
        Case testCaseRet = new Case();
        
        System.runAs(testUser)
        {
            //RDZ Revome this section to add account from QTTESTUtils
            //Create test Account
            /*
            QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'US',
                QlikTech_Company_Name__c = 'QlikTech Inc'           
            );
            insert QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account(Name='Test2 Account2', OwnerId = testUser.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
            insert testAcnt;
            */
            Account testAcnt = QTTESTUtils.createMockAccount('Test2 Account2', testUser);
            //Create test Contact
            Contact testContact = new Contact(FirstName='Test2', LastName='Contact2', AccountId=testAcnt.Id, OwnerId=testUser.Id);
            insert testContact;
            
            //Create test Case
            Case testCase = new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id);
            insert testCase;
            
            testCaseRet = testCase;         
        }
        return testCaseRet;        
    }
    
    //Method to create a Valid Test Case
    static Case createTestCaseAndData()
    {
        //Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        //UserRole testUserRole = [SELECT Id, Name FROM UserRole WHERE Name = 'CEO' LIMIT 1];
        //User thisUserForRunAs = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        
        //Create test data
        //Create test User
        /*User testUser = new User(FirstName = 'Test1', 
                                 LastName= 'User1',
                                 Alias = 'tUser1', 
                                 Email = 'testUser1@testorg.com', 
                                 UserName = 'testUser1@testorg.com', 
                                 EmailEncodingKey='UTF-8', 
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US', 
                                 TimeZoneSidKey='America/Los_Angeles',
                                 ProfileId = testProfile.Id,
                                 UserRoleId = testUserRole.Id);*/
        //Create test data
        //Create test User using SFDC Recomendation to avoid duplicate username
        //RDZ remove this start
        //User testUser = Util.createUser('TestUserName', 'Testttt5', testProfile.Id, testUserRole.Id);
        //insert testUser;
        //RDZ remove this end
        //RDZ Added this
        User testUser = QTTestUtils.createMockSystemAdministrator();

        Case testCaseRet = new Case();
        
        System.runAs(testUser)//System.runAs(thisUserForRunAs)
        {
            //Create test Account
            /*QlikTech_Company__c QTComp = new QlikTech_Company__c(
                Name = 'USA',
                QlikTech_Company_Name__c = 'QlikTech Inc'           
            );
            insert QTComp;
            QTComp = [select Id, Name, QlikTech_Company_Name__c from QlikTech_Company__c where Id = :QTComp.Id];
        
            Account testAcnt = new Account(Name='Test1 Account1', OwnerId = testUser.Id, QlikTech_Company__c = QTComp.QlikTech_Company_Name__c, Billing_Country_Code__c = QTComp.Id);
            insert testAcnt;
            */
            Account testAcnt = QTTESTUtils.createMockAccount('Test1 Account1', testUser);
            //Create test Contact
            Contact testContact = new Contact(FirstName='Test1', LastName='Contact1', AccountId=testAcnt.Id, OwnerId=testUser.Id);
            insert testContact;
            
            //Create test Case
            Case testCase = new Case(ContactId=testContact.Id, Status='New' , OwnerId=testUser.Id);
            insert testCase;
        
            List<CaseComment> testCaseComments =  new List<CaseComment>();
            testCaseComments.add(new CaseComment(CommentBody='test comment body1', IsPublished=true, ParentId = testCase.Id));
            testCaseComments.add(new CaseComment(CommentBody='test comment body2', IsPublished=true, ParentId = testCase.Id));
            testCaseComments.add(new CaseComment(CommentBody='test comment body3', IsPublished=true, ParentId = testCase.Id));
            testCaseComments.add(new CaseComment(CommentBody='test comment body4', IsPublished=false, ParentId = testCase.Id));
            testCaseComments.add(new CaseComment(CommentBody='test comment body5', IsPublished=false, ParentId = testCase.Id));
            insert testCaseComments;    
            
            //2012-02-16  CCE       Added Type='TK - Email' to Task creation due to validation rule 'Task_cant_be_saved_when_Type_equals_Select'
            List<Task> testTasks = new List<Task>();
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject1', Status='Completed',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject2', Status='Completed',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject3', Status='Completed',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject4', Status='Completed',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject5', Status='Completed',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject6', Status='In Progress',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject7', Status='In Progress',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject8', Status='In Progress',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject9', Status='In Progress',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            testTasks.add(new Task(WhoId=testContact.Id, Subject='test subject10', Status='In Progress',Priority='Normal', WhatId=testCase.Id, Type='TK - Email'));
            insert testTasks;
            
            List<Event> testEvents = new List<Event>();
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject1', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=false, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject2', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=false, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject3', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=false, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject4', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=false, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject5', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=false, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject6', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=true, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject7', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=true, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject8', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=true, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject9', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=true, WhatId=testCase.Id));
            testEvents.add(new Event(WhoId=testContact.Id, Subject='test subject10', StartDateTime=System.now(), EndDateTime=System.now(), Completed__c=true, WhatId=testCase.Id));
            insert testEvents;
            
            List<Attachment> testCaseAttachments = new List<Attachment>();
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body1');
            testCaseAttachments.add(new Attachment(Name='My Doc1', ParentId=testCase.Id, Body=bodyBlob, ContentType='txt'));
            
            bodyBlob=Blob.valueOf('Unit Test Attachment Body2');
            testCaseAttachments.add(new Attachment(Name='My Doc2', ParentId=testCase.Id, Body=bodyBlob, ContentType='txt'));
            
            bodyBlob=Blob.valueOf('Unit Test Attachment Body3');
            testCaseAttachments.add(new Attachment(Name='My Doc3', ParentId=testCase.Id, Body=bodyBlob, ContentType='txt'));
            
            bodyBlob=Blob.valueOf('Unit Test Attachment Body4');
            testCaseAttachments.add(new Attachment(Name='My Doc4', ParentId=testCase.Id, Body=bodyBlob, ContentType='txt'));
            
            bodyBlob=Blob.valueOf('Unit Test Attachment Body5');
            testCaseAttachments.add(new Attachment(Name='My Doc5', ParentId=testCase.Id, Body=bodyBlob, ContentType='txt'));
            
            bodyBlob=Blob.valueOf('Unit Test Attachment Body6');
            testCaseAttachments.add(new Attachment(Name='My Doc6', ParentId=testCase.Id, Body=bodyBlob, ContentType='txt'));
            insert testCaseAttachments;
            
            testCaseRet = testCase;
        }
        return testCaseRet;
    }    
}