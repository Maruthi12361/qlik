/*************************************************************************************************************
 Name: DispatcherMDNewControllerTest
 Author: CCE
 Purpose: This is test class for DispatcherMDNewController
  
 Log History:
 2019-04-01 Initial Development
*************************************************************************************************************/
@isTest
private class DispatcherMDNewControllerTest {
	
	static testmethod void DispatcherMDNewController_TestForMarketoSupportRequestsRecordType()
    {
        System.debug('DispatcherMDNewController_TestForDataHygieneRecordType: Starting');

        ApexPages.StandardController stdController = new ApexPages.StandardController(new Marketing_Deliverable__c());
        DispatcherMDNewController controller = new DispatcherMDNewController(stdController);

        User mockSysAdmin = QTTestUtils.createMockSystemAdministrator();
        //Custom settings
        Marketing_Deliverable_Default__c MDSettings = new Marketing_Deliverable_Default__c();
        MDSettings.Default_Assigned_To_User__c = mockSysAdmin.FirstName + ' ' + mockSysAdmin.LastName;
        MDSettings.Default_Assigned_To_UserId__c = mockSysAdmin.Id;
        insert MDSettings;
        
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('RecordType', '012D0000000Jrq5');   //Data Hygiene record type
        
        PageReference pageref = controller.getRedir();
        //if the record type is "Data Hygiene" pageref should be populated.
        System.assertNotEquals(null, pageref);

        Test.stopTest();

        System.debug('DispatcherMDNewController_TestForDataHygieneRecordType: Finishing');        
    }
	
}