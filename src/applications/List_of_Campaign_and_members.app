<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Campaign__c</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>List of Campaign and members</label>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>Campaign__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Marketing_Asset_NEW__c</tab>
    <tab>Sales_Tools</tab>
    <tab>Quote_Approvals</tab>
    <tab>Support_Survey_Task__c</tab>
</CustomApplication>
