<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Project_List__c</defaultLandingTab>
    <description>Storage area for non IS projects</description>
    <formFactors>Large</formFactors>
    <label>Project List</label>
    <tab>Project_List__c</tab>
    <tab>Qollaborate_Project__c</tab>
    <tab>Marketing_Asset_NEW__c</tab>
    <tab>Sales_Tools</tab>
    <tab>Quote_Approvals</tab>
    <tab>Support_Survey_Task__c</tab>
</CustomApplication>
