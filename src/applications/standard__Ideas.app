<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-Idea</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Case_Translation__c</tab>
    <tab>QMarketing</tab>
    <tab>Deployment_Overview__c</tab>
    <tab>QCase</tab>
    <tab>QAccountProfile</tab>
    <tab>Survey__c</tab>
    <tab>Qlikview_Support_Escalation__c</tab>
    <tab>Forecasting_App</tab>
    <tab>Marketing_Asset_NEW__c</tab>
    <tab>Sales_Tools</tab>
    <tab>Quote_Approvals</tab>
    <tab>Support_Survey_Task__c</tab>
</CustomApplication>
