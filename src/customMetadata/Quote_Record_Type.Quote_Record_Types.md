<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Quote Record Types</label>
    <protected>false</protected>
    <values>
        <field>Evaluation_Locked_Approved__c</field>
        <value xsi:type="xsd:string">012D0000000khWhIAI</value>
    </values>
    <values>
        <field>Evaluation_Locked_Submitted__c</field>
        <value xsi:type="xsd:string">012D0000000khWjIAI</value>
    </values>
    <values>
        <field>Evaluation_Open__c</field>
        <value xsi:type="xsd:string">012D0000000khWgIAI</value>
    </values>
    <values>
        <field>Evaluation_Quote_Locked_Closed__c</field>
        <value xsi:type="xsd:string">012D0000000khWiIAI</value>
    </values>
    <values>
        <field>Internal_NFR_Quote_Locked_Approved__c</field>
        <value xsi:type="xsd:string">012D0000000khWlIAI</value>
    </values>
    <values>
        <field>Internal_NFR_Quote_Locked_Closed__c</field>
        <value xsi:type="xsd:string">012D0000000khWmIAI</value>
    </values>
    <values>
        <field>Internal_NFR_Quote_Locked_Submitted__c</field>
        <value xsi:type="xsd:string">012D0000000khWnIAI</value>
    </values>
    <values>
        <field>Internal_NFR_Quote__c</field>
        <value xsi:type="xsd:string">012D0000000khWkIAI</value>
    </values>
    <values>
        <field>Open_Quote__c</field>
        <value xsi:type="xsd:string">012D0000000khWsIAI</value>
    </values>
    <values>
        <field>Partner_Purchase_Locked_Approved__c</field>
        <value xsi:type="xsd:string">012D0000000khWpIAI</value>
    </values>
    <values>
        <field>Partner_Purchase_Locked_Closed__c</field>
        <value xsi:type="xsd:string">012D0000000khWqIAI</value>
    </values>
    <values>
        <field>Partner_Purchase_Locked_Submitted__c</field>
        <value xsi:type="xsd:string">012D0000000khWrIAI</value>
    </values>
    <values>
        <field>Partner_Purchase_Open__c</field>
        <value xsi:type="xsd:string">012D0000000khWoIAI</value>
    </values>
    <values>
        <field>Pricebook__c</field>
        <value xsi:type="xsd:string">01s20000000E0PWAA0</value>
    </values>
    <values>
        <field>Quote_Locked_Approved__c</field>
        <value xsi:type="xsd:string">012D0000000khWtIAI</value>
    </values>
    <values>
        <field>Quote_Locked_Closed__c</field>
        <value xsi:type="xsd:string">012D0000000khWuIAI</value>
    </values>
    <values>
        <field>Quote_Locked_Submitted__c</field>
        <value xsi:type="xsd:string">012D0000000khWvIAI</value>
    </values>
</CustomMetadata>
