<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DEVQ</label>
    <protected>false</protected>
    <values>
        <field>BoomiBaseURL__c</field>
        <value xsi:type="xsd:string">https://q2cwsbx.qliktech.com:9093/ws/simple/</value>
    </values>
    <values>
        <field>CMInstanceUrl__c</field>
        <value xsi:type="xsd:string">uatna11</value>
    </values>
    <values>
        <field>NSQuoteApprovals__c</field>
        <value xsi:type="xsd:string">https://testwebservices.qliktech.com/sfdcnsjumpsitedev</value>
    </values>
    <values>
        <field>QlikMarket_Website__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>SpringAccountPrefix__c</field>
        <value xsi:type="xsd:string">8663</value>
    </values>
    <values>
        <field>Support_Portal_CSS_Base__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>Support_Portal_index_allow_options__c</field>
        <value xsi:type="xsd:string">Disallow: /</value>
    </values>
    <values>
        <field>Support_Portal_Live_Agent_API_Endpoint__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>Support_Portal_Login_Page_Url__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>Support_Portal_Login_Url__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>Support_Portal_Logout_Page_Url__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>Support_Portal_Url_Base__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>Support_Portal_Url__c</field>
        <value xsi:type="xsd:string">TODO</value>
    </values>
    <values>
        <field>ULC_Base_URL__c</field>
        <value xsi:type="xsd:string">https://qlikid-dev.qlik.com</value>
    </values>
</CustomMetadata>
