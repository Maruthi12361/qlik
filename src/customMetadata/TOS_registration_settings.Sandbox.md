<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sandbox</label>
    <protected>false</protected>
    <values>
        <field>Endpoint__c</field>
        <value xsi:type="xsd:string">https://demo.viewcentral.com/events/api/default.asp</value>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:type="xsd:string">kmscp@yPEJQ765</value>
    </values>
    <values>
        <field>Username__c</field>
        <value xsi:type="xsd:string">QTWeb</value>
    </values>
</CustomMetadata>
