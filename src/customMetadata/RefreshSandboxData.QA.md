<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>QA</label>
    <protected>false</protected>
    <values>
        <field>BoomiBaseURL__c</field>
        <value xsi:type="xsd:string">https://boomi-qa.qlik.com/ws/rest/sfdc/</value>
    </values>
    <values>
        <field>BoomiToken__c</field>
        <value xsi:type="xsd:string">Basic c2FsZXNmb3JjZV9xYUBxbGlrdGVjaC1UU0I2UkM6M2UxYTMzZmQtNGJmNy00ZDQ0LTgzNjYtYmIwZmRkYWQ5NGIx</value>
    </values>
    <values>
        <field>CMInstanceUrl__c</field>
        <value xsi:type="xsd:string">uatna11</value>
    </values>
    <values>
        <field>NSQuoteApprovals__c</field>
        <value xsi:type="xsd:string">https://testwebservices.qliktech.com/sfdcnsjumpsiteqa</value>
    </values>
    <values>
        <field>QlikMarket_Website__c</field>
        <value xsi:type="xsd:string">http://marketstage.qlik.com/</value>
    </values>
    <values>
        <field>SpringAccountPrefix__c</field>
        <value xsi:type="xsd:string">8154</value>
    </values>
    <values>
        <field>Support_Portal_CSS_Base__c</field>
        <value xsi:type="xsd:string">https://s3.amazonaws.com/qliksupport/css/qa</value>
    </values>
    <values>
        <field>Support_Portal_index_allow_options__c</field>
        <value xsi:type="xsd:string">Disallow: /</value>
    </values>
    <values>
        <field>Support_Portal_Live_Agent_API_Endpoint__c</field>
        <value xsi:type="xsd:string">https://d.la1-c2cs-lon.salesforceliveagent.com/chat</value>
    </values>
    <values>
        <field>Support_Portal_Login_Page_Url__c</field>
        <value xsi:type="xsd:string">https://login-qa.qlik.com/login.aspx?returnURL=/external/support.aspx</value>
    </values>
    <values>
        <field>Support_Portal_Login_Url__c</field>
        <value xsi:type="xsd:string">https://qa-qliksupport.cs81.force.com</value>
    </values>
    <values>
        <field>Support_Portal_Logout_Page_Url__c</field>
        <value xsi:type="xsd:string">https://login-qa.qlik.com/logout.aspx</value>
    </values>
    <values>
        <field>Support_Portal_Url_Base__c</field>
        <value xsi:type="xsd:string">https://login-qa.qlik.com/external/support.aspx</value>
    </values>
    <values>
        <field>Support_Portal_Url__c</field>
        <value xsi:type="xsd:string">https://login-qa.qlik.com/external/support.aspx?u=https://qa-qliksupport.cs81.force.com/</value>
    </values>
    <values>
        <field>ULC_Base_URL__c</field>
        <value xsi:type="xsd:string">https://qlikid-qa.qlik.com</value>
    </values>
</CustomMetadata>
