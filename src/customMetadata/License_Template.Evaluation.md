<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Evaluation</label>
    <protected>false</protected>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">00XD0000002JK80</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Evaluation</value>
    </values>
</CustomMetadata>
