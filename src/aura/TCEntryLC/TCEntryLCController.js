({
	init : function(component, event, helper) {
		var action = component.get("c.getBaseURLForCommunity");
        action.setCallback(this, function(a) {
            var baseUrl = a.getReturnValue().split("/s/", 1)[0];
            component.set("v.vfHost", baseUrl);
        });
        $A.enqueueAction(action);
	}
})