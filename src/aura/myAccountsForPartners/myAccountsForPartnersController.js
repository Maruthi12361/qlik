({  
	init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            { label: 'Name', fieldName: 'Id', sortable: true, type: 'url', typeAttributes: {label :{ fieldName: 'Name'}, target : '_self'}},
            { label: 'Record Type', fieldName: 'RecordType', sortable: true, type: 'text'},
            { label: 'Street', fieldName: 'Territory_street__c', sortable: true, type: 'text'},
            { label: 'City', fieldName: 'Territory_city__c',sortable: true,  type: 'text'},
            { label: 'Country', fieldName: 'Territory_Country__c', sortable: true, type: 'text'},
            { label: 'Zip', fieldName: 'Territory_Zip__c', sortable: true, type: 'text'},
            { label: 'Sector', fieldName: 'Sector__c', sortable: true, type: 'text'},
            { label: 'Industry', fieldName: 'Industry_from_SIC_Code_Lookup__c', sortable: true, type: 'text'},
            ]);
            
        var totalCnt = cmp.get("c.getRecordCountAndIds");
        totalCnt.setCallback(this, function(a) {
            var accWrapper = a.getReturnValue();
            cmp.set("v.accountIds", accWrapper.accountIds);
            cmp.set("v.totalNumberOfRows", accWrapper.count);
            
            helper.getData(cmp);
        });
        $A.enqueueAction(totalCnt);
        
        
    },
            
   	updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
            
        var sortDirection = event.getParam('sortDirection');
        
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        
        fieldName = (fieldName=='Id') ? 'Name' : fieldName; //Since sorting on Ids doesn't make Names sort.
        
        helper.sortData(cmp, fieldName, sortDirection);
    },
            
    loadMoreData: function (component, event, helper) {
        //Display a spinner to signal that data is being loaded
        event.getSource().set("v.isLoading", true);
        //Display "Loading" when more data is being loaded
        component.set('v.loadMoreStatus', 'Loading...');
        helper.fetchData(component, component.get('v.rowsToLoad'))
            .then($A.getCallback(function (data) {
            if (component.get('v.data').length >= component.get('v.totalNumberOfRows')) {
                component.set('v.enableInfiniteLoading', false);
                component.set('v.loadMoreStatus', 'No more data to load.');
            } else {
                var currentData = component.get('v.data');
                //Appends new data to the end of the table
                var newData = currentData.concat(data);
                component.set('v.data', newData);
                component.set('v.loadMoreStatus', 'Please wait... ');
            }
            event.getSource().set("v.isLoading", false);
        }));
    },
})