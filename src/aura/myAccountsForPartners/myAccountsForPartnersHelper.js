({  
	getData : function(component) {
        var action = component.get('c.getAccounts');
        console.log("Account Ids : " + component.get("v.accountIds").length);
        action.setParams({
            "accountIds" : component.get("v.accountIds"),
            "limits": component.get("v.initialRows"),
            "offsets": component.get("v.rowNumberOffset")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                for (var i = 0; i < data.length; i++) {
                    var row = data[i];
                    if (row.Id) row.Id = (window.location.hostname.includes('qlikcommerce')) ? '/s/detail/'+row.Id: '/QlikCommerce/s/detail/'+row.Id;
                    if (row.RecordType) row.RecordType = row.RecordType.Name;
                    //if (row.Owner) row.Owner = row.Owner.FirstName + ' ' + row.Owner.LastName;
                }
                component.set('v.data', data);
                component.set("v.currentCount", component.get("v.initialRows"));
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.data");
        var reverse = sortDirection !== 'asc';
        //console.log("SD 1 : " + sortDirection);
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.data", data);
        
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        //console.log("SD 2 : " + sortDirection);
        
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
        
    },
    
    fetchData: function(component , rows){
        return new Promise($A.getCallback(function(resolve, reject) {
            var currentDatatemp = component.get('c.getAccounts');
            var counts = component.get("v.currentCount");
            currentDatatemp.setParams({
                "accountIds" : component.get("v.accountIds"),
            	"limits": rows,
                "offsets": counts 
            });
            currentDatatemp.setCallback(this, function(a) {
                var data = a.getReturnValue();
                for (var i = 0; i < data.length; i++) {
                    var row = data[i];
                    if (row.Id) row.Id = (window.location.hostname.includes('qlikcommerce')) ? '/s/detail/'+row.Id: '/QlikCommerce/s/detail/'+row.Id;
                    if (row.RecordType) row.RecordType = row.RecordType.Name;
                   // if (row.Owner) row.Owner = row.Owner.FirstName + ' ' + row.Owner.LastName;
            	}
                resolve(data);
                var countstemps = component.get("v.currentCount");
                countstemps = countstemps+rows;
                component.set("v.currentCount",countstemps);
                
            });
            $A.enqueueAction(currentDatatemp);
            
            
        }));
        
    } ,
})