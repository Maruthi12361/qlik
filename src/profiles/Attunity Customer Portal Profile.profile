<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <custom>true</custom>
    <fieldPermissions>
        <editable>false</editable>
        <field>Account_License__c.Signature_End_Date__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>false</editable>
        <field>Case.Signature_End_Date__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>false</editable>
        <field>Entitlement.Signature_End_Date__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <tabVisibilities>
        <tab>Health_Check_Activity__c</tab>
        <visibility>Hidden</visibility>
    </tabVisibilities>
    <userLicense>Guest User License</userLicense>
</Profile>
