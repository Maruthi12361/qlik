<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <categoryGroupVisibilities>
        <dataCategoryGroup>Attunity_Data_Categories</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <classAccesses>
        <apexClass>PlanOfActionController</apexClass>
        <enabled>true</enabled>
    </classAccesses>
    <custom>true</custom>
    <fieldPermissions>
        <editable>true</editable>
        <field>Case.PoA_Created__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <layoutAssignments>
        <layout>Skills_Assessment__c-Skills Assessment %5BData Literacy 2020%5D Layout</layout>
        <recordType>Skills_Assessment__c.Data_Literacy_2020</recordType>
    </layoutAssignments>
    <pageAccesses>
        <apexPage>PlanOfAction</apexPage>
        <enabled>true</enabled>
    </pageAccesses>
    <userLicense>Overage Customer Portal Manager Custom</userLicense>
</Profile>
