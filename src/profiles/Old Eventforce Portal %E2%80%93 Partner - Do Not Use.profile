<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <categoryGroupVisibilities>
        <dataCategoryGroup>Attunity_Data_Categories</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <categoryGroupVisibilities>
        <dataCategoryGroup>Product</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <categoryGroupVisibilities>
        <dataCategoryGroup>Version</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <custom>true</custom>
    <fieldPermissions>
        <editable>false</editable>
        <field>Case.Is_Case_Contact_logged_as_User__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <layoutAssignments>
        <layout>Skills_Assessment__c-Skills Assessment %5BData Literacy 2020%5D Layout</layout>
        <recordType>Skills_Assessment__c.Data_Literacy_2020</recordType>
    </layoutAssignments>
    <userLicense>Limited Customer Portal Manager Custom</userLicense>
</Profile>
