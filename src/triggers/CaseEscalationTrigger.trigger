/*********************************************
Change Log:

2018-07-09 ext_bad CHG0034165: create a trigger to populate Qlikview_Support_Escalation__c fields.
********************************************/
trigger CaseEscalationTrigger on Qlikview_Support_Escalation__c (before insert, after insert, before update ) {

    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            CaseEscalationHandler.populateFields(Trigger.new);
            CaseEscalationHandler.populateContactRole(Trigger.new, new Map<Id, Qlikview_Support_Escalation__c>());
        } else {
            CaseEscalationHandler.sendEmails(Trigger.new);
        }
    } else {
        for (Qlikview_Support_Escalation__c esc : Trigger.new) {
            if (esc.From_Quick_Action__c) {
                esc.From_Quick_Action__c = false;
            }
        }
        CaseEscalationHandler.populateContactRole(Trigger.new, Trigger.oldMap);
    }
}