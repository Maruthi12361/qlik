/********************************************************
* Trigger: CoveoCaseAttachedResult
* Description: Trigger for Attached Results object, object is part of Coveo managed package.
*
* Change Log:
* -------
* AUTHOR      DATE        DETAIL
*
* AIN         2019-07-09  Initial Development for IT-1959
*
**********************************************************/
trigger CoveoCaseAttachedResult on CoveoV2__CoveoCaseAttachedResult__c (before insert, after insert, before update, after update, before delete, after delete) {

    if(Trigger.isInsert && Trigger.isAfter){
        CoveoCaseAttachedResultTriggerHandler.OnAfterInsert(Trigger.new);
    }
    else if(Trigger.isDelete && Trigger.isAfter){
        CoveoCaseAttachedResultTriggerHandler.OnAfterDelete(Trigger.old);
    }
}