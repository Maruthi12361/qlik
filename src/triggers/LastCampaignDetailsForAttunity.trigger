/** File Name:LastCampaignDetailsForAttunity
*   Description : The trigger populates Last Campaingn Name and Last Camapgin Date on associated Contact & Lead record
*   @author : Slalom LLC
*   Modification Log *
Ver     Date         Author         Modification 
1.0 Aug 13 2019 Slalom LLC     Added new trigger logic.
*/
trigger LastCampaignDetailsForAttunity on CampaignMember (after insert, after update) {
    //For Leads - Start
    List<Id> leadIdList = new List<Id> ();
    List<Id> campaignIdList = new List<Id> ();
    Map<Id, Lead> leadMap = null;
    List<Lead> leadsToUpdate = new List<Lead> ();
    // For Leads - End
    //For Contact - Start
    List<Id> campaignsIdListForContact = new List<Id>();
    List<CampaignMember> campaignContactList = new List<CampaignMember>();
    Map<Id,Campaign> campaignsMap = null;
    Map<Id,Contact> contactsToUpdate = new Map<Id,Contact>();
    //For Contact - End

    for (CampaignMember cm : Trigger.new) {
		  if(cm.LeadId != null){
            leadIdList.add(cm.LeadId);
            campaignIdList.add(cm.CampaignId);
          }
          if(cm.ContactId != null){
            campaignsIdListForContact.add(cm.CampaignId);
            campaignContactList.add(cm);
          }
    }

    if (leadIdList != null) {
        leadMap = new map<Id, Lead>([SELECT Id,(SELECT CampaignId, Campaign.Name, CreatedDate, Campaign.RecordType.Name, 
                                                        HasResponded, Campaign.SFDC_Org2Id__c, Campaign.Type
				                                 FROM CampaignMembers ORDER BY LastModifiedDate desc) 
                                    FROM Lead
                                    WHERE Id IN :LeadIdList]);

        for (Lead lead : leadMap.values()) {

            if (lead.CampaignMembers != null) {
				    CampaignMember lastCampaignMember = lead.CampaignMembers[0];
                if (lastCampaignMember.Campaign.RecordType.Name == 'Marketing Desktop Campaign Layout' &&
                    lastCampaignMember.Campaign.Type != 'TM - Telemarketing' &&
                    lastCampaignMember.HasResponded == true){
                        lead.Last_Campaign_Response_Name__c = lastCampaignMember.Campaign.Name;
                        lead.Last_Campaign_Response_Date__c = lastCampaignMember.CreatedDate;
                    leadsToUpdate.add(lead);
                }
            }
        }
        if(!leadsToUpdate.isEmpty())
            Database.update(leadsToUpdate, false); 
    }

    if(!campaignsIdListForContact.isEmpty()){
        campaignsMap = new map<ID,Campaign>([SELECT Id, Name, SFDC_Org2Id__c FROM Campaign WHERE Id IN: campaignsIdListForContact]);
        for(CampaignMember camp: campaignContactList){
            if(campaignsMap.containsKey(camp.CampaignId)){
                Campaign cmp = campaignsMap.get(camp.CampaignId);
                if(contactsToUpdate.containsKey(camp.CampaignId))    continue;
                Contact con = new Contact(id=camp.ContactId);
                con.Last_Campaign_Response_Name__c = cmp.Name;
                con.Last_Campaign_Response_Date__c = DateTime.now();
                contactsToUpdate.put(con.Id,con);
            }
        }
        if(!contactsToUpdate.isEmpty())
            Database.update(contactsToUpdate.values(), false);
    }
}