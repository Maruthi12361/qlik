/* 
	CR# 8498	https://eu1.salesforce.com/a0CD000000ZAAjC
	For campaigns originated from Qchi CPT, we need to change the owner to the correct user ID
	that is saved in Qchi_ownerId__c
	27/06/2013	TJG	First created
*/
trigger SetCampaignOwnerForRecordsFromQchi on Campaign (before update) {
	for (integer i=0; i<Trigger.new.size(); i++) {
		if ('00520000000zCfxAAE' == Trigger.New[i].OwnerId && null != Trigger.New[i].Qchi_OwnerId__c && 0 < Trigger.New[i].Qchi_OwnerId__c.trim().length() && Trigger.New[i].OwnerId != Trigger.New[i].Qchi_OwnerId__c) {
			Trigger.New[i].OwnerId = Trigger.New[i].Qchi_OwnerId__c;
		}
	}
}