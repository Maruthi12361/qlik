/********************************************************
* Trigger: CRtoCase
* Description: Populate lookup field on Case record to point to CR when CR is saved while converting a case to CR
*   (CR# 5744)
*
*   Change Log:
*   ------- 
*   VERSION AUTHOR      DATE        DETAIL
*                                            
*       1.  YKA         2013-05-14  Initial Development
*                                   CR# 5744 https://eu1.salesforce.com/a0CD000000NIvu4
*                                   #DEV Create CR based on Case
*   
**********************************************************/

trigger CRtoCase on SLX__Change_Control__c (after insert, after update, before delete) 
{
    // Map of Case Id, Change Control Id
    Map<Id, Id> mapCaseChgC = new Map<Id, Id>();
    Map<Id, Id> mapCaseChgCDel = new Map<Id, Id>();
    //List<Case> updateCases = new List<Case>();
    // For Insert and Update operations
    if(trigger.isAfter)
    { 
    	List<Case> updateCases = new List<Case>();  
        for(SLX__Change_Control__c record: trigger.new)
        {
            if(record.Problem_Case_Nr__c != null)
            {               
                // Assumption is that one Case would be linked to only one Change Request
                // Adding to Map Case Id, Change Request Id
                mapCaseChgC.put(record.Problem_Case_Nr__c, record.Id);              
            }
        }       
    
        if(mapCaseChgC != null && (!mapCaseChgC.isEmpty()) && mapCaseChgC.size() >0)
        {
            for(Case caseRec :[Select Id, Linked_to_CR__c
                                    From Case
                                where (Id in:mapCaseChgC.keySet())])
                                {
                                    // If Case doesn't have Change Request linked
                                    if(caseRec.Linked_to_CR__c == null)
                                    {
                                        // Updat Change Request Lookup on Case with Change Request Id
                                        caseRec.Linked_to_CR__c = mapCaseChgC.get(caseRec.Id);
                                        updateCases.add(caseRec);                                       
                                    }                                                               
                                }
			if(updateCases != null && (!updateCases.isEmpty()) && updateCases.size() >0)
            {
                try
                {
                    update updateCases;
                }
                catch(System.Exception excep)
                {
                    System.debug('Cases Update Error'+ excep.getMessage());
                    //updateCases[0].addError('Case Update Error');
                    trigger.new[0].addError('Case Update Error');
                }
            }                                  
        }
    
    }
    
    if(trigger.isDelete)
    {
    	List<Case> updateCases = new List<Case>();
        
        for(SLX__Change_Control__c record: trigger.old)
        {
            if(record.Problem_Case_Nr__c != null)
            {               
                // Assumption is that one Case would be linked to only one Change Request
                // Adding to Map Case Id, Change Request Id
                mapCaseChgCDel.put(record.Problem_Case_Nr__c, record.Id);               
            }
        }       
    
        if(mapCaseChgCDel != null && (!mapCaseChgCDel.isEmpty()) && mapCaseChgCDel.size() >0)
        {
            for(Case caseRec :[Select Id, Linked_to_CR__c
                                    From Case
                                where (Id in:mapCaseChgCDel.keySet())])
                                {
                                    // If Case doesn't have Change Request linked
                                    if(caseRec.Linked_to_CR__c != null )
                                    {
                                        // Remove Change Request Lookup from Case
                                        caseRec.Linked_to_CR__c = null;
                                        //null;
                                        updateCases.add(caseRec);                                       
                                    }                                                               
                                }  
                                
			if(updateCases != null && (!updateCases.isEmpty()) && updateCases.size() >0)
            {
                try
                {
                    update updateCases;
                }
                catch(System.Exception excep)
                {
                    System.debug('Cases Update Error'+ excep.getMessage());
                    trigger.old[0].addError('Case Update Error');
                }
            }                                                               
                        
        }
        
    }
    
             
    
}