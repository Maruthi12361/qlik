/*****************************************************************************************************************
 Change Log:

20141127	CCE Initial development for CR# 19144 - https://eu1.salesforce.com/a0CD000000muaKy
				Copy the CreatedDate field on the QlikView Support Escalation record to 
				the "Escalation Date" field on the relevant Bug record 

******************************************************************************************************************/
trigger AddCreatedDateToBug on Qlikview_Support_Escalation__c (after insert, after update) {
	
	static final String DEBUGPREFIX = 'AddCreatedDateToBug: ';
	System.debug(DEBUGPREFIX + 'Starting');
	
	List<Qlikview_Support_Escalation__c> BugIds = new List<Qlikview_Support_Escalation__c>();
	List<Bugs__c> BugsToUpdate = new List<Bugs__c>();
	if(!Semaphores.TriggerHasRun('AddCreatedDateToBug')) {
		
		//we need to do the Query to get the CreatedDate
		List<Qlikview_Support_Escalation__c> QVSEList = new List<Qlikview_Support_Escalation__c>([Select Id, Bug_Id__c, CreatedDate From Qlikview_Support_Escalation__c where Id IN :Trigger.newMap.keySet()]);
		System.debug(DEBUGPREFIX + 'QVSEList: ' + QVSEList);
		//populate a map with the bug Id and Created Date values
		Map<String, Datetime> QVSEMap = new Map<String, Datetime>();
		for(Qlikview_Support_Escalation__c qse : QVSEList) QVSEMap.put(qse.Bug_Id__c, qse.CreatedDate);

		//Fetch a list of the Bugs we may need to update
		BugsToUpdate = [Select Id, Bug_Id__c, Escalation_date__c From Bugs__c where Bug_Id__c IN : QVSEMap.keySet()];
		System.debug(DEBUGPREFIX + 'BugsToUpdate: ' + BugsToUpdate);
		//copy over the CreatedDate field if required 
		for(Bugs__c btu : BugsToUpdate) {
            if (btu.Escalation_date__c == null) btu.Escalation_date__c = QVSEMap.get(btu.Bug_Id__c);
        }
        //and perform the update if neccessary
        if (BugsToUpdate.size() > 0) {
        	update BugsToUpdate;
        }
		
	}		
	System.debug(DEBUGPREFIX + 'Finishing');
}