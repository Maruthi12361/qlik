/*
Name:  EmailMessageAfterInsert.trigger
Copyright © 2011  salesforce.com consulting
======================================================
======================================================
Purpose:
-------
Case Milestone completion.

If EmailMessage is outbound and is the first to the customer then complete open First Response milestone.
If EmailMessage is outbound then complete open Customer Communication milestone.
======================================================
======================================================
History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     Mark Cane&  2011-10-06  Initial development.
1.1     Mark Cane&  2011-10-14  Added phrase matching.
1.2     CCE			2014-01-08  CR# 10130 - Fix EmailMessageAfterInsert trigger - Added a test to 
								check that cnt[0] is not null before trying to use the startWith function.
								The new test class is EntitlementTestSuite.caseContactHasNoEmail()
		ext_bad	    2018-04-18  Add check for ParentId
*/
trigger EmailMessageAfterInsert on EmailMessage (after insert) {
    Map<ID, String> caseEmails = new Map<ID, String>();

//    Entitlement_Settings__c es = Entitlement_Settings__c.getInstance();
//    String frPhrase = es.Email_Message_Phrase_First_Response__c;
//    String ccPhrase = es.Email_Message_Phrase_Communication__c;

    for (EmailMessage em : Trigger.new) {
        if (em.Incoming) continue;

        /* Commented by aaron 10/11/11 due to requirement in Test Log:
           Now, email case milestone completion does not rely on subject/body text
        if (em.TextBody!=null){
            if (em.TextBody.indexOf(frPhrase)>-1){
                caseEmails.put(em.parentId, 'FR');
            } else if (em.TextBody.indexOf(ccPhrase)>-1){
                caseEmails.put(em.parentId, 'CC');
            }
        } else if (em.HtmlBody!=null){
            if (em.HtmlBody.indexOf(frPhrase)>-1){
                caseEmails.put(em.parentId, 'FR');
            } else if (em.HtmlBody.indexOf(ccPhrase)>-1){
                caseEmails.put(em.parentId, 'CC');
            }
        }
        */

        // aaron added x lines below to complete commented text above
        // Not bulkified - pay attention later

        if (em.ParentId != null) {
            List<Case> cs = [select Id, ContactId from Case where Id = :em.ParentId limit 1];
            List<Contact> cnt = [select Id, Email from Contact where Id = :cs[0].ContactId limit 1];
            system.debug('aaron: ' + em.toAddress);
            if (!cnt.isEmpty() && (cnt[0].Email != null) && em.toAddress.startsWith(cnt[0].Email)) {    //CCE 2014-01-08 CR# 10130
                //if (!cnt.isEmpty() && em.toAddress.startsWith(cnt[0].Email)){

                caseEmails.put(em.parentId, 'FR');
            }
        }
        //caseEmails.put(em.parentId, 'FR'); was the only line original one
        // end of aaron additions
    }
    system.debug('aaron2: ' + caseEmails.size());
    if (caseEmails.size() == 0) return;


    CaseMilestoneUtils.completeMilestonesFromEmail(caseEmails);
}