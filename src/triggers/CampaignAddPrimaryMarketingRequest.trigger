/******************************************************

    Trigger: CampaignAddPrimaryMarketingRequest
    
    When the Primary Marketing Asset field on Campaign is populated create a Link Marketing Assets record 
    to link the Marketing Asset record in the Primary Marketing asset field to the Campaign
    
    Changelog:
        2018-03-01  CHG0033332 Initial creation
******************************************************/
trigger CampaignAddPrimaryMarketingRequest on Campaign (after insert, after update) {
    MAP<Id,Id> campaignAndPrimaryMarketingAsset = new MAP<Id,Id> ();

    System.debug('CampaignAddPrimaryMarketingRequest: Starting');
    if(!Semaphores.TriggerHasRun('CampaignAddPrimaryMarketingRequest'))
    {    
        //Create a Map of Campaign/Marketing Asset that we need to link together (create a Link Marketing Assets record for)
        for (Campaign cpgn: Trigger.new) {
            if (Trigger.isInsert) {
                if (cpgn.Primary_Marketing_Asset__c != null) {
                    campaignAndPrimaryMarketingAsset.put(cpgn.Id, cpgn.Primary_Marketing_Asset__c);
                    System.debug('CampaignAddPrimaryMarketingRequest: insert: ' + campaignAndPrimaryMarketingAsset);
                }
            }
            else {  //must be  an update
                Campaign cpgnOld = Trigger.oldMap.get(cpgn.Id);
                if((cpgn.Primary_Marketing_Asset__c != null) && (cpgn.Primary_Marketing_Asset__c != cpgnOld.Primary_Marketing_Asset__c)) {
                    campaignAndPrimaryMarketingAsset.put(cpgn.Id, cpgn.Primary_Marketing_Asset__c);
                    System.debug('CampaignAddPrimaryMarketingRequest: update: ' + campaignAndPrimaryMarketingAsset);
                }
            }
        }

        if (campaignAndPrimaryMarketingAsset.size() > 0) {
            //Check to make sure a Link Marketing Assets record does not already exist
            List<Link_Marketing_Asset__c> newLmaAdd = new List<Link_Marketing_Asset__c>();
            List<Link_Marketing_Asset__c> lma = [SELECT Campaign__c, Marketing_Asset_NEW__c
                FROM Link_Marketing_Asset__c
                WHERE isDeleted = false AND Campaign__c IN :campaignAndPrimaryMarketingAsset.keySet() AND Marketing_Asset_NEW__c IN :campaignAndPrimaryMarketingAsset.values()];
            System.debug('CampaignAddPrimaryMarketingRequest: lma: ' + lma);
            
            for (Campaign cpn: Trigger.new) {
                if (campaignAndPrimaryMarketingAsset.keySet().contains(cpn.Id) && campaignAndPrimaryMarketingAsset.values().contains(cpn.Primary_Marketing_Asset__c)) {
                    Link_Marketing_Asset__c newLma = new Link_Marketing_Asset__c(); 
                    newLma.Campaign__c = cpn.Id; 
                    newLma.Marketing_Asset_NEW__c = cpn.Primary_Marketing_Asset__c;
                    newLmaAdd.add(newLma);
                    System.debug('CampaignAddPrimaryMarketingRequest: newLmaAdd: ' + newLmaAdd);
                }
            }

            // insert any new Link Marketing Assets records
            if (newLmaAdd.size() > 0) {
                insert newLmaAdd;
                System.debug('CampaignAddPrimaryMarketingRequest: insert: newLmaAdd' + newLmaAdd);
            }

        }
    }
    System.debug('CampaignAddPrimaryMarketingRequest: Finishing');    
}