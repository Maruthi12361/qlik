/* *******  Trigger to stop the modification of 'Assigned ULC Level' record  *********
1: Stops end users from changing the Status of the QLIKBUY level to "Approved". API users and Admins should be able to do this change.

2: Stops the modification of the ULC Level field - it should not be changeable to anyone. (If a different Level needs - a new record needs to be created).

3: Stops the deletion of an Assigned ULC Level record with a ULC Level of Qlikbuy - it should only be able to be deleted by API users.

4: Calls Boomi to Send/Update the user when the Status of the Assigned ULC Level is changed. Trigger should also send a notification by email to the helpdesk letting them know there is a new partner to Approve.

Change Log:

    2014-07-09 MHG Fixed ONE Issue# 2633 and 2608
    2017-10-11 AYS BMW-393
    2018-08-01 CRW BMW-908
*/
trigger ULCLevelChangeControlTrigger on Assigned_ULC_Level__c (before delete,  before update, before insert, after insert, after update) {
    Assigned_ULC_Level__c NewLevel;
    Assigned_ULC_Level__c OldLevel;

    HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
    String customAPiUser = settings.Profile_Custom_API__c;
    String userProfileId = UserInfo.getProfileId();
    Boolean isHelpdesk = false;

    Boolean isAPIUser = (userProfileId == customAPiUser); /* API user profile*/
    Boolean isAdmin = settings.Sys_Admin_ProfileId__c.contains(userProfileId); /* Sys. admin profile*/
    If (settings.Profile_IT_HelpDesk_ID__c != null && settings.Profile_IT_HelpDesk_ID__c.contains(userProfileId)){//BMW-908
        isHelpdesk = true;/* Helpdesk profile*/
    }//BMW-908

    System.debug('Trigger.isUpdate : '+ Trigger.isUpdate + ' Trigger.isAfter : ' + Trigger.isAfter + 'Trigger.isDelete : ' + Trigger.isDelete);
    if (Trigger.isDelete && Trigger.isBefore) {

        //3: To stop the deletion of an Assigned ULC Level record with a ULC Level of Qlikbuy - it should only be able to be deleted by API users.
        for (Assigned_ULC_Level__c ulcLevel : [SELECT ULCLevelId__r.Name FROM Assigned_ULC_Level__c WHERE Id in:Trigger.old]) {
            System.debug('Cannot delete for ULC Level ' + ulcLevel.ULCLevelId__r.Name + ' isAPIUser: ' + isAPIUser);
            if (ulcLevel.ULCLevelId__r.Name == 'QLIKBUY' && !isAPIUser) {
                //ulcLevel.addError('A QlikBuy level cannot be deleted, please use the Status to control access.');
                Assigned_ULC_Level__c actualRecord = Trigger.oldMap.get(ulcLevel.Id);
                actualRecord.addError('A QlikBuy level cannot be deleted, please use the Status to control access.');
            }
        }
    }
    else if(!Trigger.isDelete){
        //For the case of Create and update

        List<Assigned_ULC_Level__c> ulcDetails = new List<Assigned_ULC_Level__c> ([SELECT Id,ContactId__r.Name, ContactId__c, ContactId__r.INT_NetSuite_InternalID__c FROM Assigned_ULC_Level__c WHERE Id in: Trigger.new]);
        List<ULC_Level__c> allULCLevels = new List<ULC_Level__c> ([SELECT Id, Name FROM ULC_Level__c WHERE IsDeleted = false]);
        Map<Id, ULC_Level__c> m_ulcLevels = new Map<Id, ULC_Level__c>();

        //We need the user id for ulc.contact ,, following code will extract user connected to contact.
        List<Id> ulcContactIds = new List<Id>();
        Map<Id,User> mapUlcUsers = new Map<Id, User>(); //Map of Contact Id -> User
        Set<Id> s_qlikbuyContactIds = new Set<Id>();

        //it would be better to refactor this part
        for(ULC_Level__c ulcLevel : allULCLevels) {
            m_ulcLevels.put(ulcLevel.id, ulcLevel);
        }

        for(Assigned_ULC_Level__c aul: ulcDetails){
            ulcContactIds.add(aul.ContactId__c);
            System.debug('Connected ULC user Id : ' + aul.ContactId__c);
        }
        //BMW-393 BEGIN
        for(Assigned_ULC_Level__c aul: Trigger.new){
            if(m_ulcLevels.get(aul.ULCLevelId__c).Name == 'QLIKBUY' ){
                if((Trigger.isAfter && Trigger.isInsert && aul.Status__c == 'Requested') ||
                    (Trigger.isBefore && Trigger.isUpdate && aul.Status__c != Trigger.oldMap.get(aul.id).Status__c && aul.Status__c == 'Requested')){
                    //ULC_Level__c need to be validated
                    s_qlikbuyContactIds.add(aul.ContactId__c);
                }
            }
        }

        if(!s_qlikbuyContactIds.isEmpty()){

            List<Contact> l_contacts = new List<Contact>([
                SELECT id, Portal_User_Active__c, Account_Type__c,
                    (SELECT id, ULCLevelId__c, ULCLevelId__r.Name, Status__c FROM Assigned_ULC_Levels__r),
                    (SELECT id, ULCStatus__c FROM ULC_Details__r )
                FROM Contact
                WHERE Id IN : s_qlikbuyContactIds
            ]);
                /*  Validation to prevent the QlikBuy ULC Level from being added to an SFDC Contact unless the following conditions are true:
                        1. ULC Detail, present and Active
                        2. PRMDT or PRMIND ULC Level is present and Approved
                        3. Contact has an Active Partner User   */
            Boolean isULCLevelPassValidation = false;
            Boolean isULCDetailPassValidation = false;
            Boolean isUserPassValidation = false;
            String errorMessage = '';
            for(Contact contact : l_contacts) {
                for(Assigned_ULC_Level__c assignedULCLevel : contact.Assigned_ULC_Levels__r) {
                    if((assignedULCLevel.ULCLevelId__r.Name == 'PRMDT' || assignedULCLevel.ULCLevelId__r.Name == 'PRMIND')
                        && assignedULCLevel.Status__c == 'Approved'){
                        isULCLevelPassValidation = true;
                        break;
                    }
                }
                if(!isULCLevelPassValidation) errorMessage += ' Approved PRMDT or PRMIND ULC Level is not present.';

                for(ULC_Details__c uLCDetail : contact.ULC_Details__r) {
                    if(uLCDetail.ULCStatus__c == 'Active'){
                        isULCDetailPassValidation = true;
                        break;
                    }
                }
                if(!isULCDetailPassValidation) errorMessage += ' Active ULC Detail is not present.';

                if(contact.Portal_User_Active__c == true && contact.Account_Type__c == 'Partner'){
                    isUserPassValidation = true;
                    //break;
                }
                if(!isUserPassValidation) errorMessage += ' Contact does not have an Active Partner User.';

                if(!String.isBlank(errorMessage)){
                    throw new CustomException(errorMessage);
                }
            }
        }
        //BMW-393 END

        for(User currUser : [SELECT Id, Name, ContactId FROM User WHERE IsActive = true AND ContactId in: ulcContactIds ]){
            mapUlcUsers.put(currUser.ContactId, currUser);
        }

        //Iterate new/updated records
        for (Integer i = 0; i< Trigger.new.size(); i++){

            String NewLevelName = '';
            String OldStatus = '';

            NewLevel = Trigger.New[i];
            if(Trigger.isUpdate) {
                OldLevel = Trigger.old[i];
                OldStatus = OldLevel.Status__c;
            }

            for(ULC_Level__c level : allULCLevels){
                if(level.Id == NewLevel.ULCLevelId__c){
                    NewLevelName = level.Name;
                }
            }

            if (Trigger.isBefore) {
                //1: Rule on the Assigned ULC Level that stops end users from changing the Status of the QLIKBUY level to "Approved".
                // API users and Admins should be able to do this change.

                System.debug('Current user profile :' + Userinfo.getProfileId() + ' NewLevelName : ' + NewLevelName + ' NewLevel.Status__c : ' + NewLevel.Status__c + ' OldLevel.Status__c: ' + OldStatus);
                if( !isAPIUser && !isAdmin && !isHelpdesk &&
                NewLevelName == 'QLIKBUY'&&
                NewLevel.Status__c <> OldStatus &&
                NewLevel.Status__c == 'Approved'
                ){
                    NewLevel.addError('QLIKBUY level can only be approved by Helpdesk in NetSuite, Helpdesk will be notified by setting this status to Requested');
                }

                //2:There needs to be a validation rule created to stop the modification of the ULC Level field -
                //it should not be changeable to anyone. (If a different Level needs - a new record needs to be created).
                if(Trigger.isUpdate && OldLevel.ULCLevelId__c != null && NewLevel.ULCLevelId__c <> OldLevel.ULCLevelId__c){
                    NewLevel.addError('A QlikBuy level cannot be converted to any other level, please use the Status to control access');
                }
            }
            else if(Trigger.isAfter){
                //4: Call Boomi to Send/Update the user when the Status of the Assigned ULC Level is changed.
                //Also send a notification by email to the helpdesk letting them know there is a new partner to Approve (the Partner name should be in the notification).

                if(NewLevel.Status__c == 'Requested' && NewLevel.Status__c != OldStatus){

                    //Get contact name, other info from the ulcDetails original record
                    String NewContactName = '', NewUserId = '', ContactNSId = '', SFDCContactId = '';

                    for(Assigned_ULC_Level__c ulcDetail : ulcDetails){
                        if(ulcDetail.Id == NewLevel.Id){
                            NewContactName = ulcDetail.ContactId__r.Name;
                            ContactNSId = ulcDetail.ContactId__r.INT_NetSuite_InternalID__c;
                            if(mapUlcUsers.containsKey(ulcDetail.ContactId__c)) {
                                NewUserId =  mapUlcUsers.get(ulcDetail.ContactId__c).Id;
                                SFDCContactId = ulcDetail.ContactId__c;
                            }
                        }
                    }
                    System.debug('For ULC Contact : '+ NewContactName +' Contact Netsuite Id :'+ ContactNSId+' Mapped user :' + NewUserId);

                    //If user is not present in Netsuite then create user/employee in netsuite before creating partner
                    if( (ContactNSId == null || ContactNSId == '' ) && NewUserId != ''){
                        //Send boomi message to Update partner in Netsuite
                        String employeeMessage = '<Record><ID>' + NewUserId + '</ID></Record>';
                        BoomiUtil.updateBoomiStatus(NewUserId);
                        BoomiUtil.callBoomi(NewLevel.Id, 'createNSEmployee', employeeMessage);
                        System.debug('Boomi message for createNSEmployee : ' + employeeMessage);
                    }

                    //Send boomi message to Update partner in Netsuite
                    String contactMessage = '<Record type="Contact" environment="' + UserInfo.getOrganizationId() + '"><ID>' + NewLevel.ContactId__c + '</ID></Record>';
                    BoomiUtil.updateBoomiStatus(NewLevel.Id);
                    BoomiUtil.callBoomi(NewLevel.Id, 'updateEmpPartnerVerStatus', contactMessage);



                    //Send an email to help desk
                    QTCustomSettings__c settings = QTCustomSettings__c.getValues('Default');

                    if(settings.Helpdesk_Email__c != null && settings.Helpdesk_Email__c != ''){
                        System.debug(' NewLevel.ContactId__c : '+ NewLevel.ContactId__c + ' NewLevel.ContactId__r.Name : ' + NewContactName);
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setToAddresses(new String[] {settings.Helpdesk_Email__c});
                        mail.setSubject('New Partner Approval required ' );
                        mail.setPlainTextBody('New Partner Approval required for ' + NewContactName +'.\nPartner Contact: ' +
                                                URL.getSalesforceBaseUrl().toExternalForm() + '/' + SFDCContactId + '\n\n' +
                                                'Please get approval for the Partner Account owner before setting the NS user to Verified');
                        mail.setHtmlBody('New Partner Approval is required for new partner :<b> <a href="' +
                                                URL.getSalesforceBaseUrl().toExternalForm() + '/' + SFDCContactId +
                                                '">' + NewContactName +'</a></b><br/><br/>' +
                                                'Please get approval for the Partner Account owner before setting the NS user to Verified'); //+ ' View case <a href=https://na1.salesforce.com/'+case.Id+'>click here</a>');
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }
                }
                /*****/
            }
        }
    }

}