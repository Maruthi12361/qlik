/******************************************************

    Trigger: OnChange_ULCDetails
    
    Initiator: Project ULCv3 
    
    Changelog:
        2009-12-04  MHG     Created file    
        2010-09-09  MHG     CR #152 ULC Rollup summary field        
                            https://emea.salesforce.com/a0C2000000ALlrw
        2016-02-01  SAN     ULC-339 update lead/campaign from cloud
        2016-11-06  BAD     CR# 97533 - Relayware
        2017-12-28  NAD     IT-138
        2018-02-13  CRQ     QE-1874
******************************************************/
trigger OnChange_ULCDetails on ULC_Details__c (after insert, after update, before insert, before update) 
{
    
    ULC ulc_helper = new ULC();
    
    if (Trigger.isBefore && Trigger.isUpdate)
    {
        ULC.CheckChangeUsername(Trigger.new, Trigger.old);
        ULC.updateULCfromHeroku(Trigger.new);
        ULC.ContactLeadRelayware(Trigger.new, Trigger.old);
    }
     
    ulc_helper.HandleStatusChanges(Trigger.New, Trigger.isBefore, Trigger.oldMap);

    if (Trigger.isAfter && Trigger.isUpdate)
    {
        Set<Id> sets = new Set<Id>();
        
        for(integer i=0; i < Trigger.New.size(); i++)
        {
            if (Trigger.New[i].ContactId__c != null) 
            {
                if (Trigger.New[i].ULCName__c != Trigger.Old[i].ULCName__c || Trigger.New[i].ULC_Password__c != Trigger.Old[i].ULC_Password__c || Trigger.New[i].ULCStatus__c == 'Disabled')
                {
                    sets.Add(Trigger.New[i].ContactID__c);
                }               
            }
        }

        if (sets.size() > 0)
        {       
            List<Contact> Contacts = new List<Contact>();
            for(Contact contact : [select Id, Allow_Partner_Portal_Access__c, TriggerPortalControl__c from Contact where Id in :sets])
            {
                if (contact.Allow_Partner_Portal_Access__c)
                {
                    contact.TriggerPortalControl__c = true;
                    Contacts.Add(contact);
                }
            }
            
            if (Contacts.size() > 0)
            {
                update Contacts;
            }
        }
    }
    
    if (Trigger.isAfter)
    {
        Set<Lead> LeadsToUpdate = new Set<Lead>();
        Set<Contact> ContactsToUpdate = new Set<Contact>();
        for (ULC_Details__c Detail : Trigger.New)
        {
            System.debug('M ' + Detail.ContactId__r.Id);            
        }
        //QE-1874 replacing Detail.Id with Detail.qlikid__c
        for (ULC_Details__c Detail : [select Id, qlikid__c, ULCStatus__c, ContactId__r.Id, ContactId__r.ActiveULC__c, ContactId__r.QlikID__C, LeadId__r.Id, LeadId__r.ActiveULC__c, LeadId__r.QlikID__C from ULC_Details__c where Id in :Trigger.New])
        {                     

            if (Detail.ContactId__r != null)
            {                     

                Contact C = Detail.ContactId__r;                
                //if (((Detail.ULCStatus__c == 'Active' ? true : false) != C.ActiveULC__c || String.valueOf(Detail.Id) != String.valueOf(C.QlikID__C)) && !ContactsToUpdate.contains(C))
                if ((Detail.ULCStatus__c == 'Active' ? true : false) != C.ActiveULC__c && !ContactsToUpdate.contains(C))
                { 
                    C.ActiveULC__c = !C.ActiveULC__c;
                    if(Detail.ULCStatus__c == 'Active') C.QlikID__C = Detail.qlikid__c;//QE-1874 replacing Detail.Id with Detail.qlikid__c
                    //C.QlikID__C = Detail.Id;
                    ContactsToUpdate.Add(C);
                }
                else if (Detail.ULCStatus__c == 'Active' && (String.valueOf(Detail.qlikid__c) != String.valueOf(C.QlikID__C)) && !ContactsToUpdate.contains(C))
                {
                    C.QlikID__C = Detail.qlikid__c;//QE-1874 replacing Detail.Id with Detail.qlikid__c
                    ContactsToUpdate.Add(C);
                }
            }
            else if (Detail.LeadId__r != null)
            {
                Lead L = Detail.LeadId__r;
                System.debug('OnChange_ULCDetails: Lead - ' + Detail.ULCStatus__c + ' - ' + L.ActiveULC__c);
                if (((Detail.ULCStatus__c == 'Active' ? true : false) != L.ActiveULC__c || String.valueOf(Detail.qlikid__c) != String.valueOf(L.QlikID__C)) && !LeadsToUpdate.contains(L))
                {
                    L.ActiveULC__c = !L.ActiveULC__c;
                    L.QlikID__C = Detail.qlikid__c;//QE-1874 replacing Detail.Id with Detail.qlikid__c
                    System.debug('OnChange_ULCDetails: Lead updating to ' + L.ActiveULC__c);
                    LeadsToUpdate.Add(L);
                }               
            }
        }
        
        if (ContactsToUpdate.size() > 0)
        {
            update new List<Contact>(ContactsToUpdate);     
        }
        
        System.debug('OnChange_ULCDetails: About to update ' + LeadsToUpdate.size() + ' leads');
        if (LeadsToUpdate.size() > 0)
        {           
            update new List<Lead>(LeadsToUpdate);
        }
    }
}