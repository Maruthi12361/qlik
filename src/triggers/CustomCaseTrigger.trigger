/*
 File Name: CustomCaseTrigger

 2018-01-01 Viktor@4Front QCW-4612, QCW-4761 Trigger consolidation.
 2018-01-24  ext_vos CHG0030671: add count all cases for related Bugs.
 2018-05-10  ext_bad CHG0032643  Add email to previous AssignedTo user.
 2018-05-22  ext_vos CHG0033970: add simplify Partner Performance data collection for 'QlikTech Master Support Record Type'.
 2018-09-26  ext_vos Remove CustomCaseTriggerHandler.handleCaseTrigger method (included duplicated methods).
                     Remove duplicated methods (already called in CustomCaseTriggerHandler): 
                         assignPartnerSuppContactToCase, updateCasesFromAcntLicenseViaEntitlement, PopulateCase, updateAccountLicenseTechInfo, 
                         handleCaseEntitlementProcessSync, handleCaseEndUserAccountNameCopy
 2019-12-12 extbad IT-2329 Block Closed Attunity Cases changes for External user
*/
trigger CustomCaseTrigger on Case (
        before insert, before update,
        after insert, after update,
        before delete, after delete) {
// add necessary events when it will be needed - , after undelete
    if (Trigger.isInsert) { // Insert
        if (Trigger.isBefore) { // before Insert
            if (!Semaphores.CustomCaseTriggerBeforeInsert) {
                Semaphores.CustomCaseTriggerBeforeInsert = true;

                CustomCaseTriggerHandler.handleCaseBeforeInsert(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Calling environment snapshot logic on insert and update methods
                CaseEnvironmentMappingHandler.onBeforeInsert(trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger QS_SetCaseOwner on Case (before insert)
                QS_CaseTriggerHandler.updateCaseOwner(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger CasePopulateSupportEmailFields on Case (before insert, before update)
                CustomCaseTriggerHandler.handleCasePopulateSupportEmailFields(Trigger.new, null);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger CaseBusinessCritical on Case (before insert, after insert, after update)
                //Due code analisys it used only before insert!
                CaseBusinessCriticalHandler.handleCaseBusinessCritical(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger CaseSalesOps on Case (before insert)
                CaseSalesOpsHandler.handleCaseSalesOps(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            }
            //FIXME double run
            //Moved from trigger CaseQuickClose on Case (before insert, before update)
            CustomCaseTriggerHandler.handleCaseQuickClose(Trigger.new, null);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

        } else if (Trigger.isAfter) { // after Insert
            if (!Semaphores.CustomCaseTriggerAfterInsert) {
                Semaphores.CustomCaseTriggerAfterInsert = true;

                //Moved from trigger CaseSendEmailToDSE on Case (after insert)
                system.debug(LoggingLevel.DEBUG,'@@@sendEmailsToDse Running');
                CaseSendEmailToDSE.sendEmailsToDse(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger Case_Sharing on Case (after insert, after update)
                CaseSharingHandler.handlerCaseSharing(Trigger.new, null);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                CaseDetectLanguageHandler.handleCaseDetectLanguage(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger CaseBusinessCriticalSupportTeamSetup on Case (after insert)
                //CaseBusinessCriticalSupTeamSetupHandler.handleCaseBusinessCriticalSupportTeamSetup(Trigger.new);
                //System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                // 2018-01-24  ext_vos CHG0030671: add count all cases for related Bugs.
                CaseServices.countForBugs(Trigger.new, Trigger.old);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            }
            //FIXME double run
            //Moved from trigger CaseClosedSendSurvey on Case (after update, after insert)
            CaseClosedSendSurveyHandler.handleCaseClosedSendSurvey(Trigger.new, null);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            CaseToCRHandler.handleCaseToCR(Trigger.new,null);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            //Moved from trigger QS_CaseEnvironmentAssociation_Tgr on Case (after insert, after update, after delete)
            //Trigger to update the associated environment with last used date which is the created date from the latest case related to the environment.
            QS_CaseTriggerHandler.updateEnvironmentLastUsedDate(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
        }
    } else if (Trigger.isUpdate) {// Update
        if (Trigger.isBefore) { // before Update
            if (!Semaphores.CustomCaseTriggerBeforeUpdate) {
                Semaphores.CustomCaseTriggerBeforeUpdate = true;

                CustomCaseTriggerHandler.handleCaseBeforeUpdate(Trigger.new, Trigger.old, Trigger.oldMap);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Calling environment snapshot logic on insert and update methods
                CaseEnvironmentMappingHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger CasePopulateSupportEmailFields on Case (before insert, before update)
                CustomCaseTriggerHandler.handleCasePopulateSupportEmailFields(Trigger.new, null);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //CHG0033970
                CaseServices.setPartnerPerformanceSection(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                CaseClosedAttunityHandler.handleCases(Trigger.newMap, Trigger.oldMap);
            }
            //FIXME double run
            //Moved from trigger CaseQuickClose on Case (before insert, before update)
            CustomCaseTriggerHandler.handleCaseQuickClose(Trigger.new, Trigger.oldMap);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

        } else if (Trigger.isAfter) { // after Update
            if (!Semaphores.CustomCaseTriggerAfterUpdate) {
                Semaphores.CustomCaseTriggerAfterUpdate = true;

                //Moved from trigger Case_Sharing on Case (after insert, after update)
                CaseSharingHandler.handlerCaseSharing(Trigger.new, Trigger.old);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                CaseDetectLanguageHandler.handleCaseDetectLanguage(Trigger.new);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger CaseClosedSendSurvey on Case (after update, after insert)
                CaseClosedSendSurveyHandler.handleCaseClosedSendSurvey(Trigger.new, Trigger.oldMap);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //Moved from trigger CaseUpdateSupportContact on Case(after update, after delete)
                CaseUpdateSupportContactHandler.handleCaseUpdateSupportContact(Trigger.new, Trigger.old);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                //2018-01-24  ext_vos CHG0030671: add count all cases for related Bugs.
                CaseServices.countForBugs(Trigger.new, Trigger.old);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                CaseAssignedToChangedHandler.emailPreviousAssignedUser(Trigger.newMap, Trigger.oldMap);
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
            }
            //FIXME
            CaseToCRHandler.handleCaseToCR(Trigger.new,Trigger.old);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            //Moved from trigger QS_CaseEnvironmentAssociation_Tgr on Case (after insert, after update, after delete)
            //Trigger to update the associated environment with last used date which is the created date from the latest case related to the environment.
            QS_CaseTriggerHandler.updateEnvironmentLastUsedDate(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

        }
    } else if (Trigger.isDelete) { // Delete
        if (Trigger.isBefore) { // before Delete
            //TODO EXECUTE
        } else if (Trigger.isAfter) { // after Delete
            //Moved from trigger QS_CaseEnvironmentAssociation_Tgr on Case (after insert, after update, after delete)
            //Trigger to update the associated environment with last used date which is the created date from the latest case related to the environment.
            QS_CaseTriggerHandler.updateEnvironmentLastUsedDate(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete);
            //Moved from trigger CaseUpdateSupportContact on Case(after update, after delete)
            CaseUpdateSupportContactHandler.handleCaseUpdateSupportContact(Trigger.new, Trigger.old);
        }
//        } else if (Trigger.isUnDelete) { // Undelete
        //TODO EXECUTE
    }

}