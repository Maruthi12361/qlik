/*
    Name:  UpdateISAssignmentToDate.trigger


    Purpose:
    -------
    CR# 5949 #dev Add recurrant assignment functionality + fix the assigned to date problem
    Calculate Assigned To date from various input fields including days of week
    
    History
    ------- 
    VERSION AUTHOR      DATE        DETAIL
    1.0     CCE         2012-11-21  Initial development.
									    
*/
trigger UpdateISAssignmentToDate on IS_Assignment__c (before insert, before update) {
    System.debug('UpdateISAssignmentToDate: starting');
    
	Private Date startDate = date.newInstance(1900, 1, 7);  //Year,month,day (a Sunday)
    Set<integer> iDaysOfWeek = new Set<integer>();
    integer iDayCount = 0;
    integer iLoop = 0;
     
    for (IS_Assignment__c i : Trigger.new)
    {
    	//check Assigned_From__c and Assigned_Days__c are not blank
    	if ((i.Assigned_From__c == null) || (i.Assigned_Days__c == null) || (i.Assigned_Days__c == 0)) continue;
    	
    	iDaysOfWeek.clear();
    	iDayCount = 0;
    	iLoop = 0;    	
    	
    	//create a Set to hold the selected days of the week 
    	string sDaysAssigned = '';   	
    	integer DaysOfWeek = 0;
    	if (i.Sunday__c == true) { iDaysOfWeek.add(0); sDaysAssigned += 'Su,'; }
    	if (i.Monday__c == true) { iDaysOfWeek.add(1); sDaysAssigned += 'Mo,'; }
    	if (i.Tuesday__c == true) { iDaysOfWeek.add(2); sDaysAssigned += 'Tu,'; }
    	if (i.Wednesday__c == true) { iDaysOfWeek.add(3); sDaysAssigned += 'We,'; }
    	if (i.Thursday__c == true) { iDaysOfWeek.add(4); sDaysAssigned += 'Th,'; }
    	if (i.Friday__c == true) { iDaysOfWeek.add(5); sDaysAssigned += 'Fr,'; }
    	if (i.Saturday__c == true) { iDaysOfWeek.add(6); sDaysAssigned += 'Sa,'; }
    	System.debug('UpdateISAssignmentToDate: iDaysOfWeek.size()=' + iDaysOfWeek.size());
    	
    	i.Days_Assigned__c = sDaysAssigned.removeEnd(',');    	    	
    	
    	//if no days have been selected set default to everyday except weekends (Sat & Sun)
    	if (iDaysOfWeek.size() == 0)
    	{
    		iDaysOfWeek.add(1);
    		sDaysAssigned += 'Mo,';
    		iDaysOfWeek.add(2);
    		sDaysAssigned += 'Tu,';
    		iDaysOfWeek.add(3);
    		sDaysAssigned += 'We,';
    		iDaysOfWeek.add(4);
    		sDaysAssigned += 'Th,';
    		iDaysOfWeek.add(5);
    		sDaysAssigned += 'Fr,';
    		i.Days_Assigned__c = sDaysAssigned.removeEnd(',');    		
    	}
    	
    	//Get the Assigned From date as a 0-6 value
    	double remainder = Math.mod(startDate.daysBetween(i.Assigned_From__c) , 7);
    	//System.debug('UpdateISAssignmentToDate: remainder=' + remainder);
    	//and convert it to an integer
    	integer iAssignedStart = remainder.intValue();
    	System.debug('UpdateISAssignmentToDate: iAssignedStart=' + iAssignedStart);
    	
    	//loop through till our iLoop counter has reached the required number of days
    	while (iLoop < i.Assigned_Days__c)
    	{
    		//check if the current day position is in the list of valid days
    		//increment the loop counter if it is
    		if (iDaysOfWeek.contains(iAssignedStart)) iLoop++;
	    	//increment our current day position 
	    	iAssignedStart = (++iAssignedStart > 6) ? 0 : iAssignedStart;
	    	//and our day counter	    	
	    	iDayCount++;
    	}
    	System.debug('UpdateISAssignmentToDate:FINAL iAssignedStart=' + iAssignedStart + ' iLoop=' + iLoop + ' iDayCount' + iDayCount);
	    //update our target field
	    i.Assigned_To__c = i.Assigned_From__c.addDays(iDayCount-1);
    }
	
    System.debug('UpdateISAssignmentToDate: finishing');
}