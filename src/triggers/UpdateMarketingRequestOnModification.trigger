/****************************************************************************************************

    UpdateMarketingRequestOnModification
    
    Trigger to update Marketing Request due to actions on Marketing Deliverys   
        
    Changelog:
        2011-05-31  MHG     Created method
                            CR #2211 - Approval trigger in Marketing Request portal
                            https://eu1.salesforce.com/a0CD000000EUyqr?srPos=1&srKp=a0C
        2011-12-15  MHG     Added Semaphore lock to stop Workflow from firing twice
                            CR #3660 - #Quick Marketing Request complete status trigger
                            https://eu1.salesforce.com/a0CD000000FytBn
        2019-05-28  CCE     Deactivated - CHG0036187 BMW-1517 MD Status                    
****************************************************************************************************/
trigger UpdateMarketingRequestOnModification on Marketing_Deliverable__c (after delete, after insert, after update) 
{

    if (Semaphores.UpdateMarketingRequestTriggerHasRun)
    {
        return;
    }
    Semaphores.UpdateMarketingRequestTriggerHasRun = true;

    Set<string> MarketingRequestIDs = new Set<string>();
    Set<string> ApprovalStatusChangedIDs = new Set<string>();

    if (trigger.isUpdate || trigger.isInsert) 
    {
        // Only add ID's where Requested_Completion_Date__c is changed
        for (integer i = 0; i < Trigger.new.size(); i++)
        {
            if (trigger.isInsert || Trigger.new[i].Requested_Due_Date__c != Trigger.old[i].Requested_Due_Date__c)
            {
                if (!MarketingRequestIDs.contains(Trigger.new[i].Marketing_Request__c))
                {
                    MarketingRequestIDs.add(Trigger.new[i].Marketing_Request__c);
                }
            }       
            if (trigger.isInsert || Trigger.new[i].Approval_Status__c != Trigger.old[i].Approval_Status__c)
            {
                if (!ApprovalStatusChangedIDs.contains(Trigger.new[i].Marketing_Request__c))
                {
                    ApprovalStatusChangedIDs.add(Trigger.new[i].Marketing_Request__c);
                }
            }
        }
    }
    else 
    {
        for (integer i = 0; i < Trigger.old.size(); i++)
        {
            if (!MarketingRequestIDs.contains(Trigger.old[i].Marketing_Request__c))
            {
                MarketingRequestIDs.add(Trigger.old[i].Marketing_Request__c);
            }
            if (!ApprovalStatusChangedIDs.contains(Trigger.old[i].Marketing_Request__c))
            {
                ApprovalStatusChangedIDs.add(Trigger.old[i].Marketing_Request__c);
            }           
        }           
    }
    
    if (MarketingRequestIDs.isEmpty() && ApprovalStatusChangedIDs.isEmpty())
    {
        return;
    }
    
    Map<string, date> MaxRequestDueDate = new Map<string, date>();
    Map<string, boolean> DeliverysAppovalStatuses = new Map<string, boolean>();
    for (Marketing_Deliverable__c MDc : [select Id, Marketing_Request__c, Approval_Status__c, Requested_Due_Date__c, Marketing_Request__r.Date_Closed__c from Marketing_Deliverable__c where Marketing_Request__c in :MarketingRequestIDs or Marketing_Request__c in :ApprovalStatusChangedIDs])
    {
        if (MarketingRequestIDs.contains(MDc.Marketing_Request__c))
        {
            if (MDc.Requested_Due_Date__c != null) 
            {
                if (MaxRequestDueDate.containsKey(MDc.Marketing_Request__c))
                {
                    if (MaxRequestDueDate.get(MDc.Marketing_Request__c) < MDc.Requested_Due_Date__c)
                    {
                        MaxRequestDueDate.put(MDc.Marketing_Request__c, MDc.Requested_Due_Date__c);
                    }
                }
                else
                {
                    MaxRequestDueDate.put(MDc.Marketing_Request__c, MDc.Requested_Due_Date__c);
                }
            }   
        }
        if (ApprovalStatusChangedIDs.contains(MDc.Marketing_Request__c))
        {           
            if ((MDc.Approval_Status__c == 'Completed' || MDc.Approval_Status__c == 'Recalled' || MDc.Approval_Status__c == 'Denied') 
                && MDc.Marketing_Request__r.Date_Closed__c == null)
            {
                if (!DeliverysAppovalStatuses.containsKey(MDc.Marketing_Request__c))
                {
                    DeliverysAppovalStatuses.put(MDc.Marketing_Request__c, true);
                }
            }
            else
            {
                DeliverysAppovalStatuses.put(MDc.Marketing_Request__c, false);
            }       
        }
    }
    
    Set<string> AllMRIDs = new Set<string>();
    AllMRIDs.addAll(MarketingRequestIDs);
    AllMRIDs.addAll(ApprovalStatusChangedIDs);
    List<Marketing_Request__c> ToUpdate = new List<Marketing_Request__c>();
    for (string MRcId : AllMRIDs)
    {
        if (MRcId == null)
        {
            continue;
        }
        Marketing_Request__c MRc = new Marketing_Request__c(Id = MRcId);
        if (MarketingRequestIDs.contains(MRcID))
        {
            if (MaxRequestDueDate.containsKey(MRcID))
            {
                MRc.Requested_Completion_Date__c = MaxRequestDueDate.get(MRcID);
            }
            else
            {
                MRc.Requested_Completion_Date__c = Date.today().addDays(3);
            }
        }
        if (ApprovalStatusChangedIDs.contains(MRcID))
        {
            if (DeliverysAppovalStatuses.containsKey(MRcID))
            {
                if (DeliverysAppovalStatuses.get(MRcID) == true)
                {
                    MRc.Approval_Status__c = 'Completed';
                    MRc.Date_Closed__c = Date.today();
                }
                else
                {
                    MRc.Approval_Status__c = 'New';
                    MRc.Date_Closed__c = null;
                }
            }
            else // Was delete
            {
                MRc.Approval_Status__c = 'New';
                MRc.Date_Closed__c = null;
            }
        }       
        ToUpdate.add(MRc);      
    }
    
    if (!ToUpdate.isEmpty())
    {
        update(ToUpdate);
    }

}