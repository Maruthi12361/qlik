/******************************************************************
Name: OperationsRequestBeforeInsertTrigger
for P0212 - Operations Request Portal Enhancements Project
https://projects.qliktech.com/sites/projects/QSMRequestPortal/default.aspx

2012-11-08	TJG	Initial creation. Default the approval status to 'New'	
*******************************************************************/
trigger OperationsRequestBeforeInsertTrigger on Marketing_Request__c (before insert) {
	for (Marketing_Request__c orequest : Trigger.new) {
		if (orequest.Approval_Status_QSM__c == null || orequest.Approval_Status_QSM__c == '')
		orequest.Approval_Status_QSM__c = 'New';
	}
}