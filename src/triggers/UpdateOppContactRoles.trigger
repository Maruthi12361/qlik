/*
 Author - Jina Chetia
 Company - Bluewolf
 Date - 09/09/2009
*/
trigger UpdateOppContactRoles on Sphere_of_Influence__c (after delete, before insert, before update) 
{
	Map<Id,OpportunityContactRole> updateOppContactRoles = new Map<Id,OpportunityContactRole>();
	List<OpportunityContactRole> insertOppContactRoles = new List<OpportunityContactRole>();
	List<OpportunityContactRole> deleteOppContactRoles = new List<OpportunityContactRole>();
	List<OpportunityContactRole> contactRoleList = new List<OpportunityContactRole>();
	Map<Id,OpportunityContactRole> updateMap = new Map<Id,OpportunityContactRole>();
	List<Id> contactList = new List<Id>();
	List<Id> oppList = new List<Id>();
	List<Sphere_of_Influence__c> sphereList = new List<Sphere_of_Influence__c>();
	
	//If the SOI is either inserted or updated
	if(Trigger.isInsert || Trigger.IsUpdate)
	{
		for(Sphere_of_Influence__c sph: Trigger.new)
		{
			//Create or update OpportunityContactRole only if they are not being created from Lead Conversion
			if(!sph.From_Lead__c)
			{
				oppList.add(sph.Opportunity__c);
				contactList.add(sph.Contact__c);
			    sphereList.add(sph);	
			}
		}
		//Retrieve all existing OpportunityContactRoles for the combination of Opportunity and Contact of 
		//triggered record
		for(OpportunityContactRole oppContactRole: [select Id, ContactId, Role, OpportunityId 
			from OpportunityContactRole where OpportunityId IN :oppList and ContactId IN :contactList])
		{
			contactRoleList.add(oppContactRole);
		}
		//Update the Role of the existing OpportunityContactRole record or create a new one
		for(Sphere_of_Influence__c sphere: sphereList)
		{
			Boolean flag = false;
			//Extract only the first role from the Sphere of Influence's Role field as it is 
			// a multi-select picklist
			String role = sphere.Role__c;
			if(role.contains(';'))
			{
				role = role.split(';')[0];
			}
			//If OpportunityContactRole exist for the same combination of Oppty and Contact as in the 
			//SOI record, update the existing OpportunityContactRole
			for(OpportunityContactRole updateOpp: contactRoleList)
			{
				if(updateOpp.ContactId == sphere.Contact__c && 
					updateOpp.OpportunityId == sphere.Opportunity__c)
				{
					updateOpp.Role = role;
					flag = true;
					updateOppContactRoles.put(updateOpp.Id, updateOpp);
					break;
				}
			}
			//Or else create a new OpportunityContactRole
			if(!flag)
			{
			    if(!insertOppContactRoles.isEmpty())
			    {
			    	for(Integer i=0; i<insertOppContactRoles.size(); i++)
			    	{		
			    		if(insertOppContactRoles[i].ContactId == sphere.Contact__c &&
			    			insertOppContactRoles[i].OpportunityId == sphere.Opportunity__c)
			    	   {
			    	   	   insertOppContactRoles.remove(i);
			    	   	   break;
			    	   }
			    	}
			    }
			    
				OpportunityContactRole newOppContactRole 
					= new OpportunityContactRole(ContactId = sphere.Contact__c, 
						OpportunityId= sphere.Opportunity__c, Role= role);
			    insertOppContactRoles.add(newOppContactRole);
			}
		}
		//Insert OpportunityContactRole records if not empty
		if(!insertOppContactRoles.isEmpty())
		{
			insert insertOppContactRoles;
		}
		//Update the OpportunityContactRole records if not empty
		if(!updateOppContactRoles.isEmpty())
		{		
			update updateOppContactRoles.values();
		}
	}
	//This is being called when delete action is performed on SOI. 
	if(Trigger.isDelete)
	{
		Map<String, Sphere_of_Influence__c> soiMap = new Map<String,Sphere_of_Influence__c>();
		Map<String, OpportunityContactRole> oppContRoleMap = new Map<String, OpportunityContactRole>();
		List<Id> sphOfInfIds = new List<Id>();
		
		for(Integer i=0; i<Trigger.old.size(); i++)
		{
			oppList.add(Trigger.old[i].Opportunity__c);
			contactList.add(Trigger.old[i].Contact__c);
			sphOfInfIds.add(Trigger.old[i].Id);
		}
		//Retrieve all the SOI records for the same combination of Oppty and Contact as in the deleted record 
		//excluding the deleted record
		for(Sphere_of_Influence__c sphere: [select Id, LastModifiedDate, Opportunity__c, Contact__c, Role__c from Sphere_of_Influence__c
			where Opportunity__c IN :oppList and Contact__c IN :contactList and Id NOT IN :sphOfInfIds])
		{
			String key = String.valueOf(sphere.Opportunity__c) + String.valueOf(sphere.Contact__c);
			if(soiMap.containsKey(key))
			{
				if(sphere.LastModifiedDate > soiMap.get(key).LastModifiedDate)
				{
					soiMap.put(key, sphere);
				}
			}
			else
			{
				soiMap.put(key, sphere);
			}		
		}
		//If there are exist other SOI records for the same Contact
		//in the Opportunity and the role of OpportunityContactRole record is same as the deleted record, 
		//update the Role in OpportunityContactRole from the Last Update SOI record in the Opportunity for that Contact.
		// If there are no other SOI records for the same Contact in the Opportunity, delete the OpportunityContactRole record
		for(OpportunityContactRole oppCont: [select Id, ContactId, Role, OpportunityId 
				from OpportunityContactRole where OpportunityId IN :oppList and ContactId IN :contactList])
		{
			String pKey = String.valueOf(oppCont.OpportunityId)+String.valueOf(oppCont.ContactId);
			oppContRoleMap.put(pKey,oppCont);
			
		}
		for(Integer i=0; i<Trigger.old.size(); i++)
		{
			String pKey = String.valueOf(Trigger.old[i].Opportunity__c)+ String.valueOf(Trigger.old[i].Contact__c);
			if(soiMap.containsKey(pKey))
			{
				OpportunityContactRole upCont = oppContRoleMap.get(pKey);
				if (soiMap.get(pKey).Role__c != null)			// MHG 10-09-01 - Moved null check to here instead
				{
					if(soiMap.get(pKey).Role__c.contains(';'))	// MHG 10-08-09 - Added null check here
					{
						upCont.Role = soiMap.get(pKey).Role__c.split(';')[0];
					}
					else
					{
						upCont.Role = soiMap.get(pKey).Role__c;
					}				
				}	
				updateMap.put(upCont.Id, upCont);				
			}
			else
			{
				deleteOppContactRoles.add(oppContRoleMap.get(pKey));
			}			
		}
		if(!updateMap.isEmpty())
		{
			update updateMap.values();
		}
		Database.DeleteResult[] DR_Dels = Database.delete(deleteOppContactRoles);
		
	}
}