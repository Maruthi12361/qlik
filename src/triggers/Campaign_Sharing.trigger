/***************************************************
Trigger: Campaign_Sharing
Object: Campaign
Description: When a campaign is created (and "Partner 1" field (Primary_Partner_Involved__c) is populated) or
             "Partner 1" field has been modified we need to call sharing handler
Change Log:
 2016-03-04 CCE CR# 78524 https://eu1.salesforce.com/a0CD000000xSfTV
            Create Partner Sharing on Marketing Campaigns
******************************************************/
trigger Campaign_Sharing on Campaign (after insert, after update) {
    System.debug('Campaign_Sharing Trigger Starting');
    
    List<Id> camIds = new List<Id>();
    
    if (Trigger.isUpdate)
    {
        for (integer i = 0; i < Trigger.New.Size(); i++)
        {
            if (Trigger.New[i].Primary_Partner_Involved__c != Trigger.Old[i].Primary_Partner_Involved__c)
            {
                camIds.Add(Trigger.New[i].Id);
            }
        }
        
    }
    
    if (Trigger.isInsert)
    {
        for (integer i = 0; i < Trigger.New.Size(); i++)
        {
            if (Trigger.New[i].Primary_Partner_Involved__c != null)
            {
                camIds.Add(Trigger.New[i].Id);
            }
        }
    }

    if (camIds.size() > 0)
    {
        if (!Test.isRunningTest())
            ApexSharingRules.UpdateCampaignSharing(camIds);
        else if (ApexSharingRules.TestingCampaignShare)
            ApexSharingRules.UpdateCampaignSharingForTest(camIds);
    }
    
    System.debug('Campaign_Sharing Trigger Finishing');
}