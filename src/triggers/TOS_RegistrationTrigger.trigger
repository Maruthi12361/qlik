/***
* 28-02-2018 - AYS - Created
***/

trigger TOS_RegistrationTrigger on TOS_Registration__c (after delete, after insert, after undelete,
after update, before delete, before insert, before update) {

  if(Trigger.isInsert){
    //if(Trigger.isBefore){}
    if(Trigger.isAfter){

      ToSRegistrationAutomationHandler.handle(Trigger.new);

      System.debug(System.LoggingLevel.DEBUG, '[SOQL queries After Insert] ' + Limits.getQueries());    
    }
  }
  if(Trigger.isUpdate){
    //if(Trigger.isBefore){}
    if(Trigger.isAfter){
      
      TOSApprovedCallHandler.handle(Trigger.new, Trigger.oldMap);

      System.debug(System.LoggingLevel.DEBUG, '[SOQL queries After Update] ' + Limits.getQueries());
    }
  }/*
  if(Trigger.isDelete){
    if(Trigger.isBefore){}
    if(Trigger.isAfter){}
  }*/
  
  System.debug(LoggingLevel.ERROR, '[SOQL queries] ' + Limits.getQueries());
}