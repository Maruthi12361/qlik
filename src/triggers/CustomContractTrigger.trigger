/*
    Change Log
    2017-02-15  TJG Updated so that when a manually created contracts without a Quote
            we still add them to Sales Channels. Create new Sales Channels if needed.
            Please note contracts created through normal quote processing are not covered.
            They are done in Process Builder
*/

trigger CustomContractTrigger on Contract (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    //To disable triggers on data import
   /* Q2CWSettings__c setting = Q2CWSettings__c.getInstance(UserInfo.getUserID());
    if(setting.Disable_Contract_trigger__c)
        return;
    if(Trigger.isBefore) {
        if(Trigger.isInsert){
            //ContractTriggerHandler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate) {
        
        }
    }
    if(Trigger.isAfter) {
         if(Trigger.isInsert){
            ContractTriggerHandler.onAfterInsert(Trigger.new);
        }
        if(Trigger.isUpdate) {
            ContractTriggerHandler.onAfterUpdate(Trigger.new, Trigger.old);
        }
    } TTG Commenting out for flow testing*/
}