/*******************************************************************
* Trigger PartnerExpertiseAreaValidateUniqueEA
* Test class for PartnerExpertiseAreaValidateUniqueEA Trigger
* 
* Description: Will validate Partner expertise area on acc have unique Expertise areas
* Log History:
* 20130221	RDZ		CR# 6729	https://eu1.salesforce.com/a0CD000000U7AFt
*								To validate PEA are created with unique EA per ACC.
* 20130411  RDZ		CR# 6729	Modifying to use label instead of string for error message
*								PEA_Validation_Unique_Expertise_Area_Error
* 20160119  MTM      Partner Expertise project V II
********************************************************************/
trigger PartnerExpertiseAreaValidateUniqueEA on Partner_Expertise_Area__c (before insert, before update) {
	
	Set<Id> AccountIds = new Set<Id>();
	//To set as sfdc custom label if requires translations
	String formaterrormessagelabel = System.Label.PEA_Validation_Unique_Expertise_Area_Error;
	
	System.debug('Getting all Accounts to check');
	for (Partner_Expertise_Area__c pea : trigger.new)
	{
		AccountIds.add(pea.Partner_Account_Name__c);
	}
    
    List<Partner_Expertise_Area__c> PEAs = [Select Id, Expertise_Area__c, Partner_Account_Name__c, Expertise_Application_Eligibility__c 
                                            From Partner_Expertise_Area__c
                                            Where Partner_Account_Name__c in :AccountIds
                                            AND Expertise_Application_Eligibility__c != 'Rejected'];
        
    for(Partner_Expertise_Area__c pea :trigger.new)
    {
        for(Partner_Expertise_Area__c listPea: PEAs)
        {
            if(listPea.Partner_Account_Name__c == pea.Partner_Account_Name__c &&
               listPea.Expertise_Area__c == pea.Expertise_Area__c &&
               listPea.id != pea.Id)  //Check for update
            {
                pea.addError(String.format(formaterrormessagelabel, new String[]{pea.Expertise_Area__c}));
            }
        }
        
    }
    
}  
    

//OLD code can be removed after testing
  /*  
    
	
	//Get Account.Partner_Expertise_Area_List__c per Acc id on map structure
	Map<Id, String> MapAccIdPEAList = new Map<Id, String>();
	System.debug('Getting all accs <Id, Partner_expertise_List> to create map');
	List<Account> accs = [Select Id, Partner_Expertise_List__c from Account where Id in :AccountIds];
	System.debug('Accs <Id, Partner_expertise_List> to create map. Accs list size: ' + accs.size());
	for(Account a :accs)
	{
		System.debug('-----');
		System.debug('Creating Map Acc Id Partner expertise area list');
		MapAccIdPEAList.put(a.Id, a.Partner_Expertise_List__c);
	}

	//if Partner Expertise Area (PEA) is created and another PEA has same Expertise Area(EA) display error and dont allow save
	if (trigger.isInsert)
	{
		Partner_Expertise_Area__c pea;
		String AccEAs = '';
		ID AccId = null;
		for(integer i=0; i<trigger.new.size();i++)
		{
			pea = trigger.new[i];
			AccId=pea.Partner_Account_Name__c;
			
			System.debug('On Insert: Check if new partner expertise area already exists on account');
			if (MapAccIdPEAList.containsKey(AccId))
			{
				AccEAs= MapAccIdPEAList.get(AccId);
				System.debug('Account Expertise Areas, already created for this acc are: ' + AccEAs);
				if (AccEAs != null && AccEAs != '' && AccEAs.contains(pea.Expertise_Area__c))
				{
					
					System.debug('add error to page trigger as alredy has ea that is trying to insert: AccEAs.contains(pea.Expertise_Area__c' +AccEAs.contains(pea.Expertise_Area__c));
					System.debug(String.format('formaterrormessagelabel', new string[]{pea.Expertise_Area__c}));
					trigger.new[i].addError(String.format(formaterrormessagelabel, new String[]{pea.Expertise_Area__c}));
				}		
			}	
		}
	}
	
	//if PEA is update to an EA that already exists in another PEA display an warning
	if (trigger.isUpdate)
	{
		Partner_Expertise_Area__c newpea;
		Partner_Expertise_Area__c oldpea;
		
		String AccEAs = '';
		ID AccId = null;
		for(integer i=0; i<trigger.new.size();i++)
		{
			newpea = trigger.new[i];
			oldpea = trigger.old[i];
			
			System.debug('On Update: Check if new partner expertise area already exists on account');
			
			if (newpea.Expertise_Area__c != oldpea.Expertise_Area__c)
			{
				System.debug('Old value Expertise Area: ' + oldpea.Expertise_Area__c);
				System.debug('New value Expertise Area: ' + newpea.Expertise_Area__c);
				AccId=newpea.Partner_Account_Name__c;
				if (MapAccIdPEAList.containsKey(AccId))
				{
					AccEAs= MapAccIdPEAList.get(AccId);
					if (AccEAs.contains(newpea.Expertise_Area__c))
					{
						System.debug('add error to page trigger as alredy has ea that is trying to insert: AccEAs.contains(newpea.Expertise_Area__c' +AccEAs.contains(newpea.Expertise_Area__c));
						System.debug(String.format('formaterrormessagelabel', new string[]{newpea.Expertise_Area__c}));
						trigger.new[i].Expertise_Area__c.addError(String.format(formaterrormessagelabel, new String[]{newpea.Expertise_Area__c}));
					}		
				}	
			}	
		}
	}
*/