/*    Copyright (c) 2014 Zuora, Inc.
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy of
*   this software and associated documentation files (the "Software"), to use copy,
*   modify, merge, publish the Software and to distribute, and sublicense copies of
*   the Software, provided no fee is charged for the Software.  In addition the
*   rights specified above are conditioned upon the following:
*
*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.
*
*   Zuora, Inc. or any other trademarks of Zuora, Inc.  may not be used to endorse
*   or promote products derived from this Software without specific prior written
*   permission from Zuora, Inc.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
*   ZUORA, INC. BE LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES
*   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
*   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
*   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*   IN THE EVENT YOU ARE AN EXISTING ZUORA CUSTOMER, USE OF THIS SOFTWARE IS GOVERNED
*
*   BY THIS AGREEMENT AND NOT YOUR MASTER SUBSCRIPTION AGREEMENT WITH ZUORA.

5/2/2019 BSL-1910 Shubham Gupta to Populate Partner Discount on Reseller Amend deals FROM initial 
Quote purchase instead of PCS record Added Z_QuoteAmendDiscountHandler.PopulatePartnerDiscount handler
17.1.2020 - BSL-3271: Ajay Santhosh - Remove Primary Quote number & Opportunity line items from Opportunity on deleting Primary Quote
*/

trigger Z_QuoteTrigger on zqu__Quote__c (before insert, before update, after delete, after insert, after update) {

    private static String FUNC_STR = 'Z_QuoteTrigger on zqu__Quote__c ';

    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            System.debug(FUNC_STR + 'This is before insert start.');
            System.debug('Semaphore is: ' + Semaphores.Z_QuoteTriggerBeforeInsert);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            if (!Semaphores.Z_QuoteTriggerBeforeInsert) {
                Semaphores.Z_QuoteTriggerBeforeInsert = true;

                Z_QuoteTriggerHandler.checkSoldToBillToByRevenue(Trigger.new);

                // Linus Löfberg - 2018.11.06 - https://jira.qlikdev.com/browse/IMP-876
                Z_QuoteTriggerHandler.selectPCSforQuote(Trigger.new);
                Z_QuoteTriggerHandler.setZuoraQuoteFields(Trigger.new, Trigger.newMap);
                Z_QuoteTriggerHandler.selectSalesRep(Trigger.new);
                Z_QuoteAmendDiscountHandler.PopulatePartnerDiscount(Trigger.new);
            }

            System.debug(FUNC_STR + 'This is before insert end.');
        } else if (Trigger.isAfter) {
            System.debug(FUNC_STR + 'This is after insert start.');
            System.debug('Semaphore is: ' + Semaphores.Z_QuoteTriggerAfterInsert);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            if (!Semaphores.Z_QuoteTriggerAfterInsert) {
                Semaphores.Z_QuoteTriggerAfterInsert = true;

                // IMP-1154 - Linus Löfberg
                Z_QuoteTriggerHandler.sendQuoteContactsToBoomi(Trigger.new, Trigger.newMap);
            }

            System.debug(FUNC_STR + 'This is after insert end.');
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            System.debug(FUNC_STR + 'This is before update start.');
            System.debug('Semaphore is: ' + Semaphores.Z_QuoteTriggerBeforeUpdate);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            if (!Semaphores.Z_QuoteTriggerBeforeUpdate) {
                Semaphores.Z_QuoteTriggerBeforeUpdate = true;

                Z_QuoteTriggerHandler.validateAndSetInvoiceOwner(Trigger.newMap, Trigger.oldMap);
                Z_QuoteTriggerHandler.setFieldsForApprovalProcess(Trigger.newMap);
                Z_QuoteTriggerHandler.updateQuoteFields(Trigger.newMap);
                //Z_QuoteTriggerHandler.checkSoldToBillToByRevenue(Trigger.new);
                //UIN added as part of IMP-872
                EcustomsHelper ecustoms = new ecustomsHelper();
                ecustoms.zuora_CheckVisualCompliance(Trigger.new, Trigger.OldMap);
                //UIN added as part of IMP-872
                /**Nov 5th: comment out based on Lauren's comment of eCustomsProcessChecksUpd  */
                //CustomZuoraQuoteTriggerHandler.eCustomsProcessChecksUpd(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap); //Added by UIN as part of IMP-709
                // Linus Löfberg - 2018.11.06 - https://jira.qlikdev.com/browse/IMP-876
                Z_QuoteTriggerHandler.selectPCSforQuote(Trigger.new);
                Z_QuoteTriggerHandler.selectSalesRep(Trigger.new);
                Z_QuoteTriggerHandler.setFulfillmentDiscount(Trigger.newMap);
                Z_QuoteAmendDiscountHandler.PopulatePartnerDiscount(Trigger.new);
            }

            Z_QuoteTriggerHandler.driveZuoraQuoteRecordTypes(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);

            System.debug(FUNC_STR + 'This is before update end.');
        } else if (Trigger.isAfter) {
            System.debug(FUNC_STR + 'This is after update start.');
            System.debug('Semaphore is: ' + Semaphores.Z_QuoteTriggerAfterUpdate);
            System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

            if (!Semaphores.Z_QuoteTriggerAfterUpdate) {
                Semaphores.Z_QuoteTriggerAfterUpdate = true;

                Z_QuoteTriggerHandler.sendToZuora(Trigger.newMap, Trigger.oldMap);
                Z_QuoteTriggerHandler.updateOppForPrimaryQuote(Trigger.new, Trigger.newMap, Trigger.oldMap, 'Update'); //BSL-1270
                Z_QuoteTriggerHandler.updateTierOwner(Trigger.newMap);

            }

            System.debug(FUNC_STR + 'This is after update end.');
        }
    } else if (Trigger.isDelete){
        System.debug(FUNC_STR + 'This is before delete start.');
        System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
        Z_QuoteTriggerHandler.deletePrimaryQuoteNumberFromOpp(Trigger.old);
    }
}