/******************************************************

    Class: InfuencingPartnerContactTrigger
    
     Changelog:
    Kumar Navneet
    20/04/17    
    For updating Influencing currency from Opportunity currency
    
    Changelog:
        2012-05-21  CCE     Created file
        2013=03-06  TJG     Partner of CR 7332 updates. We need to make sure that 
                            the value associated to the “Referral PSM/QM” field on 
                            the Influencing Partners related list is driven by the 
                            name of the Account Owner of the partner added and not 
                            that of the Contact Owner
******************************************************/
trigger InfuencingPartnerContactTrigger on Influencing_Partner__c (before insert, before update)
{
    system.debug('InfuencingPartnerContactTrigger: Starting');

    List<String> Ids = new List<String>();
    
   //Added for updating the currency 
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
         InfuencingPartnerHandler.Inserthandler(Trigger.new);
        }
        }
        
         /*     if(Trigger.isUpdate){         
        if(Trigger.isBefore) {
           InfuencingPartnerHandler.Updatehandler(Trigger.new , Trigger.newMap , Trigger.oldMap);
        }
        }
    */    
    for (Influencing_Partner__c ip:trigger.new)
    {
        if (ip.Influencing_Partner_Contact__c !=null)
        {
            ids.add(ip.Influencing_Partner_Contact__c);
        }
    }
    
    Map<Id, Contact> contactsMap = New Map<Id, Contact>([Select Account.OwnerId, AccountId FROM Contact WHERE Id In :Ids]);
    
    for (Influencing_Partner__c ip:trigger.new)
    {           
        if (ip.Influencing_Partner_Contact__c != null)
        {
            system.debug('InfuencingPartnerContactClass: Infuencing Partner contact Id = ' + ip.Influencing_Partner_Contact__c);
            
            Contact pContact = contactsMap.get(ip.Influencing_Partner_Contact__c);
            
            system.debug('InfuencingPartnerContactClass: Partner contact is ' + pContact);
            
            ip.Influencing_PSM_QM__c = pContact.Account.OwnerId;
            ip.Influencing_Partner_Account__c = pContact.AccountId;
        }           
    }   
    system.debug('InfuencingPartnerContactTrigger: Finishing');
}