/**********************************************************
* Class: LiveChatTranscript
* Changes Log:
*   2018-02-13 ext_bad create trigger
*   2018-03-01 ext_bad LCE-17 change visibility of ChatTranscriptPost
*************************************************************/
trigger LiveChatTranscript on LiveChatTranscript (before insert, after insert) {

    List<string> caseIds = new List<string>();
    for (LiveChatTranscript transcript : Trigger.new) {
        if (transcript.CaseId != null) {
            caseIds.add(transcript.CaseId);
        }
    }
    Map<Id, Case> casesMap = new Map<Id, Case>([
            SELECT Id, Chat_Start_Time__c, Chat_End_Time__c, Chat_Ended_By__c, OwnerId, RecordTypeId
            FROM Case
            WHERE Id IN :caseIds
    ]);
    if(trigger.isBefore) {
        LiveChatTranscriptHandler.populateCaseFields(Trigger.new, casesMap);
    } else {
        LiveChatTranscriptHandler.changeTranscriptPostVisibility(caseIds);
    }
}