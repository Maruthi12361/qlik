/********************************************************************************************
* 	Trigger ChangeControlChangeManagerUpdate
*
* 	Trigger that updates the field Change_Manager__c according to data in custom settings ChangeControlChangeManager__c
*
*	2014-06-25 	AIN (Andreas Nilsson)	Initial development
*										Updates the field Change_Manager__c with the applicable change manager
*										The change manager is found in the custom setting ChangeControlChangeManager__c
*										CR# 10110 Add Release Management to CR Process
**********************************************************************************************/
trigger ChangeControlChangeManagerUpdate on SLX__Change_Control__c (before insert, before update) 
{
	string recordTypeId;
	string recordTypeIdShort;
	string type;
	List<ChangeControlChangeManager__c> customSettings = ChangeControlChangeManager__c.getall().values();

	if(customSettings == null)
		return;
	
	for(SLX__Change_Control__c change : trigger.new)
	{
		recordTypeId = change.RecordTypeId;
		recordTypeIdShort = recordTypeId.substring(0,15);
		type = change.type__c;
		for(ChangeControlChangeManager__c customSetting : customSettings)
		{
			if(customSetting.RecordType__c == null || customSetting.Type__c == null || customSetting.ChangeManager__c == null || type == null || customSetting.ChangeManager__c.length() < 15)
				continue;
			if(customSetting.RecordType__c.indexOf(recordTypeIdShort) > -1 && customSetting.Type__c.indexOf(type) > -1)
			{
				change.Change_Manager__c = customSetting.ChangeManager__c.substring(0,15);
				break;
			}
		}
	}

}