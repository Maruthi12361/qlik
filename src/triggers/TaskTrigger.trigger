/**********************************************************
* Name:         TaskTrigger
* Object:       Task
* Author:       ext_vos
* Date:         2017-11-23
* Description:  Handles all actions for Task (except the TaskSetType.trigger logic). 
*               
* Changes Log:
*   Date     	author    CR#    Description
*************************************************************/
trigger TaskTrigger on Task (after insert, after update, before update, before insert) {   

    if (Trigger.isAfter) {
        if (Trigger.isInsert && !Semaphores.TaskTriggerAfterInsert) {
            Semaphores.TaskTriggerAfterInsert = true;

            TaskSetCaseFeedHandler.handle(Trigger.new, Trigger.oldMap);
            TaskAffectParentObjectsHandler.handle(Trigger.new);
            TaskCompleteMilestoneHandler.handle(Trigger.new);
            TaskCaseCommentHandler.handle(Trigger.new);
            TaskQlikviewSupportEscalationHandler.handle(Trigger.new);

        } else if (Trigger.isUpdate && !Semaphores.TaskTriggerAfterUpdate) {
            Semaphores.TaskTriggerAfterUpdate = true;

            TaskSetCaseFeedHandler.handle(Trigger.new, Trigger.oldMap);
            TaskQlikviewSupportEscalationHandler.handle(Trigger.new);
        }
    }
}