/*
 Author - Jina Chetia
 Company - Bluewolf
 Date - 09/09/2009
*/
trigger UpdateOpportunity_SOELastUpdated on Sequence_of_Event__c (after insert, after update, before delete) 
{
	Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>();
	Map<Id, DateTime> opporDatetimeMap = new Map<Id, DateTime>();
	//This is being called when any SOE record is inserted or updated
	if(Trigger.isInsert || Trigger.isUpdate)
	{
		for(Sequence_of_Event__c soe: Trigger.new)
		{
			if(!opporDateTimeMap.isEmpty() && opporDatetimeMap.containsKey(soe.Opportunity_ID__c))
			{
				if(soe.LastModifiedDate > opporDatetimeMap.get(soe.Opportunity_ID__c))
				{
					opporDatetimeMap.put(soe.Opportunity_ID__c, soe.LastModifiedDate);
				}
			}
			else
			{
				opporDatetimeMap.put(soe.Opportunity_ID__c, soe.LastModifiedDate);
			}
		}
		//Update the SOE Last Updated field in Opportunity with the Last Modified Date of the 
		//latest updated SOE for that Opportunity
		for(Opportunity opp: [select Id, SOE_Last_Updated__c from Opportunity where Id IN :opporDatetimeMap.keySet()])
		{
			if(opp.SOE_Last_Updated__c == null || opporDatetimeMap.get(opp.Id)> opp.SOE_Last_Updated__c)
			{
				opp.SOE_Last_Updated__c = opporDatetimeMap.get(opp.Id);
				opportunityMap.put(opp.Id,opp);
			}
		}
		update opportunityMap.values();
	}
	//This is being called when delete action is performed on SOE. 
	if(Trigger.isDelete)
	{
		Map<Id,DateTime> seqDateTime = new Map<Id, DateTime>(); 
		Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
		List<Id> oppId = new List<Id>();
		List<Id> soiId = new List<Id>();
		Map<Id,Opportunity> updateOppMap = new Map<Id,Opportunity>();
		
		for(Integer i=0; i<Trigger.old.size(); i++)
		{
			oppId.add(Trigger.old[i].Opportunity_ID__c);
			soiId.add(Trigger.old[i].Id);
		}
		//Retrieve all the Oppty details for the Oppty associated with the deleted SOE
		for(Opportunity opp: [select Id, SOE_Last_Updated__c from Opportunity where Id IN :oppId])
		{
			oppMap.put(opp.Id, opp);
		}
		//Retrieve all the SOE records for the Opportunity of the deleted SOE excluding the
		//deleted SOE
		for(Sequence_of_Event__c seq: [select Id,Opportunity_ID__c, LastModifiedDate 
			from Sequence_of_Event__c where Opportunity_ID__c IN :oppMap.keySet() AND Id NOT IN :soiId])
		{
			if(seqDateTime.containsKey(seq.Opportunity_ID__c))
			{
				if(seq.LastModifiedDate > seqDateTime.get(seq.Opportunity_ID__c))
				{
					seqDateTime.put(seq.Opportunity_ID__c, seq.LastModifiedDate);
				}
			}
			else
			{
				seqDateTime.put(seq.Opportunity_ID__c, seq.LastModifiedDate);
			}
		}
		//If there are exist other SOE records for the same Opportunity
		//and the SOE Last Updated value in that Oppty is same as the as the Last Modified Date of the deleted record, 
		//update the SOE Last Updated in the Opportunity with the LastModified Date from last updated SOE record for that Opportunity.
		// If there are no other SOE records for the same Opportunity, update the SOE Last Updated to null.
		for(Integer i=0; i<Trigger.old.size(); i++)
		{
			Opportunity opp = oppMap.get(Trigger.old[i].Opportunity_ID__c);
			if(opp.SOE_Last_Updated__c != null && opp.SOE_Last_Updated__c == Trigger.old[i].LastModifiedDate)
			{
				if(seqDateTime.containsKey(Trigger.old[i].Opportunity_ID__c))
				{
					opp.SOE_Last_Updated__c = seqDateTime.get(Trigger.old[i].Opportunity_ID__c);
				}
				else
				{
					opp.SOE_Last_Updated__c = null;
				}
				updateOppMap.put(opp.Id,opp);
			}
		}
		update updateOppMap.values();
	}
}