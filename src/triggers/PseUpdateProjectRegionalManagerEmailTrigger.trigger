/********************************************************************************
Name            : PseUpdateProjectRegionalManagerEmailTrigger
Author          : Jason Favors (Appirio)
Created Date    : September 6, 2010
Usage           : A trigger to update RM email when changing the Region's Head.
*********************************************************************************/

trigger PseUpdateProjectRegionalManagerEmailTrigger on pse__Region__c (after update) {

    // assemble a list of related region owner IDs
    pse__Region__c newRegion;
    pse__Region__c oldRegion;
    Set<Id> regionOwnerIds = new Set<Id>();
    Set<Id> regionIds = new Set<Id>();
    for (Id regionId : Trigger.newMap.keySet()) {
        newRegion = Trigger.newMap.get(regionId);
        oldRegion = Trigger.oldMap.get(regionId);
        if (newRegion.pse__Region_Head__c != oldRegion.pse__Region_Head__c) {
            regionOwnerIds.add(newRegion.pse__Region_Head__c);
            regionIds.add(regionId);
        }
    }

    // Only do updates if region owners changed and PM exists
    if (regionOwnerIds.size() > 0) {
        Map<Id, Contact> regionalManagers = new Map<Id, Contact>([SELECT Id, Email
                FROM Contact WHERE Id in :regionOwnerIds]);

        // Find projects to update
        List<pse__Proj__c> projects = [SELECT Id, pse__Region__c, pse__Region__r.pse__Region_Head__c, 
                Regional_Manager_Email__c, pse__Project_Manager__c
                FROM pse__Proj__c WHERE pse__Region__c in :regionIds];

        // Get updated email
        Contact regionalManager;
        for (pse__Proj__c project : projects) {
            regionalManager = regionalManagers.get(project.pse__Region__r.pse__Region_Head__c);
            if (regionalManager != null && project.pse__Project_Manager__c != null) {
                project.Regional_Manager_Email__c = regionalManager.Email;
            }
        }

        // update
        try {
            update projects;
        } catch (DmlException e) {
            System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
        }
    }
}