/******************************************************
Trigger: LeadTrigger

Changelog:    
    2017-06-28  BAD  File created. Main trigger for framework
    2017-10-25 AYS BMW-402 : Class extended with all converted triggers to handlers
    2018-04-11 AYS BMW-721 : LeadWebToLeadHandler added
    2019-07-22 CCE BMW-1631 : Debug code for ULC convert issue
    2019-07-24 CCE BMW-1637 : On Lead Convert we need to update the Product Trial and Product User objects
******************************************************/ 

trigger LeadTrigger on Lead (before insert, after insert, before update, after update) {

    if(Trigger.isInsert) {
        if(Trigger.isBefore) {

            if(!Semaphores.LeadTriggerHandlerBeforeInsert){
                Semaphores.LeadTriggerHandlerBeforeInsert = true;

                LeadPRMReferralUpdateVersionNumHandler.handle(Trigger.new, Trigger.old, Trigger.isInsert);
                LeadUpdateGroupEmpRangeHandler.handle(Trigger.new);
                LeadPRMReferralUpdateContactFieldHandler.handle(Trigger.new);
                LeadBeforeInsertBeforeUpdateHandler.handle(Trigger.new, Trigger.old, Trigger.isInsert);
                LeadOnInsertTrigger2Handler.handle(Trigger.new);
                LeadCallMarketoWebServiceHandler.handle(Trigger.new, Trigger.isBefore, Trigger.isAfter);
                LeadCountryISOUpdateHandler.handle(Trigger.new, Trigger.old, Trigger.isUpdate);
                LeadUpdateFollowUpRequiredTypeHandler.handle(Trigger.new, Trigger.old, Trigger.isUpdate);

                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries Before Insert] ' + Limits.getQueries());
            }

        } 
        if(Trigger.isAfter) {
            if(!Semaphores.LeadTriggerHandlerAfterInsert){
                Semaphores.LeadTriggerHandlerAfterInsert = true;

                LeadCallMarketoWebServiceHandler.handle(Trigger.new, Trigger.isBefore, Trigger.isAfter);
                LeadWebToLeadHandler.handle(Trigger.new);

                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries After Insert] ' + Limits.getQueries());
            }
        }
    }
    if(Trigger.isUpdate) {
        Map<Id, Id> l2c = new Map<Id, Id>(); //l2c = LeadToContact
        String sUp = Trigger.isBefore ? 'before update' : 'after update';
        String sSem = 'LTHBU='+ Semaphores.LeadTriggerHandlerBeforeUpdate + ' LTHBU2=' + Semaphores.LeadTriggerHandlerBeforeUpdate2 + ' LTHAU=' + Semaphores.LeadTriggerHandlerAfterUpdate;
        for (integer i = 0; i < Trigger.new.size(); i++) {
            if (Trigger.new[i].IsConverted == true && Trigger.old[i].IsConverted == false && Trigger.new[i].ConvertedContactId != null) {
                l2c.put(Trigger.new[i].Id, Trigger.new[i].ConvertedContactId);                
            }
        }
        if (!l2c.isEmpty()) { WriteLogInfo(sUp, sSem + ' l2c.size=(' + l2c.size() + ') l2c=' + l2c); }

        if(Trigger.isBefore) {
            if(!Semaphores.LeadTriggerHandlerBeforeUpdate){

                LeadUpdateULCDetailsOnConvertHandler.handle(Trigger.new, Trigger.old, Trigger.isAfter, Trigger.isBefore); //this one first
                LeadZiftInitHandler.handle(Trigger.new, Trigger.oldMap); //Instead on PB
                LeadPRMReferralUpdateVersionNumHandler.handle(Trigger.new, Trigger.old, Trigger.isInsert);          
                LeadUpdateAutoCreateEventHandler.handle(Trigger.new, Trigger.oldMap);
                LeadUpdateGroupEmpRangeHandler.handle(Trigger.new);
                LeadPartnerOppRegRejectHandler.handle(Trigger.new, Trigger.old);
                LeadCheckOnLeadConversionHandler.handle(Trigger.new, Trigger.old, Trigger.isUpdate);
                LeadPRMReferralUpdateContactFieldHandler.handle(Trigger.new);
                LeadZiftHandler.handle(Trigger.new, Trigger.old);
                LeadRecordTypeUpdateHandler.handle(Trigger.new, Trigger.old);
                LeadCallMarketoWebServiceHandler.handle(Trigger.new, Trigger.isBefore, Trigger.isAfter);
                LeadCountryISOUpdateHandler.handle(Trigger.new, Trigger.old, Trigger.isUpdate);
                LeadScoreHandler.handle(Trigger.new, Trigger.oldMap, Trigger.isAfter, Trigger.isBefore);
                LeadSetOwnerHandler.handle(Trigger.new, Trigger.oldMap);
                LeanDataPopulateFieldonMatchinghandler.handle(Trigger.new, Trigger.old);

                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries Before Update] ' + Limits.getQueries());
            }
            if(!Semaphores.LeadTriggerHandlerBeforeUpdate2){

                LeadPRMReferralConfirmAcceptFieldHandler.handle(Trigger.new, Trigger.old);
                LeadBeforeInsertBeforeUpdateHandler.handle(Trigger.new, Trigger.old, Trigger.isInsert);
                LeadUpdateFollowUpRequiredTypeHandler.handle(Trigger.new, Trigger.old, Trigger.isUpdate); //https://cs84.salesforce.com/01QD0000000Iuxc

            }

            //handle semaphores
            if(Semaphores.LeadTriggerHandlerBeforeUpdate == false){
                Semaphores.LeadTriggerHandlerBeforeUpdate = true;
            }else if(Semaphores.LeadTriggerHandlerBeforeUpdate2 == false){
                Semaphores.LeadTriggerHandlerBeforeUpdate2 = true;
            }

        }
        if(Trigger.isAfter) {

            if(!Semaphores.LeadTriggerHandlerAfterUpdate){
                Semaphores.LeadTriggerHandlerAfterUpdate = true;

                LeadUpdateULCDetailsOnConvertHandler.handle(Trigger.new, Trigger.old, Trigger.isAfter, Trigger.isBefore); //this one first
                LeadConvertUpdateSubscriptTableHandler.handle(Trigger.new, Trigger.old); //this one second
                LeadConvertUpdateProductHandler.handle(Trigger.new, Trigger.old); //this one third
                LeadOppSourceUpdonLeadConvHandler.handle(Trigger.new, Trigger.old); //this one fourth
                LeadConvUpdateQVMCustomerProductsHandler.handle(Trigger.new, Trigger.old, Trigger.isAfter); //this one fith
                LeadPartnerSharingAfterSubmissionHandler.handle(Trigger.new, Trigger.old);
                LeadCreateSphereOfInfluenceHandler.handle(Trigger.new);
                LeadUpdateULCTriggerHandler.handle(Trigger.new, Trigger.old);
                LeadAcademicProgramConversionHandler.handle(Trigger.new, Trigger.old);
                LeadCallMarketoWebServiceHandler.handle(Trigger.new, Trigger.isBefore, Trigger.isAfter);
                LeadScoreHandler.handle(Trigger.new, Trigger.oldMap, Trigger.isAfter, Trigger.isBefore);
                LeadZiftClawbackHandler.handle(Trigger.new, Trigger.old);
				LeadRedCountryCheckonConversionHandler.handle(Trigger.new, Trigger.old);
				
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries After Update] ' + Limits.getQueries());

            }
        }
    }
    if(Trigger.isDelete) {
        if(Trigger.isBefore) {
        }
        if(Trigger.isAfter) {
        }
    }
    System.debug(LoggingLevel.ERROR, '[SOQL queries] ' + Limits.getQueries());



    public static void WriteLogInfo(String sMethod, String sData) {
        new ApexDebugLog().createLog(new ApexDebugLog.Information('LeadTrigger', sMethod, sData, NULL));
    }
}

                    //Semaphores.LeadTriggerHandlerBeforeUpdate = false;
                    //Semaphores.LeadTriggerHandlerAfterUpdate = false;
                    //Semaphores.LeadTriggerHandlerBeforeInsert = false;
                    //Semaphores.LeadTriggerHandlerAfterInsert = false;