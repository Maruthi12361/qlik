trigger RejectDuplicateULCDetailsOnContactLead on ULC_Details__c (before insert) {
	Set<Id> Contacts = new Set<Id>();
	Set<Id> Leads = new Set<Id>();
	
	for(ULC_Details__c Detail : Trigger.New)
	{
		
		if (Detail.ContactId__c != null)
		{
			Contacts.Add(Detail.ContactId__c);
		}
		
		if (Detail.LeadId__c != null)
		{
			Leads.Add(Detail.LeadId__c);
		}
		
	}
	
	for (ULC_Details__c Duplicate : [select Id, LeadId__c, ContactId__c from ULC_Details__c where ContactId__c in : Contacts or LeadId__c in :Leads]) 
	{
		
		for (ULC_Details__c Detail : Trigger.New) 
		{
			
			if (Detail.LeadId__c != null && Detail.LeadId__c == Duplicate.LeadId__c)
			{
				Detail.addError('Only one ULC Details record can be attached to a Lead');
			}

			if (Detail.ContactId__c != null && Detail.ContactId__c == Duplicate.ContactId__c)
			{
				Detail.addError('Only one ULC Details record can be attached to a Contact');
			}
			
		}	
		
	}
	
}