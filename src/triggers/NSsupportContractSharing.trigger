/*    Changelog:
        2015-12-21  IRN     CR42998 partnershare project - Created file 
*/
trigger NSsupportContractSharing on NS_Support_Contract__c (after insert, after update, after delete) {
    System.debug('---NSsupportContractSharing---');
    if((trigger.isAfter && trigger.isInsert)||(trigger.isAfter && trigger.isUpdate)){
        Set<Id> uniqueEndUserSet = new Set<Id>();
        List<Id>contractIds = new List<Id>(); 
        for(Integer i = 0;  i< Trigger.New.size(); i++){
            if(Trigger.New[i].End_User__c != null){
                uniqueEndUserSet.add(Trigger.New[i].End_User__c);
                if(Trigger.Old != null && Trigger.New[i].End_User__c != Trigger.Old[i].End_User__c && Trigger.Old[i].End_User__c != null){
                    uniqueEndUserSet.add(Trigger.Old[i].End_User__c);
                }   
            }else if(Trigger.Old != null && Trigger.Old[i].End_User__c != null){
                uniqueEndUserSet.add(Trigger.Old[i].End_User__c);
            }
            contractIds.add(trigger.New[i].Id);
        }
        List<Id> uniqueEndUser = new List<Id>();
        uniqueEndUser.addAll(uniqueEndUserSet);

        if(!Test.isRunningTest()){
            ApexSharingRules.updateNSSupportContractSharing(uniqueEndUser, contractIds);
        }else if(ApexsharingRules.TestingParnershare){
            ApexSharingRules.updateNSSupportContractSharingForTest(uniqueEndUser, contractIds);
        }
    }else if(trigger.isAfter && trigger.isDelete){
        List<Id>contractIds = new List<Id>();
        Set<Id> uniqueEndUserSet = new Set<Id>();
        for(Integer i = 0; i<Trigger.Old.size(); i++){
            contractIds.add(Trigger.Old[i].Id);
            uniqueEndUserSet.add(Trigger.Old [i].End_User__c);
        }
        List<Id> uniqueEndUser = new List<Id>();
        uniqueEndUser.addAll(uniqueEndUserSet);
        ResponsiblePartners rp = new ResponsiblePartners();
        rp.deleteResponsiblePartnerRecord(Trigger.Old, uniqueEndUser);
        if(!Test.isRunningTest()){
            ApexsharingRules.updateAccountLicensesDueToNSSupportContractSharing(contractIds);
        }else if(ApexsharingRules.TestingParnershare){
            ApexsharingRules.updateAccountLicensesDueToNSSupportContractSharingForTest(contractIds);
        }
        
    }
}