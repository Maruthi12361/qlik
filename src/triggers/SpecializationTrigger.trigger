/********************************************************
* Specialization
* Description: This trigger is used to update,delete Specialization field on account from specialization field from related list
*
* 
*********************************************************/
trigger SpecializationTrigger on Specialization__c (after insert, after update, after delete) {
    if(trigger.isAfter){
        if(Trigger.isInsert) {
            AccUpdateSpecializationHandler.onAfterInsert(Trigger.new, Trigger.newMap);
        }else if(Trigger.isUpdate) {
            AccUpdateSpecializationHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }else if(Trigger.isDelete) {
            AccUpdateSpecializationHandler.onAfterDelete(Trigger.old, Trigger.newMap);
        }
    }
}