/****************************************************************
*
* AddressTrigger
*
* Address approvals trigger.
*
*       2017-04-02 MTM Initial development
		2017-06-27 QCW-1733 Linus Löfberg
        2018-01-26 MTM QCW-2983 Optimize StrikeIron Usage
*
*****************************************************************/

trigger AddressTrigger on Address__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete)
{
	//To disable triggers on data import
	Boolean isApiUser = AddressTriggerHelper.IsApiUser();
	Q2CWSettings__c appSetting = Q2CWSettings__c.getInstance(UserInfo.getUserID());
	if(appSetting.Disable_Address_trigger__c)
		return;
    AddressTriggerHandler handler = new AddressTriggerHandler(Trigger.isExecuting, Trigger.size);

    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
        if(!isApiUser)  {
            AddressTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
			}
    }

    else if(Trigger.isUpdate && Trigger.isBefore){
       handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
        if(!isApiUser){
			Set<Id> updatedAddresses = AddressTriggerHelper.CheckforUpdates(Trigger.new, Trigger.oldMap);
			if(!updatedAddresses.isEmpty())
				AddressTriggerHandler.OnAfterUpdateAsync(updatedAddresses);			
		}
    }

    else if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    else if(Trigger.isDelete && Trigger.isAfter){
        handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        AddressTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
    }

    else if(Trigger.isUnDelete){
        handler.OnUndelete(Trigger.new);
    }
}