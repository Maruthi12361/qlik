/*****************************************************
Trigger: UpdateHQLResponse
Object: CampaignMember
Description: CR# 84987 – Enable Tracking for HQL Campaigns

Change Log:
2017-04-18 CCE CR# 84987 - Initial development - if "High Quality Campaign Response" on Campaign Member is set to true 
               then set "High Quality Campaign Response" field to true on Lead and contact object. if "High Quality Campaign Response" on 
               Campaign Member is set to false then set "High Quality Campaign Response" field to false on Lead and contact object.

******************************************************/
trigger UpdateHQLResponse on CampaignMember (before insert, before update) {

    Set<Id> leadIds_true = new Set<Id>();
    Set<Id> ctIds_true = new Set<Id>();
    Set<Id> leadIds_false = new Set<Id>();
    Set<Id> ctIds_false = new Set<Id>();
    
    for(CampaignMember cm : Trigger.new) {
        if (Trigger.isInsert) {
            if ((cm.LeadId != null) && (cm.Lead.IsConverted == false) && cm.High_Quality_Campaign_Response__c == true) {
                leadIds_true.add(cm.LeadId);
            } else if (cm.ContactId != null && cm.High_Quality_Campaign_Response__c == true) {
                ctIds_true.add(cm.ContactId);
            }
        }
        else if (Trigger.isUpdate) {
            CampaignMember oldCM = Trigger.oldMap.get(cm.Id);
            if ((cm.LeadId != null) && (cm.Lead.IsConverted == false)) {
                if (cm.High_Quality_Campaign_Response__c != oldCM.High_Quality_Campaign_Response__c) { //check if High Quality Campaign Response has changed
                    if (cm.High_Quality_Campaign_Response__c == true) { leadIds_true.add(cm.LeadId); }
                    else { leadIds_false.add(cm.LeadId); }
                }
            } else if (cm.ContactId != null) {
                if (cm.High_Quality_Campaign_Response__c != oldCM.High_Quality_Campaign_Response__c) { //check if High Quality Campaign Response has changed
                    if (cm.High_Quality_Campaign_Response__c == true) { ctIds_true.add(cm.ContactId); }
                    else { ctIds_false.add(cm.ContactId); }
                }
            }
        }
    }

    // Update the Lead HQL Response field
    //System.debug('UpdateHQLResponse: leadIds_true.size() = ' + leadIds_true.size());
    //System.debug('UpdateHQLResponse: leadIds_false.size() = ' + leadIds_false.size());
    if((leadIds_true.size() + leadIds_false.size()) > 0) {
        List<Lead> lstLeads = [SELECT Id, High_Quality_Campaign_Response__c FROM Lead WHERE (Id IN :leadIds_true) OR (Id IN :leadIds_false)];
        //System.debug('UpdateHQLResponse: List of leads: ' + lstLeads);
        for(Integer i = 0; i < lstLeads.size(); i++) {
            if (leadIds_true.contains(lstLeads[i].Id)) {
                lstLeads[i].High_Quality_Campaign_Response__c = true;
            }
            if (leadIds_false.contains(lstLeads[i].Id)) {
                lstLeads[i].High_Quality_Campaign_Response__c = false;
            }
            //System.debug('UpdateHQLResponse: lstLeads[i].High_Quality_Campaign_Response__c: ' + lstLeads[i].High_Quality_Campaign_Response__c);
        }
        if(!lstLeads.isEmpty()) update lstLeads;
    }

    // Update the Contact HQL Response field
    //System.debug('UpdateHQLResponse: ctIds_true.size() = ' + ctIds_true.size());
    //System.debug('UpdateHQLResponse: ctIds_false.size() = ' + ctIds_false.size());
    if((ctIds_true.size() + ctIds_false.size()) > 0) {
        List<Contact> lstCons = [SELECT Id, High_Quality_Campaign_Response__c FROM Contact WHERE (Id IN :ctIds_true) OR (Id IN :ctIds_false)];
        //System.debug('UpdateHQLResponse: List of contacts: ' + lstCons);
        for(Integer i = 0; i < lstCons.size(); i++) {
            if (ctIds_true.contains(lstCons[i].Id)) {
                lstCons[i].High_Quality_Campaign_Response__c = true;
            }
            if (ctIds_false.contains(lstCons[i].Id)) {
                lstCons[i].High_Quality_Campaign_Response__c = false;
            }
            //System.debug('UpdateHQLResponse: lstCons[i].High_Quality_Campaign_Response__c: ' + lstCons[i].High_Quality_Campaign_Response__c);
        }
        if(!lstCons.isEmpty()) update lstCons;
    }

}