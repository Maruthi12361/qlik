/*
 Initail development MTM
 Don't add any calls/functionality  here directly. Add in handler/helper  classes. 
    14-08-2017 MTM Moving boomi calls to handler method.
    25-09-2017 Oleksandr@4Front QCW-2618
    2017-11-01 Viktor@4Front QCW-3787, QCW-4218 rewrite trigger. Last Modified By Oleksandr Kuzmych, 11/10/2017 18:17
                Ask me for previous version while we don't have a repository for all our code.
	03/19/2019 Anjuna BSL-1702
	
*/
trigger SBQQQuoteTrigger on SBQQ__Quote__c (
        before insert, before update, before delete,
        after insert, after update, after delete,
        after undelete) {

    SBQQQuoteTriggerHandler handler = new SBQQQuoteTriggerHandler(Trigger.isExecuting, Trigger.size);
    try {
        if (Trigger.isInsert) { // Insert
            if (Trigger.isBefore) {
                if (!Semaphores.SBQQQuoteTriggerHandlerBeforeInsert) {
                    Semaphores.SBQQQuoteTriggerHandlerBeforeInsert = true;
                    handler.OnBeforeInsert(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            } else if (Trigger.isAfter) {
                if (!Semaphores.SBQQQuoteTriggerHandlerAfterInsert) {
                    Semaphores.SBQQQuoteTriggerHandlerAfterInsert = true;
                    handler.OnAfterInsert(Trigger.new, Trigger.newMap);
                    /* Anjuna Baby - BSL-1702 - Populate subscription field on opportunity from Primary Quote*/
                    CustomQuoteTriggerHandler.populateSubscriptionFields(Trigger.new, null);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            }
        } else if (Trigger.isUpdate) {// Update
            if (Trigger.isBefore) {
                if (!Semaphores.SBQQQuoteTriggerHandlerBeforeUpdate) {
                    Semaphores.SBQQQuoteTriggerHandlerBeforeUpdate = true;
                    handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    /* UIN commented as part of BSL-449
					ECustomsHelper eCustomsHelper = new ECustomsHelper();
                    eCustomsHelper.UpdateECustomsStatus(Trigger.new);
					*/
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
/* TODO Viktor@4Front should be there while we not fix trigger issues
                    SBQQQuoteUncheckLegalApproved uncheckerLegalApproved = new SBQQQuoteUncheckLegalApproved();
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    uncheckerLegalApproved.uncheckLegalApproved(Trigger.oldMap, Trigger.New);
*/
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    //ApproversSharing approversSharingHandler = new ApproversSharing();    //Oleksandr@4Front QCW-2618
                    //approversSharingHandler.shareApproversWithPartner(Trigger.New);       //Oleksandr@4Front QCW-2618 sharing
                }
// TODO Viktor@4Front should be there while we not fix trigger issues
                SBQQQuoteUncheckLegalApproved uncheckerLegalApproved = new SBQQQuoteUncheckLegalApproved();
                System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                uncheckerLegalApproved.uncheckLegalApproved(Trigger.oldMap, Trigger.New);

            } else if (Trigger.isAfter) {
                /* Anjuna Baby - BSL-1702 - Populate subscription field on opportunity from Primary Quote*/
                CustomQuoteTriggerHandler.populateSubscriptionFields(Trigger.new, Trigger.oldMap);
                if (!Semaphores.SBQQQuoteTriggerHandlerAfterUpdate) {
                    Semaphores.SBQQQuoteTriggerHandlerAfterUpdate = true;
                    handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    CustomQuoteTriggerHandler.changingTermsProcessing(Trigger.new, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                    //ApproversSharing approversSharingHandler = new ApproversSharing();                //Oleksandr@4Front QCW-2618
                    //approversSharingHandler.shareApproversWithPartner(Trigger.New);                   //Oleksandr@4Front QCW-2618 sharing
                    //approversSharingHandler.unShareApproversWithPartner(Trigger.New, Trigger.oldMap); //Oleksandr@4Front QCW-2618 unsharing
                }
            }
        } else if (Trigger.isDelete) { // Delete
            if (Trigger.isBefore) {
                handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
            } else if (Trigger.isAfter) {
                handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
                SBQQQuoteTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
            }
        } else if (Trigger.isUnDelete) { // Undelete
            handler.OnUndelete(Trigger.new);
        }
    } catch (Exception e) {
        system.debug(System.LoggingLevel.ERROR, '[SBQQQuoteTriggerHandler] '
                                                                    + e.getStackTraceString() + '\n' + e.getMessage());
    }
}