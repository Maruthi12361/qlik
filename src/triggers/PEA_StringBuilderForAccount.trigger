/******************************************************

	Trigger: PEA_StringBuilderForAccount
	
	2013-02-15	CCE	Initial development	for CR# 6833 https://eu1.salesforce.com/a0CD000000U7aL2
					The trigger is used to help populate a field on the Account which will hold
					a ; separated list of partner Experise Areas. Test method Test_PEA_StringBuilderForAccount()
				
******************************************************/
trigger PEA_StringBuilderForAccount on Partner_Expertise_Area__c (after insert, after update, after delete) {
	System.debug('PEA_StringBuilderForAccount: Starting');
	list<Account> AccToUpdate = new list<Account>();
	List<String> Ids = new List<String>();	
		
	if (Trigger.isDelete)
	{
		//Create a list of Accounts that need updating when a Partner Expertise Area gets deleted	
		for (Partner_Expertise_Area__c pea:trigger.old)
		{
			Ids.add(pea.Partner_Account_Name__c);		
		}	
	}
	else
	{
		//Create a list of Accounts that need updating when a Partner Expertise Area get added or updated
		for (Partner_Expertise_Area__c pea:trigger.new)
		{
			Ids.add(pea.Partner_Account_Name__c);		
		}
	}
	System.debug('PEA_StringBuilderForAccount Ids.size = ' + Ids.size() + ' Ids = ' + Ids);    	
		
	//Get a list of Accounts with their list of Partner Expertise Areas
	list<Account> accs = new list<Account>([SELECT Id, Partner_Expertise_List__c, (Select Expertise_Area__c From Partner_Expertise_Area__r) FROM Account WHERE Id IN :Ids]);
	System.debug('PEA_StringBuilderForAccount accs.size() = ' + accs.size());
	//Loop through the list of Accounts, extracting the Expertise Area field from each Partner Expertise Area and 
	//concaternate them into a ; seperated text string for each Account.
	for (Account a : accs){
		//Get the list of Partner Expertise Area for an Account
		list<Partner_Expertise_Area__c> pea = a.Partner_Expertise_Area__r;
		System.debug('PEA_StringBuilderForAccount pea.size() = ' + pea.size());
		//if there is at least one Partner Expertise Areas for this Account then build the string
		if (pea.size() > 0) {
			//Build the concaternated string			
			String ea = '';
		    for(Partner_Expertise_Area__c p : pea) {
		        if (!ea.contains(p.Expertise_Area__c)) {	//remove duplicates (shouldn't need this but just in case)
		        	ea = ea + ';'+ p.Expertise_Area__c;		
		        }    
			}
			//Update the result field on the Account with the concaternated string of Expertise Fields, also removing the leading ;
			if (string.isNotBlank(ea)) {
				a.Partner_Expertise_List__c = ea.RemoveStart(';');
				AccToUpdate.add(a);
				System.debug('PEA_StringBuilderForAccount AccToUpdate.add = ' + a);	
			}
		}
		else	//if pea.size() == 0 then there are no Partner Expertise Areas for this Account so blank the result string
		{
			a.Partner_Expertise_List__c = '';
			AccToUpdate.add(a);				
		}
	}
	
	try
	{
		if (AccToUpdate.size() > 0) {
			update AccToUpdate;
		}
	}
	Catch(DMLException ex)
	{
    	AccToUpdate[0].addError(ex.getMessage());
    }
    
	System.debug('PEA_StringBuilderForAccount: Finishing');
}