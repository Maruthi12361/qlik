trigger QVM_ProductDataTriggers on QVM_Product_Data__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	
	if(trigger.isUpdate && trigger.isAfter) {
		
		for(QVM_Product_Data__c p : trigger.new) {
			
			QVM_Product_Data__c old = trigger.oldMap.get(p.Id);
			
			if(p.Magento_Product_Id__c == null && p.Status__c == 'Ready to Publish' && old.Status__c != 'Ready to Publish') {
				
				QVM.createMagentoProduct(p.Id);
                
			}
			/* Removed on 04/02/2012 to resolve issue with partner changes being made live 
			if(p.Magento_Product_Id__c != null && p.Status__c == 'Ready to Publish' && old.Status__c != 'Ready to Publish') {
                QVM.updateMagentoProduct(p.Id);
            }
			*/
			
		}
		
	}
    
}