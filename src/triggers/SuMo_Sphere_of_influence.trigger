trigger SuMo_Sphere_of_influence on Sphere_of_Influence__c (after insert, after update) {
  success.SuMoTriggerHandler handler = new success.SuMoTriggerHandler();
  if(Trigger.isInsert && Trigger.isAfter){
    if(success.SuMoObjectSettings.getInstance().objectIsEnabled('Sphere_of_Influence__c')){
      handler.OnInsert(Trigger.new) ;
    }
  } else if(Trigger.isUpdate && Trigger.isAfter){
    if(success.SuMoObjectSettings.getInstance().objectIsEnabled('Sphere_of_Influence__c')){
      handler.OnUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }
  }
}