trigger SBQQSubScriptionTrigger on SBQQ__Subscription__c (after insert, after update) {
	List<Id> subsIds = new List<Id>();
    
    if (Trigger.isUpdate)
    {
        for (integer i = 0; i < Trigger.New.Size(); i++)
        {
            if (Trigger.New[i].SBQQ__Account__c != Trigger.Old[i].SBQQ__Account__c)
            {
                subsIds.Add(Trigger.New[i].Id);
            }
        }
    }
    
    if (Trigger.isInsert)
    {
        for (integer i = 0; i < Trigger.New.Size(); i++)
        {
            subsIds.Add(Trigger.New[i].Id);
        }
    }
    
    System.Debug('SBQQSubScriptionTrigger: About to update sharing for' + subsIds.size() + 'records');
    
    if (subsIds.size() > 0)
    {       
       if (!Test.isRunningTest())
            ApexSharingRules.UpdateSBQQSubscriptionSharing(subsIds);
        else if (ApexSharingRules.TestingParnershare)
            ApexSharingRules.UpdateSBQQSubscriptionSharingNow(subsIds);
    }
}