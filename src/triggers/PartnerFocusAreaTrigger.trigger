/********************************************************
* Author: Pramod Kumar V 
* Date : 28/07/2017
* PAEX - 220
* Partner Focus Area
* Description: This trigger is used to update,delete Industries field on account from Industry field from related list
*
* 
*********************************************************/
trigger PartnerFocusAreaTrigger on Partner_Focus_Area__c (after insert, after update, after delete) {
if(trigger.isAfter){
        if(Trigger.isInsert) {
            PartnerFocusAreaHandler.onAfterInsert(Trigger.new, Trigger.newMap);
        }else if(Trigger.isUpdate) {
            PartnerFocusAreaHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }else if(Trigger.isDelete) {
            PartnerFocusAreaHandler.onAfterDelete(Trigger.old, Trigger.newMap);
        }
    }
}