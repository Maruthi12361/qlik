/*******************************************************************************************************************
* Name: CampaignMemberOnInsertDeDupeLeads
* Does: Removes duplicate leads sent by SharedVue,
*		Update relevant fields of existing leads.
*	 														
* Log Changes:
*
* 10/09/2012	TJG Fix issues with existing campaign member problems
* 04/09/2012	TJG Filter out leads other than those from SharedVue and ensure bulk import still works
* 17/08/2012	TJG PO179 https://projects.qliktech.com/sites/projects/MarketingasaServiceforPartners/default.aspx
*
********************************************************************************************************************/
trigger CampaignMemberOnInsertDeDupeLeads on CampaignMember (after insert) {
	static final String DEBUG_PREFIX = 'TJ-JT: ';
	static final String WA_SOURCE = 'Qonnect Marketing Services';
	static final String LEADSOURCE = 'PART - Partner';
	static final String RAWPROSPECT = 'Raw - Prospect';
	static final String RT_PARTEROPPREG = '012D0000000JsWAIA0';
	static final String TRIGGER_SIG = '---created by dedupe trigger---';

	System.debug(DEBUG_PREFIX + 'Inside CampaignMemberOnInsertDeDupeLeads');
	for (CampaignMember newCMember : Trigger.new) {
		if (newCMember.CM_Keywords__c != null && newCMember.CM_Keywords__c.contains(TRIGGER_SIG)) {
			System.debug(DEBUG_PREFIX + 'No dedupe needed');
			return;
		}
	}
	Set<ID> cmIds = Trigger.newMap.keySet();
	// get Leads from all new campaign members
	List<Lead> newLeads = 
	[select Id, Email, OwnerId, QMS_Original_Partner_OwnerId__c, 
		Salutation, Firstname, LastName, Phone, City, State, Street, 
		PostalCode, Country, Company, Industry, Job_Title__c, HasOptedOutOfEmail, 
		LeadSource, RecordTypeId 
	from Lead 
	where Id in (select LeadId from CampaignMember where Id in :cmIds and LeadId != '' and LeadId != null) and RecordTypeId = :RT_PARTEROPPREG and Web_Activity_Source__c = :WA_SOURCE];
		
	if (newLeads.size() == 0){
		return;
	}	
		
	Set<ID> newLeadIds = new Set<ID>();
	List<String> origOwnerIds = new List<String>();
	List<String> newEmails = new List<String>();
	// leads to be deleted
	List<Lead> delLeads = new List<Lead>();
	// Campaign members to be inserted
	List<CampaignMember> insCms = new List<CampaignMember>();
	
	Set<Integer> indexDeleted = new Set<Integer>();
	List<Lead> newLeadsNodupe = new List<Lead>();
	// collection of leads need updated after the de-dupe process
	Map<ID, Lead> updLeads = new Map<ID, Lead>();	
	// map Campaign ID to old lead IDs set
	// CampaignId -> Set<ID>
	Map<ID, Set<ID>> oldCLeads = new Map<ID,Set<ID>>();	
	
	// dedupe amongst new leads
	Integer numNewLds = newLeads.size();
	for (Integer i = 0; i < numNewLds && !indexDeleted.contains(i); i++) {
		Lead leadi = newLeads.get(i);
		newLeadsNodupe.add(leadi);
		for (Integer j = i+1; j < numNewLds && !indexDeleted.contains(j); j++){
			Lead leadj = newLeads.get(j);
			if (leadi.Email == leadj.Email && leadi.QMS_Original_Partner_OwnerId__c == leadj.QMS_Original_Partner_OwnerId__c) {
				indexDeleted.add(j);
				delLeads.add(leadj);
				for (CampaignMember nCMember : Trigger.new) {
					if (nCMember.LeadId == leadj.Id) {
						Boolean iExist = false;
						for (CampaignMember mCMember : Trigger.new) {
							if (mCMember.LeadId == leadi.Id && mCMember.CampaignId == nCMember.CampaignId) {
								iExist = true;
							}
						}
						if (!iExist) {
							CampaignMember cm4Leadj = new CampaignMember(CampaignId = nCMember.CampaignId, LeadId = leadi.Id, Status = nCMember.Status, CM_Keywords__c = TRIGGER_SIG);
							insCms.add(cm4Leadj);
							
							if (!oldCLeads.containsKey(nCMember.CampaignId)){
								Set<ID> leadIdsSet = new Set<ID>();
								oldCLeads.put(nCMember.CampaignId, leadIdsSet);
							}
							oldCLeads.get(nCMember.CampaignId).add(leadi.Id);
						}					
					}
				}
			}
		}
	}
	
	for (Lead nlead : newLeadsNodupe) {
		if (nlead.QMS_Original_Partner_OwnerId__c != null && nlead.QMS_Original_Partner_OwnerId__c != '')
		{
			origownerIds.add(nlead.QMS_Original_Partner_OwnerId__c);
			origownerIds.add(nlead.QMS_Original_Partner_OwnerId__c.substring(0,15));
		}
		newLeadIds.add(nlead.Id);
		if (nlead.Email != null && nlead.Email != '')
		{
			newEmails.add(nlead.Email);
		}
	} 
	System.debug(DEBUG_PREFIX + 'newEmails=' + newEmails);
	System.debug(DEBUG_PREFIX + 'newLeadIds=' + newLeadIds);
	System.debug(DEBUG_PREFIX + 'origownerIds=' + origownerIds);
	System.debug(DEBUG_PREFIX + 'WA_SOURCE=' + WA_SOURCE);
	// get existing Leads that may be duplicated by new Leads
	List<Lead> oldLeads =
	[select Id, Leadsource, Email, OwnerId, QMS_Original_Partner_OwnerId__c, 
		Web_Activity_Source__c 
	from Lead 
	where Email in :newEmails and QMS_Original_Partner_OwnerId__c in :origownerIds and Web_Activity_Source__c = :WA_SOURCE and Id NOT in :newLeadIds];
	
	Set<ID> oldLeadIds = new Set<ID>();
	for (Lead oldLd :oldLeads){
		oldLeadIds.add(oldLd.Id);
	}
	
	for (CampaignMember oldCm : [select Id, LeadId, CampaignId from CampaignMember where LeadId in :oldLeadIds]) {
		if (!oldCLeads.containsKey(oldCm.CampaignId)){
			Set<ID> leadIdSet = new Set<ID>();
			oldCLeads.put(oldCm.CampaignId, leadIdSet);
		}
		oldCLeads.get(oldCm.CampaignId).add(oldCm.LeadId);
	}

	Boolean matchFound = false;
	// checking old leads against new ones	
	for (Lead newLead :newLeadsNodupe) {
		matchFound = false;
		for (Lead oldLead :oldLeads) {
			if (newLead.Email == oldLead.Email && newLead.QMS_Original_Partner_OwnerId__c == oldLead.QMS_Original_Partner_OwnerId__c) {
				/* duplicate lead found */
				matchFound = true;
				if (!updLeads.ContainsKey(oldLead.Id)) {
					updLeads.put(oldLead.Id, oldLead);
				}
				Lead oLead = updLeads.get(oldLead.Id);
				System.debug(DEBUG_PREFIX + 'old lead = ' + oLead);
				oLead.Salutation = newLead.Salutation;
		 		oLead.FirstName = newLead.Firstname;
		 		oLead.LastName = newLead.LastName;
				oLead.Phone = newLead.Phone;
				oLead.City = newLead.City;
				oLead.State = newLead.State;
				oLead.Street = newLead.Street;
				oLead.PostalCode = newLead.PostalCode;
				oLead.Country = newLead.Country;
				oLead.Company = newLead.Company;
				oLead.Industry = newLead.Industry;
				oLead.Job_Title__c = newLead.Job_Title__c;
				oLead.HasOptedOutOfEmail = newLead.HasOptedOutOfEmail;
				oLead.RecordTypeId = newLead.RecordTypeId;
				oLead.LeadSource = newLead.LeadSource;
				oLead.Status = RAWPROSPECT;
				for (CampaignMember newCm : Trigger.new) {
					if (newCm.LeadId == newLead.Id) {
						// if oLead not already a campaign member
						if (!oldCLeads.Containskey(newCm.CampaignId) || !oldCLeads.get(newCm.CampaignId).contains(oLead.Id)){
							CampaignMember cm4OldLead = new CampaignMember(CampaignId = newCm.CampaignId, LeadId = oLead.Id, Status = newCm.Status, CM_Keywords__c = TRIGGER_SIG);
							insCms.add(cm4OldLead);					
						}						
					}
				}
			}
		}
		if (matchFound){
			delLeads.add(newLead);
		}
		else {
			newLead.Status = RAWPROSPECT;
			updLeads.put(newLead.Id, newLead);
		}
	}
	if (insCms.size() > 0){
		insert insCms;
		System.debug(DEBUG_PREFIX + 'after inserting insCms');
	}

	if (updLeads.size() > 0) {
		update updLeads.values();
		System.debug(DEBUG_PREFIX + 'after updating updLeads');
	}
	
	if(delLeads.size() > 0) {
		delete delLeads;
		System.debug(DEBUG_PREFIX + 'after deleting delLeads');
	}
}