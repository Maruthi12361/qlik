/******************************************************

	Trigger: User_PortalUser
	
	Trigger to update field "Portal user active" on Contact object, when Portal user is activated/deactivated

	Initiator: CR# 97619
	
	Changelog:
		2017-01-11	BAD	 Created file 
		2017-09-11	UIN	 CHG0030632	Modified to update disable salesforce 1 field for partner portal users
******************************************************/
trigger User_PortalUser on User (before insert, after insert, after update) {

	List<Id> ContactsID = new List<Id>();
	List<Id> UserIds = new List<Id>();

	if(Trigger.isInsert)
	{
		for (Integer i = 0; i < Trigger.New.size(); i++)
		{
			/* UIN Commented CHG0030632
			if(Trigger.New[i].IsActive && Trigger.New[i].Profile__c.toUpperCase().startswith('PRM'))
			{
				ContactsID.add(Trigger.New[i].ContactId);
			} **/
			//Start UIN Changed CHG0030632
			if(Trigger.New[i].IsActive && Trigger.New[i].Profile__c.toUpperCase().startswith('PRM') && Trigger.isAfter)
			{
				ContactsID.add(Trigger.New[i].ContactId);
			}else if(Trigger.New[i].IsActive && Trigger.New[i].Profile__c.toUpperCase().startswith('PRM') && Trigger.isbefore){
				Trigger.New[i].UserPreferencesHideS1BrowserUI = true;
			}//End UIN Changed	CHG0030632	 
		}
	}

    if (Trigger.isUpdate)
    {
		for (Integer i = 0; i < Trigger.New.size(); i++)
		{
			if(Trigger.Old[i].IsActive != Trigger.New[i].IsActive && Trigger.New[i].Profile__c.toUpperCase().startswith('PRM'))
			{
				ContactsID.add(Trigger.New[i].ContactId);
			}
		}
	}
	
	if (ContactsID.size() > 0)
	{
		UserPortalUserHandler.UpdateContacts(ContactsID);
	}
	
}