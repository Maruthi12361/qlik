/******************************************************

    Trigger: OnCreate_ULCDetails
    
    Changelog:
    
    Shubham Goyal, QE-1820 2017-12-27 generating ulc details within qlik account and execute trigger to create lead/contact.
******************************************************/
trigger OnCreate_ULCDetails on ULC_Details__c (after insert, after update, before insert, before update) {
    HerokuUtillities.ULCData ULCData = new HerokuUtillities.ULCData ();
    List<Web_Activity__c> lstWA = new List<Web_Activity__c>();

    if(Trigger.isInsert) {
        if(Trigger.isAfter) { 
            for(integer i=0; i < Trigger.New.size(); i++)
            {
                if(Trigger.New[i].oUser__c != null && !Trigger.New[i].oUser__c.contains('QSCBTW'))
                {
                    Web_Activity__c wa = new Web_Activity__c();
                    wa.name = Trigger.New[i].Name;   
                    wa.Type__c = 'LeadGeneration';
                    wa.Sender__c = 'QlikId';
                    //wa.TimeStamp__c = DateTime.now();
                    wa.ULC_Username__c = Trigger.New[i].ULCName__c;   
                    wa.Content__c = Trigger.New[i].oUser__c;   
                    lstWA.add(wa);                 
                }
            }
            if(lstWA.size() > 0)
            {
                insert lstWA;
            }
        }
    }
}