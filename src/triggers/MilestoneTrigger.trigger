/****************************************************************
*
* MilestoneTrigger
*
* 2019-04-24 extbad IT-1742 Update pse__Assignments after change 'Closed for Timecard Entry' field
*
*****************************************************************/
trigger MilestoneTrigger on pse__Milestone__c (before update) {

    if (Trigger.isUpdate) {
        MilestoneHandler.updateAssignments(Trigger.newMap, Trigger.oldMap);
        MilestoneHandler.updateTimecards(Trigger.newMap, Trigger.oldMap);
    }
}