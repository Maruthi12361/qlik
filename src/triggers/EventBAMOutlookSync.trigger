/******************************************************
Purpose:
-------
Setup whatId, whoId, WhatEntityName from Related To

======================================================
======================================================
Log History
------- 
AUTHOR      	DATE        DETAIL
Raquel Deniz	2012-05-09  Initial development.

*******************************************************/

trigger EventBAMOutlookSync on Event (before insert, before update, after insert, after update) {
	
	//Event.whatId is polymorphic. 
	Event eclone;
	List<Event> ToUpdate = new List<Event>();
	List<Event> EventsToCheckAfter = new List<Event>();
	Set<Id> EventIdsAfter = new Set<Id>();
	String BAMEventTypes = null;
	Event oldEvent, newEvent;
	
	try
	{
		QTCustomSettings__c BAMSetting = QTCustomSettings__c.getValues('Default');
		if (BAMSetting != null)
		{
			BAMEventTypes  = BAMSetting.BAMEventTypes__c;
			
			if (BAMEventTypes == null) 
			{
				System.debug('No BAMEventTypes for EventBAMOutlookSync could be found!');
			}
		}
	}
	catch (Exception e)
	{
		System.debug('Could not Find Valid Event Types for BAM Outlook Sync');
	}

	for (Integer i=0; i<trigger.new.size(); i++)
	{
		newEvent = trigger.new[i];
		if (trigger.isUpdate && trigger.isBefore)
		{
			oldEvent = trigger.old[i];
		
			//If WhatId has not changed continue
			if ((newEvent != null) &&(newEvent.WhatId__c == oldEvent.WhatId__c) && (newEvent.WhoId__c == oldEvent.WhoId__c))
				continue;
		}	
		
		//if Event type == EV - Presales Activity then add to list
		if (newEvent.Type != null && BAMEventTypes.ToLowerCase().contains(newEvent.Type.ToLowerCase()))
		{
			if (trigger.isBefore)
			{
				//If its before Update/Insert then set WhatId and WhoId to values passed by WhatId__c and WhoId__c
				if ((newEvent.WhoId__c != null) && (newEvent.WhatId != newEvent.WhatId__c))
				{
					newEvent.WhatId = newEvent.WhatId__c;					
				}
				if ((newEvent.WhoId__c != null) && (newEvent.WhoId != newEvent.WhoId__c))
				{
					newEvent.WhoId = newEvent.WhoId__c;										
				}
			}
			if (trigger.isAfter)
			{
				EventsToCheckAfter.add(newEvent);	
				EventIdsAfter.add(newEvent.Id);	
			}
			
		}	
		
	}
	
	
	//Check events in After Update Insert to set the WhatEntityName__c by cloning the event
	if(EventsToCheckAfter.size()>0)
	{
		for (Event e : [select WhatEntityName__c, WhatId__c, WhoId__c, what.Id, what.Type, Type, WhatId, WhoId from Event where Id = :EventIdsAfter])
		{
			System.Debug('-----------Start After Update/Insert--------------');
			System.Debug('e.WhatId='+e.WhatId);
			System.Debug('e.WhoId='+ e.WhoId);
			System.Debug('e.what.Type='+ e.what.Type);
			System.Debug('e.WhatEntityName__c='+ e.WhatEntityName__c);
			System.Debug('e.Type='+e.Type);
			System.Debug('-----------End--------------');
			if ((BAMEventTypes != null) && !(BAMEventTypes.toLowerCase().contains(e.Type.toLowerCase())))				
				continue;
			if ((e.what != null) && (e.WhatEntityName__c != e.what.Type))
			{
				System.Debug('Setting value WhatEntityName__c to e.what.Type');
				System.Debug('e.e.what.Type = '+e.WhatEntityName__c);
				System.Debug('e.what.Type='+ e.what.Type);
				System.Debug('-----------End--------------');
				eclone = e.clone();
				eclone.WhatEntityName__c = e.what.Type;				
				ToUpdate.add(eclone);
			}	
		}	
	}
	
	
	
	if (ToUpdate.size()>0)
	{
		update ToUpdate;	
	}
	
}