/********************************************************************************
Name            : ExpenseReportAssignProjectManagerToApprover
Author          : Jason Favors (Appirio)
Created Date    : September 22, 2010
Usage           : Assigned apprver for expenses reports
Change Log      :
20150818        RDZ     Re-saving file to make it isValid in production
*********************************************************************************/

trigger ExpenseReportAssignProjectManagerToApprover on pse__Expense_Report__c (before insert) {
    // Update Approver in expense reports
    AssignProjectManagerToApprover.assignApproversForExpenseReports(Trigger.new);
}