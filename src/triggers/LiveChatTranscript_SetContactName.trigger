/****************************************************************************
*
*   LiveChatTranscript_SetContactName
*   
*   Set the transcipt Contact based upon the email address saved from
*   the Live Agent preChat Form  
*                     
*   Test Class: TestLiveChatTranscript_SetContactName

*****************************************************************************
CHANGE LOG:
    
CR# 78698 - Created new Customer_Email__c field instead of email__c in order to be able to use
workflow with a standard Email field type (Not Text)
**********************cs86***************************************************/


trigger LiveChatTranscript_SetContactName on LiveChatTranscript (before insert, before update) 
{
    //return;
    //Start by getting a list of the Emails for each LiveChatTranscript
    List<string> LCTEmail = new List<string>();
    for (LiveChatTranscript LCT : Trigger.New)
    {
        if (LCT.Customer_Email__c != null) {
            LCTEmail.Add(LCT.Customer_Email__c);  //Add the email address 
            System.debug('LCT.Customer_Email__c = ' + LCT.Customer_Email__c);   
        }
    }
    
    //Get a list of contacts matching the email addresses
    Map<Id, Contact> contactsMap = New Map<Id, Contact>([Select Id,Email FROM Contact WHERE email in :LCTEmail]);
    
    //Update the LiveChatTranscripts with the related contact
    for (LiveChatTranscript LCT : Trigger.New)
    {
        if (LCT.Customer_Email__c != null) {
        for (contact pContact : contactsMap.values())
        {
            if (pContact.email != null & pContact.email.equals(LCT.Customer_Email__c)) {
            LCT.ContactId = pContact.Id;
            System.debug('LCT.Contact.Id = ' + LCT.Contact.Id);
            }
        }
        } 
    }
}