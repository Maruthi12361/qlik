/**********************************************************
* Name:			PseProjectTrigger
* Object:      	pse__Proj__c
* Author:		ext_vos
* Date:			2017-11-13
* Description: 	Handles all actions for Project.
*               
* Changes Log:
*   Date     	Author		CR#		Description
*	2017-11-15	ext_vos				Added Semaphores.
*************************************************************/
trigger PseProjectTrigger on pse__Proj__c (before insert, before update, after insert, after update, after delete) {

	if (Trigger.isBefore) {
		if (Trigger.isInsert && !Semaphores.PseProjectTriggerBeforeInsert) {
			Semaphores.PseProjectTriggerBeforeInsert = true;
			PseProjectTriggerHandler.handleBeforeInsert(Trigger.new);
		} else if (Trigger.isUpdate && !Semaphores.PseProjectTriggerBeforeUpdate) {
			Semaphores.PseProjectTriggerBeforeUpdate = true;
			PseProjectTriggerHandler.handleBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
		}
	} else if (Trigger.isAfter) {
		if (Trigger.isInsert && !Semaphores.PseProjectTriggerAfterInsert) {
			Semaphores.PseProjectTriggerAfterInsert = true;
			PseProjectTriggerHandler.handleAfterInsert(Trigger.new);
		} else if (Trigger.isUpdate && !Semaphores.PseProjectTriggerAfterUpdate) {
			Semaphores.PseProjectTriggerAfterUpdate = true;
			PseProjectTriggerHandler.handleAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
		} else if (Trigger.isDelete && !Semaphores.PseProjectTriggerAfterDelete) {
			Semaphores.PseProjectTriggerAfterDelete = true;
			PseProjectTriggerHandler.handleAfterDelete(Trigger.old);
		}
	}
}