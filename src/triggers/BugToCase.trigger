/********************************************************
* Trigger: BugToCase
* Description: Populate/ unpopulate/ bug lookup field on Case record to point to the originating Bug when Bug is saved with Lookup value to a Case 
* (CR# 15361)
*
*   Change Log:
*   ------- 
*   VERSION AUTHOR      DATE        DETAIL
*                                            
*       1.  AIN         2014-09-05  Initial Development
*                                   CR# 15361 https://eu1.salesforce.com/a0CD000000k1d2G
*                                   Changes related to JIRA and SFdC integration
*   
**********************************************************/

trigger BugToCase on Bugs__c (after insert) 
{
    Map<Id, Id> mapBugCase = new Map<Id, Id>();
    List<Case> updateCases = new List<Case>();
    List<Bugs__c> updateBugs = new List<Bugs__c>();


    for(Bugs__c newBugRec : trigger.new)
    {
        //If there is a case on the bug record, add it to the map for 2 reasons
        //To use the case id in the key and update the case with the bug in the value
        //Use the values to update the bugs and set the field LookupForPublisherActionToCase__c to null 
        //so the trigger doesn't update the same record twice.
        if(newBugRec.LookupForPublisherActionToCase__c != null)
        {
            // Adding to Map Case id, Bug Id
            mapBugCase.put(newBugRec.LookupForPublisherActionToCase__c, newBugRec.Id);
        }
    }

    if(mapBugCase.KeySet() != null && (!mapBugCase.KeySet().isEmpty()))
    {
        //Query the cases that have the IDs found on the bug records
        for(Case caseObj : [Select Bug__c, Id from Case where (Id in : mapBugCase.KeySet())])
        {
            //Make sure the id is in the keyset
            if(mapBugCase.keySet().contains(caseObj.Id))
            {
                //Check if the field needs to be updated
                Id bugId = mapBugCase.get(caseObj.Id);
                if(bugId != caseObj.Bug__c)
                {
                    caseObj.Bug__c = bugId;
                    updateCases.add(caseObj);
                }
                else
                    system.debug('Bug is already assigned to the correct case');
            }
        }
        //Only update if there are changed records
        if(updateCases != null && (!updateCases.isEmpty()))
        {
            try
            {
                // Update case Records
                update updateCases;
            }
            catch(System.Exception excep)
            {
                System.debug('Case Update Error'+ excep.getMessage());
                trigger.new[0].addError('Case Update Error');
            }
        }
    }
}