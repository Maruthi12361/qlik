/******************************************************
Name:  User_UpdateExternalSAMLId.trigger

Purpose:
-------

Updates the ExternalSAMLId on insert and update. If 
user is an employee the fields should be fedid@qlikview.com
else it should be the user email. This cannot be done 
using a workflow since FedId is not available.

History
------- 
VERSION AUTHOR      DATE        DETAIL
1.0     MHG         2013-03-01  Initial development.      
1.1     MAW         2014-08-19  If there is prime user then use Prime users' ExternalSAMLId (NS Issue # 2647) 
2017-04-26  UIN Modified logic to reduce SOQL queries             
******************************************************/ 
trigger User_UpdateExternalSAMLId on User (before insert, before update) {

    //Load users with prime users information as this is not provided in related record
    Set<Id> primeUserIds = new Set<Id>();
    List<User> lUsers = new List<User>();
    User pUsr;
/* UIN commented for improving soql query count
    for (User u : Trigger.New)
    {
        if(u.Prime_User__c != null){primeUserIds.add(u.Prime_User__c );}
    }
    //List<User> PrimeUsers= new List<User> ([SELECT Id, ExternalSAMLId__c FROM User WHERE Id in: primeUserIds ]);
    Map<Id, User> PrimeUsers= new Map<Id, User> ([SELECT Id, ExternalSAMLId__c FROM User WHERE Id in: primeUserIds ]);

    //Populate ExternalSAMLId now
    for (User u : Trigger.New)
    {
        if (u.Username.toLowerCase().Contains('@qlikview.com') || u.Username.toLowerCase().Contains('@qliktech.com'))
        {
            //If there is prime user then use Prime users' ExternalSAMLId (NS Issue # 2647) Otherwise create ExternalSAMLId
            System.Debug('-----u.Prime_User__c----' +  u.Prime_User__c); 
            if(u.Prime_User__c != null)
            {
                if(PrimeUsers.containsKey(u.Prime_User__c)){
                    User pUsr = PrimeUsers.get(u.Prime_User__c);
                    System.Debug('-----Prime_User.ExternalSAMLId__c----' +  pUsr.ExternalSAMLId__c); 
                    u.ExternalSAMLId__c = pUsr.ExternalSAMLId__c;
                }
            }
            else
            {
                u.ExternalSAMLId__c = u.FederationIdentifier + '@qlikview.com';
            }
        } else {
            u.ExternalSAMLId__c = u.Email;          
        }
    }
UIN commented for improving soql query count*/
    if(Trigger.isBefore && Trigger.isInsert){
        
        for (User u : Trigger.New){
            if(u.Username.toLowerCase().Contains('@qlikview.com') || u.Username.toLowerCase().Contains('@qliktech.com')){
                if(u.Prime_User__c != null)
                    primeUserIds.add(u.Prime_User__c );
                lUsers.add(u);
            }else{
                u.ExternalSAMLId__c = u.Email;
            }
        }
        if(!lUsers.isEmpty()){
            Map<Id, User> PrimeUsers= new Map<Id, User> ([SELECT Id, ExternalSAMLId__c FROM User WHERE Id in: primeUserIds ]);
            for(User u: lUsers){
                if(u.Prime_User__c != null && !PrimeUsers.isEmpty()){
                    if(PrimeUsers.containsKey(u.Prime_User__c)){
                        pUsr = PrimeUsers.get(u.Prime_User__c);
                        if(pUsr != null)
                            u.ExternalSAMLId__c = pUsr.ExternalSAMLId__c;
                    }
                }else{
                    u.ExternalSAMLId__c = u.FederationIdentifier + '@qlikview.com';
                }
            }
        }
    }else if(Trigger.isBefore && Trigger.isUpdate){
        for (User u : Trigger.New){
            if(String.isBlank(u.ExternalSAMLId__c) || ((String.isNotBlank(u.Email) && u.email.equalsIgnoreCase(Trigger.oldMap.get(u.id).email)) ||
                (String.isNotBlank(u.FederationIdentifier) && u.FederationIdentifier.equalsIgnoreCase(Trigger.oldMap.get(u.id).FederationIdentifier)) ||
                    (String.isNotBlank(u.Prime_User__c) && u.Prime_User__c!=Trigger.oldMap.get(u.id).Prime_User__c) || 
                        (String.isNotBlank(u.Username) && u.Username.equalsIgnoreCase(Trigger.oldMap.get(u.id).Username) && 
                                (u.Username.toLowerCase().Contains('@qlikview.com') || u.Username.toLowerCase().Contains('@qliktech.com'))))){
                    if(u.Prime_User__c != null)
                        primeUserIds.add(u.Prime_User__c);
                    lUsers.add(u);
            }    
        }
        if(!lUsers.isEmpty()){
            Map<Id, User> PrimeUsers= new Map<Id, User> ([SELECT Id, ExternalSAMLId__c FROM User WHERE Id in: primeUserIds]);
            for(User u: lUsers){
                if (u.Username.toLowerCase().Contains('@qlikview.com') || u.Username.toLowerCase().Contains('@qliktech.com')){
                    if(u.Prime_User__c != null && !PrimeUsers.isEmpty()){
                        if(PrimeUsers.containsKey(u.Prime_User__c)){
                            pUsr = PrimeUsers.get(u.Prime_User__c);
                            if(pUsr != null)
                                u.ExternalSAMLId__c = pUsr.ExternalSAMLId__c;
                        }
                    }else{
                        u.ExternalSAMLId__c = u.FederationIdentifier + '@qlikview.com';
                    }
                }else {
                    u.ExternalSAMLId__c = u.Email;          
                }
            }
        }
    }
}