/**
* DiagnosticKnowledgeArticlesTrigger
* 
* Description:	Trigger for Diagnostic Knowledge Articles.
* Added: 		8-11-2018 - ext_vos - CHG0034844 
*               6-11-2019 - AIN - IT-2229 - Removed the before update trigger condition.
*
* Change log:
*
*/
trigger DiagnosticKnowledgeArticlesTrigger on Diagnostic__kav (before insert, before update, before delete, after insert, after update, after delete) {
	if (Trigger.isUpdate) {
		if (Trigger.isBefore && !Semaphores.DiagnosticKnowledgeArticlesTriggerBeforeUpdate) {
			Semaphores.DiagnosticKnowledgeArticlesTriggerBeforeUpdate = true;
			PushKnowledgeArticlesToKPBHandler.articleChannelsPopulation(Trigger.new, 'Diagnostic__kav');
		} 
	}
}