/************************************************
* Name: CampaignMemberStatus_Trigger
* Does: Will get fired when CampaignMember Status changes its value.
*       If New value contains '- Trigger' will remove this part of the string from the Status
*       And set specific values from Contact and Lead related to 
*       the Campaign member into Campaing Member obj.
*                                                       
* Log Changes:
* 01/12/2011    RDZ CR# 3089    https://eu1.salesforce.com/a0CD000000EXTNR
* 27/07/2012    RDZ CR# 4327    https://eu1.salesforce.com/a0CD000000G1NjL
*                   Making that sitecore forms will clear up the values from Contacts and Leads after the CampaignMember is created
* 16/05/2013    YKA CR# 8148:   https://eu1.salesforce.com/a0CD000000YHuFd
*                               (Fix CampaignMemberStatus_Trigger)
*                               Uncommenting the section for Non triggerPattern values and dealing with the errors 
*
* 2015-08-10 Linus Löfberg @ 4front - CR# 52109 - Fix CM Source fields on Campaign Member
* - Added checks for preventing overwriting previous values on second campaign response.
* 2018-05-30 CCE CHG0033789 BMW-751 Add new Source URL field with larger character limit
* 2019-02-21 CRW BMW-1216 Adding a check for 6 users to give access to modify CM Source Site field
* 2019-04-17 CRW/CCE CHG0035768 BMW-1404 Event flow model for Conversica 
****************************************************/

trigger CampaignMemberStatus_Trigger on CampaignMember (before insert, after insert, before update, after update) {

    List<CampaignMember> objsToUpdate = new List<CampaignMember>();
    //List of CampaignMembers to check if their contact or lead have the same value on specific fields
    //If the value is stored in the CampaignMember, then the value will be clear out from Lead or Contact
    //List of fields to check can be found in CampaignMember_Helper Class.
    List<CampaignMember> objsToCheckAndUpdate = new List<CampaignMember>();
    
    integer index;
    String TriggerPattern = 'xxDoNotUse MKTOTrigger' ;//' - Trigger';
    System.Debug('-------------------------------');
    System.Debug('In CampaignMemberStatus_Trigger');
    List<CampaignMember> CMList = new List<CampaignMember>();
    
    if (Trigger.isBefore) {
        for (CampaignMember NewValue : Trigger.new) {
            System.Debug('NewValue.Status:' + NewValue.Status);
        
            System.Debug('New value contains Pattern:' + TriggerPattern);
            
            if (NewValue.Status != '' && NewValue.Status != null && NewValue.Status.contains(TriggerPattern))
            {
                //System.Debug('Replacing pattern:' + NewValue.Status.replace(TriggerPattern, ''));
            
                NewValue.Status = NewValue.Status.replace(TriggerPattern, '').trim();
                objsToUpdate.add(NewValue);
                                
                System.Debug('NewValue:' + NewValue.LeadId + ';' + NewValue.ContactId);
            }
            // YKA CR # 8148 uncommenting this else block
            // SLH CR# 8148 - have to roll-back the chnage again :-( 
            /*
            else
            {
                //If its from a sitecore form or other
                System.Debug('Adding Checking if Lead/Contact need to clear off values: ' + NewValue.Id);
                objsToCheckAndUpdate.add(NewValue);
            }
            */

            //CR# 52109 - START
    		if (Trigger.isUpdate) {
                HardcodedValuesQ2CW__c settings = HardcodedValuesQ2CW__c.getOrgDefaults();
                List<Id> AllProfileIds = settings.CM_SourceURL_field_Access__c.split(',');
                //Set<String> ProfileIds = new Set<String>();
                Boolean EditAccessFlag = false;
                if (AllProfileIds.size() > 0){
                    for (Id ild:AllProfileIds){
                        if (ild == UserInfo.getUserId()){EditAccessFlag = true;}
                    }
                }
                system.debug('string'+AllProfileIds);
                //system.debug('setids'+ProfileIds);
                system.debug('flag'+EditAccessFlag);
                CampaignMember OldValue = Trigger.oldMap.get(NewValue.Id);
                if(!String.isBlank(OldValue.CM_Source_Partner__c) && NewValue.CM_Source_Partner__c != OldValue.CM_Source_Partner__c) {
                    NewValue.CM_Source_Partner__c = OldValue.CM_Source_Partner__c;
                }
                if(!String.isBlank(OldValue.CM_Source_Site__c) && NewValue.CM_Source_Site__c != OldValue.CM_Source_Site__c && EditAccessFlag == false) {
                    NewValue.CM_Source_Site__c = OldValue.CM_Source_Site__c;
                }
                if(!String.isBlank(OldValue.CM_Source_ID2__c) && NewValue.CM_Source_ID2__c != OldValue.CM_Source_ID2__c) {
                    NewValue.CM_Source_ID2__c = OldValue.CM_Source_ID2__c;
                }
                if(!String.isBlank(OldValue.CM_Source_URL_NEW__c) && NewValue.CM_Source_URL_NEW__c != OldValue.CM_Source_URL_NEW__c) {
                    NewValue.CM_Source_URL_NEW__c = OldValue.CM_Source_URL_NEW__c;
                }
                if(!String.isBlank(OldValue.CM_Keywords__c) && NewValue.CM_Keywords__c != OldValue.CM_Keywords__c) {
                    NewValue.CM_Keywords__c = OldValue.CM_Keywords__c;
                }
                ////changes for conversica phase 2 BMW-1404
                //if (!String.isBlank(NewValue.Status)  && NewValue.Status != OldValue.Status && 
                //    (NewValue.Status =='Attended' || NewValue.Status =='Attended - Walk In' || NewValue.Status =='No show' || NewValue.Status =='New - Viewed On Demand')){
                //    CMList.add(NewValue);
                //}
            }

            //CR# 52109 - END
            System.Debug('-------------------------------');
        }
    }
    //changes for conversica phase 2 BMW-1404
    if (Trigger.isAfter && Trigger.isInsert) {
        System.Debug('CampaignMemberStatus_Trigger after insert');
        for (CampaignMember NewValue : Trigger.new) {
            if (String.isNotBlank(NewValue.Status) && 
                (NewValue.Status =='Attended' || NewValue.Status =='Attended - Walk In' || NewValue.Status =='No show' || NewValue.Status =='New - Viewed On Demand')){
                CMList.add(NewValue);
            }
        }
    }
    //changes for conversica phase 2 BMW-1404
    if (Trigger.isAfter && Trigger.isUpdate) {
        System.Debug('CampaignMemberStatus_Trigger after update');
        for (CampaignMember NewValue : Trigger.new) {
            CampaignMember OldValue = Trigger.oldMap.get(NewValue.Id);
            if (String.isNotBlank(NewValue.Status) && NewValue.Status != OldValue.Status && 
                (NewValue.Status =='Attended' || NewValue.Status =='Attended - Walk In' || NewValue.Status =='No show' || NewValue.Status =='New - Viewed On Demand')){
                CMList.add(NewValue);
            }
        }
    }

    if (CMList.size() > 0){ //changes for conversica phase 2 BMW-1404
        List<String> CMID = new List<String>();
        system.debug('CampaignMemberStatus_Trigger CMList = ' + CMList);
        
        For (CampaignMember CMRecord:CMList) {
            system.debug('CampaignMemberStatus_Trigger CMRecord = ' + CMRecord);
            system.debug('CampaignMemberStatus_Trigger CMRecord.Id = ' + CMRecord.Id);
            CMID.add(CMRecord.Id);
        }
        system.debug('CampaignMemberStatus_Trigger CMID = ' + CMID);
        CampaignMemberHelper.PopulateConversicaEventFieldsOnUpdate(CMList, CMID);
    }
    if(objsToUpdate != null && objsToUpdate.size()>0)
    {
        CampaignMemberHelper.setupCampaignMember(objsToUpdate); 
        System.Debug('Campaigns to update:'+ objsToUpdate);
    }
    if(objsToCheckAndUpdate != null && objsToCheckAndUpdate.size()>0)
    {
        CampaignMemberHelper.checkAndCleanValues(objsToCheckAndUpdate);
        System.Debug('Campaigns to update:'+ objsToCheckAndUpdate);
    }
}