/**
* Change log:
*
*   2019-05-22 ext_vos	CHG0036145: Add onAfterInsert and onAfterUpdate methods.
*   2019-09-16 ext_ciz IT-1821: Add onAfterDelete methods
*/
trigger PseTimecardHeaderTrigger on pse__Timecard_Header__c (
        before insert, before update,
        after insert, after update,
        before delete, after delete) {

    if (Trigger.isAfter) {
        if (Trigger.isInsert && !Semaphores.PseTimecardHeaderTriggerAfterInsert) {
            Semaphores.PseTimecardHeaderTriggerAfterInsert = true;
            PseTimecardHeaderTriggerHandler.onAfterInsert(Trigger.newMap);
        }
        if (Trigger.isUpdate && !Semaphores.PseTimecardHeaderTriggerAfterUpdate) {
            Semaphores.PseTimecardHeaderTriggerAfterUpdate = true;
            PseTimecardHeaderTriggerHandler.onAfterUpdate(Trigger.newMap);
        }
        if (Trigger.isDelete && !Semaphores.PseTimecardHeaderTriggerAfterDelete) {
            Semaphores.PseTimecardHeaderTriggerAfterDelete = true;
            PseTimecardHeaderTriggerHandler.onAfterDelete(Trigger.oldMap);

        }
    }
}