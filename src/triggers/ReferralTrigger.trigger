trigger ReferralTrigger on Referral__c (before update) {
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            ReferralTriggerController.validateOpportunityAssociationBeforeApproval(Trigger.New, Trigger.OldMap);
            ReferralTriggerController.validateRejectionReasonForRejectedReferral(Trigger.New, Trigger.OldMap);
        }
    }
}