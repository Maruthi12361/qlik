/******************************************************

    Changelog:
        2017-05-25  CCE     CHG0031108 - Updates to Operations Request - Initial development
                            When an Attachment is added to an Operations Request record of Marketo Support 
                            Requests record type we set a flag on the record (a workflow will then use this flag to send an email)
                        
******************************************************/
trigger AttachmentTrigger on Attachment (after insert) {
    // Get List of Object Ids to notify on
    Set<Id> objectIds = new Set<Id>();
    
    // Loop through list of attachments, and if attachment is associated to object we want to notify on, add Parent Id to objectIds set
    for(Attachment a:trigger.new){
         String keyPrefix = String.valueOf(a.ParentId).substring(0, 3);
         
         if(keyPrefix == 'a3c'){  //a3c = Operations Request object prefix
            objectIds.add(a.ParentId);
         }
    }
    
    // Get the objects we want to notify on, and set the Send Attachment Notification Email field to True 
    // This will to fire the workflow rule to send the email
    if(objectIds.size() > 0){
        List<Marketing_Request__c> mr = [SELECT Id, RecordTypeId FROM Marketing_Request__c WHERE Id IN :objectIds];
        
        for(Marketing_Request__c obj:mr){
            if (obj.RecordTypeId == '012D0000000KJMD') { //Marketo Support Requests record type
                obj.Send_Attachment_Email__c = true;
            }
        }
        
        if(mr.size() > 0){
            update mr;
        }       
    }     
}