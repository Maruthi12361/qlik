/**
*   extcqb  2020-01-30 IT-2196 Make chatter posts about case attachments 'QlikTech Only'
*/

trigger AttachmentUpdateForCase on Attachment (after insert) {
    List<Attachment> caseAttachments = new List<Attachment>();
    for(Attachment attachment : Trigger.new){
        if(attachment.ParentId.getSobjectType() == Case.SObjectType){
            caseAttachments.add(attachment);
        }
    }
    if (!caseAttachments.isEmpty()) {
        CaseAttachmentHandler.handleCaseAttachments(caseAttachments);
    }

}