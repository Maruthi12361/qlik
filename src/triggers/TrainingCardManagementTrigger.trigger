/**
* Change log:
*
*   2018-07-04 ext_vos	CHG0033919: Create trigger. Sending the notification about reselling of training card.
*   2019-03-05 ext_vos	CHG0035575: Add onAfterInsert method.
*/
trigger TrainingCardManagementTrigger on Voucher_Management__c (
											before insert, before update,
									        after insert, after update,
									        before delete, after delete) {

	if (Trigger.isAfter) {
		if (Trigger.isInsert && !Semaphores.TrainingCardManagementTriggerAfterInsert) {
			Semaphores.TrainingCardManagementTriggerAfterInsert = true;
			TrainingCardManagementTriggerHandler.onAfterInsert(Trigger.newMap);
		}
	}
}