trigger CustomProductTrigger on Product2 (after insert, after update, before insert, before update) {
    if(Trigger.isInsert) {
        if(Trigger.isAfter) {
        
        }
        if(Trigger.isBefore) {
        	ProductMultiSelectHandler.onBeforeInsert(Trigger.new);
        }
    }
    if(Trigger.isUpdate) {
        if(Trigger.isAfter) {
        
        }
        if(Trigger.isBefore) {
        	ProductMultiSelectHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}