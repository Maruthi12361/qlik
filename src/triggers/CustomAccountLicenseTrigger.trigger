/**     * File Name:CustomAccountLicenseTrigger
        * Description : This is the common trigger to call all the account license handlers
        * @author : Ramakrishna Kini
        * Modification Log ===============================================================
        Ver     Date         Author         Modification 
        1.0 Feb 3rd 2017 RamakrishnaKini     Added new trigger logic.
        1.2 July 23rd 2017 QCW-3462 RamakrishnaKini  Modified logic to pass old map version in after update context
*/
trigger CustomAccountLicenseTrigger on Account_License__c (after insert, after update, before delete) {
	if (Trigger.isAfter) {
		if(Trigger.isInsert){
			AccLicenseCreateProdLicenseHandler.onAfterInsert(Trigger.new, Trigger.newMap);
		}else if(Trigger.isUpdate){
			AccLicenseCreateProdLicenseHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
		}
	}
	if(Trigger.isBefore) {
		if(Trigger.isDelete){
			AccLicenseCreateProdLicenseHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
		}
	}
}