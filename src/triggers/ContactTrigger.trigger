/*
 File Name: ContactTrigger

 2017-12-22 Shubham Gupta QCW-4610 Trigger consolidation.
 2019-02-20 BAD - Qlik Commerce - removing sharing
 2019-06-12 extbad IT-1923 Disabled Contact Duplicate rule for chats
*/

trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

        if(Trigger.isInsert){
            if(Trigger.isBefore){
                if(!Semaphores.ContactTriggerHandlerBeforeInsert)
                {
                    Semaphores.ContactTriggerHandlerBeforeInsert = true;
                    
                    ContactOnInsertUpdateHelper.updateAccountOwner(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactOnInsertUpdateHelper.cleanSkipDuplicateRule(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    UpdateFollowUpRequiredTypeHelper.updateFollowUpRequiredType(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactCallMarketoWebServiceHelper.callServiceOnBefore(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    TriggerPortalControlHelper.checkPortalControl(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            }
            if(Trigger.isAfter){
                if(!Semaphores.ContactTriggerHandlerAfterInsert){
                    Semaphores.ContactTriggerHandlerAfterInsert = true;
                    PseCustomResourceHelper.psePermissionControlOnInsert(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactCallMarketoWebServiceHelper.callServiceOnAfter(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    //Contact_SharingHelper.shareContactOnInsert(Trigger.new);
                    //System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            }
        }

        if(Trigger.isUpdate){
            if(Trigger.isBefore){
                if(!Semaphores.ContactTriggerHandlerBeforeUpdate){
                    Semaphores.ContactTriggerHandlerBeforeUpdate = true;
                    ContactOnInsertUpdateHelper.updateAccountOwner(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactOnInsertUpdateHelper.cleanSkipDuplicateRule(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactOnInsertUpdateHelper.validateSOIContact(Trigger.new, Trigger.old, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactOnInsertUpdateHelper.suspectRecTypeUpdate(Trigger.new, Trigger.old, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    PseCustomResourceHelper.psePermissionControlOnUpdate(Trigger.new, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactSetOwnerHelper.setOwner(Trigger.new, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactStatusAgeHelper.conStatusAgeUpdate(Trigger.new, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactPartnerPortalRelaywareHandler.handle(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    UpdateFollowUpRequiredTypeHelper.updateFollowUpRequiredType(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactScoreHelper.addLeadScoreGrade(Trigger.new, Trigger.oldMap);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactCallMarketoWebServiceHelper.callServiceOnBefore(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    AutoCreateEventContactHelper.createEventAutomaticaly(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    TriggerPortalControlHelper.checkPortalControl(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            }
            if(Trigger.isAfter){
                if(!Semaphores.ContactTriggerHandlerAfterUpdate){
                    Semaphores.ContactTriggerHandlerAfterUpdate = true;
                    ContactUpdateULCHelper.updateULC(Trigger.new, Trigger.old);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    ContactCallMarketoWebServiceHelper.callServiceOnAfter(Trigger.new);
                    System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());

                    //Contact_SharingHelper.shareContactOnUpdate(Trigger.new, Trigger.old);
                    //System.debug(System.LoggingLevel.DEBUG, '[SOQL queries] ' + Limits.getQueries());
                }
            }
        }

        if(Trigger.isDelete){
            if(Trigger.isBefore){
                if(!Semaphores.ContactTriggerHandlerBeforeDelete){
                    Semaphores.ContactTriggerHandlerBeforeDelete = true;

                }
            }
            if(Trigger.isAfter){
                if(!Semaphores.ContactTriggerHandlerAfterDelete){
                    Semaphores.ContactTriggerHandlerAfterDelete = true;
                }
            }
        }
}