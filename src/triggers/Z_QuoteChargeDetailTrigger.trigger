trigger Z_QuoteChargeDetailTrigger on zqu__QuoteChargeDetail__c (before insert, before update) {
	Z_QuoteChargeDetailTriggerHandler.updateChargeSummaryMetrics(Trigger.new);
}