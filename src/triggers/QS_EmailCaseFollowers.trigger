/*
* Change log:
*
* ext_vos   2018-02-14 CHG0032935 - Remove "sendEmail" flag.
*/
trigger QS_EmailCaseFollowers on FeedItem (After Insert,After Update) {
    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
        QS_FeedItemTriggerhandler.emailCaseFollowers(trigger.New);

}