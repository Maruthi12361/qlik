trigger AssetTrigger on Asset (after insert, after update) {
    List<Id> assetIds = new List<Id>();
    
    if (Trigger.isUpdate)
    {
        for (integer i = 0; i < Trigger.New.Size(); i++)
        {
            if (Trigger.New[i].AccountId != Trigger.Old[i].AccountId || Trigger.New[i].Reseller__c != Trigger.Old[i].Reseller__c)
            {
                assetIds.Add(Trigger.New[i].Id);
            }
        }
    }
    
    if (Trigger.isInsert)
    {
        for (integer i = 0; i < Trigger.New.Size(); i++)
        {
            assetIds.Add(Trigger.New[i].Id);
        }
    }
    
    System.Debug('AssetTrigger.trigger: About to update sharing for' + assetIds.size() + 'records');
    
    if (assetIds.size() > 0)
    {       
       if (!Test.isRunningTest())
            ApexSharingRules.UpdateAssetSharing(assetIds);
        else if (ApexSharingRules.TestingParnershare)
            ApexSharingRules.UpdateAssetSharingNow(assetIds);
    }
}