/*Nov 4th: The first draft version created by Ariel Xiao
*The purpose of this trigger is to get the BillToContact Id and SoldToContact Id whenever there is a new Quote
* 2018-11-30 AIN - IMP-1078 - Moved isApiUser outside of for loop. Added support for new boomi call that uses a list instead of ids so we only do one callout per trigger execution.
* 2018-12-06 Linus - IMP-1154 - Refactoring this trigger to be included in the Z_QuoteTrigger and commenting this out.
*/
trigger z_QuoteContactTrigger on zqu__Quote__c (after insert) {
    
    // private static String FUNC_STR = 'z_QuoteContactTrigger';
    // System.debug(FUNC_STR+' start.');
    // Map<ID, SObject> quotes = Trigger.newMap;
    // boolean isApiUser = IsApiUser();
    // List<string> billToContacts = new List<string>();
    // List<string> soldToContacts = new List<string>();
    //
    // for(ID qid : quotes.keySet()){
    //     zqu__Quote__c quote = (zqu__Quote__c) quotes.get(qid);
    //     Set<Id> contactIds = new Set<Id>();
    //     if(quote != null){
    //         System.debug(FUNC_STR+ ' :zqu__SoldToContact__c:'+quote.zqu__SoldToContact__c);
    //     	contactIds.add(quote.zqu__SoldToContact__c);
    //         System.debug(FUNC_STR+ ' :zqu__BillToContact__c:'+quote.zqu__BillToContact__c);
    //         contactIds.add(quote.zqu__BillToContact__c);
    //         billToContacts.add(quote.zqu__BillToContact__c);
    //         soldToContacts.add(quote.zqu__SoldToContact__c);
    //         //if(!isApiUser)
    //             //createCustomerBoomiCall(quote.zqu__BillToContact__c, quote.zqu__SoldToContact__c);
    //         //String msg = '<?xml version="1.0"?><Record environment="DEVSQ" type="Contact"><Ids><Id name="Billing">0031x000003e7jS</Id><Id name ="Shipping">0031x000003e7jS</Id></Ids></Record>';
	// 		//CustomQuoteTriggerHandler.callServiceBoomi('aF41x0000004CRXCA2', 'createNSCustomerFromSubQuote', msg);
    //     }else{
    //     	System.debug(FUNC_STR+' the quote:'+quote.Id +' does not have contacts informaiton.');
    //     }
    //
 	// 	System.debug(FUNC_STR+ ' :The contacts:'+contactIds);
    // }
    // if(!isApiUser && !billToContacts.isEmpty() && !soldToContacts.isEmpty())
    //     createCustomerBoomiCall(billToContacts, soldToContacts);
    //
    // System.debug(FUNC_STR +' end.');

    //call to Boomi system.General method.It forms request body
    // private static void createCustomerBoomiCall(list<string> billtoidList, list<string> shiptoidList) {
    //
    //     String environment=  UserInfo.getUserName().substringAfterLast('.').toUpperCase();
    //     String message = '<?xml version="1.0"?><Record environment="'+environment+'" type="Contact">';
    //     String boomiMethodToCall = 'createNSCustomerFromSubQuote';
    //
    //     for(Integer i = 0; i < billtoidList.size(); i++){
    //         message += '<Ids>';
    //         message += '<Id name="Billing">' + billtoidList[i] + '</Id>';
    //         message += '<Id name="Shipping">' + shiptoidList[i] + '</Id>';
    //         message += '</Ids>';
    //     }
    //     message += '</Record>';
    //
    //     System.debug(message);
    //
    //     CustomQuoteTriggerHandler.callServiceBoomi('aF41x0000004CRXCA2', boomiMethodToCall, message);
    // }
    // public static Boolean IsApiUser() {
    //     boolean isApiUser = false;
    //     id apiId = userinfo.getProfileId();
    //     Profile profile = [select Id, Name from profile where id = :apiId];
    //     if (profile.Name == 'Custom: Api Only User')
    //         isApiUser = true;
    //     return isApiUser;
    // }

}
