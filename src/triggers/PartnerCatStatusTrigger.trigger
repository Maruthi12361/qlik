/*----------------------------------------------------------------------------------------------------------
Purpose: PAEX-56
Created Date: 04/07/17
Created by: Kumar Navneet
Change log:
1       07.04.2017   Rodion Vakulovskyi added class invocations AccUpdatePartnerLevelStatusHandler
2       19/07/2019   Pramod Kumar V Added Trigger.oldMap to isDelete
--------------------------------------------------------------------------------------------------------------*/

trigger PartnerCatStatusTrigger on Partner_Category_Status__c (
  after insert, 
  after update, 
  after delete) {

     if(Trigger.isAfter) {
      if(Trigger.isInsert){           
          
          AccUpdatePartnerLevelStatusHandler.onAfterInsert(Trigger.new, Trigger.newMap);
      }
  } 
  
     if(Trigger.isUpdate) {
         if(Trigger.isAfter) { 
            
            AccUpdatePartnerLevelStatusHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
         }
      }
      if(Trigger.isDelete) {
         if(Trigger.isAfter) { 
            
            AccUpdatePartnerLevelStatusHandler.onAfterDelete(Trigger.old, Trigger.newMap,Trigger.oldMap);
         }
      }
}