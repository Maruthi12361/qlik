<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Partner_Flag_Followup_120_Days</fullName>
        <description>Partner Flag Followup - 120 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_OnBoarding_Templates/Partner_Flag_Followup_120_Days</template>
    </alerts>
    <alerts>
        <fullName>Partner_Flag_Followup_60_Days</fullName>
        <description>Partner Flag Followup - 60 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_OnBoarding_Templates/Partner_Flag_Followup_60_Days</template>
    </alerts>
    <fieldUpdates>
        <fullName>PartnerFlag_Set_Owner_to_Unassigned</fullName>
        <field>OwnerId</field>
        <lookupValue>Unassigned</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Partner Flag: Set Owner to Unassigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Notify Partner Flag Owner</fullName>
        <active>true</active>
        <description>Notify Partner Flag Record Owner At 60 and 120 Days</description>
        <formula>AND (     Follow_Up_Complete__c == True,     NOT(ISPICKVAL(Review_Status__c, &quot;Improved&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Partner_Flag_Followup_60_Days</name>
                <type>Alert</type>
            </actions>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Partner_Flag_Followup_120_Days</name>
                <type>Alert</type>
            </actions>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Partner Flag%3A Set Owner to Unassigned</fullName>
        <actions>
            <name>PartnerFlag_Set_Owner_to_Unassigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Flag__c.OwnerId</field>
            <operation>notEqual</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <description>Set Owner to Unassigned when a record is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
