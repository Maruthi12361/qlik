<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LCT_Email_Missed_Customer</fullName>
        <description>LCT Email Missed Customer</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>cri@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>carmen.reilly@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Live_Agent/Missed_Live_Chat</template>
    </alerts>
    <rules>
        <fullName>LCT Missed Chat Followup</fullName>
        <actions>
            <name>LCT_Email_Missed_Customer</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>LiveChatTranscript.Status</field>
            <operation>equals</operation>
            <value>Missed</value>
        </criteriaItems>
        <criteriaItems>
            <field>LiveChatTranscript.LiveChatDeploymentId</field>
            <operation>equals</operation>
            <value>Sales Deployment</value>
        </criteriaItems>
        <description>Workflow used to trigger an email to a customer evertyime a chat is missed</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
