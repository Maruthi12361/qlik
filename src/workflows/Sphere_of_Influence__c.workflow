<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CCS_Champion_Letter_To_Contact</fullName>
        <description>CCS Champion Letter To Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCS_System_Templates/CCS_Champion_Letter_Template</template>
    </alerts>
    <alerts>
        <fullName>CCS_Goal_Letter_To_Contact</fullName>
        <description>CCS Goal Letter To Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCS_System_Templates/CCS_Goal_Letter_Template</template>
    </alerts>
    <alerts>
        <fullName>CCS_Goal_Letter_To_Modifier</fullName>
        <description>CCS Goal Letter To Modifier</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCS_System_Templates/CCS_Goal_Letter_Template</template>
    </alerts>
    <alerts>
        <fullName>CCS_Send_Email_To_Modifier</fullName>
        <description>CCS Send Email To Modifier</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CCS_System_Templates/CCS_Champion_Letter_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>LoadChampionEmailBody</fullName>
        <field>Champion_letter_body__c</field>
        <formula>&quot;Dear &quot; &amp; Contact__r.FirstName &amp; &quot; &quot; &amp; Contact__r.LastName &amp; BR() &amp; BR() &amp; 
&quot;Thank you for your interest in QlikTech. The purpose of this letter is to summarize my understanding of our conversation. During our discussion you told me your primary goal is to &quot; &amp; Goal__c &amp; &quot;. &quot; &amp; BR() &amp; BR() &amp; 

Current_Situation__c &amp; BR() &amp; BR() &amp; 

Capabilities_Required_Solution__c &amp; BR() &amp; 
&quot;You thought if you had these capabilities you could &quot; &amp; Value__c &amp; &quot;.&quot; &amp; BR() &amp; BR() &amp; 

&quot;You expressed interest in further investigating QlikTech capabilities. Based on my experience, I suggest our next logical steps are:&quot; &amp; BR() &amp; 
Next_Steps__c &amp; BR() &amp; BR()</formula>
        <name>LoadChampionEmailBody</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LoadGoalEmailBody</fullName>
        <field>Goal_Letter_Body__c</field>
        <formula>&quot;Dear &quot; &amp; Contact__r.FirstName &amp; &quot; &quot; &amp; Contact__r.LastName &amp; BR() &amp; BR() &amp; 
&quot;Thank you for your interest in QlikTech. The purpose of this letter is to summarize my understanding of our conversation. During our discussion you told me your primary goal is to &quot; &amp; Goal__c &amp; &quot;. &quot; &amp; BR() &amp; BR()</formula>
        <name>LoadEmailBody</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CCS Champion Letter To Contact</fullName>
        <actions>
            <name>CCS_Champion_Letter_To_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Send_Champion_letter__c ) , Send_Champion_letter__c = true, ISPICKVAL(Champion_letter_recipient__c, &apos;Contact&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCS Champion Letter To Modifier</fullName>
        <actions>
            <name>CCS_Send_Email_To_Modifier</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Send_Champion_letter__c ) , Send_Champion_letter__c = true, ISPICKVAL(Champion_letter_recipient__c, &apos;Modifier&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCS Generate Champion Email Body</fullName>
        <actions>
            <name>LoadChampionEmailBody</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Champion_Letter__c ) , Champion_Letter__c = true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCS Generate Goal Email Body</fullName>
        <actions>
            <name>LoadGoalEmailBody</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Goal_Letter__c ) , Goal_Letter__c = true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCS Goal Letter To Contact</fullName>
        <actions>
            <name>CCS_Goal_Letter_To_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Send_Goal_Letter__c ) , Send_Goal_Letter__c = true, ISPICKVAL(Goal_Letter_Recipient__c, &apos;Contact&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CCS Goal Letter To Modifier</fullName>
        <actions>
            <name>CCS_Goal_Letter_To_Modifier</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Send_Goal_Letter__c ) , Send_Goal_Letter__c = true, ISPICKVAL( Goal_Letter_Recipient__c, &apos;Modifier&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
