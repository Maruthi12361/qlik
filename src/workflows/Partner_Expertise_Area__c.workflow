<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Expertise_Area_Under_Review_Partner</fullName>
        <ccEmails>QlikPartnerNetwork@qlik.com</ccEmails>
        <ccEmails>dnn@qlik.com</ccEmails>
        <description>Expertise Area Under Review (Partner)</description>
        <protected>false</protected>
        <recipients>
            <field>Dedicated_Expertise_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Expertise_Area/Partner_Expertise_Area_Under_Review_Partner</template>
    </alerts>
    <alerts>
        <fullName>Expertise_Area_Under_Review_internal</fullName>
        <ccEmails>QlikPartnerNetwork@qlik.com;dnn@qlik.com;mtm@qlik.com</ccEmails>
        <description>Expertise Area Under Review (internal)</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Expertise_Area/Partner_Expertise_Area_Under_Review_internal</template>
    </alerts>
    <alerts>
        <fullName>Partner_Expertise_Area_Approved</fullName>
        <ccEmails>QlikPartnerNetwork@qlik.com</ccEmails>
        <ccEmails>dnn@qlik.com</ccEmails>
        <description>Partner Expertise Area Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Dedicated_Expertise_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Expertise_Area/Partner_Expertise_Area_Approved</template>
    </alerts>
    <alerts>
        <fullName>Partner_Expertise_Area_New_EmailAlert</fullName>
        <ccEmails>qonnect@qlikview.com</ccEmails>
        <description>Partner Expertise Area (New-EmailAlert)</description>
        <protected>false</protected>
        <recipients>
            <field>Dedicated_Expertise_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Expertise_Area/Partner_Expertise_Area_New_EmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>Partner_Expertise_Area_Test_EmailAlert_Just_for_testers</fullName>
        <ccEmails>rdz@qlikview.com;daniela.aicher@qlikview.com;Lisa.Stifelman-Perry@qlikview.com</ccEmails>
        <description>Partner Expertise Area (Test-EmailAlert Just for testers)</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Partner_Expertise_Area/Partner_Expertise_Area_New_EmailTemplate</template>
    </alerts>
    <fieldUpdates>
        <fullName>Partner_Expertise_Area_Email_Sent_FU</fullName>
        <description>Partner Expertise Area Email Sent Field Update</description>
        <field>PartnerExpertiseAreaCreationEmailSent__c</field>
        <formula>NOW()</formula>
        <name>Partner Expertise Area Email Sent FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Expertise Area Approved</fullName>
        <actions>
            <name>Partner_Expertise_Area_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Expertise_Area__c.Expertise_Application_Eligibility__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <description>Workflow to notify partners when approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expertise Area Under Review</fullName>
        <actions>
            <name>Expertise_Area_Under_Review_Partner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Expertise_Area_Under_Review_internal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partner_Expertise_Area__c.Expertise_Application_Eligibility__c</field>
            <operation>equals</operation>
            <value>Under Review</value>
        </criteriaItems>
        <description>Workflow to notify partners when under review (actual status is rejected)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partner Expertise Area New WF Rule</fullName>
        <actions>
            <name>Partner_Expertise_Area_New_EmailAlert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Partner_Expertise_Area_Test_EmailAlert_Just_for_testers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Partner_Expertise_Area_Email_Sent_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partner_Expertise_Area__c.PartnerExpertiseAreaCreationEmailSent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>PRM</value>
        </criteriaItems>
        <description>Workflow rule that will act upon Partner Expertise Area Creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
