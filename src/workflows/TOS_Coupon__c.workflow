<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TOS_Coupon_Global_Ops_notification_of</fullName>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <description>TOS Coupon Global Ops notification of coupon submission</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_Coupon_submitted</template>
    </alerts>
    <alerts>
        <fullName>TOS_Coupon_Rejected</fullName>
        <description>TOS Coupon Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_Coupon_rejected</template>
    </alerts>
    <alerts>
        <fullName>TOS_Coupon_approved</fullName>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <ccEmails>training.support@qlik.com</ccEmails>
        <description>TOS Coupon approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>aww@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_Coupon_approved</template>
    </alerts>
    <alerts>
        <fullName>TOS_Coupon_approved_owner</fullName>
        <description>TOS Coupon approved - owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_Coupon_approved_owner</template>
    </alerts>
    <alerts>
        <fullName>TOS_Coupon_approved_under10percent_owner</fullName>
        <description>TOS Coupon approved - under 10 percent owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_Coupon_approved_owner</template>
    </alerts>
</Workflow>
