<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetDeploymentOverviewName</fullName>
        <field>Name</field>
        <formula>Account_Name__r.Name + &quot; Deployment Overview&quot;</formula>
        <name>Set Deployment Overview Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>App Profile%3A Deployment Overview%3A Set name</fullName>
        <actions>
            <name>SetDeploymentOverviewName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Deployment_Overview__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
