<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Email_Status</fullName>
        <field>Status</field>
        <literalValue>Response received – awaiting action</literalValue>
        <name>Change Email Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Email_Status_to_response_recieved</fullName>
        <description>Changes the email status from it&apos;s current value to response recieved</description>
        <field>Status</field>
        <literalValue>Response received – awaiting action</literalValue>
        <name>Change Email Status to response recieved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_1st_Response_Flag_on_Case</fullName>
        <field>X1st_Response_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Update 1st Response Flag on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_1st_Response_Timestamp_on_Case</fullName>
        <field>X1st_Response_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Update 1st Response Timestamp on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Trigger_Box</fullName>
        <field>Trigger_Incomming_Email_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Update Trigger Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case E2C Change Case Status on New email</fullName>
        <actions>
            <name>Change_Email_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Trigger_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FIN - Change Case Status on New email</fullName>
        <actions>
            <name>Change_Email_Status_to_response_recieved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Trigger_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance Query</value>
        </criteriaItems>
        <description>Triggers when a new email is recieved and the case is a Finance Record Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FINHD%3A Change Case Status on New email</fullName>
        <actions>
            <name>Change_Email_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Trigger_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance Operations</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HD Change Case Status on New email</fullName>
        <actions>
            <name>Change_Email_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Trigger_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type,General IT Support Record Type,Hardware Request Record Type,Software Request Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HD Notify owner of new email</fullName>
        <actions>
            <name>Update_Trigger_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type,Hardware Request Record Type,General IT Support Record Type,Software Request Record Type</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set 1st Response flag %28Finance Query%29</fullName>
        <actions>
            <name>Update_1st_Response_Flag_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_1st_Response_Timestamp_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.X1st_Response_Flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance Query</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
