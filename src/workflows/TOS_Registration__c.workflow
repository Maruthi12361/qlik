<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TOS_Registration_Italy_Account_missing_VAT_number</fullName>
        <ccEmails>EMEATraining@qlik.com</ccEmails>
        <description>TOS Registration: Italy Account missing VAT number</description>
        <protected>false</protected>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS_Registrations/Italy_Account_missing_VAT_number</template>
    </alerts>
    <alerts>
        <fullName>TOS_Registration_Rejected</fullName>
        <ccEmails>smm@qlik.com</ccEmails>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <ccEmails>training.support@qlik.com</ccEmails>
        <description>TOS Registration Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Training_Manager_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS_Registrations/TOS_URGENT_Legal_Approval_Refused</template>
    </alerts>
    <alerts>
        <fullName>TOS_Registration_changed_trigger_for_manual_update</fullName>
        <description>TOS Registration changed, trigger for manual update</description>
        <protected>false</protected>
        <recipients>
            <recipient>smm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TOS/TOS_Manual_update_post_legal_review</template>
    </alerts>
    <alerts>
        <fullName>TOS_Submit_for_Legal_Approval</fullName>
        <description>TOS Submit for Legal Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>smm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_Submit_to_Legal_notification</template>
    </alerts>
    <alerts>
        <fullName>TOS_Training_Manager_Notification_to_follow_up_with_Legal</fullName>
        <description>TOS Training Manager Notification to follow-up with Legal</description>
        <protected>false</protected>
        <recipients>
            <field>Training_Manager_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>systems@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_TM_Legal_notification_reminder</template>
    </alerts>
    <alerts>
        <fullName>TOS_Training_Manager_Notification_to_follow_up_with_Legal_3_days</fullName>
        <ccEmails>smm@qlik.com</ccEmails>
        <description>TOS Training Manager Notification to follow-up with Legal 3 days</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>TOS/TOS_TM_Legal_notification_reminder</template>
    </alerts>
    <alerts>
        <fullName>TOS_Training_Manager_Notification_to_follow_up_with_Legal_team</fullName>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <ccEmails>smm@qlik.com</ccEmails>
        <ccEmails>SFLegalApprovals@Qlik.com</ccEmails>
        <description>TOS Training Manager Notification to follow-up with Legal</description>
        <protected>false</protected>
        <recipients>
            <field>Training_Manager_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS/TOS_TM_Legal_notification_reminder</template>
    </alerts>
    <alerts>
        <fullName>TOS_manual_Legal_Approval</fullName>
        <description>TOS manual Legal Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>rys@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TOS/TOS_Manual_update_post_legal_review</template>
    </alerts>
    <alerts>
        <fullName>Training_Manager_Notification_of_Rejection</fullName>
        <ccEmails>smm@qlik.com</ccEmails>
        <ccEmails>DL-GlobalEducationServicesOperations@qlik.com</ccEmails>
        <ccEmails>training.support@qlik.com</ccEmails>
        <description>Training Manager Notification of Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Training_Manager_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS_Registrations/TOS_URGENT_Legal_Approval_Refused</template>
    </alerts>
    <alerts>
        <fullName>Training_Manager_Notification_of_manual_Legal_approval</fullName>
        <ccEmails>smm@qlik.com</ccEmails>
        <description>Training Manager Notification of manual Legal approval</description>
        <protected>false</protected>
        <recipients>
            <field>Training_Manager_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TOS_Registrations/TOS_TM_Legal_notification</template>
    </alerts>
    <alerts>
        <fullName>Training_Manager_Notification_of_manual_Legal_approval1</fullName>
        <ccEmails>smm@qlik.com</ccEmails>
        <description>Training Manager Notification of manual Legal approval</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>TOS_Registrations/TOS_TM_Legal_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>TOS_Legal_Approval_Status__c</field>
        <literalValue>Legal Approval Granted</literalValue>
        <name>Legal Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legal_approval_Refused</fullName>
        <field>TOS_Legal_Approval_Status__c</field>
        <literalValue>Legal Approval Refused</literalValue>
        <name>Legal approval Refused</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legal_approval_Rejected</fullName>
        <field>TOS_Legal_Approval_Status__c</field>
        <literalValue>Legal Approval Refused</literalValue>
        <name>Legal approval Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Manual_Legal_approval</fullName>
        <field>TOS_Legal_Approval_Status__c</field>
        <literalValue>Legal Approval Granted</literalValue>
        <name>Manual Legal approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TOS_Registration_needs_Legal_approval</fullName>
        <field>TOS_Legal_Approval_Status__c</field>
        <literalValue>Legal Approval Required</literalValue>
        <name>TOS Registration needs Legal approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TOS_external</fullName>
        <field>Registration_Number_Reference__c</field>
        <formula>Name</formula>
        <name>TOS external</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TOS Manual check Legal status change</fullName>
        <actions>
            <name>TOS_Registration_needs_Legal_approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3) OR (1 AND 4) OR (1 AND 5) OR (1 AND 6) OR (1 AND 7) OR (1 AND 8) OR (1 AND 9) OR (1 AND 10) OR (1 AND 11) OR (1 AND 12) OR (1 AND 13) OR (1 AND 14) OR (1 AND 15) OR (1 AND 16)</booleanFilter>
        <criteriaItems>
            <field>TOS_Registration__c.TOS_Legal_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Legal Approval Auto Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Palestine</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Iraq</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Sierra Leone</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Ivory Coast</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Lebanon</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Russia</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Ukraine</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Belarus</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Pakistan</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Saudi Arabia</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Zimbabwe</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Sudan</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Cuba</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>Iran</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Billto_Country__c</field>
            <operation>contains</operation>
            <value>North Korea</value>
        </criteriaItems>
        <description>To set the registration to Legal approval required when specific countries are matched.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TOS Registration changed by Legal</fullName>
        <actions>
            <name>TOS_Registration_changed_trigger_for_manual_update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TOS_Registration__c.TOS_Legal_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Legal Approval Refused</value>
        </criteriaItems>
        <description>Until Apex trigger is created by Angel</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TOS Submit to Legal notification</fullName>
        <actions>
            <name>TOS_Submit_for_Legal_Approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>TOS_Registration__c.TOS_Legal_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Legal Approval Required</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TOS TM Legal notification</fullName>
        <active>True</active>
        <criteriaItems>
            <field>TOS_Registration__c.TOS_Legal_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Legal Approval Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>TOS_Registration__c.Schedule_Start_date__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Follow up with Legal if an approval has not been completed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TOS_Training_Manager_Notification_to_follow_up_with_Legal_team</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>TOS_Registration__c.Schedule_Start_date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update external Id field</fullName>
        <actions>
            <name>TOS_external</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TOS_Registration__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
