<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LIC_Send_Email_when_unnassigned_Account_License_is_created</fullName>
        <ccEmails>ssh@qlikview.com</ccEmails>
        <description>LIC: Send Email when unnassigned Account License is created</description>
        <protected>false</protected>
        <recipients>
            <field>Navision_Sales_Person__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Internal_Emails/Unassigned_License</template>
    </alerts>
    <rules>
        <fullName>LIC%3A check for Unassigned Licenses</fullName>
        <actions>
            <name>LIC_Send_Email_when_unnassigned_Account_License_is_created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>Begins(Account__c, &apos;0012000000OMFQC&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
