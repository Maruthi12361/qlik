<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Conclusion_for_Case_population</fullName>
        <description>Update the field &quot;Conclusion for Case&quot; field on create and every edit</description>
        <field>Conclusion_for_case__c</field>
        <formula>LEFT( Conclusion__c , 255)</formula>
        <name>Conclusion for Case population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Bug_Name</fullName>
        <description>Populate Bug Title (Name) from Title field.</description>
        <field>Name</field>
        <formula>LEFT(Title__c, 80)</formula>
        <name>Populate Bug Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Conclusion for Case population</fullName>
        <actions>
            <name>Conclusion_for_Case_population</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the field &quot;Conclusion for Case&quot; field on create and every edit</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Bug Name</fullName>
        <actions>
            <name>Populate_Bug_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate Bug Title (Name) field with first 80 characters of the Title field.</description>
        <formula>AND( NOT(ISBLANK(Title__c)), LEFT(Title__c, 80) != Name )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
