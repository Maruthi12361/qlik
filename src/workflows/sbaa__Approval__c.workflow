<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Quote_Approval_Approved_Notification</fullName>
        <description>Quote Approval Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <field>sbaa__ApprovedBy__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Email_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Quote_Approval_Rejected_Notification</fullName>
        <description>Quote Approval Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <field>sbaa__RejectedBy__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SteelBrick_Email_Templates/Email_Approval_Confirmation</template>
    </alerts>
</Workflow>
