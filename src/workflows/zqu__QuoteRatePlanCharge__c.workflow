<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>zUpdate_Charge_to_AlignToCharge</fullName>
        <field>zqu__BillingPeriodAlignment__c</field>
        <literalValue>AlignToCharge</literalValue>
        <name>Update Charge to AlignToCharge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Prepaid Alignment to Charge</fullName>
        <actions>
            <name>zUpdate_Charge_to_AlignToCharge</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>zqu__QuoteRatePlanCharge__c.zqu__BillingPeriodAlignment__c</field>
            <operation>notEqual</operation>
            <value>AlignToCharge</value>
        </criteriaItems>
        <criteriaItems>
            <field>zqu__QuoteRatePlanCharge__c.zqu__Period__c</field>
            <operation>equals</operation>
            <value>Subscription Term</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
