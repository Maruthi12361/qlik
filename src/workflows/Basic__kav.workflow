<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Basic_Public_Knowledge_Email</fullName>
        <description>Basic:Public Knowledge Email</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/PKB_Approval_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>Basic_Public_Knowledge_Recall_Email</fullName>
        <description>Basic:Public Knowledge Recall Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>PKB_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/PKB_Approval_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>Basic_Public_Knowledge_Reject_Email</fullName>
        <description>Basic:Public Knowledge Reject Email</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/PKB_Approval_Status_Change</template>
    </alerts>
    <fieldUpdates>
        <fullName>Basic_PKB_Visibility</fullName>
        <field>IsVisibleInPkb</field>
        <literalValue>0</literalValue>
        <name>Basic:PKB Visibility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Basic_Public_Knowledge_Field_Update</fullName>
        <field>IsVisibleInPkb</field>
        <literalValue>1</literalValue>
        <name>Basic:Public Knowledge Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Basic_Public_Knowledge_Action</fullName>
        <action>PublishAsNew</action>
        <label>Basic:Public Knowledge Action</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>Basic%3APKB Visibility</fullName>
        <actions>
            <name>Basic_PKB_Visibility</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Basic__kav.PublishStatus</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
