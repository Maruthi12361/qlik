<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Resource_of_New_Assignment</fullName>
        <description>Notify Resource of New Assignment</description>
        <protected>false</protected>
        <recipients>
            <field>pse__Resource__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/PSE_Assignment_Assigned_to_Resource</template>
    </alerts>
    <fieldUpdates>
        <fullName>Cost_Rate_Update</fullName>
        <field>Cost_Rate_PSE__c</field>
        <formula>pse__Resource__r.pse__Default_Cost_Rate__c</formula>
        <name>Cost Rate Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cost_rate_update_new</fullName>
        <field>Cost_Rate_PSE__c</field>
        <formula>pse__Cost_Rate_Amount__c</formula>
        <name>Cost rate update new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Exclude_from_planners</fullName>
        <field>pse__Exclude_from_Planners__c</field>
        <literalValue>1</literalValue>
        <name>Exclude from planners</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>assignment_goes_from_Closed_to_scheduled</fullName>
        <field>pse__Exclude_from_Planners__c</field>
        <literalValue>0</literalValue>
        <name>assignment goes from Closed to scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assignment status is flipped</fullName>
        <actions>
            <name>Exclude_from_planners</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Assignment__c.pse__Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>IT-2262: Exclude from Planners Box to Check when Assignment Status Flips to Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assignment status not closed</fullName>
        <actions>
            <name>assignment_goes_from_Closed_to_scheduled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Assignment__c.pse__Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>IT-2262: Exclude from Planners Box to Check when Assignment Status Flips to Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cost Rate Assignment Update</fullName>
        <actions>
            <name>Cost_rate_update_new</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>pse__Assignment__c.Cost_Rate_PSE__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Created as part of IT-1032 to update cost Rate fields on Assignment</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cost Rate Update</fullName>
        <actions>
            <name>Cost_Rate_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>pse__Assignment__c.pse__Use_Resource_Default_Cost_Rate__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Created as part of IT-1032 to update cost Rate fields on Assignment</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PSE Assignment Resource Is Assigned</fullName>
        <actions>
            <name>Notify_Resource_of_New_Assignment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify the Resource of their new Assignment once it is &apos;Scheduled&apos;.</description>
        <formula>OR (AND (ISNEW(),NOT(ISBLANK( pse__Resource__c ))), AND (NOT(ISBLANK( pse__Resource__c )), PRIORVALUE(pse__Resource__c) &lt;&gt; pse__Resource__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
