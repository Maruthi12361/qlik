<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Business_critical_case_standard</fullName>
        <description>Business critical case standard</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Team_lead_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Team_lead_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Team_lead_3__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Email_Templates/Case_business_critical</template>
    </alerts>
    <fieldUpdates>
        <fullName>Send_email_false</fullName>
        <field>Send_email__c</field>
        <literalValue>0</literalValue>
        <name>Send email false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Support Office Email</fullName>
        <actions>
            <name>Business_critical_case_standard</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_email_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Support_Office__c.Send_email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends an email to all members of the support office</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
