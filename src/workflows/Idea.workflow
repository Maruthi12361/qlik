<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Idea_recorded_in_QlikTech_HR_Community</fullName>
        <ccEmails>peopleideasportal@qlikview.com</ccEmails>
        <description>Idea recorded in QlikTech HR Community</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Internal_Ideas/New_Internal_Idea</template>
    </alerts>
    <alerts>
        <fullName>Ideas_New_Idea_Finance_Ops</fullName>
        <description>Ideas - New Idea Finance Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>cwd@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Idea_E_Mail_Templates/Ops_Ideas_Notification</template>
    </alerts>
    <alerts>
        <fullName>Ideas_New_Idea_Marketing_Ops</fullName>
        <description>Ideas - New Idea Marketing Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>drv@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svc-arc@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Idea_E_Mail_Templates/Ops_Ideas_Notification</template>
    </alerts>
    <alerts>
        <fullName>Ideas_New_Idea_Partner_Ops</fullName>
        <description>Ideas - New Idea Partner Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>jgl@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Idea_E_Mail_Templates/Ops_Ideas_Notification</template>
    </alerts>
    <alerts>
        <fullName>Ideas_New_Idea_Sales_Ops</fullName>
        <description>Ideas - New Idea Sales Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>jfi@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jgl@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Idea_E_Mail_Templates/Ops_Ideas_Notification</template>
    </alerts>
    <alerts>
        <fullName>Ideas_New_Idea_Services_Ops</fullName>
        <description>Ideas - New Idea Services Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>ulrika.sanden@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Idea_E_Mail_Templates/Ops_Ideas_Notification</template>
    </alerts>
    <alerts>
        <fullName>Ideas_New_Idea_Support_Ops</fullName>
        <description>Ideas - New Idea Support Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>ulrika.sanden@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Idea_E_Mail_Templates/Ops_Ideas_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_QT_Internal_Ideas_group</fullName>
        <ccEmails>peopleideasportal@qlikview.com</ccEmails>
        <description>Send email to QT Internal Ideas group</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Internal_Ideas/New_Internal_Idea</template>
    </alerts>
    <rules>
        <fullName>Idea - New Idea Ops - Finance</fullName>
        <actions>
            <name>Ideas_New_Idea_Finance_Ops</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Idea.Categories</field>
            <operation>includes</operation>
            <value>Finance Operations</value>
        </criteriaItems>
        <description>New idea notification for Partner Operations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Idea - New Idea Ops - Marketing</fullName>
        <actions>
            <name>Ideas_New_Idea_Marketing_Ops</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Idea.Categories</field>
            <operation>includes</operation>
            <value>Marketing Operations</value>
        </criteriaItems>
        <description>New idea for Marketing Operations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Idea - New Idea Ops - Partner</fullName>
        <actions>
            <name>Ideas_New_Idea_Partner_Ops</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Idea.Categories</field>
            <operation>includes</operation>
            <value>Partner Operations</value>
        </criteriaItems>
        <description>New or edited idea for Partner Operations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Idea - New Idea Ops - Sales</fullName>
        <actions>
            <name>Ideas_New_Idea_Sales_Ops</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Idea.Categories</field>
            <operation>includes</operation>
            <value>Sales Operations</value>
        </criteriaItems>
        <description>New or updated idea for Sales Operations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Idea - New Idea Ops - Services</fullName>
        <actions>
            <name>Ideas_New_Idea_Services_Ops</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Idea.Categories</field>
            <operation>includes</operation>
            <value>Services Operations</value>
        </criteriaItems>
        <description>New or updated idea for Services Operations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Idea - New Idea Ops - Support</fullName>
        <actions>
            <name>Ideas_New_Idea_Support_Ops</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Idea.Categories</field>
            <operation>includes</operation>
            <value>Support Operations</value>
        </criteriaItems>
        <description>New or updated idea for SupportOperations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Internal Idea</fullName>
        <actions>
            <name>Send_email_to_QT_Internal_Ideas_group</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.CommunityId</field>
            <operation>equals</operation>
            <value>QlikTech HR</value>
        </criteriaItems>
        <description>Sends mail when new Idea created in QlikTech HR Community</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
