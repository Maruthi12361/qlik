<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SuMo_Level_Up</fullName>
        <description>SuMo Level Up</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SuMo_eMail_Templates1/SuMo_Level_Up</template>
    </alerts>
    <fieldUpdates>
        <fullName>NS_Flagged_for_NS_Update</fullName>
        <description>NS Field Update</description>
        <field>Flagged_for_NS_update__c</field>
        <literalValue>1</literalValue>
        <name>NS: Flagged for NS Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>NS%3A Update Flagged for NS Update</fullName>
        <actions>
            <name>NS_Flagged_for_NS_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISCHANGED( Delegate_Start_Date__c ), ISCHANGED( Delegate_End_Date__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SuMo - Send Level Up eMail</fullName>
        <actions>
            <name>SuMo_Level_Up</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send level up email to manager</description>
        <formula>success__Level__c &gt; PRIORVALUE(success__Level__c) &amp;&amp; NOT(ISBLANK(Manager_Email__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
