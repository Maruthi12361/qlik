<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Campaign_Update_Status_Cancelled</fullName>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>Campaign: Update Status - Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_Update_Status_Completed</fullName>
        <field>Status</field>
        <literalValue>Complete</literalValue>
        <name>Campaign: Update Status - Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaign_Update_Status_In_Progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Campaign: Update Status - In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Job_Function</fullName>
        <description>Clear Job Function if Strategic Initiative is not Solution - Other</description>
        <field>Campaign_Job_Function__c</field>
        <name>Clear Job Function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Sector</fullName>
        <description>Clear Sector if Strategic Initiative is not Solution - Other</description>
        <field>Campaign_Sector__c</field>
        <name>Clear Sector</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Publishable_Name_Change</fullName>
        <description>Sets date and time on Publishable_Name_Last_Modified_Date__c field</description>
        <field>Publishable_Name_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Publishable Name Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Active_to_false</fullName>
        <description>Makes a campaign inactive if the end date has passed</description>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Set Active to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Copy_To_Qchi</fullName>
        <description>Tick the Copy To Qchi checkbox</description>
        <field>Copy_To_Qchi__c</field>
        <literalValue>1</literalValue>
        <name>Set Copy To Qchi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_active</fullName>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Set  to active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_In_Progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCampaignNameNoDuplicates</fullName>
        <field>CampaignNameNoDuplicates__c</field>
        <formula>Name</formula>
        <name>UpdateCampaignNameNoDuplicates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_End_Date</fullName>
        <field>Actual_End_Date__c</field>
        <formula>Today()</formula>
        <name>Update Actual End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Start_Date</fullName>
        <field>Actual_Start_Date__c</field>
        <formula>Today()</formula>
        <name>Update Actual Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Campaign_Status_to_Complete</fullName>
        <field>Status</field>
        <literalValue>Complete</literalValue>
        <name>set Campaign Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Campaign Flag Qchi For Copy</fullName>
        <actions>
            <name>Set_Copy_To_Qchi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If certain fields are updated in SFDC then this should trigger the check box for &quot;Copy to Qchi&quot; when the record is saved.</description>
        <formula>/* CCE 20140120 CR# 10838 - Webservice exclusion for Qchi copy */  AND  (   LEFT($Profile.Id, 15) &lt;&gt; &quot;00e20000000zFfp&quot;,  /* Custom: Api Only User */   LEFT(Name, 3) &lt;&gt; &quot;MDF&quot;,  /* Ignore MDF generated campaigns */   NOT  /* Ignore Complete or Cancelled campaigns*/   (     OR     (       ISPICKVAL(Status , &quot;Complete&quot;),       ISPICKVAL(Status , &quot;Cancelled&quot;)     )   ),   OR   (     ISCHANGED(Type),     ISCHANGED(Campaign_Sub_Type__c),     ISCHANGED(Publishable_Name__c),     ISCHANGED(StartDate),     ISCHANGED(EndDate),     ISCHANGED(Description),     ISCHANGED(Planned_Opportunity_Value__c),     ISCHANGED(CurrencyIsoCode),     ISCHANGED(OwnerId),     ISCHANGED(Qchi_OwnerId__c),     ISCHANGED(Campaign_Funnel_Stage__c)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Update Start and End Date</fullName>
        <actions>
            <name>Update_Actual_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Actual_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Campaign_Sub_Type__c</field>
            <operation>equals</operation>
            <value>DM - Email Campaign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Campaign_Member_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Actual_Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Actual_End_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketing Desktop Campaign Layout</value>
        </criteriaItems>
        <description>Auto populate Actual Start Date and Acutal End Date on Campaign if
-	Campaign Sub-Type field equals “DM- Email” 
-	There are 5 or more campaign members 
-	Campaign members  Status field equal  “Target”</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Update Status - Cancelled</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Campaign_Member_Count__c</field>
            <operation>lessThan</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketing Desktop Campaign Layout</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Campaign_MDF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Cancelled: 
&lt; 5 campaign members with non-Target statuses AND 
Campaign End Date + 2 months &lt; Today 
Should be triggered based on the End Date + 2 months being less than Today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Campaign_Update_Status_Cancelled</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Campaign Update Status - Completed</fullName>
        <actions>
            <name>Campaign_Update_Status_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Campaign_Member_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.EndDate</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketing Desktop Campaign Layout</value>
        </criteriaItems>
        <description>CR# 20585
Completed: 
&gt;= 5 campaign members with non-Target statuses (status is not Target) AND 
Campaign End Date &lt; Today 
Should be triggered based on the End Date being greater than today</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Update Status - In Progress</fullName>
        <actions>
            <name>Campaign_Update_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Campaign_Member_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.EndDate</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketing Desktop Campaign Layout</value>
        </criteriaItems>
        <description>CR# 20585
In Progress: 
&gt;= 5 campaign member status changes (same logic in CR #10014) AND 
Campaign End Date &gt;= TODAY</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Sector and Job Function if Strategic Initiative is not Solution - Other</fullName>
        <actions>
            <name>Clear_Job_Function</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Sector</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Campaign_Theme__c</field>
            <operation>notEqual</operation>
            <value>Solution - Other</value>
        </criteriaItems>
        <description>Clear Sector and Job Function if Strategic Initiative is not Solution - Other</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Publishable Name Changes</fullName>
        <actions>
            <name>Publishable_Name_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the Publishable_Name_Last_Modified_Date__c field on Campaign</description>
        <formula>ISCHANGED(Publishable_Name__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Campaign to Active on Start date</fullName>
        <actions>
            <name>Set_to_active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>notEqual</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <description>Planned - ACTIVE 
In Progress - ACTIVE 
On Hold- ACTIVE 
Complete- ACTIVE 
Cancelled – INACTIVE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Campaign.StartDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Campaign to Inactive</fullName>
        <actions>
            <name>Set_Active_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <description>Planned - ACTIVE 
In Progress - ACTIVE 
On Hold- ACTIVE 
Complete- ACTIVE 
Cancelled – INACTIVE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Campaign.StartDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>UpdateCampaignNameNoDuplicates</fullName>
        <actions>
            <name>UpdateCampaignNameNoDuplicates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule is used to populate the CampaignNameNoDuplicates field</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
