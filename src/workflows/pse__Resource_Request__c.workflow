<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_Resource_Request_Has_Been_Created</fullName>
        <description>A Resource Request Has Been Created</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Resource_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/Resource_Request_Has_Been_Created_Custom</template>
    </alerts>
    <alerts>
        <fullName>Resource_Request_for_Europe_region</fullName>
        <ccEmails>DL-EMEAResourceManagement@qlik.com</ccEmails>
        <description>Resource Request for Europe region</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PS_Enterprise_Workflows/Resource_Request_Has_Been_Created_Custom</template>
    </alerts>
    <rules>
        <fullName>PSE Send Email Alert of a Europe resource request</fullName>
        <actions>
            <name>Resource_Request_for_Europe_region</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>IT-2059: Notify Service Ops when New Resource Request is created</description>
        <formula>AND(OR((ISPICKVAL(pse__Status__c,&quot;Draft&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Ready to Staff&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Hold&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Tentative&quot;))), 
(pse__Region__r.Resource_Api__c == &quot;EUROPE&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PSE Send Email Alert of a New Resource Request</fullName>
        <actions>
            <name>A_Resource_Request_Has_Been_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Service Ops when New Resource Request is created</description>
        <formula>AND(OR((ISPICKVAL(pse__Status__c,&quot;Draft&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Ready to Staff&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Hold&quot;)), 
(ISPICKVAL(pse__Status__c,&quot;Tentative&quot;))), 
(pse__Region__r.Resource_Api__c &lt;&gt; &quot;EUROPE&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
