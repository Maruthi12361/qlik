<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BP_Set_Q1_sign_off_date</fullName>
        <description>Sets the date in the Q4 sign off field when the Q4 AD Sign off field is checked</description>
        <field>Q1_AD_Sign_Off_Date__c</field>
        <formula>TODAY()</formula>
        <name>BP Set Q1 sign off date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BP_Set_Q2_Sign_off_date</fullName>
        <description>Sets the date in the Q2 sign off field when the Q2 AD Sign off field is checked</description>
        <field>Q2_AD_Sign_Off_Date__c</field>
        <formula>Today()</formula>
        <name>BP Set Q2 Sign off date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BP_Set_Q3_Sign_off_Date</fullName>
        <description>Sets the date in the Q3 sign off field when the Q3 AD Sign off field is checked</description>
        <field>Q3_AD_Sign_Off_Date__c</field>
        <formula>TODAY()</formula>
        <name>BP Set Q3 Sign off Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BP_Set_Q4_sign_off_Date</fullName>
        <description>Sets the date in the Q4 sign off field when the Q4 AD Sign off field is checked</description>
        <field>Q4_AD_Sign_Off_Date__c</field>
        <formula>TODAY()</formula>
        <name>BP Set Q4 sign off Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BP Set Q1 sign off date</fullName>
        <actions>
            <name>BP_Set_Q1_sign_off_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Plan__c.Q1_AD_Sign_off__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Business_Plan__c.Q1_AD_Sign_Off_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sets the date in the Q1 sign off field when the Q1 AD Sign off field is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BP Set Q2 sign off date</fullName>
        <actions>
            <name>BP_Set_Q2_Sign_off_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Plan__c.Q2_AD_Sign_off__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Business_Plan__c.Q2_AD_Sign_Off_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sets the date in the Q2 sign off field when the Q2 AD Sign off field is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BP Set Q3 sign off date</fullName>
        <actions>
            <name>BP_Set_Q3_Sign_off_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Plan__c.Q3_AD_Sign_off__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Business_Plan__c.Q3_AD_Sign_Off_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sets the date in the Q3 sign off field when the Q3 AD Sign off field is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BP Set Q4 sign off date</fullName>
        <actions>
            <name>BP_Set_Q4_sign_off_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Business_Plan__c.Q4_AD_Sign_off__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Business_Plan__c.Q4_AD_Sign_Off_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sets the date in the Q4 sign off field when the Q4 AD Sign off field is checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
