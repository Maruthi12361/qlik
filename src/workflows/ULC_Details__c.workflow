<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_To_Administrators_when_user_reports_he_cannot_login</fullName>
        <ccEmails>helpdesk@qlikview.com</ccEmails>
        <description>Send Email To Administrators when user reports he cannot login</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ULC_Templates/HP_Cannot_Login</template>
    </alerts>
    <fieldUpdates>
        <fullName>ResetProblemType</fullName>
        <field>ULC_Problem_Type__c</field>
        <literalValue>None</literalValue>
        <name>ResetProblemType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ULC Cannot Login</fullName>
        <actions>
            <name>Send_Email_To_Administrators_when_user_reports_he_cannot_login</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>ResetProblemType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Email sent via workflow when user sets problem type - I Cannot Login</description>
        <formula>AND( 
    ISCHANGED( ULC_Problem_Type__c ), 
    ISPICKVAL(ULC_Problem_Type__c, &apos;Cannot login&apos;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
