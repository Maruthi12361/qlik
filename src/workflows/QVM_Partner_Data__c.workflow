<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>QVM_AD_QlikMarket_Registration_Approval_Request</fullName>
        <description>QVM AD QlikMarket Registration Approval Request</description>
        <protected>false</protected>
        <recipients>
            <field>Alliance_Director__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>QlikMarket_Templates/QVM_QlikMarket_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>QVM_PSM_QlikMarket_Registration_Approval_Request</fullName>
        <description>QVM PSM QlikMarket Registration Approval Request</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Success_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>QlikMarket_Templates/QVM_QlikMarket_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>QVM_Partner_has_been_approved_on_Qlikmarket</fullName>
        <description>QVM Partner has been approved on Qlikmarket</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>QlikMarket_Templates/QVM_Partner_Approved_on_Qlikmarket</template>
    </alerts>
    <fieldUpdates>
        <fullName>QVM_Set_Rejected_Flag_to_False</fullName>
        <field>Rejected__c</field>
        <literalValue>0</literalValue>
        <name>QVM Set Rejected Flag to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Set_Status_to_Registered</fullName>
        <field>Status__c</field>
        <literalValue>Registered</literalValue>
        <name>QVM Set Status to &quot;Registered&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Uncheck_T_C_Flag</fullName>
        <field>Agreed_to_T_C__c</field>
        <literalValue>0</literalValue>
        <name>QVM Uncheck T&amp;C Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Update_Approved_Flag</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>QVM Update Approved Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QVM_Update_Rejected_Flag</fullName>
        <field>Rejected__c</field>
        <literalValue>1</literalValue>
        <name>QVM Update Rejected Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>QVM AD Approval Request</fullName>
        <actions>
            <name>QVM_AD_QlikMarket_Registration_Approval_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>QVM_Partner_Data__c.Alliance_Director_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>QVM_Partner_Data__c.Alliance_Director__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>QVM_Partner_Data__c.QlikMarket_Rep__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used to trigger an email when Alliance Director and QlikMarket Rep have been identified.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QVM PSM Approval Request</fullName>
        <actions>
            <name>QVM_PSM_QlikMarket_Registration_Approval_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>QVM_Partner_Data__c.PSM_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>QVM_Partner_Data__c.Partner_Success_Manager__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>QVM_Partner_Data__c.QlikMarket_Rep__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used to trigger an email when PSM and QlikMarket Rep have been identified.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
