<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Address_approval_reject</fullName>
        <description>Address approval reject</description>
        <field>Legal_Approval_Status__c</field>
        <literalValue>Refused</literalValue>
        <name>Address approval reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legal_update_time</fullName>
        <field>Legal_Approval_Time__c</field>
        <formula>NOW()</formula>
        <name>Legal update time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Address_legal_status</fullName>
        <description>Approve Address legal status</description>
        <field>Legal_Approval_Status__c</field>
        <literalValue>Granted</literalValue>
        <name>Approve Address legal status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status</fullName>
        <field>Legal_Approval_Status__c</field>
        <literalValue>Requested</literalValue>
        <name>Update Approval Status to Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Time</fullName>
        <description>Update Legal Approval Time</description>
        <field>Legal_Approval_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Approval Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update legal update time</fullName>
        <actions>
            <name>Legal_update_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Address__c.LastModifiedById</field>
            <operation>equals</operation>
            <value>00520000000zCfx</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
