<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CULCBeneluxEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (Benelux) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>att@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCDACHEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (DACH) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>rwz@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCDenmarkEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (Denmark) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>~evy-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCEmailAllApproversthatacontacthasrequestedaccess</fullName>
        <description>CULC: Email All Approvers that a contact has requested access</description>
        <protected>false</protected>
        <recipients>
            <recipient>jml@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~evy-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~gpt-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_approval_request</template>
    </alerts>
    <alerts>
        <fullName>CULCEmailBeneluxApproverthatacontacthasrequestedaccess</fullName>
        <description>CULC: Email Benelux Approver that a contact has requested access</description>
        <protected>false</protected>
        <recipients>
            <recipient>att@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_approval_request</template>
    </alerts>
    <alerts>
        <fullName>CULCEmailContactthatAccessrequestapproved</fullName>
        <description>CULC: Email Contact that Access request approved</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_approved</template>
    </alerts>
    <alerts>
        <fullName>CULCEmailContactthatAccessrequestreceived</fullName>
        <description>CULC: Email Contact that Access request received</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_request</template>
    </alerts>
    <alerts>
        <fullName>CULCEmailIMApproverthatacontacthasrequestedaccess</fullName>
        <description>CULC: Email IM Approver that a contact has requested access</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_approval_request</template>
    </alerts>
    <alerts>
        <fullName>CULCEmailIncApproverthatacontacthasrequestedaccess</fullName>
        <description>CULC: Email Inc Approver that a contact has requested access</description>
        <protected>false</protected>
        <recipients>
            <recipient>jml@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_approval_request</template>
    </alerts>
    <alerts>
        <fullName>CULCEmailUKApproverthatacontacthasrequestedaccess</fullName>
        <ccEmails>test@test.com</ccEmails>
        <description>CULC: Email UK Approver that a contact has requested access</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_approval_request</template>
    </alerts>
    <alerts>
        <fullName>CULCFranceEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (France) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>~gpt-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCIMEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (IM) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCIberiaEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (Iberia) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>cpr@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCIncEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (Inc) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>jml@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCNordicsFinEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (Nordics &amp; Fin) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>klndelete@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>CULCUKEmailContactthatAccessrequestrejected</fullName>
        <description>CULC: (UK) Email Contact that Access request rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Email_Templates/Contact_ULC_access_rejected</template>
    </alerts>
    <alerts>
        <fullName>PORTALtriggerfromULC</fullName>
        <ccEmails>downloads2@qliktech.com</ccEmails>
        <description>PORTAL trigger from ULC</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>ULC_Trigger_Mails/Trigger_for_ULC_to_control_access_to_Portals</template>
    </alerts>
    <rules>
        <fullName>CULC%3A %28Benelux%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCBeneluxEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Benelux</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (Benelux)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28DACH%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCDACHEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech GmbH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (DACH)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28Denmark%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCDenmarkEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Denmark ApS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (Denmark)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28France%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCFranceEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech France SARL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (France)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28IM%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCIMEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech International AB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (IM)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28Iberia%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCIberiaEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Iberia SL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (Iberia)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28Inc%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCIncEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Inc</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (Inc)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28Nordics%2FFinland%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCNordicsFinEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Nordic AB,QlikTech Finland OY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (Nordics/Finland)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A %28UK%29 Inform contact that access request rejected</fullName>
        <actions>
            <name>CULCUKEmailContactthatAccessrequestrejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND NOT (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech UK Ltd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been rejected (UK)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform ALL approvers that access request recd %28NO QT COMPANY%29</fullName>
        <actions>
            <name>CULCEmailAllApproversthatacontacthasrequestedaccess</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform All ULC Approvesr that access request has been received (for a contact on an account where QlikTech Company is not set) - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform Benelux approver that access request recd</fullName>
        <actions>
            <name>CULCEmailBeneluxApproverthatacontacthasrequestedaccess</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Benelux</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform ULC Benelux Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform DACH approver that access request recd</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech GmbH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform DACH ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform Denmark approver that access request recd</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Denmark ApS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform Denmark ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform France approver that access request recd</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech France SARL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform France ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform IM India that access request recd</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech International AB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>India</value>
        </criteriaItems>
        <description>Inform India IM ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform IM Singapore approver that access request recd</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech International AB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>China,South Korea,Taiwan,Hong Kong,Macau,Nepal,Bhutan,Myanmar,Cambodia,Vietnam,Thailand,Malaysia,Singapore,Indonesia,Brunei,Philippines,East Timor,Australia,New Zealand</value>
        </criteriaItems>
        <description>Inform Singapore IM ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform IM approver that access request recd</fullName>
        <actions>
            <name>CULCEmailIMApproverthatacontacthasrequestedaccess</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech International AB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>notEqual</operation>
            <value>India,China,South Korea,Taiwan,Hong Kong,Macau,Nepal,Bhutan,Myanmar,Cambodia,Vietnam,Thailand,Malaysia,Singapore,Indonesia,Brunei,Philippines,East Timor,Australia,New Zealand</value>
        </criteriaItems>
        <description>Inform IM ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform Iberia approver that access request recd</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Iberia SL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform Iberia ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform Inc approver that access request recd</fullName>
        <actions>
            <name>CULCEmailIncApproverthatacontacthasrequestedaccess</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Inc</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform Inc ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform Nordics%2FFinland approver that access request recd</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech Nordic AB,QlikTech Finland OY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform Nordics / Finland ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform UK approver that access request recd</fullName>
        <actions>
            <name>CULCEmailUKApproverthatacontacthasrequestedaccess</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>equals</operation>
            <value>QlikTech UK Ltd</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>Inform UK ULC Approver that access request has been received - where the contact requested level does not match account level</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform contact that access request approved</fullName>
        <actions>
            <name>CULCEmailContactthatAccessrequestapproved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND NOT (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CULC%3A Inform contact that access request recd</fullName>
        <actions>
            <name>CULCEmailContactthatAccessrequestreceived</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Inform ULC Contact that access request has been received and the status of their request.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PORTAL%3A Trigger from ULC</fullName>
        <actions>
            <name>PORTALtriggerfromULC</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>Customer Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact_ULC_Level__c.ULC_Level_Description__c</field>
            <operation>startsWith</operation>
            <value>PRM</value>
        </criteriaItems>
        <description>Sends a mail to Orbis so Orbis knows when to fire the webservice</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
