<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_new_Severity_1_Premier_Support_Case</fullName>
        <ccEmails>DL-SignatureSupport@Qlik.com</ccEmails>
        <description>A new Severity 1 Premier Support Case</description>
        <protected>false</protected>
        <senderAddress>no-reply@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/A_new_Severity_1_Premier_Support_Case_has_been_created</template>
    </alerts>
    <alerts>
        <fullName>Alert_Support_Managers_Case_not_actioned</fullName>
        <description>Alert Support Managers Case not actioned</description>
        <protected>false</protected>
        <recipients>
            <recipient>flo@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lco@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/NoActivity3HoursInQueue</template>
    </alerts>
    <alerts>
        <fullName>Auto_Close_Customer_English</fullName>
        <description>Auto Close Customer - English</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/ClosingNoResponse</template>
    </alerts>
    <alerts>
        <fullName>Auto_Close_Customer_Finnish</fullName>
        <description>Auto Close Customer - Finnish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/ClosingNoResponse_Finnish</template>
    </alerts>
    <alerts>
        <fullName>Auto_Close_Customer_French</fullName>
        <description>Auto Close Customer - French</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/ClosingNoResponse_French</template>
    </alerts>
    <alerts>
        <fullName>Auto_Close_Customer_German</fullName>
        <description>Auto Close Customer - German</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/ClosingNoResponse_German</template>
    </alerts>
    <alerts>
        <fullName>Auto_Close_Customer_Japanese</fullName>
        <description>Auto Close Customer - Japanese</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/ClosingNoResponse_Japan</template>
    </alerts>
    <alerts>
        <fullName>Auto_Close_Customer_Spanish</fullName>
        <description>Auto Close Customer - Spanish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/ClosingNoResponse_Spanish</template>
    </alerts>
    <alerts>
        <fullName>Auto_Close_Customer_Swedish</fullName>
        <description>Auto Close Customer - Swedish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/ClosingNoResponse_Swedish</template>
    </alerts>
    <alerts>
        <fullName>Business_critical_case</fullName>
        <description>Business critical case premium</description>
        <protected>false</protected>
        <recipients>
            <recipient>Regional_Team_Lead</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/Case_business_critical</template>
    </alerts>
    <alerts>
        <fullName>Business_critical_case_standard</fullName>
        <description>Business critical case standard</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_3_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/Case_business_critical</template>
    </alerts>
    <alerts>
        <fullName>Case_Action_Needed_Contact_Notification</fullName>
        <description>Case Action Needed Contact Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>HelpDesk_emails/Case_Action_Needed_Contact_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Action_Needed_Notification_to_Owner</fullName>
        <description>Case Action Needed Notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HelpDesk_emails/Case_Action_Needed_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Alert_Case_Assigned_To_when_new_Comment_Added_to_Case</fullName>
        <description>Case: Alert Case Assigned To when new Comment Added to Case</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/ActionNeeded_Comment_Added_Assigned_To</template>
    </alerts>
    <alerts>
        <fullName>Case_Alert_Case_Owner_when_new_Comment_Added_to_Case</fullName>
        <description>Case: Alert Case Owner when new Comment Added to Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/ActionNeeded_Comment_Added</template>
    </alerts>
    <alerts>
        <fullName>Case_E2C_New_Incoming_email</fullName>
        <description>Case: E2C New Incoming email for support</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>HelpDesk_emails/HD_Notify_Case_Owner_a_New_Incoming</template>
    </alerts>
    <alerts>
        <fullName>Case_Notify_ITBD_new_BD_access_point_Issue</fullName>
        <ccEmails>DL-ITBD@qlikview.com</ccEmails>
        <description>Case: Notify ITBD new BD access point Issue</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HelpDesk_emails/HD_General_support_request_request_received</template>
    </alerts>
    <alerts>
        <fullName>Case_PreSales_Support_Email</fullName>
        <description>Case PreSales Support Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Q_Templates/Case_PreSales_Support_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Sales_Ops_Email</fullName>
        <description>Case Sales Ops Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Sales_Ops_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_an_email_to_Q_admins_when_new_AD_user_created</fullName>
        <ccEmails>q@qlikview.com</ccEmails>
        <description>Case - Send an email to Q admins when new AD user created</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Q_Templates/Case_Notify_Q_of_New_User</template>
    </alerts>
    <alerts>
        <fullName>Case_Service_Operation_Email</fullName>
        <description>Case Service Operation Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Service_Ops_Email</template>
    </alerts>
    <alerts>
        <fullName>Case_Service_Operation_Email_Completed</fullName>
        <description>Case Service Operation Email - Completed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Service_Operations_Email_Completed</template>
    </alerts>
    <alerts>
        <fullName>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_DE</fullName>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [DE]</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/No_response_case_closed_DE</template>
    </alerts>
    <alerts>
        <fullName>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_EN</fullName>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [EN]</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/Case_Closed_No_Response</template>
    </alerts>
    <alerts>
        <fullName>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_ES</fullName>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [ES]</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/No_response_Case_Closed_ES1</template>
    </alerts>
    <alerts>
        <fullName>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_FR</fullName>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [FR]</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/No_response_case_closed_FR</template>
    </alerts>
    <alerts>
        <fullName>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_IT</fullName>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot;&quot;[IT]</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/No_response_case_closed_IT</template>
    </alerts>
    <alerts>
        <fullName>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_JA</fullName>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [JA]</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/No_response_case_closed_JP</template>
    </alerts>
    <alerts>
        <fullName>E2C_Notify_French_Contact_that_case_raised</fullName>
        <description>Sends an email to the Case.contact web and contact mail fields (to email 2 case test support group in qttest) for contacts with a qliktech company containing France</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/NewSupportCaseAck_Fr</template>
    </alerts>
    <alerts>
        <fullName>EMailCaseOwnerandContactStatuschangefromPendingCustomerInfotoResolved</fullName>
        <description>E-Mail Case Owner and Contact (Status change from Pending Customer Info to Resolved)</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Email_Templates/SUPPORTCaseResponsewithSolutionSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Email_2_Assigned_To_User</fullName>
        <description>Email 2 Assigned-To User</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/Case_Assigned_To_Ownership_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_2_Case_Owner_that_Assigned_To_Ownership_has_changed</fullName>
        <description>Email 2 Case Owner that Assigned-To Ownership has changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/Case_Assigned_To_Ownership_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_2_Contact_when_Case_is_transferred_out_of_Helpdesk</fullName>
        <description>Email 2 Contact when Case is transferred out of Helpdesk</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HelpDesk_emails/Case_E2C_Notify_Contact_that_Owner_has_changed</template>
    </alerts>
    <alerts>
        <fullName>Email_2_Contact_when_Sales_Ops_Internal_Apps_Case_is_transferred</fullName>
        <description>Email 2 Contact when Sales Ops Internal Apps Case is transferred</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Sales_Ops_Internal_Apps_E2C_Notify_Contact_that_Owner_has_changed</template>
    </alerts>
    <alerts>
        <fullName>Email_2_Contact_when_Sales_Ops_Internal_Apps_Case_status_is_changed</fullName>
        <description>Email 2 Contact when Sales Ops Internal Apps Case status is changed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Sales_Ops_Internal_Apps_E2C_Notify_Contact_that_Status_has_changed</template>
    </alerts>
    <alerts>
        <fullName>Email_2_IT_Manager_Americas_when_a_new_case_is_transferred_to_the_IT_US_Queue</fullName>
        <ccEmails>test@test.com</ccEmails>
        <description>Email 2 IT Manager Americas when a new case is transferred to the IT/US Queue</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_Notify_Queue_of_New_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_for_Attunity_Core_Team</fullName>
        <ccEmails>Attunity-SME_Core@qlik.com</ccEmails>
        <description>Email Notification for Attunity Core Team</description>
        <protected>false</protected>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_SME_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_for_Attunity_Partner_Strategic_Team</fullName>
        <ccEmails>Attunity-SME_Strategic@qlik.com</ccEmails>
        <description>Email Notification for Attunity Partner/Strategic Team</description>
        <protected>false</protected>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_SME_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_for_Attunity_QDC_Team</fullName>
        <ccEmails>dl-sme_qdc@qlik.com</ccEmails>
        <description>Email Notification for Attunity QDC Team</description>
        <protected>false</protected>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_SME_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_for_Attunity_SME_Legacy_SAP</fullName>
        <ccEmails>Attunity-SME_legacy@qlik.com</ccEmails>
        <description>Email Notification for Attunity SME Legacy/SAP</description>
        <protected>false</protected>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Attunity_SME_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_Customer_Communication_Milestone_GSD</fullName>
        <description>Email Violation: Customer Communication Milestone (GSD)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>GlobalSupportDirectorT</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_Customer_Communication_Milestone_GSM</fullName>
        <description>Email Violation: Customer Communication Milestone (GSM)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>GlobalSupportManager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_Customer_Communication_Milestone_Owner_and_RSM</fullName>
        <description>Email Violation: Customer Communication Milestone (Owner and RSM)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_Customer_Communication_Milestone_Owner_and_RSM_and_TL</fullName>
        <description>Email Violation: Customer Communication Milestone (Owner and RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_Customer_Communication_Milestone_Owner_and_all_RSM</fullName>
        <description>Email Violation: Customer Communication Milestone (Owner and all RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Regional_Team_Lead</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_Customer_Communication_Milestone_Owner_and_all_RSM_and_SS</fullName>
        <ccEmails>DL-SignatureSupport@Qlik.com</ccEmails>
        <description>Email Violation: Customer Communication Milestone (Owner and all RSM and SS)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_Customer_Communication_Milestone_all_RSM_and_Owner</fullName>
        <description>Email Violation: Customer Communication Milestone (all RSM and Owner)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_First_Response_Milestone_GSD</fullName>
        <description>Email Violation: First Response Milestone (GSD)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>GlobalSupportDirectorT</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_First_Response_Milestone_GSM</fullName>
        <description>Email Violation: First Response Milestone (GSM)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>GlobalSupportManager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_First_Response_Milestone_Owner_and_RSM</fullName>
        <description>Email Violation: First Response Milestone (Owner and RSM)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_First_Response_Milestone_Owner_and_RSM_and_TL</fullName>
        <description>Email Violation: First Response Milestone (Owner and RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_First_Response_Milestone_Owner_and_all_RSM</fullName>
        <description>Email Violation: First Response Milestone (Owner and all RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Regional_Team_Lead</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_First_Response_Milestone_SS_and_all_RSM_and_Owner</fullName>
        <ccEmails>DL-SignatureSupport@Qlik.com</ccEmails>
        <description>Email Violation: First Response Milestone (SS and all RSM and Owner)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Violation_First_Response_Milestone_all_RSM_and_Owner</fullName>
        <description>Email Violation: First Response Milestone (all RSM and Owner)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_Customer_Communication_Milestone_Owner_and_RSM</fullName>
        <description>Email Warning: Customer Communication Milestone (Owner and RSM)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_Customer_Communication_Milestone_Owner_and_RSM_and_TL</fullName>
        <description>Email Warning: Customer Communication Milestone (Owner and RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_Customer_Communication_Milestone_Owner_and_SS</fullName>
        <ccEmails>DL-SignatureSupport@Qlik.com</ccEmails>
        <description>Email Warning: Customer Communication Milestone (Owner and SS)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_Customer_Communication_Milestone_Owner_and_all_RSM</fullName>
        <description>Email Warning: Customer Communication Milestone (Owner and all RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Regional_Team_Lead</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_First_Response_Milestone_Owner_and_RSM</fullName>
        <description>Email Warning: First Response Milestone (Owner and RSM)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_First_Response_Milestone_Owner_and_RSM_and_TL</fullName>
        <description>Email Warning: First Response Milestone (Owner and RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_First_Response_Milestone_Owner_and_all_RSM</fullName>
        <description>Email Warning: First Response Milestone (Owner and all RSM and TL)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Regional_Team_Lead</recipient>
            <type>role</type>
        </recipients>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_Warning_First_Response_Milestone_Signature_Support</fullName>
        <ccEmails>dl-signaturesupport@qlik.com</ccEmails>
        <description>Email Warning: First Response Milestone (Signature Support)</description>
        <protected>false</protected>
        <senderAddress>support@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Email_Templates/SLA_Warning</template>
    </alerts>
    <alerts>
        <fullName>Email_sent_to_Oli_when_a_case_is_created_or_escalated</fullName>
        <description>Email sent to Dirk/Katarina/TAM when a case is created or escalated</description>
        <protected>false</protected>
        <recipients>
            <recipient>dse@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpn@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sjn-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Escalated_High_Urgent</template>
    </alerts>
    <alerts>
        <fullName>FINHDNew_Incoming_email</fullName>
        <description>FINHD: New Incoming email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Finance_Operations_Mails/FINHD_Notify_Case_Owner_a_New_Incoming</template>
    </alerts>
    <alerts>
        <fullName>FINHD_notify_user_of_case_details</fullName>
        <description>FinHD: notify user of case details</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Finance_Operations_Mails/FinHD_HelpDesk_request_user_notification</template>
    </alerts>
    <alerts>
        <fullName>FIN_Notify_Agent_Maintenance_case_created</fullName>
        <description>FIN - Notify Agent Maintenance case created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>orders@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Finance_HelpDesk_Maintenance/Finance_maintenance_case_created</template>
    </alerts>
    <alerts>
        <fullName>Fin_Notify_case_owner_case_untouched</fullName>
        <description>Fin - Notify case owner case untouched</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>orders@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Finance_HelpDesk_Maintenance/FIN_Finance_maintenance_case_untouched</template>
    </alerts>
    <alerts>
        <fullName>Fin_notify_requestor_of_maintenance_case_details</fullName>
        <description>Fin: notify requestor of maintenance case details</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>orders@qlik.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Finance_HelpDesk_Maintenance/Fin_HelpDesk_request_requestor_notification</template>
    </alerts>
    <alerts>
        <fullName>Follow_up_call_needed_reminder_to_case_owner_English</fullName>
        <description>Follow-up call needed reminder to case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/FollowUpCallNeeded</template>
    </alerts>
    <alerts>
        <fullName>HD_Notify_ITsystems_that_a_new_general_support_case_has_been_created</fullName>
        <description>HD Notify ITsystems that a new general support case has been created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_support_request_received</template>
    </alerts>
    <alerts>
        <fullName>HD_Notify_ITsystems_that_a_new_hardware_request_has_been_created</fullName>
        <description>HD Notify ITsystems that a new hardware request has been created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>HelpDesk_emails/HD_Hardware_request_received</template>
    </alerts>
    <alerts>
        <fullName>HD_Send_Urgent_Case_email</fullName>
        <ccEmails>systems@qlikview.com</ccEmails>
        <description>HD: Send Urgent Case email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_Urgent_mail_needs_attention</template>
    </alerts>
    <alerts>
        <fullName>HD_Send_mail_to_Case_creator_when_insufficient_details_supplied</fullName>
        <description>HD Send mail to Case creator when insufficient details supplied</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>HelpDesk_emails/HD_Standard_response_no_user_details</template>
    </alerts>
    <alerts>
        <fullName>HD_Send_transfer_email_to_SN</fullName>
        <ccEmails>qliktechdev@service-now.com</ccEmails>
        <ccEmails>qliktech@service-now.com</ccEmails>
        <description>HD: Send transfer email to SN</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_Transfer_to_ServiceNow</template>
    </alerts>
    <alerts>
        <fullName>HD_notify_user_of_case_details</fullName>
        <description>HD notify user of case details</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_HelpDesk_request_user_notification</template>
    </alerts>
    <alerts>
        <fullName>Missing_Email_Address_Notification</fullName>
        <ccEmails>supportadmin@qlikview.com</ccEmails>
        <description>Missing Email Address Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Missing_Email_on_Case_Template</template>
    </alerts>
    <alerts>
        <fullName>NNotifyDSTRepresentativesHighPriorityCustomer_NewCaseCreated</fullName>
        <ccEmails>arthur.lee@qlikview.com</ccEmails>
        <description>NotifyDSTRepresentativesHighPriorityCustomer_NewCaseCreated</description>
        <protected>false</protected>
        <recipients>
            <recipient>flo@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_HighPriorityCustomer_Templates/HighPriorityCustomer_NewCase</template>
    </alerts>
    <alerts>
        <fullName>New_Incoming_email</fullName>
        <description>New Incoming email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_Notify_Case_Owner_a_New_Incoming</template>
    </alerts>
    <alerts>
        <fullName>New_case_overdue_notification</fullName>
        <description>New case overdue notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>dlr@qlikview.com-retired</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smm@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HelpDesk_emails/Case_Action_Needed_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_user_starts_in_48_hours</fullName>
        <description>New user starts in 48 hours</description>
        <protected>false</protected>
        <recipients>
            <recipient>IT</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Systems</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_New_User_Request_not_completed</template>
    </alerts>
    <alerts>
        <fullName>Notification_Attunity_Case_SLA_Reminder</fullName>
        <description>Notification: Attunity Case SLA Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>enh@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Automation/Attunity_SLA_Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_New_Attunity_Gold_Client_Case_Created</fullName>
        <description>Notification: New Attunity Gold Client Case Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Attunity_Gold_Client_Users</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Automation/Gold_Client_Attunity_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Notification_New_Azure_AWS_Case_Created</fullName>
        <ccEmails>Strategicpartnerandcustomersteam@qliktech.com</ccEmails>
        <description>Notification: New Azure/AWS Case Created</description>
        <protected>false</protected>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Automation/Notify_Azure_AWS_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Notification_New_Sev_1_Attunity_Case_Created</fullName>
        <ccEmails>Attunity-Global_Support@qlik.com</ccEmails>
        <description>Notification: New Sev 1 Attunity Case Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>enh@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>no-reply.customer-support@attunity.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Automation/Notify_Sev_1_Attunity_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Indirect_Customer_Partner_Details</fullName>
        <description>Notification to Indirect Customer - Partner Details</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/PartnerReferralCustomer_English</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Partner_Indirect_Customer_Details</fullName>
        <description>Notification to Partner - Indirect Customer Details</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Support_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/EmailToPartner_CustReferral</template>
    </alerts>
    <alerts>
        <fullName>Notifies_ITSystems_New_user_request_received</fullName>
        <description>Notifies ITSystems New user request received</description>
        <protected>false</protected>
        <recipients>
            <recipient>IT</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Systems</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_New_User_Request_Recieved</template>
    </alerts>
    <alerts>
        <fullName>Notify_Case_Requestor_about_case_Closed_status</fullName>
        <description>Notify Case Requestor about case Closed status</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>HelpDesk_emails/HD_HelpDesk_case_Closed_Pending_Customer_Reply</template>
    </alerts>
    <alerts>
        <fullName>Notify_that_hardware_request_more_than_24_hours_old</fullName>
        <description>HD Notify that hardware request more than 24 hours old</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>~mcd-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>HelpDesk_emails/HD_Internal_case_24_hours_old</template>
    </alerts>
    <alerts>
        <fullName>Progress_Path_Action_Required</fullName>
        <description>Progress path Action Required - to be sent out after 5 business days</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Support_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Automation/Progress_Path_Action_Required</template>
    </alerts>
    <alerts>
        <fullName>Progress_Path_Under_Review</fullName>
        <description>Progress path under review - to be sent out after 3 business days</description>
        <protected>false</protected>
        <recipients>
            <field>Support_Office_Team_Lead_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Support_Office_Team_Lead_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_Automation/Progress_Path_Under_Review</template>
    </alerts>
    <alerts>
        <fullName>QT_Designated_Support_Send_Email</fullName>
        <description>QT Designated Support Send Email</description>
        <protected>false</protected>
        <recipients>
            <field>Designated_Support_Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Automation/DSE_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_English</fullName>
        <description>Remind Customer - English</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_English_8day</fullName>
        <description>Remind Customer - English</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_Finnish</fullName>
        <description>Remind Customer - Finnish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded_Finnish</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_French</fullName>
        <description>Remind Customer - French</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded_French</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_German</fullName>
        <description>Remind Customer - German</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded_German</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_Japanese</fullName>
        <description>Remind Customer - Japanese</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded_Japan</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_Spanish</fullName>
        <description>Remind Customer - Spanish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded_Spanish</template>
    </alerts>
    <alerts>
        <fullName>Remind_Customer_Swedish</fullName>
        <description>Remind Customer - Swedish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Reminder_InfoNeeded_Swedish</template>
    </alerts>
    <alerts>
        <fullName>SendClosednotificationtoOwner</fullName>
        <description>Send Closed notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Email_Templates/SUPPORTCaseResponsewithSolutionSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>SendEMailtoContactName</fullName>
        <description>Send E-Mail to Contact Name</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Email_Templates/SUPPORT_Pending_Customer_Reply_7_Days</template>
    </alerts>
    <alerts>
        <fullName>SendEmailtoIncidentmanager</fullName>
        <description>Send E-mail to Incident manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>RegionalServiceDeskManager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Email_Templates/SUPPORTCaseescalationnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Send_an_alert_email_to_Extended_Support_Co_ordinator</fullName>
        <description>Send an alert email to Extended Support Co-ordinator</description>
        <protected>false</protected>
        <recipients>
            <recipient>bbs@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bbt@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>csg@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pwd@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tko@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>~egn-retired@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Ext_Sup_New</template>
    </alerts>
    <alerts>
        <fullName>Urgent_or_High_case_escalated_to_2nd_line</fullName>
        <description>Urgent or High case escalated to 2nd line</description>
        <protected>false</protected>
        <recipients>
            <recipient>flo@qlikview.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Removed_templates/Escalated_High_Urgent</template>
    </alerts>
    <alerts>
        <fullName>sends_an_email_to_dl_demosite_administrator_qlikview_com</fullName>
        <ccEmails>dl-demosite-administrator@qlikview.com</ccEmails>
        <description>Case: Send notification of case to demosite admin</description>
        <protected>false</protected>
        <senderAddress>noreply@qlikview.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>HelpDesk_emails/HD_General_support_request_request_received</template>
    </alerts>
    <fieldUpdates>
        <fullName>Business_Critical_Send_Email_False</fullName>
        <field>Business_Critical_Send_Email__c</field>
        <literalValue>0</literalValue>
        <name>Business Critical Send Email False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assign_Case_to_Attunity_Case_Queue</fullName>
        <description>Cases created in the Attunity Customer Community are assigned to the Attunity Case Queue</description>
        <field>OwnerId</field>
        <lookupValue>Attunity_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Assign Case to Attunity Case Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Copy_Bug_Id_to_Bug_ID</fullName>
        <field>Bug_ID__c</field>
        <formula>Bug_Id_formula__c</formula>
        <name>Case: Copy Bug Id to Bug ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_E2C_Reset_Trigger_Email_Checkbox</fullName>
        <field>Trigger_Incomming_Email_Notification__c</field>
        <literalValue>0</literalValue>
        <name>Case: E2C Reset Trigger Email Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Populate_Regional_Support_Manage</fullName>
        <field>Regional_Support_Manager_Email__c</field>
        <formula>Account.Support_Office__r.Regional_Support_Manager__r.Email</formula>
        <name>Case - Populate &quot;Regional Support Manage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_1st_Response_Timestamp</fullName>
        <field>X1st_Response_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Case Set 1st Response Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Not_Product_Related</fullName>
        <field>Product__c</field>
        <literalValue>Not product related</literalValue>
        <name>Case: Set Not Product Related</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Red_Account_to_False</fullName>
        <description>Set the Red Account checkbox to False.</description>
        <field>Red_Account__c</field>
        <literalValue>0</literalValue>
        <name>Case: Set Red Account to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Red_Account_to_True</fullName>
        <description>Set the Red Account checkbox to True.</description>
        <field>Red_Account__c</field>
        <literalValue>1</literalValue>
        <name>Case: Set Red Account to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Submitted_Date</fullName>
        <field>Case_submitted__c</field>
        <formula>NOW()</formula>
        <name>Case Set Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Case Status New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_1st_Response_Flag</fullName>
        <description>Updates the 1st Response Flag. This will change the functionality of the process to meet with Maria&apos;s emergency CR. The 1st Response button can now be checked an unlimited number of times.</description>
        <field>X1st_Response_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Case: Update 1st Response Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_1st_Response_Timestamp</fullName>
        <description>Updates the 1st Repsonse Timestamp field with the current date time.</description>
        <field>X1st_Response_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Case: Update 1st Response Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Type_to_Service_Request</fullName>
        <description>Updates Case Type to Service Request when a customer support value is selected in the Case Category field</description>
        <field>Type</field>
        <literalValue>Service Request</literalValue>
        <name>Case: Update Type to Service Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_set_Wait_Internal_On_to_blank</fullName>
        <field>Wait_Internal_on__c</field>
        <name>Case set Wait Internal On to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_submitted_timestamp</fullName>
        <field>Case_submitted__c</field>
        <formula>NOW()</formula>
        <name>Case submitted timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_update_Extended_Support</fullName>
        <description>Extended Support Field will be updated with the combined info from &quot;Extended Support - End Date&quot; &amp; &quot;Extended Support - Version&quot; on the Account record.</description>
        <field>Extended_Support_new__c</field>
        <formula>&quot;Version &quot; &amp;
IF(ISPICKVAL(Account_Origin__r.Extended_Support_Version__c,&quot;&quot;), TEXT(Account.Extended_Support_Version__c ), TEXT(Account_Origin__r.Extended_Support_Version__c))
&amp; &quot; - &quot; &amp;
IF(ISPICKVAL(Account_Origin__r.Extended_Support_Version__c,&quot;&quot;),
TEXT(Account.Extended_Support_End_Date__c), TEXT(Account_Origin__r.Extended_Support_End_Date__c))</formula>
        <name>Case: update Extended Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Category_Data</fullName>
        <field>Category__c</field>
        <literalValue>Account Data/Challenge</literalValue>
        <name>Category Data</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Category_Internal_Apps</fullName>
        <field>Category__c</field>
        <literalValue>Applications</literalValue>
        <name>Category Internal Apps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Category_Training</fullName>
        <field>Category__c</field>
        <literalValue>Systems/Tools Training</literalValue>
        <name>Category Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ChangeStatustoResolved</fullName>
        <description>If Status=Pending Customer Reply and Last Activity &gt; 2 days, change Status=Resolved</description>
        <field>Status</field>
        <literalValue>Resolved</literalValue>
        <name>Change Status to Resolved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Owner_to_IT_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>ITNordicROW</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to IT Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Collaboration_Team</fullName>
        <description>Clear the &quot;Collaboration Team&quot; field.</description>
        <field>Collaboration_Team__c</field>
        <name>Clear Collaboration Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Notification_sent_date_time</fullName>
        <description>Clear &quot;Notification sent&quot; field</description>
        <field>Notification_sent__c</field>
        <name>Clear &quot;Notification sent&quot; date/time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Pending_Reply_Start_Date_Update</fullName>
        <description>Update date to 0 when case is not longer on status &apos;Pending Customer Reply&apos;</description>
        <field>Pending_Customer_Reply_Start_time__c</field>
        <name>Clear Pending Reply Start Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Status_Escalated_Change_Date</fullName>
        <description>Clear status escalated field</description>
        <field>Status_Escalated_Change_Date__c</field>
        <name>Clear Status Escalated Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HD_Change_owner_to_Helpdesk_Queue</fullName>
        <description>Changes owner to Helpdesk queue</description>
        <field>OwnerId</field>
        <lookupValue>Helpdesk</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>HD Change owner to Helpdesk Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HD_Change_owner_to_ITSystems_queue</fullName>
        <description>changes owner to IT Systems queue</description>
        <field>OwnerId</field>
        <lookupValue>ITSystemsQueue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>HD Change owner to ITSystems queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HD_Reset_Trigger_Email_Checkbox</fullName>
        <field>Trigger_Incomming_Email_Notification__c</field>
        <literalValue>0</literalValue>
        <name>HD Reset Trigger Email Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HD_Set_Status_to_Pending_Cust_Reply</fullName>
        <field>Status</field>
        <literalValue>Pending Customer Reply</literalValue>
        <name>HD Set Status to Pending Cust Reply</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HD_Set_status_to_transferred_to_SN</fullName>
        <description>changes the status to &apos;Transferred to service now&apos;</description>
        <field>Status</field>
        <literalValue>Transferred to ServiceNow</literalValue>
        <name>HD: Set status to transferred to SN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HD_Set_transfer_to_ServiceNow_to_False</fullName>
        <description>resets the checkbox to prevent accidental sending if the status changes on a case after transfer</description>
        <field>Transfer_to_ServiceNow__c</field>
        <literalValue>0</literalValue>
        <name>HD: Set transfer to ServiceNow to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Communication_Timestamp_now</fullName>
        <field>Last_Communication_Timestamp__c</field>
        <formula>now()</formula>
        <name>Last Communication Timestamp = now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_starter_is_Confidential_text_Clear</fullName>
        <field>New_starter_is_Confidential_text__c</field>
        <name>&quot;New starter is Confidential text&quot; Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_starter_is_Confidential_text_Popul</fullName>
        <field>New_starter_is_Confidential_text__c</field>
        <formula>&quot;&lt;font color=#FF0000&gt;&lt;b&gt;This is a Confidential new starter:&lt;/b&gt;&lt;/font&gt;&lt;ul&gt;&lt;li&gt;&lt;font color=#FF0000&gt;&lt;b&gt;DO NOT add new starter name to the “Subject” line&lt;/b&gt;&lt;/font&gt;&lt;/li&gt;&lt;li&gt;&lt;font color=#FF0000&gt;&lt;b&gt;Configure hardware for new starter to be ready for Start Date noted on case record&lt;/b&gt;&lt;/font&gt;&lt;/li&gt;&lt;li&gt;&lt;font color=#FF0000&gt;&lt;b&gt;DO NOT set up network or systems accounts &lt;u&gt;until the Start Date&lt;/u&gt; noted on the case record&lt;/b&gt;&lt;/font&gt;&lt;/li&gt;&lt;/ul&gt;&quot;</formula>
        <name>&quot;New starter is Confidential text&quot; Popul</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Reply_Start_Time_Update</fullName>
        <description>Update field to Now when Case status goes to Pending Customer reply</description>
        <field>Pending_Customer_Reply_Start_time__c</field>
        <formula>now()</formula>
        <name>Pending Reply Start Time Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PopulateResolutionWithClosureCode</fullName>
        <description>On closing a case, if there is no resolution populate it with the closure code.</description>
        <field>Resolution__c</field>
        <formula>&quot;Closure code: &quot; &amp; Text(Closure_Code__c)</formula>
        <name>PopulateResolutionWithClosureCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Priority_urgent</fullName>
        <field>Priority</field>
        <literalValue>Urgent</literalValue>
        <name>Priority urgent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QT_Designated_Support_Email_Addr_Populat</fullName>
        <description>CR 61267 Previous value: Account.QT_Designated_Support_Contact__r.Email</description>
        <field>QT_Designated_Support_Email_Address__c</field>
        <formula>&quot;&quot;</formula>
        <name>QT Designated Support Email Addr Populat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_Status_Blank</fullName>
        <field>SLA_Status__c</field>
        <formula>&quot;&quot;</formula>
        <name>SLA Status Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_Status_Violation</fullName>
        <field>SLA_Status__c</field>
        <formula>&quot;Violation&quot;</formula>
        <name>SLA Status Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_Status_Warning</fullName>
        <field>SLA_Status__c</field>
        <formula>&quot;Warning&quot;</formula>
        <name>SLA Status Warning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetClosureCodetoNoResponse</fullName>
        <description>If Status=Pending Customer Reply and Last Activity &gt; 2 days, change  set Closure Code = No Response</description>
        <field>Closure_Code__c</field>
        <literalValue>No Response</literalValue>
        <name>Set Closure Code to No Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusInProcess</fullName>
        <field>Status</field>
        <literalValue>In Process</literalValue>
        <name>SetStatusInProcess</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Active_Status_to_False</fullName>
        <field>Active_Status__c</field>
        <literalValue>0</literalValue>
        <name>Set Active Status to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Active_Status_to_True</fullName>
        <field>Active_Status__c</field>
        <literalValue>1</literalValue>
        <name>Set Active Status to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closure_Code_to_No_Response</fullName>
        <description>If Status=Pending Customer Reply and Last Activity &gt; 2 days, change set Closure Code = No Response</description>
        <field>Closure_Code__c</field>
        <literalValue>No Response</literalValue>
        <name>Set Closure Code to No Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_Level_1</fullName>
        <field>Priority_Level__c</field>
        <literalValue>1</literalValue>
        <name>Set Priority Level 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_Level_2</fullName>
        <field>Priority_Level__c</field>
        <literalValue>2</literalValue>
        <name>Set Priority Level 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_Level_3</fullName>
        <field>Priority_Level__c</field>
        <literalValue>3</literalValue>
        <name>Set Priority Level 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_Level_4</fullName>
        <field>Priority_Level__c</field>
        <literalValue>4</literalValue>
        <name>Set Priority Level 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Action_Required</fullName>
        <description>Changes the Status field to &quot;Action Required&quot;</description>
        <field>Status</field>
        <literalValue>Action Required</literalValue>
        <name>Set Status to Action Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Closed_Pending_Confirmatio</fullName>
        <field>Status</field>
        <literalValue>Closed Pending Confirmation</literalValue>
        <name>Set Status to Closed Pending Confirmatio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Under_Review</fullName>
        <description>Changes the Status field to &quot;Under Review&quot;</description>
        <field>Status</field>
        <literalValue>Under Review</literalValue>
        <name>Set Status to Under Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_case_Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Set case Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_closure_code_to_No_Response2</fullName>
        <field>Closure_Code__c</field>
        <literalValue>No Response</literalValue>
        <name>Set closure code to No Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Severity_1</fullName>
        <field>Severity__c</field>
        <literalValue>1</literalValue>
        <name>Severity 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_New_Comment_Checkbox</fullName>
        <description>Unchecks the New Case Comment Check box.</description>
        <field>New_Case_Comment_Check__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck New Comment Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Visible_in_Self_Service_Portal</fullName>
        <description>Unchecks the checkbox &quot;Visible in Self Service Portal&quot;</description>
        <field>IsVisibleInSelfService</field>
        <literalValue>0</literalValue>
        <name>Uncheck &quot;Visible in Self Service Portal&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Phone_Call_Needed</fullName>
        <description>Update the Case Status to Phone Call needed.</description>
        <field>Status</field>
        <literalValue>Phone Call Needed</literalValue>
        <name>Update Case Status Phone Call Needed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_modified_case_last</fullName>
        <description>Update Customer modified case last</description>
        <field>Customer_Modified_Case_Last__c</field>
        <literalValue>1</literalValue>
        <name>Update Customer modified case last</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_modified_case_last_F</fullName>
        <description>Clear &apos;Customer Modified Case Last&apos; flag if someone in QT updates case</description>
        <field>Customer_Modified_Case_Last__c</field>
        <literalValue>0</literalValue>
        <name>Update Customer modified case last - F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Escalated_False</fullName>
        <description>Clear the Escalated checkbox</description>
        <field>IsEscalated</field>
        <literalValue>0</literalValue>
        <name>Update Escalated False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Escalated_True</fullName>
        <description>Set the Escalated checkbox</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Update Escalated True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Update_internal</fullName>
        <description>Update the &quot;Last Update (internal)&quot; field with the current date and time</description>
        <field>Last_Update_internal__c</field>
        <formula>NOW()</formula>
        <name>Update Last Update (internal)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Notification_sent_date_time</fullName>
        <description>Used by workflow to set current date/time when an email alert is triggered.</description>
        <field>Notification_sent__c</field>
        <formula>NOW()</formula>
        <name>Update &quot;Notification sent&quot; date/time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Problem_Queue</fullName>
        <description>When a new Problem case is created by an agent, it is assigned to the problem queue</description>
        <field>OwnerId</field>
        <lookupValue>ProblemCases</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Problem Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_queue</fullName>
        <description>Cases that are handed over by agents to follow the sun will be sent to the &apos;To be allocated&apos; queue to be re-assigned to a new agent</description>
        <field>OwnerId</field>
        <lookupValue>Tobeallocated</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Problem_Case_Status_to_Pending_Ap</fullName>
        <description>Update Problem Case Status to Pending Approval</description>
        <field>Status</field>
        <literalValue>Pending Approval</literalValue>
        <name>Update Problem Case Status to Pending Ap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Changed_field</fullName>
        <description>Update &quot;Status Changed&quot; field with current date/time</description>
        <field>Status_Changed__c</field>
        <formula>NOW()</formula>
        <name>Update &quot;Status Changed&quot; field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Escalated_Change_Date</fullName>
        <description>Add date/time to Status escalated fied</description>
        <field>Status_Escalated_Change_Date__c</field>
        <formula>now()</formula>
        <name>Update Status Escalated Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Response_received</fullName>
        <description>Update Status to &apos;Response Received - awaiting action&apos;</description>
        <field>Status</field>
        <literalValue>Contact Response Received</literalValue>
        <name>Update Status to &apos;Response received&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Violation_Timestamp</fullName>
        <field>Violation_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Violation Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X1st_Response_Timestamp_now</fullName>
        <description>Updates 1st response to be now</description>
        <field>X1st_Response_Timestamp__c</field>
        <formula>now()</formula>
        <name>1st Response Timestamp = now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_escalation_status_from_case</fullName>
        <description>Sets the set escalation status which in turn populates the Linked case status field on the qlikview support escalation</description>
        <field>Set_Escalation_status__c</field>
        <formula>Case
(
Status,
&quot;New&quot;,&quot;New&quot;,
&quot;In Process&quot;,&quot;In Process&quot;,
&quot;Pending Customer Reply&quot;,&quot;Pending Customer Reply&quot;,
&quot;Response received – awaiting action&quot;,&quot;Response received – awaiting action&quot;,
&quot;On Hold&quot;,&quot;On Hold&quot;,
&quot;Awaiting Licenses&quot;,&quot;Awaiting Licenses&quot;,
&quot;Wait Internal&quot;,&quot;Wait Internal&quot;,
&quot;Escalated&quot;,&quot;Escalated&quot;,
&quot;Escalated – Returned&quot;,&quot;Escalated – Returned&quot;,
&quot;Pending Approval&quot;,&quot;Pending Approval&quot;,
&quot;Rejected&quot;,&quot;Rejected&quot;,
&quot;Approved&quot;,&quot;Approved&quot;,
&quot;Resolved&quot;,&quot;Resolved&quot;,
&quot;Closed&quot;,&quot;Closed&quot;,
&quot;Proposed&quot;,&quot;Proposed&quot;,
&quot;Under Investigation&quot;,&quot;Under Investigation&quot;,
&quot;Under Investigation - Workaround Found&quot;,&quot;Under Investigation - Workaround Found&quot;,
&quot;Under Investigation - Root Cause Found&quot;,&quot;Under Investigation - Root Cause Found&quot;,
&quot;Phone Call Needed&quot;,&quot;Phone Call Needed&quot;,
&quot;no status applied - update Case_set_status_for_escalation_to_read field update&quot;)</formula>
        <name>set escalation status from case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>test</fullName>
        <field>Employee_Last_Name__c</field>
        <formula>&quot;fffffffff&quot;</formula>
        <name>test</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>%22New starter is Confidential text%22 Clear</fullName>
        <actions>
            <name>New_starter_is_Confidential_text_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Confidential_New_Starter__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Clear text in &quot;New starter is Confidential text&quot; field when &quot;Confidential New starter Confidential&quot; field is set to False</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%22New starter is Confidential text%22 Populate</fullName>
        <actions>
            <name>New_starter_is_Confidential_text_Popul</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Confidential_New_Starter__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Populate text to &quot;New starter is Confidential text&quot; field when &quot;Confidential New starter&quot; field is set to True (e.g. through the helpdesk portal)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%28Attunity%29 Requesting SME Research - Core Team</fullName>
        <actions>
            <name>Email_Notification_for_Attunity_Core_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Wait Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Wait_Internal_on__c</field>
            <operation>equals</operation>
            <value>SME</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SME_Team__c</field>
            <operation>equals</operation>
            <value>Attunity Core Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_To__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This rule sends an email notification when the Case Status set to Wait Internal and the SME team is Attunity Core Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%28Attunity%29 Requesting SME Research - Legacy%2FSAP Team</fullName>
        <actions>
            <name>Email_Notification_for_Attunity_SME_Legacy_SAP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Wait Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Wait_Internal_on__c</field>
            <operation>equals</operation>
            <value>SME</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SME_Team__c</field>
            <operation>equals</operation>
            <value>Attunity Legacy/SAP Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_To__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This rule sends an email notification when the Case Status set to Wait Internal and the SME team is Legacy/SAP team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%28Attunity%29 Requesting SME Research - Partner%2FStrategic Team</fullName>
        <actions>
            <name>Email_Notification_for_Attunity_Partner_Strategic_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Wait Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Wait_Internal_on__c</field>
            <operation>equals</operation>
            <value>SME</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SME_Team__c</field>
            <operation>equals</operation>
            <value>Attunity Partner/Strategic Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_To__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This rule sends an email notification when the Case Status set to Wait Internal and the SME team is Partner/Strategic Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%28Attunity%29 Requesting SME Research - QDC Team</fullName>
        <actions>
            <name>Email_Notification_for_Attunity_QDC_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Wait Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Wait_Internal_on__c</field>
            <operation>equals</operation>
            <value>SME</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SME_Team__c</field>
            <operation>equals</operation>
            <value>Attunity QDC Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_To__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This rule sends an email notification when the Case Status set to Wait Internal and the SME team is Attunity QDC  Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>3 business days after Last Update %28internal%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Active_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If Active Status is TRUE send email 3 business days after Last Update (internal).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Progress_Path_Under_Review</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Status_to_Under_Review</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Escalated_True</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.X3_Business_Days__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>3 days without activity notify Case Owner to contact Customer</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solution Suggested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>New User Request Record Type,Hardware Request Record Type,General IT Support Record Type,Webmaster Support Layout,Software Request Record Type</value>
        </criteriaItems>
        <description>If Status = Solution Suggested and Last Activity (Modified date)&gt; 3 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Solution_suggested_three_days_ago</name>
                <type>Task</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>5 business days after Last Update %28internal%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Active_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If Active Status is TRUE send email 5 business days after Last Update (internal).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Progress_Path_Action_Required</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Status_to_Action_Required</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.X5_Business_Days__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>7 days without activity notify Case Owner to contact Customer</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solution Suggested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>New User Request Record Type,Hardware Request Record Type,General IT Support Record Type,Software Request Record Type</value>
        </criteriaItems>
        <description>If Status = Solution Suggested and Last Activity (Modified date)&gt; 7 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Solution_suggested_seven_days_ago</name>
                <type>Task</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>9 days without activity Close Case</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solution Suggested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>New User Request Record Type,Hardware Request Record Type,General IT Support Record Type,Webmaster Support Layout,Software Request Record Type</value>
        </criteriaItems>
        <description>If Status = Solution Suggested and Last Activity (Modified date)&gt; 9 days, Action:</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_to_Closed_Pending_Confirmatio</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_closure_code_to_No_Response2</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Case_closed</name>
                <type>Task</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>A new Severity 1 Premier Support Case has been created</fullName>
        <actions>
            <name>A_new_Severity_1_Premier_Support_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Origin, &apos;Support Portal&apos;), Account.Premier_Support__c, ISPICKVAL(Severity__c, &apos;1&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Alert Incident manager after 3 hours of Inactivity</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Second Line</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Notify Incident manager after 3 hours of Inactivity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SendEmailtoIncidentmanager</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Attunity Sev 1 SLA Notification</fullName>
        <active>true</active>
        <booleanFilter>(1 OR (2 AND 3) )  AND (4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Contact Response Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Wait Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Wait_Internal_on__c</field>
            <operation>equals</operation>
            <value>L1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Product__c</field>
            <operation>equals</operation>
            <value>Attunity AIS (Connect),Attunity CloudBeam,Attunity Compose,Attunity Compose for Data Lake,Attunity Connect,Attunity Enterprise Manager (AEM),Attunity Optim Connect,Attunity Replicate,Attunity RepliWeb (R-1)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Product__c</field>
            <operation>equals</operation>
            <value>Attunity RepliWeb (RMFT),Attunity Visibility</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Severity__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>Notification for Attunity SLA Reminders</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_Attunity_Case_SLA_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Trigger_Time_30__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_Attunity_Case_SLA_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Trigger_Time_45__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case %22Pending Approval%22%2F%22Escalated%22 Check-up overdue notification</fullName>
        <actions>
            <name>Set_Priority_Level_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send notification to helpdesk and update fields</description>
        <formula>AND(
OR(
ISPICKVAL( Status , &quot;Pending Approval&quot; ) ,
ISPICKVAL( Status , &quot;Escalated&quot; )
) ,
OR(
OwnerId = &quot;00GD0000001H8ja&quot; ,
OwnerId = &quot;00520000000yunw&quot; ,
OwnerId = &quot;005D0000001pRNs&quot; ,
OwnerId = &quot;005D0000001qq8V&quot; ,
OwnerId = &quot;005D0000001paKo&quot; ,
OwnerId = &quot;005D0000001rnCU&quot;
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Notification_sent_date_time</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case %22Pending Customer Reply%22 action overdue notification</fullName>
        <actions>
            <name>Set_Priority_Level_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send notification to helpdesk and update fields</description>
        <formula>AND(
ISPICKVAL( Status , &quot;Pending Customer Reply&quot; ) ,
ISPICKVAL( Status , &quot;New&quot; ) ,
OR(
OwnerId = &quot;00GD0000001H8ja&quot; ,
OwnerId = &quot;00520000000yunw&quot; ,
OwnerId = &quot;005D0000001pRNs&quot; ,
OwnerId = &quot;005D0000001qq8V&quot; ,
OwnerId = &quot;005D0000001paKo&quot; ,
OwnerId = &quot;005D0000001rnCU&quot;
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Action_Needed_Contact_Notification</name>
                <type>Alert</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case %22Response received%2E%2E%2E%22 action overdue notification</fullName>
        <actions>
            <name>Set_Priority_Level_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send notification to helpdesk and update fields</description>
        <formula>AND(
ISPICKVAL( Status , &quot;Response received - awaiting action&quot; ) ,
OR(
OwnerId = &quot;00GD0000001H8ja&quot; ,
OwnerId = &quot;00520000000yunw&quot; ,
OwnerId = &quot;005D0000001pRNs&quot; ,
OwnerId = &quot;005D0000001qq8V&quot; ,
OwnerId = &quot;005D0000001paKo&quot; ,
OwnerId = &quot;005D0000001rnCU&quot;
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Notification_sent_date_time</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case %22Wait Internal%22%2F%22Escalated%22 Owner notification</fullName>
        <actions>
            <name>Set_Priority_Level_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send notification to helpdesk and update fields</description>
        <formula>AND(
OR(
ISPICKVAL( Status , &quot;Wait Internal&quot; ) ,
ISPICKVAL( Status , &quot;Escalated&quot; )
) ,
OR(
OwnerId = &quot;00GD0000001H8ja&quot; , /* HelpDesk queue */
OwnerId = &quot;00520000000yunw&quot; , /* Daniel Le Masurier */
OwnerId = &quot;005D0000001pRNs&quot; , /* Naheed Talpur Neale */
OwnerId = &quot;005D0000001qq8V&quot; , /* Simon Millard */
OwnerId = &quot;005D0000001paKo&quot; , /* Victor Canarias Perez */
OwnerId = &quot;005D0000001rnCU&quot; /* David Jackson */
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Action_Needed_Notification_to_Owner</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Notification_sent_date_time</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case - Populate %22Regional Support Manager Email%22 field</fullName>
        <actions>
            <name>Case_Populate_Regional_Support_Manage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Regional_Support_Manager_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Invisible in SSP when Origin %3D POC</fullName>
        <actions>
            <name>Uncheck_Visible_in_Self_Service_Portal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>POC</value>
        </criteriaItems>
        <description>When Origin = POC, Visible in Self-Service Portal = False</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Last Activity More than 2 weeks %28Resolved%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>If Status=Resolvedand Last Activity &gt; 2 weeks, change Status=Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_to_Closed_Pending_Confirmatio</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Last Activity More than 7 Days%28Pending Info%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <description>If Status = Pending Customer Reply and Last Activity (Modified date)&gt; 7 days, create a Task for Case Owner and send an e-mail to Contact</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SendEMailtoContactName</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>CaseStatusPendingCustomerReply7daysPleasefollowup</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Last Activity More than 9 Days%28Pending Customer Info%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <description>If Status=Pending Customer Reply and Last Activity &gt; 9 days, change Status=Resolved and send e-mail to Case Owner and Contact, set Closure Code = No Response</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>EMailCaseOwnerandContactStatuschangefromPendingCustomerInfotoResolved</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>ChangeStatustoResolved</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>SetClosureCodetoNoResponse</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case No Action Violation Notification</fullName>
        <active>true</active>
        <description>Send notification to HelpDesk manager and update fields</description>
        <formula>AND(
NOT( ISBLANK( Notification_sent__c ) ) ,
OR(
ISPICKVAL( Status , &quot;New&quot; ) ,
ISPICKVAL( Status , &quot;Response received - awaiting action&quot; ) ,
ISPICKVAL( Status , &quot;Pending Approval&quot; ) ,
ISPICKVAL( Status , &quot;Escalated&quot; )
) ,
OR(
OwnerId = &quot;00GD0000001H8ja&quot; ,
OwnerId = &quot;00520000000yunw&quot; ,
OwnerId = &quot;005D0000001pRNs&quot; ,
OwnerId = &quot;005D0000001qq8V&quot; ,
OwnerId = &quot;005D0000001paKo&quot; ,
OwnerId = &quot;005D0000001rnCU&quot;
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Clear_Notification_sent_date_time</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Notification_sent__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case PreSales Support Email</fullName>
        <actions>
            <name>Case_PreSales_Support_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SI/Partner Presales Support</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Sales Ops Data</fullName>
        <actions>
            <name>Category_Data</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Ops Data</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Sales Ops Email</fullName>
        <actions>
            <name>Case_Sales_Ops_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Ops Requests,Sales Ops Data,Sales Ops Internal Apps,Sales Ops Training,Sales Ops Americas Requests,Sales Ops APAC Requests,Sales Ops QlikHelp,Sales Ops EMEA Requests,Sales Ops Beermea Requests</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Sales Ops Internal Apps</fullName>
        <actions>
            <name>Category_Internal_Apps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Ops Internal Apps</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Sales Ops Training</fullName>
        <actions>
            <name>Category_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Ops Training</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Service Operations Email</fullName>
        <actions>
            <name>Case_Service_Operation_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EducationServicesOperations,ConsultingServiceOperations</value>
        </criteriaItems>
        <description>Workflow used by Service Operations to notify Case Contact of new Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Service Operations Email - Completed</fullName>
        <actions>
            <name>Case_Service_Operation_Email_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EducationServicesOperations,ConsultingServiceOperations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Workflow used by Service Operations to notify Case Contact (requestor) that case has been Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Set Active Status to False</fullName>
        <actions>
            <name>Set_Active_Status_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Phone Call Needed,Awaiting Ownership,Collaboration Ongoing,New,Contact Response Received,In Process,Returned to Case Owner,Wait Internal,Collaboration Requested,Under Review,Action Required</value>
        </criteriaItems>
        <description>Set Active Status ckbox False if Case Status NOT equals to:
Phone Call Needed,Awaiting Ownership,Collaboration Ongoing,New,Contact Response Received,In Process,Returned to Case Owner,Wait Internal,Collaboration Requested,Under Review,Action Required</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Set Active Status to True</fullName>
        <actions>
            <name>Set_Active_Status_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Ownership,Collaboration Ongoing,New,Contact Response Received,In Process,Returned to Case Owner,Wait Internal,Collaboration Requested,Under Review,Action Required,Phone Call Needed</value>
        </criteriaItems>
        <description>Set Active Status ckbox to True if Case Status equals to:
Awaiting Ownership,Collaboration Ongoing,New,Contact Response Received,In Process,Returned to Case Owner,Wait Internal,Collaboration Requested,Under Review,Action Required, Phone Call Needed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Set Dates for Chat Record</fullName>
        <actions>
            <name>Case_Set_1st_Response_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Set_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Live Chat Support Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Urgent notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Urgent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>New User Request Record Type,General IT Support Record Type,Webmaster Support Layout,Hardware Request Record Type</value>
        </criteriaItems>
        <description>Notify Incident Manager as soon as there&apos;s an Urgent case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case business critical send email</fullName>
        <actions>
            <name>Business_critical_case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Business_Critical_Send_Email_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Priority_urgent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Severity_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Business_Critical__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Business_Critical_Send_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case business critical with support office</fullName>
        <actions>
            <name>Business_critical_case_standard</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Business_Critical__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Business_Critical_Send_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Support_Office__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>If case is business critical, there is a support office and no entitlement is set - send email to the support office leads and manager</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case change status from Wait Internal</fullName>
        <actions>
            <name>Case_set_Wait_Internal_On_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Wait Internal</value>
        </criteriaItems>
        <description>When status is changed from Wait Internal to any other status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case set Status to New on transfer</fullName>
        <actions>
            <name>Case_Status_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( OwnerId )
&amp;&amp;
($RecordType.Name = &apos;General IT Support Record Type&apos;
|| $RecordType.Name = &apos;Hardware Request Record Type&apos;
|| $RecordType.Name = &apos;New User Request Record Type&apos;
|| $RecordType.Name = &apos;Software Request Record Type&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case submitted</fullName>
        <actions>
            <name>Case_submitted_timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(PRIORVALUE( Subject )  = null || PRIORVALUE( Subject )  = &apos;&apos; ) &amp;&amp; Subject != null &amp;&amp; Subject != &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Alert Case Assigned To of new Comment</fullName>
        <actions>
            <name>Case_Alert_Case_Assigned_To_when_new_Comment_Added_to_Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_New_Comment_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends an email to the case owner when the &quot;New Case Comment Check&quot; field is set to true, then unchecks the box. Rule check verifies Assigned To should receive the email, and not Owner. Condition is updated per CR# 7856 / 42181</description>
        <formula>(New_Case_Comment_Check__c = TRUE) &amp;&amp; (OwnerId =  CaseComment_UserID__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Alert Case Owner%2FAssigned To of new Comment</fullName>
        <actions>
            <name>Case_Alert_Case_Assigned_To_when_new_Comment_Added_to_Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Alert_Case_Owner_when_new_Comment_Added_to_Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_New_Comment_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends an email to the case owner when the &quot;New Case Comment Check&quot; field is set to true, then unchecks the box. Rule check verifies that neither Assigned To / Owner made the Case Comment. Condition is updated per CR# 7856 / 42181</description>
        <formula>(New_Case_Comment_Check__c = TRUE) &amp;&amp; (CaseComment_UserID__c != Assigned_To__c) &amp;&amp; (OwnerId != CaseComment_UserID__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Alert Case owner of new Comment</fullName>
        <actions>
            <name>Case_Alert_Case_Owner_when_new_Comment_Added_to_Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_New_Comment_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends an email to the case owner when the &quot;New Case Comment Check&quot; field is set to true, then unchecks the box. Rule check verifies Owner should receive the email, and not Assigned To. Condition is updated per CR# 7856 / 42181</description>
        <formula>(New_Case_Comment_Check__c = TRUE) &amp;&amp; (CaseComment_UserID__c = Assigned_To__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Assign Case to Attunity Case Queue</fullName>
        <actions>
            <name>Case_Assign_Case_to_Attunity_Case_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Attunity Portal</value>
        </criteriaItems>
        <description>Cases created in the Attunity Customer Community are assigned to the Attunity Case Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A BD access point notification</fullName>
        <actions>
            <name>Case_Notify_ITBD_new_BD_access_point_Issue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Please_specify__c</field>
            <operation>equals</operation>
            <value>Access Point</value>
        </criteriaItems>
        <description>sends the standard case notification email to the BD team when a case is marked as access point</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A BD demo%2Eqlikview%2Ecom notification</fullName>
        <actions>
            <name>sends_an_email_to_dl_demosite_administrator_qlikview_com</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Please_specify__c</field>
            <operation>equals</operation>
            <value>Demo.QlikView.com/download,Demo.QlikView.com</value>
        </criteriaItems>
        <description>sends the standard case notification email to the BD team when a case is marked as access point</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Copy Bug Id to Bug ID for Search</fullName>
        <actions>
            <name>Case_Copy_Bug_Id_to_Bug_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy case field &apos;Bug Id&apos; to field &apos;Bug ID&apos; to overcome that Lookup and Formula fields are not indexed in Global Search. CR 63306</description>
        <formula>ISCHANGED(Bug__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A E2C Notify Case Owner about new incoming Email</fullName>
        <actions>
            <name>Case_E2C_Reset_Trigger_Email_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to notify when a new email has been recieved on a case</description>
        <formula>AND(     ISCHANGED( Trigger_Incomming_Email_Notification__c ),     Trigger_Incomming_Email_Notification__c  = true,     NOT(ISNEW()) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A E2C Notify French Contact that case raised</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.QlikTech_Company__c</field>
            <operation>contains</operation>
            <value>France</value>
        </criteriaItems>
        <description>Sends an email to the Case.contact web and contact mail fields (to email 2 case test support group in qttest) for contacts with a qliktech company containing France</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Email No Response or Pending Confimation %5BJA%5D</fullName>
        <actions>
            <name>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_JA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [JA]</description>
        <formula>AND(ISPICKVAL(Status , &quot;Closed&quot;),OR(ISPICKVAL(Closure_Code__c ,&quot;No Response&quot;),ISPICKVAL(Closure_Code__c ,&quot;Pending Confirmation&quot;)), OR(RecordType.DeveloperName &lt;&gt; &quot;SQT Support Customer Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt;&quot;QT Support Partner Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt; &quot;QlikTech Master Support Record Type&quot;), ISPICKVAL(Contact.Preferred_Language__c,&quot;Japanese&quot;), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Email No Response or Pending Confirmaiton %5BEN%5D</fullName>
        <actions>
            <name>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_EN</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [EN]</description>
        <formula>AND(ISPICKVAL(Status , &quot;Closed&quot;),OR(ISPICKVAL(Closure_Code__c ,&quot;No Response&quot;),ISPICKVAL(Closure_Code__c ,&quot;Pending Confirmation&quot;)),  OR(RecordType.DeveloperName &lt;&gt; &quot;SQT Support Customer Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt;&quot;QT Support Partner Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt; &quot;QlikTech Master Support Record Type&quot;), NOT(OR( ISPICKVAL(Contact.Preferred_Language__c,&quot;French&quot;),ISPICKVAL(Contact.Preferred_Language__c,&quot;German&quot;),ISPICKVAL(Contact.Preferred_Language__c,&quot;Japanese&quot;),ISPICKVAL(Contact.Preferred_Language__c,&quot;Spanish&quot;),ISPICKVAL(Contact.Preferred_Language__c,&quot;Italian&quot;))), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Email No Response or Pending Confirmation %5BDE%5D</fullName>
        <actions>
            <name>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_DE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [DE]</description>
        <formula>AND(ISPICKVAL(Status , &quot;Closed&quot;),OR(ISPICKVAL(Closure_Code__c ,&quot;No Response&quot;),ISPICKVAL(Closure_Code__c ,&quot;Pending Confirmation&quot;)),  OR(RecordType.DeveloperName &lt;&gt; &quot;SQT Support Customer Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt;&quot;QT Support Partner Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt; &quot;QlikTech Master Support Record Type&quot;),  ISPICKVAL(Contact.Preferred_Language__c,&quot;German&quot;), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Email No Response or Pending Confirmation %5BES%5D</fullName>
        <actions>
            <name>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_ES</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [ES]</description>
        <formula>AND(ISPICKVAL(Status , &quot;Closed&quot;),OR(ISPICKVAL(Closure_Code__c ,&quot;No Response&quot;),ISPICKVAL(Closure_Code__c ,&quot;Pending Confirmation&quot;)),  OR(RecordType.DeveloperName &lt;&gt; &quot;SQT Support Customer Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt;&quot;QT Support Partner Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt; &quot;QlikTech Master Support Record Type&quot;),  ISPICKVAL(Contact.Preferred_Language__c,&quot;Spanish&quot;), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Email No Response or Pending Confirmation %5BFR%5D</fullName>
        <actions>
            <name>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_FR</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confimration&quot; [FR]</description>
        <formula>AND(ISPICKVAL(Status , &quot;Closed&quot;),OR(ISPICKVAL(Closure_Code__c ,&quot;No Response&quot;),ISPICKVAL(Closure_Code__c ,&quot;Pending Confirmation&quot;)),  OR(RecordType.DeveloperName &lt;&gt; &quot;SQT Support Customer Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt;&quot;QT Support Partner Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt; &quot;QlikTech Master Support Record Type&quot;),  ISPICKVAL(Contact.Preferred_Language__c,&quot;French&quot;), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Email No Response or Pending Confirmation %5BIT%5D</fullName>
        <actions>
            <name>Case_is_Closed_with_Closure_Code_No_response_or_Pending_Confirmation_IT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case is Closed with Closure Code &quot;No response&quot; or &quot;Pending Confirmation&quot; [IT]</description>
        <formula>AND(ISPICKVAL(Status , &quot;Closed&quot;),OR(ISPICKVAL(Closure_Code__c ,&quot;No Response&quot;),ISPICKVAL(Closure_Code__c ,&quot;Pending Confirmation&quot;)),  OR(RecordType.DeveloperName &lt;&gt; &quot;SQT Support Customer Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt;&quot;QT Support Partner Portal Record Type&quot;,RecordType.DeveloperName &lt;&gt; &quot;QlikTech Master Support Record Type&quot;),  ISPICKVAL(Contact.Preferred_Language__c,&quot;Italian&quot;), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Send Alert to Q Administrators</fullName>
        <actions>
            <name>Case_Send_an_email_to_Q_admins_when_new_AD_user_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.New_User_TLA__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.New_User_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to let Q admins know a new user has been added to the AD via email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Set Not Product Related on Service Request</fullName>
        <actions>
            <name>Case_Set_Not_Product_Related</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Product to &apos;Not Product Related&apos; on Service Requests. CR 90216</description>
        <formula>AND( ISPICKVAL(Type, &apos;Service Request&apos;),  ISPICKVAL(Product__c, &apos;&apos;), NOT(ISPICKVAL(Area__c, &apos;I have a license issue&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update 1st Response Timestamp</fullName>
        <actions>
            <name>Case_Update_1st_Response_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_1st_Response_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.X1st_Response_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the 1st Response Timestamp when the 1st Response Flag has been checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Red Account %28False%29</fullName>
        <actions>
            <name>Case_Set_Red_Account_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Red_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Reflect a false value on the case Red Account field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Red Account %28True%29</fullName>
        <actions>
            <name>Case_Set_Red_Account_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Red_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Reflect a true value on the case Red Account field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Type to Service Request</fullName>
        <actions>
            <name>Case_Update_Type_to_Service_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Product__c</field>
            <operation>contains</operation>
            <value>Attunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Community User Changes,Licensing Issue</value>
        </criteriaItems>
        <description>Updates Case Type to Service Request when a customer support value is selected in the Case Category field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A auto populate Extended Support Field</fullName>
        <actions>
            <name>Case_update_Extended_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Extended Support field will populate with values from the 2 fields &quot;Extended Support - Version&quot; &amp; &quot;Extended Support - End Date&quot; on the Account Record.</description>
        <formula>AND( OR(RecordTypeId=&quot;01220000000DZqG&quot;, RecordTypeId=&quot;01220000000DZgN&quot;, RecordTypeId=&quot;01220000000DZgI&quot;),  OR(NOT( ISPICKVAL(Account.Extended_Support_Version__c,&quot;&quot;)),  NOT( ISPICKVAL(Account_Origin__r.Extended_Support_Version__c,&quot;&quot;)),  Account_Origin__c &lt;&gt;&quot;&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Collaboration Team</fullName>
        <actions>
            <name>Clear_Collaboration_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Collaboration_Team__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Phone Call Needed,Action Required,Collaboration Ongoing,Contact Response Received,Under Review,Collaboration Requested</value>
        </criteriaItems>
        <description>This is used to clear the &quot;Collaboration Team&quot; field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clear Pending Reply Start Date Update</fullName>
        <actions>
            <name>Clear_Pending_Reply_Start_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Pending Contact Response,Pending Customer Reply</value>
        </criteriaItems>
        <description>Update &apos;Pending Customer Reply Start time&apos; when Case status &lt;&gt; Pending Customer Reply.  Used in Workflow to remind customers of auto closure</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clear Status Escalated Change Date</fullName>
        <actions>
            <name>Clear_Status_Escalated_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Escalated</value>
        </criteriaItems>
        <description>Reset the Update Status Escalated change date when the status is changed  away from Escalated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close after 5 days of %22Pending Customer Reply%22 Status</fullName>
        <actions>
            <name>Pending_Reply_Start_Time_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Close case after it has been in status &quot;Pending Customer Reply&quot; for 5 days.</description>
        <formula>AND (
    ISPICKVAL( Status , &quot;Pending Customer Reply&quot; ) ,
    RecordTypeId = &quot;01220000000Ddyi&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Case_Requestor_about_case_Closed_status</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_case_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_closure_code_to_No_Response2</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email 2 Case Owner and Assigned-To User</fullName>
        <actions>
            <name>Email_2_Assigned_To_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_2_Case_Owner_that_Assigned_To_Ownership_has_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email Case Owner and Assigned-To user (old &amp; new) when Assigned-To ownership has changed.</description>
        <formula>ISCHANGED( Assigned_To__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email 2 Contact when Case is transferred out of Helpdesk</fullName>
        <actions>
            <name>Email_2_Contact_when_Case_is_transferred_out_of_Helpdesk</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email sent to contact on Case when it has been reassigned to another Queue or person.</description>
        <formula>AND
(
ISCHANGED(OwnerId),
Dont_trigger_transfer_notification_mail__c =false,
   OR
   (
   $RecordType.Id = &quot;01220000000Ddyi&quot;, /*General IT Support    Record Type*/
   $RecordType.Id = &quot;01220000000DdyY&quot;, /*Hardware Request Record Type*/
   $RecordType.Id = &quot;01220000000DdyT&quot;, /*New User Request Record Type*/
   $RecordType.Id = &quot;01220000000IbD1&quot;, /*QT Problem Case Record Type*/
   $RecordType.Id = &quot;01220000000IE3k&quot;/*Software Request Record Type*/
   )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email 2 Contact when Sales Ops Internal Apps Case is transferred</fullName>
        <actions>
            <name>Email_2_Contact_when_Sales_Ops_Internal_Apps_Case_is_transferred</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email sent to contact on Sales Ops Internal Apps Case when it has been reassigned to another Queue or person.</description>
        <formula>AND( ISCHANGED( OwnerId ), $RecordType.Id = &quot;012D0000000K9Ig&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email 2 Contact when Sales Ops Internal Apps Case status is changed</fullName>
        <actions>
            <name>Email_2_Contact_when_Sales_Ops_Internal_Apps_Case_status_is_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email sent to contact on Sales Ops Internal Apps Case when case status is changed</description>
        <formula>AND( ISCHANGED( Status ), $RecordType.Id = &quot;012D0000000K9Ig&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Esc%3A set escalation status from case</fullName>
        <actions>
            <name>set_escalation_status_from_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>sets the &quot;set escalation status&quot; field on the case to populate the case status on the qlikview support escalation</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Escalated - High%2FUrgent</fullName>
        <actions>
            <name>Urgent_or_High_case_escalated_to_2nd_line</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Second Line</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High,Urgent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Severity__c</field>
            <operation>equals</operation>
            <value>2,1</value>
        </criteriaItems>
        <description>Sends an email to IM/ops man when a case is escalated with priority high/urgent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Escalated-WaitingForProcessing</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Second Line</value>
        </criteriaItems>
        <description>This email will be sent every day for cases which are escalated and waiting for someone to action them.   Just to let the customer know we have not forgotten them.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ExtendedSupportEscalation</fullName>
        <actions>
            <name>Send_an_alert_email_to_Extended_Support_Co_ordinator</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Second Line,Third Line</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Extended_Support__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Email alert to ESC.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FIN - Maintenance case created</fullName>
        <actions>
            <name>Fin_notify_requestor_of_maintenance_case_details</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Maintenance &amp; Orders - Global Reply</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FIN - Maintenance case created %28agent%29</fullName>
        <actions>
            <name>FIN_Notify_Agent_Maintenance_case_created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Maintenance &amp; Orders - Global Reply,Maintenance &amp; Orders - AMERICAS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FIN - Maintenance case untouched</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Maintenance - Australia,Maintenance - Denmark,Maintenance - Finland,Maintenance - India,Maintenance - ITALY,&quot;Maintenance - NORDICS, BEERMEA, Aus&quot;,Maintenance - Norway,Maintenance &amp; Orders - AMERICAS,Maintenance &amp; Orders - DACH</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Maintenance &amp; Orders - FRANCE,Maintenance &amp; Orders - Global Reply,Maintenance &amp; Orders - IBERIA,Maintenance &amp; Orders - JAPAN,Maintenance - QT International Markets,Maintenance - QT NL &amp; Poland,Maintenance - Singapore/Hongkong</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Maintenance - Sweden,Maintenance - UK,Orders - Australia,Orders - Denmark,Orders - Finland,Orders - India,Orders - ITALY,&quot;Orders - NORDICS, APAC, IMAB, Aus&quot;,Orders - Norway,Orders - QT International Markets</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Orders - QT NL &amp; Poland,Orders - Singapore/Hongkong,Orders - Sweden,Orders - UK</value>
        </criteriaItems>
        <description>Triggers when a case is untouched for a specified time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Fin_Notify_case_owner_case_untouched</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FINHD%3A Notify User of Case details</fullName>
        <actions>
            <name>FINHD_notify_user_of_case_details</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance Operations</value>
        </criteriaItems>
        <description>Sends an email to the creator of a finance ops case notifying of case number</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GHD-AlertForElizabeth</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>E. Breuninger GmbH &amp; Co,Landesbank Baden-Württemberg,Dr. Johannes Heidenhain GmbH</value>
        </criteriaItems>
        <description>New case created from E. Breuninger, LandesBank Baden-Wurttemberg, Dr. Johannes Heidenhain</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HD Auto respond to User when Details Incomplete</fullName>
        <actions>
            <name>HD_Send_mail_to_Case_creator_when_insufficient_details_supplied</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insufficient_Account_details_on_request</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 AND 3) OR (4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Software_Required__c</field>
            <operation>includes</operation>
            <value>Navision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Navision_User_Account_to_Copy__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Software_Required__c</field>
            <operation>includes</operation>
            <value>Salesforce</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SFDC_User_account_to_Copy__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send a mail to the user asking for the request to be completed</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HD Change owner to IT</fullName>
        <actions>
            <name>Change_Owner_to_IT_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type,Hardware Request Record Type</value>
        </criteriaItems>
        <description>Changes the owner of any new Hardware/New User request record types to the IT</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HD Change owner to ITSystems queue</fullName>
        <actions>
            <name>HD_Change_owner_to_ITSystems_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General IT Support Record Type</value>
        </criteriaItems>
        <description>Changes the owner of any general IT support record type records to the IT Systems Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HD Move case to Helpdesk queue after user created</fullName>
        <actions>
            <name>HD_Change_owner_to_Helpdesk_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.User_Accounts_Added__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Software_Required__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HD Notify Case Owner about new incoming Email</fullName>
        <actions>
            <name>New_Incoming_email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>HD_Reset_Trigger_Email_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
	NOT( LEFT( RecordTypeId , 15 ) = &quot;012D00000003IAe&quot; ),
	ISCHANGED( Trigger_Incomming_Email_Notification__c ),
	Trigger_Incomming_Email_Notification__c  = true,
	NOT( ISNEW() )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HD Notify IT new user request received</fullName>
        <actions>
            <name>Create_new_Domain_User_Account</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type,Software Request Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed,Resolved</value>
        </criteriaItems>
        <description>Notify IT new user request received</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_user_starts_in_48_hours</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Start_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Notifies_ITSystems_New_user_request_received</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HD Notify IT new user request received %28New Mar 2010%29</fullName>
        <actions>
            <name>Notifies_ITSystems_New_user_request_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type,Software Request Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed,Resolved</value>
        </criteriaItems>
        <description>Notify IT new user request received</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_user_starts_in_48_hours</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Start_date_alert__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HD Notify IT that a new hardware request received</fullName>
        <actions>
            <name>HD_Notify_ITsystems_that_a_new_hardware_request_has_been_created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware Request Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Sends and email to IT to advise of new Hardware request and sends follow up if the case remains untouched for 24 hours</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_that_hardware_request_more_than_24_hours_old</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HD Notify ITsystems that a support request received</fullName>
        <actions>
            <name>HD_Notify_ITsystems_that_a_new_general_support_case_has_been_created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General IT Support Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Sends a mail to ITSystems notifying that a new support case has been raised</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_that_hardware_request_more_than_24_hours_old</name>
                <type>Alert</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HD Notify User of Case details</fullName>
        <actions>
            <name>HD_notify_user_of_case_details</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type,General IT Support Record Type,Hardware Request Record Type,Software Request Record Type</value>
        </criteriaItems>
        <description>Sends an email to the creator of the case notifying of case number</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HD Setup New Hardware</fullName>
        <active>false</active>
        <booleanFilter>1 or (2 and 3)OR 4</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Equipment_required__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Hardware Request Record Type</value>
        </criteriaItems>
        <description>Creates tasks as reminders to set up and send hardware</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Ship_New_Hardware_for_USer</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Start_Date__c</offsetFromField>
            <timeLength>-4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HD populate Contact Name from Requester Name</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User Request Record Type,General IT Support Record Type,Webmaster Support Layout,Hardware Request Record Type,Software Request Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HD%3A  Urgent case needs attention</fullName>
        <actions>
            <name>HD_Send_Urgent_Case_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>#URGENT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HD%3A Send case to ServiceNow</fullName>
        <actions>
            <name>HD_Send_transfer_email_to_SN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>HD_Set_status_to_transferred_to_SN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>HD_Set_transfer_to_ServiceNow_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Transfer_to_ServiceNow__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Transferred to ServiceNow</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>I%2EM%2E New Case Roger Bigger</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>equals</operation>
            <value>roger.bigger@citi.com</value>
        </criteriaItems>
        <description>Advise Incident Manager when a new case is created for Roger Bigger from Citi</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Missing Email Address notification</fullName>
        <actions>
            <name>Missing_Email_Address_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>New User Request Record Type,General IT Support Record Type,Hardware Request Record Type,Software Request Record Type</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Move new Case to next Tier after 3 hours of Inactivity</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>First Line</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Move new Case to next Tier after 3 hours of Inactivity - MODIFIED BY GHD to send alert mail instead of change queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Alert_Support_Managers_Case_not_actioned</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Needs Handover</fullName>
        <actions>
            <name>Update_Owner_to_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Needs_Handover__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update needs handover flag and associate to new queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Problem Case to Queue</fullName>
        <actions>
            <name>Update_Owner_to_Problem_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Problem_Case_Status_to_Pending_Ap</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>notEqual</operation>
            <value>Problem Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Problem Case Record Type</value>
        </criteriaItems>
        <description>When a problem case is created by an agent, then it should be assigned to the problem queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New case overdue notification</fullName>
        <actions>
            <name>Set_Priority_Level_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Send notification to helpdesk and update fields</description>
        <formula>AND(
ISPICKVAL( Status , &quot;New&quot; ) ,
ISBLANK( Status_Changed__c ) ,
OwnerId = &quot;00GD0000001H8ja&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_case_overdue_notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Notification_sent_date_time</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>NewCaseFromHighPriorityCustomer</fullName>
        <actions>
            <name>NNotifyDSTRepresentativesHighPriorityCustomer_NewCaseCreated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Prioritisation__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>A customer with Account Prioritization High Priority has opened a new case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NewInfineonCase</fullName>
        <actions>
            <name>Email_sent_to_Oli_when_a_case_is_created_or_escalated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Infineon Technologies</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>First Line,Second Line</value>
        </criteriaItems>
        <description>Temporary alert to Dirk/Katarina when an infineon case is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification%3A Attunity Gold Client Case Created</fullName>
        <actions>
            <name>Notification_New_Attunity_Gold_Client_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Product__c</field>
            <operation>equals</operation>
            <value>Attunity Gold Client</value>
        </criteriaItems>
        <description>Notification for new Attunity Gold Client Cases</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification%3A Azure%2FAWS Case Created</fullName>
        <actions>
            <name>Notification_New_Azure_AWS_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Product__c</field>
            <operation>equals</operation>
            <value>Attunity_Amazon AWS Migrate,Attunity Microsoft Azure Migrate</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification%3A Sev1 Attunity Case Created</fullName>
        <actions>
            <name>Notification_New_Sev_1_Attunity_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Severity__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Product__c</field>
            <operation>contains</operation>
            <value>Attunity</value>
        </criteriaItems>
        <description>Notification for new Attunity Severity 1 cases</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify IT Americas Mgr of new cases in IT%2FUS Queue</fullName>
        <actions>
            <name>Email_2_IT_Manager_Americas_when_a_new_case_is_transferred_to_the_IT_US_Queue</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>IT US</value>
        </criteriaItems>
        <description>Email alert to go to Corey Musselman when cases are transferred to IT/US helpdesk.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Partner and Customer of customer service request</fullName>
        <actions>
            <name>Notification_to_Indirect_Customer_Partner_Details</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Notification_to_Partner_Indirect_Customer_Details</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Closure_Code__c</field>
            <operation>equals</operation>
            <value>Partner Responsible</value>
        </criteriaItems>
        <description>If customer calls QT for support but has a responsible partner, case raised against customer with responsible partner added. Case is closed immediately and customer and partner notified</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Reps that customer has responded via portal</fullName>
        <actions>
            <name>Update_Customer_modified_case_last</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_to_Response_received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If a case is modified in the Customer Portal, case status to change and notification flag set</description>
        <formula>AND(NOT(ispickval(Status,&quot;New&quot;)), OR(  CONTAINS(LastModifiedBy.Profile.Name, &quot;PRM&quot;) , CONTAINS(LastModifiedBy.Profile.Name, &quot;Portal&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Reps that customer has responded via portal - Reset</fullName>
        <actions>
            <name>Update_Customer_modified_case_last_F</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If someone within QT edits the case, the &apos;Customer Modified Case Last&apos; flag must be cleared</description>
        <formula>NOT(OR( CONTAINS(LastModifiedBy.Profile.Name, &quot;PRM&quot;) , CONTAINS(LastModifiedBy.Profile.Name, &quot;Portal&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify the Designated Support Contacts and Engineers</fullName>
        <actions>
            <name>QT_Designated_Support_Send_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Designated_Support_Engineer__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Cases%3A Assigment rule to First Line</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High,Medium / Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Extended_Support__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This rule assigns the case owner to First Line when the case is created by Partner User</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Cases%3A Assigment rule to Second Line</fullName>
        <active>false</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5 AND 6))</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Reseller</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Urgent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.By_pass_First_Line__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Area__c</field>
            <operation>notEqual</operation>
            <value>License</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>This rule assigns new and urgent cases to Second Line</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PRM%3A Cases%3A Assigment rule to Third Line</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Reseller</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Extended_Support__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule assigns new cases to Third Line if indicated as such</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - English</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND ((3 AND (4 OR 6 ) ) OR (5 AND 4) OR (5 AND 6)) and 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Reminder_email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>notEqual</operation>
            <value>Japanese,Finnish,Spanish,German,Swedish,French</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>notEqual</operation>
            <value>French,German,Japanese,Spanish,Swedish,Finnish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Problem Case Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type</value>
        </criteriaItems>
        <description>Pending Reply Customer - English</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remind_Customer_English</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Remind_Customer_English_8day</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - Finnish</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (5 AND 4)) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Reminder_email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
            <value>Finnish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>equals</operation>
            <value>Finnish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Problem Case Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type,Webmaster Support Layout</value>
        </criteriaItems>
        <description>Pending Reply Customer - Finnish</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Remind_Customer_Finnish</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Remind_Customer_Finnish</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - French</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (5 AND 4)) and 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Reminder_email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
            <value>French</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>equals</operation>
            <value>French</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Problem Case Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type</value>
        </criteriaItems>
        <description>Pending Reply Customer - French</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remind_Customer_French</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Remind_Customer_French</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - German</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (5 AND 4)) and 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Reminder_email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
            <value>German</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>equals</operation>
            <value>German</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Problem Case Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type</value>
        </criteriaItems>
        <description>Pending Reply Customer - German</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Remind_Customer_German</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Remind_Customer_German</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - Japanese</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (5 AND 4)) and 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Reminder_email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
            <value>Japanese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>equals</operation>
            <value>Japanese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Problem Case Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type</value>
        </criteriaItems>
        <description>Pending Reply Customer - Japanese</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Remind_Customer_Japanese</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Remind_Customer_Japanese</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - Spanish</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (5 AND 4)) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Reminder_email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Problem Case Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type</value>
        </criteriaItems>
        <description>Pending Reply Customer - Spanish</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remind_Customer_Spanish</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Remind_Customer_Spanish</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - Swedish</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR (5 AND 4)) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Do_Not_Send_Reminder_email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
            <value>Swedish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Language__c</field>
            <operation>equals</operation>
            <value>Swedish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Preferred_Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>QT Support Customer Portal Record Type,QT Problem Case Record Type,QT Support Partner Portal Record Type,QlikTech Master Support Record Type</value>
        </criteriaItems>
        <description>Pending Reply Customer - Swedish</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remind_Customer_Swedish</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Remind_Customer_Swedish</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - notify case owner</fullName>
        <active>false</active>
        <description>Rule is deactivated by setting False in formula field to make sure it won&apos;t trigger in the future. Rule cannot be deactivated on standard way since there are Time based actions that will be performed until 13/08/2013. After this date rule should be delete</description>
        <formula>False</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - notify case owner I</fullName>
        <active>true</active>
        <description>Pending Reply Customer - notify case owner in 3 days</description>
        <formula>AND
(
ISPICKVAL( Status , &quot;Pending Contact Response&quot;),
OR
(
RecordTypeId = &quot;01220000000DZgI&quot;, /* QT Support Customer Portal Record Type */
RecordTypeId = &quot;01220000000IbD1&quot;, /* QT Problem Case Record Type */
RecordTypeId = &quot;01220000000DZgN&quot;, /* QT Support Partner Portal Record Type */
RecordTypeId = &quot;01220000000DZqG&quot; /* QlikTech Master Support Record Type */
),
OR
(
ISBLANK( Next_Communication__c ),
Next_Communication__c &lt; TODAY()
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Pending_Customer_Reply_Start_time__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Customer - notify case owner II</fullName>
        <active>true</active>
        <description>Pending Reply Customer - notify case owner in case when Next communication day is &gt;= Today. The same Email alert and Field update as in rule &apos;Pending Reply Customer - notify case owner I&apos; will be time dependent:  1 day after Next Communication date</description>
        <formula>AND
(
NOT(ISPICKVAL(Status, &apos;Closed&apos;)||
ISPICKVAL(Status, &apos;Ready To Close&apos;)),
OR
(
RecordTypeId = &quot;01220000000DZgI&quot;, /* QT Support Customer Portal Record Type */
RecordTypeId = &quot;01220000000DZgN&quot;, /* QT Support Partner Portal Record Type */
RecordTypeId = &quot;01220000000DZqG&quot; /* QlikTech Master Support Record Type */
),
NOT(ISBLANK( Next_Communication__c )),
Next_Communication__c &gt;= TODAY()
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Follow_up_call_needed_reminder_to_case_owner_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Phone_Call_Needed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Next_Communication__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pending Reply Start Date update</fullName>
        <actions>
            <name>Pending_Reply_Start_Time_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Contact Response</value>
        </criteriaItems>
        <description>Update &apos;Pending Customer Reply Start time&apos; when Case status = Pending Customer Reply.  Used in Workflow to remind customers of auto closure</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PopulateResolutionWithClosureCodeIfEmpty</fullName>
        <actions>
            <name>PopulateResolutionWithClosureCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Resolution__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Closure_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to In Process</fullName>
        <actions>
            <name>Clear_Pending_Reply_Start_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetStatusInProcess</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Work flow to determine when status should be changed to in progress.
1) TakeOwnershipGreg Gregory Hood,Dirk Scharffetter, ,Escalated – Returned
2) Email Reminder flag unchecked</description>
        <formula>OR(  AND(  OR(ISPICKVAL(Status, &quot;Escalated&quot;),ISPICKVAL(Status,&quot;Escalated – Returned&quot;)),  OR( OwnerId = &quot;00520000000z3OJ&quot;, OwnerId = &quot;00520000001LZNX&quot; )  ),  AND(  ISPICKVAL(Status,&quot;Pending Customer Reply&quot;),  ISCHANGED( Do_Not_Send_Reminder_email__c ),  Do_Not_Send_Reminder_email__c = false  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status Escalated Change Date</fullName>
        <actions>
            <name>Update_Status_Escalated_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated,Escalated – Returned</value>
        </criteriaItems>
        <description>Update Status Escalated change date when the status is changed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sup_New_Swedbank_Case</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>startsWith</operation>
            <value>Swedbank</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Swedbank</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Description</field>
            <operation>contains</operation>
            <value>Swedbank</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>First Line,Second Line</value>
        </criteriaItems>
        <description>Send alert to Hanna and Katarina</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TakeOwnership</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Process</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Second Line,Third Line</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update %22Status Changed%22 field</fullName>
        <actions>
            <name>Clear_Notification_sent_date_time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_Changed_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update &quot;Status Changed&quot; field with current date/time</description>
        <formula>ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Escalation if ActiveStatus</fullName>
        <actions>
            <name>Update_Escalated_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Update_internal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Active_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update the Escalated and Last Update (internal) fields if Active Status is TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Escalation if Last ModifiedBy contains Support</fullName>
        <actions>
            <name>Update_Escalated_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Update_internal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Escalated and Last Update (internal) fields if Last Modified By contains “Support”.</description>
        <formula>CONTAINS(LastModifiedBy.Profile__c, &quot;Support&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>CaseStatusPendingCustomerReply7daysPleasefollowup</fullName>
        <assignedToType>owner</assignedToType>
        <description>The status of this case has remained in Pending Customer Reply for more than 7 days. The customer has been notified.

Please update the case accordingly.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case Status = Pending Customer Reply &gt; 7 days - Please follow up</subject>
    </tasks>
    <tasks>
        <fullName>Case_closed</fullName>
        <assignedToType>owner</assignedToType>
        <description>You reminded the customer two days ago, the case will now be closed, pending confirmation. A notification has been sent to the contact, using template: “Day 9 – Case closed</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Case closed</subject>
    </tasks>
    <tasks>
        <fullName>Create_new_Domain_User_Account</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Case.Start_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>HD Create new Domain User Account</subject>
    </tasks>
    <tasks>
        <fullName>Insufficient_Account_details_on_request</fullName>
        <assignedToType>owner</assignedToType>
        <description>Auto email sent</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Insufficient Account details on request</subject>
    </tasks>
    <tasks>
        <fullName>Set_up_New_Hardware</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Set up New Hardware</subject>
    </tasks>
    <tasks>
        <fullName>Set_up_software_accounts</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Set up software accounts</subject>
    </tasks>
    <tasks>
        <fullName>Ship_New_Hardware_for_USer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Ship New Hardware for User</subject>
    </tasks>
    <tasks>
        <fullName>Solution_suggested_seven_days_ago</fullName>
        <assignedToType>owner</assignedToType>
        <description>There was a solution suggested seven days ago, please call the customer and if you cannot reach him/her via phone, please send a reminder to the contact using template: “Day 7 – Case assumed resolved</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Solution suggested 7 days ago</subject>
    </tasks>
    <tasks>
        <fullName>Solution_suggested_three_days_ago</fullName>
        <assignedToType>owner</assignedToType>
        <description>There was a solution suggested three days ago, please call the customer and if you cannot reach him/her via phone, please send a reminder to the contact using template: “Day 3 – Awaiting confirmation“</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Solution suggested three days ago</subject>
    </tasks>
    <tasks>
        <fullName>Task_Warning_Customer_Communication_Milestone</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Task Warning: Customer Communication Milestone</subject>
    </tasks>
    <tasks>
        <fullName>Task_Warning_First_Response_Milestone</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Task Warning: First Response Milestone</subject>
    </tasks>
    <tasks>
        <fullName>TestTask</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Test Task</subject>
    </tasks>
</Workflow>
