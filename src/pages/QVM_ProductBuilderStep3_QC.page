<apex:page controller="QVM_ProductBuilderStep3_QC" sidebar="false" title="{!$Label.QVM_PB_Step_3}">
    <!--
2012-09-03  |  MLN  |  Added left nav for Qonnect launch |  Qonnect Update 2012
2013-05-13  |  PGE  |  Added include file qvm_leftnav to move side nav into one file
2013-12-06  |  MLN  |  CR#9513 https://eu1.salesforce.com/a0CD000000b9oLU #Quick :: QlikMarket Product Preview Style Sheet Incorrect
2015-07-31  |  CCE  |  CR# 48632 Update Qlik Partner Network center URLs

2015-09-29  |  BAD  |  CR# 57108 Added "Save and preview button" and updated behaviour of existing button "Save"
2017-09-27  |  CCE  |  CHG0032112 - adding new fields to page (Product_Acquisition_Visibility__c)
-->
    <link rel="stylesheet" href="https://login.qlikview.com/external/prm/css/qlikmarket.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://qlikpartnerportal.s3.amazonaws.com/qa/prm/css/screen.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://qlikpartnerportal.s3.amazonaws.com/qa/prm/css/partnerportal.css" type="text/css" media="screen" />
    
    <style>
        html body.sfdcBody {
        padding: 0px;
        }
        
        .ajaxLoader {
        display: inline-block;
        background: URL({!$Resource.MM_AjaxLoader});
        width: 43px;
        height: 11px;
        margin-left: 10px;
        
    </style>
    
    <img src="{!$Resource.MM_AjaxLoader}" style="visbility: hidden; display: none;" />
    <apex:includeScript value="{!$Resource.QVM_jquery}" />
    <apex:includeScript value="{!URLFOR($Resource.QVM_plupload, 'plupload.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.QVM_plupload, 'plupload.flash.js')}" />
    
    
    <script type="text/javascript">
    $(document).ready(function($) {
        
        $(".filePanel").hide();
        $(".urlPanel").hide();
        $("#fileContainer").hide();
        $('#uploadfiles').hide();
        $('#uploadFileMessage').hide();
        
        var productMethod = '{!product.Product_Method__c}';
        
        if(productMethod == 'Enter a Referral URL') {
            $(".urlPanel").show();
            $('#productMethod0').attr('checked', true);
        };
        
        if(productMethod == 'Upload Your Product File') {
            $(".filePanel").show();
            $('#productMethod1').attr('checked', true);
        };
        
        makeVisible = function(panelClass1, panelClass2) {
            $(panelClass1).show();
            $(panelClass2).hide();
        };
        
        /*** upload method from http://www.plupload.com ***/
        
        var uploader = new plupload.Uploader({
            runtimes : 'flash',
            browse_button : 'pickfiles',
            multi_selection: false,
            container : 'container',
            max_file_size : '200mb',
            url : 'http://{!bucketName}.s3.amazonaws.com/',
            flash_swf_url : '{!URLFOR($Resource.QVM_plupload, 'plupload.flash.swf')}',
            silverlight_xap_url : '{!URLFOR($Resource.QVM_plupload, 'plupload.silverlight.xap')}',
            filters : [
            {title : "QVW / ZIP Files", extensions : "qvw,zip"}
                                             ],
                                             multipart: true,
                                             multipart_params: {
                                             'key': '{!fileFolder}' + '${filename}', // use filename as a key
                                             'Filename': '${filename}', // adding this to keep consistency across the runtimes
                                             'acl': 'public-read',
                                             //'Content-Type': 'image/jpeg',
                                             'success_action_status': '201',
                                             'AWSAccessKeyId' : '{!AWSAccessKeyId}',
                                             'policy': '{!Policy}',
                                             'signature': '{!SignedPolicy}'
                                             },
                                             // optional, but better be specified directly
                                             file_data_name: 'file',
                                             
                                             // re-use widget (not related to S3, but to Plupload UI Widget)
                                             multiple_queues: true
                                             });
        
        uploader.bind('Init', function(up, params) {
            $('#filelist').html("<div> </div>");
        });
        
        $('#uploadfiles').click(function(e) {
            uploader.start();
            e.preventDefault();
        });
        
        uploader.init();
        
        uploader.bind('FilesAdded', function(up, files) {
            $.each(files, function(i, file) {
                $('#filelist').append(
                    '<div id="' + file.id + '">' +
                    file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                    '</div>');
            });
            
            up.refresh(); // Reposition Flash/Silverlight
            $('#pickfiles').hide();
            $('#uploadfiles').show();
            $('#uploadFileMessage').show();
        });
        
        uploader.bind('UploadProgress', function(up, file) {
            $('#' + file.id + " b").html(file.percent + "%");
        });
        
        uploader.bind('Error', function(up, err) {
            $('#filelist').append("<div>Error: " + err.code +
                                  ", Message: " + err.message +
                                  (err.file ? ", File: " + err.file.name : "") +
                                  "</div>"
                                 );
            
            up.refresh(); // Reposition Flash/Silverlight
        });
        
        uploader.bind('FileUploaded', function(up, file) {
            $('#' + file.id + " b").html("100%");
            QVM_ProductBuilderStep3.setupFileName(file.name, '{!pId}', function(result, event){ 
                if(event.type == 'exception') {
                    alert(event.message);
                } else {
                    alternateUpload();
                }
            });
            
        });
        
    });
    </script>
    <div id="main">
        <div class="clearfix">
            <!--LEFT SIDEBAR-->                 
            
            <!--SIDENAV-->                      
            <div id="sidebar">
                <!-- Start Sidebar Modules -->
                <apex:include PageName="QVM_LeftNav_QC" />
                <!-- End Sidebar Modules -->
            </div>
            <!--END LEFT SIDEBAR-->
            <!--RIGHT COLUMN HEADER-->                      
            <div id="detail">
                <div class="detailLeft">
                    <div class="detailRight">
                        
                        <div class="clearfix">
                            
                            
                            <!--RIGHT COLUMN CONTENT-->
                            <div class="nowrap">
                                <apex:sectionHeader title="{!$Label.QVM_PB_Step_3}" />
                                <apex:variable value="{!IF($CurrentPage.parameters.id == null, false, true)}" var="hasId" />
                                <div class="stepBreadcrum">
                                    <apex:outputText value="{!$Label.QVM_PB_Step_1_Overview}" rendered="{!NOT(hasId)}" />
                                    <apex:outputLink value="qvm_productbuilderstep1_qc?Id={!$CurrentPage.parameters.id}" rendered="{!hasId}">{!$Label.QVM_PB_Step_1_Overview}</apex:outputLink>
                                    <apex:outputText value=" >> " />
                                    <apex:outputText value="{!$Label.QVM_PB_Step_2_Categorization}" rendered="{!NOT(hasId)}" />
                                    <apex:outputLink value="qvm_productbuilderstep2_qc?Id={!$CurrentPage.parameters.id}" rendered="{!hasId}">{!$Label.QVM_PB_Step_2_Categorization}</apex:outputLink>
                                    <apex:outputText value=" >> " />
                                    <span>{!$Label.QVM_PB_Step_3_Product_File}</span>
                                    <apex:outputText value=" >> " />
                                    <apex:outputText value="{!$Label.QVM_PB_Step_4_Product_Images}" rendered="{!NOT(hasId)}" />
                                    <apex:outputLink value="qvm_productbuilderstep4_qc?Id={!$CurrentPage.parameters.id}" rendered="{!hasId}">{!$Label.QVM_PB_Step_4_Product_Images}</apex:outputLink>
                                </div>
                                <apex:messages layout="table" styleClass="errorMessages"/>
                                <apex:form id="productForm" styleClass="productForm">
                                    
                                    <apex:pageBlock mode="edit" id="thepageblock">
                                        
                                        <apex:pageBlockButtons location="both">
                                            <apex:commandButton value="{!$Label.QVM_Back}" action="{!backPage}" immediate="true"/>
                                            <apex:commandButton value="{!$Label.QVM_Save}" action="{!saveProduct}"/>
                                            <apex:commandButton value="{!$Label.QVM_Preview}" action="{!previewPage}"/>
                                            <apex:commandButton value="{!$Label.QVM_Next}" action="{!nextPage}"/>
                                        </apex:pageBlockButtons>
                                        
                                        <div class="tip">
                                            <apex:outputText escape="false" value="{!$Label.QVM_PB_Step_3_Tip}" />
                                        </div>
                                        <apex:pageBlockSection title="{!$Label.QVM_PB_Step_3_Title}" columns="1">
                                            <apex:pageBlockSectionItem >
                                                <apex:outputLabel value="{!$Label.QVM_PB_Step_3_Select_Product_Method}" />
                                                <apex:outputPanel layout="block" styleClass="requiredInput">
                                                    
                                                    <apex:outputPanel layout="block" styleClass="requiredBlock"/>
                                                    <input type="radio" name="productMethod" id="productMethod0" value="Enter a Referral URL" onclick="referralSelected(); " />
                                                    <label for="productMethod0"> {!$Label.QVM_PB_Step_3_Enter_a_Referral_URL}</label>
                                                    <input type="radio" name="productMethod" id="productMethod1" value="Upload Your Product File" onclick="fileSelected(); " />
                                                    <label for="productMethod1"> {!$Label.QVM_PB_Step_3_Upload_Your_Product_File}</label>
                                                    <apex:actionStatus id="methodStatus" startStyleClass="ajaxLoader" />
                                                    
                                                </apex:outputPanel>
                                            </apex:pageBlockSectionItem>
                                            <apex:outputPanel id="filePanel" styleClass="filePanel" style="display: block; padding-top: 10px; margin-left: 19%;">
                                                <apex:outputPanel >
                                                    <apex:variable value="{!IF(LEN(product.Product_Download_Link__c) > 0, true, false)}" var="hasLink" />
                                                    <apex:outputText rendered="{!NOT(hasLink)}">
                                                        <div>{!$Label.QVM_PB_Step_3_File_Types}</div>
                                                        
                                                        <div id="container">
                                                            <div id="filelist" style="padding: 10px 0 0 0; color: red;"><apex:outputText escape="false" value="{!$Label.QVM_PB_Step_3_File_Upload_No_Flash}" /></div>
                                                            <br />
                                                            <input type="button" value="Select File" id="pickfiles" class="btn" />
                                                            <input type="button" value="Upload" id="uploadfiles" class="btn" style="color: red;" />
                                                            <div id="uploadFileMessage" style="width: 200px; padding: 10px 0 0 0;">{!$Label.QVM_PB_Step_3_File_Upload_Message} </div>
                                                            
                                                        </div>
                                                        
                                                    </apex:outputText>
                                                    <apex:outputText rendered="{!hasLink}">
                                                        
                                                        <span><apex:outputText value="{!$Label.QVM_PB_Step_3_Product_File_Label}" /> </span>
                                                        <apex:outputLink value="{!product.Product_Download_Link__c}" >{!product.Product_Download_Link__c}</apex:outputLink>
                                                        <br />
                                                        <br />
                                                        <apex:commandButton action="{!removeProductFile}" value="{!$Label.QVM_PB_Step_3_Remove_File}" />
                                                        
                                                    </apex:outputText>
                                                </apex:outputPanel>
                                            </apex:outputPanel>
                                            <apex:outputPanel id="urlPanel" styleClass="urlPanel" style="display: block; padding-top: 10px; margin-left: 19%;">
                                                <apex:outputPanel >
                                                    
                                                    <apex:variable value="{!IF(ISBLANK(product.Referral_Link__c), true, false)}" var="showInput" />
                                                    <span>{!$Label.QVM_PB_Step_3_URL} </span>
                                                    <apex:inputField value="{!product.Referral_Link__c}" style="width: 300px;" rendered="{!showInput}" />
                                                    <apex:commandButton action="{!quickSave}" value="{!$Label.QVM_Save_URL}" rendered="{!showInput}" />
                                                    <apex:outputText value="{!$Label.QVM_PB_Step_3_Must_Begin}" rendered="{!showInput}" escape="false" />
                                                    <apex:outputLink value="{!product.Referral_Link__c}" rendered="{!NOT(showInput)}">{!product.Referral_Link__c}</apex:outputLink>
                                                    <apex:commandButton action="{!removeProductReferral}" value="{!$Label.QVM_PB_Step_3_Remove_URL}" rendered="{!NOT(showInput)}" style="margin-left: 10px;" />
                                                </apex:outputPanel>
                                            </apex:outputPanel>
                                        </apex:pageBlockSection>
                                        <apex:pageBlockSection columns="1">
                                            <apex:inputField value="{!product.Product_Acquisition_Visibility__c}" required="false" style="width: 300px;" />
                                        </apex:pageBlockSection>
                                    </apex:pageBlock>
                                </apex:form>
                                <apex:form id="secondaryState">
                                    <apex:actionFunction name="referralSelected" reRender="" status="methodStatus" oncomplete="makeVisible('.urlPanel', '.filePanel');" >
                                        <apex:param name="productMethod" assignTo="{!productMethod}" value="Enter a Referral URL"/>
                                    </apex:actionFunction>
                                    <apex:actionFunction name="fileSelected" reRender="" status="methodStatus" oncomplete="makeVisible('.filePanel', '.urlPanel');" >
                                        <apex:param name="productMethod" assignTo="{!productMethod}" value="Upload Your Product File"/>
                                    </apex:actionFunction>
                                    <apex:actionFunction action="{!alternateUploadFile}"  name="alternateUpload" reRender="filePanel" status="methodStatus" />
                                </apex:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</apex:page>