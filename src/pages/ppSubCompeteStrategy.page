<apex:page >

<script type="text/javascript" src="https://qvfiles.s3.amazonaws.com/partner_portal/js/virtualpaginate.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="https://qvfiles.s3.amazonaws.com/partner_portal/js/animatedcollapse.js"></script>

<script type="text/javascript">
//Addressing the Competition
animatedcollapse.addDiv('compq1', 'fade=1,speed=200,group=countries')
animatedcollapse.addDiv('compq2', 'fade=1,speed=200,group=countries')
animatedcollapse.addDiv('compq3', 'fade=1,speed=200,group=countries')
animatedcollapse.addDiv('compq4', 'fade=1,speed=200,group=countries')

animatedcollapse.ontoggle=function($, divobj, state){ //fires each time a DIV is expanded/contracted
    //$: Access to jQuery
    //divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
    //state: "block" or "none", depending on state
}

animatedcollapse.init()
</script>

<style>
.countryDDs {
margin:25px;
}

#compq1, #compq2, #compq3, #compq4

 {
margin-left:17px;
padding-bottom:10px;
padding-left:10px;
}

/*#subitemifneeded */
#subbullet > p {
margin-left:25px;
}

#mainenablecontainer {
     width : 892px;
     height : 100px;
     padding-bottom:10px;
     }
#mainenablecontainer img {
        width : 250px;
        height : 100px;
        margin:0 auto;
        padding-right:10px;
        display:block;
        float:left;
        opacity:1.0;
        filter:alpha(opacity=40); /* For IE8 and earlier */
}
#mainenablecontainer a:hover
{
    opacity:0.4;
    filter:alpha(opacity=100); /* For IE8 and earlier */
}
</style>
  <!-- MENU -->
    <div id="mainenablecontainer">
        <div>
          <apex:outputlink value="ppCompeting?SubPage=VendorTypes"><img class="displayed" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/CompeteCenter/VendorFade-250x100.jpg" border="0" width="250" /></apex:outputlink>

          <apex:outputlink value="ppCompeting?SubPage=Respond"><img class="displayed" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/CompeteCenter/Respond-250x100.jpg" border="0" width="250" /></apex:outputlink>
          
          <apex:outputlink value="ppCompeting?SubPage=FindMore"><img class="displayed" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/CompeteCenter/MoreInfoFade-250x100.jpg" border="0" width="250" /></apex:outputlink>

        </div>
    </div>

    <p style="clear:left;">&nbsp;</p>

  	<h1>Competitive Strategy</h1>
  	<h2><font style="color:#62ac1e;">Respond to questions about the competition while retaining your credibility and integrity.</font></h2>
	<p>Your response to any question is important to retain your credibility and integrity. The customer is not likely to buy from a seller they don’t believe or trust. How you respond to questions about the competitor is just as important. With many vendors on rapid release cycles, it’s difficult to keep up with what features are available and it’s easy to become out of date. These responses have been crafted with this in mind.</p>
	<p>If the customer asks:</p>
   
   	<p><a href="#" rel="toggle[compq1]" data-openimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/collapse.png" data-closedimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png" border="0" /></a> <a href="javascript:animatedcollapse.toggle('compq1')"><b>How does Qlik compare to other vendors in the market?</b></a>

        <div id="compq1" style="display:none">Don’t compare Qlik to specific vendors. Instead, compare Qlik’s approach to the approach of the broader market. The following are some examples are how you could start your response.          
        <ul>
        <li>"Most vendors are reliant on SQL to create reports which limits insight to only a small amount of information."</li>
        <li>"Many self-service vendors do one thing extremely well but run into difficulty when addressing broader use cases."</li>
        <li>"The large BI vendors typically have dependencies on other products which means you may need to purchase additional tools or upgrade other platform components to get upgraded BI which significantly increases TCO and reduces agility."</li> 
        <li>"Generally, when people think about self-service, they only consider the authoring aspect. But not all users want to author – in fact, the majority of BI users will only ever consume information authored for them."</li>
        </ul>
        Follow these openings by highlighting how Qlik’s approach is different with a focus on the <a target="_blank" href="/apex/ppCompeting?SubPage=CoreDifferentiators">core differentiators</a>.
        </div>
    </p>

   <hr style="margin-bottom:15px;" color="#efefef" width="100%" size="1" />

   <p><a href="#" rel="toggle[compq2]" data-openimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/collapse.png" data-closedimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png" border="0" /></a> <a href="javascript:animatedcollapse.toggle('compq2')"><b>Can you provide a comparison between Qlik and vendor X?</b></a>

        <div id="compq2" style="display:none">
        <ul>
        	<li>Highlight that while other vendors may do this, it is not part of Qlik’s culture to provide these competitive comparisons. They will never be unbiased opinions. Every competitive comparison will be skewed in favor of the vendor who wrote the comparison and won’t always take the customer’s unique requirements into account. <strong>Focus on the customer’s specific needs and highlight how we can address them.</strong></li>
        	<li>If pressed for a response, don't speak negatively about the competition:
	          <ul id="subbullet">
	          <li>Acknowledge the competitor for what they do well.</li>
	          <li>Provide truthful statements about where the customers may struggle to address their requirements due to competitor’s weakness.</li>
	          <li>Quickly pivot the conversation back to <a target="_blank" href="/apex/ppCompeting?SubPage=CoreDifferentiators">Qlik’s Key Differentiators</a> and how they can better serve the customers use case.</li>
	          <li>Keep the conversation about competitors to a minimum and use that valuable time to set the agenda for Qlik</li>
	          </ul>
	        </li>
        </ul>
        </div>
    </p> 
   
   <hr style="margin-bottom:15px;" color="#efefef" width="100%" size="1" />
   
   <p><a href="#" rel="toggle[compq3]" data-openimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/collapse.png" data-closedimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png" border="0" /></a> <a href="javascript:animatedcollapse.toggle('compq3')"><b>Can you respond to this competitive comparison papers written by vendor X?</b></a>

        <div id="compq3" style="display:none">
        <ul>
        	<li>The least effective approach is a point by point response. This makes you look like you’re on the defensive and it’s responding to an agenda set by another vendor. The most effective approach is to discredit the document.
	          <ol id="subbullet">
	          <li>Ask if the customer feels the document is unbiased.</li>
	          <li>Ask if they can take any of the document seriously if you can prove a few obvious errors (they always have some errors or misrepresentation of features).</li>
	          <li>Highlight the document is written from a vendor-specific point of view about best practice for delivery of BI and will reflect their own strengths and ignore their weaknesses.</li>
	          <li>If the customer still is not prepared to discount the document, ask what parts of the document are applicable to them and address only their concerns about meeting business requirements.</li>
	          <li>Expose other business requirements that are not addressed by the competitor’s comparison and highlight how Qlik’s unique approach is better suited to meeting them.</li>
	          </ol>
	        </li>
	        <li>If the customer demands a point-by-point response and you don’t have a strong champion in the account that can help change the agenda, the opportunity is likely not yours to win.</li>
        </ul>
        </div>
    </p>

   <hr style="margin-bottom:15px;" color="#efefef" width="100%" size="1" />
   
   <p><a href="#" rel="toggle[compq4]" data-openimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/collapse.png" data-closedimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png" border="0" /></a> <a href="javascript:animatedcollapse.toggle('compq4')"><b>Can you tell me why Qlik placed lower than vendor X in this analyst report?</b></a>

        <div id="compq4" style="display:none">
        <ul>
          <li>Highlight that Qlik's standings and rankings in analyst reports vary, depending on the topic of the report (e.g., modern BI, traditional BI, self-service data preparation, etc.), the Qlik product(s) and version(s) evaluated, and the analyst’s perspective and research methodology.</li>
          <li>Remind them that with Qlik’s rapid release cycle (Qlik Sense three times a year), analyst reports of Qlik Sense  and other Qlik products quickly become outdated.</li>
          <li>Use quotes or rankings from other analyst firms (or even the same firm) as valid counterpoints to analyst rankings.</li>
        </ul>
        </div>
    </p>
</apex:page>