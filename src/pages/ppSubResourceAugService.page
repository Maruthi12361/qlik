<!-- JNR 2016-10-19 CR# 98791-->

<apex:page title="Augmentation Services">

<div><img style="float:left; margin-right:15px; margin-bottom: 20px;" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/ConsultingPeople.png" width="80" />

    <h1 style="margin-bottom:5px;"><br />Augmentation Services</h1>

</div>

<p style="clear:left;"></p>

<p><strong>Qlik Partner Augmentation Services</strong> helps you maximize success for your Qlik 
customers and ensure implementation and delivery services provide the most value. This suite of implementation, project management and strategy services complement the services you provide to your customers. Offered through joint delivery with Qlik Consulting, these services accelerate value realization and adoption of Qlik solutions.</p>

<!--RELATED RESOURCES BEGIN-->
<div style="width:200px; float:right; margin-left:10px;">


<div class="module m1">
    <div class="module m2">
        <div class="module m3">
            <div id="uiRowWrapper">
                <div id="uiColWrapper">
                    <div class="bubble blue" id="textBoxWrapper">
                        <div id="uiSingleText">
                            <p><strong>Consulting Resources</strong><br />
                            <ul class="linkedlist">
                                <!--<li><a href="">Why to Resell Education Services</a></li>-->
                                <li><a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003Y9Wh">Augmentation Services Overview</a></li>
                                <li><a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003rqkL">Augmentation Services Catalog</a></li>
                                <!--li><a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003rJ2k">eBook</a></li-->
                                <li><a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003Y9XV">Augmentation Data Sheet</a></li>
                                <li><a target="_blank" href="http://www.qlik.com/us/services/qlik-consulting">Qlik.com Consulting Page</a></li>
                                <li><a target="_blank" href="https://login.qlik.com/lms/qed.aspx?returnURL=http://qed.qlik.com/auth/saml/index.php?wantsurl=/pluginfile.php/11111/mod_resource/content/1/Partner%20Learning/index.html">Partner Learning Tracks</a></li>
                            </ul>
                            </p>
                           
                        </div>
                    <div id="uiFooter"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="module m1">
    <div class="module m2">
        <div class="module m3">
            <div id="uiRowWrapper">
                <div id="uiColWrapper">
                    <div class="bubble blue" id="textBoxWrapper">
                        <div id="uiSingleText">
                            <p><i>“Qlik Services is a key consulting partner for Bardess’ Qlik Sense team.  We jointly work on projects at each other’s customers leveraging our unique skills and client relationships.  It is a two-way relationship based upon trust and combining our skills to best meet our clients’ needs.”</i><br />
                            Joseph L. DeSiena
                            </p>                           
                        </div>
                    <div id="uiFooter"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<!--RELATED RESOURCES END-->




<p style="clear:left;"></p>

<div style="width:650px;">

    <h2 style="color:#62ac1e;"><strong>Why we're better together</strong></h2>

    <div class="clearfix">
        <div class="floatleft mr20"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Agreement_color.png" width="53" alt="joint"/></div>
        <div class="nowrap">
            <h2 class="nomargin">Deliver jointly with the Qlik Consulting team</h2>
            <p>Ensure rapid delivery of solutions to your customers.</p> 
                                                                                 
        </div>

        <div class="floatleft mr20"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/QlikSuccess-Color.png" width="53" alt="accelerate"/></div>
        <div class="nowrap">
            <h2 class="nomargin">Accelerate adoption of your customer’s Qlik solution</h2>
            <p>Maximize your customer’s success in developing, deploying and using Qlik.</p> 
                                                                                   
        </div>

        <div class="floatleft mr20"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Piggybank_color.png" width="53" alt="reduce"/></div>
        <div class="nowrap">
            <h2 class="nomargin">Reduce your investment in specialized Qlik skilled hires</h2>
            <p>Avoid costly in-house resources to augment your capabilities.</p> 
                                                                                  
        </div>
        
        <div class="hr"></div>   

        <h2 style="color:#62ac1e;"><strong>Engage with Qlik implementation capabilities</strong></h2>
        
        <div id="position">
            <ol>
                <h2><li>Complete the <a target="_blank" href="https://login.qlik.com/lms/qed.aspx?returnURL=http://qed.qlik.com/course/view.php?id=186">Consulting Qlik Education and Development</a> module and explore the <a target="_blank" href="https://login.qlik.com/lms/qed.aspx?returnURL=http://qed.qlik.com/auth/saml/index.php?wantsurl=/pluginfile.php/11111/mod_resource/content/1/Partner%20Learning/index.html">Consulting Partner Learning Track Station</a></li>
                <li>Download and review the <a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003Y9Wh">Augmentation Services Overview</a>, <a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003Y9XV">Augmentation Data Sheet</a>, <a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003rqkL">Augmentation Services Catalog</a></li>
                <li>Reach out to your <a href="http://www.qlik.com/us/services/qlik-consulting/contact-consulting">local Qlik Consulting office</a> to get the conversation started.</li></h2>
            </ol>

        </div>
    </div>

</div>

</apex:page>