<!--
Changelog: 
           20141104   PGE  CR# 18807 Update content for Qonnections 2015
           20141212   JNR  CR# 20380 Update content for Qonnections 2015
           20141216   JNR  CR# 20545 Update content for Qonnections 2015
           20150106   JNR  CR# 21120 Update content to announce Qonnections 2015 winners
           20150422   JNR   CR# 36347 Remove Q4 Qonnections 2015 promo and winners
           20150817   JNR  CR# 55115 Add Score with Qlik Sense program
-->

<apex:page title="ProgramsAndContests">

<style>
#inlineexternal { background: url('https://login.qlik.com/external/prm/images/icons/icon_external.png') center right no-repeat; padding-right: 18px; margin-left:0px !important;}
}
</style>

<div style="width:800px;">

<!--<div class="floatright"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/DeviseImages.png" width="220"/></div>-->
<h2>The incentive program to learn, market and sell Qlik Sense</h2>

<div class="floatright ml15"><a target="_blank" href="https://qtube.qlik.com/videos/?v=ScoreWithQlikSense/02_Score_QlikSense_Q4_hd.mp4"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/video_stills/Score_with_Qlik_Sense_Q3.png" width="220" alt="Score with Qlik Sense"/></a><br />
<p><strong>A video message from Rick Jackson</strong></p></div>

<div style="no wrap">

<p style="clear:left;">&nbsp;</p>

<p style="FONT-SIZE: 16px;">Qlik Sense provides an opportunity to drive more revenue. The Score with Qlik Sense Program rewards partner organizations and is formed on 3 pillars: Learn, Market, Sell. A partner organization could participate in any or all pillars.</p>

<!--p style="FONT-SIZE: 16px;">The winners from Q4 2015 will be announced shortly in 2016. Check back soon to learn who won!</p-->

</div>

<p style="clear:left;">&nbsp;</p>

<div class="roundedcorners greyborder"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/WinnerBannerQ4_QlikSense_1000x258.png" width="800" alt="winners"/></div>

<p style="clear:left;">&nbsp;</p>

<!--<p><I>*The Sell pillar winner will be announced following the close of the pillar on November 25<sup>th</sup>.</I></p>-->

    <!--<div class="clearfix">
        <div class="floatleft mr20"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Perks-Color.png" width="53" alt="fame"/></div>
        <div class="nowrap">
            <h2 class="nomargin"><a href="#FAME">Hall of Fame</a></h2>
            <p>See who won in Q3! What will you need to do to win?</p>
            <div class="hr"></div> 
        </div>
        </div>-->

<!--
<h2 style="color:#62ac1e;"><strong>A video message from Rick Jackson</strong></h2>
<p style="clear:left;"></p>
<div class="floatleft"><a target="_blank" href="https://qtube.qlik.com/videos/?v=ScoreWithQlikSense/01_Score_QlikSense_Q3_hd.mp4"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/video_stills/Score_with_Qlik_Sense_Q3.jpg" width="220" alt="Score with Qlik Sense"/></a></div>
<p style="clear:left;">&nbsp;</p>-->
<!--
<img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/Qlik_WhiteboardBanner_2.5.jpg" width="800" alt="Start Learning Marketing and Selling"/>

<p style="clear:left;">&nbsp;</p>
-->

<table cellspacing="0" cellpadding="0" border="0" width="800" align="left">
    <tbody>
        <tr class="odd">
            <th style="padding-right:15px; padding-left:100px;" width="33%"><h2 style="color:#62ac1e; font-weight: bold;">Learn</h2><br />
            <img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/eLearning_color_350.png" width="63"/></th>
            <th style="padding-right:15px; padding-left:100px;" width="33%"><h2 style="color:#62ac1e; font-weight: bold;">Market</h2><br />
            <img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/CampaignCenter_color.png" width="63"/></th>
            <th style="padding-left:100px;" width="33%"><h2 style="color:#62ac1e; font-weight: bold;">Sell</h2><br />
            <img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Solution_color.png" width="63"/></th>
        </tr>
       <!--<tr class="odd">
            <td colspan="3"><div class="col800 left roundedcorners greyborder height85"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/promos/Qlik_WhiteboardBanner_LearnMarketSell.png" width="800"/></div></td>
        </tr>-->
        <tr class="odd">
            <td style="padding-right:15px; padding-left:15px;" width="33%"><p style="FONT-SIZE: 14px;">Use <a target="_blank" id="#inlineexternal" href="https://login.qlik.com/lms/qed.aspx">Qlik Education and Development</a> content and materials to better enable your teams. Certification validates your knowledge and skills with Qlik® software, and can lead to accelerated professional development.<br /><br />
            The partner organization with the most Qlik Sense Certifications completed and passed by employees in all of 2015 will be awarded with a US$1000 prize voucher. All certification courses taken in 2015 to date will be included in the total count of courses completed and passed.</p></td>
            <td style="padding-right:15px; padding-left:15px;" width="33%"><p style="FONT-SIZE: 14px;">Go to market with strength and conviction, drive awareness and demand for Qlik Sense.<br /><br />
            The partner organization that executed a marketing campaign, as listed in the playbook, in the <a target="_blank" href="/apex/PPMarketing?SubPage=MarketingServices">Qlik Partner Marketing Services</a> and generated the highest number of qualified opportunities will be awarded with a US$1000 prize voucher.</p></td>
            <td style="padding-left:15px;"><p style="FONT-SIZE: 14px;" width="33%">Leverage the <a target="_blank" href="/apex/PPSales?SubPage=SalesPrograms">Q4 Promotion Packages</a>, sales content, materials and best practices to achieve and exceed your revenue goals.<br /><br />
            The partner organization that executed the highest aggregate total value (calculated in U.S. Dollars) Close-Won transactions for Qlik Sense licenses included in the Q4 Promotion Packages will be awarded a US$1000 prize voucher.</p></td>
        </tr>
        <tr class="odd">
            <td style="padding-right:15px; padding-left:15px;" width="33%"><p style="FONT-SIZE: 14px; color:#F8981D;">
            <strong>Closing date: December 31, 2015.</strong></p></td>
            <td style="padding-right:15px; padding-left:15px;" width="33%"><p style="FONT-SIZE: 14px; color:#F8981D;">
            <strong>Closing date: December 31, 2015.</strong></p></td>
            <td style="padding-left:15px;"><p style="FONT-SIZE: 14px; color:#F8981D;" width="33%">
            <strong>Closing date: December 31, 2015.</strong></p></td>
        </tr>

        <tr class="odd">
            <td style="padding-right:15px; padding-left:60px;"><a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000002TV9H"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/Start_Learning.jpg" /></a></td>
            <td style="padding-left:60px;"><a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000002TV9H"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/Start_Marketing.jpg" /></a></td>
            <td style="padding-left:67px;"><a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000002TV9H"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/Start_Selling.jpg" /></a></td>
        </tr>
    </tbody>
</table>

<p style="clear:left;">&nbsp;</p>

<h2>Resources<br /></h2>

<p style="clear:left;">&nbsp;</p>

    <div class="floatleft mr10"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Preperation_color.png" width="30" style="vertical-align:middle; padding-left:40px;" /></div>
    <div class="nowrap">&nbsp;<a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000002TV9H">Score with Qlik Sense Playbook</a><br />
    &nbsp; &nbsp; Get details on each pillar of the competition.</div>

    <p style="clear:left;"></p>

    <div class="floatleft mr10"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Recipient_color.png" width="30" style="vertical-align:middle; padding-left:40px;" /></div>
    <div class="nowrap">&nbsp;<a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000002TV9C">Incentive Program Prize Catalogue</a><br />
    &nbsp; &nbsp; Learn about the prizes you could win!</div>

    <p style="clear:left;"></p>

    <div class="floatleft mr10"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/RulesEngine_color.png" width="30" style="vertical-align:middle; padding-left:40px;" /></div>
    <div class="nowrap">&nbsp;<a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000002TV97">Terms and Conditions</a></div>

<div class="hr"></div> 
<p style="clear:left;">&nbsp;</p>

<!--<p><a style="VISIBILITY: hidden; TEXT-DECORATION: none" id="FAME" class="FAME" title="FAME" name="FAME" alias="FAME"></a></p>

    <div class="clearfix">
        <div class="floatleft mr20"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Perks-Color.png" width="53" alt="fame"/></div>
        <div class="nowrap">
            <h2 class="nomargin">Hall of Fame</h2>
            <p>See what it takes to make a winner!</p>
        </div>
        </div>

<p style="clear:left;">&nbsp;</p>

<table cellspacing="0" cellpadding="0" border="0" width="800" align="left">
    <tbody>
        <tr class="odd">
            <th style="padding-right:15px; padding-left:102px;" width="33%"><h2 style="color:#62ac1e; font-weight: bold;">Learn</h2></th>
            <th style="padding-right:15px; padding-left:100px;" width="33%"><h2 style="color:#62ac1e; font-weight: bold;">Market</h2></th>
            <th style="padding-left:110px;" width="33%"><h2 style="color:#62ac1e; font-weight: bold;">Sell</h2></th>
        </tr>
        <tr class="odd">
            <th style="padding-right:15px; padding-left:100px;" width="33%"><br />
            <img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Champion-Color.png" width="63"/></th>
            <th style="padding-right:15px; padding-left:100px;" width="33%"><br />
            <img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Champion-Color.png" width="63"/></th>
            <th style="padding-left:100px;" width="33%"><br />
            <img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Champion-Color.png" width="63"/></th>
        </tr>        
        <tr class="odd">
            <td style="padding-right:15px; padding-left:25px;" width="33%"><p style="FONT-SIZE: 14px;">How is your organization increasing its Qlik Sense Learning? Here are the top three partners:
            <ol>
            <strong><li>15 courses</li></strong>
            <li>12 courses</li>
            <li>12 courses</li>
            </ol></p></td>
            <td style="padding-right:15px; padding-left:25px;" width="33%"><p style="FONT-SIZE: 14px;">How far is your demand generation from being first? Compare your organization to the current leaders:
            <ol>
            <strong><li>25 downloads</li></strong>
            <li>20 downloads</li>
            <li>10 downloads</li>
            </ol></p></td>
            <td style="padding-left:25px;"><p style="FONT-SIZE: 14px;" width="33%">As the leads start converting into pipeline, we’ll show you who’s leading with Qlik Sense wins.</p></td>
        </tr>
        <tr class="odd">
            <td style="padding-left:67px;"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="40" style="padding-right:5px;"/><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="60" style="padding-right:5px;"/><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="80" style="padding-right:5px;"/></td>
            <td style="padding-left:67px;"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="40" style="padding-right:5px;"/><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="60" style="padding-right:5px;"/><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="80" style="padding-right:5px;"/></td>
            <td style="padding-left:67px;"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="40" style="padding-right:5px;"/><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="60" style="padding-right:5px;"/><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Repository_color.png" width="40" height="80" style="padding-right:5px;"/></td>
        </tr>
    </tbody>
</table>

<p style="clear:left;">&nbsp;</p>-->

<p>Questions? <a href="mailto:partnermarketing@qlik.com?subject=Score with Qlik Sense">Contact Us</a></p>

<div style="background-color:#def1fc; border:1px solid #98c3dc; padding:10px 10px 0px 10px; width:800px;">
    <p style="color:#333333;">Please note participation in the Contest is option. However, by successfully completing any Qlik Sense Certifications at any time in 2015, using the Campaigns or transacting any Qlik Sense Q4 promotion package deal during the Contest period, you will be deemed to have entered the Contest and agreed to be bound by the Contest Terms. <strong>If you do not wish to participate in the Contest, please email <a href="mailto:partnermarketing@qlik.com?subject=Q4 2015 Contest opt-out">partnermarketing@qlik.com</a> and include “Q4 2015 Contest opt-out” in the subject line.</strong></p>
</div>

</div>

</apex:page>