<apex:page controller="QVM_ProductBuilderStep1_QC" sidebar="false" title="{!$Label.QVM_PB_Step_1}" action="{!checkRegistration}">
    <!--
2012-09-03  |  MLN  |  Added left nav for Qonnect launch |  Qonnect Update 2012
2013-05-02  |  CCE  |  Added link to return to Expertise Program page (ppqonnectprogrampage?SubPage=PEP) CR# 8031 - #Quick - add Return to Expertise Program link
2013-05-13  |  PGE  |  Added include file qvm_leftnav to move side nav into one file
2013-12-06  |  MLN  |  CR#9513 https://eu1.salesforce.com/a0CD000000b9oLU #Quick :: QlikMarket Product Preview Style Sheet Incorrect
2015-07-31  |  CCE  |  CR# 48632 Update Qlik Partner Network center URLs
2015-09-29  |  BAD  |  CR# 57108 Added "Save and preview button" and updated behaviour of existing button "Save"
2015-12-07  |  CCE  |  CR#40489 https://eu1.salesforce.com/a0CD000000tYHWh Qlik Market - add notice to register to additional pages
2017-09-27  |  CCE  |  CHG0032112 - adding two new fields to page (Product_Visibility__c, Ajax_Demo_Visibility__c)

-->
    <link rel="stylesheet" href="https://login.qlikview.com/external/prm/css/qlikmarket.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://qlikpartnerportal.s3.amazonaws.com/qa/prm/css/screen.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://qlikpartnerportal.s3.amazonaws.com/qa/prm/css/partnerportal.css" type="text/css" media="screen" />
    
    <style>
        html body.sfdcBody {
        padding: 0px;
        }
    </style>
    <div id="main">
        <div class="clearfix">
            <!--LEFT SIDEBAR-->                 
            
            <!--SIDENAV-->                      
            <div id="sidebar">
                <!-- Start Sidebar Modules -->
                <apex:include PageName="QVM_LeftNav_QC" />
                <!-- End Sidebar Modules -->
            </div>
            <!--END LEFT SIDEBAR-->
            <!--RIGHT COLUMN HEADER-->                      
            <div id="detail">
                <div class="detailLeft">
                    <div class="detailRight">
                        
                        <div class="clearfix">
                            
                            
                            <!--RIGHT COLUMN CONTENT-->
                            <div class="nowrap">
                                
                                <apex:sectionHeader title="{!$Label.QVM_PB_Step_1}" />
                                <apex:variable value="{!IF($CurrentPage.parameters.id == null, false, true)}" var="hasId" />
                                <div class="stepBreadcrum"> <span>{!$Label.QVM_PB_Step_1_Overview}</span>
                                    <apex:outputText value=" >> " />
                                    <apex:outputText value="{!$Label.QVM_PB_Step_2_Categorization}" rendered="{!NOT(hasId)}" />
                                    <apex:outputLink value="qvm_productbuilderstep2_qc?Id={!$CurrentPage.parameters.id}" rendered="{!hasId}">{!$Label.QVM_PB_Step_2_Categorization}</apex:outputLink>
                                    <apex:outputText value=" >> " />
                                    <apex:outputText value="{!$Label.QVM_PB_Step_3_Product_File}" rendered="{!NOT(hasId)}" />
                                    <apex:outputLink value="qvm_productbuilderstep3_qc?Id={!$CurrentPage.parameters.id}" rendered="{!hasId}">{!$Label.QVM_PB_Step_3_Product_File}</apex:outputLink>
                                    <apex:outputText value=" >> " />
                                    <apex:outputText value="{!$Label.QVM_PB_Step_4_Product_Images}" rendered="{!NOT(hasId)}" />
                                    <apex:outputLink value="qvm_productbuilderstep4_qc?Id={!$CurrentPage.parameters.id}" rendered="{!hasId}">{!$Label.QVM_PB_Step_4_Product_Images}</apex:outputLink>
                                </div>
                                <apex:messages styleClass="errorMessages" title="Required Fields" />
                                <apex:form id="productForm" styleClass="productForm">
                                    
                                    <!-- CCE CR# 8031 -->
                                    <apex:pageBlock mode="edit" id="thepageblock" title="Welcome to the Qlik Market Product Creation Wizard!">
                                        
                                        <apex:pageBlockButtons location="both">
                                            <apex:commandButton value="{!$Label.QVM_Save}" action="{!saveProduct}"/>
                                            <apex:commandButton value="{!$Label.QVM_Preview}" action="{!previewPage}"/>
                                            <apex:commandButton value="{!$Label.QVM_Next}" action="{!nextPage}"/>
                                            <apex:commandButton value="{!$Label.QVM_Cancel}" action="{!cancel}" immediate="true" rendered="{!showCancelButton}"/>
                                        </apex:pageBlockButtons>
                                        
                                        <div class="tip">
                                            <apex:outputText escape="false" value="{!$Label.QVM_PB_Step_1_Details}" />
                                        </div>
                                        <apex:pageBlockSection title="{!$Label.QVM_PB_Product_Details}" columns="2">
                                            <apex:inputField value="{!product.Product_Name__c}" required="true" styleClass="pName" />
                                            <apex:inputField value="{!product.Product_Visibility__c}" required="false" styleClass="pName" />
                                            <!-- Add the javascript file to intialize the CkEditor -->
                                            <apex:includescript value="{!URLFOR($Resource.CkEditor, 'ckeditor/ckeditor.js')}" />
                                            
                                        </apex:pageBlockSection>
                                        <apex:pageBlockSection columns="1">
                                            <apex:inputTextarea id="overview" value="{!product.Overview__c}" required="true" style="width: 100%;" richtext="false" styleClass="ckeditor" />
                                            
                                            <apex:inputField value="{!product.Short_Description__c}" required="true" style="width: 100%;" />
                                        </apex:pageBlockSection>
                                        <apex:pageBlockSection columns="2">
                                            <apex:inputField value="{!product.Ajax_Demo_Link__c}" required="false" style="width: 300px;" />
                                            <apex:inputField value="{!product.Ajax_Demo_Visibility__c}" required="false" style="width: 300px;" />
                                        </apex:pageBlockSection>
                                    </apex:pageBlock>
                                </apex:form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</apex:page>