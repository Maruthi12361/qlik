<!--
Changelog: May 9 2014  PGE  Modified page layout with new version including conent updates and new gpf links  CR# 12585 /a0CD000000iDjfU         
May 14, 2014: MLN Readded the dynamic autologin link for QChi 
2015-02-19 JNR update the page CR# 24381 https://eu1.salesforce.com/a0CD000000qepW1 - remove links to the old tool, just leaving QChi         
2015-12-02 BAD, CR# 64732,  update of global funding link 
2017-03-31 BAD, Removed dead code for Q2CW deployment
-->

<apex:page title="Market Development Funds" controller="PPMarketingPortalClass" action="{!getUserInfo}">


<!--Sliding div BEGIN-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="https://qvfiles.s3.amazonaws.com/partner_portal/js/animatedcollapse.js"></script>

<script type="text/javascript">
animatedcollapse.addDiv('register', 'fade=1')

animatedcollapse.ontoggle=function($, divobj, state){ //fires each time a DIV is expanded/contracted
  //$: Access to jQuery
  //divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
  //state: "block" or "none", depending on state
}

animatedcollapse.init()
</script>
<style>
.countryDDs {
margin-left:25px;
}
#ondemandqvtechlibrary {
margin-top:-17px;
}
#inlineexternal { background: url('https://login.qlik.com/external/prm/images/icons/icon_external.png') center right no-repeat; padding-right: 18px; margin-left:0px !important;}
}
</style>

<!--Sliding div END-->

<div class="intro">

    <h1>Qlik Market Development Fund Program</h1>

    <div class="roundedcorners" style="background-color:#F0F0F0 ; border:1px; padding:10px 10px 0px 10px; width:855px;"> 

    <div><img style="float:left; margin-top:0px; margin-left:10px; margin-right:20px; margin-bottom: 5px;" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/MDF-Logo-Web.png" width="100" /> 

    <div class="nowrap"><h2>Market Development Fund (MDF)</h2>
    <p>MDF is a discretionary program available to eligible Partners to drive revenue and pipeline. Together we can drive toward mutual success and a continued strong partnership.<br /><br /></p></div>
    </div> 

    </div>

</div>


    <!--p><font style="color:#F8981D; font-size:16px"><strong>Do you have outstanding GPF requests?</strong></font> Please <a href="#GPF">click here</a> for information on the retirement of GPF.</p-->


<div style="width:200px; float:right; margin-left:10px;">

    <!--RELATED RESOURCES 1-->

    <div class="module m1">
        <div class="module m2">
            <div class="module m3">
                <div id="uiRowWrapper">
                    <div id="uiColWrapper">
                        <div class="bubble blue" id="textBoxWrapper">
                            <div id="uiSingleText">
                                <p><strong>MDF Resources</strong><br /><br />
                                    <ul class="linkedlist">
                                        <li><a href="/sfc/#version?selectedDocumentId=069D0000003DbZ7">MDF Program Guide</a></li>
                                        <li><a href="/sfc/#version?selectedDocumentId=069D0000003DbZC">MDF Quick How-To Guide</a></li>
                                        <li><a href="/sfc/#version?selectedDocumentId=069D0000003DbZH">MDF Partner FAQ</a></li>
                                        <li><a href="/apex/PPMarketing?SubPage=MarketingContacts">Partner Marketing Manager</a></li>
                                    </ul>
                                </p>
                            </div>
                        <div id="uiFooter"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--p>&nbsp;</p-->
    <!--RELATED RESOURCES 2-->

    <div class="module m1">
        <div class="module m2">
            <div class="module m3">
                <div id="uiRowWrapper">
                    <div id="uiColWrapper">
                        <div class="bubble blue" id="textBoxWrapper">
                            <div id="uiSingleText">
                                <p><strong>Related Resources</strong><br />
                                    <ul class="linkedlist">
                                        <li><apex:outputlink value="PPMarketing?SubPage=MarketingServices">Qlik Partner Marketing Services</apex:outputlink></li>
                                        <li><apex:outputlink value="/apex/PPEducation?SubPage=ppSubEducationTraining">Education and Training</apex:outputlink></li>
                                        <li><apex:outputlink value="PPMarketing?SubPage=Brand">Brand Center</apex:outputlink></li>
                                    </ul>
                                </p>
                            </div>
                        <div id="uiFooter"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div style="width:650px;" class="nowrap">

    <h2 style="color:#62ac1e;"><strong>MDF</strong></h2>

    <p>The new Market Development Fund (MDF) rewards you for co-marketing activities planned in partnership with your Qlik Partner Marketing Team. Together with eligible partners and a joint-marketing plan, we are more likely to win the business and successfully deploy business intelligence solutions that drive pipeline for both of us.</p>


    <div class="clearfix">
        <div class="floatleft mr20"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/PartnerType_color.png" width="53" alt="EligiblePartners"/></div>
        <div class="nowrap">
            <h2 class="nomargin">Eligible Partner Types:</h2>
                <p><font style="color:#62ac1e;">Elite Solution Providers, Solution Providers, Registered Partners, Master Resellers, Distribution Authorized Resellers and Distributors</font><br />
                    &nbsp; &nbsp; &bull; <strong><a href="/{!acc.Id}">{!acc.Name}</a></strong>, your partner type is: {!acc.Partner_Type__c}</p>                                       
        </div>
    </div>

    <div class="hr"></div> 

    <h2 style="color:#62ac1e;"><strong>Requesting Funds and Claiming for MDF</strong></h2>
    <p>Before you can use the MDF tool, you will need to send a request for access.  Please follow the instructions below.</p>

    <!--Registration Instructions BEGIN-->
    <p><a style="VISIBILITY: hidden; TEXT-DECORATION: none" id="ACCESS" class="ACCESS" title="ACCESS" name="ACCESS" alias="ACCESS"></a></p>

    <div class="clearfix">
        <div class="floatleft mr20"><img style="margin-left:10px; margin-right:10px;" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/LockOpen_color.png" width="33" alt="RequestAccess"/></div>
        <div class="nowrap">
            <h2 class="nomargin">Request Access to MDF</h2>
                    <p><a href="#" rel="toggle[register]" data-openimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/collapse.png" data-closedimage="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/product_support/expand.png" border="0" /></a>&nbsp;<a href="javascript:animatedcollapse.toggle('register')"><b>How to Request Access</b></a></p>

                        <div id="register" style="width: 550px; background: #ffffff; display:none">
                        <p>Please send an email to <a href="mailto:partnermdf@qlik.com?subject=Requesting MDF access for {!$User.FirstName} {!$User.LastName}, {!acc.Name}, {!acc.Partner_Type__c}, {!$User.Email}">partnermdf@qlik.com</a> and include the following:
                        <ul>
                        <li><b>Subject line:</b> Provide name, email and company information, or copy and paste the following personalized subject line: <i>"Requesting MDF Access for {!$User.FirstName} {!$User.LastName}, {!acc.Name}, {!acc.Partner_Type__c}, {!$User.Email}"</i></li>
                        <li>Please identify all the users who need access to the MDF Tool at your company and include their email in the Cc. We will need the following user types:<br />
                            <b>- Submitters:</b><br />
                            <ol>
                            <li>Those who will be submitting MDF Fund Requests (1-3 people)</li>
                            <li>Those who will be submitting MDF Claims (1-3 people)</li></ol>
                            <b>- Payment Contact –</b> The person who will be submitting summary invoices for reimbursement (only 1 person or 1 email alias allowed)</li>
                        </ul>
                        You will be provided notice within 3 business days of request results. Once you have confirmation of access, please use the link below to access the MDF tool.
                        </p>
                    </div>
                <p style="clear:left;"></p>

        </div>
    </div>

    <!-- Registration Instructions END-->

    <div class="clearfix">
        <div class="floatleft mr20"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/Payment_color.png" width="53" alt="AccessTool"/></div>
        <div class="nowrap">
            <h2 class="nomargin"><a target="_blank" href="#" class="external">Access MDF Tool</a></h2>

            <p>Once you receive confirmation from Qlik that you have access to MDF, you can <a target="_blank" href="#" class="external">access the Marketing Developement Fund tool</a>.</p>

            <div class="floatleft mr10"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/RulesEngine_color.png" width="30" style="vertical-align:middle; padding-left:20px;" /></div>
            <div class="nowrap">&nbsp;Subscribe to and download these key resources:<br />
                &nbsp; &nbsp; &bull; <a target="_blank" href="/sfc/#version?selectedDocumentId=1">MDF Terms and Conditions</a>: the full terms and conditions for using the MDF tool.<br />
                &nbsp; &nbsp; &bull; <a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003DbZ7">MDF Program Guide</a>: this explains the terms of the MDF Program.<br />
                &nbsp; &nbsp; &bull; <a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003DbZC">MDF Quick How-To</a>: step by step instructions for submission of Fund Request and Claims.<br />
                &nbsp; &nbsp; &bull; <a target="_blank" href="/sfc/#version?selectedDocumentId=069D0000003DbZH">MDF FAQ</a>: all your basic questions about MDF are answered here.
            </div>

            <p style="clear:left;"></p>

            <div class="floatleft mr10"><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/Partner%20Icons/BusinessQuestions-Color.png" width="30" style="vertical-align:middle; padding-left:20px;" /></div>
            <div class="nowrap">&nbsp;For additional assistance, please contact your local <a href="/apex/PPMarketing?SubPage=MarketingContacts">Partner Marketing Manager</a>.</div>

            <p style="clear:left;"></p>
        </div>
    </div>

    <img style="float:left;" src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/PlanExecuteClaim.png" alt="MDFProcess" title="MDFProcess" width="600"/>

    <p style="clear:left;"></p>

    <div class="hr"></div>

    <h2 style="color:#62ac1e;"><strong>Co-Marketing with Qlik<sup>&reg;</sup></strong></h2>

    <p>All partners can access a variety of marketing tools and programs that can be leveraged without cost. Access <a target="_blank" href="/apex/PPMarketing?SubPage=MarketingServices">Qlik Partner Marketing Services</a> to take advantage of:
        <ul>
            <li>co-branded campaigns</li>
            <li>web content syndication</li>
            <li>social media programs</li>
            <li>and more</li>
        </ul>

    Don’t forget our <a target="_blank" href="/apex/PPEducation?SubPage=Enablement">Fast Track Learning Webcasts</a> are available <a target="_blank" href="/apex/PPEducation?SubPage=Recent">on demand</a>. Learn about Qlik products and services so you can position yourself in the marketplace.</p>

    <div class="hr"></div>

    <p><a style="VISIBILITY: hidden; TEXT-DECORATION: none" id="GPF" class="GPF" title="GPF" name="GPF" alias="GPF"></a></p>

    <h2 style="color:#62ac1e;"><strong>Global Partner Funding (GPF)</strong></h2>

    <!--p><img src="https://qvfiles.s3.amazonaws.com/partner_portal/images/marketing/ppsubmarketing-gpf-program-cycle.jpg" alt="GPF" title="GPF" border="0" width="600" /></p-->

    <p>Following the launch of the Global Partner Market Development Fund Program, the Global Partner Fund (GPF) program has been retired. All new claims must go through the <a target="_blank" href="#" class="external">new MDF tool</a> after <a href="#ACCESS">requesting access</a> to the tool.</p>

    <p>To complete an old GPF request, please contact your <a href="/apex/PPMarketing?SubPage=MarketingContacts">Qlik Partner Marketing Manager</a> for instructions and assistance. All future requests for marketing reimbursements should be submitted through the<a href="/sfc/#version?selectedDocumentId=069D0000003DbZ7"> MDF Program Guidelines</a>.</p>

</div>
</apex:page>