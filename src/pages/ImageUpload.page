<!-----------------------------
*
* Change log
* 2019-03-05 IT-1390 extbad
*
 ---------------------------- -->

<apex:page id="ImageUpload" controller="ImageUploadController">
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"/>
    <apex:includeScript value="/soap/ajax/33.0/connection.js"/>
    <script>
        var jqEnv;
        if (typeof $ === 'undefined')
            jqEnv = jQuery.noConflict();
        else
            jqEnv = $.noConflict();
        var delimiter = '!,!';
        var images = {ids:'', names: '', errors: '', errorNames: ''};
        var filesToUploadCount;
        var uploadedImages;

        function cleanImagesInfo() {
            images.names = '';
            images.ids = '';
            images.errors = '';
            images.errorNames = '';
            uploadedImages = 0;
        }

        function uploadFiles() {
            cleanImagesInfo();

            var filesToUpload = [];
            var filesList = jqEnv("input[type=file]")[0].files;
            jqEnv(filesList).each(function () {
                filesToUpload.push(this);
            });
            filesToUploadCount = filesToUpload.length;

            if (filesToUploadCount === 0) {
                return;
            }

            sforce.connection.sessionId = "{!$Api.Session_ID}";
            for (var i = 0; i < filesToUploadCount; i++) {
                f = filesToUpload[i];
                var reader = new FileReader();

                // Keep a reference to the File in the FileReader so it can be accessed in callbacks
                reader.file = f;

                reader.onload = function (e) {
                    var doc = new sforce.SObject("Document");
                    doc.Name = this.file.name;
                    doc.FolderId = "{!folderId}";
                    doc.IsPublic = true;

                    var binary = "";
                    var bytes = new Uint8Array(e.target.result);
                    var length = bytes.byteLength;

                    for (var i = 0; i < length; i++) {
                        binary += String.fromCharCode(bytes[i]);
                    }

                    doc.Body = (new sforce.Base64Binary(binary)).toString();

                    createNewDocument(doc);
                };

                reader.readAsArrayBuffer(f);
            }
        }

        function createNewDocument(doc) {
            sforce.connection.create([doc], {
                onSuccess: function (result, source) {
                    if (result[0].getBoolean("success")) {
                        console.log("new Document created with id " + result[0].id);
                        images.names += doc.Name + delimiter;
                        images.ids += result[0].id + delimiter;
                    } else {
                        console.log("Failed to create new Document: " + result[0].errors.message);
                        images.errors += result[0].errors.message + delimiter;
                        images.errorNames += doc.Name + delimiter;
                    }
                    uploadedImages++;
                    if (uploadedImages === filesToUploadCount) { //check if all Documents were processed
                        addImages(images.names, images.ids, images.errorNames, images.errors);
                    }
                },
                onFailure: function (error, source) {
                    console.log("an error has occurred " + error);
                    images.errors += result[0].errors.message + delimiter;
                    images.errorNames += doc.Name + delimiter;
                    uploadedImages++;
                    if (uploadedImages === filesToUploadCount) { //check if all Documents were processed
                        addImages(images.names, images.ids, images.errorNames, images.errors);
                    }
                }
            });
        }

        function hideUploadButton () {
            jqEnv('.btn-upload').css('display', 'none');
            jqEnv('.btn-process').css('display', 'block');
        }

        function enableUploadButton () {
            if (jqEnv("input[type=file]")[0].files.length > 0) {
                jqEnv('.btn-upload').prop('disabled', false);
                jqEnv('.btn-upload').removeAttr('disabled');
            }
        }

        function copyLink (str) {
            // Create new element
            var el = document.createElement('textarea');
            // Set value (string to be copied)
            el.value = str;
            // Set non-editable to avoid focus and move outside of view
            el.setAttribute('readonly', '');
            // Make it invisible
            el.style = {position: 'absolute', left: '-9999px'};
            document.body.appendChild(el);
            // Select text inside element
            el.select();
            // Copy text to clipboard
            document.execCommand('copy');
            // Remove temporary element
            document.body.removeChild(el);
        }

    </script>
    <style>
        .previewImg {
            width: 150px;
        }
        .inBorder {
            padding-top:20px;
        }
        .commandButton {
            float:left
        }
    </style>
    <apex:form id="mainForm">
        <apex:pageblock id="imagesPageBlock" title="Upload multiple Images to the Article Images Documents folder">
            <apex:pageBlockSection columns="1" showHeader="false">
                <apex:outputPanel id="buttons">
                    <input type="file" multiple="multiple" class="commandButton" onchange="enableUploadButton()"/>
                    <input type="button" value="Upload" onClick="hideUploadButton();uploadFiles()"
                           class="commandButton btn-upload" disabled="disabled"/>
                    <input type="button" value="In process..." disabled="disabled"
                           class="commandButton btn-process"
                           style="display:none;"/>
                </apex:outputPanel>
                <apex:actionFunction name="addImages" id="addImages" action="{!addImages}" rerender="imagesPageBlock">
                    <apex:param value="" name="namesParam" id="imgName"/>
                    <apex:param value="" name="idsParam" id="imgId"/>
                    <apex:param value="" name="errorNamesParam" id="imgError"/>
                    <apex:param value="" name="errorsParam" id="imgNameError"/>
                </apex:actionFunction>
            </apex:pageBlockSection>
            <br/><br/>
            <apex:pageBlockSection collapsible="false"  columns="2" id="imagesSection" showHeader="false">
                <apex:dataTable value="{!imagesMap}" var="imgName" rendered="{!showImages}">
                    <apex:column styleClass="inBorder" style="width: 200px;">
                        <apex:facet name="header"><h2>Successful Images</h2></apex:facet>
                        <apex:image url="{!imagesMap[imgName]}" styleClass="previewImg"/>
                    </apex:column>
                    <apex:column styleClass="inBorder">
                        <apex:outputPanel >
                            <apex:outputText value="{!imgName}"/>
                            <br/><br/>
                            <apex:outputText value="{!imagesMap[imgName]}"/>
                            <br/><br/>
                            <apex:commandLink reRender="empty" onClick="copyLink('{!imagesMap[imgName]}')"
                                              style="color:#1797c0">Copy link</apex:commandLink>
                        </apex:outputPanel>
                    </apex:column>
                </apex:dataTable>
                <apex:dataTable value="{!imagesErrorsMap}" var="imgName" rendered="{!showErrors}">
                    <apex:column styleClass="inBorder" style="width: 200px;">
                        <apex:facet name="header"><h2>Failed Images</h2></apex:facet>
                        <apex:outputText value="{!imgName}"/>
                    </apex:column>
                    <apex:column styleClass="inBorder">
                        <apex:outputText value="{!imagesErrorsMap[imgName]}"/>
                    </apex:column>
                </apex:dataTable>
            </apex:pageBlockSection>
        </apex:pageblock>
    </apex:form>
</apex:page>